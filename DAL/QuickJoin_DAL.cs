﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class QuickJoin_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetRefNameAndPinNo(string PinNo)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetRefNameAndPinNo_sp");
                //db.AddInParameter(dbCmd, "RefID", DbType.StringFixedLength, RefID);
                db.AddInParameter(dbCmd, "PinNumber", DbType.StringFixedLength, PinNo);
                ds=db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }
            return ds;
        }

        public DataSet getLedgerName(string RefID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetRefName_sp");
                db.AddInParameter(dbCmd, "RefID", DbType.StringFixedLength, RefID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }
            return ds;
        }
    }
}
