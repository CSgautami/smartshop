﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class SubCategory_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetData()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetSubCategory_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }

        public string SaveSubCategory(string SubCategoryID, string SubCategory,string CategoryID)
        {
            string res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("InsertOrUpdateSubCategory_sp");
                db.AddInParameter(dbcmd, "SubCategoryID", DbType.StringFixedLength, SubCategoryID);
                db.AddInParameter(dbcmd, "SubCategory", DbType.StringFixedLength, SubCategory);
                db.AddInParameter(dbcmd, "CategoryID", DbType.StringFixedLength, CategoryID);
                res = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            { throw; }
            return res;
        }

        public string DeleteSubCategory(string SubCategoryID)
        {
            string res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteSubCategory_sp");
                db.AddInParameter(dbcmd, "SubCategoryID", DbType.StringFixedLength, SubCategoryID);
                res = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }
    }
}
