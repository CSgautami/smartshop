﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;


namespace DAL
{
    public class ChangePwd_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string ChangePassword(string OldPassword, string NPassword,string UserID,string Type)
        {
            string strReturn;
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("ChangePassword_sp");
                //db.AddInParameter(dbCmd, "Password", DbType.StringFixedLength, OldPassword);
                db.AddInParameter(dbCmd, "NPassword", DbType.StringFixedLength, NPassword);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "Type", DbType.StringFixedLength, Type);
                strReturn = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                return "0";
            }
            return strReturn;
        }
    }
}

    
