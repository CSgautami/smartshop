﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public  class Login_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public string CheckAuthorization(string UserID, string URL)
        {
            string rtnval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("CheckAuthorization_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "URL", DbType.StringFixedLength, URL);
                rtnval = db.ExecuteScalar(dbcmd).ToString();
            }
            catch (Exception ex)
            {
                rtnval = "";
            }
            return rtnval;
        }

        public DataSet GetUserMenu(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetUserMenu_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetName(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetName_sp");
                db.AddInParameter(dbCmd, "UserID",DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetNameID(string CID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetNameID_sp");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, CID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string UpdatePassword(string NewPassword, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();

            DbCommand dbCmd = db.GetStoredProcCommand("UpdatePassword_sp");
            db.AddInParameter(dbCmd, "Password", DbType.StringFixedLength, NewPassword);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID );
            return db.ExecuteScalar(dbCmd).ToString();
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }
    }
}
