﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DAL
{
    public class UserMaping_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet GetUser()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetUsers_sp");
            return db.ExecuteDataSet(dbCmd);

        }
        public DataSet GetBranch()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBranch_sp");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, "0");
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetPages()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPages_sp");
            return db.ExecuteDataSet(dbCmd);
        }

        public void SavePages(string UserID, string UserPages)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("SaveUserPages_sp");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "UserPages", DbType.StringFixedLength, UserPages);
            db.ExecuteScalar(dbCmd);
        }

        public DataSet GetUserData(string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetUserData_sp");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }
    }
}
