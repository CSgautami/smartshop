﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
namespace DAL
{
    public class GroupsFD_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        public DataSet GetStockItem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("StockItemName_sp");            
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetData()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetGroupsForDisplay_sp");
            return db.ExecuteDataSet(dbCmd);
        }

        public string SaveData(int nID, string Group1, string Group2, string Group3, string Group4, string Group5, string Group6, string Group7, string Group8, string ItemID1, string ItemID2, string ItemID3, string ItemID4, string ItemID5, string ItemID6, string ItemID7, string ItemID8, string Desc1, string Desc2, string Desc3, string Desc4, string Desc5, string Desc6, string Desc7, string Desc8, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("SaveGroupsForDisplay_sp");
                db.AddInParameter(dbcmd, "ID", DbType.StringFixedLength, nID);
                db.AddInParameter(dbcmd, "GroupName1", DbType.StringFixedLength, Group1);
                db.AddInParameter(dbcmd, "GroupName2", DbType.StringFixedLength, Group2);
                db.AddInParameter(dbcmd, "GroupName3", DbType.StringFixedLength, Group3);
                db.AddInParameter(dbcmd, "GroupName4", DbType.StringFixedLength, Group4);
                db.AddInParameter(dbcmd, "GroupName5", DbType.StringFixedLength, Group5);
                db.AddInParameter(dbcmd, "GroupName6", DbType.StringFixedLength, Group6);
                db.AddInParameter(dbcmd, "GroupName7", DbType.StringFixedLength, Group7);
                db.AddInParameter(dbcmd, "GroupName8", DbType.StringFixedLength, Group8);
                db.AddInParameter(dbcmd, "ItemID1", DbType.StringFixedLength, ItemID1);
                db.AddInParameter(dbcmd, "ItemID2", DbType.StringFixedLength, ItemID2);
                db.AddInParameter(dbcmd, "ItemID3", DbType.StringFixedLength, ItemID3);
                db.AddInParameter(dbcmd, "ItemID4", DbType.StringFixedLength, ItemID4);
                db.AddInParameter(dbcmd, "ItemID5", DbType.StringFixedLength, ItemID5);
                db.AddInParameter(dbcmd, "ItemID6", DbType.StringFixedLength, ItemID6);
                db.AddInParameter(dbcmd, "ItemID7", DbType.StringFixedLength, ItemID7);
                db.AddInParameter(dbcmd, "ItemID8", DbType.StringFixedLength, ItemID8);
                db.AddInParameter(dbcmd, "Desc1", DbType.StringFixedLength, Desc1);
                db.AddInParameter(dbcmd, "Desc2", DbType.StringFixedLength, Desc2);
                db.AddInParameter(dbcmd, "Desc3", DbType.StringFixedLength, Desc3);
                db.AddInParameter(dbcmd, "Desc4", DbType.StringFixedLength, Desc4);
                db.AddInParameter(dbcmd, "Desc5", DbType.StringFixedLength, Desc5);
                db.AddInParameter(dbcmd, "Desc6", DbType.StringFixedLength, Desc6);
                db.AddInParameter(dbcmd, "Desc7", DbType.StringFixedLength, Desc7);
                db.AddInParameter(dbcmd, "Desc8", DbType.StringFixedLength, Desc8);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteData(int nID)
        {
            String rtnval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteGroupsForDisplay_sp");
                db.AddInParameter(dbcmd, "ID", DbType.StringFixedLength, nID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                rtnval = "";
            }
            return rtnval;
        }
    }
}
