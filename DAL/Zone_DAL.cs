﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Zone_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string InsertZone(string ZoneNo, string ZoneName, string Address, string Phone, string UserID,string StartID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertZone_sp");
                db.AddInParameter(dbcmd, "ZoneNo", DbType.StringFixedLength, ZoneNo);
                db.AddInParameter(dbcmd, "ZoneName", DbType.StringFixedLength, ZoneName);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "Phone", DbType.StringFixedLength, Phone);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "StartID", DbType.StringFixedLength, StartID);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public DataSet GetZone(string strSearch)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetZone_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, strSearch);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }


        public string UpdateZone(string ZoneID, string ZoneNo, string ZoneName, string Address, string Phone, string UserID,string StartID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateZone_sp");
                db.AddInParameter(dbcmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbcmd, "ZoneNo", DbType.StringFixedLength, ZoneNo);
                db.AddInParameter(dbcmd, "ZoneName", DbType.StringFixedLength, ZoneName);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "Phone", DbType.StringFixedLength, Phone);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "StartID", DbType.StringFixedLength, StartID);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                    rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteZone(string ZoneID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteZone_sp");
                db.AddInParameter(dbcmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        

        }
    }
}



        
    

 
     
    
    



        

