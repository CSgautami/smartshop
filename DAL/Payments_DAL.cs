﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DAL
{
    public class Payments_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetPaymentReport(string BranchID, string DateFrom, string DateTo)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPaymentReport_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, DateFrom);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, DateTo);
                ds = db.ExecuteDataSet(dbCmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
            
        }
    }
}
