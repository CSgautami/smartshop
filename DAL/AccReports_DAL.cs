﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class AccReports_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
                
        public DataSet GetBookData(string UserID, string FromDate, string ToDate,string Voch)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetBookData_sp");
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "Voch", DbType.StringFixedLength, Voch);
                ds = db.ExecuteDataSet(dbCmd);

            }
            catch (Exception ex)
            {
            }
            return ds;

        }
    }
}
