﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Branch_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public string InsertBranch(string BranchName, string BranchAddress, string ContactPerson, string ContactNo, string EmailID, string BranchType, string Circle, string Division, string Designation, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertBranch_sp");
                db.AddInParameter(dbcmd, "BranchName", DbType.StringFixedLength, BranchName);
                db.AddInParameter(dbcmd, "BranchAddress", DbType.StringFixedLength, BranchAddress);
                db.AddInParameter(dbcmd, "ContactPerson", DbType.StringFixedLength, ContactPerson);
                db.AddInParameter(dbcmd, "ContactNo", DbType.StringFixedLength, ContactNo);
                db.AddInParameter(dbcmd, "EmailID", DbType.StringFixedLength, EmailID);
                db.AddInParameter(dbcmd, "BranchType", DbType.StringFixedLength,BranchType);
                db.AddInParameter(dbcmd, "Circle", DbType.StringFixedLength, Circle);
                db.AddInParameter(dbcmd, "Division", DbType.StringFixedLength, Division);
                db.AddInParameter(dbcmd, "Designation", DbType.StringFixedLength, Designation);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }


        public DataSet GetBranch()
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranch_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, "0");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public string UpdateBranch(string BranchID, string BranchName, string BranchAddress, string ContactPerson, string ContactNo, string EmailID, string BranchType, string Circle, string Division, string Designation, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateBranch_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "BranchName", DbType.StringFixedLength, BranchName);
                db.AddInParameter(dbcmd, "BranchAddress", DbType.StringFixedLength, BranchAddress);
                db.AddInParameter(dbcmd, "ContactPerson", DbType.StringFixedLength, ContactPerson);
                db.AddInParameter(dbcmd, "ContactNo", DbType.StringFixedLength, ContactNo);
                db.AddInParameter(dbcmd, "EmailID", DbType.StringFixedLength, EmailID);
                db.AddInParameter(dbcmd, "BranchType", DbType.StringFixedLength, BranchType);
                db.AddInParameter(dbcmd, "Circle", DbType.StringFixedLength, Circle);
                db.AddInParameter(dbcmd, "Division", DbType.StringFixedLength, Division);
                db.AddInParameter(dbcmd, "Designation", DbType.StringFixedLength, Designation);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteBranch(string BranchID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteBranch_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }
    }
}


    


        
    
