﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class ShopingCart_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet SaveTempOrder(string ItemID, string Qty, string BathNo, string TempID, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            DataSet ds=null;
            string strRval;
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                
                DbCommand dbcmd = db.GetStoredProcCommand("SaveTempOrder_sp");
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbcmd, "Qty", DbType.StringFixedLength, Qty);
                db.AddInParameter(dbcmd, "BatchNo", DbType.StringFixedLength, BathNo);
                db.AddInParameter(dbcmd, "TOID", DbType.StringFixedLength, TempID);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                strRval=Convert.ToString( db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
                DbCommand dbcmd1 = db.GetStoredProcCommand("GetTempOrderDetails_sp");
                db.AddInParameter(dbcmd1, "TOID", DbType.StringFixedLength, strRval );
                ds = db.ExecuteDataSet(dbcmd1);               
                
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                ds= new DataSet();
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return ds;
            
        }

        public DataSet GetProductDetails(string TempOrderID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTempOrderDetails_sp");
            db.AddInParameter(dbCmd, "TOID", DbType.StringFixedLength, TempOrderID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string DeleteTempOrder(int TODID)
        {
            try
            {
                string rVal = "";
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteTempOrder_sp");                
                db.AddInParameter(dbcmd, "TODID", DbType.StringFixedLength, TODID);                
                rVal = Convert.ToString(db.ExecuteScalar(dbcmd));
                return rVal;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string UpdateTempOrder(int TODID)
        {
                DbConnection dbCon = null;
                DbTransaction dbTrans = null;
                String rVal = "";
                try
                {
                    dbCon = db.CreateConnection();
                    dbCon.Open();
                    dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateTempOrder_sp");
                db.AddInParameter(dbcmd, "TOID", DbType.StringFixedLength, TODID);
                //db.AddInParameter(dbcmd, "TempTable", DbType.StringFixedLength, TempOrderDet);
                //db.AddInParameter(dbcmd, "TOID", DbType.StringFixedLength, TempOrderID);
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
                                  
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rVal = "";
            }
                finally
                {
                    if (dbCon != null)
                        dbCon.Close();
                }
                return rVal;
        }

        public string UpdateTempOrderQtyChange(string TempOrderID, string ItemID, string Qty,string Price)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
                String rVal = "";
                try
                {
                    dbCon = db.CreateConnection();
                    dbCon.Open();
                    dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateTempOrderQtyChange_sp");
                db.AddInParameter(dbcmd, "TempOrderID", DbType.StringFixedLength, TempOrderID);
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbcmd, "Qty", DbType.StringFixedLength, Qty);
                db.AddInParameter(dbcmd, "Price", DbType.StringFixedLength, Price);
                rVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();                
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rVal = "";
            }
                finally
                {
                    if (dbCon != null)
                        dbCon.Close();
                }

                return rVal;
        }
        public DataSet GetAddress(string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetAddressForUser_sp");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string SaveOrder(string TempOrderID,string Amount,string DCharges,string Address,string Transport,string ModeOfPayment,string PaymentStatus)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
                String rVal = "";
                try
                {
                    dbCon = db.CreateConnection();
                    dbCon.Open();
                    dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("SaveOrder_sp");
                db.AddInParameter(dbcmd, "TOID", DbType.StringFixedLength, TempOrderID);
                db.AddInParameter(dbcmd, "Amount", DbType.StringFixedLength, Amount );
                db.AddInParameter(dbcmd, "DCharges", DbType.StringFixedLength, DCharges);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "Transport", DbType.StringFixedLength, Transport);
                db.AddInParameter(dbcmd, "ModeOfPayment", DbType.StringFixedLength, ModeOfPayment);
                db.AddInParameter(dbcmd, "PaymentStatus", DbType.StringFixedLength, PaymentStatus);             
                rVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                throw;
            }
                finally
                {
                    if (dbCon != null)
                        dbCon.Close();
                }
                return rVal;

        }

        public DataSet GetPrintData(string OrderID,string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetShoppingCartPrintData_sp");
            db.AddInParameter(dbCmd, "OrderID", DbType.StringFixedLength, OrderID);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetOrderHistory(string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetOrderHistoryReport_sp");            
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetOrderReport(string FromDate,string ToDate , string Status,string BranchID, string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetOrderReport_sp");
            db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
            db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
            db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, Status);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetOrderSummury(string OrderID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetOrderDetailsReport_sp");
            db.AddInParameter(dbCmd, "OrderID", DbType.StringFixedLength, OrderID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetStockAvailability(int ItemID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetStockAvailability_sp");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetData(string TempID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTempOrderDetails_sp");
            db.AddInParameter(dbCmd, "TOID", DbType.StringFixedLength, TempID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet DeleteTemp(string ItemID,string TOID)
        {
            DataSet ds;
            try
            {
                string rVal = "";
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteTemp_sp");
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbcmd, "TOID", DbType.StringFixedLength, TOID);
                rVal = Convert.ToString(db.ExecuteScalar(dbcmd));
                DbCommand dbcmd1 = db.GetStoredProcCommand("GetTempOrderDetails_sp");
                db.AddInParameter(dbcmd1, "TOID", DbType.StringFixedLength, TOID);
                ds = db.ExecuteDataSet(dbcmd1);  
                return ds;
            }
            catch (Exception ex)
            {
                throw;
            }
           // return ds;
        }

        public DataSet GetOrderShipmentDet(string OrderID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetOrderShipmentDet_sp");
            db.AddInParameter(dbCmd, "OrderID", DbType.StringFixedLength, OrderID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string SaveOrderShipmentDetails(string OrderID, string ShipmentDet, string ShipmentDetID, string Transport, string DisDate, string TrackNo, string UserID)
        {
            string rtnval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("SaveOrderShipmentDetails_sp");
                db.AddInParameter(dbcmd, "OrderID", DbType.StringFixedLength, OrderID);
                db.AddInParameter(dbcmd, "ShipmentDet", DbType.StringFixedLength, ShipmentDet);
                db.AddInParameter(dbcmd, "ShipmentDetID", DbType.StringFixedLength, ShipmentDetID);
                db.AddInParameter(dbcmd, "TransportName", DbType.StringFixedLength, Transport);
                db.AddInParameter(dbcmd, "DisputeDate", DbType.StringFixedLength, DisDate);
                db.AddInParameter(dbcmd, "TrackNo", DbType.StringFixedLength, TrackNo);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);

                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex) { throw; }
            return rtnval;
        }

        public string SaveInProcess(string OrderIDs)
        {
            string rtnval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("SaveInProcess_sp");
                db.AddInParameter(dbcmd, "OrderIDs", DbType.StringFixedLength, OrderIDs);                
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex) { throw; }
            return rtnval;
        }

        public string CancelClick(string OrderID)
        {
            string rtnval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("CancelOrder_sp");
                db.AddInParameter(dbcmd, "OrderID", DbType.StringFixedLength, OrderID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex) { throw; }
            return rtnval;
        }
    }
}
