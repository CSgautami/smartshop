﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.IO;

namespace DAL
{
    public class TaxSystem_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetVatMaster()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetVatMaster_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetVatTypeMaster()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetVatTypeMaster_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public string InsertTaxSystem(string TaxName, string ExDuty, string CST, string Other, string SAT, string Rate, string VAT, string Type, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertTaxSystem_sp");
                //db.AddInParameter(dbcmd, "TaxNo", DbType.StringFixedLength, TaxNo);
                db.AddInParameter(dbcmd, "TaxName", DbType.StringFixedLength, TaxName);
                db.AddInParameter(dbcmd, "ExDuty", DbType.StringFixedLength, ExDuty);
                db.AddInParameter(dbcmd, "CST", DbType.StringFixedLength, CST);
                db.AddInParameter(dbcmd, "Other", DbType.StringFixedLength, Other);
                db.AddInParameter(dbcmd, "SAT", DbType.StringFixedLength, SAT);
                db.AddInParameter(dbcmd, "VATRATE", DbType.StringFixedLength, Rate);
                db.AddInParameter(dbcmd, "VatID", DbType.StringFixedLength, VAT);
                db.AddInParameter(dbcmd, "VatTypeID", DbType.StringFixedLength, Type);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtval =Convert.ToString( db.ExecuteScalar(dbcmd,dbTrans).ToString());
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }


        public DataSet GetTaxSystem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetTaxSystemMaster_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }


        public string UpdateTaxSystem(string TaxNo, string TaxName, string ExDuty, string CST, string Other, string SAT, string Rate, string VAT, string Type, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateTaxSystem_sp");
                db.AddInParameter(dbcmd, "TaxNo", DbType.StringFixedLength, TaxNo);
                db.AddInParameter(dbcmd, "TaxName", DbType.StringFixedLength, TaxName);
                db.AddInParameter(dbcmd, "ExDuty", DbType.StringFixedLength, ExDuty);
                db.AddInParameter(dbcmd, "CST", DbType.StringFixedLength, CST);
                db.AddInParameter(dbcmd, "Other", DbType.StringFixedLength, Other);
                db.AddInParameter(dbcmd, "SAT", DbType.StringFixedLength, SAT);
                db.AddInParameter(dbcmd, "VATRATE", DbType.StringFixedLength, Rate);
                db.AddInParameter(dbcmd, "VatID", DbType.StringFixedLength, VAT);
                db.AddInParameter(dbcmd, "VatTypeID", DbType.StringFixedLength, Type);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans).ToString());
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteTaxSystem(string TaxNo)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteTaxSystem_sp");
                db.AddInParameter(dbcmd, "TaxNo", DbType.StringFixedLength, TaxNo);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }
    }
}
        
        
    
 

