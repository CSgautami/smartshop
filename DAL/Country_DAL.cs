﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DAL
{
    public class Country_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetCountrys()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCountry_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        public string InsertCountry(string CountryName)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertCountry_sp");
                db.AddInParameter(dbcmd, "CountryName", DbType.StringFixedLength, CountryName);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }
        public string UpdateCountrys(int CountryID, string CountryName)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateCountry_sp");
                db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                db.AddInParameter(dbcmd, "CountryName", DbType.StringFixedLength, CountryName);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public string DeleteCountry(int CountryID)
        {
            {
                string rtn = "";
                try
                {
                    DbCommand dbcmd = db.GetStoredProcCommand("DeleteCountry_sp");
                    //db.AddInParameter(dbcmd, "ServiceID", DbType.StringFixedLength, ServiceID);
                    db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                    rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
                }
                catch (Exception ex)
                {
                }
                return rtn;

            }
        }
    }
}
