﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Company_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetTaxSystem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetTaxSystem_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }


        public string InsertCompany(string CompanyName, string Address, string TaxSystem, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertCompany_sp");
                db.AddInParameter(dbcmd,"CompanyName", DbType.StringFixedLength, CompanyName);
                db.AddInParameter(dbcmd,"Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd,"TaxNo", DbType.StringFixedLength, TaxSystem);
                db.AddInParameter(dbcmd,"UserID", DbType.StringFixedLength, UserID);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }


        public DataSet GetCompany(string Search)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCompany_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, Search);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public string UpdateCompany(string CompanyID, string CompanyName, string Address, string TaxSystem, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateCompany_sp");
                db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, CompanyID);
                db.AddInParameter(dbcmd, "CompanyName", DbType.StringFixedLength, CompanyName);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "TaxNo", DbType.StringFixedLength, TaxSystem);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval =Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans).ToString());
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }

            return rtnval;
        }

        public string DeleteCompany(string CompanyID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteCompany_sp");
                db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, CompanyID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;


        }

        
        }
    }
