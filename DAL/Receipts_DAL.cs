﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DAL
{
    public class Receipts_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetReceiptReport(string BranchID, string DateTo, string DateFrom)
        {

            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetReceiptReport_sp");
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, DateTo);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, DateFrom);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbCmd);

            }
            catch (Exception ex)
            {
            }
            return ds;

        }
    }
}
