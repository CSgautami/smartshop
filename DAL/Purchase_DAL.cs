﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DAL
{
    public class Purchase_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetLedgerName(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSupplier_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetLedgerDetails(string LedgerID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetLedgerDetails_SP");
            db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetAccLedger(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPurchaseAccLedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetStockItem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("StockItemName_sp");
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetTaxSystem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTaxSystem_sp");
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }
        public string SaveInvoice(string InvID, string UInvID, string InvNo, string Date, string PurInvNo, string PurDate, string LNo, string Address, string Mode, string Days, string Note, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,string UserID, string XmlString)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string res="";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SavePurchase_SP");
                db.AddInParameter(dbCmd, "InvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "UInvID", DbType.StringFixedLength, UInvID);
                //db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, Status);
                db.AddInParameter(dbCmd, "InvNo", DbType.StringFixedLength, InvNo);
                db.AddInParameter(dbCmd, "InvDate", DbType.StringFixedLength, Date);
                db.AddInParameter(dbCmd, "DCNo", DbType.StringFixedLength, PurInvNo);
                db.AddInParameter(dbCmd, "DCDate", DbType.StringFixedLength, PurDate);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LNo);
                db.AddInParameter(dbCmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbCmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbCmd, "Days", DbType.StringFixedLength, Days);
                db.AddInParameter(dbCmd, "Note", DbType.StringFixedLength, Note);
                db.AddInParameter(dbCmd, "ACLedgerID", DbType.StringFixedLength, AcLNo);
                db.AddInParameter(dbCmd, "TotalAmount", DbType.StringFixedLength, TotAmt);
                db.AddInParameter(dbCmd, "TotalDisc", DbType.StringFixedLength, TotDisc);
                db.AddInParameter(dbCmd, "TotalTax", DbType.StringFixedLength, TotTax);
                db.AddInParameter(dbCmd, "NetDisc", DbType.StringFixedLength, NetDisc);
                db.AddInParameter(dbCmd, "Frieght", DbType.StringFixedLength, Frieght);
                db.AddInParameter(dbCmd, "LULCharges", DbType.StringFixedLength, LAndUL);
                db.AddInParameter(dbCmd, "Adjustment", DbType.StringFixedLength, Adj);
                db.AddInParameter(dbCmd, "NetAmount", DbType.StringFixedLength, NetAmt);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID );
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, XmlString);
                res= Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }
        public DataSet GetTax(string tax)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTaxRate_sp");
            db.AddInParameter(dbCmd, "TaxNo", DbType.StringFixedLength, tax);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetData(string BranchID, string InvID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPurchaseData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "InvID", DbType.StringFixedLength, InvID);
            //db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, "P");
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }
        //public DataSet GetInvMaxID(string UserID)
        //{
        //    DbCommand dbCmd = db.GetStoredProcCommand("GetInvoiceMaxID_SP");
        //    db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
        //    db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, "P");
        //    return db.ExecuteDataSet(dbCmd);
        //}

        public DataSet GetBranch(string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBranch_SP");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetItemWiseTaxSystem(string ItemID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetItemWiseTaxSystem_sp");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, "P");
            return db.ExecuteDataSet(dbCmd);
        }

        public string DeletePurchase(string InvID,string BranchID)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("DeletePurchase_SP");
                db.AddInParameter(dbCmd, "InvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }

        public DataSet GetPriceAndMRP(string ItemID, string BatchNo)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPriceAndMRP_sp");
                db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbCmd, "BatchNo", DbType.StringFixedLength, BatchNo); 
                ds= db.ExecuteDataSet(dbCmd);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }
        public DataSet CallFillItem(string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillItem_sp");
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetSearchInvoiceData(string BranchID, string InvID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSearchPurchaseInvoiceData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "InvNo", DbType.StringFixedLength, InvID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetInvoiceBillData(string strInvID, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPurchaseInvoiceBillData_sp");
            db.AddInParameter(dbCmd, "InvID", DbType.StringFixedLength, strInvID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            
            return db.ExecuteDataSet(dbCmd);
        }
    }
}
