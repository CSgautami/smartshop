﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DAL
{
    public class District_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        public string InsertDistrict(string DistrictName)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertDistrict_sp");
                db.AddInParameter(dbcmd, "DistrictName", DbType.StringFixedLength, DistrictName);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public DataSet GetDistrict()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetDistrict_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string UpdateDistrict(string DistrictID, string DistrictName)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateDistrict_sp");
                db.AddInParameter(dbcmd, "DistrictID", DbType.StringFixedLength, DistrictID);
                db.AddInParameter(dbcmd, "DistrictName", DbType.StringFixedLength, DistrictName);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public string DeleteDistrict(string DistrictID)
        {
            string rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteDistrict_sp");
                db.AddInParameter(dbcmd, "DistrictID", DbType.StringFixedLength, DistrictID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
            }
            return rtn;
        }
    }
}
