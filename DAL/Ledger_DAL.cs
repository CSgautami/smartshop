﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
namespace DAL
{
    public class Ledger_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        //public DataSet GetGroupName(string BranchID)
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand dbcmd = db.GetStoredProcCommand("GetGroupName_sp");
        //        db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
        //        ds = db.ExecuteDataSet(dbcmd);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return ds;
        //}
        public string SaveLedger(string YourID,string Password, string BranchID, string LedgerName,string RefNo,string LandMark, string GroupID, string Status, string Amount,  string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserId,string PinNo)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertLedger_sp");
                db.AddInParameter(dbcmd, "YourID", DbType.StringFixedLength, YourID );
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password );
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "LedgerName", DbType.StringFixedLength, LedgerName);
                db.AddInParameter(dbcmd, "RefNo", DbType.StringFixedLength, RefNo );
                db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, LandMark );
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, GroupID);
                db.AddInParameter(dbcmd, "Status", DbType.StringFixedLength, Status);
                db.AddInParameter(dbcmd, "Amount", DbType.StringFixedLength, Amount);                
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "Town", DbType.StringFixedLength, Town);
                db.AddInParameter(dbcmd, "Phone", DbType.StringFixedLength, Phone);
                db.AddInParameter(dbcmd, "APGST", DbType.StringFixedLength, APGST);
                db.AddInParameter(dbcmd, "Mode", DbType.StringFixedLength, Mode );
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, Area);
                db.AddInParameter(dbcmd, "Sales", DbType.StringFixedLength, Sales);
                db.AddInParameter(dbcmd, "Purchase", DbType.StringFixedLength, Purchase);
                db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserId);
                db.AddInParameter(dbcmd, "PinNo", DbType.StringFixedLength, PinNo);
                //db.AddInParameter(dbcmd, "XmlPinDet", DbType.StringFixedLength, XmlDet);
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;
        }

        //public string UpdateLedger(string LedgerNo, string YourID, string Password, string BranchID, string LedgerName, string RefNo, string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserId)
        public string UpdateLedger(string LedgerID, string LedgerNo,  string Password, string BranchID, string LedgerName, string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserId)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateLedger_sp");
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                db.AddInParameter(dbcmd, "LedgerNo", DbType.StringFixedLength, LedgerNo);                
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "LedgerName", DbType.StringFixedLength, LedgerName);
                //db.AddInParameter(dbcmd, "RefNo", DbType.StringFixedLength, RefNo);
                db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, LandMark);
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, GroupID);
                db.AddInParameter(dbcmd, "Status", DbType.StringFixedLength, Status);
                db.AddInParameter(dbcmd, "Amount", DbType.StringFixedLength, Amount);                
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "Town", DbType.StringFixedLength, Town);
                db.AddInParameter(dbcmd, "Phone", DbType.StringFixedLength, Phone);
                db.AddInParameter(dbcmd, "APGST", DbType.StringFixedLength, APGST);
                db.AddInParameter(dbcmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, Area);
                db.AddInParameter(dbcmd, "Sales", DbType.StringFixedLength, Sales);
                db.AddInParameter(dbcmd, "Purchase", DbType.StringFixedLength, Purchase);
                db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserId);
                //db.AddInParameter(dbcmd, "PinNo", DbType.StringFixedLength, PinNo);
                //db.AddInParameter(dbcmd, "XmlPinDet", DbType.StringFixedLength, XmlDet);
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;
        }

        public DataSet GetLedgerDisplayData(string BranchID, string FromDate, string ToDate, string LedgerID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetLedgerDisplayData_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetLedgerDisplayLedgers(string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetLedgerDisplayLedgers_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetLMNavData(string Search,string BranchID, string LMNO, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetLMNavData_sp");
            db.AddInParameter(dbCmd, "SearchYourID", DbType.StringFixedLength, Search);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "LMNO", DbType.StringFixedLength, LMNO);
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetBranch(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranch_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }



        public DataSet GetGroups(string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetGroupName_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetArea()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetArea_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetRefMember(string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetRef_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        public DataSet GetRefName(string LedgerID)
        {            
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetLedgerDetails_SP");
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetGroupDetails(string GroupID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetGroupDetails_sp");
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, GroupID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetPins(string RefNo)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetPins_sp");
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, RefNo);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string DeleteLedger(string LedgerID)
        {
            string rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteLedger_SP");
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
            }
            return rtn;
        }

        //public DataSet GetSearch(string Search)
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand dbcmd = db.GetStoredProcCommand("GetSearchData_sp");
        //        db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, Search);
        //        ds = db.ExecuteDataSet(dbcmd);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return ds;
        //}
    }
}






