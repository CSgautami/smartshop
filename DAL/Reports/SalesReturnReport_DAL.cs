﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;
namespace DAL.Reports
{
    public class SalesReturnReport_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet SalesReturnReportData(string LedgerID, string FromDate, string ToDate,string Mode,string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetSalesReturnReportData_sp");
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, LedgerID );
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "Mode", DbType.StringFixedLength, Mode );
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }
    }
}
