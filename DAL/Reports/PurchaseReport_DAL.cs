﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL.Reports
{
    public class PurchaseReport_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet PurchaseReportBetweenDates(string Branch, string FromDate, string ToDate, string Mode, string Name,string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("PurchaseReportBetweenDates_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, Branch);
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbcmd, "Name", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetStockGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroupName_sp");
                //db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID );
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HGID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetCompany()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCompany_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                //db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetPurchaseReportGroupAndComWise(string Branch, string FromDate, string ToDate, string Group, string Company,  string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetGroupAndComWisePurchaseReport_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, Branch);
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, Group);
                db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, Company);
                //db.AddInParameter(dbcmd, "HGID", DbType.StringFixedLength, HeadGroup);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetPurchaseReportInvWise(string Branch, string FromDate, string ToDate, string FromInv, string ToInv, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetInvWisePurchaseReport_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, Branch);
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "FromInvNo", DbType.StringFixedLength, FromInv);
                db.AddInParameter(dbcmd, "ToInvNo", DbType.StringFixedLength, ToInv);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetPurchaseReportProductWise(string Branch, string FromDate, string ToDate, string Product, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetProdWisePurchaseReport_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, Branch);
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, Product);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetStockItem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("StockItemName_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetHeadGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetHeadGroup_sp");
                //db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
    }
}

        



