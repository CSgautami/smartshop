﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;
namespace DAL.Reports
{
    public class SaleReport_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet SaleReportBetweenDates(string FromDate, string ToDate, string Mode, string Name, string BranchID,string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("SaleReportBetweenDates_sp");
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbcmd, "Name", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID );
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetStockGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroupName_sp");
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HGID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetCompany()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCompany_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetSaleReportGroupAndComWise(string FromDate, string ToDate, string Group, string Company, string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetGroupAndComWiseSalesReport_sp");
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                //db.AddInParameter(dbcmd, "HGID", DbType.StringFixedLength, HeadGroup);
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, Group);
                db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, Company);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID );
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetSaleReportInvWise(string FromDate, string ToDate, string InvFrom, string InvTo, string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetInvWiseSalesReport_sp");
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "FromInvNo", DbType.StringFixedLength, InvFrom );
                db.AddInParameter(dbcmd, "ToInvNo", DbType.StringFixedLength, InvTo );
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID );
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID );
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetSaleReportProductWise(string FromDate, string ToDate, string Product, string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetProdWiseSalesReport_sp");
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, Product);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID );
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetStockItem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("StockItemName_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetHeadGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetHeadGroup_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
    }
}
