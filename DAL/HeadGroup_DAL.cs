﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;
//using System.Data.SqlClient;
//using System.Configuration;

namespace DAL
{
    public class HeadGroup_DAL
    {
        //SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Constr"].ToString());
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string InsertHeadGroup(string HeadGroupName,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertHeadGroup_sp");
                db.AddInParameter(dbcmd, "HeadGroupName", DbType.StringFixedLength, HeadGroupName);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;


        }

        public DataSet GetHeadGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                //SqlDataAdapter da = new SqlDataAdapter("GetHeadGroup_sp", con);
                //da.SelectCommand.CommandType = CommandType.StoredProcedure;
                //da.Fill(ds);
                DbCommand dbcmd = db.GetStoredProcCommand("GetHeadGroup_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string UpdateHeadGroup(string HeadGroupID, string HeadGroupName,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateHeadGroup_sp");
                db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroupID);
                db.AddInParameter(dbcmd, "HeadGroupName", DbType.StringFixedLength, HeadGroupName);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                throw;
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        
        }

        public string DeleteHeadGroup(string HeadGroupID)
        {
            String rtval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteHeadGroup_sp");
                db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroupID);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtval;
        }
      }
    }

        
    

