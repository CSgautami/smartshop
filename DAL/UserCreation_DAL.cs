﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class UserCreation_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        public string CreateUser(string UserName,string Password,string BranchID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertUser_sp");
                db.AddInParameter(dbcmd, "UserName", DbType.StringFixedLength, UserName);
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
                //db.AddInParameter(dbcmd, "CPassword", DbType.StringFixedLength, ConformPassword);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;
        }
        public DataSet GetBranch()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranch_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, "0");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public DataSet CheckUserCreation(string UserName, string Password,string Type)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("CheckUserCreation_sp");
                db.AddInParameter(dbcmd, "UserName", DbType.StringFixedLength, UserName);
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
                db.AddInParameter(dbcmd, "Type", DbType.StringFixedLength, Type);
                ds=db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
    }
}

        

