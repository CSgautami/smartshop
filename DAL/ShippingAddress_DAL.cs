﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class ShippingAddress_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetShippingAddress(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetShippingAddressReport_sp");                
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
    }
}
