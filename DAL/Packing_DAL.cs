﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Packing_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetStockItem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("StockItemName_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string SavePackDetails(string Name, string xmlDet, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("SavePackDetails_sp");
                //db.AddInParameter(dbcmd, "PackID", DbType.StringFixedLength, PackID);
                db.AddInParameter(dbcmd, "PackName", DbType.StringFixedLength, Name);                
                db.AddInParameter(dbcmd, "xmlDet", DbType.StringFixedLength, xmlDet);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                res = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }

        public string UpdatePackDetails(string PackID, string Name, string xmlDet, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdatePackDetails_sp");
                db.AddInParameter(dbcmd, "PackID", DbType.StringFixedLength, PackID);
                db.AddInParameter(dbcmd, "PackName", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "xmlDet", DbType.StringFixedLength, xmlDet);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;

        }

        public DataSet GetPDNavData(string PID, string Flag)
        {
            DbCommand dbcmd = db.GetStoredProcCommand("GetPackDetailsData_sp");
            db.AddInParameter(dbcmd, "PID", DbType.StringFixedLength, PID);
            db.AddInParameter(dbcmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbcmd);
        }



        public string DeletePack(string PackID)
        {
            string res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeletePack_SP");
                db.AddInParameter(dbcmd, "PackID", DbType.StringFixedLength, PackID);
                res = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }
    }
}
