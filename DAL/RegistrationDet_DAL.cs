﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;


namespace DAL
{
    public class RegistrationDet_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetArea()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetArea_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetCity()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCityReg_sp");                
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetState()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStates_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetAreaWiseZone(string AreaID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetAreaWiseZone");
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, AreaID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet InsertRegistrationDet(string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string PhNo, string Mobile, string Email, string UserName, string Password, string BranchID,string ACode)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            DataSet ds = new DataSet();
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertRegistrationDetails_sp");
                db.AddInParameter(dbcmd, "RegisName", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "HNO", DbType.StringFixedLength, HNO);
                db.AddInParameter(dbcmd, "StreetName1", DbType.StringFixedLength,SOne);
                db.AddInParameter(dbcmd, "StreetName2", DbType.StringFixedLength,STwo);
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, Area);
                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, City);
                db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, State);
                db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength,Mark);
                db.AddInParameter(dbcmd, "PhoneNo", DbType.StringFixedLength,PhNo);
                db.AddInParameter(dbcmd, "Mobile", DbType.StringFixedLength,Mobile);
                db.AddInParameter(dbcmd, "Email", DbType.StringFixedLength, Email);                
                db.AddInParameter(dbcmd, "UserName", DbType.StringFixedLength,UserName);
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength,BranchID );
                db.AddInParameter(dbcmd, "ACode", DbType.StringFixedLength, ACode);
                ds = db.ExecuteDataSet(dbcmd); 
                dbTrans.Commit();

            }

            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                //rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return ds;

        }

        public string UpdateProfile(string UserID, string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string PhNo, string Mobile, string Email)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateUserProfile_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "RegisName", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "HNO", DbType.StringFixedLength, HNO);
                db.AddInParameter(dbcmd, "StreetName1", DbType.StringFixedLength, SOne);
                db.AddInParameter(dbcmd, "StreetName2", DbType.StringFixedLength, STwo);
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, Area);
                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, City);
                db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, State);
                db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, Mark);
                db.AddInParameter(dbcmd, "PhoneNo", DbType.StringFixedLength, PhNo);
                db.AddInParameter(dbcmd, "Mobile", DbType.StringFixedLength, Mobile);
                db.AddInParameter(dbcmd, "Email", DbType.StringFixedLength, Email);                
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }

            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;

        }

        public DataSet GetUserData(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetUserProfile_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength,UserID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string InsertAgentDet(string AgentID, string AgentCode,string Name,string Address,string ContactNo,string Email,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("SaveAgent_sp");
                db.AddInParameter(dbcmd, "AgentID", DbType.StringFixedLength, AgentID);
                db.AddInParameter(dbcmd, "AgentCode", DbType.StringFixedLength, AgentCode);
                db.AddInParameter(dbcmd, "Name", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "ContactNo", DbType.StringFixedLength, ContactNo);
                db.AddInParameter(dbcmd, "Email", DbType.StringFixedLength, Email);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtval = db.ExecuteScalar(dbcmd, dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;

        }

        public DataSet GetAgent()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetAgent_sp");                
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string DeleteAgent(string AgentID)
        {
            String rtval = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteAgent_sp");
                db.AddInParameter(dbcmd, "AgentID", DbType.StringFixedLength, AgentID);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtval;
        }

        public string GetAgentValid(string strAgent)
        {
            string strRet = "0";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("IsAgentValid_SP");
                db.AddInParameter(dbcmd, "ACode", DbType.StringFixedLength, strAgent);
                strRet = db.ExecuteDataSet(dbcmd).Tables[0].Rows[0][0].ToString();

            }
            catch (Exception ex)
            {
                strRet = "0";
            }
            return strRet;
        }
    }
}
