﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class VatMaster_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet GetVatMaster()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetVatMaster_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }
        public string InsertVatMaster(string VatDesc)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertVatMaster_sp");
                db.AddInParameter(dbcmd, "VatDesc", DbType.StringFixedLength, VatDesc);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();


            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }
        public string UpdateVatMaster(string VatID,string VatDesc)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateVatMaster_sp");
                db.AddInParameter(dbcmd, "VatID", DbType.StringFixedLength, VatID);
                db.AddInParameter(dbcmd, "VatDesc", DbType.StringFixedLength, VatDesc);
                rtnval = db.ExecuteScalar(dbcmd).ToString();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteVatMaster(string VatID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteVatMaster");
                db.AddInParameter(dbcmd, "VatID", DbType.StringFixedLength, VatID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }


    }
}
