﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DAL
{
    public class Sale_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetLedgerName(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetCustomer_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetLedgerDetails(string LedgerID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetLedgerDetails_SP");
            db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetAccLedger(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSaleAccLedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetStockItem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("StockItemName_sp");
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetTaxSystem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTaxSystem_sp");
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID,string TypeOfPrice)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetStock_SP");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "BatchNo", DbType.StringFixedLength, BatchNo);
            db.AddInParameter(dbCmd, "UDate", DbType.StringFixedLength, UDate);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetBatchNo(string ItemID, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBatchNo_SP");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            return db.ExecuteDataSet(dbCmd);
        }
        public string SaveSaleInvoice(string SInvID, string UInvID, string SInvNo, string Date, string DCNO, string DCDate, string LNo, string Address, string Mode, string TypeOfPrice, string Days, string Note, string PackID, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj,string RedemAmt, string NetAmt, string nTotMargin, string BranchID,string UserID, string XmlString)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("InsertSaleInvoice_SP");
                //db.AddInParameter(dbCmd, "SInvID", DbType.StringFixedLength, SInvID);
                //db.AddInParameter(dbCmd, "UInvID", DbType.StringFixedLength, UInvID);
                //db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, Status);
                db.AddInParameter(dbCmd, "SInvNo", DbType.StringFixedLength, SInvNo);
                db.AddInParameter(dbCmd, "InvDate", DbType.StringFixedLength, Date);
                db.AddInParameter(dbCmd, "DCNo", DbType.StringFixedLength, DCNO);
                db.AddInParameter(dbCmd, "DCDate", DbType.StringFixedLength, DCDate);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LNo);
                db.AddInParameter(dbCmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbCmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
                db.AddInParameter(dbCmd, "Days", DbType.StringFixedLength, Days);
                db.AddInParameter(dbCmd, "Note", DbType.StringFixedLength, Note);
                db.AddInParameter(dbCmd, "PackID", DbType.StringFixedLength, PackID);
                db.AddInParameter(dbCmd, "ACLedgerID", DbType.StringFixedLength, AcLNo);
                db.AddInParameter(dbCmd, "TotalAmount", DbType.StringFixedLength, TotAmt);
                db.AddInParameter(dbCmd, "TotalDisc", DbType.StringFixedLength, TotDisc);
                db.AddInParameter(dbCmd, "TotalTax", DbType.StringFixedLength, TotTax);
                db.AddInParameter(dbCmd, "NetDisc", DbType.StringFixedLength, NetDisc);
                db.AddInParameter(dbCmd, "Frieght", DbType.StringFixedLength, Frieght);
                db.AddInParameter(dbCmd, "LULCharges", DbType.StringFixedLength, LAndUL);
                db.AddInParameter(dbCmd, "Adjustment", DbType.StringFixedLength, Adj);
                db.AddInParameter(dbCmd, "RedemAmt", DbType.StringFixedLength, RedemAmt);
                db.AddInParameter(dbCmd, "TotMargin", DbType.StringFixedLength, nTotMargin);
                db.AddInParameter(dbCmd, "NetAmount", DbType.StringFixedLength, NetAmt);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );                
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, XmlString);
                res = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }
        public string UpdateSaleInvoice(string SInvID, string UInvID, string SInvNo, string Date, string DCNO, string DCDate, string LNo, string Address, string Mode, string TypeOfPrice, string Days, string Note, string PackID, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj,string RedemAmt, string NetAmt, string nTotMargin, string BranchID, string UserID, string XmlString)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("UpdateSaleInvoice_SP");
                db.AddInParameter(dbCmd, "SInvID", DbType.StringFixedLength, SInvID);
                db.AddInParameter(dbCmd, "UInvID", DbType.StringFixedLength, UInvID);                
                db.AddInParameter(dbCmd, "SInvNo", DbType.StringFixedLength, SInvNo);
                db.AddInParameter(dbCmd, "InvDate", DbType.StringFixedLength, Date);
                db.AddInParameter(dbCmd, "DCNo", DbType.StringFixedLength, DCNO);
                db.AddInParameter(dbCmd, "DCDate", DbType.StringFixedLength, DCDate);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LNo);
                db.AddInParameter(dbCmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbCmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
                db.AddInParameter(dbCmd, "Days", DbType.StringFixedLength, Days);
                db.AddInParameter(dbCmd, "Note", DbType.StringFixedLength, Note);
                db.AddInParameter(dbCmd, "PackID", DbType.StringFixedLength, PackID);
                db.AddInParameter(dbCmd, "ACLedgerID", DbType.StringFixedLength, AcLNo);
                db.AddInParameter(dbCmd, "TotalAmount", DbType.StringFixedLength, TotAmt);
                db.AddInParameter(dbCmd, "TotalDisc", DbType.StringFixedLength, TotDisc);
                db.AddInParameter(dbCmd, "TotalTax", DbType.StringFixedLength, TotTax);
                db.AddInParameter(dbCmd, "NetDisc", DbType.StringFixedLength, NetDisc);
                db.AddInParameter(dbCmd, "Frieght", DbType.StringFixedLength, Frieght);
                db.AddInParameter(dbCmd, "LULCharges", DbType.StringFixedLength, LAndUL);
                db.AddInParameter(dbCmd, "Adjustment", DbType.StringFixedLength, Adj);
                db.AddInParameter(dbCmd, "RedemAmt", DbType.StringFixedLength, RedemAmt);
                db.AddInParameter(dbCmd, "TotMargin", DbType.StringFixedLength, nTotMargin);
                db.AddInParameter(dbCmd, "NetAmount", DbType.StringFixedLength, NetAmt);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, XmlString);
                res = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();


            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }
        public DataSet GetTax(string tax)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTaxRate_sp");
            db.AddInParameter(dbCmd, "TaxNo", DbType.StringFixedLength, tax);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetData(string BranchID, string SInvID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSaleInvoiceData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "SInvID", DbType.StringFixedLength, SInvID);
            //db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, "P");
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }
        //public string GetCustomerName(string LedgerID)
        //{
        //    string res = "";
        //    DbCommand dbCmd = db.GetStoredProcCommand("GetCustomerName_sp");
        //    db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
        //    res = db.ExecuteDataSet(dbCmd).Tables[0].Rows[0]["LedgerName"].ToString();
        //    return res;
        //}


        public DataSet GetItemWiseTaxSystem(string ItemID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetItemWiseTaxSystem_sp");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, "S");
            return db.ExecuteDataSet(dbCmd);
        }

        public string DeleteSale(string InvID,string BranchID)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("DeleteSaleInvoice_SP");
                db.AddInParameter(dbCmd, "SInvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }
        public DataSet GetInvoiceBillData(string SInvID,string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetInvoiceBillData_sp");
            //db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "SInvID", DbType.StringFixedLength, SInvID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetPackData()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPackData_sp");            
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet AddPackDataInSales(string PackID, string TypeOfPrice)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("AddPackDataInSales_sp");
                db.AddInParameter(dbCmd, "PackID", DbType.StringFixedLength, PackID);
                db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }                        
            return ds;
        }

        public DataSet GetPinValue(string PinID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPinValue_sp");
                db.AddInParameter(dbCmd, "PinID", DbType.StringFixedLength, PinID);
               // db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
                ds= db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }
            return ds;
        }

        public DataSet CallFillLedger(string BranchID, string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillCustomer_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);            
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet CallFillItem(string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillItem_sp");            
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }
        //GetSearchSaleInvoiceData_sp
        public DataSet GetSearchInvoiceData(string BranchID, string SInvID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSearchSaleInvoiceData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "SInvNo", DbType.StringFixedLength, SInvID);            
            return db.ExecuteDataSet(dbCmd);
        }

        public string GetSMSMembers(string LNo)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetSMSMembers_sp");
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LNo);                
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }

        public DataSet FillUserLedger()
        {            
            DbCommand dbCmd = db.GetStoredProcCommand("FillUserLedger_sp");
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetItemWithBarCode(string BarCode)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetItemWithBarCode_sp");
            db.AddInParameter(dbCmd, "BarCode", DbType.StringFixedLength, BarCode);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetPriceAndMRP(string ItemID,string TypeOfPrice)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPrice_sp");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);            
            db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
            return db.ExecuteDataSet(dbCmd);
        }

        public object GetBranch(string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBranch_SP");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetEffPrice(string ItemID, string BatchNo, string TypeOfPrice)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetItemEffPirce_sp");
                db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbCmd, "BatchNo", DbType.StringFixedLength, BatchNo);
                db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, TypeOfPrice);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }
            return ds;
        }
    }
}
