﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Area_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet GetZone()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetZone_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch(Exception ex)
            {
            }
            return ds;
        }

        public string InsertArea(string AreaCode, string AreaCentre, string Address, string ZoneID, string ContactNo,string BranchID,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertArea_sp");
                //db.AddInParameter(dbcmd, "ZoneNo", DbType.StringFixedLength, ZoneNo);
                db.AddInParameter(dbcmd, "AreaCode", DbType.StringFixedLength, AreaCode);
                db.AddInParameter(dbcmd, "AreaCentre", DbType.StringFixedLength, AreaCentre);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbcmd, "ContactNo", DbType.StringFixedLength, ContactNo);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }

            return rtnval;
        }

        public DataSet GetArea(string strSearch)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetArea_sp");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, strSearch);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }


        public string UpdateArea(string AreaID, string AreaCode, string AreaCentre, string Address, string ZoneID, string ContactNo,string BranchID,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateArea_sp");
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, AreaID);
                db.AddInParameter(dbcmd, "AreaCode", DbType.StringFixedLength, AreaCode);
                db.AddInParameter(dbcmd, "AreaCentre", DbType.StringFixedLength, AreaCentre);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbcmd, "ContactNo", DbType.StringFixedLength, ContactNo);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = db.ExecuteScalar(dbcmd).ToString();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteArea(string AreaID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteArea_sp");
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, AreaID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }

        public object GetBranch()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranch_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, "0");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch (Exception ex)
            {
            }
            return ds;
        }
    }
}
 
        
    
