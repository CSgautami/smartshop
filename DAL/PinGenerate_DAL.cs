﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DAL
{
    public class PinGenerate_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        public string SavePinGeneration(string Amount, string Value, string ZeroValue, string NoofPins, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SavePinGeneration_sp");
                db.AddInParameter(dbCmd, "Total", DbType.StringFixedLength, Amount);
                db.AddInParameter(dbCmd, "Value", DbType.StringFixedLength, Value);
                db.AddInParameter(dbCmd, "NoOfPins", DbType.StringFixedLength, NoofPins);
                db.AddInParameter(dbCmd, "ZeroValue", DbType.StringFixedLength, ZeroValue);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }

        public DataSet GetCustomer()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCustomers_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetPins()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetAvailablePins_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string SavePins(string LedgerID, string xmlDet,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SavePinsIssue_sp");
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                db.AddInParameter(dbCmd, "xmlPinDet", DbType.StringFixedLength, xmlDet);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();


            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }
    }
}

        

        
    

