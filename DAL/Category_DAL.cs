﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Category_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetData()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCategory_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }

        public string SaveCategory(string HeadGroupID,string CategoryID, string Category)
        {
            string res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("InsertOrUpdateCategory_sp");
                db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroupID);
                db.AddInParameter(dbcmd, "CategoryID", DbType.StringFixedLength, CategoryID);
                db.AddInParameter(dbcmd, "Category", DbType.StringFixedLength, Category);
                res = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            { throw; }
            return res;
        }

        public string DeleteCategory(string CategoryID)
        {
            string res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteCategory_sp");
                db.AddInParameter(dbcmd, "CategoryID", DbType.StringFixedLength, CategoryID);
                res = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }
    }
}
