﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Sale_BLL
    {
        Sale_DAL obj_DAL = new Sale_DAL();
        public DataSet GetLedgerName(string BranchID)
        {
            return obj_DAL.GetLedgerName(BranchID );
        }
        public DataSet GetLedgerDetails(string LedgerID)
        {
            return obj_DAL.GetLedgerDetails(LedgerID);
        }

        public DataSet GetAccLedger(string BranchID)
        {
            return obj_DAL.GetAccLedger(BranchID );
        }

        public DataSet GetStockItem()
        {
            return obj_DAL.GetStockItem();
        }

        public DataSet GetTaxSystem()
        {
            return obj_DAL.GetTaxSystem();
        }
        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID,string TypeOfPrice)
        {
            return obj_DAL.GetStock(ItemID, BatchNo, UDate, BranchID,TypeOfPrice );
        }
        public DataSet GetBatchNo(string ItemID, string BranchID)
        {
            return obj_DAL.GetBatchNo(ItemID, BranchID);
        }
        public string SaveSaleInvoice(string SInvID, string UInvID, string SInvNo, string Date, string DCNO, string DCDate, string LNo, string Address, string Mode, string TypeOfPrice,string Days, string Note, string PackID, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj,string RedemAmt, string NetAmt, string nTotMargin, string BranchID, string UserID, string XmlString)
        {
            return obj_DAL.SaveSaleInvoice(SInvID, UInvID, SInvNo, Date, DCNO, DCDate, LNo, Address, Mode, TypeOfPrice, Days, Note, PackID, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj,RedemAmt, NetAmt, nTotMargin, BranchID, UserID, XmlString);
        }
        public string UpdateSaleInvoice(string SInvID, string UInvID, string SInvNo, string Date, string DCNO, string DCDate, string LNo, string Address, string Mode, string TypeOfPrice, string Days, string Note, string PackID, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj,string RedemAmt, string NetAmt, string nTotMargin, string BranchID, string UserID, string XmlString)
        {
            return obj_DAL.UpdateSaleInvoice(SInvID, UInvID, SInvNo, Date, DCNO, DCDate, LNo, Address, Mode, TypeOfPrice, Days, Note, PackID, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj,RedemAmt, NetAmt, nTotMargin, BranchID, UserID, XmlString);
        }
        public DataSet GetTax(string tax)
        {
            return obj_DAL.GetTax(tax);
        }
        public DataSet GetData(string BranchID, string InvID, string Flag)
        {
            return obj_DAL.GetData(BranchID , InvID, Flag);
        }
        public DataSet GetSearchInvoiceData(string BranchID, string InvID)
        {
            return obj_DAL.GetSearchInvoiceData(BranchID, InvID);
        }
        //public string GetCustomerName(string LedgerID)
        //{
        //    return obj_DAL.GetCustomerName(LedgerID);
        //}

        public DataSet GetItemWiseTaxSystem(string ItemID)
        {
            return obj_DAL.GetItemWiseTaxSystem(ItemID);
        }
        public DataSet GetInvoiceBillData(string SInvID,string BranchID)
        {
            return obj_DAL.GetInvoiceBillData(SInvID,BranchID);
        }

        public string DeleteSale(string InvID,string BranchID)
        {
            return obj_DAL.DeleteSale(InvID,BranchID);
        }
        public DataSet GetPackData()
        {
            return obj_DAL.GetPackData();
        }
        public DataSet AddPackDataInSales(string PackID, string TypeOfPrice)
        {
            return obj_DAL.AddPackDataInSales(PackID, TypeOfPrice);
        }

        public DataSet GetPinValue(string PinID)
        {
            return obj_DAL.GetPinValue(PinID);
        }

        public DataSet CallFillLedger(string BranchID, string prefix)
        {
            return obj_DAL.CallFillLedger(BranchID, prefix);
        }

        public DataSet CallFillItem(string prefix)
        {
            return obj_DAL.CallFillItem(prefix);
        }

        public string GetSMSMembers(string LNo)
        {
            return obj_DAL.GetSMSMembers(LNo);
        }

        public DataSet FillUserLedger()
        {
            return obj_DAL.FillUserLedger();
        }

        public DataSet GetItemWithBarCode(string BarCode)
        {
            return obj_DAL.GetItemWithBarCode(BarCode);
        }

        public DataSet GetPriceAndMRP(string ItemID,string TypeOfPrice)
        {
            return obj_DAL.GetPriceAndMRP(ItemID,TypeOfPrice);
        }



        public object GetBranch(string UserID)
        {
            throw new NotImplementedException();
        }

        public DataSet GetEffPrice(string ItemID, string BatchNo, string TypeOfPrice)
        {
            return obj_DAL.GetEffPrice(ItemID,BatchNo,TypeOfPrice);
        }
    }
}
