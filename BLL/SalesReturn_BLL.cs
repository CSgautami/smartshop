﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;
namespace BLL
{
    public class SalesReturn_BLL
    {
        SalesReturn_DAL obj_DAL = new SalesReturn_DAL();
        public DataSet GetLedgerName(string BranchID)
        {
            return obj_DAL.GetLedgerName(BranchID);
        }

        public DataSet GetAccLedger(string BranchID)
        {
            return obj_DAL.GetAccLedger(BranchID);
        }

        public DataSet GetBatchNo(string ItemID, string BranchID)
        {
            return obj_DAL.GetBatchNo(ItemID, BranchID);
        }

        public DataSet GetLedgerDetails(string LedgerID)
        {
            return obj_DAL.GetLedgerDetails(LedgerID);
        }

        public DataSet GetStockItem()
        {
            return obj_DAL.GetStockItem();
        }

        public DataSet GetTaxSystem()
        {
            return obj_DAL.GetTaxSystem();
        }
        public string SaveInvoice(string SRInvID, string UInvID, string SRInvNo, string Date, string SRetInvNo, string SRDate, string LNo, string Address, string Mode, string Days, string Note, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,string UserID, string XmlString)
        {
            return obj_DAL.SaveInvoice(SRInvID, UInvID, SRInvNo, Date, SRetInvNo , SRDate, LNo, Address, Mode, Days, Note, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID ,UserID, XmlString);
        }
        public DataSet GetTax(string tax)
        {
            return obj_DAL.GetTax(tax);
        }
        public DataSet GetData(string BranchID, string SRInvID, string Flag)
        {
            return obj_DAL.GetData(BranchID , SRInvID, Flag);
        }

        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID, string LedgerID)
        {
            return obj_DAL.GetStock(ItemID, BatchNo, UDate, BranchID , LedgerID);
        }

        public string DeleteSaleReturn(string InvID,string BranchID)
        {
            return obj_DAL.DeleteSaleReturn(InvID,BranchID);
        }
        public DataSet CallFillLedger(string BranchID, string prefix)
        {
            return obj_DAL.CallFillLedger(BranchID, prefix);
        }

        public DataSet CallFillItem(string prefix)
        {
            return obj_DAL.CallFillItem(prefix);
        }
        public DataSet GetSearchInvoiceData(string BranchID, string InvID)
        {
            return obj_DAL.GetSearchInvoiceData(BranchID, InvID);
        }

        public DataSet GetInvoiceBillData(string strInvID, string BranchID)
        {
            return obj_DAL.GetInvoiceBillData(strInvID, BranchID);
        }
    }
}
