﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BLL
{
    public class GroupsFD_BLL
    {
        GroupsFD_DAL obj_DAL = new GroupsFD_DAL();

        public DataSet GetStcokItem()
        {
            return obj_DAL.GetStockItem();
        }

        public DataSet GetData()
        {
            return obj_DAL.GetData();
        }

        public string SaveData(int nID, string Group1, string Group2, string Group3, string Group4, string Group5, string Group6,
               string Group7, string Group8, string ItemID1, string ItemID2, string ItemID3, string ItemID4, string ItemID5, 
               string ItemID6, string ItemID7, string ItemID8, string Desc1, string Desc2, string Desc3, string Desc4, 
               string Desc5, string Desc6, string Desc7, string Desc8, string UserID)
        {
            return obj_DAL.SaveData(nID, Group1, Group2, Group3, Group4, Group5, Group6, Group7, Group8, ItemID1, ItemID2, ItemID3, ItemID4, ItemID5, ItemID6, ItemID7, ItemID8, Desc1, Desc2, Desc3, Desc4, Desc5, Desc6, Desc7, Desc8, UserID);

        }

        public string DeleteData(int nID)
        {
            return obj_DAL.DeleteData(nID);
        }
    }
}
