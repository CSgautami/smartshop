﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Receipt_BLL
    {
        Receipts_DAL dal_Receipts = new Receipts_DAL();

        public DataSet GetReceiptsReport(string BranchID, string DateTo, string DateFrom)
        {
            return dal_Receipts.GetReceiptReport(BranchID, DateFrom, DateTo);
        }

    }
}
