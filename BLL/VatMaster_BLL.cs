﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class VatMaster_BLL
    {
        VatMaster_DAL Obj_DAL = new VatMaster_DAL();
        public DataSet GetVatMaster()
        {
            return Obj_DAL.GetVatMaster();
        }
        public string InsertVatMaster(string VatDesc)
        {
            return Obj_DAL.InsertVatMaster(VatDesc);
        }
        public string UpdateVatMaster(string VatID, string VatDesc)
        {
            return Obj_DAL.UpdateVatMaster(VatID, VatDesc);
        }
        public string DeleteVatMaster(string VatID)
        {
            return Obj_DAL.DeleteVatMaster(VatID);
        }
    }
}
