﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL.Reports;

namespace BLL.Reports
{
    public class SaleReport_BLL
    {
        SaleReport_DAL obj_Dal = new SaleReport_DAL();
        public DataSet SaleReportBetweenDates(string FromDate, string ToDate, string Mode, string Name, string BranchID,string UserID)
        {
            return obj_Dal.SaleReportBetweenDates(FromDate, ToDate, Mode, Name, BranchID,UserID);
        }

        public DataSet GetStockGroup()
        {
            return obj_Dal.GetStockGroup();
        }

        public DataSet GetCompany()
        {
            return obj_Dal.GetCompany();
        }

        public DataSet GetSaleReportGroupAndComWise(string FromDate, string ToDate, string Group, string Company, string BranchID, string UserID)
        {
            return obj_Dal.GetSaleReportGroupAndComWise(FromDate, ToDate, Group, Company, BranchID, UserID);
        }

        public DataSet GetSaleReportInvWise(string FromDate, string ToDate, string InvFrom, string InvTo, string BranchID, string UserID)
        {
            return obj_Dal.GetSaleReportInvWise(FromDate, ToDate, InvFrom , InvTo , BranchID, UserID);
        }

        public DataSet GetSaleReportProductWise(string FromDate, string ToDate, string Product, string BranchID, string UserID)
        {
            return obj_Dal.GetSaleReportProductWise(FromDate, ToDate, Product,BranchID, UserID) ;
        }

        public DataSet  GetStockItem()
        {
            return obj_Dal.GetStockItem();
        }

        public DataSet  GetHeadGroup()
        {
            return obj_Dal.GetHeadGroup();
        }
    }
}
