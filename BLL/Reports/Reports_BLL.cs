﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL.Reports;
namespace BLL.Reports
{
    public class Reports_BLL
    {
        Reports_DAL obj_DAL = new Reports_DAL();
        public DataSet StockIndentReport(string GroupID,string Company)
        {
            return obj_DAL.StockIndentReportData(GroupID,Company);
        }
        public DataSet ClosingStock(string ToDate,  string GID, string BranchID, string UserID)
        {
            return obj_DAL.ClosingStock(ToDate, GID, BranchID, UserID);
        }

        public DataSet GetStockGroup()
        {
            return obj_DAL.GetStockGroup();
        }

        public DataSet GetCompany()
        {
            return obj_DAL.GetCompany();
        }

        public DataSet StockMovementData(string FromDate, string ToDate, string Group, string Company, string All, string BranchID,string UserID)
        {
            return  obj_DAL.StockMovementData(FromDate, ToDate,Group, Company, All, BranchID,UserID);
        }

        public DataSet GetStockItem(string GID)
        {
            return obj_DAL.GetStockItem(GID);
        }

        public DataSet StockLedgerData(string FromDate, string ToDate,  string Group, string Item, string Batch, string Branch,string UserID)
        {
            DataSet ds = obj_DAL.StockLedgerData(FromDate, ToDate, Group, Item, Batch, Branch,UserID);
            int count = 0;
            decimal OBal = 0;
            int Sno = 0;            
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (count == 0)
                {
                    if (dr["BalQuantity"].ToString() == "")
                        dr["BalQuantity"] = 0;
                    OBal = decimal.Parse(dr["BalQuantity"].ToString());
                    dr["Slno"] = DBNull.Value;
                }
                else
                {
                    if (dr["LedgerName"].ToString() != "Total")
                    {
                        OBal = OBal + decimal.Parse(dr["InQuantity"].ToString()) - decimal.Parse(dr["OutQuantity"].ToString());
                        dr["BalQuantity"] = OBal;
                        dr["Slno"] = Sno;
                    }
                    else
                    {
                        dr["Slno"] = DBNull.Value;
                        //OBal = OBal + decimal.Parse(dr["InQuantity"].ToString()) - decimal.Parse(dr["OutQuantity"].ToString());
                    }   
                    
                }
                count++;
                Sno++;
            }
            return ds;
        }
        public DataSet GetArea()
        {
            return obj_DAL.GetArea();
        }
        public DataSet GetZone()
        {
            return obj_DAL.GetZone();
        }
        public DataSet GetAreaCustomerList(string AreaID)
        {
            return obj_DAL.GetAreaCustomerList(AreaID);
        }
        public DataSet GetZoneCustomerList(string ZoneID)
        {
            return obj_DAL.GetZoneCustomerList(ZoneID);
        }
        public DataSet GetCustomerList(string BranchID, string UserID)
        {
            return obj_DAL.GetCustomerList(BranchID, UserID);
        }
        public DataSet GetAreaCommission(string AreaID, string dtFrom, string dtTo)
        {
            return obj_DAL.GetAreaCommission(AreaID, dtFrom, dtTo);
        }
        public DataSet GetRegisteredMembersList(string AreaID, string dtFrom, string dtTo)
        {
            return obj_DAL.GetRegisteredMembersList(AreaID, dtFrom, dtTo);
        }
        public DataSet GetZoneCommission(string ZoneID, string dtFrom, string dtTo)
        {
            return obj_DAL.GetZoneCommission(ZoneID, dtFrom, dtTo);
        }
        public DataSet GetMember()
        {
            return obj_DAL.GetMember();
        }
        public DataSet GetMyTeam(string MemberID, string bCustomer)
        {
            return obj_DAL.GetMyTeam(MemberID, bCustomer);
        }

        public DataSet TransfersReportData(string FromDate, string ToDate, string BranchID)
        {
            return obj_DAL.TransfersReportData(FromDate, ToDate, BranchID);
        }

        public DataSet PendingTransfersReportData(string FromDate, string ToDate, string ToBranchID)
        {
            return obj_DAL.PendingTransfersReportData(FromDate, ToDate, ToBranchID);
        }
        public DataSet GetAreaAc(string AreaID, string FromDate, string ToDate, string chkPay)
        {
            return obj_DAL.GetAreaAc(AreaID, FromDate, ToDate, chkPay);
        }
        public DataSet GetZoneAc(string ZoneID, string FromDate, string ToDate, string chkPay)
        {
            return obj_DAL.GetZoneAc(ZoneID, FromDate, ToDate, chkPay);
        }
        public DataSet GetMyAccount(string LedgerID, string FromDate, string ToDate, string chkPay, string bCustomer)
        {
            return obj_DAL.GetMyAccount(LedgerID, FromDate, ToDate, chkPay, bCustomer);
        }
        public DataSet GetMembersMast(string LedgerID, string chkPay, string bCustomer)
        {
            return obj_DAL.GetMembersMast(LedgerID, chkPay, bCustomer);
        }
        public string SavePayData(string FromDate, string ToDate, string PayDate, string UserID, string BranchID, string xmlCustomerPay)
        {
            return obj_DAL.SavePayData(FromDate,ToDate,PayDate,UserID,BranchID,xmlCustomerPay);
        }
        public DataSet GetWeeklyComm(string YourID, string FromDate, string ToDate)
        {
            return obj_DAL.GetWeeklyComm(YourID, FromDate, ToDate);
        }

        public DataSet GetMyPins(string YourID)
        {
            return obj_DAL.GetMyPins(YourID);
        }

        public DataSet GetPinIssue(string LedgerID)
        {
            return obj_DAL.GetPinIssue(LedgerID);
        }
        public DataSet CallFillItem(string prefix)
        {
            return obj_DAL.CallFillItem(prefix);
        }

        public DataSet GetItemName(string ItemID)
        {
            return obj_DAL.GetItemName(ItemID);
        }

        public DataSet  CallFillMember(string prefix)
        {
            return obj_DAL.CallFillMember(prefix);
        }
        public DataSet GetWeeklyCommdet(string UserID, string Date)
        {
            return obj_DAL.GetWeeklyCommdet(UserID, Date);
        }
        public DataSet InActiveMembers(string UserID)
        {
            return obj_DAL.InActiveMembers(UserID);
        }
        public DataSet GetIncentivesReport(string YourID)
        {
            return obj_DAL.GetIncentivesReport(YourID);
        }
        public DataSet GetIncentivesReportdet(string YourID, string FDate, string TDate)
        {
            return obj_DAL.GetIncentivesReportdet(YourID, FDate, TDate); 
        }

        public DataSet ReportFinalAccount(string chkDateWise, string Date, string To, string rDetailed, string rTradingAccount, string rProfitandLossAc, string rBalenceSheet)
        {
            return obj_DAL.ReportFinalAccount(chkDateWise, Date, To, rDetailed, rTradingAccount, rProfitandLossAc, rBalenceSheet);
        }

        public DataSet GetUserPriceListReport(string GroupID, string Search, string StockItemsOnly)
        {
            return obj_DAL.GetUserPriceListReport(GroupID, Search,StockItemsOnly);
        }

        public DataSet GetGroups()
        {
            return obj_DAL.GetGroups();
        }

        public DataSet GetTrailBalence(string chkSerialByName, string Branch, string chkGroups, string chkOpenings, string rDebtors, string chkLSTGroups, string chkDateWise, string Date, string chkShowDebtors, string UserID)
        {
            return obj_DAL.GetTrailBalence(chkSerialByName, Branch, chkGroups, chkOpenings, rDebtors, chkLSTGroups, chkDateWise, Date, chkShowDebtors, UserID);
        }

        public DataSet TransfersInReportData(string FromDate, string ToDate, string BranchID)
        {
            return obj_DAL.TransfersInReportData(FromDate,ToDate,BranchID);
        }

        public DataSet GetPaySMSMembers(string FromDate, string ToDate, string PayDate)
        {
            return obj_DAL.GetPaySMSMembers(FromDate, ToDate, PayDate);
        }

        public DataSet TotalSalesReportData(string FromDate, string ToDate, string FromInv, string ToInv, string BranchID, string UserID)
        {
            return obj_DAL.TotalSalesReportData(FromDate,ToDate,FromInv,ToInv,BranchID,UserID);
        }

        public DataSet DetailSalesReportData(string FromDate, string ToDate, string FromInv, string ToInv, string BranchID, string UserID)
        {
            return obj_DAL.DetailSalesReportData(FromDate,ToDate,FromInv,ToInv,BranchID,UserID);
        }

        public DataSet GetBranchList()
        {
            return obj_DAL.GetBranchList();
        }

        public DataSet PriceWiseSalesReportData(string FromDate, string ToDate, string BranchID, string Prices, string UserID)
        {
            return obj_DAL.PriceWiseSalesReportData(FromDate,ToDate,BranchID,Prices,UserID);
        }

        //public DataSet TotalPurchaseReportData(string FromDate, string ToDate, string BranchID, string UserID)
        //{
        //    return obj_DAL.TotalPurchaseReportData(FromDate,ToDate,BranchID,UserID); 
        //}

        public DataSet GetTotalPurchaseReportData(string BranchID, string FromDate, string ToDate, string UserID)
        {
            return obj_DAL.GetTotalPurchaseReportData(BranchID,FromDate,ToDate,UserID);
        }

        public DataSet DetailPurchaseReportData(string BranchID, string FromDate, string ToDate, string UserID)
        {
            return obj_DAL.DetailPurchaseReportData(BranchID,FromDate,ToDate,UserID);
        }
        public DataSet GetAgentNames()
        {
            return obj_DAL.GetAgentNames();
        }
        public DataSet GetAgentWiseCustomer(string AgentID)
        {
            return obj_DAL.GetAgentWiseCustomer(AgentID);
        }
        public DataSet GetAgentAccntReport(string AgentID, string CustID, string FromDate, string ToDate)
        {
            return obj_DAL.GetAgentAccntReport(AgentID, CustID, FromDate, ToDate);
        }
        public DataSet GetAgentMembersReport(string AgentID, string FromDate, string ToDate)
        {
            return obj_DAL.GetAgentMembersReport(AgentID, FromDate, ToDate);
        }
        public DataSet GetZoneNames()
        {
            return obj_DAL.GetZoneNames();
        }
        public DataSet GetZoneWiseCustomer(string ZoneID)
        {
            return obj_DAL.GetZoneWiseCustomer(ZoneID);
        }
        public DataSet GetZoneAccountReport(string ZoneID, string CustID, string FromDate, string ToDate)
        {
            return obj_DAL.GetZoneAccountReport(ZoneID, CustID, FromDate, ToDate);
        }
        public DataSet GetZoneMembersReport(string ZoneID, string FromDate, string ToDate)
        {
            return obj_DAL.GetZoneMembersReport(ZoneID, FromDate, ToDate);
        }

    }
}
