﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL.Reports;

namespace BLL.Reports
{
    public class PurchaseReturnReport_BLL
    {
        PurchaseReturnReport_DAL Obj_DAL = new PurchaseReturnReport_DAL();
        public DataSet PurchasReturnReportData(string LedgerID, string FromDate, string ToDate, string Mode, string BranchID, string UserID)
        {
            return Obj_DAL.PurchasReturnReportData(LedgerID, FromDate, ToDate, Mode, BranchID, UserID);
        }
    }
}
