﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Category_BLL
    {
        Category_DAL Obj_DAL = new Category_DAL();
        public DataSet GetData()
        {
            return Obj_DAL.GetData();
        }

        public string SaveCategory(string HeadGroupID,string CategoryID, string Category)
        {
            return Obj_DAL.SaveCategory(HeadGroupID,CategoryID,Category);
        }

        public string DeleteCategory(string CategoryID)
        {
            return Obj_DAL.DeleteCategory(CategoryID);
        }
    }
}
