﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class ChangePwd_BLL
    {
        ChangePwd_DAL Obj_DAL = new ChangePwd_DAL();
        public string ChangePassword(string OldPassword, string Password,string UserID,string Type)
        {
            return Obj_DAL.ChangePassword(OldPassword,Password,UserID,Type);
        }
    }
}
