﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class UserRegistration_BLL
    {
        UserRegistration_DAL dal_UserReg = new UserRegistration_DAL();
        public string RegisterUser(string UserName, string Password, string Address, string CityID, string OtherCity, string StateID, string CountryID, string EmailID, string PhoneNos, string MobileNos)
        {
            return dal_UserReg.RegisterUser(UserName, Password, Address, CityID, OtherCity, StateID, CountryID, EmailID, PhoneNos, MobileNos);
        }

        public string CheckAuthentication(string UserName, string Password)
        {
            return dal_UserReg.CheckAuthentication(UserName, Password);
        }
    }
}
