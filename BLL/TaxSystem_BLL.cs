﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using DAL; 

namespace BLL
{
   public  class TaxSystem_BLL
    {
       TaxSystem_DAL obj_DAL = new TaxSystem_DAL();      
       public DataSet GetVatMaster()
        {
            return obj_DAL.GetVatMaster();
        }
       public DataSet GetVatTypeMaster()
       {
           return obj_DAL.GetVatTypeMaster();
       }
       public string InsertTaxSystem(string TaxName, string ExDuty, string CST, string Other, string SAT, string Rate, string VAT, string Type,string UserID)
       {
           return obj_DAL.InsertTaxSystem(TaxName,ExDuty,CST,Other,SAT,Rate,VAT,Type,UserID);
       }
       public DataSet GetTaxSystem()
       {
           return obj_DAL.GetTaxSystem();
       }
       public string UpdateTaxSystem(string TaxNo, string TaxName, string ExDuty, string CST, string Other, string SAT, string Rate, string VAT, string Type, string UserID)
       {
           return obj_DAL.UpdateTaxSystem(TaxNo, TaxName, ExDuty, CST, Other, SAT, Rate, VAT, Type, UserID);
       }
       public string DeleteTaxSystem(string TaxNo)
       {
           return obj_DAL.DeleteTaxSystem(TaxNo);
       }
    }
}
