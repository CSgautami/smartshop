﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;


namespace BLL
{
    public class Branch_BLL
    {
        Branch_DAL Obj_DAL = new Branch_DAL();
        public string InsertBranch(string BranchName, string BranchAddress, string ContactPerson, string ContactNo, string EmailID,string BranchType,string Circle,string Division,string Designation,string UserID)
        {
            return Obj_DAL.InsertBranch(BranchName, BranchAddress, ContactPerson, ContactNo, EmailID,BranchType,Circle,Division,Designation,UserID);
        }
        public DataSet GetBranch()
        {
            return Obj_DAL.GetBranch();
        }
        public string UpdateBranch(string BranchID, string BranchName, string BranchAddress, string ContactPerson, string ContactNo, string EmailID, string BranchType, string Circle, string Division, string Designation, string UserID)
        {
            return Obj_DAL.UpdateBranch(BranchID,BranchName, BranchAddress, ContactPerson, ContactNo, EmailID,BranchType,Circle,Division,Designation,UserID);
        }
        public string DeleteBranch(string BranchID)
        {
            return Obj_DAL.DeleteBranch(BranchID);
        }
    }
}
