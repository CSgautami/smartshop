﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Company_BLL
    {
        Company_DAL Obj_DAL = new Company_DAL();
        public DataSet GetTaxSysyem()
        {
            return Obj_DAL.GetTaxSystem();
        }
        public string InsertCompany(string CompanyName,string Address,string TaxNo,string UserID)
        {
            return Obj_DAL.InsertCompany(CompanyName,Address,TaxNo,UserID);
        }
        public DataSet GetCompany(string Search)
        {
            return Obj_DAL.GetCompany(Search);
        }
        public string UpdateCompany(string CompanyID,string CompanyName,string Address,string TaxNo,string UserID)
        {
            return Obj_DAL.UpdateCompany(CompanyID,CompanyName,Address,TaxNo,UserID);
        }
        public string DeleteCompany(string CompanyID)
        {
            return Obj_DAL.DeleteCompany(CompanyID);
        }
          
    }
}
