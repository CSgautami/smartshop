﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class VatType_BLL
    {
        VatType_DAL Obj_DAL = new VatType_DAL();
        public string InsertVatTypeDesc(string VatTypeDesc)
        {
            return Obj_DAL.InsertVatTypeDesc(VatTypeDesc);
        }
        public DataSet GetVatTypeDesc()
        {
            return Obj_DAL.GetVatTypeDesc();
        }
        public string UpdateVatTypeDesc(string VatTypeID, string VatTypeDesc)
        {
            return Obj_DAL.UpdateVatTypeDesc(VatTypeID, VatTypeDesc);
        }
        public string DeleteVatTypeDesc(string VatTypeID)
        {
            return Obj_DAL.DeleteVatTypeDesc(VatTypeID);
        }


    }
}
