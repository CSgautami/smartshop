﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BLL
{
    public class ShopingCart_BLL
    {
        ShopingCart_DAL obj_DAL = new ShopingCart_DAL();

        public DataSet SaveTempOrder(string ItemID, string Qty, string BathNo, string TempID, string UserID)
        {
            return obj_DAL.SaveTempOrder(ItemID, Qty, BathNo, TempID, UserID);
        }

        public DataTable GetProductDetails(string TempOrderID)
        {
            DataSet ds = new DataSet();
            ds=obj_DAL.GetProductDetails(TempOrderID);
            return ds.Tables[0];
        }

        public string DeleteTempOrder(int TODID)
        {
            return obj_DAL.DeleteTempOrder(TODID);
        }

        public string UpdateTempOrder(int TODID)
        {
            return obj_DAL.UpdateTempOrder(TODID);
        }

        public string UpdateTempOrderQtyChange(string TempOrderID, string ItemID, string Qty,string Price)
        {
            return obj_DAL.UpdateTempOrderQtyChange(TempOrderID,ItemID,Qty,Price);
        }

        public DataSet GetAddress(string UserID)
        {
            return obj_DAL.GetAddress(UserID);
        }

        public string SaveOrder(string TempOrderID,string Amount,string DCharges,string Address,string Transport,string ModeOfPayment,string PaymentStatus)
        {
            return obj_DAL.SaveOrder(TempOrderID,Amount,DCharges,Address,Transport,ModeOfPayment,PaymentStatus);
        }

        public DataSet GetPrintData(string OrderID,string UserID)
        {
            return obj_DAL.GetPrintData(OrderID,UserID);
        }

        public DataSet GetOrderHistory(string UserID)
        {
            return obj_DAL.GetOrderHistory(UserID);
        }        
        public DataSet GetOrderReport(string FromDate,string ToDate,string Status,string BranchID,string UserID)
        {
            return obj_DAL.GetOrderReport(FromDate,ToDate,Status,BranchID,UserID);
        }

        public DataSet GetOrderSummury(string OrderID)
        {
            return obj_DAL.GetOrderSummury(OrderID);
        }

        public DataSet GetStockAvailability(int ItemID)
        {
            return obj_DAL.GetStockAvailability(ItemID);
        }


        public DataSet GetData(string TempID)
        {
            return obj_DAL.GetData(TempID);
        }

        public DataSet DeleteTempOrder(string ItemID,string TOID)
        {
            return obj_DAL.DeleteTemp(ItemID,TOID);
        }

        public DataSet GetOrderShipmentDet(string OrderID)
        {
            return obj_DAL.GetOrderShipmentDet(OrderID);
        }

        public string SaveOrderShipmentDetails(string OrderID, string ShipmentDet, string ShipmentDetID, string Transport, string DisDate, string TrackNo, string UserID)
        {
            return obj_DAL.SaveOrderShipmentDetails(OrderID,ShipmentDet,ShipmentDetID,Transport,DisDate,TrackNo ,UserID);
        }

        public string SaveInProcess(string OrderIDs)
        {
            return obj_DAL.SaveInProcess(OrderIDs);
        }

        public string CancelClick(string OrderID)
        {
            return obj_DAL.CancelClick(OrderID);
        }
    }
}
