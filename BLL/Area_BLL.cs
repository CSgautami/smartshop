﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Area_BLL
    {
        Area_DAL Obj_DAL = new Area_DAL();
        public DataSet GetZone()
        {
            return Obj_DAL.GetZone();
        }
        public string InsertArea(string AreaCode, string AreaCentre, string Address,string ZoneID,string ContactNo,string BranchID,string UserID)
        {
            return Obj_DAL.InsertArea(AreaCode,AreaCentre, Address,ZoneID,ContactNo,BranchID,UserID);
        }
        public DataSet GetArea(string strSearch)
        {
            return Obj_DAL.GetArea(strSearch);
        }
        public string UpdateArea(string AreaID, string AreaCode, string AreaCentre, string Address, string ZoneID, string ContactNo,string BranchID,string UserID)
        {
            return Obj_DAL.UpdateArea(AreaID, AreaCode, AreaCentre, Address, ZoneID, ContactNo,BranchID,UserID);
        }
        public string DeleteArea(string AreaID)
        {
            return Obj_DAL.DeleteArea(AreaID);
        }

        public object GetBranch()
        {
            return Obj_DAL.GetBranch();
        }
    }
}
