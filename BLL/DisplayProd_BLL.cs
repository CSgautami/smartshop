﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class DisplayProd_BLL
    {
        DisplayProd_DAL Obj_DAL = new DisplayProd_DAL();
        public string InsertDisplayProd(string Product, string Price)
        {
            return Obj_DAL.InsertDisplayProd(Product, Price);
        }
        public DataSet GetDisplayProd()
        {
            return Obj_DAL.GetDisplayProd();
        }
        public string UpdateDisplayProd(string ID, string Product, string Price)
        {
            return Obj_DAL.UpdateDisplayProd(ID, Product, Price);
        }

        public string DeleteDisplayProd(string ID)
        {
            return Obj_DAL.DeleteDisplayProd(ID);
        }
    }
}
