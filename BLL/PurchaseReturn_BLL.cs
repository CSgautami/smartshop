﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;

namespace BLL
{
    public class PurchaseReturn_BLL
    {

        PurchaseReturn_DAL obj_DAL = new PurchaseReturn_DAL();
        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID, string LedgerID)
        {
            return obj_DAL.GetStock(ItemID,BatchNo,UDate,BranchID ,LedgerID);
        }
        public DataSet GetLedgerDetails(string LedgerID)
        {
            return obj_DAL.GetLedgerDetails(LedgerID);
        }

        public DataSet GetBatchNos(string ItemID, string BranchID)
        {
            return obj_DAL.GetBatchNos(ItemID, BranchID);
        }
        public DataSet GetLedgerName(string BranchID)
        {
            return obj_DAL.GetLedgerName(BranchID );
        }

        public DataSet GetAccLedger(string BranchID)
        {
            return obj_DAL.GetAccLedger(BranchID );
        }

        public DataSet GetStockItem()
        {
            return obj_DAL.GetStockItem();
        }

        public DataSet GetTaxSystem()
        {
            return obj_DAL.GetTaxSystem();
        }
        public DataSet GetTax(string tax)
        {
            return obj_DAL.GetTax(tax);
        }
        public DataSet GetData(string BranchID, string InvID, string Flag)
        {
            return obj_DAL.GetData(BranchID , InvID, Flag);
        }
        public string SavePurchaseReturn(string PRInvID, string UInvID, string PRInvNo, string Date, string PurReInvNo,
                                         string PRDate, string Lno, string Address, string Mode, string Days, string Note,
                                         string AcLno, string TotAmt, string TotDisc, string TotTax, string NetDisc,
                                         string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,
                                         string UserID, string XmlString)
        {
            return obj_DAL.SavePurchaseReturn(PRInvID, UInvID, PRInvNo, Date, PurReInvNo, 
                                            PRDate, Lno, Address, Mode, Days, Note, 
                                            AcLno, TotAmt, TotDisc, TotTax, NetDisc, 
                                            Frieght, LAndUL, Adj, NetAmt, BranchID, 
                                            UserID, XmlString);
        }

        public string DeletePurchaseReturn(string InvID, string BranchID)
        {
            return obj_DAL.DeletePurchaseReturn(InvID,BranchID);
        }
        public DataSet CallFillItem(string prefix)
        {
            return obj_DAL.CallFillItem(prefix);
        }
        public DataSet GetSearchInvoiceData(string BranchID, string PRInvID)
        {
            return obj_DAL.GetSearchInvoiceData(BranchID, PRInvID);  
        }
    }
}
