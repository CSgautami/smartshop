﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class RegistrationDet_BLL
    {
        RegistrationDet_DAL Obj_DAL = new RegistrationDet_DAL();
        public DataSet GetArea()
        {
            return Obj_DAL.GetArea();
        }

        public DataSet GetCity()
        {
            return Obj_DAL.GetCity();
        }

        public DataSet GetState()
        {
            return Obj_DAL.GetState();
        }

        public DataSet GetAreaWiseZone(string AreaID)
        {
            return Obj_DAL.GetAreaWiseZone(AreaID);
        }

        public DataSet InsertRegistrationDet(string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string PhNo, string Mobile, string Email, string UserName, string Password,string BranchID,string ACode)
        {
            return Obj_DAL.InsertRegistrationDet(Name,HNO,SOne,STwo,Area,City,State,Mark,PhNo,Mobile,Email,UserName,Password,BranchID,ACode);
        }
        public string UpdateProfile(string UserID,string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string PhNo, string Mobile, string Email)
        {
            return Obj_DAL.UpdateProfile(UserID,Name, HNO, SOne, STwo, Area, City, State, Mark, PhNo, Mobile, Email);
        }

        public DataSet GetUserData(string UserID)
        {
            return Obj_DAL.GetUserData(UserID);
        }

        public string InsertAgentDet(string AgentID,string AgentCode,string Name,string Address,string Mobile,string Email,string UserID)
        {
            return Obj_DAL.InsertAgentDet(AgentID, AgentCode,Name,Address,Mobile,Email ,UserID);
        }
        public DataSet GetAgent()
        {
            return Obj_DAL.GetAgent();
        }        
        public string DeleteAgent(string AgentID)
        {
            return Obj_DAL.DeleteAgent(AgentID);
        }

        public string GetAgentValid(string strAgent)
        {
            return Obj_DAL.GetAgentValid(strAgent);
        }
    }
}
