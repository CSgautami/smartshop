﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class State_BLL
    {
        State_DAL obj_DAL = new State_DAL();

        public DataSet GetStates(string CountryID)
        {
            return obj_DAL.GetStates();
        }
        public string InsertStates(string Countryid, string Statename)
        {
            return obj_DAL.InsertStates(Countryid, Statename);
        }

        public string UpdateState(string StateID, string StateName)
        {
            return obj_DAL.UpdateState(StateID, StateName);
        }

        public string DeleteState(int StateName)
        {
            return obj_DAL.DeleteState(StateName);
        }

        public DataSet SelectCountry()
        {
            return obj_DAL.SelectCountry();
        }
    }
}
