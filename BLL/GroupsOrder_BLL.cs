﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;

namespace BLL
{
    public class GroupsOrder_BLL
    {
        GroupsOrder_DAL obj_DAL = new GroupsOrder_DAL();
        public DataSet GetProductList(string HGNo)
        {
            return obj_DAL.GetProductList(HGNo);
        }
        public string SaveGroupsOrder(string strXml)
        {
            return obj_DAL.SaveGroupsOrder(strXml);
        }

        public DataSet GetHeadGroupList()
        {
            return obj_DAL.GetHeadGroupList();
        }

        public string SaveHeadGroupsOrder(string strXmlString)
        {
            return obj_DAL.SaveHeadGroupsOrder(strXmlString);
        }
    }
}
