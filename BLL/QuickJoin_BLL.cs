﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class QuickJoin_BLL
    {
        QuickJoin_DAL obj_DAL = new QuickJoin_DAL();

        public DataSet GetRefNameAndPinNo(string PinNo)
        {
            return obj_DAL.GetRefNameAndPinNo(PinNo);
        }

        public DataSet getLedgerName(string RefID)
        {
            return obj_DAL.getLedgerName(RefID);
        }
    }
}
