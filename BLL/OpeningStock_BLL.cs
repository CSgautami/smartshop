﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class OpeningStock_BLL
    {
        OpeningStock_DAL Obj_DAL = new OpeningStock_DAL();
        public string SaveOpeningStock(string BranchID, string Item, string BatchNo, string MRP, string Price, string Quantity,string UserID)
        {
            return Obj_DAL.SaveOpeningStock(BranchID, Item, BatchNo, MRP, Price, Quantity,UserID);
        }
        

        public DataSet GetData(string BranchID)
        {
            return Obj_DAL.GetData(BranchID);
        }

        public DataSet GetBranch(string UserID)
        {
            return Obj_DAL.GetBranch(UserID);
        }

        public DataSet GetItem()
        {
            return Obj_DAL.GetItem();
        }
        public string UpdateOpeningStock(string StockID, string BranchID, string Item, string BatchNo, string MRP, string Price, string Quantity,string UserID)
        {
            return Obj_DAL.UpdateOpeningStock(StockID, BranchID, Item, BatchNo, MRP, Price, Quantity, UserID);
        }
        public string DeleteOpeningStock(string StockID, string BranchID)
        {
            return Obj_DAL.DeleteOpeningStock(StockID, BranchID);
        }
        public DataSet GetStockItem()
        {
            return Obj_DAL.GetStockItem();
        }

        
    }
}
