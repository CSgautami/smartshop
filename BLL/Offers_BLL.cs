﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Offers_BLL
    {
        Offers_DAL Obj_DAL = new Offers_DAL();
        public DataSet GetItemCreation()
        {
            return Obj_DAL.GetItemCreation(); 
        }

        public string InsertOffers(string HeadGroupID,string Item, string Percent, string UserID)
        {
            return Obj_DAL.InsertOffers(HeadGroupID,Item,Percent,UserID);
        }

        public DataSet GetOffers()
        {
            return Obj_DAL.GetOffers();
        }

        public string UpdateOffers(string hidOfersID,string HeadGroupID ,string Item, string Percent, string UserID)
        {
            return Obj_DAL.UpdateOffers(hidOfersID, HeadGroupID, Item, Percent, UserID);
        }

        public string DeleteOffers(string OffersID)
        {
            return Obj_DAL.DeleteOffers(OffersID);
        }

        public DataSet GetHeadWiseItems(string HeadGroupID)
        {
            return Obj_DAL.GetHeadWiseItems(HeadGroupID);
        }
    }
}
