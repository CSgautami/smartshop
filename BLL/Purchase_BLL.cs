﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;
namespace BLL
{    
    public class Purchase_BLL
    {
        Purchase_DAL obj_DAL = new Purchase_DAL();
        public DataSet GetLedgerName(string BranchID)
        {
            return obj_DAL.GetLedgerName(BranchID );
        }
        public DataSet GetLedgerDetails(string LedgerID)
        {
            return obj_DAL.GetLedgerDetails(LedgerID);
        }

        public DataSet GetAccLedger(string BranchID)
        {
            return obj_DAL.GetAccLedger(BranchID);
        }

        public DataSet  GetStockItem()
        {
            return obj_DAL.GetStockItem();
        }

        public DataSet GetTaxSystem()
        {
            return obj_DAL.GetTaxSystem();
        }
        public string SaveInvoice(string InvID, string UInvID, string InvNo, string Date, string PurInvNo, string PurDate, string LNo, string Address, string Mode, string Days, string Note, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,string UserID, string XmlString)
        {
            return obj_DAL.SaveInvoice(InvID,UInvID,InvNo,Date,PurInvNo,PurDate,LNo, Address, Mode, Days, Note, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID,UserID, XmlString);
        }
        public DataSet GetTax(string tax)
        {
            return obj_DAL.GetTax(tax);
        }
        public DataSet GetData(string BranchID, string InvID, string Flag)
        {
            return obj_DAL.GetData(BranchID ,InvID,Flag);
        }
        //public DataSet GetInvMaxID(string UserID)
        //{
        //    return obj_DAL.GetInvMaxID(UserID);
        //}

        public DataSet GetBranch(string UserID)
        {
            return obj_DAL.GetBranch(UserID);
        }

        public DataSet GetItemWiseTaxSystem(string ItemID)
        {
            return obj_DAL.GetItemWiseTaxSystem(ItemID);
        }


        public string DeletePurchase(string InvID,string BranchID)
        {
            return obj_DAL.DeletePurchase(InvID,BranchID);
        }

        public DataSet GetPriceAndMRP(string ItemID, string BatchNo)
        {
            return obj_DAL.GetPriceAndMRP(ItemID,BatchNo);
        }
        public DataSet CallFillItem(string prefix)
        {
            return obj_DAL.CallFillItem(prefix);
        }
        public DataSet GetSearchInvoiceData(string BranchID, string InvID)
        {
            return obj_DAL.GetSearchInvoiceData(BranchID, InvID);
        }

        public DataSet GetInvoiceBillData(string strInvID, string BranchID)
        {
            return obj_DAL.GetInvoiceBillData(strInvID,BranchID);
        }
    }
}
