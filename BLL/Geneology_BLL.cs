﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;

namespace BLL
{
    public class Geneology_BLL
    {
        Geneology_DAL dalGenology = new Geneology_DAL();
        public DataSet GetGeneology(string UserID)
        {
            return dalGenology.GetGeneology(UserID);
        }
        public DataSet GetIDExists(string MLedgerID, string LedgerID,string Customer)
        {
            return dalGenology.GetIDExists(MLedgerID, LedgerID,Customer);
        }
    }
}
