﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BLL
{
    public class GroupMaster_BLL
    {
        GroupMaster_DAL obj_DAL = new GroupMaster_DAL();
        public string SaveGroup(string BranchID,string GroupName, string GroupCode, string Status, string Bank, string UserID)
        {
            return obj_DAL.SaveGroup(BranchID,GroupName, GroupCode, Status, Bank, UserID);
        }
        public string UpdateGroup(string GMNo,string BranchID,string GroupName, string GroupCode, string Status, string Bank, string UserID)
        {
            return obj_DAL.UpdateGroup(GMNo,BranchID ,GroupName, GroupCode, Status, Bank, UserID);
        }

        public DataSet GetGMNavData(string BranchID, string GMNO, string Flag)
        {
            return obj_DAL.GetGMNavData(BranchID, GMNO, Flag);
        }
        public DataSet GetBranch(string UserID)
        {
            return obj_DAL.GetBranch(UserID);
        }
        public string DeleteGroup(string GroupID)
        {
            return obj_DAL.DeleteGroup(GroupID);
        }
        
    }
}
