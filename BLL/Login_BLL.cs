﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;

namespace BLL
{
    public class Login_BLL
    {
        Login_DAL dalLogin = new Login_DAL();
        public string CheckAuthorization(string UserID, string URL)
        {
            return dalLogin.CheckAuthorization(UserID, URL);
        }
        public DataSet GetUserMenu(string UserID)
        {
            return dalLogin.GetUserMenu(UserID);
        }
        public DataSet GetName(string UserID)
        {
            return dalLogin.GetName(UserID);
        }
        public DataSet GetNameID(string CID)
        {
            return dalLogin.GetNameID(CID);
        }

        public string UpdatePassword(string NewPassword,string UserID)
        {
            return dalLogin.UpdatePassword(NewPassword,UserID);
        }
    }
}
