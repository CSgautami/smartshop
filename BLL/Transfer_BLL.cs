﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;
namespace BLL
{
    public class Transfer_BLL
    {
        Transfer_DAL obj_DAL = new Transfer_DAL();
        public DataSet GetToBranch(string BranchID,string UserID)
        {
            return obj_DAL.GetToBranch(BranchID,UserID);
        }

        public DataSet GetBranchDetails(string ToBranchID)
        {
            return obj_DAL.GetBranchDetails(ToBranchID);
        }

        public string SaveInvoice(string hidTInvID, string hidUInvID, string InvNo, string Date, string TInvNo, string TDate, string ToBranchID, string Address, string Mode, string Days, string Note, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID, string UserID, string XmlString)
        {
            return obj_DAL.SaveInvoice(hidTInvID,hidUInvID,InvNo,Date,TInvNo,TDate,ToBranchID,Address,Mode,Days,Note,TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID,UserID, XmlString);
        }

        public DataSet GetData(string BranchID, string InvID, string Flag)
        {
            return obj_DAL.GetData(BranchID, InvID, Flag);
        }

        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID)
        {
            return obj_DAL.GetStock(ItemID,BatchNo,UDate,BranchID);
        }

        public DataSet GetBatchNo(string ItemID, string BranchID)
        {
            return obj_DAL.GetBatchNo(ItemID, BranchID);
        }

        public string DeleteTransfer(string InvID,string BranchID)
        {
            return obj_DAL.DeleteTransfer(InvID,BranchID);
        }

        public string SaveInvoiceTransferIn(string hidTInvID, string hidUInvID, string InvNo, string InvDate, string TInvNo, string TInvDate, string ToBranchID, string Address, string Mode, string Days, string Note, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID, string UserID, string XmlString,string RefTInvID)
        {
            return obj_DAL.SaveInvoiceTransferIn(hidTInvID, hidUInvID, InvNo, InvDate, TInvNo, TInvDate, ToBranchID, Address, Mode, Days, Note, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID, UserID, XmlString,RefTInvID);
        }

        public DataSet GetDataTransferIn(string BranchID, string InvID, string Flag)
        {
            return obj_DAL.GetDataTransferIn(BranchID, InvID, Flag);
        }

        public string DeleteTransferIn(string InvID, string BranchID)
        {
            return obj_DAL.DeleteTransferIn(InvID, BranchID);
        }

        public DataSet GetDataTransfer(string RefTInvID)
        {
            return obj_DAL.GerDataTransfer(RefTInvID);
        }
        public DataSet CallFillItem(string prefix)
        {
            return obj_DAL.CallFillItem(prefix);
        }
        public DataSet GetSearchInvoiceData(string BranchID, string TInvID)
        {
            return obj_DAL.GetSearchInvoiceData(BranchID, TInvID);
        }
        public DataSet GetSearchInvoiceDat(string BranchID, string TInvID)
        {
            return obj_DAL.GetSearchInvoiceDat(BranchID, TInvID);
        }

        public DataSet GetBranch(string UserID)
        {
            return obj_DAL.GetBranch(UserID);
        }

        public DataSet GetInvoiceBillData(string strInvID, string BranchID)
        {
            return obj_DAL.GetInvoiceBillData(strInvID,BranchID);
        }
    }
}
