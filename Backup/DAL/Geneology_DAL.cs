﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class Geneology_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetGeneology(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetGeneology_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        public DataSet GetIDExists(string MLedgerID, string LedgerID, string Customer)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetIDExists_sp");
                db.AddInParameter(dbcmd, "MLedgerID", DbType.StringFixedLength, MLedgerID);
                db.AddInParameter(dbcmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                db.AddInParameter(dbcmd, "bCustomer", DbType.StringFixedLength, Customer);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
    }
}
