﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
     public class VatReturn_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetVatReturn(string BranchID, string FromDate, string ToDate, string Refresh)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetVatReturnData_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "Refresh", DbType.StringFixedLength, Refresh);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {

            }
            return ds;
        }
        public string SaveVatReturn(string VATReturnNo, string fromDate, string ToDate, string ExemptPurch, string FourPPurch, string FourPPurchVat,
            string StdRatePurch, string StdRatePurchVat, string OtherVatPurch, string OtherVatPurchVat, string SatPurch, string SatPurchVat, string ExemptSales,
            string ZeroRateSalesIE, string ZeroRateSalesOther, string TaxDue, string TaxDueVat, string FourPRateSales, string FourPRateSalesVat,
            string StdRateSales, string StdRateSalesVat, string SatSales, string SatSalesVat, string OtherVatRateSales, string OtherVatRateSalesVat,
            string TotalAmoutToPay, string BranchId, string Paid, string Refund, string CarredForward, string box24a, string box24b, string CSTCF,
            string StdRate14P5Purch, string StdRate14P5PurchVat, string StdRate14P5Sales, string StdRate14P5SalesVat,
            string FivePPurch, string FivePPurchVat, string FivePRateSales, string FivePRateSalesVat)
        {
            String res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("SaveVatReturn_sp");
                db.AddInParameter(dbcmd, "VATReturnNo", DbType.StringFixedLength, VATReturnNo);
                db.AddInParameter(dbcmd, "fromDate", DbType.StringFixedLength, fromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbcmd, "ExemptPurch", DbType.StringFixedLength, ExemptPurch);
                db.AddInParameter(dbcmd, "4PPurch", DbType.StringFixedLength, FourPPurch);
                db.AddInParameter(dbcmd, "4PPurchVat", DbType.StringFixedLength, FourPPurchVat);
                db.AddInParameter(dbcmd, "StdRatePurch", DbType.StringFixedLength, StdRatePurch);
                db.AddInParameter(dbcmd, "StdRatePurchVat", DbType.StringFixedLength, StdRatePurchVat);
                db.AddInParameter(dbcmd, "OtherVatPurch", DbType.StringFixedLength, OtherVatPurch);
                db.AddInParameter(dbcmd, "OtherVatPurchVat", DbType.StringFixedLength, OtherVatPurchVat);
                db.AddInParameter(dbcmd, "SatPurch", DbType.StringFixedLength, SatPurch);
                db.AddInParameter(dbcmd, "SatPurchVat", DbType.StringFixedLength, SatPurchVat);
                db.AddInParameter(dbcmd, "ExemptSales", DbType.StringFixedLength, ExemptSales);
                db.AddInParameter(dbcmd, "ZeroRateSalesIE", DbType.StringFixedLength, ZeroRateSalesIE);
                db.AddInParameter(dbcmd, "ZeroRateSalesOther", DbType.StringFixedLength, ZeroRateSalesOther);
                db.AddInParameter(dbcmd, "TaxDue", DbType.StringFixedLength, TaxDue);
                db.AddInParameter(dbcmd, "TaxDueVat", DbType.StringFixedLength, TaxDueVat);
                db.AddInParameter(dbcmd, "4PRateSales", DbType.StringFixedLength, FourPRateSales);
                db.AddInParameter(dbcmd, "4PRateSalesVat", DbType.StringFixedLength, FourPRateSalesVat);
                db.AddInParameter(dbcmd, "StdRateSales", DbType.StringFixedLength, StdRateSales);
                db.AddInParameter(dbcmd, "StdRateSalesVat", DbType.StringFixedLength, StdRateSalesVat);
                db.AddInParameter(dbcmd, "SatSales", DbType.StringFixedLength, SatSales);
                db.AddInParameter(dbcmd, "SatSalesVat", DbType.StringFixedLength, SatSalesVat);
                db.AddInParameter(dbcmd, "OtherVatRateSales", DbType.StringFixedLength, OtherVatRateSales);
                db.AddInParameter(dbcmd, "OtherVatRateSalesVat", DbType.StringFixedLength, OtherVatRateSalesVat);
                db.AddInParameter(dbcmd, "TotalAmoutToPay", DbType.StringFixedLength, TotalAmoutToPay);
                db.AddInParameter(dbcmd, "BranchId", DbType.StringFixedLength, BranchId);
                db.AddInParameter(dbcmd, "Paid", DbType.StringFixedLength, Paid);
                db.AddInParameter(dbcmd, "Refund", DbType.StringFixedLength, Refund);
                db.AddInParameter(dbcmd, "CarredForward", DbType.StringFixedLength, CarredForward);
                db.AddInParameter(dbcmd, "box24a", DbType.StringFixedLength, box24a);
                db.AddInParameter(dbcmd, "box24b", DbType.StringFixedLength, box24b);
                db.AddInParameter(dbcmd, "CSTCF", DbType.StringFixedLength, CSTCF);
                //db.AddInParameter(dbcmd, "Claim", DbType.StringFixedLength, Claim);
                //db.AddInParameter(dbcmd, "ClaimEligible", DbType.StringFixedLength, ClaimEligible);
                db.AddInParameter(dbcmd, "StdRate14P5Purch", DbType.StringFixedLength, StdRate14P5Purch);
                db.AddInParameter(dbcmd, "StdRate14P5PurchVat", DbType.StringFixedLength, StdRate14P5PurchVat);
                db.AddInParameter(dbcmd, "StdRate14P5Sales", DbType.StringFixedLength, StdRate14P5Sales);
                db.AddInParameter(dbcmd, "StdRate14P5SalesVat", DbType.StringFixedLength, StdRate14P5SalesVat);
                db.AddInParameter(dbcmd, "5PPurch", DbType.StringFixedLength, FivePPurch);
                db.AddInParameter(dbcmd, "5PPurchVat", DbType.StringFixedLength, FivePPurchVat);
                db.AddInParameter(dbcmd, "5PRateSales", DbType.StringFixedLength, FivePRateSales);
                db.AddInParameter(dbcmd, "5PRateSalesVat", DbType.StringFixedLength, FivePRateSalesVat);
                //db.AddInParameter(dbcmd, "xmlStringAdj", DbType.StringFixedLength, XmlStringAdj);
                //db.AddInParameter(dbcmd, "xmlStringPayment", DbType.StringFixedLength, XmlStringPayment);
                res = db.ExecuteScalar(dbcmd).ToString();
            }
            catch
            {
            }
            return res;
        }
        public string SavePay(string VRNo, string BranchID, string XmlString)
        {
            String res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("InsertVatRetPayment_sp");
                db.AddInParameter(dbcmd, "VATReturnNo", DbType.StringFixedLength, VRNo);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "xmlString", DbType.StringFixedLength, XmlString);
                res = db.ExecuteScalar(dbcmd).ToString();
            }
            catch
            {
            }
            return res;

        }
        public string SaveAdj(string VRNo, string BranchID, string XmlString)
        {
            String res = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("InsertVatRetAdj_sp");
                db.AddInParameter(dbcmd, "VATReturnNo", DbType.StringFixedLength, VRNo);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "xmlString", DbType.StringFixedLength, XmlString);
                res = db.ExecuteScalar(dbcmd).ToString();
            }
            catch
            {
            }
            return res;

        }
        public DataSet GetVatReturnPrintData(string SRNo, string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetVatReturnPrintData_sp");
                db.AddInParameter(dbcmd, "SRNo", DbType.StringFixedLength, SRNo);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {

            }
            return ds;
        }
    }
}
