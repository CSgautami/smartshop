﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class City_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string InsertCity(string CountryID, string StateID, string CityName)
        {
            
                DbConnection dbCon = null;
                DbTransaction dbTrans = null;
                String rtnVal = "";
                try
                {
                    dbCon = db.CreateConnection();
                    dbCon.Open();
                    dbTrans = dbCon.BeginTransaction();
                    DbCommand dbcmd = db.GetStoredProcCommand("InsertCity_sp");
                    db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                    db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, StateID);
                    db.AddInParameter(dbcmd, "CityName", DbType.StringFixedLength, CityName);
                    rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                    dbTrans.Commit();

                }
                catch (Exception ex)
                {
                    if (dbTrans != null)
                        dbTrans.Rollback();
                    rtnVal = "";
                }
                finally
                {
                    if (dbCon != null)
                        dbCon.Close();
                }
                return rtnVal;            

        }

        public DataSet GetCity(string StateID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetCity_sp");
                db.AddInParameter(dbCmd, "StateID", DbType.StringFixedLength, StateID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch (Exception ex)
            {
            }
            return ds;

        }

        public string UpdateCity(string CityID, string CityName)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtn = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateCity_sp");
                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, CityID);
                db.AddInParameter(dbcmd, "CityName", DbType.StringFixedLength, CityName);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtn = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }

            return rtn;
        }



        public string DeleteCity(string Cityid)
        {
            string rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteCity_sp");

                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, Cityid);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
            }
            return rtn;

        }

        public DataSet GetStates()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStates_sp");
                //db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet CountryChange(string CountryID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStateByCountry_sp");
                db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;

        }
    }
}
