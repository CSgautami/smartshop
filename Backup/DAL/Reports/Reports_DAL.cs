﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.IO;

namespace DAL.Reports
{
    public class Reports_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet StockIndentReportData(string GroupID,string Company)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockIndentReport_sp");
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, GroupID);
                db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, Company);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch   
            {
            }
            return ds;
        }
        public DataSet ClosingStock(string ToDate, string GID, string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("ClosingStock_sp");
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                //db.AddInParameter(dbcmd, "HGID", DbType.StringFixedLength, HGID);
                db.AddInParameter(dbcmd, "GID", DbType.StringFixedLength, GID);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetStockGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroupName_sp");
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HGID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetCompany()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCompany_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet StockMovementData(string FromDate, string ToDate,  string Group, string Company, string All, string BranchID,string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockMovementReport_sp");
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate );
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HGroup);
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, Group);
                db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, Company);
                db.AddInParameter(dbcmd, "bAll", DbType.StringFixedLength, All );
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetStockItem(string GID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockItemGroupWise_SP");
                db.AddInParameter(dbcmd, "GID", DbType.StringFixedLength, GID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet StockLedgerData(string FromDate, string ToDate, string Group, string Item, string Batch, string Branch,string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockLedgerData_sp");
                db.AddInParameter(dbcmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbcmd, "ToDate", DbType.StringFixedLength, ToDate);
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HG);
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, Group);
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, Item);
                db.AddInParameter(dbcmd, "BatchNo", DbType.StringFixedLength, Batch );
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, Branch);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetArea()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetArea_sp");
                db.AddInParameter(dbCmd, "Search", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZone()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetZone_sp");
                db.AddInParameter(dbCmd, "Search", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetAreaCustomerList(string AreaID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetCustomerListArea_sp");
                db.AddInParameter(dbCmd, "AreaID", DbType.StringFixedLength, AreaID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZoneCustomerList(string ZoneID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetCustomerListZone_sp");
                db.AddInParameter(dbCmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetCustomerList(string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetCustomerList_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetAreaCommission(string AreaID, string dtFrom, string dtTo)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetAreaComm_sp");
                db.AddInParameter(dbCmd, "AreaID", DbType.StringFixedLength, AreaID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, dtFrom);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, dtTo);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetRegisteredMembersList(string AreaID, string dtFrom, string dtTo)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetRegisteredMembersList_sp");
                db.AddInParameter(dbCmd, "AreaID", DbType.StringFixedLength, AreaID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, dtFrom);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, dtTo);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        

        public DataSet GetZoneCommission(string ZoneID, string dtFrom, string dtTo)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetZoneCommission_sp");
                db.AddInParameter(dbCmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, dtFrom);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, dtTo);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetMember()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetMembers_sp");
            //db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetMyTeam(string MemberID, string bCustomer)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetMyTeam_SP");
            db.AddInParameter(dbCmd, "RefNo", DbType.StringFixedLength, MemberID);
            db.AddInParameter(dbCmd, "bCustomer", DbType.StringFixedLength, bCustomer);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet TransfersReportData(string FromDate, string ToDate, string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTransferReportData_sp");                
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet PendingTransfersReportData(string FromDate, string ToDate, string ToBranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPendingTransferData_sp");
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "ToBranchID", DbType.StringFixedLength, ToBranchID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetAreaAc(string AreaID, string FromDate, string ToDate, string chkPay)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetAreaAc_sp");
                db.AddInParameter(dbCmd, "AreaID", DbType.StringFixedLength, AreaID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "Pay", DbType.StringFixedLength, chkPay);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZoneAc(string ZoneID, string FromDate, string ToDate, string chkPay)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetZoneAc_sp");
                db.AddInParameter(dbCmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "Pay", DbType.StringFixedLength, chkPay);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetMyAccount(string LedgerID, string FromDate, string ToDate, string chkPay, string bCustomer)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetMyAcc_sp");
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID );
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "Pay", DbType.StringFixedLength, chkPay);
                db.AddInParameter(dbCmd, "bCustomer", DbType.StringFixedLength, bCustomer);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetMembersMast(string LedgerID,string chkPay, string bCustomer)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetMembersMast_sp");
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);                
                db.AddInParameter(dbCmd, "Pay", DbType.StringFixedLength, chkPay);
                db.AddInParameter(dbCmd, "bCustomer", DbType.StringFixedLength, bCustomer);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public string SavePayData(string FromDate, string ToDate, string PayDate, string UserID, string BranchID, string xmlCustomerPay)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("SaveCustomerPayments_sp");
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "PayDate", DbType.StringFixedLength, PayDate);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "CustomerPayments", DbType.StringFixedLength, xmlCustomerPay);
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch
            {
                res = "";
            }
            return res;
        }
        public DataSet GetWeeklyComm(string YourID, string FromDate, string ToDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("WeeklyCustomerCommission_SP");
                db.AddInParameter(dbCmd, "YourID", DbType.StringFixedLength, YourID);
                db.AddInParameter(dbCmd, "DFrom", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "DTo", DbType.StringFixedLength, ToDate);                
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetMyPins(string YourID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetAvailablePinNumbers_sp");
                db.AddInParameter(dbCmd, "YourID", DbType.StringFixedLength, YourID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetPinIssue(string LedgerID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPinsIssue_sp");
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;   
        }
        public DataSet CallFillItem(string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillItem_sp");
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetItemName(string ItemID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetItemWiseTaxSystem_sp");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "Status", DbType.StringFixedLength, "");
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet CallFillMember(string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillMembers_sp");
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetWeeklyCommdet(string UserID, string Date)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("WeeklyDetails_sp");
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "Date", DbType.StringFixedLength, Date);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;   
        }
        public DataSet InActiveMembers(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetInActiveMembers_sp");
                db.AddInParameter(dbCmd, "YourID", DbType.StringFixedLength, UserID);                
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetIncentivesReport(string YourID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetIncentivesReport_SP");
                db.AddInParameter(dbCmd, "YourID", DbType.StringFixedLength, YourID);                
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetIncentivesReportdet(string YourID, string FDate,string TDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetIncentiveReportDet_SP");
                db.AddInParameter(dbCmd, "YourID", DbType.StringFixedLength, YourID);
                db.AddInParameter(dbCmd, "FDate", DbType.StringFixedLength, FDate);
                db.AddInParameter(dbCmd, "TDate", DbType.StringFixedLength, TDate);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet ReportFinalAccount(string chkDateWise, string Date, string To,  string rDetailed, string rTradingAccount, string rProfitandLossAc, string rBalenceSheet)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetFinalAcounts_sp");
                db.AddInParameter(dbCmd, "bDate", DbType.StringFixedLength, chkDateWise);
                db.AddInParameter(dbCmd, "dateFrom", DbType.StringFixedLength, Date);
                db.AddInParameter(dbCmd, "dateTo", DbType.StringFixedLength, To);
                db.AddInParameter(dbCmd, "bDetail", DbType.StringFixedLength, rDetailed);
                db.AddInParameter(dbCmd, "optTrade", DbType.StringFixedLength, rTradingAccount);
                db.AddInParameter(dbCmd, "optPAndL", DbType.StringFixedLength, rProfitandLossAc);
                db.AddInParameter(dbCmd, "optBal", DbType.StringFixedLength,rBalenceSheet);
                
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetUserPriceListReport(string GroupID, string Search,string StockItemsOnly)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPriceDetailsReport_sp");
                db.AddInParameter(dbCmd, "StockGroupID", DbType.StringFixedLength, GroupID);
                db.AddInParameter(dbCmd, "SearchItem", DbType.StringFixedLength, Search);
                db.AddInParameter(dbCmd, "StockItemsOnly", DbType.StringFixedLength, StockItemsOnly);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw; 
            }
            return ds;
        }

        public DataSet GetGroups()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetGroupName_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetTrailBalence(string chkSerialByName, string Branch, string chkGroups, string chkOpenings, string rDebtors, string chkLSTGroups, string chkDateWise, string Date, string chkShowDebtors, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetTrialBalance_sp");
                db.AddInParameter(dbcmd, "bOrderName", DbType.StringFixedLength, chkSerialByName);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, Branch);
                db.AddInParameter(dbcmd, "bGroup", DbType.StringFixedLength, chkGroups);
                db.AddInParameter(dbcmd, "bOpenings", DbType.StringFixedLength, chkOpenings);
                db.AddInParameter(dbcmd, "bDebitor", DbType.StringFixedLength, rDebtors);
                db.AddInParameter(dbcmd, "strGIDs", DbType.StringFixedLength, chkLSTGroups);
                db.AddInParameter(dbcmd, "bDtwise", DbType.StringFixedLength, chkDateWise);
                db.AddInParameter(dbcmd, "Date", DbType.StringFixedLength, Date);
                db.AddInParameter(dbcmd, "bDebiConde", DbType.StringFixedLength, chkShowDebtors);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet TransfersInReportData(string FromDate, string ToDate, string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTransferInReportData_sp");
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "ToBranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetPaySMSMembers(string FromDate, string ToDate, string PayDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetPaySMSMembers_sp");
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "PayDate", DbType.StringFixedLength, PayDate);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet TotalSalesReportData(string FromDate, string ToDate, string FromInv, string ToInv, string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTotalSalesReport_sp");
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "FromInvNo", DbType.StringFixedLength, FromInv);
                db.AddInParameter(dbCmd, "ToInvNo", DbType.StringFixedLength, ToInv);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet DetailSalesReportData(string FromDate, string ToDate, string FromInv, string ToInv, string BranchID, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetDetailSalesReport_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "FromInvNo", DbType.StringFixedLength, FromInv);
                db.AddInParameter(dbCmd, "ToInvNo", DbType.StringFixedLength, ToInv);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet GetBranchList()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranchList_sp");                
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }

        public DataSet PriceWiseSalesReportData(string FromDate, string ToDate, string BranchID, string Prices, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetItemSalesReport_sp");
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "TypeOfPrice", DbType.StringFixedLength, Prices);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }

        //public DataSet TotalPurchaseReportData(string FromDate, string ToDate, string BranchID, string UserID)
        //{

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand dbCmd = db.GetStoredProcCommand("GetTotalPurchaseReport_sp");
        //        db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
        //        db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
        //        db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
        //        db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
        //        ds = db.ExecuteDataSet(dbCmd);
        //    }
        //    catch
        //    {
        //    }
        //    return ds;  
        //}

        public DataSet GetTotalPurchaseReportData(string BranchID, string FromDate, string ToDate, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTotalPurchaseReport_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);                
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;  
        }

        public DataSet DetailPurchaseReportData(string BranchID, string FromDate, string ToDate, string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetDetaailPurchaseReport_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;  
        }
        public DataSet GetAgentNames()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetAgentNames_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetAgentWiseCustomer(string AgentID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetAgentWiseCust_sp");
                db.AddInParameter(dbcmd, "AgentID", DbType.StringFixedLength, AgentID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetAgentAccntReport(string AgentID,string CustID, string FromDate, string ToDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetAgentAccount_sp");
                db.AddInParameter(dbCmd, "AgentID", DbType.StringFixedLength, AgentID);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, CustID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "Pay", DbType.StringFixedLength, 1);
                db.AddInParameter(dbCmd, "bCustomer", DbType.StringFixedLength, 1);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetAgentMembersReport(string AgentID,string FromDate, string ToDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetAgentMembers_sp");
                db.AddInParameter(dbCmd, "AgentID", DbType.StringFixedLength, AgentID);                
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);               
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZoneNames()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetZoneNames_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZoneWiseCustomer(string ZoneID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetZoneWiseCust_sp");
                db.AddInParameter(dbcmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZoneAccountReport(string ZoneID, string CustID, string FromDate, string ToDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetZoneAccount_sp");
                db.AddInParameter(dbCmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, CustID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                db.AddInParameter(dbCmd, "Pay", DbType.StringFixedLength, 1);
                db.AddInParameter(dbCmd, "bCustomer", DbType.StringFixedLength, 1);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
        public DataSet GetZoneMembersReport(string ZoneID, string FromDate, string ToDate)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetZoneMembers_sp");
                db.AddInParameter(dbCmd, "ZoneID", DbType.StringFixedLength, ZoneID);
                db.AddInParameter(dbCmd, "FromDate", DbType.StringFixedLength, FromDate);
                db.AddInParameter(dbCmd, "ToDate", DbType.StringFixedLength, ToDate);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch
            {
            }
            return ds;
        }
    }
}
