﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class VatType_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string InsertVatTypeDesc(string VatTypeDesc)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertVatType_sp");
                db.AddInParameter(dbcmd, "VatTypeDesc", DbType.StringFixedLength, VatTypeDesc);
                rtval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public DataSet GetVatTypeDesc()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetVatTypeMaster_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string UpdateVatTypeDesc(string VatTypeID, string VatTypeDesc)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateVatType_sp");
                db.AddInParameter(dbcmd, "VatTypeID", DbType.StringFixedLength, VatTypeID);
                db.AddInParameter(dbcmd, "VatTypeDesc", DbType.StringFixedLength, VatTypeDesc);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public string DeleteVatTypeDesc(string VatTypeID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteVatType");
                db.AddInParameter(dbcmd, "VatTypeID", DbType.StringFixedLength, VatTypeID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }
        public DataSet GetStockGroupData(string SearchItem)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroupData_sp");
                db.AddInParameter(dbcmd, "SearchItem", DbType.StringFixedLength, SearchItem);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }


    }
}
