﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;


namespace DAL
{
    public class ADDAddress_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public string InsertAddress(string UserID,string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string Zone)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertAddAddress_sp");
                db.AddInParameter(dbcmd, "UserID",DbType.StringFixedLength,UserID);
                //db.AddInParameter(dbcmd, "RegisID", DbType.StringFixedLength, RID);
                db.AddInParameter(dbcmd, "RegisName", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "HNO", DbType.StringFixedLength, HNO);
                db.AddInParameter(dbcmd, "StreetName1", DbType.StringFixedLength, SOne);
                db.AddInParameter(dbcmd, "StreetName2", DbType.StringFixedLength, STwo);
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, Area);
                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, City);
                db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, State);
                db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, Mark);
                db.AddInParameter(dbcmd, "Zone", DbType.StringFixedLength, Zone);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd, dbTrans));
                dbTrans.Commit();
            }

            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;

        }
        public string UpdateAddress(string SID,string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string Zone)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateAddress_sp");
                db.AddInParameter(dbcmd, "ShippingID",DbType.StringFixedLength,SID);
                //db.AddInParameter(dbcmd, "CustomerID", DbType.StringFixedLength, Customer);
                db.AddInParameter(dbcmd, "RegisName", DbType.StringFixedLength, Name);
                db.AddInParameter(dbcmd, "HNO", DbType.StringFixedLength, HNO);
                db.AddInParameter(dbcmd, "StreetName1", DbType.StringFixedLength, SOne);
                db.AddInParameter(dbcmd, "StreetName2", DbType.StringFixedLength, STwo);
                db.AddInParameter(dbcmd, "AreaID", DbType.StringFixedLength, Area);
                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, City);
                db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, State);
                db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, Mark);
                db.AddInParameter(dbcmd, "Zone", DbType.StringFixedLength, Zone);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                    rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }
    }
}
