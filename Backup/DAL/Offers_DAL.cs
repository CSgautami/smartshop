﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class Offers_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");

        public DataSet GetItemCreation()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetItemCreation_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public string InsertOffers(string HeadGroupID, string Item, string Percent, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertOffers_sp");
                db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroupID);
                db.AddInParameter(dbcmd, "ICID", DbType.StringFixedLength, Item);
                db.AddInParameter(dbcmd, "Percentage", DbType.StringFixedLength, Percent);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = db.ExecuteScalar(dbcmd, dbTrans).ToString();
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public DataSet GetOffers()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetOffers_sp");
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }

        public string UpdateOffers(string hidOfersID, string HeadGroupID,string Item, string Percent, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateOffers_sp");
                db.AddInParameter(dbcmd, "OfferID", DbType.StringFixedLength, hidOfersID);
                db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroupID);
                db.AddInParameter(dbcmd, "ICID", DbType.StringFixedLength, Item);
                db.AddInParameter(dbcmd, "Percentage", DbType.StringFixedLength, Percent);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd, dbTrans).ToString());
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }

            return rtnval;
        }

        public string DeleteOffers(string OffersID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteOffers_sp");
                db.AddInParameter(dbcmd, "OfferID", DbType.StringFixedLength,OffersID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }

        public DataSet GetHeadWiseItems(string HeadGroupID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetHeadWiseItems_sp");
                db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroupID);
                ds = db.ExecuteDataSet(dbcmd);

            }
            catch
            {
            }
            return ds;
        }
    }
}
