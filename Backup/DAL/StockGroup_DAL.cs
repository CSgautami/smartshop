﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;

namespace DAL
{
    public class StockGroup_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string InsertStockGroup(string StockGroupName,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertStockGroup_sp");
                db.AddInParameter(dbcmd, "StockGroupName", DbType.StringFixedLength, StockGroupName);
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroup);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength,UserID);
                rtval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public DataSet GetStockGroup(string Search)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroup_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, Search);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string UpdateStockGroup(string StockGroupID, string StockGroupName,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateStockGroup_sp");
                db.AddInParameter(dbcmd, "StockGroupID", DbType.StringFixedLength, StockGroupID);
                db.AddInParameter(dbcmd, "StockGroupName", DbType.StringFixedLength, StockGroupName);
                //db.AddInParameter(dbcmd, "HeadGroupID", DbType.StringFixedLength, HeadGroup);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public string DeleteStockGroup(string StockGroupID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteStockGroup_sp");
                db.AddInParameter(dbcmd, "StockGroupID", DbType.StringFixedLength, StockGroupID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }

        //public DataSet GetHeadGroup()
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand dbcmd = db.GetStoredProcCommand("GetHeadGroup_sp");
        //        //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
        //        ds = db.ExecuteDataSet(dbcmd);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return ds;

        //}

        //public DataSet GetStockGroupData(string SearchItem)
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroupData_sp");
        //        db.AddInParameter(dbcmd, "SearchItem", DbType.StringFixedLength, SearchItem);
        //        ds = db.ExecuteDataSet(dbcmd);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return ds;
        //}
    }
}
