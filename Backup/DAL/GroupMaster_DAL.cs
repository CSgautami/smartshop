﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
namespace DAL
{
    public class GroupMaster_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        public string SaveGroup(string BranchID,string GroupName, string GroupCode, string Status, string Bank,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertGroup_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "GroupName", DbType.StringFixedLength, GroupName);
                db.AddInParameter(dbcmd, "GroupCode", DbType.StringFixedLength, GroupCode);
                db.AddInParameter(dbcmd, "Status", DbType.StringFixedLength, Status);
                db.AddInParameter(dbcmd, "Bank", DbType.StringFixedLength, Bank);                
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);                
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;
        }

        public string UpdateGroup(string GMNo,string BranchID,string GroupName, string GroupCode, string Status, string Bank, string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            string rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateGroupMaster_sp");
                db.AddInParameter(dbcmd, "GroupNo", DbType.StringFixedLength, GMNo);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "GroupName", DbType.StringFixedLength, GroupName);
                db.AddInParameter(dbcmd, "GroupCode", DbType.StringFixedLength, GroupCode);
                db.AddInParameter(dbcmd, "Status", DbType.StringFixedLength, Status);
                db.AddInParameter(dbcmd, "Bank", DbType.StringFixedLength, Bank);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }

            return rtnVal;
        }

        public DataSet GetGMNavData(string BranchID, string GMNO, string Flag)
        {
            DbCommand dbcmd = db.GetStoredProcCommand("GetGMNavData_sp");
            db.AddInParameter(dbcmd, "GMNo", DbType.StringFixedLength, GMNO);
            db.AddInParameter(dbcmd, "Flag", DbType.StringFixedLength, Flag);
            db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbcmd);
        }


        public DataSet GetBranch(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranch_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string DeleteGroup(string GroupID)
        {
            string rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteGroupMaster_SP");
                db.AddInParameter(dbcmd, "GroupID", DbType.StringFixedLength, GroupID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
            }
            return rtn;
        }
    }
}
