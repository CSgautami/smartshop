﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class Registraton_DAL
    {
       Database db = DatabaseFactory.CreateDatabase("constr");

       public DataSet GetFilldata()
       {
           DataSet ds = new DataSet();
           try
           {
               DbCommand dbCmd = db.GetStoredProcCommand("GetFillRegistration_sp");
               ds = db.ExecuteDataSet(dbCmd);
           }
           catch
           {
               throw;
           }
           return ds;
       }

       public string InsertRedg(string RefID, string PinNo, string strRegDate, string Name, string fathet, string DOB, string LandMark, string Hno, string Post, string City, string District, string State, string PinCode, string SHno, string SPost, string SCity, string SDistrict, string SState, string SPinCode, string SLandMark, string PhoneNo, string Mobile, string PanNo, string Email, string Password, string Nominee, string Relation, string BankName, string AccountNo, string IFSCode, string Branch)
       {
           DbConnection dbCon = null;
           DbTransaction dbTrans = null;
           String rtnval = "";
           try
           {
               dbCon = db.CreateConnection();
               dbCon.Open();
               dbTrans = dbCon.BeginTransaction();
               DbCommand dbcmd = db.GetStoredProcCommand("InsertRegistration_sp");
               db.AddInParameter(dbcmd, "RefID", DbType.StringFixedLength, RefID);
               db.AddInParameter(dbcmd, "PinNumber", DbType.StringFixedLength, PinNo);
               db.AddInParameter(dbcmd, "RegDate", DbType.StringFixedLength, strRegDate);
               db.AddInParameter(dbcmd, "ApplicantName", DbType.StringFixedLength, Name);
               db.AddInParameter(dbcmd, "FatherOrHusband", DbType.StringFixedLength, fathet);
               db.AddInParameter(dbcmd, "DOB", DbType.StringFixedLength, DOB);
               db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, LandMark);
               db.AddInParameter(dbcmd, "HNo", DbType.StringFixedLength, Hno);
               db.AddInParameter(dbcmd, "Post", DbType.StringFixedLength, Post);
               db.AddInParameter(dbcmd, "City", DbType.StringFixedLength, City);
               db.AddInParameter(dbcmd, "DistrictID", DbType.StringFixedLength, District);
               db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, State);
               db.AddInParameter(dbcmd, "PinCode", DbType.StringFixedLength, PinCode);
               db.AddInParameter(dbcmd, "SHNo", DbType.StringFixedLength, SHno);
               db.AddInParameter(dbcmd, "SPost", DbType.StringFixedLength, SPost);
               db.AddInParameter(dbcmd, "SCity", DbType.StringFixedLength, SCity);
               db.AddInParameter(dbcmd, "SDistrictID", DbType.StringFixedLength, SDistrict);
               db.AddInParameter(dbcmd, "SStateID", DbType.StringFixedLength, SState);
               db.AddInParameter(dbcmd, "SPinCode", DbType.StringFixedLength, SPinCode);
               db.AddInParameter(dbcmd, "SLandMark", DbType.StringFixedLength, SLandMark);
               db.AddInParameter(dbcmd, "PhoneNo", DbType.StringFixedLength, PhoneNo);
               db.AddInParameter(dbcmd, "Mobile", DbType.StringFixedLength, Mobile);
               db.AddInParameter(dbcmd, "PANNo", DbType.StringFixedLength, PanNo);
               db.AddInParameter(dbcmd, "EmailID", DbType.StringFixedLength, Email);
               db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
               //db.AddInParameter(dbcmd, "CPassword", DbType.StringFixedLength, CPassword);
               db.AddInParameter(dbcmd, "NomineeName", DbType.StringFixedLength, Nominee);
               db.AddInParameter(dbcmd, "Relation", DbType.StringFixedLength, Relation);
               db.AddInParameter(dbcmd, "BankName", DbType.StringFixedLength, BankName);
               db.AddInParameter(dbcmd, "AccountNo", DbType.StringFixedLength, AccountNo);
               db.AddInParameter(dbcmd, "IFSCode", DbType.StringFixedLength, IFSCode);
               db.AddInParameter(dbcmd, "BranchName", DbType.StringFixedLength, Branch);
               rtnval= db.ExecuteScalar(dbcmd,dbTrans).ToString();
               dbTrans.Commit();


           }
           catch(Exception)
           {
               if (dbTrans != null)
                   dbTrans.Rollback();
               throw;
           }
           finally
           {
               if (dbCon != null)
                   dbCon.Close();
           }
           return rtnval;
       }
       public string UpdateRedg(string RedgID, string Name, string fathet, string DOB, string LandMark, string Hno, string Post, string City, string District, string State, string PinCode, string SHno, string SPost, string SCity, string SDistrict, string SState, string SPinCode, string SLandMark,string PhoneNo, string Mobile, string PanNo, string Email, string Nominee, string Relation, string BankName, string AccountNo, string IFSCode, string Branch)
       {
           DbConnection dbCon = null;
           DbTransaction dbTrans = null;
           String rtnval = "";
           try
           {
               dbCon = db.CreateConnection();
               dbCon.Open();
               dbTrans = dbCon.BeginTransaction();
               DbCommand dbcmd = db.GetStoredProcCommand("UpdateRegistration_sp");
               //db.AddInParameter(dbcmd, "RefID", DbType.StringFixedLength, RefID);
               db.AddInParameter(dbcmd, "RedgID", DbType.StringFixedLength, RedgID);
               db.AddInParameter(dbcmd, "ApplicantName", DbType.StringFixedLength, Name);
               db.AddInParameter(dbcmd, "FatherOrHusband", DbType.StringFixedLength, fathet);
               db.AddInParameter(dbcmd, "DOB", DbType.StringFixedLength, DOB);
               db.AddInParameter(dbcmd, "LandMark", DbType.StringFixedLength, LandMark);
               db.AddInParameter(dbcmd, "HNo", DbType.StringFixedLength,Hno);
               db.AddInParameter(dbcmd, "Post", DbType.StringFixedLength, Post);
               db.AddInParameter(dbcmd, "City", DbType.StringFixedLength, City);
               db.AddInParameter(dbcmd, "DistrictID", DbType.StringFixedLength, District);
               db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, State);
               db.AddInParameter(dbcmd, "PinCode", DbType.StringFixedLength, PinCode);
               db.AddInParameter(dbcmd, "SHNo", DbType.StringFixedLength, SHno);
               db.AddInParameter(dbcmd, "SPost", DbType.StringFixedLength, SPost);
               db.AddInParameter(dbcmd, "SCity", DbType.StringFixedLength, SCity);
               db.AddInParameter(dbcmd, "SDistrictID", DbType.StringFixedLength, SDistrict);
               db.AddInParameter(dbcmd, "SStateID", DbType.StringFixedLength, SState);
               db.AddInParameter(dbcmd, "SPinCode", DbType.StringFixedLength, SPinCode);
               db.AddInParameter(dbcmd, "SLandMark", DbType.StringFixedLength, SLandMark);
               db.AddInParameter(dbcmd, "PhoneNo", DbType.StringFixedLength, PhoneNo);
               db.AddInParameter(dbcmd, "Mobile", DbType.StringFixedLength, Mobile);
               db.AddInParameter(dbcmd, "PANNo", DbType.StringFixedLength, PanNo);
               db.AddInParameter(dbcmd, "EmailID", DbType.StringFixedLength, Email);
               //db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
               //db.AddInParameter(dbcmd, "CPassword", DbType.StringFixedLength, CPassword);
               db.AddInParameter(dbcmd, "NomineeName", DbType.StringFixedLength, Nominee);
               db.AddInParameter(dbcmd, "Relation", DbType.StringFixedLength, Relation);
               db.AddInParameter(dbcmd, "BankName", DbType.StringFixedLength, BankName);
               db.AddInParameter(dbcmd, "AccountNo", DbType.StringFixedLength, AccountNo);
               db.AddInParameter(dbcmd, "IFSCode", DbType.StringFixedLength, IFSCode);
               db.AddInParameter(dbcmd, "BranchName", DbType.StringFixedLength, Branch);
               rtnval = db.ExecuteScalar(dbcmd,dbTrans).ToString();

           }
           catch (Exception)
           {
               if (dbTrans != null)
                   dbTrans.Rollback();
              rtnval="";
           }
           return rtnval;
       }

       public DataSet GetDistrict()
       {
           DataSet ds = new DataSet();
           try
           {
               DbCommand dbcmd = db.GetStoredProcCommand("GetDistrict_sp");
               ds = db.ExecuteDataSet(dbcmd);
           }
           catch (Exception ex)
           {
           }
           return ds;
       }
       public DataSet GetData(string UserID)
       {
           DataSet ds = new DataSet();
           try
           {
               DbCommand dbcmd = db.GetStoredProcCommand("GetUserRegistrationData_sp");
               db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
               ds = db.ExecuteDataSet(dbcmd);
           }
           catch (Exception ex)
           {
           }
           return ds;
       }
    }
}
