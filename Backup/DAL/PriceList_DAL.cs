﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
namespace DAL
{
    public class PriceList_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string SavePriceList(string ItemID, string MRP, string PurchasePrice, string SalesPrice, string TaxSystem, string UserId)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertPriceList_sp");
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbcmd, "MRP", DbType.StringFixedLength, MRP);
                db.AddInParameter(dbcmd, "PurchasePrice", DbType.StringFixedLength, PurchasePrice);
                db.AddInParameter(dbcmd, "SalesPrice", DbType.StringFixedLength, SalesPrice);
                db.AddInParameter(dbcmd, "TaxNo", DbType.StringFixedLength, TaxSystem);
                db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserId);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }

            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;

        }

        public DataSet GetTaxSystem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetTaxSystem_sp");
                //db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetItemCreation()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetItemCreation_sp");
               // db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetPLNavData(string UserId, string PLID, string Flag)
        {
            DbCommand dbcmd = db.GetStoredProcCommand("GetPLNavData_sp");
            db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserId);
            db.AddInParameter(dbcmd, "PLID", DbType.StringFixedLength, PLID );
            db.AddInParameter(dbcmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbcmd);
        }

        public string UpdatePriceList(string PLID, string ItemID, string MRP, string PurchasePrice, string SalesPrice, string TaxSystem, string UserId)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdatePriceList_sp");
                db.AddInParameter(dbcmd, "PLID", DbType.StringFixedLength, PLID );
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, ItemID );
                db.AddInParameter(dbcmd, "MRP", DbType.StringFixedLength, MRP);
                db.AddInParameter(dbcmd, "PurchasePrice", DbType.StringFixedLength, PurchasePrice);
                db.AddInParameter(dbcmd, "SalesPrice", DbType.StringFixedLength, SalesPrice);
                db.AddInParameter(dbcmd, "TaxNo", DbType.StringFixedLength, TaxSystem);
                db.AddInParameter(dbcmd, "UserId", DbType.StringFixedLength, UserId);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string SavePriceDetails(string ItemID, string BatchNo,string PurchasePrice, string MRP, string MRPMargin, string Retail, string RetailMargin, string Customer, string CustomerMargin, string VIP, string VIPMargin, string Disc, string CDisc,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("SavePriceDetails_SP");                
                db.AddInParameter(dbcmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbcmd, "BatchNo", DbType.StringFixedLength, BatchNo);
                db.AddInParameter(dbcmd, "PurchasePrice", DbType.StringFixedLength,PurchasePrice);
                db.AddInParameter(dbcmd, "MRP", DbType.StringFixedLength, MRP);
                db.AddInParameter(dbcmd, "MRPMargin", DbType.StringFixedLength, MRPMargin );
                db.AddInParameter(dbcmd, "RetailPrice", DbType.StringFixedLength, Retail );
                db.AddInParameter(dbcmd, "RetailMargin", DbType.StringFixedLength, RetailMargin );
                db.AddInParameter(dbcmd, "CustomerPrice", DbType.StringFixedLength, Customer );
                db.AddInParameter(dbcmd, "CustomerMargin", DbType.StringFixedLength, CustomerMargin );
                db.AddInParameter(dbcmd, "VIPPrice", DbType.StringFixedLength, VIP);
                db.AddInParameter(dbcmd, "VIPMargin", DbType.StringFixedLength, VIPMargin );
                db.AddInParameter(dbcmd, "Discount", DbType.StringFixedLength, Disc );
                db.AddInParameter(dbcmd, "CashDiscount", DbType.StringFixedLength, CDisc);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public DataSet GetData(string SearchItem, string GroupID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetPriceDetails_sp");
                db.AddInParameter(dbcmd, "SearchItem", DbType.StringFixedLength, SearchItem);
                db.AddInParameter(dbcmd, "StockGroupID", DbType.StringFixedLength, GroupID);
                //db.AddInParameter(dbcmd, "CompanyID", DbType.StringFixedLength, CompanyID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        //public DataSet GetCompany()
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        DbCommand dbcmd = db.GetStoredProcCommand("GetCompany_sp");
        //        db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");                
        //        ds = db.ExecuteDataSet(dbcmd);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return ds;
        //}
        public DataSet GetStockGroup()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStockGroup_sp");
                db.AddInParameter(dbcmd, "Search", DbType.StringFixedLength, "");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        public DataSet GetItemPriceDet(string ItemID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetProductPriceDetails_sp");
                db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string DeletePriceList(string ItemID, string BatchNo)
        {
            string str="";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("DeletePriceList_sp");
                db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
                db.AddInParameter(dbCmd, "BatchNo", DbType.StringFixedLength, BatchNo);
                str  = db.ExecuteScalar(dbCmd).ToString();
            }
            catch (Exception ex)
            {
            }
            return str;
        }
    }
}
        
    


    
    




