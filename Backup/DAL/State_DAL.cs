﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class State_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetCountrys()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetCountrys_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        public DataSet GetStates()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetStates_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
        public string InsertStates(string CountryID, string StateName)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String resval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertStates_sp");
                db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                db.AddInParameter(dbcmd, "StateName", DbType.StringFixedLength, StateName);
                resval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                resval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return resval;
        }

        public string UpdateState(string StateID, string StateName)
        {
           DbConnection dbCon = null;
            DbTransaction dbTrans = null;
                String rtn = "";
                try
                {
                    dbCon = db.CreateConnection();
                    dbCon.Open();
                    dbTrans = dbCon.BeginTransaction();
                    DbCommand dbcmd = db.GetStoredProcCommand("UpdateState_sp");
                    db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, StateID);
                    db.AddInParameter(dbcmd, "StateName", DbType.StringFixedLength, StateName);
                    rtn = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                    dbTrans.Commit();
                }
                catch (Exception ex)
                {
                    if (dbTrans != null)
                        dbTrans.Rollback();
                    rtn = "";
                }
                finally
                {
                    if (dbCon != null)
                        dbCon.Close();
                }
                return rtn;
            

        }

        public string DeleteState(int StateID)
        {
            {
                string rtn = "";
                try
                {
                    DbCommand dbcmd = db.GetStoredProcCommand("DeleteState_sp");
                    //db.AddInParameter(dbcmd, "ServiceID", DbType.StringFixedLength, ServiceID);
                    db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, StateID);
                    rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
                }
                catch (Exception ex)
                {
                }
                return rtn;

            }

        }
        public DataSet SelectCountry()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("SelectCountry_sp");
                //db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, Countryid);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }
    }
}
