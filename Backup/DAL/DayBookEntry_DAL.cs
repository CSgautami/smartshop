﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DAL
{
    public class DayBookEntry_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetDayBookReport(string BranchID, string DateTo)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetDayBookReport_sp");
            db.AddInParameter(dbCmd, "DateTo", DbType.StringFixedLength, DateTo);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetCELedger(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetNoCashLedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet CallFillCELedger(string BranchID, string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillCELedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetCENavData(string BranchID, string CEID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetCENavData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "CEID", DbType.StringFixedLength, CEID);
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetPENavData(string BranchID, string PEID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPENavData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "PEID", DbType.StringFixedLength, PEID);
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetRENavData(string BranchID, string REID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetRENavData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "REID", DbType.StringFixedLength, REID);
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetJENavData(string BranchID, string JEID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetJENavData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "JEREFID", DbType.StringFixedLength, JEID);
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetGVCELedgersData(string BranchID,string JID,string Voch)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetDayBookGvData_sp");
            db.AddInParameter(dbCmd, "JID", DbType.StringFixedLength, JID);
            db.AddInParameter(dbCmd, "Voch", DbType.StringFixedLength, Voch);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetPayRecLedgers(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetPayRecLedgers_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetJournalLedger(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetNoCashLedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string InsertCashEntry(string CEDate, string LedgerID, string CreditAmount, string DebitAmount, string Narration, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
                
                DbCommand dbCmd = db.GetStoredProcCommand("InsertCashEntry_sp");
                db.AddInParameter(dbCmd, "CEDate", DbType.StringFixedLength, CEDate);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
                db.AddInParameter(dbCmd, "CreditAmount", DbType.StringFixedLength, CreditAmount);
                db.AddInParameter(dbCmd, "DebitAmount", DbType.StringFixedLength, DebitAmount);
                db.AddInParameter(dbCmd, "Narration", DbType.StringFixedLength, Narration);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                rtnval = db.ExecuteScalar(dbCmd).ToString();
                
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;
        }
        public string UpdateCashEntry(string CEDate, string LedgerID, string CreditAmount, string DebitAmount, string Narration, string JournalNo, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
               
            DbCommand dbCmd = db.GetStoredProcCommand("UpdateCashEntry_sp");
            db.AddInParameter(dbCmd, "CEDate", DbType.StringFixedLength, CEDate);
            db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
            db.AddInParameter(dbCmd, "CreditAmount", DbType.StringFixedLength, CreditAmount);
            db.AddInParameter(dbCmd, "DebitAmount", DbType.StringFixedLength, DebitAmount);
            db.AddInParameter(dbCmd, "Narration", DbType.StringFixedLength, Narration);
            db.AddInParameter(dbCmd, "JournalNo", DbType.StringFixedLength, JournalNo);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;
        }


        public string InsertPayment(string PaymentDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string PaymentType, string PaymentTypeDesc, string Narration, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
                
            DbCommand dbCmd = db.GetStoredProcCommand("InsertPayment_sp");
            db.AddInParameter(dbCmd, "PaymentDate", DbType.StringFixedLength, PaymentDate);
            db.AddInParameter(dbCmd, "RefName", DbType.StringFixedLength, RefName);
            db.AddInParameter(dbCmd, "ToLedgerID", DbType.StringFixedLength, ToLedgerID);
            db.AddInParameter(dbCmd, "FromLedgerID", DbType.StringFixedLength, FromLedgerID);
            db.AddInParameter(dbCmd, "Amount", DbType.StringFixedLength, Amount);
            db.AddInParameter(dbCmd, "PaymentType", DbType.StringFixedLength, PaymentType);
            db.AddInParameter(dbCmd, "PaymentTypeDesc", DbType.StringFixedLength, PaymentTypeDesc);
            db.AddInParameter(dbCmd, "Narration", DbType.StringFixedLength, Narration);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;
        }       

        public string UpdatePayment(string PaymentNo, string PaymentDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string PaymentType, string PaymentTypeDesc, string Narration, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
                
            DbCommand dbCmd = db.GetStoredProcCommand("UpdatePayment_sp");
            db.AddInParameter(dbCmd, "PaymentNo", DbType.StringFixedLength, PaymentNo);
            db.AddInParameter(dbCmd, "PaymentDate", DbType.StringFixedLength, PaymentDate);
            db.AddInParameter(dbCmd, "RefName", DbType.StringFixedLength, RefName);
            db.AddInParameter(dbCmd, "ToLedgerID", DbType.StringFixedLength, ToLedgerID);
            db.AddInParameter(dbCmd, "FromLedgerID", DbType.StringFixedLength, FromLedgerID);
            db.AddInParameter(dbCmd, "Amount", DbType.StringFixedLength, Amount);
            db.AddInParameter(dbCmd, "PaymentType", DbType.StringFixedLength, PaymentType);
            db.AddInParameter(dbCmd, "PaymentTypeDesc", DbType.StringFixedLength, PaymentTypeDesc);
            db.AddInParameter(dbCmd, "Narration", DbType.StringFixedLength, Narration);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;     
        }

        public string InsertReceipt(string ReceiptDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string ReceiptType, string ReceiptTypeDesc, string Narration, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
                
            DbCommand dbCmd = db.GetStoredProcCommand("InsertReceipt_sp");
            db.AddInParameter(dbCmd, "ReceiptDate", DbType.StringFixedLength, ReceiptDate);
            db.AddInParameter(dbCmd, "RefName", DbType.StringFixedLength, RefName);
            db.AddInParameter(dbCmd, "ToLedgerID", DbType.StringFixedLength, ToLedgerID);
            db.AddInParameter(dbCmd, "FromLedgerID", DbType.StringFixedLength, FromLedgerID);
            db.AddInParameter(dbCmd, "Amount", DbType.StringFixedLength, Amount);
            db.AddInParameter(dbCmd, "ReceiptType", DbType.StringFixedLength, ReceiptType);
            db.AddInParameter(dbCmd, "ReceiptTypeDesc", DbType.StringFixedLength, ReceiptTypeDesc);
            db.AddInParameter(dbCmd, "Narration", DbType.StringFixedLength, Narration);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;
        }

        public string UpdateReceipt(string ReceiptNo, string ReceiptDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string ReceiptType, string ReceiptTypeDesc, string Narration, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
                
            DbCommand dbCmd = db.GetStoredProcCommand("UpdateReceipt_sp");
            db.AddInParameter(dbCmd, "ReceiptNo", DbType.StringFixedLength, ReceiptNo);
            db.AddInParameter(dbCmd, "ReceiptDate", DbType.StringFixedLength, ReceiptDate);
            db.AddInParameter(dbCmd, "RefName", DbType.StringFixedLength, RefName);
            db.AddInParameter(dbCmd, "ToLedgerID", DbType.StringFixedLength, ToLedgerID);
            db.AddInParameter(dbCmd, "FromLedgerID", DbType.StringFixedLength, FromLedgerID);
            db.AddInParameter(dbCmd, "Amount", DbType.StringFixedLength, Amount);
            db.AddInParameter(dbCmd, "ReceiptType", DbType.StringFixedLength, ReceiptType);
            db.AddInParameter(dbCmd, "ReceiptTypeDesc", DbType.StringFixedLength, ReceiptTypeDesc);
            db.AddInParameter(dbCmd, "Narration", DbType.StringFixedLength, Narration);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;       
        }

        public string InsertJournal(string XmlJournalTable, string JournalDate, string UserID, string BranchID)
        {
            
            String rtnval = "";
            try
            {
                
            DbCommand dbCmd = db.GetStoredProcCommand("InsertJournal_sp");
            db.AddInParameter(dbCmd, "XmlJournalTable", DbType.StringFixedLength, XmlJournalTable);
            db.AddInParameter(dbCmd, "JournalDate", DbType.StringFixedLength, JournalDate);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;
        }

        public string UpdateJournal(string JERefNo, string XmlJournalTable, string JournalDate, string UserID, string BranchID)
        {   
            String rtnval = "";
            try
            {
                
            DbCommand dbCmd = db.GetStoredProcCommand("UpdateJournal_sp");
            db.AddInParameter(dbCmd, "JERefNo", DbType.StringFixedLength, JERefNo);
            db.AddInParameter(dbCmd, "XmlJournalTable", DbType.StringFixedLength, XmlJournalTable);
            db.AddInParameter(dbCmd, "JournalDate", DbType.StringFixedLength, JournalDate);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            rtnval = db.ExecuteScalar(dbCmd).ToString();
            
            }
            catch (Exception ex)
            {
                
                rtnval = "";
            }
            
            return rtnval;
        }
        public string CEDelete(string RefNo,string BranchID)
        {

            DbCommand dbCmd = db.GetStoredProcCommand("DeleteCashEntry_sp");
            db.AddInParameter(dbCmd, "RefNo", DbType.StringFixedLength, RefNo);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);            
            return Convert.ToString(db.ExecuteScalar(dbCmd));
        }
        public string PaymentDelete(string RefNo,string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("DeletePayment_sp");
            db.AddInParameter(dbCmd, "PaymentNo", DbType.StringFixedLength, RefNo);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);            
            return Convert.ToString(db.ExecuteScalar(dbCmd));
        }
        public string ReceiptDelete(string RefNo, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("DeleteReceipt_sp");
            db.AddInParameter(dbCmd, "ReceiptNo", DbType.StringFixedLength, RefNo);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);            
            return Convert.ToString(db.ExecuteScalar(dbCmd));
        }
        public string JournalDelete(string RefNo,string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("DeleteJournal_sp");
            db.AddInParameter(dbCmd, "RefNo", DbType.StringFixedLength, RefNo);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);            
            return Convert.ToString(db.ExecuteScalar(dbCmd));
        }


        public DataSet CallFillPayAndRecLedger(string BranchID, string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillPayRecLedgers_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet CallFillJELedger(string BranchID, string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillNoCashLedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }
    }
}
