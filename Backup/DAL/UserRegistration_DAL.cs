﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class UserRegistration_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public string RegisterUser(string UserName, string Password, string Address, string CityID, string OtherCity, string StateID, string CountryID, string EmailID, string PhoneNos, string MobileNos)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("RegisterUser_sp");
                db.AddInParameter(dbcmd, "UserName", DbType.StringFixedLength, UserName);
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);
                db.AddInParameter(dbcmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbcmd, "CityID", DbType.StringFixedLength, CityID);
                db.AddInParameter(dbcmd, "OtherCity", DbType.StringFixedLength, OtherCity);
                db.AddInParameter(dbcmd, "StateID", DbType.StringFixedLength, StateID);
                db.AddInParameter(dbcmd, "CountryID", DbType.StringFixedLength, CountryID);
                db.AddInParameter(dbcmd, "EmailID", DbType.StringFixedLength, EmailID);
                db.AddInParameter(dbcmd, "PhoneNos", DbType.StringFixedLength, PhoneNos);
                db.AddInParameter(dbcmd, "MobileNos", DbType.StringFixedLength, MobileNos);
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;
        }

        public string CheckAuthentication(string UserName, string Password)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnVal = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("CheckAuthentication_sp");
                db.AddInParameter(dbcmd, "UserName", DbType.StringFixedLength, UserName);
                db.AddInParameter(dbcmd, "Password", DbType.StringFixedLength, Password);                
                rtnVal = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnVal = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnVal;
        }
    }
}
