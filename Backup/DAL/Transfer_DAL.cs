﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DAL
{
    public class Transfer_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");
        public DataSet GetToBranch(string BranchID,string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetToBranch_SP");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetBranchDetails(string ToBranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBranchDetails_SP");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, ToBranchID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string SaveInvoice(string hidTInvID, string hidUInvID, string InvNo, string Date, string TInvNo, string TDate, string ToBranchID, string Address, string Mode, string Days, string Note, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID, string UserID, string XmlString)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String res="";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SaveTransfers_SP");
                db.AddInParameter(dbCmd, "TInvID", DbType.StringFixedLength, hidTInvID);
                db.AddInParameter(dbCmd, "UInvID", DbType.StringFixedLength, hidUInvID);                
                db.AddInParameter(dbCmd, "TInvNo", DbType.StringFixedLength, TInvNo);
                db.AddInParameter(dbCmd, "InvDate", DbType.StringFixedLength, TDate);
                db.AddInParameter(dbCmd, "DCNo", DbType.StringFixedLength, TInvNo);
                db.AddInParameter(dbCmd, "DCDate", DbType.StringFixedLength, TDate);
                db.AddInParameter(dbCmd, "ToBranchID", DbType.StringFixedLength, ToBranchID);
                db.AddInParameter(dbCmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbCmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbCmd, "Days", DbType.StringFixedLength, Days);
                db.AddInParameter(dbCmd, "Note", DbType.StringFixedLength, Note);                
                db.AddInParameter(dbCmd, "TotalAmount", DbType.StringFixedLength, TotAmt);
                db.AddInParameter(dbCmd, "TotalDisc", DbType.StringFixedLength, TotDisc);
                db.AddInParameter(dbCmd, "TotalTax", DbType.StringFixedLength, TotTax);
                db.AddInParameter(dbCmd, "NetDisc", DbType.StringFixedLength, NetDisc);
                db.AddInParameter(dbCmd, "Frieght", DbType.StringFixedLength, Frieght);
                db.AddInParameter(dbCmd, "LULCharges", DbType.StringFixedLength, LAndUL);
                db.AddInParameter(dbCmd, "Adjustment", DbType.StringFixedLength, Adj);
                db.AddInParameter(dbCmd, "NetAmount", DbType.StringFixedLength, NetAmt);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID );
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, XmlString);
                res= Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }
        public DataSet GetData(string BranchID, string InvID, string Flag)
        {
            DataSet ds=new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTransfersData_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "TInvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
                ds= db.ExecuteDataSet(dbCmd);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }

        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetStockTransfer_SP");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "BatchNo", DbType.StringFixedLength, BatchNo);
            db.AddInParameter(dbCmd, "UDate", DbType.StringFixedLength, UDate);            
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            //db.AddInParameter(dbCmd, "VIPPrice", DbType.StringFixedLength,"");
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetBatchNo(string ItemID, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBatchNo_SP");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);                        
            return db.ExecuteDataSet(dbCmd);
        }

        public string DeleteTransfer(string InvID,string BranchID)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("DeleteTransfers_SP");
                db.AddInParameter(dbCmd, "TInvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }

        public string SaveInvoiceTransferIn(string hidTInvID, string hidUInvID, string InvNo, string InvDate, string TInvNo, string TInvDate, string ToBranchID, string Address, string Mode, string Days, string Note, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID, string UserID, string XmlString,string RefTInvID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SaveTransfersIn_SP");
                db.AddInParameter(dbCmd, "TInvID", DbType.StringFixedLength, hidTInvID);
                db.AddInParameter(dbCmd, "UInvID", DbType.StringFixedLength, hidUInvID);
                db.AddInParameter(dbCmd, "TInvNo", DbType.StringFixedLength, TInvNo);
                db.AddInParameter(dbCmd, "InvDate", DbType.StringFixedLength, InvDate);
                db.AddInParameter(dbCmd, "DCNo", DbType.StringFixedLength, TInvNo);
                db.AddInParameter(dbCmd, "DCDate", DbType.StringFixedLength, TInvDate);
                db.AddInParameter(dbCmd, "ToBranchID", DbType.StringFixedLength, ToBranchID);
                db.AddInParameter(dbCmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbCmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbCmd, "Days", DbType.StringFixedLength, Days);
                db.AddInParameter(dbCmd, "Note", DbType.StringFixedLength, Note);
                db.AddInParameter(dbCmd, "TotalAmount", DbType.StringFixedLength, TotAmt);
                db.AddInParameter(dbCmd, "TotalDisc", DbType.StringFixedLength, TotDisc);
                db.AddInParameter(dbCmd, "TotalTax", DbType.StringFixedLength, TotTax);
                db.AddInParameter(dbCmd, "NetDisc", DbType.StringFixedLength, NetDisc);
                db.AddInParameter(dbCmd, "Frieght", DbType.StringFixedLength, Frieght);
                db.AddInParameter(dbCmd, "LULCharges", DbType.StringFixedLength, LAndUL);
                db.AddInParameter(dbCmd, "Adjustment", DbType.StringFixedLength, Adj);
                db.AddInParameter(dbCmd, "NetAmount", DbType.StringFixedLength, NetAmt);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, XmlString);
                db.AddInParameter(dbCmd, "RefTInvID", DbType.StringFixedLength, RefTInvID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }

        public DataSet GetDataTransferIn(string BranchID, string InvID, string Flag)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTransfersInData_sp");
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbCmd, "TInvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
                ds = db.ExecuteDataSet(dbCmd);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }

        public string DeleteTransferIn(string InvID, string BranchID)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("DeleteTransfersIn_SP");
                db.AddInParameter(dbCmd, "TInvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }

        public DataSet GerDataTransfer(string RefTInvID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetTransfersPendingData_sp");
                db.AddInParameter(dbCmd, "RefTInvID", DbType.StringFixedLength, RefTInvID);                
                ds  = db.ExecuteDataSet(dbCmd);
            }
            catch (Exception ex)
            {
                throw;
            }
            return ds;
        }
        public DataSet CallFillItem(string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillItem_sp");
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetSearchInvoiceData(string BranchID, string TInvID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSearchTransferInvoiceData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "TInvNo", DbType.StringFixedLength, TInvID);
            return db.ExecuteDataSet(dbCmd);
        }


        public DataSet GetSearchInvoiceDat(string BranchID, string TInvID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSearchTransferINInvoiceData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "TInvNo", DbType.StringFixedLength, TInvID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetBranch(string UserID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBranch_SP");
            db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, "0");
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetInvoiceBillData(string strInvID, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTransfersInvoiceBillData_sp");
            db.AddInParameter(dbCmd, "TInvNo", DbType.StringFixedLength, strInvID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);            
            return db.ExecuteDataSet(dbCmd);
        }
    }
}
