﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace DAL
{
    public class OpeningStock_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string SaveOpeningStock(string BranchID, string Item, string BatchNo, string MRP, string Price, string Quantity,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertOpeningStock_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "ICID", DbType.StringFixedLength, Item);
                db.AddInParameter(dbcmd, "BatchNo", DbType.StringFixedLength, BatchNo);
                db.AddInParameter(dbcmd, "MRP", DbType.StringFixedLength, MRP);
                db.AddInParameter(dbcmd, "Price", DbType.StringFixedLength, Price);
                db.AddInParameter(dbcmd, "Quantity", DbType.StringFixedLength, Quantity);
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }
        public DataSet GetData(string BranchID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetOpeningStock_sp");
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetBranch(string UserID)
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetBranch_sp");
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public DataSet GetItem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetItemCreation_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }



        public string UpdateOpeningStock(string StockID, string BranchID, string Item, string BatchNo, string MRP, string Price, string Quantity,string UserID)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtnval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateOpeningStock_sp");
                db.AddInParameter(dbcmd, "StockID", DbType.StringFixedLength, StockID);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                db.AddInParameter(dbcmd, "ICID", DbType.StringFixedLength, Item);
                db.AddInParameter(dbcmd, "BatchNo", DbType.StringFixedLength, BatchNo);
                db.AddInParameter(dbcmd, "MRP", DbType.StringFixedLength, MRP);
                db.AddInParameter(dbcmd, "Price", DbType.StringFixedLength, Price);
                db.AddInParameter(dbcmd, "Quantity", DbType.StringFixedLength, Quantity);                
                db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength, UserID);
                rtnval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtnval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtnval;
        }

        public string DeleteOpeningStock(string StockID, string BranchID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteOpeningStock_sp");
                db.AddInParameter(dbcmd, "StockID", DbType.StringFixedLength, StockID);
                db.AddInParameter(dbcmd, "BranchID", DbType.StringFixedLength, BranchID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }
        public DataSet GetStockItem()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("StockItemName_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch
            {
            }
            return ds;
        }
                
    }
}
  
        
    
  

