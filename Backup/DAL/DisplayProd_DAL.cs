﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Data;


namespace DAL
{
    public class DisplayProd_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public string InsertDisplayProd(string Product, string Price)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("InsertDisplayProducts_sp");
                db.AddInParameter(dbcmd, "Product", DbType.StringFixedLength, Product);
                db.AddInParameter(dbcmd, "Price", DbType.StringFixedLength, Price);
                //db.AddInParameter(dbcmd, "UserID", DbType.StringFixedLength,UserID);
                rtval = db.ExecuteScalar(dbcmd,dbTrans).ToString();
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public DataSet GetDisplayProd()
        {
            DataSet ds = new DataSet();
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("GetDisplayProd_sp");
                ds = db.ExecuteDataSet(dbcmd);
            }
            catch (Exception ex)
            {
            }
            return ds;
        }

        public string UpdateDisplayProd(string ID, string Product, string Price)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
                
            String rtval = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbcmd = db.GetStoredProcCommand("UpdateDisplayProd_sp");
                db.AddInParameter(dbcmd, "ID", DbType.StringFixedLength, ID);
                db.AddInParameter(dbcmd, "Product", DbType.StringFixedLength, Product);
                db.AddInParameter(dbcmd, "Price", DbType.StringFixedLength, Price);
                rtval = Convert.ToString(db.ExecuteScalar(dbcmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                rtval = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return rtval;
        }

        public string DeleteDisplayProd(string ID)
        {
            String rtn = "";
            try
            {
                DbCommand dbcmd = db.GetStoredProcCommand("DeleteDisplayProd_sp");
                db.AddInParameter(dbcmd, "ID", DbType.StringFixedLength, ID);
                rtn = Convert.ToString(db.ExecuteScalar(dbcmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return rtn;
        }
    }
}
        
    

