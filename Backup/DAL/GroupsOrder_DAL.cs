﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;

namespace DAL
{
    public class GroupsOrder_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("constr");
        public DataSet GetProductList(string HGNo)
        {

            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetGroupList_sp");
                db.AddInParameter(dbCmd, "HeadGroupID", DbType.StringFixedLength, HGNo);
                return db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }

        }
        public string SaveGroupsOrder(string strXml)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String strReturn;
            strReturn = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SaveGroupsOrder_sp");
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, strXml);
                strReturn = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                strReturn = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return strReturn;
        }


        public DataSet GetHeadGroupList()
        {
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("GetHeadGroupList_sp");
                return db.ExecuteDataSet(dbCmd);
            }
            catch
            {
                throw;
            }
        }

        public string SaveHeadGroupsOrder(string strXmlString)
        {


            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String strReturn;
            strReturn = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SaveHeadGroupsOrder_sp");
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, strXmlString);
                strReturn = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();
            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                strReturn = "";
                
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return strReturn;
        }
    }
}
