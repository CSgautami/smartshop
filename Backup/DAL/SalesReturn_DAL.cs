﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DAL
{
    public class SalesReturn_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Constr");

        public DataSet GetLedgerName(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetCustomer_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetAccLedger(string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSaleAccLedger_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetBatchNo(string ItemID, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetBatchNo_SP");
            //db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID );
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID );
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetLedgerDetails(string LedgerID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetLedgerDetails_SP");
            db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);            
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetStockItem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("StockItemName_sp");
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetTaxSystem()
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTaxSystem_sp");
            //db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID);
            return db.ExecuteDataSet(dbCmd);
        }
        public string SaveInvoice(string SRInvID, string UInvID, string SRInvNo, string Date, string SRetInvNo, string SRDate, string LNo, string Address, string Mode, string Days, string Note, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,string UserID, string XmlString)
        {
            DbConnection dbCon = null;
            DbTransaction dbTrans = null;
            String res = "";
            try
            {
                dbCon = db.CreateConnection();
                dbCon.Open();
                dbTrans = dbCon.BeginTransaction();
                DbCommand dbCmd = db.GetStoredProcCommand("SaveSalesReturn_SP");
                db.AddInParameter(dbCmd, "SRInvID", DbType.StringFixedLength, SRInvID);
                db.AddInParameter(dbCmd, "UInvID", DbType.StringFixedLength, UInvID);                
                db.AddInParameter(dbCmd, "SRInvNo", DbType.StringFixedLength, SRInvNo);
                db.AddInParameter(dbCmd, "InvDate", DbType.StringFixedLength, Date);
                db.AddInParameter(dbCmd, "DCNo", DbType.StringFixedLength, SRetInvNo );
                db.AddInParameter(dbCmd, "DCDate", DbType.StringFixedLength, SRDate);
                db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LNo);
                db.AddInParameter(dbCmd, "Address", DbType.StringFixedLength, Address);
                db.AddInParameter(dbCmd, "Mode", DbType.StringFixedLength, Mode);
                db.AddInParameter(dbCmd, "Days", DbType.StringFixedLength, Days);
                db.AddInParameter(dbCmd, "Note", DbType.StringFixedLength, Note);
                db.AddInParameter(dbCmd, "ACLedgerID", DbType.StringFixedLength, AcLNo);
                db.AddInParameter(dbCmd, "TotalAmount", DbType.StringFixedLength, TotAmt);
                db.AddInParameter(dbCmd, "TotalDisc", DbType.StringFixedLength, TotDisc);
                db.AddInParameter(dbCmd, "TotalTax", DbType.StringFixedLength, TotTax);
                db.AddInParameter(dbCmd, "NetDisc", DbType.StringFixedLength, NetDisc);
                db.AddInParameter(dbCmd, "Frieght", DbType.StringFixedLength, Frieght);
                db.AddInParameter(dbCmd, "LULCharges", DbType.StringFixedLength, LAndUL);
                db.AddInParameter(dbCmd, "Adjustment", DbType.StringFixedLength, Adj);
                db.AddInParameter(dbCmd, "NetAmount", DbType.StringFixedLength, NetAmt);
                db.AddInParameter(dbCmd, "UserID", DbType.StringFixedLength, UserID );
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
                db.AddInParameter(dbCmd, "xmlString", DbType.StringFixedLength, XmlString);
                res = Convert.ToString(db.ExecuteScalar(dbCmd,dbTrans));
                dbTrans.Commit();

            }
            catch (Exception ex)
            {
                if (dbTrans != null)
                    dbTrans.Rollback();
                res = "";
            }
            finally
            {
                if (dbCon != null)
                    dbCon.Close();
            }
            return res;
        }
        public DataSet GetTax(string tax)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetTaxRate_sp");
            db.AddInParameter(dbCmd, "TaxNo", DbType.StringFixedLength, tax);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetData(string BranchID, string SRInvID, string Flag)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSalesReturnData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            db.AddInParameter(dbCmd, "SRInvID", DbType.StringFixedLength, SRInvID);            
            db.AddInParameter(dbCmd, "Flag", DbType.StringFixedLength, Flag);
            return db.ExecuteDataSet(dbCmd);
        }        
        public DataSet GetStock(string ItemID, string BatchNo, string UDate, string BranchID, string LedgerID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetStockSR_SP");
            db.AddInParameter(dbCmd, "ItemID", DbType.StringFixedLength, ItemID);
            db.AddInParameter(dbCmd, "BatchNo", DbType.StringFixedLength, BatchNo);
            db.AddInParameter(dbCmd, "UDate", DbType.StringFixedLength, UDate);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID );
            db.AddInParameter(dbCmd, "LedgerID", DbType.StringFixedLength, LedgerID);
            return db.ExecuteDataSet(dbCmd);
        }

        public string DeleteSaleReturn(string InvID,string BranchID)
        {
            string res = "";
            try
            {
                DbCommand dbCmd = db.GetStoredProcCommand("DeleteSaleReturn_SP");
                db.AddInParameter(dbCmd, "SRInvID", DbType.StringFixedLength, InvID);
                db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
                res = Convert.ToString(db.ExecuteScalar(dbCmd));
            }
            catch (Exception ex)
            {
                throw;
            }
            return res;
        }
        public DataSet CallFillLedger(string BranchID, string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillCustomer_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);            
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet CallFillItem(string prefix)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("CallFillItem_sp");
            db.AddInParameter(dbCmd, "prefix", DbType.StringFixedLength, prefix);
            return db.ExecuteDataSet(dbCmd);
        }
        public DataSet GetSearchInvoiceData(string BranchID, string SInvID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSearchSaleReturnInvoiceData_sp");
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);
            db.AddInParameter(dbCmd, "SRInvNo", DbType.StringFixedLength, SInvID);
            return db.ExecuteDataSet(dbCmd);
        }

        public DataSet GetInvoiceBillData(string strInvID, string BranchID)
        {
            DbCommand dbCmd = db.GetStoredProcCommand("GetSalesReturnInvoiceBillData_sp");
            db.AddInParameter(dbCmd, "SRInvID", DbType.StringFixedLength, strInvID);
            db.AddInParameter(dbCmd, "BranchID", DbType.StringFixedLength, BranchID);            
            return db.ExecuteDataSet(dbCmd);
        }
    }
}
