﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class SubCategory : System.Web.UI.Page
    {
        SubCategory_BLL Obj_BLL = new SubCategory_BLL();
        Category_BLL Obj_CBLL = new Category_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                hidSubCategoryID.Value = "-1";
                GetCategory();
                FillGrid();
            }
        }

        private void FillGrid()
        {
            try
            {
                DataSet ds = Obj_BLL.GetData();
                gvSCategory.DataSource = ds;
                gvSCategory.DataBind();
            }
            catch(Exception ex)
            {
            }
        }
        private void GetCategory()
        {
            ddlCategory.DataSource = Obj_CBLL.GetData();
            ddlCategory.DataTextField = "Category";
            ddlCategory.DataValueField = "CategoryID";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("Select", "0"));
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string res = Obj_BLL.SaveSubCategory(hidSubCategoryID.Value, txtSCategory.Text,ddlCategory.SelectedValue.ToString());
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error while Saving SubCategory";
                }
                if (res == "-1")
                {
                    lblMsg.Text = "SubCategory Already Exist";
                }
                if (res == "1")
                {
                    lblMsg.Text = "SubCategory Saved Successfully";
                    btnCancel_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvSCategory.PageIndex = 0;
            ddlCategory.SelectedValue = "0";
            txtSCategory.Text = "";
            ddlCategory.Focus();
            lblMsg.Text = "";
            hidSubCategoryID.Value = "-1";
            FillGrid();
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                btnCancel.Visible = true;
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidSubCategoryID.Value = ((HtmlInputHidden)grow.FindControl("hidSubCategoryID")).Value;
                txtSCategory.Text = ((Label)grow.FindControl("lblSCategory")).Text;
                ddlCategory.SelectedValue=((HtmlInputHidden)grow.FindControl("hidCategoryID")).Value;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidSubCategoryID.Value = ((HtmlInputHidden)grow.FindControl("hidSubCategoryID")).Value;
                string res = Obj_BLL.DeleteSubCategory(hidSubCategoryID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error while Deleting SubCategory";
                    return;
                }
                if (res == "1")
                {
                    lblMsg.Text = "SubCategory Deleted Successfully";                    
                    btnSave.Visible = true;
                    btnCancel.Visible = true;
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        protected void gvSCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSCategory.PageIndex = e.NewPageIndex;
            FillGrid();
        }
    }
}
