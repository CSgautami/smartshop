﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx", true);
                }
                if (!IsPostBack)
                {
                    if (Session["Type"].ToString() != "Customer" || Session["UserID"].ToString() == "0")
                        GetBranchs();
                    else
                        trDeBranch.Visible = false;
                }

            }
            catch (Exception ex)
            {

            }

        }
        private void GetBranchs()
        {
            Purchase_BLL Obj_BLL = new Purchase_BLL();
            ddlGBranch.DataSource = Obj_BLL.GetBranch(Session["UserID"].ToString());
            ddlGBranch.DataTextField = "BranchName";
            ddlGBranch.DataValueField = "BranchID";
            ddlGBranch.DataBind();
            if (ddlGBranch.Items.Count != 1)
            {
                ddlGBranch.Items.Insert(0, new ListItem("Select", "0"));
            }
            else
                Session["GBID"] = ddlGBranch.SelectedValue;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Session["GBID"] = ddlGBranch.SelectedValue;

        }
    }
}
