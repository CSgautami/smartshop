﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
namespace MyAccounts20
{
    public partial class ReportClosingStock : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
           // hidUserID.Value = Session["UserID"].ToString();
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetHeadGroup()
        {
            try
            {
                PurchaseReport_BLL  obj_BLL = new PurchaseReport_BLL ();
                return obj_BLL.GetHeadGroup().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetGroup()
        {
            try
            {
                PurchaseReport_BLL  obj_BLL = new PurchaseReport_BLL();
                return obj_BLL.GetStockGroup().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
