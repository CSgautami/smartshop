﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="listingview.aspx.cs" Inherits="MyAccounts20.listingview" Title="ListView" %>

<%@ Register Src="~/DataListPager.ascx" TagName="Pager" TagPrefix="DataList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="JS/Commonfunctions.js" type="text/javascript"></script>

    <script src="JS/Listing.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main1">
        <div>
            <div class="salwar">
                <div class="totalsalwar">
                    <span style="padding: 0px 0px 0px 8px; text-align: left;">
                        <asp:Label ID="lblHeadGroup" runat="server"></asp:Label>
                    </span>
                </div>
                <div class="listview" align="left">
                    <a href="listingview.aspx">List View</a></div>
                <div class="gridview" align="left">
                    <a href="listing.aspx">Grid View</a></div>
            </div>
        </div>
        <asp:DataList ID="dlItemDet" runat="server">
            <ItemTemplate>
                <div class="product_bg">
                    <table border="1" cellpadding="0" cellspacing="0" style="border-color: #eeeeee; border-radius: 10px;
                        -moz-border-radius: 10px; -webkit-border-radius: 10px;">
                        <tr>
                            <td>
                                <div class="product_top_div" align="left">
                                    <table border="1px" cellpadding="0" cellspacing="0" style="border-color: #eeeeee"
                                        class="product_top_div">
                                        <tr>
                                            <td>
                                                <div class="product_coad" style="border: 0px;">
                                                    <span>
                                                        <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode") %>'></asp:Label>
                                                    </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="product_name_main" style="border: 0px;">
                                                    <div class="product_name1">
                                                        <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <div class="product_bottom_div">
                                                <table border="0" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-color: #eeeeee">
                                                                <tr>
                                                                    <td>
                                                                        <div class="product_img">
                                                                            <img src="ItemImages/<%#Eval("SiNo")%>.jpg" width="96" height="96" style="border: #cccccc solid 1px;" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td colspan="100%">
                                                            <table border="1" cellpadding="0" cellspacing="0" style="border-color: #eeeeee">
                                                                <tr>
                                                                    <td align="left" valign="middle" style="border: 0px;">
                                                                        <div class="product_cost">
                                                                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td width="45%" height="35" class="product_cost1">
                                                                                            Market price / M.R.P
                                                                                        </td>
                                                                                        <td width="12%" style="text-align: center;">
                                                                                            :
                                                                                        </td>
                                                                                        <td width="21%" class="product_rs">
                                                                                            Rs.<asp:Label ID="lblMrp" runat="server" Text='<%#Eval("MRP") %>'></asp:Label>
                                                                                        </td>
                                                                                        <td width="22%" rowspan="3" align="center" style="text-align: Center;">
                                                                                            <div class="product_off">
                                                                                                &nbsp;&nbsp<asp:Label ID="lblPer" runat="server" Text='<%#Eval("Percentage")%> '></asp:Label>
                                                                                                &nbsp Off
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="35" class="product_cost1">
                                                                                            Our Price
                                                                                        </td>
                                                                                        <td width="12%" style="text-align: center">
                                                                                            :
                                                                                        </td>
                                                                                        <td class="product_rs1">
                                                                                            Rs.<asp:Label ID="lblPrice" runat="server" Text='<%#Eval("OSP") %>'></asp:Label>
                                                                                        </td>
                                                                                        <input type="hidden" value="<%#Eval("BatchNo")%>" id="hidBat<%#Eval("SiNo")%>" style="display: none;">
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="35" class="product_cost1">
                                                                                            Availability
                                                                                        </td>
                                                                                        <td width="12%" style="text-align: center">
                                                                                            :
                                                                                        </td>
                                                                                        <td>
                                                                                            <img src="MasterPageFiles2/images/no.png" width="16" height="16" border="0">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                    <td style="border: 0">
                                                                        <div class="product_apply" style="border: 0px;">
                                                                            <div class="quantity_text">
                                                                                Quantity</div>
                                                                            <div align="center" style="margin-bottom: 10px;">
                                                                                <input type="text" class="input_style" style="text-align: center" id="<%#Eval("SiNo")%>txtQty"
                                                                                    runat="server" value="" maxlength="3"></div>
                                                                            <div align="center">
                                                                                <input type="button" value="Add to Cart" onclick="AddToCart(this)" id="<%#Eval("SiNo")%>btnAdd"
                                                                                    class="add addtocart addtocart_text" style="width: 100px;">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:DataList>
        <table align="center">
            <tr>
                <td>
                    <DataList:Pager ID="dlPager" runat="server" OnPageIndexChanged="dlPager_PageIndexChagned" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <input type="hidden" id="hidUserID" runat="server" />
        <input type="hidden" id="hidTempID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="cart_main">
        <div class="cart">
            <div class="mycart" align="left">
                <span>My Cart</span>
                <img src="MasterPageFiles2/images/c.png" width="55" height="42" border="0" /></div>
            <div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="gvShopingCart"
                    runat="server">
                    <tbody>
                        <tr>
                            <td width="134" height="25" class="product_heading">
                                Item Name
                            </td>
                            <td width="5">
                            </td>
                            <td width="44" class="product_heading">
                                Price
                            </td>
                            <td width="1">
                            </td>
                            <td width="31" class="product_heading">
                                Qty
                            </td>
                            <td width="1">
                            </td>
                            <td width="72" class="product_heading">
                                Amount
                            </td>
                            <td width="1">
                            </td>
                            <td width="28" class="product_heading">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" height="10">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="5" class="product_totle">
                            Total <span class="WebRupee" style="color: #000 !important;">Rs.</span><span>:</span>
                        </td>
                        <td>
                        </td>
                        <td colspan="2" class="product_item_totle">
                            <asp:Label ID="lblTotal" runat="server" Width="70px" Text="0.00"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="product_totle">
                            Delivery Charges <span class="WebRupee" style="color: #000 !important;">Rs.</span><span>:</span>
                        </td>
                        <td>
                        </td>
                        <td colspan="2" class="product_item_totle">
                            <asp:Label ID="lblDCharges" runat="server" Width="70px" Text="0.00"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="product_totle">
                            Grand Total <span class="WebRupee" style="color: #000 !important;">Rs.</span><span>:</span>
                        </td>
                        <td>
                        </td>
                        <td colspan="2" class="product_item_totle">
                            <asp:Label ID="lblGrand" runat="server" Width="70px" Text="0.00"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                        <%--<td colspan="6" height="23" class="checkout"><a href="#"><asp:Label ID="lblCheckOut" runat="server" Width="70px" Text="Check Out" OnClientClick="return CheckOutValidation();"></asp:Label></a></td>--%>
                        <td align="right">
                            <a href="#">
                                <asp:ImageButton ID="btnCheckOut" runat="server" Width="107" Height="32" ImageUrl="MasterPageFiles/bottom-rigth.jpg"
                                    OnClick="btnCheckOut_Click" OnClientClick="return CheckOutValidation();"></asp:ImageButton>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="off">
            <img src="MasterPageFiles2/images/41.gif" width="290" height="290"></div>
        <div class="off">
            <img src="MasterPageFiles2/images/delivery.gif" width="290" height="116"></div>
        <div class="off">
            <img src="MasterPageFiles2/images/cardon.png" width="290" height="119"></div>
        <div class="off">
            <img src="MasterPageFiles2/images/coupon.png" width="290" height="248"></div>
        <div class="cart">
            <div class="add_bg">
                <div style="height: 40px; margin-bottom: 30px; padding: 0px;">
                    <div class="add_safe_text">
                        Secure &amp; Safe<br />
                        Transactions</div>
                    <div class="add_safe_text1">
                        100% secure online transactions<br />
                        <strong>256 BIT SSL ENCRYPTION</strong></div>
                </div>
                <div style="margin: 0px; padding: 0px;">
                    <div class="add_safe_text_rs">
                        Inorder to protect your card from fraudulent transactions we might redirect you
                        to a partner site, that will verify your card information, before we proceed with
                        the order-payment.<br />
                        <br />
                        Inorder to protect your card from fraudulent transactions we might redirect you
                        to a partner site, that will verify your card information, before we proceed with
                        the order-payment.</div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
