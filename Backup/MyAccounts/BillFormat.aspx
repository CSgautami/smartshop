﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillFormat.aspx.cs" Inherits="MyAccounts20.BillFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice</title>
    <style type="text/css">
     FWidth{}
        .style1
        {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="left" style="font-size:xx-small; font-weight:bold;" width="37%">           
            <tr>
                <td align="center" colspan="100%">
                    <div id="divBranchDet" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="100%" align="center" style="text-decoration:underline;">
                    INVOICE
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 10%;">
                    Bill No</td>
                <td align="left" style="width: 2%;">
                    :
                </td>
                <td id="tdBillNo" runat="server" style="width: 45%;">
                </td>
                <%--<td style="width: 50px">
                </td>--%>
                <td align="left" style="width: 5%;">
                    Date
                </td>
                <td align="left" style="width: 2%;">
                    :
                </td>
                <td id="tdDate" runat="server" align="left" style="width:36%">
                </td>
            </tr>
            <tr>              
                <td align="left" style="width: 10%;">
                    C.ID</td>
                <td align="left" style="width: 2%;">
                    :
                </td>
                <td id="tdCusID" runat="server"  align="left" colspan="4">
                </td>
                <%--<td style="width: 50px">
                </td>--%>
                
            </tr>
            <tr>
              <td align="left">
                    Name</td>
                <td align="left" style="width: 2%;">
                    :
                </td>
                <td id="tdCusName" runat="server"  align="left" colspan="4">
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" >
                    Address</td>
                <td align="left" valign="top" style="width: 2%;">
                    :
                </td>
                <td id="tdAddress" runat="server" colspan="4"  align="left">
                </td>
            </tr>
           <%-- <tr>
                <td align="left" style="width: 13%;">
                    Phone</td>
                <td align="left" style="width: 2%;">
                    :
                </td>
                <td id="tdPhone" runat="server" colspan="4" align="left">
                </td>
            </tr>--%>
            <tr>
                <td align="left" style="width: 13%;">
                    LandMark</td>
                <td align="left" style="width: 2%;">
                    :
                </td>
                <td id="tdLandMark" runat="server" colspan="4" align="left">
                </td>
            </tr>
            <tr valign="top" align="left">
                <td colspan="6"  id="divItemDet" runat="server">
                  <%--  <div id="divItemDet" runat="server">
                    </div>--%>
                </td>
            </tr>
            <%--<tr>
                <td align="left>
                    Total
                </td>
                <td id="tdTotAmt" runat="server">                    
                </td>
                <td id="tdNetDisc" runat="server">
                </td>
            </tr>--%>
            <tr>
                <td colspan="100%">
                    <table style="font-family:Verdana; font-style:Italic;" align="center">
                         <tr> 
                            <td  style="font-size:xx-small; "><b> Total MRP:</b></td>              
                            <td id="tdNetAmt" runat="server"  align ="left" style="text-align:left; font-weight:bold; font-size:medium;" valign="top">
                            </td>
                        </tr>
                         <tr>
                              <td  style="font-size:xx-small"><b> You Saved:</b></td>
                             <td id="tdMargin" runat="server" align ="left" 
                                  style="text-align:left; font-weight:bold; font-size:medium" valign="top">
                               
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:xx-small"></td>
                            <td style=" font-size:medium"><b>+</b></td>
                        </tr>
                         <tr >            
                            <td style="width:2px; font-size:medium" >
                            <b>Free Vegitable Fruits</b> </td>
                            <td  id="divCoupenAmt" runat="server"  align ="left" valign="middle" style="text-align:left; font-weight:bold; font-size:x-large; border:2px solid;">               
                            </td>                          
                        </tr>
                                          
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td id="tdline" runat="server" align="left" colspan="6"></td>
            </tr> 
            <tr>
                <td colspan="100%">
                    <table style="font-family:Verdana; font-style:Italic;" align="center">                             
                            <tr>
                                <td  style="font-size:xx-small"><b> Effective Pirce : </b></td>
                                <td id="effPrice" runat="server"  align ="left" style="text-align:left; font-weight:bold; font-size:medium"></td>
                            </tr>                         
                            <tr>
                                <td  style="font-size:xx-small"><b> Discount : </b></td>
                                <td id="tddisc" runat="server"  align ="left" style="text-align:left; font-weight:bold; font-size:medium"></td>
                            </tr>      
                    </table>
                </td>
            </tr>                            
            <tr>
                <td colspan="100%" align="center">
                   <b> THANQ VISIT AGAIN</b>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
