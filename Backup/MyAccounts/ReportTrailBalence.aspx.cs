﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;

namespace MyAccounts20
{
    public partial class ReportTrailBalence : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                GetGroups();
                GetBranch();
                chkSerialByName.Checked=true;
                rAllLedgers.Checked=true;
                txtDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
                //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);                
            }
        }

        private void GetGroups()
        {
            Reports_BLL obj_BLL = new Reports_BLL();
            chkLSTGroups.DataSource = obj_BLL.GetGroups();
            chkLSTGroups.DataTextField = "GroupName";
            chkLSTGroups.DataValueField = "GroupID";
            chkLSTGroups.DataBind();
        }
        private void GetBranch()
        {
            Purchase_BLL obj_BLL = new Purchase_BLL();
            ddlBranch.DataSource = obj_BLL.GetBranch(hidUserID.Value);
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "BranchID";
            ddlBranch.DataBind();
            if (ddlBranch.Items.Count != 1)
            {
                ddlBranch.Items.Insert(0, new ListItem("All", "0"));
            }
            if (hidGBID.Value != null)
                ddlBranch.SelectedIndex = Convert.ToInt32(hidGBID.Value);
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        void GroupsSelection()
        {
            if (rGroups.Checked == true)
            {
                chkAllGroups.Enabled = true;
                chkLSTGroups.Enabled = true;
            }
            else
            {
                chkAllGroups.Enabled = false;
                chkLSTGroups.Enabled = false;
            }
        }
        protected void rGroups_CheckedChanged(object sender, EventArgs e)
        {
            rAllLedgers.Checked = false;
            rDebtors.Checked = false;
            GroupsSelection();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            string strGIDs = "";            
            foreach (ListItem Li in chkLSTGroups.Items)
            {
                if (Li.Selected == true)
                {
                    if (strGIDs != "" && strGIDs != null)
                        strGIDs = strGIDs + " ," + Li.Value;
                    else
                        strGIDs = Li.Value;
                }
            }            
            ClientScript.RegisterStartupScript(this.GetType(), "myscript", "ShowData('" + chkSerialByName.Checked + "','" + ddlBranch.SelectedValue + "','" + rGroups.Checked + "','"+ chkOpenings.Checked +"','"+ rDebtors.Checked +"','"+ strGIDs +"','"+ chkDateWise.Checked +"','"+ txtDate.Text +"','"+ chkShowDebtors.Checked +"');", true);             
        }

        protected void chkAllGroups_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAllGroups.Checked == true)
            {
                foreach (ListItem Li in chkLSTGroups.Items)
                {
                    Li.Selected = true;
                }
            }
            else
            {
                foreach (ListItem Li in chkLSTGroups.Items)
                {
                    Li.Selected = false;
                }
            }
        }

        protected void rDebtors_CheckedChanged(object sender, EventArgs e)
        {
            rAllLedgers.Checked = false;
            rGroups.Checked = false;
            GroupsSelection();
        }

        protected void rAllLedgers_CheckedChanged(object sender, EventArgs e)
        {
            rGroups.Checked = false;
            rDebtors.Checked = false;
            GroupsSelection();
        }

        protected void chkDateWise_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDateWise.Checked == true)
                txtDate.Enabled = true;
            else
                txtDate.Enabled = false;
        }
    }
}
