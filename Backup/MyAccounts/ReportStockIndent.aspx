﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportStockIndent.aspx.cs" Inherits="MyAccounts20.ReportStockIndent" Title="Stock Indent" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/Reports/Reports.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    var ddlGroup;
    var ddlCompany;
    preid ='ctl00_ContentPlaceHolder1_'
    
    function GetControls()
    {
           
        ddlGroup = document.getElementById(preid+"ddlGroup");    
        ddlCompany=document.getElementById(preid+"ddlCompany");
        
    } 
    function SetStockIndentData()
    {
        GetControls();   
        GetStockGroup();
        GetCompany();   
    }      
      
        function btnShow_Click()
        {
//            preid='';
//            var StockGroup=document.getElementById("ddlGroup").value;        
//            var Company=document.getElementById("ddlCompany").value;
        
            window.frames["frmStockIndentReport"].location.href="Reports/StockIndentReport.aspx?StockGroup="+ ddlGroup.value +"&Company="+ddlCompany.value; 
        }
        function getXml(xmlString)
        {
            xmlObj=null;
            try {
                    var browserName = navigator.appName;
                    if (browserName == "Microsoft Internet Explorer") {
                        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                        xmlObj.async = "false";
                        xmlObj.loadXML(xmlString);
                }
                else 
                {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }    
        }
    function GetStockGroup()
    {    
        PageMethods.GetStockGroup(GetStockGroupComplete);    
    }

    function GetStockGroupComplete(res)
    {
        getXml(res);    
        ddlGroup.options.length=0;
        var opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlGroup.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupID")[0].firstChild.nodeValue;
            ddlGroup.options.add(opt);
        }    
    }
    function GetCompany()
    {
        if(UserID==null)
           GetUserID(); 
        PageMethods.GetCompany(GetCompanyComplete);    
    }

    function GetCompanyComplete(res)
    {
        getXml(res);    
        ddlCompany.options.length=0;
        var opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlCompany.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyID")[0].firstChild.nodeValue;
            ddlCompany.options.add(opt);
        }    
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
<table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
    <tr>    
        <td align="left">
             Stock Group:
        </td>
        <td align="left">
            <asp:DropDownList ID="ddlGroup" runat="server" Width="200px"></asp:DropDownList>
        </td>
        <td align="left">
            Company:
        </td>
        <td align="left">
            <asp:DropDownList ID="ddlCompany" runat="server" Width="200px"></asp:DropDownList>
        </td>
       <td align="center" >
            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="6">
            <iframe name="frmStockIndentReport" id="frmStockIndentReport" 
                style="width: 100%; height: 400px;">
            </iframe>             
        </td>
    </tr>    
</table>
</div>
<input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
