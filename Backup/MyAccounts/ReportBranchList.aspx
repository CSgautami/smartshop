﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportBranchList.aspx.cs" Inherits="MyAccounts20.ReportBranchList" Title="Report BranchList" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript">
var preid;
var UserID;
       preid ='ctl00_ContentPlaceHolder1_'        
        var xmlObj;
        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }
            
        }        
        function GetUserID()
        {
            UserID=document.getElementById(preid+"hidUserID");
        }        
        function btnShow_Click()
        {             
            window.frames["frmBranchList"].location.href="Reports/BranchListReport.aspx";
            
        }    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border:solid 1px black;">
            <tr>
                 <td style="height:10px">
                 </td>
            </tr>
            <tr>            
                <td>
                    <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                </td>
            </tr>
            <tr>
                <td style="height:5px">
                </td>
            </tr>
            <tr>                                        
                <td>                                            
                    <iframe name="frmBranchList" id="frmBranchList" style="width: 900px; height: 400px;"></iframe>
                </td>
            </tr>
        </table>
    </div>
         <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
