﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportSale.aspx.cs" Inherits="MyAccounts20.ReportSale" Title="Customer Wise Sale Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="JS/Reports/SalesReturnReport.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
    function btnShow_Click()
    {
        CustomerWiseShowClick();
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">         
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                     <tr>    
                                        <td align="left">Branch</td>
                                        <td colspan="3" align="left">
                                            <select id="ddlBranch" style="width:290px;" onchange="GetLedgerName();"></select>
                                        </td>
                                        <td>
                                            Mode:
                                        </td>
                                        <td>
                                            <select id="ddlMode" style="width:100px;" >
                                               <option value="0">All</option>
                                               <option value="Cash">Cash</option>
                                               <option value="Credit">Credit</option>                                            
                                            </select>                                            
                                        </td>
                                        <td>
                                            Customer:
                                        </td>
                                        <td>
                                            <%--<select id="ddlName" style="width:200px;" ></select>--%>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr style="display: none;">
                                                    <td>
                                                        <select style="width: 100px;" id="ddlName" onchange="GetLedgerDetails();" onkeydown="LedgerKeydown(event, this);">
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width:300px;">
                                                        <input type="text" style="width: 100px;" id="tdName" onkeydown="selectLedger(event,this);" onkeyup="FillLedger(event,this);"  onblur="ApplyLedger(this);GetLedgerDetails();"/>                                                        
                                                        <asp:Label ID="lblName" Text="" runat="server"></asp:Label>                                    
                                                    </td>
                                                </tr>
                                                <tr style="position: relative;">
                                                    <td align="left">
                                                        <div style=" display: none; width:100px; " class="divautocomplete"
                                                            id="divLedger">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            FromDate:
                                        </td>
                                        <td align="left">
                                            <input type="text" id="txtFromDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>
                                        <td>
                                            ToDate:
                                        </td>
                                        <td>
                                            <input type="text" id="txtToDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;"/>
                                        </td>                                        
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmSaleReport" id="frmSaleReport"
                                    style="width: 100%; height: 400px;">
                                    </iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" runat="server" id="hidUserID" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
