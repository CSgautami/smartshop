﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesReturnReport.aspx.cs" Inherits="MyAccounts20.Reports.SalesReturnReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SalesReturnReport</title>
    <script language="javascript">
            function callprint()
            {
                document.getElementById("trMenu").style.display="none";
                window.print();
                document.getElementById("trMenu").style.display="block";
            }
    </script>
    <style type="text/css">
        th#tdFixedHeader
        {
            position: relative;
            background-color:Gray;
            height:20px;
            color:White;
            top: expression(document.getElementById( "tdFixedHeader" ).parentElement.parentElement.parentElement.parentElement.scrollTop); /*IE5+ only*/
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr id="trMenu" runat="server" visible="false">
                <td nowrap="nowrap">
                    <img src="../Imgs/print.png" alt="" onclick="callprint();" style="cursor:pointer;" />&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnExport" runat="server" ImageUrl="~/Imgs/excel.png" OnClick="ibtnExport_Click" style="cursor:pointer;"/>
                </td>
            </tr>
            <tr>
                <td runat="server" id="tdHeading" align="center">
                </td>
            </tr>
            <tr>
                <td style="height: 20px;" align="center">
                    <asp:Label ID="lblMsg" Text="" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divTable" runat="server" style="width: 97%;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
