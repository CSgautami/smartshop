﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.IO;
using System.Text;

namespace MyAccounts20.Reports
{
    public partial class IncentivesDetReport : System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string Fdt = Request.QueryString["Fdt"];
            string Tdt = Request.QueryString["Tdt"];
            tdHeading.InnerText = "Incentive Details";
            DataSet ds = obj_BLL.GetIncentivesReportdet(Session["UserID"].ToString(), dateFormat(Fdt),dateFormat(Tdt));
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            sb.Append("<tr style='font-weight: bold;'>");
            sb.Append("<td align='center'>SNo</td>");
            sb.Append("<td align='center'>CID</td>");
            sb.Append("<td align='center'>Name</td>");
            sb.Append("<td align='center'>RefNo</td>");
            sb.Append("<td align='center'>InvNo</td>");
            sb.Append("<td align='center'>InvAmt</td>");
            sb.Append("<td align='center'>Comm</td>");
            sb.Append("</tr>");
            int cellscount;
            string EmptyVal;
            int SNo;
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    sb.Append("<tr style='font-weight: bold;'>");
                    sb.Append("<td colspan='true'> Left</td>");
                    sb.Append("</tr>");
                    cellscount = ds.Tables[0].Columns.Count;
                    EmptyVal = "&nbsp";
                    SNo = 1;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>" + SNo + "</td>");
                        for (int i = 0; i < cellscount; i++)
                        {
                            if (dr[i].ToString() == "")
                                sb.Append("<td>" + EmptyVal + "</td>");
                            else if (i == 0 || i == 2 || i == 3)
                                sb.Append("<td align='center'>" + dr[i].ToString() + "</td>");
                            else if (i == 1)
                                sb.Append("<td align='left'>" + dr[i].ToString() + "</td>");
                            else
                                sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        }
                        sb.Append("</tr>");
                        SNo++;
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    sb.Append("<tr style='font-weight: bold;'>");
                    sb.Append("<td colspan='true'> Right</td>");
                    sb.Append("</tr>");
                    cellscount = ds.Tables[1].Columns.Count;
                    EmptyVal = "&nbsp";
                    SNo = 1;
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>" + SNo + "</td>");
                        for (int i = 0; i < cellscount; i++)
                        {
                            if (dr[i].ToString() == "")
                                sb.Append("<td>" + EmptyVal + "</td>");
                            else if (i == 0 || i == 2 || i == 3)
                                sb.Append("<td align='center'>" + dr[i].ToString() + "</td>");
                            else if (i == 1)
                                sb.Append("<td align='left'>" + dr[i].ToString() + "</td>");
                            else
                                sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        }
                        sb.Append("</tr>");
                        SNo++;
                    }
                }
                sb.Append("<table>");
                divTable.InnerHtml = sb.ToString();
                trMenu.Visible = true;
            }
            else
            {
                lblMsg.Text = "No Data Found";
            }
        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }
    }
}

