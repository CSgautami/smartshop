﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using BLL;
namespace MyAccounts20.Reports
{
    public partial class OrderReport : System.Web.UI.Page
    {
        ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
        static string Status;
        static string FromDate;
        static string ToDate;
        static string BranchID;
        static string UserID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Status= Request.QueryString["Status"];
                FromDate = Request.QueryString["FromDate"];
                ToDate = Request.QueryString["ToDate"];
                BranchID = Request.QueryString["BranchID"];
                //UserID = Request.QueryString["UserID"];
                GetData(FromDate,ToDate,Status);
            }
        }
        void GetData(string FromDate,string ToDate,string Status)
        {
            if (FromDate == ToDate)
                tdHeading.InnerText = "Order Report On " + FromDate;
            else
                tdHeading.InnerText = "Order Report Between " + FromDate + " And " + ToDate;
            DataSet ds = obj_BLL.GetOrderReport(dateFormat(FromDate), dateFormat(ToDate), Status,BranchID,Session["UserID"].ToString());
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            if (ds.Tables.Count > 0)
            {
                int count = 0;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (count == 0)
                        sb.Append("<th style='width:100px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 1)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 2)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 3)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 4)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    count++;
                }
                int cellscount = ds.Tables[0].Columns.Count;
                string EmptyVal = "&nbsp";
                string strDate;
                string OrderIDs="";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    //if (dr[1].ToString() != "")
                    //    sb.Append("<tr style='cursor:pointer;' onclick='rowclick(this);' onmousemove='rowmousemove(this);' onmouseout='rowmouseout(this);' >");
                    //else 
                    sb.Append("<tr>");
                      
                    for (int i = 0; i < cellscount; i++)
                    {
                        if (dr[i].ToString() == "")
                            sb.Append("<td>" + EmptyVal + "</td>");
                        else if (i == 3)
                            sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        else if (i == 0)
                        {
                            if(dr[2].ToString()=="Pending")
                            {
                                if (OrderIDs != "")
                                    OrderIDs = OrderIDs + ",";
                                OrderIDs = OrderIDs + dr[i].ToString();
                            }
                            
                            //sb.Append("<td align='center'>" + dr[i].ToString() + "</td>");
                            sb.Append("<td style='cursor:pointer;'>");
                            if (dr[2].ToString() == "Cancelled")
                               sb.Append( dr[i].ToString() );
                            else
                                sb.Append("<a href='#' onclick='rowclick(this);'> " + dr[i].ToString() + "</a>");
                            if (dr[2].ToString() != "Delivered" && dr[2].ToString() != "Cancelled" )                            
                                sb.Append("<a href='#' onclick='CancelClick("+dr[i].ToString()+");'> Cancel </a>");
                            
                            //sb.Append("<asp:LinkButton Text='" + dr[i].ToString() + "' onclick='rowclick(this);'></asp:LinkButton>");
                            sb.Append("</td>");
                        }
                        else
                            sb.Append("<td>" + dr[i].ToString() + "</td>");
                        strDate = dr[i].ToString();
                    }
                    sb.Append("</tr>");
                }
                sb.Append("<table>");
                hidOrderIDs.Value = OrderIDs;
                divTable.InnerHtml = sb.ToString();
                trMenu.Visible = true;
            }
            else
            {
                lblMsg.Text = "No Data Found";
            }
        }
        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

        protected void btnOrderSummary_Click(object sender, EventArgs e)
        {
            GetData(FromDate, ToDate, Status);
        }

        //protected void btnOrderSummary_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("../OrderSummury.aspx?OrderID=46");
        //}
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveInProcess(string OrderIDs)
        {
            string str;
            try
            {
                ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
                str= obj_BLL.SaveInProcess(OrderIDs);
            }
            catch
            {
                str = "";
            }
            return str;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CancelClick(string OrderID)
        {
            string str;
            try
            {
                ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
                str = obj_BLL.CancelClick(OrderID);
            }
            catch
            {
                str = "";
            }
            return str;
        }
    }
}
