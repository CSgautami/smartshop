﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.IO;
using System.Text;
namespace MyAccounts20.Reports
{
    public partial class DetailSalesReport : System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string BranchID = Request.QueryString["Branch"];
            string FromDate = Request.QueryString["FromDate"];            
            string ToDate = Request.QueryString["ToDate"];
            string FromInv = Request.QueryString["FromInv"];
            string ToInv = Request.QueryString["ToInv"];
            if (FromDate == ToDate)
                tdHeading.InnerText = "Detail Sales Report On " + FromDate;
            else
                tdHeading.InnerText = "Detail Sales Report Between " + FromDate + " And " + ToDate;
            DataSet ds = obj_BLL.DetailSalesReportData(dateFormat(FromDate), dateFormat(ToDate), FromInv, ToInv, BranchID, Session["UserID"].ToString());
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            if (ds.Tables.Count > 0)
            {
                int count = 0;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (count == 0)
                        sb.Append("<th style='width:100px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else  if (count == 1)
                        sb.Append("<th style='width:100px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 2)
                        sb.Append("<th style='width:200px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 3)
                        sb.Append("<th style='width:800px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 4)
                        sb.Append("<th style='width:100px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 5)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 6)
                        sb.Append("<th style='width:200px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 7)
                        sb.Append("<th style='width:200px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 8)
                        sb.Append("<th style='width:250px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    //else if (count == 9)
                    //    sb.Append("<th style='width:250px;'>" + dc.ColumnName + "</th>");
                    else
                        sb.Append("<th>" + dc.ColumnName + "</th>");
                    count++;
                }
                int cellscount = ds.Tables[0].Columns.Count;
                string EmptyVal = "&nbsp";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sb.Append("<tr>");
                    for (int i = 0; i < cellscount; i++)
                    {
                        if (dr[i].ToString() == "")
                            sb.Append("<td>" + EmptyVal + "</td>");
                        else if (dr[i].ToString() == "0")
                            sb.Append("<td>" + EmptyVal + "</td>");
                        else if (i == 1 || i == 2 || i == 4)
                            sb.Append("<td align='center'>" + dr[i].ToString() + "</td>");
                        else if (i == 0 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8)
                            sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        else
                            sb.Append("<td>" + dr[i].ToString() + "</td>");
                    }
                    sb.Append("</tr>");               
                }
                sb.Append("</table>");
                divTable.InnerHtml = sb.ToString();
                trMenu.Visible = true;
            }
            else
            {
                lblMsg.Text = "No Data Found";
            }
        }
        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();
            }
            catch
            {

            }
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

    }
}
