﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.Text;
using System.IO;

namespace MyAccounts20.Reports
{
    public partial class FinalAccountReport : System.Web.UI.Page
    {
        Reports_BLL bllRep = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string chkDateWise = Request.QueryString["bDate"];
            string From = Request.QueryString["dateFrom"];
            string To = Request.QueryString["dateTo"];
            string rDetailed = Request.QueryString["bDetail"];
            string rTradingAccount = Request.QueryString["optTrade"];
            string rProfitandLossAc = Request.QueryString["optPAndL"];
            string rBalenceSheet = Request.QueryString["optBal"];
            
            if (From == To)
                tdHeading.InnerText = "Final Account Report On " + From;
            else
                tdHeading.InnerText = "Final Account Report Between " + From + " And " + To;
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            if (rTradingAccount == "true")
            {
                sb.Append("<tr style='font-weight:bold;'>");
                sb.Append("<td align='center' style='width:250px;'>Name Of Account</td>");
                sb.Append("<td align='center' style='width:150px;'>Amount(Rs)</td>");
                sb.Append("<td align='center' style='width:250px;'>Name Of Account</td>");
                sb.Append("<td align='center' style='width:150px;'>Amount(Rs)</td>");
                sb.Append("</tr>");
            }
            else if (rProfitandLossAc == "true")
            {
                sb.Append("<tr style='font-weight:bold;'>");
                sb.Append("<td align='center' style='width:250px;'>Expenditure</td>");
                sb.Append("<td align='center' style='width:150px;'>Amount(Rs)</td>");
                sb.Append("<td align='center' style='width:250px;'>Income</td>");
                sb.Append("<td align='center' style='width:150px;'>Amount(Rs)</td>");
                sb.Append("</tr>");
            }
            else if (rBalenceSheet == "true")
            {
                sb.Append("<tr style='font-weight:bold;'>");
                sb.Append("<td align='center' style='width:250px;'>Liabilities</td>");
                sb.Append("<td align='center' style='width:150px;'>Amount(Rs)</td>");
                sb.Append("<td align='center' style='width:250px;'>Assets</td>");
                sb.Append("<td align='center' style='width:150px;'>Amount(Rs)</td>");
                sb.Append("</tr>");
            }
            DataSet ds =bllRep.ReportFinalAccount(chkDateWise,dateFormat(From),dateFormat(To),rDetailed,rTradingAccount,rProfitandLossAc,rBalenceSheet);
            if (ds.Tables.Count > 0)
            {
                int cellscount = ds.Tables[0].Columns.Count;
                string emptyval = "&nbsp;";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sb.Append("<tr>");
                    for (int i = 0; i < cellscount; i++)
                    {

                        if (dr[i].ToString() == "")
                            sb.Append("<td>" + emptyval + "</td>");
                        else if (i == 1 || i == 3)
                            sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        else
                            sb.Append("<td>" + dr[i].ToString() + "</td>");
                    }
                    sb.Append("</tr>");

                }
                sb.Append("</table>");
                divTable.InnerHtml = sb.ToString();
                trMenu.Visible = true;
            }
            else
            {
                lblMsg.Text = "No Data Found";
            }
        }
        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

    }

}

