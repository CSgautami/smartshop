﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.Text;
using System.IO;

namespace MyAccounts20.Reports
{
    public partial class ZoneAc : System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string ZoneID = Request.QueryString["ZoneID"];
            string dtFrom = Request.QueryString["FromDate"];
            string dtTo = Request.QueryString["ToDate"];
            string chkPay = Request.QueryString["Pay"];
            tdHeading.InnerText = "Zone Account";
            DataSet ds = obj_BLL.GetZoneAc(ZoneID, dateFormat(dtFrom), dateFormat(dtTo), chkPay);
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            if (ds.Tables.Count > 0)
            {
                int count = 0;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (count == 0)
                        sb.Append("<th style='width:100px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 1)
                        sb.Append("<th style='width:300px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 2)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 3)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 4)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 5)
                        sb.Append("<th style='width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else
                        sb.Append("<th>" + dc.ColumnName + "</th>");
                    count++;
                }
                int cellscount = ds.Tables[0].Columns.Count;
                string EmptyVal = "&nbsp";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sb.Append("<tr>");
                    for (int i = 0; i < cellscount; i++)
                    {
                        if (dr[i].ToString() == "")
                            sb.Append("<td>" + EmptyVal + "</td>");
                        else if (i == 0 || i == 2 || i == 3 || i == 4 || i == 5)
                            sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        else
                            sb.Append("<td>" + dr[i].ToString() + "</td>");
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                divTable.InnerHtml = sb.ToString();
                trMenu.Visible = true;
            }
            else
            {
                lblMsg.Text = "No Data Found";
            }
        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }
    }
}

