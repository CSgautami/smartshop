﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.Text;
using System.IO;

namespace MyAccounts20.Reports
{
    public partial class MyAccount : System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string LedgerID = Request.QueryString["LedgerID"];
                string dtFrom = Request.QueryString["FromDate"];
                string dtTo = Request.QueryString["ToDate"];
                string chkPay = Request.QueryString["Pay"];
                bool bCustomer = false;
                if (Session["Type"].ToString() == "Customer")
                {
                    LedgerID = Session["UserID"].ToString();
                    bCustomer = true;
                }
                tdHeading.InnerText = "MY Account";
                StringBuilder sb = new StringBuilder();
                sb.Append("<table id='tblAc' rules='all' border='1' cellspacing='0' align='center'  style='font-size:small' width='100%'> ");
                sb.Append("<tr style='font-weight: bold;'>");
                sb.Append("<td rowspan='3' align='center'>CID</td>");
                sb.Append("<td rowspan='3' align='center'>CName</td>");
                sb.Append("<td colspan='4' align='center'>Total</td>");
                sb.Append("<td colspan='2' align='center'>Paid</td>");
                sb.Append("<td colspan='4' align='center'>This Week</td>");
                sb.Append("<td rowspan='3' align='center'>Consider</td>");
                sb.Append("<td rowspan='3' align='center'>To Pay</td>");
                sb.Append("</tr>");
                sb.Append("<tr style='font-weight: bold;'>");
                sb.Append("<td colspan='2' align='center'>Sales</td>");
                sb.Append("<td colspan='2' align='center'>Commission</td>");
                sb.Append("<td colspan='2' align='center'>Commission</td>");
                sb.Append("<td colspan='2' align='center'>Sales</td>");
                sb.Append("<td colspan='2' align='center'>Commission</td>");
                sb.Append("</tr>");
                sb.Append("<tr style='font-weight: bold;'>");
                sb.Append("<td align='center'>Left</td>");
                sb.Append("<td align='center'>Right</td>");
                sb.Append("<td align='center'>Left</td>");
                sb.Append("<td align='center'>Right</td>");
                sb.Append("<td align='center'>Left</td>");
                sb.Append("<td align='center'>Right</td>");
                sb.Append("<td align='center'>Left</td>");
                sb.Append("<td align='center'>Right</td>");
                sb.Append("<td align='center'>Left</td>");
                sb.Append("<td align='center'>Right</td>");
                sb.Append("</tr>");
                DataSet ds = new DataSet();
                ds = obj_BLL.GetMembersMast(LedgerID, chkPay, bCustomer.ToString());
                //ds.Clear;
                DataSet dsmain = new DataSet();
                DataSet dssub = new DataSet();
                //ds.Tables[0].
                //DataTable dt = new DataTable();
                //dt.Columns.Add("YourID", typeof(int));
                //dt.Columns.Add("Amount", typeof(decimal));            
                int count = 0;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    LedgerID = dr["LedgerID"].ToString();
                    if (count == 0)
                        dsmain = obj_BLL.GetMyAccount(LedgerID, dateFormat(dtFrom), dateFormat(dtTo), chkPay, bCustomer.ToString());
                    else
                    {
                        dssub = obj_BLL.GetMyAccount(LedgerID, dateFormat(dtFrom), dateFormat(dtTo), chkPay, bCustomer.ToString());
                        if (dssub.Tables.Count > 0)
                        {
                            foreach (DataRow drow in dssub.Tables[0].Rows)
                                dsmain.Tables[0].ImportRow(drow);
                        }
                    }
                    count++;
                }
                if (dsmain.Tables.Count > 0)
                {
                    //foreach (DataColumn dc in ds.Tables[0].Columns)
                    //    sb.Append("<th>" + dc.ColumnName + "</th>");
                    int cellscount = dsmain.Tables[0].Columns.Count-2;
                    foreach (DataRow dr in dsmain.Tables[0].Rows)
                    {
                        sb.Append("<tr>");
                        for (int i = 0; i < cellscount; i++)
                            sb.Append("<td>" + dr[i].ToString() + "</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    divTable.InnerHtml = sb.ToString();
                    trMenu.Visible = true;
                }
                DataSet dsSession = new DataSet();
                if (dsmain.Tables[0].Rows.Count > 0)
                {
                    dsSession = dsmain;
                    count = dsSession.Tables[0].Columns.Count - 1;
                    //foreach (DataColumn dc in dsSession.Tables[0].Columns)
                    //{
                    //    if (dc.ColumnName != "Amount" && dc.ColumnName != "CID")
                    //        dsSession.Tables["Table"].Columns.Remove(dsSession.Tables["Table"].Columns[dc.ColumnName]);
                    //}
                    for (int i = count; i > 0; i--)
                    {
                        if (i != 0 && i != count && i!=14 && i!=13 && i!=1)
                            dsSession.Tables["Table"].Columns.Remove(dsSession.Tables["Table"].Columns[i]);
                    }
                    Session["CustomerPaySlip"] = dsSession.GetXml();
                    count = dsSession.Tables[0].Columns.Count - 1;
                    for (int i = count; i > 0; i--)
                    {
                        if (i != 0 && i != count-2)
                            dsSession.Tables["Table"].Columns.Remove(dsSession.Tables["Table"].Columns[i]);
                    }
                    Session["CustomerPay"] = dsSession.GetXml();
                }
            }
            catch (Exception ex)
            {
            }
        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }
    }
}
