﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class VatType : System.Web.UI.Page
    {
        VatType_BLL Obj_BLL = new VatType_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            string strID = Request.QueryString["ID"];
            if (!IsPostBack)
            {
                txtVatType.Focus();
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
                if (strID != null && strID != "")
                {
                    btnUpdate.Visible = false;
                    btnCancel.Visible = false;
                    tdCancel.Visible = false;
                    strID = "";
                }
            }
        }
        void FillGrid()
        {
            try
            {
                VatType_BLL Obj_BLL = new VatType_BLL();

                DataSet ds = new DataSet();
                ds = Obj_BLL.GetVatTypeDesc();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvVatType.DataSource = ds;
                        gvVatType.DataBind();
                        gvVatType.Rows[0].Cells[1].Text = "";
                    }
                    else
                    {
                        gvVatType.DataSource = ds;
                        gvVatType.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidVTypeID.Value = ((HtmlInputHidden)grow.FindControl("hidVTypeID")).Value;
                txtVatType.Text = ((Label)grow.FindControl("lblVTypeDesc")).Text;
                txtVatType.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidVTypeID.Value = ((HtmlInputHidden)grow.FindControl("hidVTypeID")).Value;
                string res = Obj_BLL.DeleteVatTypeDesc(hidVTypeID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting VatType";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "VatType Deleted Successfully";
                    txtVatType.Text = "";
                    txtVatType.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        protected void gvVatType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVatType.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertVatTypeDesc(txtVatType.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving VatType";


            }
            else if (res == "-1")
            {
                lblMsg.Text = "VatType already existed";
                txtVatType.Focus();

            }
            else if (res == "1")
            {
                lblMsg.Text = "VatType saved successfully";
                FillGrid();
                txtVatType.Text = "";
                txtVatType.Focus();
            }


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateVatTypeDesc(hidVTypeID.Value, txtVatType.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating VatType";
                txtVatType.Focus();
                return;
            }
            else if (res == "-1")
            {
                lblMsg.Text = "VatType already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "VatType Updated successfully";
                FillGrid();
                txtVatType.Text = "";
                txtVatType.Focus();
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnCancel.Visible = true;
                FillGrid();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvVatType.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtVatType.Text = "";
            lblMsg.Text = "";
        }
    }
}
