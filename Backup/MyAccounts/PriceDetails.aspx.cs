﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;
namespace MyAccounts20
{
    public partial class PriceDetails : GlobalPage
    {
        PriceList_BLL obj_BLL = new PriceList_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                FillData();
                //ddlItemCode.Focus();
                hidUserID.Value = Session["UserID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);                
            }
        }
        public void FillData()
        {
            DataSet ds;
            //ds=obj_BLL.GetCompany();
            //ddlCompany.DataSource = ds;
            //ddlCompany.DataTextField = "CompanyName";
            //ddlCompany.DataValueField = "CompanyID";
            //ddlCompany.DataBind();
            //ddlCompany.Items.Insert(0, new ListItem("Select", "0"));

            //ds.Clear();
            ds = obj_BLL.GetStockGroup();
            ddlStockGroup.DataSource = ds;
            ddlStockGroup.DataTextField = "StockGroupName";
            ddlStockGroup.DataValueField = "StockGroupID";
            ddlStockGroup.DataBind();
            ddlStockGroup.Items.Insert(0, new ListItem("Select", "0"));

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem()
        {
            try
            {
                PurchaseReport_BLL Obj_BLL = new PurchaseReport_BLL();
                return Obj_BLL.GetStockItem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SavePriceDetails(string ItemID, string BatchNo, string PurchasePrice, string MRP, string MRPMargin, string Retail, string RetailMargin, string Customer, string CustomerMargin, string VIP, string VIPMargin, string Disc, string CDisc,string UserID)
        {
            try
            {
                string res;
                PriceList_BLL obj_BLL = new PriceList_BLL();
                res = obj_BLL.SavePriceDetails(ItemID , BatchNo, PurchasePrice, MRP , MRPMargin , Retail , RetailMargin ,Customer , CustomerMargin , VIP , VIPMargin , Disc , CDisc,UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string SearchItem,string StockGroupID)
        {
            try
            {
                PriceList_BLL  obj_Bll = new PriceList_BLL();
                return obj_Bll.GetData(SearchItem,StockGroupID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemName(string ItemID)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetItemName(ItemID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemPriceDet(string ItemID)
        {
            try
            {
                PriceList_BLL obj_BLL = new PriceList_BLL();
                return obj_BLL.GetItemPriceDet(ItemID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeletePriceList(string ItemID,string BatchNo)
        {
            try
            {
                PriceList_BLL obj_BLL = new PriceList_BLL();
                return obj_BLL.DeletePriceList(ItemID,BatchNo);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
