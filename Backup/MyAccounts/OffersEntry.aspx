﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="OffersEntry.aspx.cs" Inherits="MyAccounts20.OffersEntry" Title="Offers Entry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
var preid='ctl00_ContentPlaceHolder1_';
    String.prototype.trim=function() {
          return this.replace(/^\s*/, "").replace(/\s*$/, "");
          }
          
          function ValidateSave()
          {
              try
              {
                var Item = document.getElementById(preid + "ddlItem");
                var HeadGroupID=document.getElementById(preid + "ddlHeadGroup");
                var Percentage=document.getElementById(preid + "txtPercent");
                if (HeadGroupID.value=="0")
                {        
                    HeadGroupID.focus();
                    alert("Select HaedGroup");
                    return false;   
                }   
                if (Item.value=="0")
                {        
                    Item.focus();
                    alert("Select Item");
                    return false;   
                }    
                if(Percentage.value.trim()=="")
                {
                 alert("Enter Percentage");
                 Percentage.focus();
                 return false;
                }
                  return true;
                   }
                  catch(err)
                  {
                     return false;
                  }
               }
                function ConfirmEdit()
                {
                    if(!confirm("Do you want to Edit?"))
                    return false;
                }
                function ConfirmDelete()
                {
                    if(! confirm("Do you want to Delete?"))
                    return false;
                } 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 800px;">
            <tr>
                <td colspan="100%">
                    &nbsp
                </td>
            </tr>
            
            <tr>
                <td colspan="100%" style="height:10px;" >  
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:GridView ID="gvOffers" runat="server" AutoGenerateColumns="false" CssClass="grid"
                        PageSize="10" AllowPaging="true" Font-Size="Small" OnPageIndexChanging="gvOffers_PageIndexChanging" Width="800px">
                        <Columns>
                            <asp:TemplateField HeaderText="HeadGroup">
                                <ItemTemplate>                                    
                                    <input type="hidden" runat="server" id="hidHeadGroupID" value='<%#Eval("HeadGroupID")%>' />
                                    <asp:Label ID="lblHead" runat="server" style="text-align:left" Width="120px" Text='<%#Eval("HeadGroupName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Items">
                                <ItemTemplate>                                    
                                    <input type="hidden" runat="server" id="hidICID" value='<%#Eval("ICID")%>' />
                                    <asp:Label ID="lblItem" runat="server" style="text-align:left"  Width="250px" Text='<%#Eval("StockItemName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Percentage">
                                <ItemTemplate>
                                    <input type="hidden" runat="server" id="hidOfersID" value='<%#Eval("OfferID")%>' />
                                    <asp:Label ID="lblPercentage" runat="server" style="text-align:right" Text='<%#Eval("Percentage") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();" OnClick="lbtnEdit_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();" OnClick="lbtnDelete_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table align="center" width="500px">
            <tr>
                <td colspan="100%"  height="5px">
                    
                </td>
            </tr>
            <tr>
                 <td align="left">
                        HeadGroup
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlHeadGroup" runat="server" Width="200px" 
                            onselectedindexchanged="ddlHeadGroup_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                 </td>
                 <td align="left">
                        Items
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlItem" runat="server" Width="250px"></asp:DropDownList>
                 </td>
                 <td>
                   Percentage
                 </td>
                 <td>
                     <asp:TextBox ID="txtPercent" runat="server" Width="100px" style="text-align:right"></asp:TextBox>
                 </td>
            </tr>   
            <tr>
            
                <td colspan="100%" align="center">
                    <table>
                        <tr>
                       
                            <td>
                                 <asp:Button ID="btnSave" runat="server" Text="Save" 
                                     OnClientClick="return ValidateSave();" onclick="btnSave_Click"/>
                            </td>
                            <td>
                                 <asp:Button ID="btnUpdate" runat="server" Text="Update" 
                                     OnClientClick="return ValidateSave();" Visible="false" 
                                     onclick="btnUpdate_Click"/>
                            </td>
                            <td runat="server" id="tdCancel">
                                 <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td colspan="100%" align="center">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" runat="server" id="hidOfersID" />
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
