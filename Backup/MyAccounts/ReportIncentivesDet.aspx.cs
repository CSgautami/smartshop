﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace MyAccounts20
{
    public partial class ReportIncentivesDet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
            // hidUserID.Value = Session["UserID"].ToString();
            if (!IsPostBack)
            {
                string strPeriod = Request.QueryString["Period"];
                //strActPage = strActPage.Substring(0, strActPage.IndexOf('?'));
                hidFDate.Value = strPeriod.Substring(0, strPeriod.IndexOf(" - "));
                hidTDate.Value = strPeriod.Substring(strPeriod.IndexOf(" - ")+3);
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            }
        }
    }
}
