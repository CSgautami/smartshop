﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.Collections.Generic;

namespace MyAccounts20
{
    public partial class DayBookEntry : GlobalPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    trNav.Visible = false;

                //hidDate.Value = DateTime.Today.ToString("dd-MM-yyyy");
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
            }
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetDayBookReport(string BranchID, string DateTo)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetDayBookReport(BranchID, dateFormat(DateTo)).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCELedger(string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetCELedger(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetGVCELedger(string BranchID, string JournalID, string Voch)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetGVCELedgersData(BranchID, JournalID, Voch).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCENavData(string BranchID, string CEID, string Flag)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetCENavData(BranchID, CEID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPENavData(string BranchID, string PEID, string Flag)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetPENavData(BranchID, PEID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetRENavData(string BranchID, string REID, string Flag)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetRENavData(BranchID, REID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetJENavData(string BranchID, string JEID, string Flag)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetJENavData(BranchID, JEID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPayRecLedgers(string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetPayRecLedgers(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetJournalLedger(string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetJournalLedger(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string InsertCashEntry(string CEDate, string LedgerID, string DebitAmount, string CreditAmount, string Narration, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                if (string.IsNullOrEmpty(CreditAmount))
                    CreditAmount = null;
                if (string.IsNullOrEmpty(DebitAmount))
                    DebitAmount = null;
                return bll_DayBookEntry.InsertCashEntry(dateFormat(CEDate), LedgerID, CreditAmount, DebitAmount, Narration, UserID, BranchID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdateCashEntry(string CEDate, string LedgerID, string DebitAmount, string CreditAmount, string Narration, string JournalNo, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                if (string.IsNullOrEmpty(CreditAmount))
                    CreditAmount = null;
                if (string.IsNullOrEmpty(DebitAmount))
                    DebitAmount = null;
                return bll_DayBookEntry.UpdateCashEntry(dateFormat(CEDate), LedgerID, CreditAmount, DebitAmount, Narration, JournalNo, UserID, BranchID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string InsertPayment(string PaymentDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string PaymentType, string PaymentTypeDesc, string Narration, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.InsertPayment(dateFormat(PaymentDate), RefName, ToLedgerID, FromLedgerID, Amount, PaymentType, PaymentTypeDesc, Narration, UserID, BranchID);
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdatePayment(string PaymentNo, string PaymentDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string PaymentType, string PaymentTypeDesc, string Narration, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.UpdatePayment(PaymentNo, dateFormat(PaymentDate), RefName, ToLedgerID, FromLedgerID, Amount, PaymentType, PaymentTypeDesc, Narration, UserID, BranchID);
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string InsertReceipt(string ReceiptDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string ReceiptType, string ReceiptTypeDesc, string Narration, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.InsertReceipt(dateFormat(ReceiptDate), RefName, ToLedgerID, FromLedgerID, Amount, ReceiptType, ReceiptTypeDesc, Narration, UserID, BranchID);
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdateReceipt(string ReceiptNo, string ReceiptDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string ReceiptType, string ReceiptTypeDesc, string Narration, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.UpdateReceipt(ReceiptNo, dateFormat(ReceiptDate), RefName, ToLedgerID, FromLedgerID, Amount, ReceiptType, ReceiptTypeDesc, Narration, UserID, BranchID);
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string InsertJournal(string XmlJournalTable, string JournalDate, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.InsertJournal(XmlJournalTable, dateFormat(JournalDate), UserID, BranchID);
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdateJournal(string JERefNo, string XmlJournalTable, string JournalDate, string UserID, string BranchID)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.UpdateJournal(JERefNo, XmlJournalTable, dateFormat(JournalDate), UserID, BranchID);
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CEDelete(string RefNo, string BranchID)
        {
            try
            {
                DayBookEntry_BLL obj_BLL = new DayBookEntry_BLL();
                return obj_BLL.CEDelete(RefNo, BranchID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string PaymentDelete(string RefNo, string BranchID)
        {
            try
            {
                DayBookEntry_BLL obj_BLL = new DayBookEntry_BLL();
                return obj_BLL.PaymentDelete(RefNo, BranchID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string ReceiptDelete(string RefNo, string BranchID)
        {
            try
            {
                DayBookEntry_BLL obj_BLL = new DayBookEntry_BLL();
                return obj_BLL.ReceiptDelete(RefNo, BranchID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string JournalDelete(string RefNo, string BranchID)
        {
            try
            {
                DayBookEntry_BLL obj_BLL = new DayBookEntry_BLL();
                return obj_BLL.JournalDelete(RefNo, BranchID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillCELedger(string BranchID, string prefix)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.CallFillCELedger(BranchID, prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillPayAndRecLedger(string BranchID, string prefix)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.CallFillPayAndRecLedger(BranchID, prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillJELedger(string BranchID, string prefix)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.CallFillJELedger(BranchID, prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
