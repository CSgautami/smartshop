﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="ReportShippingAddress.aspx.cs" Inherits="MyAccounts20.ReportShippingAddress"
    Title="Shipping Address" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
    var preid;
    var Customer;
    var Name;
    
    preid ='ctl00_ContentPlaceHolder1_'
        var xmlObj;
        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }
            
        }
        function GetControls()
        {
           Customer=document.getElementById(preid+"txtCustomer");
           Name=document.getElementById(preid+"txtName");           
        }
        function ConformEdit()
        {
           if (! confirm("Do you want to Edit?") )             
           return false;    
        }
         
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table>
            <tr>
                <td valign="top" width="734">
                    <table cellspacing="0" cellpadding="0" width="730" align="left" border="0">
                        <tbody>
                            <tr>
                                <td valign="top" align="left" height="2">
                                </td>
                            </tr>
                            <tr>
                                <td class="rectangleborder" valign="middle" align="left" bgcolor="#cccccc">
                                    <table cellspacing="0" cellpadding="0" width="700" border="0">
                                        <tbody>
                                            <tr>
                                                <td width="113">
                                                    <h6 class="balck">
                                                        <strong>Shipping Address </strong>
                                                    </h6>
                                                </td>
                                                <td align="left" width="587">
                                                    <table cellspacing="2" cellpadding="0" width="400" align="left">
                                                        <tbody>
                                                            <tr>
                                                                <td class="bodytext">
                                                                    <strong>Customer Id</strong>
                                                                </td>
                                                                <td align="center">
                                                                    <strong>:</strong>
                                                                </td>
                                                                <td>
                                                                    <asp:textbox id="txtCustomer" runat="server" width="50px"></asp:textbox>
                                                                </td>
                                                                <td class="bodytext">
                                                                    <strong>Name</strong>
                                                                </td>
                                                                <td align="center">
                                                                    <strong>:</strong>
                                                                </td>
                                                                <td>
                                                                    <asp:textbox id="txtName" runat="server" width="150px"></asp:textbox>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left" bgcolor="#ffffff">
                                    <table cellspacing="0" cellpadding="0" width="730" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="zixegreen" width="472" bgcolor="#cccccc">
                                                    <asp:radiobutton id="rAddress" runat="server"></asp:radiobutton>
                                                    <strong>Address</strong><a class="samrtprize" href="http://websitebulksms.com/shs/shipping%20Adress.html#">
                                                        <strong class="samrtprize5">(Add New)</strong></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="zixegreen3" bgcolor="#f3f3f3">
                                                    List of Address's
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="border-left-right-bg">
                                                    <table width="715" border="0" align="center" cellpadding="0" cellspacing="2">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:gridview id="gvAddress" runat="server" autogeneratecolumns="false">
                                                    <Columns>                                                                             
                                                        <asp:TemplateField>                    
                                                            <HeaderTemplate>
                                                                <table width="34px" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                    <tr>
                                                                        <td width="14" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                            SL.No
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table bgcolor="#f6fef0" class="zixe4" width="121px">
                                                                    <tr>
                                                                        <td bgcolor="#f6fef0" class="zixe4">
                                                                            
                                                                            <asp:Label ID="lblSNO" runat="server" Text=' <%#Eval("SNO")%>'></asp:Label>    
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                                                                                                       
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table width="46px" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                    <tr>
                                                                        <td width="55" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                            SID
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="46px" bgcolor="#f6fef0" class="zixe4">
                                                                    <tr>
                                                                        <td width="46px" bgcolor="#f6fef0" class="zixe4">
                                                                            <asp:Label ID="lblSID" runat="server" Text=' <%#Eval("SID")%>'></asp:Label>            
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>                    
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table width="33px" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                    <tr>
                                                                        <td width="163" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                            Name
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table bgcolor="#f6fef0" class="zixe4" width="33px">
                                                                    <tr>
                                                                        <td bgcolor="#f6fef0" class="zixe4" width="33px">
                                                                            <asp:Label ID="lblName" runat="server" Text=' <%#Eval("RegisName")%>'></asp:Label>    
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>                    
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table align="center" bgcolor="#ecebeb" class="zixegreen2" width="25">
                                                                    <tr>
                                                                        <td align="center" bgcolor="#ecebeb" class="zixegreen2" width="95">
                                                                            Rec. MobileNo
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                    <tr>
                                                                        <td width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                            <asp:Label ID="lblMobileNo" runat="server" Text=' <%#Eval("MobileNo")%>'></asp:Label>    
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>                    
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table align="center" bgcolor="#ecebeb" class="zixegreen2" width="25">
                                                                    <tr>
                                                                        <td align="center" bgcolor="#ecebeb" class="zixegreen2" width="169">
                                                                            Address
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                    <tr>
                                                                        <td width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                            <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>    
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>                    
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table align="center" bgcolor="#ecebeb" class="zixegreen2" width="25">
                                                                    <tr>
                                                                        <td align="center" bgcolor="#ecebeb" class="zixegreen2" width="77">
                                                                            AreaName
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                    <tr>
                                                                        <td width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                            <input type="hidden" runat="server" id="hidAreaID" value='<% #Eval("AreaID") %>'/>
                                                                            <asp:Label ID="lblAreaName" runat="server" Text=' <%#Eval("AreaCentre")%>'></asp:Label>    
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>                    
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <table align="center" bgcolor="#ecebeb" class="zixegreen2" width="25">
                                                                    <tr>
                                                                        <td align="center" bgcolor="#ecebeb" class="zixegreen2" width="78">
                                                                            City
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                    <tr>
                                                                        <td width="30px" bgcolor="#f6fef0" class="zixe4">
                                                                            <input type="hidden" runat="server" id="hidCityID" value='<% #Eval("CityID") %>'/>
                                                                            <asp:Label ID="lblCity" runat="server" Text=' <%#Eval("CityName")%>'></asp:Label>    
                                                                        </td>
                                                                    </tr>
                                                                </table>                                                                                                        
                                                            </ItemTemplate>                    
                                                        </asp:TemplateField>
                                                        
                                                        <asp:TemplateField>
                                                          <HeaderTemplate>
                                                                <table align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                    <tr>
                                                                        <td align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>                                                                  
                                                                <table>
                                                                    <tr>
                                                                        <td class="red" align="center" bgcolor="#f6fef0" height="25">
                                                                        <asp:LinkButton id="lbtnEdit" runat="server" class="red" text="Edit" OnClientClick ="return ConfirmEdit();"></asp:LinkButton>
                                                                        
                                                                        </td>
                                                                    </tr>
                                                                </table>                                
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>                
                                                </asp:gridview>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                <%--<td valign="top" align="left">
                                <table cellspacing="2" cellpadding="0" width="715" align="center" border="0">
                                <tbody>
                                <tr>
                                <td class="zixegreen2" align="center" width="34" bgcolor="#ecebeb">SL.<br />No</td>
                                <td class="zixegreen2" align="center" width="55" bgcolor="#ecebeb">SID</td>
                                <td class="zixegreen2" align="center" width="163" bgcolor="#ecebeb">Name</td>
                                <td class="zixegreen2" align="center" width="95" bgcolor="#ecebeb">Rec. MobileNo</td>
                                <td class="zixegreen2" align="center" width="169" bgcolor="#ecebeb">Address</td>
                                <td class="zixegreen2" align="center" width="77" bgcolor="#ecebeb">AreaName</td>
                                <td class="zixegreen2" align="center" width="78" bgcolor="#ecebeb">City</td>
                                <td class="zixegreen2" align="center" width="26" bgcolor="#ecebeb" height="25">&nbsp;</td></tr>
                                <tr>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="red" align="center" bgcolor="#f6fef0" height="25"><a class="red" href="http://websitebulksms.com/shs/shipping%20Adress.html#">Edit</a></td></tr>
                                <tr>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="zixe4" bgcolor="#f6fef0">&nbsp;</td>
                                <td class="red" align="center" bgcolor="#f6fef0" height="25"><a class="red" href="http://websitebulksms.com/shs/shipping%20Adress.html#">Edit</a></td></tr></tbody></table></td></tr>--%>
                                            <tr>
                                                <td class="rectangleborder" valign="top" align="left" bgcolor="#cccccc" height="25">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
