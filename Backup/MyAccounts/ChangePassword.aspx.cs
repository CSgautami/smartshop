﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        Login_BLL obj_BLL = new Login_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                GetNameID();
            }
        }
        void GetNameID()
        {
            DataSet ds = new DataSet();
            ds= obj_BLL.GetNameID(Session["UserID"].ToString());
            txtCustomer.Text = ds.Tables[0].Rows[0]["Name"].ToString();
            txtCustomerID.Text = ds.Tables[0].Rows[0]["CID"].ToString();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res;
            res = obj_BLL.UpdatePassword(txtNPassword.Text,Session["UserID"].ToString());
            if (res == "0")
            {
                lblMsg.Text = "Error While Updation";
            }
            else
            {
                lblMsg.Text = "Updated Successfully";
                txtNPassword.Text = "";
                txtCPassword.Text = "";
            }

        }
    }
}
