﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="UpdateProfile.aspx.cs" Inherits="MyAccounts20.UpdateProfile" Title="Update Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript" language="javascript">
        var preid;
        var Name;
        var HNO;
        var Street1;
        var Street2;
        var AreaName;
        var City;
        var State;
        var LandMark;
        var PHNO;
        var Mobile;
        var Email;
        var Zone;
        var UserID;
        
        var preid = 'ctl00_ContentPlaceHolder1_'
        
        var xmlObj;
        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }
            
        }
               function GetControls()
               {
                 Name = document.getElementById(preid + "txtName");
                 HNO=document.getElementById(preid+"txtHNO");
                 Street1= document.getElementById(preid + "txtSOne");
                 Street2 = document.getElementById(preid+"txtSTwo");
                 AreaName = document.getElementById(preid+"ddlArea");
                 City = document.getElementById(preid + "ddlCity");
                 State=document.getElementById(preid+"ddlState");
                 LandMark = document.getElementById(preid + "txtLMark");
                 PHNO = document.getElementById(preid+"txtPHNO");
                 Mobile = document.getElementById(preid+"txtMobile");
                 Email = document.getElementById(preid + "txtEmail");
                 Zone=document.getElementById(preid+"txtZone");
                 
               }
                function GetAreaWiseZone()
                {
                    GetControls();
                    PageMethods.GetAreaWiseZone(AreaName.value,GetAreaWiseZoneComplete);    
                }
                function GetAreaWiseZoneComplete(res)
                {
                    getXml(res);    
                    Zone.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ZoneName")[0].firstChild.nodeValue;;
                    
                }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="730" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td height="2" align="left" valign="top"></td>
                        </tr>
                        <tr>
                          <td height="15" align="left" valign="middle" bgcolor="#cccccc" class="rectangleborder"><table width="700" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                              <td width="134"><h6 class="balck"><strong>Update Profile</strong></h6></td>
                              <td width="566"><table width="600" cellpadding="0" cellspacing="2">
                                <tbody><tr>
                                  <td width="82" class="bodytext"><strong>Customer Id</strong></td>
                                  <td width="12" align="center"><strong>:</strong></td>
                                  <td width="48"><asp:TextBox ID="txtCustomer" runat="server" width="50px" 
                                          Enabled="False"></asp:TextBox></td>
                                  <td width="43" class="bodytext"><strong>Name</strong></td>
                                  <td width="12" align="center"><strong>:</strong></td>
                                  <td width="387"><asp:TextBox ID="txtName" runat="server" Width="200px" Size="31" 
                                          Enabled="False"></asp:TextBox></td>
                                </tr>
                              </tbody></table></td>
                            </tr>
                          </tbody></table></td>
                        </tr>
                        <tr>
                          <td height="15" align="left" valign="middle" bgcolor="#cccccc" class="rectangleborder">
                              &nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" bgcolor="#f1f0f0" class="rectangleborder"><table width="600" border="0" cellspacing="2" cellpadding="0">
                            
                            <tbody><tr>
                              <td width="131" class="bodytext"><strong>H.No.</strong></td>
                              <td width="9" align="center"><strong>:</strong></td>
                              <td width="432"><asp:TextBox ID="txtHNO" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Street Name 1 </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtSOne" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Street Name 2</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtSTwo" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Area name <span class="style5">*</span> </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:DropDownList ID="ddlArea" runat="server" Width="210px" onchange="GetAreaWiseZone();"></asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>City <span class="style5">*</span> </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:DropDownList ID="ddlCity" runat="server" Width="210px"></asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>State <span class="style5">*</span></strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:DropDownList ID="ddlState" runat="server" Width="210px"></asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Land mark </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtLMark" runat="server" width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Ph No</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtPHNO" runat="server" width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Mobile<span class="style5">*</span> </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtMobile" runat="server" width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Email </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtEmail" runat="server" width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td><strong class="bodytext">Zone</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtZone" runat="server" width="200px" Size="31"></asp:TextBox></td>
                            </tr>

                            <tr>
                              <td class="bodytext">&nbsp;</td>
                              <td align="center">&nbsp;</td>
                              <td><asp:Button ID="btnUpdate" runat="server" Text="Update" 
                                      onclick="btnUpdate_Click" /></td>
                            </tr>
                            <tr>
                                <td colspan="100%" align="center">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                              <td colspan="3" align="left" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                              <td colspan="3" align="left" valign="top">&nbsp;</td>
                            </tr>
                          </tbody></table></td>
                        </tr>
                        <tr>
                          <td height="25" align="center" valign="middle" bgcolor="#cccccc" class="rectangleborder">
                              &nbsp;</td>
                        </tr>
                        
                    </tbody></table>                    
                     <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
