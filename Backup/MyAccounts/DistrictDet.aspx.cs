﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
namespace MyAccounts20
{
    public partial class DistrictDet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["UserID"].ToString() != "0")
                {
                    Response.Redirect("Login.aspx");
                }
                if (!IsPostBack)
                {
                    txtDistrict.Focus();
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
            }
        }
        void FillGrid()
        {
            try
            {
                District_BLL Obj_BLL = new District_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetDistrict();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvDistrict.DataSource= ds;
                        gvDistrict.DataBind();
                        gvDistrict.Rows[0].Cells[2].Text = "";
                    }
                    else
                    {
                        gvDistrict.DataSource = ds;
                        gvDistrict.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }


        public static string GetGridData()
        {
            StringWriter sw = new StringWriter();
            try
            {
                District_BLL Obj_BLL = new District_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetDistrict();
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "District";
                ds.WriteXml(sw);
            }
            catch (Exception ex)
            {
            }
            return sw.ToString();
        }


        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string FillDistrict()
        {
            return GetGridData();
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnSave_Click(string DistrictName)
        {
            string rtval = "";
            if (string.IsNullOrEmpty(DistrictName.Trim()))
            {
                rtval = "-2";
                return rtval;
            }
            District_BLL Obj_BLL = new District_BLL();
            string res = Obj_BLL.InsertDistrict(DistrictName);
            if (res == "1")
                rtval = GetGridData();
            else
                rtval = res;
            return rtval;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnUpdate_Click(string DistrictID, string DistrictName)
        {
            string rtnval = "";
            if (string.IsNullOrEmpty(DistrictName.Trim()))
            {
                rtnval = "-2";
                return rtnval;
            }
            District_BLL Obj_BLL = new District_BLL();
            string result = Obj_BLL.UpdateDistrict(DistrictID, DistrictName); ;
            if (result == "1")
                rtnval = GetGridData();
            else
                rtnval = result;
            return rtnval;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeleteClick(string DistrictID)
        {
            string rtn = "";
            District_BLL Obj_BLL = new District_BLL();
            string res = Obj_BLL.DeleteDistrict(DistrictID);
            if (res == "1")
                rtn = GetGridData();
            else
                rtn = res;
            return rtn;
        }

    }
}
        
