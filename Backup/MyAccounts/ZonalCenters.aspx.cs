﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class ZonalCenters : System.Web.UI.Page
    {
        ItemCreation_BLL obj_Bll = new ItemCreation_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        public void FillData()
        {
            DataSet ds = obj_Bll.GetZoneCenters();
            foreach (DataRow dr in ds.Tables[0].Rows)
                dr["Address"] = dr["Address"].ToString().Replace("\r\n", "<br/>");
            dlZone.DataSource = ds;
            dlZone.DataBind();
        }
    }
}
