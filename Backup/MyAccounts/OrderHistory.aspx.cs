﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;
namespace MyAccounts20
{
    public partial class OrderHistory : System.Web.UI.Page
    {
        ShopingCart_BLL obj_Bll = new ShopingCart_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                GetOrderHistory();
            }
        }
        void GetOrderHistory()
        {
            DataSet ds = new DataSet();
            ds = obj_Bll.GetOrderHistory(Session[MyAccountsSession.UserID].ToString());
            gvOrder.DataSource = ds;
            gvOrder.DataBind();           
        }
        protected void lbtnOrderID_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
            string OrderID = ((HtmlInputHidden)grow.FindControl("hidOrderID")).Value;
            string Status = "O";
            ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + OrderID + ");", true);                 
        }
    }
}
