﻿
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
namespace MyAccounts20
{

    public partial class TaxSystem : GlobalPage
    {
        TaxSystem_BLL Obj_BLL = new TaxSystem_BLL();
        //static string strUserID;
        public string strUserID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (!IsPostBack)
                {
                    txtTaxName.Focus();
                    GetVatMaster();
                    GetVatTypeMaster();
                    FillGrid();
                    hidUserID.Value = Session["UserID"].ToString();                    
                }
                strUserID = hidUserID.Value;
            }
            catch (Exception ex)
            {

            }
        }

        private void FillGrid()
        {
            try
            {
                TaxSystem_BLL Obj_BLL = new TaxSystem_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetTaxSystem();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvTaxSystem.DataSource = ds;
                        gvTaxSystem.DataBind();
                        gvTaxSystem.Rows[0].Cells[8].Text = "";
                    }
                    else
                    {
                        gvTaxSystem.DataSource = ds;
                        gvTaxSystem.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        

        protected void gvTaxSystem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTaxSystem.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        private void GetVatTypeMaster()
        {
            TaxSystem_BLL Obj_BLL = new TaxSystem_BLL();
            ddlType.DataSource = Obj_BLL.GetVatTypeMaster();
            ddlType.DataTextField = "VatTypeDesc";
            ddlType.DataValueField = "VatTypeID";
            ddlType.DataBind();
            ddlType.Items.Insert(0, new ListItem("Select", "0"));
        }

        private void GetVatMaster()
        {
            TaxSystem_BLL Obj_BLL = new TaxSystem_BLL();
            ddlVat.DataSource = Obj_BLL.GetVatMaster();
            ddlVat.DataTextField = "VatDesc";
            ddlVat.DataValueField = "VatID";
            ddlVat.DataBind();
            ddlVat.Items.Insert(0, new ListItem("Select", "0"));
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtRate.Text == "")
                txtRate.Text = "0"; 
            if (txtExDuty.Text == "")
                txtExDuty.Text = "0";
            if (txtCST.Text == "")
                txtCST.Text = "0";
            if (txtOther.Text == "")
                txtOther.Text = "0";
            if (txtSAT.Text == "")
                txtSAT.Text = "0";
            string res = Obj_BLL.InsertTaxSystem(txtTaxName.Text, txtExDuty.Text, txtCST.Text, txtOther.Text, txtSAT.Text, txtRate.Text, ddlVat.SelectedValue, ddlType.SelectedValue, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving TaxSystem";

            }
            else if (res == "-1")
            {
                lblMsg.Text = "TaxName already existed";
            }
            else if (res == "1")
            {
                lblMsg.Text = "TaxSystem saved successfully";
                FillGrid();
                //txtTaxNo.Text = "";
                txtTaxName.Text = "";
                txtExDuty.Text = "";
                txtCST.Text = "";
                txtOther.Text = "";
                txtSAT.Text = "";
                txtRate.Text = "";
                ddlVat.SelectedIndex = 0;
                txtRate.Focus();
                ddlType.SelectedIndex = 0;
                txtTaxName.Focus();

            }
        }

        protected void ddlVat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlVat.SelectedItem.Text == "Others")
            {
                txtRate.Visible = true;
                lblRate.Visible = true;
                txtRate.Focus();
                return;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateTaxSystem(hidTaxNo.Value, txtTaxName.Text, txtExDuty.Text, txtCST.Text, txtOther.Text, txtSAT.Text, txtRate.Text, ddlVat.SelectedValue, ddlType.SelectedValue, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating TaxSystem";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
            }
            else if (res == "1")
            {
                lblMsg.Text = "TaxSystem Updated successfully";
                FillGrid();
                //txtTaxNo.Text = "";
                txtTaxName.Text = "";
                txtExDuty.Text = "";
                txtCST.Text = "";
                txtOther.Text = "";
                txtSAT.Text = "";
                txtRate.Text = "";
                txtTaxNo.Text = "";
                ddlVat.SelectedIndex = 0;
                txtRate.Focus();
                ddlType.SelectedIndex = 0;
                txtTaxName.Focus();

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvTaxSystem.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            //txtTaxNo.Text = "";
            txtTaxName.Text = "";
            txtExDuty.Text = "";
            txtCST.Text = "";
            txtOther.Text = "";
            txtSAT.Text = "";
            txtRate.Text = "";
            ddlVat.SelectedIndex = 0;
            txtRate.Focus();
            ddlType.SelectedIndex = 0;
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidTaxNo.Value = ((HtmlInputHidden)grow.FindControl("hidTaxNo")).Value;
                txtTaxNo.Text = hidTaxNo.Value;
                txtTaxName.Text = ((Label)grow.FindControl("lblTaxName")).Text;
                txtExDuty.Text = ((Label)grow.FindControl("lblExDuty")).Text;
                txtCST.Text = ((Label)grow.FindControl("lblCST")).Text;
                txtOther.Text = ((Label)grow.FindControl("lblOther")).Text;
                txtSAT.Text = ((Label)grow.FindControl("lblSAT")).Text;
                txtRate.Text = ((Label)grow.FindControl("lblVATRATE")).Text;
                ddlVat.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidVAT")).Value;
                hidtypeId.Value = ((HtmlInputHidden)grow.FindControl("hidType")).Value;
                if (hidtypeId.Value == "")
                    ddlType.SelectedValue = "0";
                else
                    ddlType.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidType")).Value;
                // ddlType.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidType")).Value;
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidTaxNo.Value = ((HtmlInputHidden)grow.FindControl("hidTaxNo")).Value;
                TaxSystem_BLL Obj_BLL = new TaxSystem_BLL();
                string res = Obj_BLL.DeleteTaxSystem(hidTaxNo.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting TaxSystem";
                    

                }
                if (res == "1")
                {
                    lblMsg.Text = "TaxSystem Deleted Successfully";
                    txtTaxNo.Text = "";
                    txtTaxName.Text = "";
                    txtExDuty.Text = "";
                    txtCST.Text = "";
                    txtOther.Text = "";
                    txtSAT.Text = "";
                    txtRate.Text = "";
                    ddlVat.SelectedIndex = 0;
                    txtRate.Focus();
                    ddlType.SelectedIndex = 0;
                    txtTaxName.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
    
        

    

        

    