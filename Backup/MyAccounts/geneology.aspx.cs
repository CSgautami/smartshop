﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace MyAccounts20
{
    public partial class geneology : System.Web.UI.Page
    {
        BLL.Geneology_BLL bllGeneology = new BLL.Geneology_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx", true);
                }
                if (!IsPostBack)
                {
                    hidYourID.Value = Session["UserID"].ToString();
                }
                FillTree();
            }
            catch
            {
            }
        }

        void FillTree()
        {
            try
            {
                divLeft.Controls.Clear();
                divRight.Controls.Clear();
                DataSet ds = bllGeneology.GetGeneology(hidYourID.Value);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        imgMain.Src = "Imgs/" + dr["IsSaleExist"].ToString() + "SmallTree.jpg";
                        //imgMain.Alt = dr["LedgerName"].ToString() + "<br/>" + dr["JoiningDate"].ToString();
                        imgMain.Attributes["Title"] = "Name : " + dr["Name"].ToString() + "\nDate Of Joining : " + dr["JoiningDate"].ToString();
                        tdmain.InnerText = dr["UserID"].ToString();
                    }

                    if (ds.Tables.Count > 1)
                    {
                        int count = 0;
                        HtmlTable tbl;
                        HtmlTableRow tr;
                        HtmlTableCell tc;
                        Label lbl;
                        LinkButton lbtnUserID;

                        tbl = new HtmlTable();
                        tbl.CellPadding = 10;
                        tbl.CellSpacing = 10;
                        tr = new HtmlTableRow();
                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            if (count % 5 == 0 && count > 0)
                            {
                                tbl.Rows.Add(tr);
                                tr = new HtmlTableRow();
                            }
                            ImageButton img = new ImageButton();
                            img.ImageUrl = "Imgs/" + dr["IsSaleExist"].ToString() + "SmallTree.jpg";
                            img.Style["Cursor"] = "pointer";
                            img.Attributes["Title"] = "Name : " + dr["Name"].ToString() + "\nDate Of Joining : " + dr["JoiningDate"].ToString();
                            img.CssClass = dr["UserID"].ToString();
                            img.ID = "img" + dr["UserID"].ToString();
                            img.Click += new ImageClickEventHandler(img_Click);
                            tc = new HtmlTableCell();
                            tc.Controls.Add(img);
                            lbl = new Label();
                            lbl.Text = "<br/>";
                            lbl.ID = "lbl" + dr["UserID"].ToString();
                            tc.Controls.Add(lbl);
                            lbtnUserID = new LinkButton();
                            lbtnUserID.Text = dr["UserID"].ToString();
                            lbtnUserID.ID = "lbtn" + dr["UserID"].ToString();
                            lbtnUserID.Click += new EventHandler(lbtnUserID_Click);
                            tc.Controls.Add(lbtnUserID);
                            tr.Cells.Add(tc);
                            count++;
                        }
                        tbl.Rows.Add(tr);
                        divLeft.Controls.Add(tbl);


                        if (ds.Tables.Count > 2)
                        {
                            count = 0;

                            tbl = new HtmlTable();
                            tbl.CellPadding = 10;
                            tbl.CellSpacing = 10;
                            tr = new HtmlTableRow();
                            foreach (DataRow dr in ds.Tables[2].Rows)
                            {
                                if (count % 5 == 0 && count > 0)
                                {
                                    tbl.Rows.Add(tr);
                                    tr = new HtmlTableRow();
                                }
                                ImageButton img = new ImageButton();
                                img.ImageUrl = "Imgs/" + dr["IsSaleExist"].ToString() + "SmallTree.jpg";
                                img.Style["Cursor"] = "pointer";
                                img.Attributes["Title"] = "Name : " + dr["Name"].ToString() + "\nDate Of Joining : " + dr["JoiningDate"].ToString();
                                img.CssClass = dr["UserID"].ToString();
                                img.ID = "img" + dr["UserID"].ToString();
                                img.Click += new ImageClickEventHandler(img_Click);
                                tc = new HtmlTableCell();
                                tc.Controls.Add(img);
                                lbl = new Label();
                                lbl.Text = "<br/>";
                                lbl.ID = "lbl" + dr["UserID"].ToString();
                                tc.Controls.Add(lbl);
                                lbtnUserID = new LinkButton();
                                lbtnUserID.Text = dr["UserID"].ToString();
                                lbtnUserID.ID = "lbtn" + dr["UserID"].ToString();
                                lbtnUserID.Click += new EventHandler(lbtnUserID_Click);
                                tc.Controls.Add(lbtnUserID);
                                tr.Cells.Add(tc);
                                count++;
                            }
                            tbl.Rows.Add(tr);
                            divRight.Controls.Add(tbl);
                            tdLCount.InnerText = "";
                            tdRCount.InnerText = "";
                        }
                    }

                }
            }
            catch
            {
                throw;
            }
        }

        void lbtnUserID_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                hidYourID.Value = lbtn.Text;
                FillTree();

            }
            catch
            {
            }
        }

        void img_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton img = (ImageButton)sender;
                hidYourID.Value = img.CssClass;
                FillTree();
            }
            catch
            {
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            int Exist;
            bool bCustomer;
            bCustomer = false;
            if (Session["Type"].ToString() == "Customer")
                bCustomer = true;
            Exist = Convert.ToInt32(bllGeneology.GetIDExists(Session["UserID"].ToString(), txtFind.Text, bCustomer.ToString()).Tables[0].Rows[0]["Exist"].ToString());
            if (Exist != 0)
            {
                hidYourID.Value = txtFind.Text;
                FillTree();
                lblMsg.Text = "";
            }
            else
            {
                lblMsg.Text = "ID doesn't Exists";
            }
        }
    }
}
