﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;

namespace MyAccounts20
{
    public partial class ReportPinIssue : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "GetControls();", true);
                GetMember();                
            }
        }
        public void GetMember()
        {
            try
            {
                //Reports_BLL obj_BLL = new Reports_BLL();
                //ddlMember.DataSource = obj_BLL.GetMember().Tables[0];
                //ddlMember.DataTextField = "YourID";
                //ddlMember.DataValueField = "LedgerID";
                //ddlMember.DataBind();
                //ddlMember.Items.Insert(0, new ListItem("Select","0"));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCustomerName(string LedgerID)
        {
            string res = "";
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.GetLedgerDetails(LedgerID).Tables[0].Rows[0]["LedgerName"].ToString();
            }
            catch (Exception ex)
            {
                res = "";
                throw;
            }
            return res;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillMember(string prefix)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.CallFillMember(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
