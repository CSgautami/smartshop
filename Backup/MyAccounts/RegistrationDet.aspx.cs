﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;
namespace MyAccounts20
{
    public partial class RegistrationDet : System.Web.UI.Page
    {
        RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["UserID"] == null)
            //{
            //    Response.Redirect("Login.aspx");
            //}

           
            if (!IsPostBack)
            {
                //hidUserID.Value = Session["UserID"].ToString();                
                GetDefaults();
                //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
               
            }              
        }

        public void GetDefaults()
        {
            
            //RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
            ddlArea.DataSource = Obj_BLL.GetArea();
            ddlArea.DataTextField = "AreaCentre";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();

            ddlCity.DataSource = Obj_BLL.GetCity();
            ddlCity.DataTextField = "CityName";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();
            
            ddlState.DataSource = Obj_BLL.GetState();
            ddlState.DataTextField = "StateName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();

            ddlArea.Items.Insert(0, new ListItem("Select", "0"));
            ddlCity.SelectedValue = "2";
            ddlState.SelectedValue = "1";
            //ddlCity.Items.Insert(0, new ListItem("Select", "0"));
            //ddlState.Items.Insert(0, new ListItem("Select", "0"));
        }

        

        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            if (ddlArea.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select Area";
                ddlArea.Focus();
                return;
            }
            if (ddlCity.SelectedIndex < 0)
            {
                lblMsg.Text = "Please Select City";
                ddlCity.Focus();
                return;
            }
            if (ddlState.SelectedIndex < 0)
            {
                lblMsg.Text = "Please Select State";
                ddlState.Focus();
                return;
            }
            if (txtMobile.Text == "")
            {
                lblMsg.Text = "Please Enter Mobile";
                txtMobile.Focus();
                return;
            }
            if (txtACode.Text.Trim() != "")
            {
                string strRet;
                strRet= Obj_BLL.GetAgentValid(txtACode.Text.Trim());
                if (strRet == "0")
                {
                    lblMsg.Text = "Check Agent Code";
                    txtACode.Focus();
                    return;
                }
            }
            //if (txtUserName.Text == "")
            //{
            //    lblMsg.Text = "Please Enter UserName";
            //    txtUserName.Focus();
            //    return;
            //}
            //if (txtPassword.Text == "")
            //{
            //    lblMsg.Text = "Please Enter Password";
            //    txtPassword.Focus();
            //    return;
            //}
            //if (txtCPassword.Text == "")
            //{
            //    lblMsg.Text = "Please Enter CPassword";
            //    txtCPassword.Focus();
            //    return;
            //}

            //if (txtPassword.Text != txtCPassword.Text)
            //{
            //    lblMsg.Text = "Check Password and ConfirmPassword.";
            //    return;
            //}
            if (chkTermsAndConditions.Checked == false)
            {
                lblMsg.Text="Accept Terms And Conditions";
                return;
            }
            DataSet res = new DataSet();
             res = Obj_BLL.InsertRegistrationDet(txtName.Text, txtHNO.Text, txtSOne.Text, txtSTwo.Text, ddlArea.SelectedValue, ddlCity.SelectedValue, ddlState.SelectedValue, txtLMark.Text, txtPHNO.Text, txtMobile.Text, txtEmail.Text, "", "", "0",txtACode.Text);
             if (res.Tables[1].Rows[0]["ID"].ToString() == "0" || res.Tables[1].Rows[0]["ID"].ToString() == "")
            {
                lblMsg.Text = "Error while saving Registration";
            }
             else if (res.Tables[1].Rows[0]["ID"].ToString() == "-1")
            {
                lblMsg.Text = "User Name already existed";

            }
            else 
            {
                //lblMsg.Text = "You are Registered successfully With ID:" + res;
                //Page.RegisterStartupScript("WelCome", "<Script type='text/javascript'>alert('WelCome To Vanitha SuperMarket \\nYou are Successfully Registered With');</Script> ");
                //FillGrid();
                Response.Redirect("UserRegDetails.aspx?ID=" + res.Tables[1].Rows[0]["ID"].ToString() + "&Pwd=" + res.Tables[1].Rows[0]["Password"].ToString());
                txtName.Text = "";
                txtHNO.Text = "";
                txtSOne.Text = "";
                txtSTwo.Text = "";
                ddlArea.SelectedIndex = 0;
                ddlCity.SelectedValue = "2";
                ddlState.SelectedValue = "1";
                txtLMark.Text = "";
                txtPHNO.Text = "";
                txtMobile.Text = "";
                txtEmail.Text = "";
                txtZone.Text = "";
                txtUserName.Text = "";
                txtPassword.Text = "";
                txtCPassword.Text = "";
                chkTermsAndConditions.Checked = false;
                txtName.Focus();

            }
        }
        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetArea()
        //{
        //    try
        //    {
        //        RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
        //        return Obj_BLL.GetArea().GetXml();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAreaWiseZone(string AreaID)
        {
            try
            {
                RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
                return Obj_BLL.GetAreaWiseZone(AreaID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
    }
}
