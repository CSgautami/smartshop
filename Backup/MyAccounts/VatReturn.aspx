<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="VatReturn.aspx.cs" Inherits="MyAccounts20.VatReturn" Title="Vat Return" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="Calendar.css" rel="stylesheet" type="text/css" />

    <script src="JS/CalendarPopup.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">


        document.write(getCalendarStyles());
        var cal = new CalendarPopup("divCalendar");
        cal.showNavigationDropdowns();
        function showcalendar(t) {
            cal.select(t, t.id, 'dd-MM-yyyy');
        }
        var UserID = null;
        var hidBranchID;
        var hidGBID;
        var ddlBranch;
        var preid;
        var PChallanaNo;
        var PChallanaDate;
        var PBank;
        var PBranch;
        var PAmount;
        var ANOADJ;
        var ADetails;
        var AAmount;
        var tblPGrid;
        var tblAGrid;
        var SrNo;
        preid = 'ctl00_ContentPlaceHolder1_'

        var xmlObj;
        function getXml(xmlString) {
            xmlObj = null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }

        }
        function GetControls() 
        {
            ddlBranch = document.getElementById(preid + "ddlBranch");
            hidBranchID = document.getElementById(preid + "hidBranchID");
            PChallanaNo = document.getElementById(preid + "txtPAChalan");
            PChallanaDate = document.getElementById(preid + "txtPADate");
            PBank = document.getElementById(preid + "txtPABank");
            PBranch = document.getElementById(preid + "txtPABranch");
            PAmount = document.getElementById(preid + "txtPAAmount");
            ANOADJ = document.getElementById(preid + "txtPANature");
            ADetails = document.getElementById(preid + "txtPADetails");
            AAmount = document.getElementById(preid + "txtPAAmt");
            SrNo = document.getElementById(preid + "txtVRSrNo");
        }
        function GetUserID() {
            UserID = document.getElementById(preid + "hidUserID");
        }
        function GetDefaultBranch() {
            hidGBID = document.getElementById(preid + "hidGBID");
        }
        function btnCancel_Click() {
            GetControls();
            if (UserID == null)
                GetUserID();

        }
        function SetVisibility() {

            document.getElementById("divVatReturn").style.display = "none";
            document.getElementById("divPayMent&AdjustMent").style.display = "none";
        }
        function SelectTab(t) {
            SetVisibility();
            t.className = "DBENavClick";
            if (t.id == "VatReturn") {
                document.getElementById("divVatReturn").style.display = "block";
            }
            else if (t.id == "PaymentAdjustment") {
                document.getElementById("divPayMent&AdjustMent").style.display = "block";
            }
        }
        function btnPAdd_Click() {

            if (PChallanaNo.value == 0) {
                PChallanaNo.focus();
                alert("Enter Challana No");
                return;
            }
            if (PChallanaDate.value == "") {
                PChallanaDate.focus();
                alert("Select Challana Date");
                return;
            }
            if (PBank.value == "") {
                PBank.focus();
                alert("Enter Bank");
                return;
            }
            if (PBranch.value == "") {
                PBranch.focus();
                alert("Enter Branch Code");
                return;
            }
            if (PAmount.value == "") {
                PAmount.focus();
                alert("Enter Amount");
                return;
            }
            if (isNaN(PAmount.value)) {
                PAmount.text = "";
                PAmount.focus();
                alert("Enter valid Amount");
                PAmount.text = "";
                return;
            }
            tblPGrid = document.getElementById(preid + "tblPay");
            var i;
            i = tblPGrid.rows.length;
            var tr = tblPGrid.insertRow(i);
            for (i = 0; i < 5; i++) {
                tr.insertCell(i);
            }
            tr.cells[0].innerHTML = PChallanaNo.value;
            //tr.cells[0].style.display="none";
            tr.cells[1].innerHTML = PChallanaDate.value;
            tr.cells[2].innerHTML = PBank.value;
            tr.cells[3].innerHTML = PBranch.value;
            tr.cells[4].innerHTML = parseFloat(PAmount.value);
            PChallanaNo.value = "";
            PChallanaDate.value = "";
            PBank.value = "";
            PBranch.value = "";
            PAmount.value = "";
            PChallanaNo.focus();


        }
        var Sno = 1;
        function btnAAdd_Click() {

            if (ANOADJ.value == "") {
                ANOADJ.focus();
                alert("Enter Nature Of Adjustment");
                return;
            }
            if (ADetails.value == "") {
                ADetails.focus();
                alert("Enter Details");
                return;
            }
            if (AAmount.value == "") {
                AAmount.focus();
                alert("Enter Amount");
                return;
            }
            if (isNaN(AAmount.value)) {
                AAmount.text = "";
                AAmount.focus();
                alert("Enter valid Amount");
                AAmount.text = "";
                return;
            }
            tblAGrid = document.getElementById(preid + "tblAdj");
            var i;
            i = tblAGrid.rows.length;
            var tr = tblAGrid.insertRow(i);
            for (i = 0; i < 5; i++) {
                tr.insertCell(i);
            }
            Sno = Sno + 1;
            tr.cells[0].innerHTML = Sno;
            tr.cells[0].style.display = "none";
            tr.cells[1].innerHTML = ANOADJ.value;
            tr.cells[2].innerHTML = ADetails.value;
            tr.cells[3].innerHTML = parseFloat(AAmount.value);
            ANOADJ.value = "";
            ADetails.value = "";
            AAmount.value = "";
            ANOADJ.focus();
        }
        function SetData() {
            GetControls();
            //btnCancel_Click();
        }
        function DateFormat(input) {
            if (input != "") {
                var datePart = input.match(/\d+/g)
                day = datePart[0];
                month = datePart[1];
                year = datePart[2].substring(2) // get only two digits  
                return month + '/' + day + '/' + year;
                
            }
        }
        function btnSavePay_Click(e, t) {
            tblPGrid = document.getElementById(preid + "tblPay");
            var xmlDet = "<XML>";
            for (var i = 1; i < tblPGrid.rows.length; i++) {
                xmlDet = xmlDet + "<VatRetPaymentDet>";
                var tr = tblPGrid.rows[i];
                xmlDet = xmlDet + "<Challanno>" + tr.cells[0].innerHTML.trim() + "</Challanno>";
                xmlDet = xmlDet + "<ChallanDate>" +DateFormat(tr.cells[1].innerHTML).trim() + "</ChallanDate>";
                xmlDet = xmlDet + "<BankName>" + tr.cells[2].innerHTML.trim() + "</BankName>";
                xmlDet = xmlDet + "<BranchCode>" + tr.cells[3].innerHTML.trim() + "</BranchCode>";
                xmlDet = xmlDet + "<Amount>" + tr.cells[4].innerHTML.trim() + "</Amount>";
                xmlDet = xmlDet + "</VatRetPaymentDet>";
            }
            xmlDet = xmlDet + "</XML>";
            var Con = confirm("Are You Sure,Do You Want To Save?");
            if (Con == true) {
                PageMethods.SavePay(SrNo.value,ddlBranch.value,xmlDet,SavePayComplete);
            }
           }
            function SavePayComplete(res) {
                if (res == "0" || res == "") {
                    alert("Error while Saving");
                }
                else if (res.substring(0, 1) == "E") {
                    alert(res);
                }
                else if (res.length > 20) {
                    alert(res);
                }
                else {
                    alert("Payment Saved successfully" + res);
                    //btnCancel_Click();
                }
             }
             function btnSaveAdj_Click(e, t) {
                 tblAGrid = document.getElementById(preid + "tblAdj");
                 var xmlDet = "<XML>";
                 for (var i = 1; i < tblAGrid.rows.length; i++) {
                     xmlDet = xmlDet + "<VatRetAdjDet>";
                     var tr = tblAGrid.rows[i];
                     xmlDet = xmlDet + "<AdjID>" + tr.cells[0].innerHTML.trim() + "</AdjID>";
                     xmlDet = xmlDet + "<NatureOfAdjustment>" + tr.cells[1].innerHTML.trim() + "</NatureOfAdjustment>";
                     xmlDet = xmlDet + "<Details>" + tr.cells[2].innerHTML.trim() + "</Details>";
                     xmlDet = xmlDet + "<Amount>" + tr.cells[3].innerHTML.trim() + "</Amount>";
                     xmlDet = xmlDet + "</VatRetAdjDet>";
                 }
                 xmlDet = xmlDet + "</XML>";
                 var Con = confirm("Are You Sure,Do You Want To Save?");
                 if (Con == true) {
                     PageMethods.SaveAdj(SrNo.value, ddlBranch.value, xmlDet, SaveAdjComplete);
                 }
             }
            function SaveAdjComplete(res) {
                if (res == "0" || res == "") {
                    alert("Error while Saving");
                }
                else if (res.substring(0, 1) == "E") {
                    alert(res);
                }
                else if (res.length > 20) {
                    alert(res);
                }
                else {
                    alert("Adjustment Saved successfully" + res);
                    //btnCancel_Click();
                }
            }  
            function btnPrintClick()
            {
                try
                {           
                    var invid=document.getElementById(preid + "txtVRSrNo").value;
                    var ddlBranch=document.getElementById(preid + "ddlBranch").value;        
                    window.open("PrintVatReturn.aspx?SRNo="+invid+"&BranchID="+ddlBranch,"","Height=1000,Width=1000,scroll=1");
                }
                catch(err)
                {
                }
            }         

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table width="500px">
            <tr>
                <td style="width: 100px;" colspan="13">
                    <table width="100%" class="Navigation" id="tblDBNav">
                        <tr>
                            <td style="width: 100px;">
                                <a href="#" id="VatReturn" class="DBENavClick" onclick="SelectTab(this);">VatReturn</a>
                            </td>
                            <td>
                                &nbsp
                            </td>
                            <td nowrap="nowrap">
                                <a href="#" id="PaymentAdjustment" class="DBENav" onclick="SelectTab(this);">Payment
                                    And Adjustment</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table align="left" id="tblShow" runat="server">
                        <tr>
                            <td align="left">
                                SRNO
                            </td>
                            <td>
                            </td>
                            <td align="left" width="50px">
                                <asp:TextBox ID="txtVRSrNo" runat="server" Width="100px" ReadOnly="true" TabIndex="-1"></asp:TextBox>
                            </td>
                            <td nowrap="nowrap" align="left">
                                Branch :
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlBranch" Width="325px" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnPrint" Text="Print" runat="server" OnClientClick="btnPrintClick();return false;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table align="left">
                        <tr align="left">
                            <td nowrap="nowrap" align="left">
                                Period Coverd by this return From
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="txtVRDtpFrom" runat="server" Width="100px" onfocus="showcalendar(this);"
                                    onclick="showcalendar(this);" onkeydown="HideCalendar(event);" 
                                    Style="width: 100px;" />
                            </td>
                            <td>
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="txtVRDTPTo" runat="server" Width="100px" MaxLength="10" onfocus="showcalendar(this);"
                                    onclick="showcalendar(this);" onkeydown="HideCalendar(event);" Style="width: 100px;"
                                    OnTextChanged="txtVRDTPTo_TextChanged" />
                            </td>
                            <td>
                            </td>
                            <td nowrap="nowrap">
                                <asp:CheckBox ID="chkVRRefresh" runat="server" Text="Refresh" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnShow" Text="Show" runat="server" Width="70px" OnClick="btnShow_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="13">
                    <div id="divVatReturn" style="position: relative; top: 10px;">
                        <table runat="server" id="tblMain" width="100%">
                            <tr>
                                <td align="left">
                                    <b>Purchases In The Month(Input)</b>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    Input tax credit from Previous month :
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: solid 1px black" align="center" width="100%">
                                        <tr>
                                            <%--align="left" style="width: 280px" nowrap="nowrap"--%>
                                            <td align="left">
                                                Exempt or Non-Creditable Purchase
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtExAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtExTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                4% Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt4PurAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt4PurTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                5% Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt5PurAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt5PurTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td align="left">
                                                Standard Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt8a1" runat="server"  style="text-align:right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt8b1" runat="server"  style="text-align:right"></asp:TextBox>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td align="left">
                                                12.5% Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt125PurAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt125PurTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                14.5% Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt145PurAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt145PurTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                1% Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt1PurAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt1PurTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td align="left">
                                                VAT 116
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt116Pur" runat="server" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td align="left">
                                                Special Rate Purchases
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSplPurAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSplPurTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Total Amount of Input tax
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtTotIpTaxAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtTotIpTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <b>Sales In The Month(OutPut)</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: solid 1px black" align="center" width="100%">
                                        <tr>
                                            <td align="left">
                                                Exempt or Non-Creditable Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtExSaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtExSaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Zero Rate Sales - International Exports
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSaleExpoAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSaleExpoTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Zero Rate Sales - Others(CST Sales)
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCSTAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCSTTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Tax Due on Purchase point Goods
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtPurVal" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtVatPay" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                4 % Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt4SaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt4SaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                5 % Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt5SaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt5SaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td align="left">
                                                Standard Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt17a1" runat="server" style="text-align:right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt17b1" runat="server" style="text-align:right"></asp:TextBox>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td align="left">
                                                12.5% Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt125SaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt125SaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                14.5% Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt145SaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt145SaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Special Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSplSaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSplSaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                1% Rate Sales
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt1SaleAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txt1SaleTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Total Amount of Output tax
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtTotOpTaxAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtTotOpTax" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 890px;">
                    <div id="divPayMent&AdjustMent" style="display: none; position: relative; top: 10px;">
                        <table id="tblAdjustments" runat="server">
                            <tr>
                                <td nowrap="nowrap" align="left">
                                    <b>Payments & Adjustment Details</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="13">
                                    <table style="border: solid 1px black" align="center" width="100%">
                                        <tr>
                                            <td align="center" colspan="13" nowrap="nowrap">
                                                <b>Payment Details</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="divItemDet" class="divgrid" style="height: 100px; width: 460px; border: solid 1px #98bf21;">
                                                    <table id="tblPay" runat="server" align="center" style="font-size: small">
                                                        <tr class="dbookheader">
                                                            <th style="width: 100px;">
                                                                Challana No
                                                            </th>
                                                            <th style="width: 100px;">
                                                                Challana Date
                                                            </th>
                                                            <th style="width: 50px;">
                                                                Bank
                                                            </th>
                                                            <th style="width: 100px;">
                                                                Branch Code
                                                            </th>
                                                            <th style="width: 80px;">
                                                                Amount
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>                                           
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Challan/Instrument No
                                                        </td>
                                                        <td>
                                                            Date
                                                        </td>
                                                        <td>
                                                            Bank/Treasury
                                                        </td>
                                                        <td>
                                                            Branch Code
                                                        </td>
                                                        <td>
                                                            Amount
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtPAChalan" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPADate" runat="server" Width="100px" onfocus="showcalendar(this);"
                                                                onclick="showcalendar(this);" onkeydown="HideCalendar(event);" Style="width: 100px;" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPABank" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPABranch" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPAAmount" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="13">
                                                            <input type="button" id="btnPAdd" value="Add" runat="server" onclick="btnPAdd_Click();" />
                                                            <input type="button" id="btnSavePay" value="SavePayment" runat="server" onclick="btnSavePay_Click();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="13">
                                    <table style="border: solid 1px black" align="left" width="100%">
                                        <tr>
                                            <td align="center" colspan="13" nowrap="nowrap" width="100%">
                                                <b>Adjustment Details</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="divAdj" class="divgrid" style="height: 100px; width: 400px; border: solid 1px #98bf21;">
                                                    <table id="tblAdj" runat="server" style="font-size: small">
                                                        <tr class="dbookheader">
                                                            <th style="width: 0px; display: none" id="Th1">
                                                                AdjId
                                                            </th>
                                                            <th style="width: 200px;" id="Th2">
                                                                Nature Of Adjustment
                                                            </th>
                                                            <th style="width: 100px;" id="Th3">
                                                                Details
                                                            </th>
                                                            <th style="width: 80px;" id="Th5">
                                                                Amount
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Nature Of Adjustments
                                                        </td>
                                                        <td>
                                                            Details
                                                        </td>
                                                        <td>
                                                            Amount
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtPANature" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPADetails" runat="server" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtPAAmt" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnAAdd" value="Add" runat="server" onclick="btnAAdd_Click();" />
                                                            <input type="button" id="btnSaveAdj" value="SaveAdjustment" runat="server" onclick="btnSaveAdj_Click();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <tr>
                                        <td colspan="13">
                                            <table style="border: solid 1px black" align="left" width="100%">
                                            </table>
                                        </td>
                                    </tr>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divVRLRow" style="position: relative; top: 10px;">
                        <table id="tblPaymets" runat="server">
                            <tr>
                                <td align="left">
                                    <b>Payments</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="13">
                                    <table style="border: solid 1px black" align="left">
                                        <tr>
                                            <td nowrap="nowrap">
                                                Amount to Be paid
                                            </td>
                                            <td>
                                                Paid
                                            </td>
                                            <td>
                                                Refund
                                            </td>
                                            <td nowrap="nowrap">
                                                Carried forward
                                            </td>
                                            <td>
                                                24(a)
                                            </td>
                                            <td>
                                                24(b)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtVRAmtPaid" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVRPaid" runat="server" Width="100px" Style="text-align: right"
                                                    OnTextChanged="txtVRPaid_TextChanged"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVRRef" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVRCF" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVR24a" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtVR24b" runat="server" Width="100px" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="13">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                                <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                         
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
        <input type="hidden" runat="server" id="hidUserID" />
        <input type="hidden" runat="server" id="hidBranchID" />
        <input type="hidden" runat="server" id="hidGBID" />
        <input type="hidden" runat="server" id="hidVRID" />
    </div>
</asp:Content>
