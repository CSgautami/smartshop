﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class ZoneMaster : GlobalPage
    {
        Zone_BLL Obj_BLL = new Zone_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            if (!IsPostBack)
            {
                FillGrid();
                //GetDefaults();
                hidUserID.Value = Session["UserID"].ToString();
            }
        }
               
        private void FillGrid()
        {
            try
            {
                Zone_BLL Obj_BLL = new Zone_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetZone(txtSearch.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvZone.DataSource = ds;
                        gvZone.DataBind();
                        gvZone.Rows[0].Cells[4].Text = "";
                        //gvZone.Rows[0].Visible = false;
                      
                    }
                    else
                    {
                        gvZone.DataSource = ds;
                        gvZone.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public string GetGridData()
        {
            
            StringWriter sw = new StringWriter();
            try
            {
                Zone_BLL Obj_BLL = new Zone_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetZone(txtSearch.Text);
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "ZoneMaster";
                ds.WriteXml(sw);
            }
            catch (Exception ex)
            {
            }
            return sw.ToString();
        }
        public string FillZone()
        {
            return GetGridData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertZone(txtZoneNo.Text, txtZoneName.Text, txtAddress.Text, txtPhone.Text, hidUserID.Value,txtStartID.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Zone";
                //txtStockGroup.Focus();

            }
            else if (res == "-1")
            {
                lblMsg.Text = "ZoneName already existed";
                //txtStockGroup.Text = "";
                //ddlHeadGroup.SelectedIndex = 0;
                txtZoneNo.Focus();
            }
            else if (res == "1")
            {
                lblMsg.Text = "Zone saved successfully";
                FillGrid();
                txtZoneNo.Text = "";
                txtZoneName.Text = "";
                txtAddress.Text = "";
                txtPhone.Text = "";
                txtStartID.Text = "";
                txtSearch.Text = "";
                txtZoneNo.Focus();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateZone(hidZoneID.Value, txtZoneNo.Text, txtZoneName.Text, txtAddress.Text, txtPhone.Text, hidUserID.Value,txtStartID.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Zone";
                //txtStockGroup.Focus();

            }
            else if (res == "-1")
            {
                lblMsg.Text = "ZoneName already existed";
                //txtStockGroup.Text = "";
                //ddlHeadGroup.SelectedIndex = 0;
                txtZoneNo.Focus();
            }
            else if (res == "1")
            {
                lblMsg.Text = "Zone Updated successfully";
                FillGrid();
                txtZoneNo.Text = "";
                txtZoneName.Text = "";
                txtAddress.Text = "";
                txtPhone.Text = "";
                txtSearch.Text = "";
                txtStartID.Text = "";
                btnUpdate.Visible = false;
                btnSave.Visible = true;                
                txtZoneNo.Focus();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvZone.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtZoneNo.Text = "";
            txtZoneName.Text = "";
            txtAddress.Text = "";
            txtPhone.Text = "";
            txtStartID.Text = "";
            txtSearch.Text = "";
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidZoneID.Value = ((HtmlInputHidden)grow.FindControl("hidZoneID")).Value;
                txtZoneNo.Text = ((Label)grow.FindControl("lblZoneNo")).Text;
                txtZoneName.Text = ((Label)grow.FindControl("lblZoneName")).Text;
                txtAddress.Text = ((Label)grow.FindControl("lblAddress")).Text;
                txtPhone.Text = ((Label)grow.FindControl("lblPhone")).Text;
                txtStartID.Text = ((Label)grow.FindControl("lblStartID")).Text;
                txtZoneNo.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidZoneID.Value = ((HtmlInputHidden)grow.FindControl("hidZoneID")).Value;
                Zone_BLL Obj_BLL = new Zone_BLL();
                string res = Obj_BLL.DeleteZone(hidZoneID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Zone";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "Zone Deleted Successfully";
                    txtZoneNo.Text = "";
                    txtZoneName.Text = "";
                    txtAddress.Text = "";
                    txtPhone.Text = "";
                    txtSearch.Text = "";
                    txtStartID.Text = "";
                    txtZoneNo.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        protected void gvZone_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvZone.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
    }
}
        
    
 
    


        

        



