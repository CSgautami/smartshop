﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="RegistrationDet.aspx.cs" Inherits="MyAccounts20.RegistrationDet" Title="Registration" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
        var preid;
        var Name;
        var HNO;
        var Street1;
        var Street2;
        var AreaName;
        var City;
        var State;
        var LandMark;
        var PHNO;
        var Mobile;
        var Email;
        var Zone;
        var UserName;
        var Password;
        var TermsAndConditions;
        var UserID;
        var ChangePassword;
        var preid = 'ctl00_ContentPlaceHolder1_'
        
        var xmlObj;
        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }
            
        }
               function GetControls()
               {
                 Name = document.getElementById(preid + "txtName");
                 HNO=document.getElementById(preid+"txtHNO");
                 Street1= document.getElementById(preid + "txtSOne");
                 Street2 = document.getElementById(preid+"txtSTwo");
                 AreaName = document.getElementById(preid+"ddlArea");
                 City = document.getElementById(preid + "ddlCity");
                 State=document.getElementById(preid+"ddlState");
                 LandMark = document.getElementById(preid + "txtLMark");
                 PHNO = document.getElementById(preid+"txtPHNO");
                 Mobile = document.getElementById(preid+"txtMobile");
                 Email = document.getElementById(preid + "txtEmail");
                 Zone=document.getElementById(preid+"txtZone");
                 UserName= document.getElementById(preid + "txtUserName");
                 Password = document.getElementById(preid+"txtPassword");
                 ChangePassword=document.getElementById(preid+"txtCPassword");
                 TermsAndConditions=document.getElementById(preid+"chkTermsAndConditions");
               }

            
               
                function GetAreaWiseZone()
                {
                    GetControls();
                    PageMethods.GetAreaWiseZone(AreaName.value,GetAreaWiseZoneComplete);    
                }
                function GetAreaWiseZoneComplete(res)
                {
                    getXml(res);    
                    Zone.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ZoneName")[0].firstChild.nodeValue;;
                    
                }
                function ACodechange()
                {
                    var lblCode=document.getElementById(preid+"lblCode");
                    var txtACode=document.getElementById(preid+"txtACode");
                    if(txtACode.value=="None")
                        lblCode.visible=true;
                    else
                        lblCode.visible=false;
                }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="40%" border="0"  cellpadding="0" cellspacing="0" align="center" >
                       <tr>
                          <td height="2" align="left" valign="top"></td>
                        </tr>
                        <tr>
                          <td height="33" align="center" valign="middle" style="color:#1570a2"><h4>Registration</h4></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top" bgcolor="#FFFFFF" class="roundedcorners"><table width="600" border="0" cellspacing="2" cellpadding="0" align="center">
                          
                            <tr>
                              <td width="131" height="31" class="bodytext" align="left"><strong>Name</strong></td>
                              <td width="9" align="center"><strong>:</strong></td>
                              <td width="200" align="left"><asp:TextBox ID="txtName" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>H.No.</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtHNO" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Street Name 1 </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtSOne" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Street Name 2</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtSTwo" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Area name <span class="style5">*</span> </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:DropDownList ID="ddlArea" runat="server" Width="210px" onchange="GetAreaWiseZone();"></asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>City <span class="style5">*</span> </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"> <asp:DropDownList ID="ddlCity" runat="server" Width="210px"></asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>State <span class="style5">*</span></strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:DropDownList ID="ddlState" runat="server" Width="210px"></asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Land mark </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtLMark" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Ph No</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtPHNO" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Mobile<span class="style5">*</span> </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtMobile" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Application Code</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left">  
                                  <asp:TextBox ID="txtACode" runat="server" Width="100px" MaxLength="6" 
                                      onchange="ACodechange();" ></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="txtACode_TextBoxWatermarkExtender" 
                                      runat="server" Enabled="True" TargetControlID="txtACode" 
                                      WatermarkText="None" WatermarkCssClass="WaterMark">
                                  </cc1:TextBoxWatermarkExtender>
                                    <asp:Label ID="lblCode" runat="server" Text="If Code None Then Skip" ForeColor="Red"></asp:Label>
                              </td>
                            </tr>
                            <tr>
                              <td class="bodytext" align="left"><strong>Email </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtEmail" runat="server" Width="200px" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td align="left"><strong class="bodytext">Zone</strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtZone" runat="server" Width="200px" ReadOnly="True" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr style="display:none;">
                              <td colspan="3" bgcolor="#f3f3f3" class="zixegreen">Login Information</td>
                            </tr>
                            <tr style="display:none;">
                              <td width="131" class="bodytext" align="left"><strong>Username <span class="style5">*</span></strong></td>
                              <td width="9" align="center"><strong>:</strong></td>
                              <td width="432" align="left"><asp:TextBox ID="txtUserName" runat="server" Width="200px" Size="31"></asp:TextBox>  </td>
                            </tr>
                            <tr style="display:none">
                              <td class="bodytext" align="left"><strong>Password <span class="style5">*</span></strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td align="left"><asp:TextBox ID="txtPassword" runat="server" Width="200px" TextMode="Password" Size="31"></asp:TextBox></td>
                            </tr>
                            <tr style="display:none">
                              <td class="bodytext" align="left"><strong>Confirm Password</strong><span class="style5"> *</span></td>
                              <td align="center"><strong>:</strong></td>
                              <td style="color:#FF3300;" align="left"><asp:TextBox ID="txtCPassword" 
                                      runat="server" Width="200px" Size="31" TextMode="Password"></asp:TextBox></td>
                            </tr>
                            <tr>
                              <td colspan="3" class="bodytext" valign="top">                 <asp:CheckBox ID="chkTermsAndConditions" runat="server" />
                                  I agree the <a href="#" class="zixegreen2"><strong>terms and conditions</strong></a></td>
                            </tr>
                            <tr>
                              <td class="bodytext">&nbsp;</td>
                              <td align="center">&nbsp;</td>
                              <td><asp:Button ID="btnSave" runat="server" Text="Submit" onclick="btnSave_Click"/></td>
                            </tr>
                             <tr>
                <td colspan="100%" align="left">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
                            <tr>
                              <td colspan="3" align="left" valign="top">&nbsp;</td>
                            </tr>
                            
                            <tr>
                              <td colspan="3" align="left" valign="top">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                        
                    </table>
    <input type="hidden" runat="server" id="hidRegisID" />
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server" Visible="false">
</asp:Content>