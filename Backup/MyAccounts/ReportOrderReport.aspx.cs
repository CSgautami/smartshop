﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
namespace MyAccounts20
{
    public partial class ReportOrderReport : System.Web.UI.Page
    {
        ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
        Purchase_BLL obj_BLL1 = new Purchase_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
                //ddlShipmentDet.Items.Insert(0, new ListItem("Select", "0"));
                ddlShipmentDet.Items.Insert(0, new ListItem("All", "0"));
                ddlShipmentDet.Items.Insert(1, new ListItem("Delivered", "Delivered"));
                ddlShipmentDet.Items.Insert(2, new ListItem("Pending", "Pending"));                
                ddlShipmentDet.Items.Insert(3, new ListItem("InProcess", "InProcess"));
                ddlShipmentDet.Items.Insert(4, new ListItem("Cancelled", "Cancelled"));
                DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
                txtFromDate.Value = nFirstDate.ToString("dd-MM-yyyy");
                txtToDate.Value = nLastDate.ToString("dd-MM-yyyy");
                ddlBranch.DataSource = obj_BLL1.GetBranch(Session["UserID"].ToString());                
                ddlBranch.DataTextField = "BranchName";
                ddlBranch.DataValueField = "BranchID";
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0,new ListItem("Select","0"));                
            }
        }
            
    }
}
