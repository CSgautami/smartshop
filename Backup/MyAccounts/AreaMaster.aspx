﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AreaMaster.aspx.cs" Inherits="MyAccounts20.AreaMaster"
    MasterPageFile="~/MyAccounts20.Master" Title="Area Creation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script language="javascript" type="text/javascript">
    var preid='ctl00_ContentPlaceHolder1_';
    String.prototype.trim=function() {
          return this.replace(/^\s*/, "").replace(/\s*$/, "");
          }
          function ValidateSave()
          {
              try
              {
                var AreaCode = document.getElementById(preid + "txtAreaCode");
                var AreaCentre=document.getElementById(preid+"txtAreaCentre");
                var Address = document.getElementById(preid + "txtAddress");
                var ZoneID = document.getElementById(preid+"ddlZone");
                var ContactNo = document.getElementById(preid+"txtContactNo");
                var BranchID=document.getElementById(preid+"ddlBranch");
                if (AreaCode.value.trim() == "")
                {        
                    AreaCode.value="";
                    AreaCode.focus();
                    alert("Enter AreaCode");
                    return false;
                    
                }    
                if (AreaCentre.value.trim() == "")
                {        
                    AreaCentre.value="";
                    AreaCentre.focus();
                    alert("Enter AreaCentre");
                    return false;
                    
                }    
                if (Address.value.trim() == "")
                {        
                    Address.value="";
                    Address.focus();
                    alert("Enter Address.");
                    return false;
                    
                } 
                if (ZoneID.value== "0")
                {        
                    ZoneID.focus();
                    alert("Select ZoneID");
                    return false;  
                }
                if (ContactNo.value.trim() == "")
                {        
                    ContactNo.value="";
                    ContactNo.focus();
                    alert("Enter ContactNo");
                    return false;
                    
                }        
                  return true;
                   }
                  catch(err)
                  {
                     return false;
                  }
               }
                function ConfirmEdit()
                {
                    if(!confirm("Do you want to Edit?"))
                    return false;
                }
                function ConfirmDelete()
                {
                    if(! confirm("Do you want to Delete?"))
                    return false;
                } 
               
    </script>

    <%--<style type="text/css">
        .style1
        {
            width: 169px;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
            <tr>
                <td colspan="100%">
                    &nbsp
                </td>
            </tr>
            <tr>
                 <td>
                    Search :
                </td>
                <td align="left">
                    <asp:TextBox ID="txtSearch" runat="server" Width="150px" AutoPostBack="True" ontextchanged="txtSearch_TextChanged" 
                        ></asp:TextBox>
                </td>           
            </tr>
            <tr>
                <td colspan="100%" align="center">
                    <asp:GridView ID="gvArea" runat="server" AutoGenerateColumns="false" CssClass="grid"
                        PageSize="10" AllowPaging="true" Font-Size="Small" Width="900px" 
                        OnPageIndexChanging="gvArea_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblAreaCode" runat="server" Text='<%#Eval("AreaCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AreaCentre">
                                <ItemTemplate>
                                    <input type="hidden" runat="server" id="hidAreaID" value='<%#Eval("AreaID")%>' />
                                    <asp:Label ID="lblAreaCentre" runat="server" Text='<%#Eval("AreaCentre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>' Width="200px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zone">
                                <ItemTemplate>
                                    <input type="hidden" runat="server" id="hidZoneID" value='<%#Eval("ZoneID")%>' />
                                    <asp:Label ID="lblZone" runat="server" Text='<%#Eval("ZoneName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ContactNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch">
                                <ItemTemplate>
                                    <input type="hidden" runat="server" id="hidBranchID" value='<%#Eval("BranchID")%>' />
                                    <asp:Label ID="lblBranch" runat="server" Text='<%#Eval("BranchName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                        OnClick="lbtnEdit_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                        OnClick="lbtnDelete_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <table align="center" width="100%">
            <tr>
                <td colspan="100%">
                    &nbsp
                </td>
            </tr>
            <tr>
                
                <td align="left" width="100px">
                      &nbsp &nbsp Area Code
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAreaCode" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td align="left" width="100px">
                    Area Centre
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAreaCentre" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td align="left">
                    Address &nbsp
                </td>                
                <td align="left" style="width:210px;">
                    <asp:TextBox ID="txtAddress" runat="server" Width="190px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    &nbsp &nbsp Zone
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlZone" runat="server" Width="155px">
                    </asp:DropDownList>
                </td>
                <td align="left">
                    Contact NO
                </td>
                <td align="left">
                    <asp:TextBox ID="txtContactNo" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td align="left">
                    Branch
                </td>
               <td align="left">
                   <asp:DropDownList ID="ddlBranch" runat="server" Width="155px"></asp:DropDownList>
               </td> 
            </tr>
            <tr>
                <td colspan="100%" align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="return ValidateSave();"
                                OnClick="btnSave_Click" />
                            </td>
                            <td runat="server" id="tdUpdate">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClientClick="return ValidateSave();"
                                OnClick="btnUpdate_Click" Visible="false" />
                            </td>
                            <td runat="server" id="tdCancel">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>                   
                    
                    
                </td>
            </tr>
            <tr>
                <td align="center" colspan="100%">
                    <asp:Label ID="lblMsg" runat="server" EnableViewState="false" ForeColor="Blue"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <%--</table>--%>
    <input type="hidden" runat="server" id="hidAreaID" />
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
