﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class PackingDetails : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    trNav.Visible = false;
                hidUserID.Value = Session["UserID"].ToString();
                GetDefaults();
            }
            txtPack.Focus();
        }

        private void GetDefaults()
        {
            //Packing_BLL Obj_BLL = new Packing_BLL();
            //ddlItem.DataSource = Obj_BLL.GetStockItem();
            //ddlItem.DataTextField = "ItemCode";
            //ddlItem.DataValueField = "ICID";
            //ddlItem.DataBind();
            //ddlItem.Items.Insert(0, new ListItem("Select", "0"));
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SavePackDetails(string Name, string xmlDet, string UserID)
        {
            try
            {
                string res;
                Packing_BLL Obj_BLL = new Packing_BLL();
                res = Obj_BLL.SavePackDetails(Name, xmlDet, UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdatePackDetails(string PackID, string Name, string xmlDet, string UserID)
        {
            try
            {
                string res;
                Packing_BLL Obj_BLL = new Packing_BLL();
                res = Obj_BLL.UpdatePackDetails(PackID, Name, xmlDet, UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPDNavData(string PID, string Flag)
        {
            try
            {
                Packing_BLL Obj_Bll = new Packing_BLL();
                return Obj_Bll.GetPDNavData(PID, Flag).GetXml();
                
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeletePack(string PackID)
        {
            try
            {
                Packing_BLL Obj_BLL = new Packing_BLL();
                return Obj_BLL.DeletePack(PackID);
                
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemName(string ItemID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWiseTaxSystem(ItemID).GetXml();
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
                
        
                
                
            
        
               

        
