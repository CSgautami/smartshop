﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class StockGroup : GlobalPage
    {
        StockGroup_BLL Obj_BLL = new StockGroup_BLL();
        //public static string strID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }

           string strID = Request.QueryString["ID"];
            if (!IsPostBack)
            {
                txtStockGroup.Focus();
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
               // GetDefaults();

                if (strID != null && strID != "")
                {
                    trHeading.Visible = false;
                    trMenu.Visible = false;
                    tdCancel.Visible = false;
                    // ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + strID + "," + BranchID + ");", true);
                    strID = "";
                }
            }


        }
        //private void GetDefaults()
        //{
        //    //StockGroup_BLL Obj_BLL = new StockGroup_BLL();
        //    ddlHeadGroup.DataSource =Obj_BLL.GetHeadGroup();
        //    ddlHeadGroup.DataTextField = "HeadGroupName";
        //    ddlHeadGroup.DataValueField = "HeadGroupID";
        //    ddlHeadGroup.DataBind();
        //    ddlHeadGroup.Items.Insert(0, new ListItem("Select", "0"));
        //}
        void FillGrid()
        {
            try
            {
                //StockGroup_BLL Obj_BLL = new StockGroup_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetStockGroup(txtSearch.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvStockGroup.DataSource = ds;
                        gvStockGroup.DataBind();
                        gvStockGroup.Rows[0].Cells[1].Text = "";
                    }
                    else
                    {
                        gvStockGroup.DataSource = ds;
                        gvStockGroup.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            string res = Obj_BLL.InsertStockGroup(txtStockGroup.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Group";


            }
            else if (res == "-1")
            {
                lblMsg.Text = "GroupName already existed";
                txtStockGroup.Focus();

            }
            else if (res == "1")
            {
                lblMsg.Text = "Group saved successfully";
                FillGrid();
                txtStockGroup.Text = "";
                //ddlHeadGroup.SelectedIndex = 0;
                txtStockGroup.Focus();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateStockGroup(hidStockGroupID.Value,txtStockGroup.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Group";
                txtStockGroup.Focus();
                return;
            }
            else if (res == "-1")
            {
                lblMsg.Text = "GroupName already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "Group Updated successfully";
                FillGrid();
                txtStockGroup.Text = "";
                //ddlHeadGroup.SelectedIndex = 0;
                txtStockGroup.Focus();
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnCancel.Visible = true;
                FillGrid();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvStockGroup.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtStockGroup.Text = "";
            //ddlHeadGroup.SelectedIndex = 0;
            txtSearch.Text = "";
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidStockGroupID.Value = ((HtmlInputHidden)grow.FindControl("hidStockGroupID")).Value;
                txtStockGroup.Text = ((Label)grow.FindControl("lblStockGroup")).Text;
                //ddlHeadGroup.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidHeadGroupID")).Value;
                txtStockGroup.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidStockGroupID.Value = ((HtmlInputHidden)grow.FindControl("hidStockGroupID")).Value;
                StockGroup_BLL Obj_BLL = new StockGroup_BLL();
                string res = Obj_BLL.DeleteStockGroup(hidStockGroupID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Group";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "Group Deleted Successfully";
                    txtStockGroup.Text = "";
                    //ddlHeadGroup.SelectedIndex = 0;
                    txtStockGroup.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        protected void gvStockGroup_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStockGroup.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetStockGroupData(string SearchItem)
        //{
        //    try
        //    {
        //        StockGroup_BLL Obj_BLL = new StockGroup_BLL();
        //        return Obj_BLL.GetStockGroupData(SearchItem).GetXml();
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
    }
}



















