﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportOrderReport.aspx.cs" Inherits="MyAccounts20.ReportOrderReport" Title="Order Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Calendar.css" rel="stylesheet" type="text/css" />
    <script src="JS/CalendarPopup.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        document.write(getCalendarStyles());        
        var cal=new CalendarPopup("divCalendar");        
        cal.showNavigationDropdowns();
        function showcalendar(t)
        {
            cal.select(t,t.id,'dd-MM-yyyy');
        }  
        var preid;
        var dtpFrom;
        var dtpTo;
        var Status;
        var ddlBranch;
        preid='ctl00_ContentPlaceHolder1_';
        String.prototype.trim = function () {
        return this.replace(/^\s*/, "").replace(/\s*$/, "");
        }

        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }    
        }  
        function GetControls()
        {
            Status =document.getElementById(preid+"ddlShipmentDet");
            ddlBranch =document.getElementById(preid+"ddlBranch");
            dtpFrom =document.getElementById(preid+"txtFromDate");
            dtpTo =document.getElementById(preid+"txtToDate");                    
        }
//        function GetDefaults()
//        {    
//            dtpFrom.value = new Date().format("dd-MM-yyyy");
//            dtpTo.value=new Date().format("dd-MM-yyyy");             
//        }
        function SetData()
        {
            GetControls();
            //GetDefaults();
        }
        function btnShow_Click()
        {           
            window.frames["frmOrderReport"].location.href="Reports/OrderReport.aspx?FromDate="+ dtpFrom.value +"&ToDate="+dtpTo.value+"&Status="+Status.value+"&BranchID="+ddlBranch.value;
         }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" style="width: 900px">
            <tr>
                <td>&nbsp;</td>
            </tr>           
            <tr>
                <td>
                    Branch
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlBranch" runat="server" Width="200px">
                        </asp:DropDownList>
                </td>
                <td>
                    Status
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlShipmentDet" runat="server" Width="155px">
                        </asp:DropDownList>
                </td>
                <td>
                    From Date
                </td>
                <td align="left" style="width:100px;">
                    <input type="text" id="txtFromDate" runat="server" maxlength="10" style="width: 100px;"
                     onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"/>                     
                </td>
                <td>
                    To Date:
                </td>
                <td align="left">
                    <input type="text" id="txtToDate" runat="server" maxlength="10" style="width: 100px;" 
                    onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"/>
                </td>
                <td>
                    <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                </td>
            </tr>
            <tr>
                <td colspan="9">
                    <iframe name="frmOrderReport" id="frmOrderReport" style="width: 100%; height: 400px;"
                        frameborder="0" scrolling="auto"></iframe>
                </td>
            </tr>            
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
</asp:Content>
