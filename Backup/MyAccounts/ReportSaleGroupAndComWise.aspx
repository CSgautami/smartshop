﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportSaleGroupAndComWise.aspx.cs" Inherits="MyAccounts20.ReportSaleGroupAndComWise" Title="Sale Group And CompanyWise" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/Reports/SalesReturnReport.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
     function btnShow_Click()
    {
        preid = 'ctl00_ContentPlaceHolder1_';        
        var fromdt=document.getElementById(preid+"txtFromDate").value;
        var todt=document.getElementById(preid+"txtToDate").value;
        var Group=document.getElementById("ddlGroup").value;
        var Company=document.getElementById("ddlCompany").value;
        //var HeadGroup=document.getElementById(preid+"ddlHeadGroup").value;
        var Branch=document.getElementById("ddlBranch").value;
        window.frames["frmSaleReportGroupAndComWise"].location.href="Reports/SaleReportGroupAndComWise.aspx?FromDate="+fromdt+"&ToDate="+todt+"&Group="+Group+"&Company="+Company+"&Branch="+Branch  
        //ddlBranch.focus()
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">         
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>    
                                        <td align="left">Branch</td>
                                        <td align="left">
                                            <select id="ddlBranch" style="width:200px;"></select>
                                        </td>
                                        <%--<td>
                                            Head Group:
                                        </td>
                                        <td>
                                            <select id="ddlHeadGroup" style="width:300px;" onchange="GetStockGroup();" ></select>
                                        </td> --%>
                                        <td>
                                            Stock Group:
                                        </td>
                                        <td>
                                            <select id="ddlGroup" style="width:200px;" ></select>
                                        </td>
                                        <td>
                                            Company:
                                        </td>
                                        <td>
                                            <select id="ddlCompany" style="width:200px;" ></select>
                                        </td>          
                                    </tr>                                    
                                    <tr>                                        
                                        <td>
                                            From Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" id="txtFromDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" />
                                        </td>
                                        <td>
                                            To Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" id="txtToDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" />
                                        </td> 
                                        <td>&nbsp</td>                                        
                                        <td align="right">
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmSaleReportGroupAndComWise" id="frmSaleReportGroupAndComWise"
                                    style="width: 100%; height: 400px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" runat="server" id="hidUserID" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
