﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
namespace MyAccounts20
{
    public partial class CompanyMaster : GlobalPage
    {
        Company_BLL Obj_BLL = new Company_BLL();
        //public static string strID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            string strID = Request.QueryString["ID"];
            if (!IsPostBack)
            {
                txtCompanyName.Focus();
                FillGrid();
                GetDefaults();
                hidUserID.Value  = Session["UserID"].ToString();

                if (strID != null && strID != "")
                {
                    trHeading.Visible = false;
                    trMenu.Visible = false;
                    tdCancel.Visible = false;
                    //trFooter.Visible = false;
                    strID = "";
                }
            }
        }

        private void FillGrid()
        {
            try
            {
                Company_BLL Obj_BLL = new Company_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetCompany( txtSearch.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvCompany.DataSource = ds;
                        gvCompany.DataBind();
                        gvCompany.Rows[0].Cells[3].Text = "";
                    }
                    else
                    {
                        gvCompany.DataSource = ds;
                        gvCompany.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        
        private void GetDefaults()
        {
            ddlTaxSystem.DataSource = Obj_BLL.GetTaxSysyem();
            ddlTaxSystem.DataTextField = "TaxName";
            ddlTaxSystem.DataValueField = "TaxNo";
            ddlTaxSystem.DataBind();
            ddlTaxSystem.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertCompany(txtCompanyName.Text, txtAddress.Text, ddlTaxSystem.SelectedValue, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Company";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
                txtCompanyName.Focus();
            }
            else if (res == "1")
            {
                lblMsg.Text = "Company saved successfully";
                FillGrid();
                txtCompanyName.Text = "";
                txtAddress.Text = "";
                ddlTaxSystem.SelectedIndex = 0;
                txtCompanyName.Focus();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateCompany(hidCompanyID.Value, txtCompanyName.Text, txtAddress.Text, ddlTaxSystem.SelectedValue, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Company";
                
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
                txtCompanyName.Focus();
            }
            else if (res == "1")
            {
                lblMsg.Text = "Company Updated successfully";
                FillGrid();
                txtCompanyName.Text = "";
                txtAddress.Text = "";
                ddlTaxSystem.SelectedIndex = 0;
                txtCompanyName.Focus();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvCompany.PageIndex=0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtCompanyName.Text = "";
            txtAddress.Text = "";
            ddlTaxSystem.SelectedIndex = 0;
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidCompanyID.Value = ((HtmlInputHidden)grow.FindControl("hidCompanyID")).Value;
                txtCompanyName.Text = ((Label)grow.FindControl("lblCompanyName")).Text;
                txtAddress.Text = ((Label)grow.FindControl("lblAddress")).Text;
                ddlTaxSystem.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidTaxNo")).Value;
                txtCompanyName.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidCompanyID.Value = ((HtmlInputHidden)grow.FindControl("hidCompanyID")).Value;
                Company_BLL Obj_BLL = new Company_BLL();
                string res = Obj_BLL.DeleteCompany(hidCompanyID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Company";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "Company Deleted Successfully";
                    txtCompanyName.Text = "";
                    txtAddress.Text = "";
                    ddlTaxSystem.SelectedIndex = 0;
                    txtCompanyName.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        protected void gvCompany_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            gvCompany.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
    }
}
    
        
    


        
    
        
    

