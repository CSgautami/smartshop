﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using BLL;
namespace MyAccounts20
{
    public partial class ShopingCart : System.Web.UI.Page
    {
        ItemCreation_BLL bllHome = new ItemCreation_BLL();
        ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
        DataTable dt = new DataTable("ShopingCart");
        string ItemID;
        string ItemName;
        string MRP;
        string Price;
        string Qty;
        string Amount;
        decimal TotAmt=0;
        int i;
        string TempID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {                
                if (!IsPostBack)
                {                   
                    hidUserID.Value = Session["UserID"].ToString();
                    if (Session[MyAccountsSession.TempOrderID] != null)
                        hidTempID.Value = Session[MyAccountsSession.TempOrderID].ToString();
                    if (Request.QueryString["GID"] != null)
                        FillDisplayGroups(Request.QueryString["GID"].ToString());
                    else
                        FillDisplayGroups("16");
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);                                        
                }
            }
            catch
            {
                throw;
            }
        }        
        void FillDisplayGroups(string StockGroupID)
        {
            try
            {
                DataSet ds = bllHome.GetDesiplayGroups(StockGroupID);
                dlItemDet.DataSource = ds;
                dlItemDet.DataBind();
                CheckAvailabity();
            }
            catch
            {
                throw;
            }
        }
        void CheckAvailabity()
        {
            foreach (DataListItem dl in dlItemDet.Items)
            {
                int dlItemID = Convert.ToInt32(((HtmlInputHidden)dl.FindControl("hidID")).Value);
                decimal Qty;
                Qty = Convert.ToDecimal(obj_BLL.GetStockAvailability(dlItemID).Tables[0].Rows[0]["BalQty"].ToString());
                if (Qty > 0)
                {
                    //((Image)dl.FindControl("imgA")).Visible = true;
                    ((CheckBox)dl.FindControl("chkAvailable")).Visible = true;
                    ((CheckBox)dl.FindControl("chkAvailable")).Checked = true;
                    //((CheckBox)dl.FindControl("chkAvailable")).Enabled = false;
                    ((Image)dl.FindControl("imgNA")).Visible = false;
                    ((HtmlInputText)dl.FindControl("txtQty")).Disabled = false;
                   // ((TextBox)dl.FindControl("txtQty")).Enabled = true;
                }
                else
                {
                    //((Image)dl.FindControl("imgA")).Visible = false;
                    ((CheckBox)dl.FindControl("chkAvailable")).Visible = false;
                    ((Image)dl.FindControl("imgNA")).Visible = true;
                    ((HtmlInputText)dl.FindControl("txtQty")).Disabled = true;
                    //((TextBox)dl.FindControl("txtQty")).Enabled = false;
                }                
            }            
        }               
        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            if(Session[MyAccountsSession.TempOrderID]!=null)
                Response.Redirect("ConfirmPage.aspx", true);
            //dt = (DataTable)Session[MyAccountsSession.UserCart];
            //if (dt != null)
            //{
            //    DataSet ds = new DataSet();
            //    ds.Tables.Add(dt.Copy());
            //    ds.DataSetName = "XML";
            //    ds.Tables[0].TableName = "ShopingCart";
            //    string TempOrderID = null;
            //    string UserID = "0";
            //    if (Session[MyAccountsSession.TempOrderID] != null)
            //        TempOrderID = Session[MyAccountsSession.TempOrderID].ToString();
            //    if (Session[MyAccountsSession.UserID] != null)
            //        UserID = Session[MyAccountsSession.UserID].ToString();
            //    string res = obj_BLL.SaveTempOrder(ds.GetXml(), TempOrderID, UserID);
            //    if (res != "" && int.Parse(res) > 0)
            //    {
            //        Session[MyAccountsSession.TempOrderID] = res;
            //        Response.Redirect("ConfirmPage.aspx", true);
            //    }
            //}
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveTempOrder(string XmlString,string TempID,string UserID)
        {
            try
            {
                string res="";
                ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
                //res = obj_BLL.SaveTempOrder(XmlString,TempID, UserID);
                HttpContext.Current.Session[MyAccountsSession.TempOrderID] = res;
                return res;
                
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }        
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string TempID)
        {
            try
            {
                ShopingCart_BLL obj_Bll = new ShopingCart_BLL();
                return obj_Bll.GetData(TempID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeleteTempOrder(string ItemID,string TOID)
        {
            try
            {
                ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
                return obj_BLL.DeleteTempOrder(ItemID,TOID).GetXml();
            }
            catch
            {
                throw;
            }
        }

          
    }
}
