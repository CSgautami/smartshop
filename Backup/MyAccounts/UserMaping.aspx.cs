﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class UserMaping : GlobalPage
    {
        UserMaping_BLL obj_BLL = new UserMaping_BLL();
        DataSet dsControls;
        string UserID;
        DataTable dttemp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            UserID = Session["UserID"].ToString();
            if (!IsPostBack)
            {
                FillData();
            }
            FillControls();
        }

        void FillData()
        {
            try
            {

                chkLBranch.DataSource = obj_BLL.GetBranch().Tables[0];
                chkLBranch.DataTextField = "BranchName";
                chkLBranch.DataValueField = "BranchID";
                chkLBranch.DataBind();


                ddlUser.DataSource = obj_BLL.GetUser();
                ddlUser.DataTextField = "UserName";
                ddlUser.DataValueField = "UserID";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("Select", "0"));

                //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            }
            catch
            {
                throw;
            }
        }

        void FillControls()
        {
            try
            {
                phPages.Controls.Clear();
                dsControls = obj_BLL.GetPages();
                Table tbl = new Table();
                tbl.Attributes["border"] = "1";
                TableRow tr = new TableRow();
                TableCell tc = new TableCell();
                int cnt = 0;
                if (dsControls != null && dsControls.Tables.Count > 1)
                {
                    foreach (DataRow dr in dsControls.Tables[0].Rows)
                    {
                        if (cnt % 4 == 0)
                        {
                            if (cnt != 0)
                                tbl.Rows.Add(tr);
                            tr = new TableRow();
                        }
                        tc = new TableCell();

                        Panel pnl = new Panel();

                        pnl.Height = new Unit(125);
                        pnl.Width = new Unit(190);
                        pnl.ScrollBars = ScrollBars.Auto;

                        tc.Controls.Add(pnl);

                        Label lbl = new Label();
                        lbl.Style["text-decoration"] = "underline";
                        lbl.Text = "<b>" + dr["MainMenu"].ToString() + "</b><br/>";
                        pnl.Controls.Add(lbl);

                        CheckBoxList chkl = new CheckBoxList();
                        chkl.ID = "chkl" + dr["ReportType"].ToString();
                        dsControls.Tables[1].DefaultView.RowFilter = "ReportType=" + dr["ReportType"].ToString();
                        chkl.DataSource = dsControls.Tables[1].DefaultView.ToTable();
                        chkl.DataTextField = "Menu";
                        chkl.DataValueField = "PageID";
                        chkl.DataBind();
                        pnl.Controls.Add(chkl);
                        tc.HorizontalAlign = HorizontalAlign.Left;
                        tc.VerticalAlign = VerticalAlign.Top;
                        tr.Cells.Add(tc);
                        cnt++;
                    }
                }
                tbl.Rows.Add(tr);
                phPages.Controls.Add(tbl);
            }
            catch
            {
            }
        }

        protected void ddlUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                if (ddlUser.SelectedValue != "0")
                    GetData();
            }
            catch
            {
            }
        }

        void GetData()
        {
            try
            {
                DataSet dsUserData = obj_BLL.GetUserData(ddlUser.SelectedValue);
                if (dsUserData != null && dsUserData.Tables.Count > 1)
                {
                    foreach (DataRow drow in dsUserData.Tables[0].Rows)
                    {
                        foreach (ListItem li in chkLBranch.Items)
                        {
                            if (li.Value == drow["BranchID"].ToString())
                            {
                                li.Selected = true;
                                break;
                            }
                        }
                    }
                    foreach (DataRow drow in dsUserData.Tables[1].Rows)
                    {
                        CheckBoxList chkl = (CheckBoxList)phPages.FindControl("chkl" + drow["ReportType"].ToString());
                        if (chkl != null)
                        {
                            dsUserData.Tables[2].DefaultView.RowFilter = "ReportType=" + drow["ReportType"].ToString();
                            dttemp = dsUserData.Tables[2].DefaultView.ToTable();
                            foreach (DataRow dr in dttemp.Rows)
                            {
                                foreach (ListItem li in chkl.Items)
                                {
                                    if (li.Value == dr["PageID"].ToString())
                                    {
                                        li.Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch
            {
                throw;
            }
        }

        void ClearControls()
        {
            try
            {
                if (dsControls != null && dsControls.Tables.Count > 1)
                {
                    foreach (DataRow dr in dsControls.Tables[0].Rows)
                    {

                        CheckBoxList chkl = (CheckBoxList)phPages.FindControl("chkl" + dr["ReportType"].ToString());
                        if (chkl != null)
                        {
                            chkl.SelectedIndex = -1;
                        }
                    }
                }
                chkLBranch.SelectedIndex = -1;
            }
            catch
            {
                throw;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlUser.SelectedValue == "0")
                {
                    lblMessage.Text = "Selecte User";
                    return;
                }
                if (chkLBranch.SelectedIndex < 0)
                {
                    lblMessage.Text = "Selecte at least one branch.";
                    return;
                }
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds.DataSetName = "XML";
                dt.TableName = "UserPages";
                dt.Columns.Add("PageID", typeof(string));

                if (dsControls != null && dsControls.Tables.Count > 1)
                {
                    foreach (DataRow dr in dsControls.Tables[0].Rows)
                    {
                        CheckBoxList chkl = (CheckBoxList)phPages.FindControl("chkl" + dr["ReportType"].ToString());
                        if (chkl != null)
                        {
                            foreach (ListItem li in chkl.Items)
                            {
                                if (li.Selected == true)
                                    dt.Rows.Add(li.Value);
                            }
                        }
                    }
                }
                ds.Tables.Add(dt);
                dt = new DataTable();
                dt.TableName = "Branches";
                dt.Columns.Add("BranchID", typeof(string));
                foreach (ListItem li in chkLBranch.Items)
                {
                    if (li.Selected == true)
                        dt.Rows.Add(li.Value);
                }
                ds.Tables.Add(dt);
                obj_BLL.SavePages(ddlUser.SelectedValue, ds.GetXml());
                lblMessage.Style["color"] = "green";
                lblMessage.Text = "UserPages Saved Successfully.";
                //ClearControls();
            }
            catch (Exception ex)
            {
                lblMessage.Style["color"] = "red";
                lblMessage.Text = ex.Message;
            }
        }

    }
}
