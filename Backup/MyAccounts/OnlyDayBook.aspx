﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlyDayBook.aspx.cs" Inherits="MyAccounts20.OnlyDayBook" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DayBook</title>
    <link href="MyAccounts20.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
    var xmlObj; 
    function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    
    } 
    function GetDayBook()
    {
        //var UserID=document.getElementById("hidUserID");
        var BranchID=document.getElementById("hidBranchID");        
        var dbeDate=document.getElementById("hidDBEDate");
        PageMethods.GetDayBookReport(BranchID.value,dbeDate.value,GetDayBookComplete);
    }

    function GetDayBookComplete(res)
    {
        
        getXml(res);
        var tbl=document.getElementById("tblGrid");
        ClearTable(tbl);
        tbl.rows[0].cells[0].style.display="none";
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            var tr=tbl.insertRow(i+1);
            tr.insertCell(0);
            tr.insertCell(0);
            tr.insertCell(0);
            tr.insertCell(0);
            tr.insertCell(0);
            tr.insertCell(0);
            tr.insertCell(0);
            tr.insertCell(0);
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TransDate")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TransDate")[0].firstChild!=null)
                    tr.cells[0].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TransDate")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0].firstChild!=null)
                    tr.cells[1].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Ledger")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Ledger")[0].firstChild!=null)
                    tr.cells[2].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Ledger")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0].firstChild!=null)
                    tr.cells[3].innerHTML="<div style='overflow:hidden; width:210px;' title='"+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0].firstChild.nodeValue+"'>"+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0].firstChild.nodeValue+"</div>";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("InAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("InAmount")[0].firstChild!=null)
                    tr.cells[4].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("InAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("OutAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("OutAmount")[0].firstChild!=null)
                    tr.cells[5].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("OutAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CreditAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CreditAmount")[0].firstChild!=null)
                    tr.cells[6].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CreditAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DebitAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DebitAmount")[0].firstChild!=null)
                    tr.cells[7].innerText=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DebitAmount")[0].firstChild.nodeValue;
            tr.cells[0].style.display="none";
            tr.cells[3].noWrap=true;
            tr.cells[4].style.textAlign="right";
            tr.cells[5].style.textAlign="right";
            tr.cells[6].style.textAlign="right";
            tr.cells[7].style.textAlign="right";
            
        }
        document.getElementById("divDayBook").scrollTop=tbl.offsetHeight;
    }

    function ClearTable(tbl)
    {
        for (var i = tbl.rows.length - 1; i > -0; i--) {
            tbl.deleteRow(i);
        } 
    }
    </script>

</head>
<body onload="GetDayBook();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="SM1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div>
        <table width="890px">
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top">
                    <div class="divgrid" style="height: 600px;" id="divDayBook">
                        <table id="tblGrid">
                            <tr class="dbookheader">
                                <th style="width: 90px;">
                                    Date
                                </th>
                                <th style="width: 50px;">
                                    Voch
                                </th>
                                <th style="width: 200px;">
                                    Name
                                </th>
                                <th style="width: 220px;">
                                    Particulars
                                </th>
                                <th style="width: 80px;">
                                    InAmt
                                </th>
                                <th style="width: 80px;">
                                    OutAmt
                                </th>
                                <th style="width: 80px;">
                                    Credit
                                </th>
                                <th style="width: 80px;">
                                    Debit
                                </th>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="hidDBEDate" runat="server" />
    <input type="hidden" id="hidUserID" runat="server" />
    <input type="hidden" id="hidBranchID" runat="server" />
    </form>
</body>
</html>
