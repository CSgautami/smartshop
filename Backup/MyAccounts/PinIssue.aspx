﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="PinIssue.aspx.cs" Inherits="MyAccounts20.PinIssue" Title="Pin Issue"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
    var tblGrid;
    var ddlPinNo;
    var ddlCustomer;
    var preid;
    var UserID;
    var lblCusName;
    //var ddlPinNo;
    var lblPinValue;
    preid='ctl00_ContentPlaceHolder1_';
    
    function getXml(xmlString)
     {
        xmlObj=null;
        try {
            var browserName = navigator.appName;
            if (browserName == "Microsoft Internet Explorer") {
                xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                xmlObj.async = "false";
                xmlObj.loadXML(xmlString);
            }
            else {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function GetControls()
    {
        ddlPinNo = document.getElementById(preid+ "ddlPinNo");
        ddlCustomer = document.getElementById(preid+ "ddlCustomer");
        UserID =document.getElementById(preid+"hidUserID");
        tblGrid =document.getElementById("tblGrid");
        lblCusName =document.getElementById(preid+ "lblName");
        lblPinValue=document.getElementById(preid+ "lblPinValue");
        //ddlPinNo=document.getElementById(preid+ "ddlPinNo");
    }
    function GetPins()
    {        
        PageMethods.GetPins(GetPinsComplete);
    }
    function GetPinsComplete(res)
    {
        getXml(res);
        ddlPinNo=document.getElementById(preid+"ddlPinNo");
        ddlPinNo.options.length=0;
         var opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";    
        ddlPinNo.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PinNumber")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PinID")[0].firstChild.nodeValue;
            ddlPinNo.options.add(opt);
        }    
        
    }
    function btnSave_Click(e,t)
    {
        GetControls();
        if(ddlCustomer.value=="0")
        {
            alert("select Customer");
            ddlCustomer.focus();
            return false;
        }        
        if(tblGrid.rows.length<=2)
        {
            alert("Enter Pin Numbers");
            ddlPinNo.focus();
            return false;
        }
        var xmlDet="<XML>";
        for(var i=1;i<tblGrid.rows.length;i++)
        {
            xmlDet=xmlDet+"<PinDet>";
            var tr=tblGrid.rows[i];            
            xmlDet=xmlDet+"<PinID>"+tr.cells[0].innerHTML.trim()+"</PinID>";
            xmlDet=xmlDet+"</PinDet>";
        }
        xmlDet=xmlDet+"</XML>";
        PageMethods.SavePins(ddlCustomer.value,xmlDet,UserID.value,SaveComplete);
    }
    function SaveComplete(res)
    {
        if(res=="0" || res=="")
        {
            alert("Error while Saving");        
        }
        else if(res.substring(0,1)=="E")
        {
            alert(res);
        }    
        else
        {
            alert("Saved successfully ");
            Clearfields(); 
        }
    }    
    function btnAdd_onclick()
    {
        GetControls();
        if(ddlPinNo.value=="0")
        {
            alert("Select Pin");
            ddlPinNo.focus();
            return false;
        }
        tblGrid =document.getElementById("tblGrid");
        var i;
        i=tblGrid.rows.length;
        var tr=tblGrid.insertRow(i);    
        for(i=0;i<3;i++)
        {
            tr.insertCell(i);
        }
        tr.cells[0].innerHTML=ddlPinNo.value;
        tr.cells[0].style.display="none";
        tr.cells[1].innerHTML=ddlPinNo[ddlPinNo.selectedIndex].innerText; 
        tr.cells[2].innerHTML=lblPinValue.innerText;
        tr.cells[2].style.display="none";
             
        ddlPinNo.value="0";
        lblPinValue.innerText="";
        tr.onmouseover=function(){MouseOver(this);};
        tr.onmouseout=function(){MouseOut(this);};
        tr.onclick=function(){tblClick(this);}                 
        ddlPinNo.focus();
    }
    function MouseOver(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {
            t.style.backgroundColor="gray";
            t.style.cursor="hand";     
            t.title="Click to edit the values";  
        }
    }

    function MouseOut(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {
            t.style.backgroundColor="";
            t.style.crusor="text";        
        }
    }


    function tblClick(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {        
            ddlPinNo.value=t.cells[0].innerHTML.trim();
            lblPinValue.innerHTML=t.cells[2].innerHTML.trim();
            tblGrid.deleteRow(t.rowIndex);
        }
    }

    function Clearfields()
    {
        GetControls();
        ddlCustomer.value="0";
        for(var i=tblGrid.rows.length-1;i>0;i--)
        {
           tblGrid.deleteRow(i);
        }
        GetPins();
        ddlPinNo.value="0";
        lblCusName.innerText="";
        lblPinValue.innerText="";
    }
    function GetName()
    {
        lblCusName.innerText="";
        if(ddlCustomer.value!="0")
            PageMethods.GetCustomerName(ddlCustomer.value,GetCustomerNameComplete);
    }
    function GetCustomerNameComplete(res)
    {
        lblCusName.innerText=res;
    }
    function GetPinValue()
    {
        lblPinValue.innerText="";
        if(ddlPinNo.value!="0")
        PageMethods.GetPinValue(ddlPinNo.value,GetPinValueComplete);
    }
    function GetPinValueComplete(res)
    {
        lblPinValue.innerText=res;
    }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 600px;">
            <tr>
                <td colspan="100%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 150px;">
                </td>
                <td align="left" style="width: 100px;">
                    Customer
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlCustomer" runat="server" Width="100px" onchange="GetName();">
                    </asp:DropDownList>
                    &nbsp &nbsp
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <table width="300px" align="center">
                        <tr>
                            <td colspan="6" align="left">
                                <div class="divgrid" style="height: 150px; border: solid 1px #98bf21;">
                                    <table id="tblGrid">
                                        <tr class="dbookheader" align="left">
                                            <th style="display: none">
                                                PinID
                                            </th>
                                            <th style="width: 200px;" align="left">
                                                Pin No
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="100%">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center" style="width:500;height:20px" >
                <asp:Label ID="lblPinValue" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table width="610px">
            
            <tr>
                <td style="width: 150px;">
                </td>
                <td align="left" style="width: 100px;">
                    &nbsp; Pin No
                </td>
                <td align="left" style="width: 200px;">
                    <asp:DropDownList ID="ddlPinNo" runat="server" onchange="GetPinValue();" Width="170px">
                    </asp:DropDownList>
                    <%--<asp:Label ID="lblPinValue" runat="server"></asp:Label>--%>
                </td>
                <td align="left">
                    <input type="button" id="btnAdd" value="Add" runat="server" onclick="return btnAdd_onclick()" />
                </td>
            </tr>
            <tr>
                <td colspan="100%">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td align="center" colspan="100%">
                    <input type="button" id="btnSave" value="Save" onclick="btnSave_Click(event,this);" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <input type="hidden" id="hidUserID" runat="server" />
    </div>
</asp:Content>
