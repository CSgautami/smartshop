﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;

namespace MyAccounts20
{
    public partial class ReportMyAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
            hidUserID.Value = Session["UserID"].ToString();
            if (Session["Type"].ToString() != "Customer")
            {
                Login_BLL bllLogin = new Login_BLL();
                string res = bllLogin.CheckAuthorization(Session["UserID"].ToString(), Request.Url.AbsolutePath.Substring(1).ToLower());
                if (res != "1")
                {
                    Response.Redirect("Login.aspx", true);
                }
            }
            if (Session["CustomerPay"] != null)
            {
                //DataSet ds = Session["CustomerPay"].ToString();
                hidXmlString.Value = Session["CustomerPay"].ToString();
            }
            else
                hidXmlString.Value = "";
            ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            if (!IsPostBack)
            {

                //GetMember();
                if (Session["Type"].ToString() != "Customer" || Session["UserID"].ToString() == "0")
                    GetMember();
                else
                {
                    trMember.Visible = false;
                    chkPay.Visible = false;
                }                
              
                //txt = DateTime.Today.AddDays(-(DateTime.Now.DayOfWeek - System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)).ToString("dd-MM-yyyy");
                //txtTo.Text = DateTime.Today.AddDays(6 - (DateTime.Now.DayOfWeek - System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)).ToString("dd-MM-yyyy");
            }
        }
        public void GetMember()
        {
            try
            {
                //Reports_BLL obj_BLL = new Reports_BLL();
                //ddlMember.DataSource = obj_BLL.GetMember().Tables[0];
                //ddlMember.DataTextField = "YourID";
                //ddlMember.DataValueField = "LedgerID";
                //ddlMember.DataBind();
                //ddlMember.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //protected void btnPay_click()
        //{
        //}
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SavePayData(string FromDate, string ToDate, string PayDate, string UserID, string xmlDet)
        {
            string res="";
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                res = obj_BLL.SavePayData(FromDate,ToDate,PayDate,UserID, "1",xmlDet);
            }
            catch (Exception ex)
            {
                res = "";
                throw;
            }
            return res;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCustomerName(string LedgerID)
        {
            string res = "";
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.GetLedgerDetails(LedgerID).Tables[0].Rows[0]["LedgerName"].ToString();
            }
            catch (Exception ex)
            {
                res = "";
                throw;
            }
            return res;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillMember(string prefix)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.CallFillMember(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            Reports_BLL obj_BLL = new Reports_BLL();
            string xmlDet; string Phone; string strMessage;
            string Ledger; decimal Amount=0;
            if (Session["CustomerPay"] != null)
            {
                xmlDet = Session["CustomerPay"].ToString();
                xmlDet = xmlDet.Replace("NewDataSet", "XML");
                xmlDet = xmlDet.Replace("Table", "CustomerPayments");
                obj_BLL.SavePayData(dateFormat(txtFromDate.Text), dateFormat(txtToDate.Text), dateFormat(txtPayDate.Text), hidUserID.Value, "1", xmlDet);

                DataSet ds = obj_BLL.GetPaySMSMembers(dateFormat(txtFromDate.Text), dateFormat(txtToDate.Text), dateFormat(txtPayDate.Text));
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Phone = dr["Phone"].ToString();
                        Ledger = dr["LedgerName"].ToString();
                        Amount = decimal.Parse(dr["LeftPaid"].ToString());
                        //Phone=Response.Write(Phone.Replace(", " , ""));                    
                        //char [] chartotrim={',' , ' '};
                        //Phone = Phone.TrimStart(chartotrim);
                        //Phone = Response.Write(Phone.Remove(", "));
                        string[] ary = Phone.Split(',');
                        if (ary.Length > 1)
                            Phone = ary[1];
                        strMessage = "Congratulations!!" + Ledger + " You got Rs." + Amount + "towards   products promotion. Keep on promoting to increase your earnings.";
                        if (Phone.ToString() != "" && Amount != 0)
                        {
                            SMS obj = new SMS();
                            obj.SendSMS("SmartHomeShop", "smart33399", Phone, strMessage, "N");
                        }

                    }
                }
            }            
        }
      
    }
}


