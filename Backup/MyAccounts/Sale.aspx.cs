﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class Sale : GlobalPage
    {
        Sale_BLL obj_BLL = new Sale_BLL();        
        DataTable dt = new DataTable();
        Login_BLL bllLogin = new Login_BLL();
        //public static string strID;
        //public static string BranchID;
        protected void Page_Load(object sender, EventArgs e)
        {   
            //strPer = MyAccounts20Session.UserPermissionPages(Session["UserID"].ToString());
            //UserPermissionPages();
            if (Session["UserID"] == null )
            {
                Response.Redirect("Login.aspx");
            }           
           //trHeading.Visible = false;
           //trMenu.Visible = false;
           string strID = Request.QueryString["ID"];
           string BranchID = Request.QueryString["BranchID"];            
            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    trNav.Visible = false;
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                ddlMode.Items.Insert(0, new ListItem("Cash", "Cash"));
                ddlMode.Items.Insert(1, new ListItem("Credit", "Credit"));
                GetPackData();
                //txtInvNo.Focus();
                txtBarCode.Focus();
                if (hidUserID.Value != "0")
                {
                    txtMrp.Enabled = false;
                    txtPrice.Enabled = false;
                    txtDiscPer.Enabled = false;
                    txtCashDisc.Enabled = false;
                    ddlTaxSystem.Enabled = false;
                    //txtMrp.ReadOnly = true; 
                }
                if (strID != null && strID != "")
                {
                    trHeading.Visible = false;
                    trMenu.Visible = false;
                    trNav.Visible = false;
                    tdCancel.Visible = false;
                    tdDelete.Visible = false;
                    tdSave.Visible = false;
                    
                    
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + strID + "," + BranchID + ");", true);                 
                    strID = "";
                }                
            }
        }
        
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        void GridHeadings()
        {            
            dt.Columns.Add("Stock Item");
            dt.Columns.Add("Batch No");
            dt.Columns.Add("Qty");
            dt.Columns.Add("MRP");
            dt.Columns.Add("Price");
            dt.Columns.Add("Disc");
            dt.Columns.Add("C.Disc");
            dt.Columns.Add("Tax System");
            dt.Columns.Add("Unit Price");
            dt.Columns.Add("Amount");
            dt.Columns.Add("Vat Amount");
            //gvDetails.DataSource = dt;
            //gvDetails.DataBind();
        }
        public void GetPackData()
        {
            ddlPack.DataSource = obj_BLL.GetPackData();
            ddlPack.DataTextField = "PackName";
            ddlPack.DataValueField = "PackID";
            ddlPack.DataBind();
            ddlPack.Items.Insert(0, new ListItem("Select", "0"));
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerName(string BranchID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetLedgerName(BranchID).GetXml();
                //ddlName.DataSource= obj_BLL.GetLedgerName(ddlBranch.SelectedValue).Tables[0];
                //ddlName.DataTextField = "YourID";
                //ddlName.DataValueField = "LedgerID";                                
                //ddlName.DataBind();
                //ddlName.Items.Insert(0, new ListItem("Select", "0"));

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerDetails(string LedgerID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetLedgerDetails(LedgerID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAccLedger(string BranchID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetAccLedger(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem()
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetStockItem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetTaxSystem()
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetTaxSystem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStock(string ItemID, string BatchNo, string UDate, string BranchID, string TypeOfPrice)
        {
            try
            {
                Sale_BLL obj_Bll = new Sale_BLL();
                return obj_Bll.GetStock(ItemID, BatchNo, dateFormat(UDate), BranchID, TypeOfPrice).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWiseTaxSystem(string ItemID)
        {            
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWiseTaxSystem(ItemID).GetXml();
            }
            catch
            {
                throw;
            }            
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBatchNo(string ItemID, string BranchID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetBatchNo(ItemID, BranchID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveSaleInvoice(string hidInvID, string hidUInvID, string SInvNo, string Date, string DCNo, string DCDate, string LNo, string Address, string Mode, string TypeOfPrice, string Days, string Note, string PackID, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj,string RedemAmt, string NetAmt, string nTotMargin, string BranchID,string UserID, string XmlString)                       
        {
            try
            {
                string res;
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.SaveSaleInvoice(hidInvID, hidUInvID, SInvNo, dateFormat(Date), DCNo, dateFormat(DCDate), LNo, Address, Mode, TypeOfPrice,Days, Note, PackID, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj,RedemAmt, NetAmt, nTotMargin, BranchID, UserID, XmlString);
                if (res != "")
                {
                    string resSMS;
                    resSMS = obj_BLL.GetSMSMembers(LNo);                 

                    if (resSMS == "1")
                    {
                        string Phone; string CID; string strMessage;
                        DataSet ds = obj_BLL.GetLedgerDetails(LNo);
                        if (ds.Tables.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                Phone = dr["Phone"].ToString();
                                //char[] chartotrim = { ',', ' ' };
                                //Phone = Phone.TrimStart(chartotrim);
                                string[] ary = Phone.Split(',');
                                if (ary.Length > 1)
                                    Phone = ary[1];
                                CID = dr["YourID"].ToString();
                                strMessage = "Welcome to Smart Retailer. Your Customer Id. " + CID + " . Purchase regularly and enjoy the discounts. Promote the products and get paid for it. ";
                                if (Phone.ToString() != "")
                                {
                                    SMS obj = new SMS();
                                    obj.SendSMS("SmartHomeShop", "smart33399", Phone, strMessage, "N");
                                }

                            }
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdateSaleInvoice(string hidInvID, string hidUInvID, string SInvNo, string Date, string DCNo, string DCDate, string LNo, string Address, string Mode, string TypeOfPrice, string Days, string Note, string PackID, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj,string RedemAmt, string NetAmt, string nTotMargin, string BranchID, string UserID, string XmlString)
        {
            try
            {
                string res;
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.UpdateSaleInvoice(hidInvID, hidUInvID, SInvNo, dateFormat(Date), DCNo, dateFormat(DCDate), LNo, Address, Mode, TypeOfPrice, Days, Note, PackID, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj,RedemAmt, NetAmt, nTotMargin, BranchID, UserID, XmlString);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static decimal GetTax(string tax)
        {
            decimal nTax;
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                nTax = Convert.ToDecimal(obj_BLL.GetTax(tax).Tables[0].Rows[0]["vatrate"].ToString());
            }
            catch
            {
                nTax = 0;
            }
            return nTax;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string BranchID, string SInvID, string Flag)
        {
            try
            {
                Sale_BLL  obj_Bll= new Sale_BLL();
                return obj_Bll.GetData(BranchID, SInvID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetSearchInvoiceData(string BranchID, string SInvID)
        {
            try
            {
                Sale_BLL obj_Bll = new Sale_BLL();
                return obj_Bll.GetSearchInvoiceData(BranchID, SInvID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnDelete_Click(string InvID,string BranchID)
        {
            try
            {
                Sale_BLL obj_BAL = new Sale_BLL();
                return obj_BAL.DeleteSale(InvID,BranchID);
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string AddPackDataInSales(string PackID, string TypeOfPrice)
        {
            try
            {
                Sale_BLL obj_BAL = new Sale_BLL();
                return obj_BAL.AddPackDataInSales(PackID, TypeOfPrice).GetXml();
            }
            catch
            {
                throw;
            }
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (hidSInvID.Value != "0")
            {
                Response.Redirect("BillFormat.aspx?SInvID=" + hidSInvID.Value );
            }
        }
        //protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GetLedgers();
        //}    
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillLedger(string BranchID, string prefix)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.CallFillLedger(BranchID, prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string FillUserLedger()
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.FillUserLedger().GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWithBarCode(string BarCode)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWithBarCode(BarCode).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPriceAndMRP(string ItemID,string TypeOfPrice)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetPriceAndMRP(ItemID,TypeOfPrice).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemEffectivePrice(string ItemID, string BatchNo, string TypeOfPrice)
        {
            try
            {
                Sale_BLL obj_Bll = new Sale_BLL();
                return obj_Bll.GetEffPrice(ItemID, BatchNo,TypeOfPrice).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
