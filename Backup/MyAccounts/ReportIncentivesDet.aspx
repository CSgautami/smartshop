﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportIncentivesDet.aspx.cs" Inherits="MyAccounts20.ReportIncentivesDet" Title="Incentives Detail Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript" >
    var hidFDate;
    var hidTDate;
    var preid='ctl00_ContentPlaceHolder1_';
    function SetData()
    {
        hidFDate=document.getElementById(preid+"hidFDate");
        hidTDate=document.getElementById(preid+"hidTDate");
        window.frames["frmIncentivesDet"].location.href="Reports/IncentivesDetReport.aspx?Fdt="+hidFDate.value+"&Tdt="+hidTDate.value ;
    }    
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border:solid 1px black;">          
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">                                    
                                    <tr>                                       
                                                                                                                                                        
                                        <td>
                                        <%--    <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />--%>
                                        </td>                                                                                                                       
                                    </tr>                                                      
                                </table>                             
                            </td>                            
                        </tr>    
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>                                        
                                        <td>                                            
                                            <iframe name="frmIncentivesDet" id="frmIncentivesDet" style="width: 100%; height: 400px;"
                                                frameborder="0" scrolling="auto"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                    
                    </table>
                </td>
            </tr>            
        </table>
 </div>        
 <div>
 <input type="hidden" id="hidFDate" runat="server" />
 <input type="hidden" id="hidTDate" runat="server" />
 </div>
</asp:Content>
