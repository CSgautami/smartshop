﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="MyAccounts20.Category" Title="Category" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
  var Category; 
  var HeadGroupID; 
  var preid = 'ctl00_ContentPlaceHolder1_';
          function ValidateSave()
          {
              try
              {
                 Category=document.getElementById(preid+"txtCategory"); 
                 HeadGroupID=document.getElementById(preid+"ddlHead");                        
                 if(Category.value.trim()=="")
                 {
                     alert("Please Enter Category");
                     Category.focus();
                     return false;
                 }
                 if(HeadGroupID.value=="0")
                 {
                     alert("Select HeadGroup");
                     HeadGroupID.focus();
                     return false;
                 }


              return true;
               }
              catch(err)
               {
              return false;
               }
             }
     function ConfirmEdit()
     {
        if(!confirm("Do you want to Edit?"))
           return false;
     }
     function ConfirmDelete()
     {
        if(! confirm("Do you want to Delete?"))
           return false;
     } 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center" width="100%">
     <tr>
         <td>
             <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="false" CssClass="grid"
              PageSize="13" AllowPaging="true" Font-Size="Small" OnPageIndexChanging="gvCategory_PageIndexChanging"  width="80%" >
                    <Columns>
                         <asp:TemplateField HeaderText="HeadGroup">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidHeadID" value='<%#Eval("HeadGroupID")%>' />
                                <asp:Label ID="lblHead" runat="server" Text='<%#Eval("HeadGroupName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidCategoryID" value='<%#Eval("CategoryID")%>' />
                                <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                    OnClick="lbtnEdit_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                    OnClick="lbtnDelete_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
             </asp:GridView>
         </td>
     </tr>
</table>
<table align="center">
            
        <tr>  
          <td align="left">
               Category
          </td>
          <td>
               <asp:TextBox ID="txtCategory" runat="server" Width="150px"></asp:TextBox>
          </td>
      </tr>
      <td align="left">
              HeadGroup
          </td>
          <td>
               <asp:DropDownList ID="ddlHead" runat="server" Width="155px"></asp:DropDownList>
          </td>
        </tr>
      <tr>
          <td align="center" colspan="2">
            <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" OnClientClick="return ValidateSave();"/>            
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"/>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
        </td>
    </tr>
</table>
<input id="hidCategoryID" runat="server" type="hidden" /> 
</asp:Content>
