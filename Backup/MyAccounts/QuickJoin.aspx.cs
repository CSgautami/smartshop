﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class QuickJoin : System.Web.UI.Page
    {
        QuickJoin_BLL obj_BLL = new QuickJoin_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
                
            ClientScript.RegisterStartupScript(this.GetType(), "myscript", "GetRefName();", true);
            if (!IsPostBack)
            {
                txtDate.Value = DateTime.Today.Date.ToString("dd-MM-yyyy");                
                txtDate.Focus();
                                
            }
        }

        protected void btnGO_Click(object sender, EventArgs e)
        {
            string PinNo = "";
            DataSet ds = new DataSet();
            try
            {
                PinNo = txtPinNo.Text;
                ds = obj_BLL.GetRefNameAndPinNo(txtPinNo.Text);
                if (ds.Tables[0].Rows.Count != 0 || Session["UserID"].ToString() == "0")
                {
                    Response.Redirect("Registraton.aspx?PINNo=" + txtPinNo.Text + "&REFID=" + txtrefid.Value + "&RegDate=" + txtDate.Value);
                    // lblMassege.Text = ds.Tables[0].Rows[0]["PinNumber"].ToString();
                }
                else
                {
                    lblMassege.Text = "Pin Not Available.";
                }
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetRefName(string RefID)
        {
            try
            {
                QuickJoin_BLL obj_BLL = new QuickJoin_BLL();

                return obj_BLL.getLedgerName(RefID).Tables[0].Rows[0]["LedgerName"].ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //protected void txtRefID_TextChanged(object sender, EventArgs e)
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        ds = obj_BLL.getLedgerName(txtRefID.Text);
        //        if (ds.Tables[0].Rows.Count != 0)
        //        {
        //            lblRefName.Visible = true;
        //            lblRefName.Text = ds.Tables[0].Rows[0]["LedgerName"].ToString();
        //        }
        //        else
        //        {
        //            lblRefName.Visible = true;
        //            lblRefName.Text = "Not Available.";
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

    }
}
