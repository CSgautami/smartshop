﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace MyAccounts20
{
    public partial class PrintCustomerSlip : System.Web.UI.Page
    {
        public string strFrom="";
        public string strTo = "";
        public string strPayDate = "";
        protected void Page_Load(object sender, EventArgs e)
        {         
            strFrom = Request.QueryString["FromDate"];
            strTo = Request.QueryString["ToDate"];
            strPayDate = Request.QueryString["PayDate"];           
            hidXML.Value = Session["CustomerPaySlip"].ToString();
                             
            if (!IsPostBack)
            {                
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "PrintCustomerSlipDataJS('" + strFrom + "','" + strTo + "','" + strPayDate + "');", true);             
            }

        }
    }
}
