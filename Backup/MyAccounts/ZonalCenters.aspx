﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="ZonalCenters.aspx.cs" Inherits="MyAccounts20.ZonalCenters" Title="Zonal Centers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="images/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" style="width: 800px;">
            <tr>
                <td align="left">
                    <asp:DataList ID="dlZone" runat="server" RepeatColumns="4">
                        <ItemTemplate>    
                            <div class="branches">
                            <div id="branchtitle">
                                <%#Eval("AreaName")%>    
                            </div>
                            <div id="branchdetails">
                                <p><%#Eval("Address") %></p>
                            </div>                            
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
