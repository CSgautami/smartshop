﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class State : GlobalPage
    {
        Country_BLL obj_BAL = new Country_BLL();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtState.Focus();
                    FillddList();
                    FillGrid();
                   
                    GetGridData();

                }
            }
            catch (Exception ex)
            {

            }

        }



        void FillddList()
        {
            try
            {                
                ds = obj_BAL.GetCountrys();
                ddlCountrys.DataSource = ds;
                ddlCountrys.DataTextField = "CountryName";
                ddlCountrys.DataValueField = "CountryID";
                ddlCountrys.DataBind();
                ddlCountrys.Items.Insert(0, new ListItem("Select", "0"));
                ddlCountrys.SelectedValue = "1";
                
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "ddlCountrys_Changed();",true);
                //GetGridData(ddlCountrys.SelectedValue);
            }
            catch (Exception ex)
            {

            }
        }
        void FillGrid()
        {
            try
            {
                //if (ddlCountrys.SelectedValue == "0")
                //{
                    DataTable dt = new DataTable();
                    dt.Columns.Add("StateID", typeof(string));
                    dt.Columns.Add("StateName", typeof(string));
                    DataRow dr = dt.NewRow();
                    dr["StateID"] = DBNull.Value;
                    dt.Rows.Add(dr);
                    gvStates.DataSource = dt;
                    gvStates.DataBind();
                    //gvStates.Rows[0].Cells[2].Text = "";
                //}
                //else
                //{
                //    State_BLL obj_BAL = new State_BLL();
                //    DataSet ds = new DataSet();
                //    ds = obj_BAL.SelectCountry(ddlCountrys.SelectedValue);
                //    gvStates.DataSource = ds;
                //    gvStates.DataBind();
                //}

            }
            catch (Exception ex)
            {

            }
        }


        public static string GetGridData()
        {
            StringWriter sw = new StringWriter();
            try
            {
                State_BLL obj_BAL = new State_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.SelectCountry();
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "States";
                ds.WriteXml(sw);
            }
            catch (Exception ex)
            {
            }
            return sw.ToString();
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnSave_Click(string CountryID, string StateName)
        {

            string resval = "";
            //if (CountryID == "" || CountryID == "0")
            //{
            //    resval = "-2";
            //    return resval;
            //}
            if (string.IsNullOrEmpty(StateName.Trim()))
            {
                resval = "-3";
                return resval;
            }
            State_BLL obj_BAL = new State_BLL();
            string res = obj_BAL.InsertStates(CountryID, StateName);
            if (res == "1")
                resval = GetGridData();
            else
                resval = res;
            return resval;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnUpdate_Click(string Countryid, string StateID, string StateName)
        {
            string rtn = "";
            //if (Countryid == "" || Countryid == "0")
            //{
            //    rtn = "-2";
            //    return rtn;
            //}
            if (string.IsNullOrEmpty(StateName.Trim()))
            {
                rtn = "-3";
                return rtn;
            }
            State_BLL obj_BAL = new State_BLL();
            string res = obj_BAL.UpdateState(StateID, StateName);
            if (res == "1")
                rtn = GetGridData();
            else
                rtn = res;
            return rtn;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeleteClick(int StateID, string Countryid)
        {
            string rtn = "";
            State_BLL obj_BAL = new State_BLL();
            string res = obj_BAL.DeleteState(StateID);
            if (res == "1")
                rtn = GetGridData();
            else
                rtn = res;
            return rtn;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string ddlCountrys_Changed(string Countryid)
        {
            StringWriter sw = new StringWriter();
            try
            {
                State_BLL obj_BAL = new State_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.SelectCountry();
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "States";
                ds.WriteXml(sw);
            }
            catch (Exception ex)
            {
            }
            return sw.ToString();
        }
    }
}
