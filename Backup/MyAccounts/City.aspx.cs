﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class City : GlobalPage
    {
        Country_BLL obj_BAL = new Country_BLL();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FillGrid();
                    GetCountrys();
                    GetState();
                }
            }
            catch (Exception ex)
            {

            }
        }
        void FillGrid()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("CityID", typeof(string));
                dt.Columns.Add("CityName", typeof(string));
                DataRow dr = dt.NewRow();
                dr["CityID"] = DBNull.Value;
                dt.Rows.Add(dr);
                gvCity.DataSource = dt;
                gvCity.DataBind();
                //gvCity.Rows[0].Cells[2].Text = "";
            }
            catch (Exception ex)
            {

            }

        }
        private void GetCountrys()
        {
            try
            {
                ds = obj_BAL.GetCountrys();
                ddlCountry.DataSource = ds;
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("Select", "0"));
                ddlState.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch { }
        }
        private void GetState()
        {
            try
            {
                City_BLL obj_BAL = new City_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetStates();
                ddlState.DataSource = ds;
                ddlState.DataTextField = "StateName";
                ddlState.DataValueField = "StateID";
                ddlState.DataBind();                
                ddlState.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch { }
        }


        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string ddlCountry_Change(string CountryID)
        {
            StringWriter sw = new StringWriter();
            try
            {
                City_BLL obj_BAL = new City_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetStates();
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "States";
                ds.WriteXml(sw);

            }
            catch { }
            return sw.ToString();
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string ddlState_Change(string StateID)
        {
            StringWriter sw = new StringWriter();
            try
            {
                City_BLL obj_BAL = new City_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetCity(StateID);
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "Citys";
                ds.WriteXml(sw);

            }
            catch { }
            return sw.ToString();
        }


        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnSave_Click(string CountryID, string StateID, string CityName)
        {
            string rtnVal = "";
            try
            {
                //if (CountryID == "0")
                //{
                //    rtnVal = "-2";
                //    return rtnVal;
                //}
                if (StateID == "0")
                {
                    rtnVal = "-3";
                    return rtnVal;
                }
                if (string.IsNullOrEmpty(CityName.Trim()))
                {
                    rtnVal = "-4";
                    return rtnVal;
                }
                City_BLL obj_BAL = new City_BLL();
                string res = obj_BAL.InsertCity(CountryID, StateID, CityName);
                if (res == "1")
                    rtnVal = GetGridData(StateID);
                else
                    rtnVal = res;
            }
            catch { rtnVal = "0"; };
            return rtnVal;
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnUpdate_Click(string CountryID, string StateID, string CityID, string CityName)
        {
            string rtn = "";
            try
            {
                //if (CountryID == "0")
                //{
                //    rtn = "-2";
                //    return rtn;
                //}
                if (StateID == "0")
                {
                    rtn = "-3";
                    return rtn;
                }
                if (string.IsNullOrEmpty(CityName.Trim()))
                {
                    rtn = "-4";
                    return rtn;
                }
                City_BLL obj_BAL = new City_BLL();
                string res = obj_BAL.UpdateCity(CityID, CityName);
                if (res == "1")
                    rtn = GetGridData(StateID);
                else
                    rtn = res;
            }
            catch { rtn = "0"; }
            return rtn;
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeleteClick(string StateID, string Cityid)
        {
            string rtn = "";
            City_BLL obj_BAL = new City_BLL();
            string res = obj_BAL.DeleteCity(Cityid);
            if (res == "1")
                rtn = GetGridData(StateID);
            else
                rtn = res;
            return rtn;
        }

        private static string GetGridData(string StateID)
        {
            StringWriter sw = new StringWriter();
            try
            {
                City_BLL obj_BAL = new City_BLL();
                DataSet ds = obj_BAL.GetCity(StateID);
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "Citys";
                ds.WriteXml(sw);
            }
            catch (Exception ex)
            {
            }
            return sw.ToString();


        }


        //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //     try
        //    {
        //        City_BLL obj_BAL = new City_BLL();
        //        DataSet ds = new DataSet();
        //        ds = obj_BAL.CountryChange(ddlCountry.SelectedValue);
        //        ddlState.DataSource = ds;
        //        ddlState.DataTextField = "StateName";
        //        ddlState.DataValueField = "StateID";
        //        ddlState.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //}




    }
}
