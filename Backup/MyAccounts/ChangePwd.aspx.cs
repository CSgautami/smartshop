﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{

    public partial class ChangePwd : System.Web.UI.Page
    {
        ChangePwd_BLL Obj_BLL = new ChangePwd_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Session["Type"].ToString() != "Customer")
            {
                Login_BLL bllLogin = new Login_BLL();
                string res = bllLogin.CheckAuthorization(Session["UserID"].ToString(), Request.Url.AbsolutePath.Substring(1).ToLower());
                if (res != "1")
                {
                    Response.Redirect("Login.aspx", true);
                }
            }
            if (!IsPostBack)
            {
                FillHeading("ChangePwd");
            }
        }
        public void FillHeading(string Heading)
        {
            try
            {
                Title = Heading;
                if (Master != null)
                {
                    Label lblHeading = (Label)Master.FindControl("lblHeading");
                    if (lblHeading != null)
                        lblHeading.Text = Heading;
                }
            }
            catch
            {
                throw;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.ChangePassword(txtOPassword.Text, txtNPassword.Text, Session["UserID"].ToString(),Session["Type"].ToString());

            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Change Password";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Password already existed";
                    
            }
            else if (res == "1")
            {
                lblMsg.Text = "Change Password Successfully";
                txtOPassword.Text = "";
                txtNPassword.Text = "";
                txtCPassword.Text = "";
            }
        }
    }
}

        
//        protected void btnSave_Click(object sender, EventArgs e)
//        {
//            string strReturn="";
//                strReturn = Obj_BLL.ChangePassword(txtOPassword.Text, txtNPassword.Text, Session["UserID"].ToString());
    
//            lblMsg.Visible = true;
//            if (strReturn == "0")
//            {
//                lblMsg.Text = "Error in Change Password";
//            }
//            else if (strReturn == "-1")
//            {
//                lblMsg.Text = "Password Already Exit";
//            }
//            else
//            {
//                lblMsg.Text = "Change Password Successfully";
//                txtOPassword.Text = "";
//                txtNPassword.Text = "";
//                txtCPassword.Text = "";
//            }
//        }
//    }
//}

        

        