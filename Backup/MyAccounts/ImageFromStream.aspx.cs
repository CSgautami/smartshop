﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace MyAccounts20
{
    public partial class ImageFromStream : System.Web.UI.Page
    {
        public bool callback()
        {
            return false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ImgStream"] != null)
            {
                byte[] mybyte = (byte[])Session["ImgStream"];
                MemoryStream myStream = new MemoryStream();
                myStream.Write(mybyte, 0, mybyte.Length);
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(myStream);
                Response.ContentType = "image/jpeg";
                bmp.GetThumbnailImage(250, 250, callback, IntPtr.Zero).Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                //bmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                bmp.Dispose();
            }
        }
    }
}
