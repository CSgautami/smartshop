﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.Collections.Generic;
namespace MyAccounts20
{
    public partial class SalesReturn : GlobalPage
    {
        SalesReturn_BLL obj_BLL = new SalesReturn_BLL();
        DataTable dt = new DataTable();
        //public static string strID;
        //public static string BranchID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            string strID = Request.QueryString["ID"];
            string BranchID = Request.QueryString["BranchID"];            
            if (!IsPostBack)
            {
                if(Session["UserID"].ToString() !="0")
                trNav.Visible = false;
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                hidUInvID.Value = "-1";
                hidSRInvID.Value = "-1";                
                ddlMode.Items.Insert(0, new ListItem("Cash", "Cash"));
                ddlMode.Items.Insert(1, new ListItem("Credit", "Credit"));
                txtInvNo.Focus();
                if (strID != null && strID != "")
                {
                    trHeading.Visible = false;
                    trMenu.Visible = false;
                    trNav.Visible = false;
                    tdCancel.Visible = false;
                    tdDelete.Visible = false;
                    //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + strID + ");" , true);
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + strID + "," + BranchID + ");", true);                 
                    strID = "";
                }
            }
        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerName(string BranchID)
        {
            try
            {
                SalesReturn_BLL  obj_BLL = new SalesReturn_BLL ();
                return obj_BLL.GetLedgerName(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAccLedger(string BranchID)
        {
            try
            {
                SalesReturn_BLL  obj_BLL = new SalesReturn_BLL ();
                return obj_BLL.GetAccLedger(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem()
        {
            try
            {
                SalesReturn_BLL  obj_BLL = new SalesReturn_BLL();
                return obj_BLL.GetStockItem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetTaxSystem()
        {
            try
            {
                SalesReturn_BLL  obj_BLL = new SalesReturn_BLL();
                return obj_BLL.GetTaxSystem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBatchNo(string ItemID, string BranchID)
        {
            try
            {
                SalesReturn_BLL obj_BLL = new SalesReturn_BLL();
                return obj_BLL.GetBatchNo(ItemID, BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerDetails(string LedgerID)
        {
            try
            {
                SalesReturn_BLL obj_BLL = new SalesReturn_BLL();
                return obj_BLL.GetLedgerDetails(LedgerID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStock(string ItemID, string BatchNo, string UDate, string BranchID ,string LedgerID)
        {
            try
            {
                SalesReturn_BLL  obj_Bll = new SalesReturn_BLL ();
                return obj_Bll.GetStock(ItemID, BatchNo, dateFormat(UDate), BranchID,LedgerID).Tables[0].Rows[0]["BalQty"].ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveInvoice(string hidSRInvID, string hidUInvID, string SRInvNo, string Date, string SRetInvNo, string SRDate, string LNo, string Address, string Mode, string Days, string Note, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,string UserID, string XmlString)
        {
            try
            {
                string res;
                SalesReturn_BLL  obj_BLL = new SalesReturn_BLL ();
                res = obj_BLL.SaveInvoice(hidSRInvID , hidUInvID, SRInvNo, dateFormat(Date), SRetInvNo , dateFormat(SRDate), LNo, Address, Mode, Days, Note, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID,UserID, XmlString);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static decimal GetTax(string tax)
        {
            decimal nTax;
            try
            {
                SalesReturn_BLL  obj_BLL = new SalesReturn_BLL();
                nTax = Convert.ToDecimal(obj_BLL.GetTax(tax).Tables[0].Rows[0]["vatrate"].ToString());
            }
            catch
            {
                nTax = 0;
            }
            return nTax;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWiseTaxSystem(string ItemID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWiseTaxSystem(ItemID).GetXml();
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string BranchID, string SRInvID, string Flag)
        {
            try
            {
                SalesReturn_BLL obj_Bll = new SalesReturn_BLL();
                return obj_Bll.GetData(BranchID , SRInvID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnDelete_Click(string InvID,string BranchID)
        {
            try
            {
                SalesReturn_BLL obj_BAL = new SalesReturn_BLL();
                return obj_BAL.DeleteSaleReturn(InvID,BranchID);
            }
            catch
            {
                throw;
            }
        }
        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetInvMaxID(string UserID)
        //{
        //    string nInvID;
        //    try
        //    {
        //        Purchase_BLL obj_Bll = new Purchase_BLL();
        //        nInvID = (obj_Bll.GetInvMaxID(UserID).Tables[0].Rows[0]["MInvID"]).ToString();
        //        //  nInvID = "0";
        //    }
        //    catch (Exception ex)
        //    {
        //        //return ex.Message;
        //        nInvID = "0";
        //    }
        //    return nInvID;
        //}
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillLedger(string BranchID, string prefix)
        {
            try
            {
                SalesReturn_BLL obj_BLL = new SalesReturn_BLL();
                return obj_BLL.CallFillLedger(BranchID, prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                SalesReturn_BLL obj_BLL = new SalesReturn_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetSearchInvoiceData(string BranchID, string SInvID)
        {
            try
            {
                SalesReturn_BLL obj_Bll = new SalesReturn_BLL();
                return obj_Bll.GetSearchInvoiceData(BranchID, SInvID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWithBarCode(string BarCode)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWithBarCode(BarCode).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
