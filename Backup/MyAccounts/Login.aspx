﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="MyAccounts20.Login" Title="Login" EnableEventValidation="false" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <noscript>
        <center>
            <h3 class="Content" style="width: 900px; color: Red;">
                Please Enable Javascript to use this site.</h3>
        </center>
    </noscript>
    <link href="MyAccounts20.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
    
    
    String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

    function SetLoginButton()
    {
        document.getElementById("btnLogin").style.display="block";
    }
    var preid='ctl00_ContentPlaceHolder1_';
    function Validate()
    {
        var UName=document.getElementById(preid+"txtUserName");
        var Pwd=document.getElementById(preid+"txtPassword");
        var Type=document.getElementById(preid+"ddlType");
        if(UName.value.trim()=="")
        {
            UName.value=UName.value.trim();
            UName.focus();
            alert("Enter username.");
            return false;
        }
        if(Pwd.value.trim()=="")
        {
            Pwd.value=Pwd.value.trim();
            Pwd.focus();
            alert("Enter password.");
            return false;
        }
//        if(Type.value=="0")
//        {
//            alert("Select Type");
//            Type.focus();
//            return false;            
//        }
        return true;
    }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <table width="100%" align="center">
        <tr>
            <td style="width:187px;">
            </td>
            <td>
                <div id="carbonForm">
                    <h1>
                        LOGIN</h1>
                    <div class="fieldContainer">
                        <div class="formRow">
                            <div class="label">
                                <label for="name">
                                    Username:</label>
                            </div>
                            <div class="field">
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="label">
                                <label for="pass">
                                    Password:</label>
                            </div>
                            <div class="field">
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="formRow">
                            <div class="label">
                                <label for="type">
                                    Type:</label>
                            </div>
                            <div class="field">
                                <asp:DropDownList ID="ddlType" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="signupButton">
                        <asp:Button ID="btnLogin" runat="server" Text="Login" OnClientClick="return Validate();"
                            OnClick="btnLogin_Click" CssClass="loginbutton" />
                    </div>
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false" ForeColor="White" ></asp:Label>
                </div>
            </td>
        </tr>
    </table>--%>
    <table width="100%" border="0"  cellpadding="0" cellspacing="0" align="center">
                       <tr><td></td></tr> 
                       <tr><td></td></tr> 
                       <tr><td></td></tr> 
                       <tr><td></td></tr> 
                       <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> 
                        <tr>
                          <td height="33" align="center" valign="middle" style="color:#1570a2"><h4>Sign In</h4></td>
                        </tr>
                        <tr align="center">
                          <td valign="top" bgcolor="#FFFFFF"><table width="400" cellpadding="0" cellspacing="2" align="center">
                            <tr>
                              <td width="139" class="bodytext"><strong>Username <span class="style5">*</span></strong></td>
                              <td><asp:TextBox ID="txtUserName" runat="server" size="31" Width="225px"></asp:TextBox>                              
                                  
                                                                                                        </td>
                            </tr>
                            <tr>
                              <td class="bodytext"><strong>Password  <span class="style5">*</span></strong></td>
                              <td><asp:TextBox ID="txtPassword" TextMode="Password" runat="server" size="31" 
                                      Width="225px" ontextchanged="btnLogin_Click"></asp:TextBox>
                        
                           </td>
                            </tr>
                            <tr>
                        <td class="bodytext"><strong>Type<span class="style5">*</span></strong></td>
                            
                        
                        <td >
                        <asp:DropDownList ID="ddlType" runat="server" width="230px" 
                                onselectedindexchanged="btnLogin_Click" >
                                </asp:DropDownList>
                        </td>
                        </tr>							
                            <tr align="center">
                              <td class="bodytext">&nbsp;</td>
                              <td style="color:#FF3300;"><a href="#" class="red">Forgot password?</a> | <a href="RegistrationDet.aspx" class="red">
                                  Register Now</a></td>
                            </tr>
                             
                            <tr align="center">
                              <td class="bodytext">&nbsp;</td>
                              <td><input type="checkbox" name="checkbox" id="checkbox">
                                  Remember me </td>
                            </tr>
                            
														
                            <tr align="center">
                              <td class="bodytext">&nbsp;</td>
                              <td><asp:Button ID="btnLogin" runat="server" Text="Submit" OnClientClick="return Validate();"
                                OnClick="btnLogin_Click"  />      </td>
                            </tr>
                          </table></td>
                        </tr>
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="false" ForeColor="White" ></asp:Label>
                       <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> <tr><td></td></tr> 
                    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server" Visible="false">
</asp:Content>

