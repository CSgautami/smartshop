﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using BLL;

namespace MyAccounts20.UserControl
{
    public partial class MenuBar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Type"].ToString() != "Customer" || Session["UserID"].ToString() == "0")
            {
                string[] strArray = Request.Url.AbsolutePath.Split('/');
                string strActPage;
                strActPage = strArray[strArray.Length - 1];
                if (strActPage.IndexOf('?') > 0)
                    strActPage = strActPage.Substring(0, strActPage.IndexOf('?'));
                
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }

                StringBuilder sbMenu = new StringBuilder();
                sbMenu.Append("<div id=\"smoothmenu1\" class=\"ddsmoothmenu\">");
                sbMenu.Append("<ul>");

                sbMenu.Append("<li>");                
                sbMenu.Append("<a href='Home.aspx'>Home</a>");                
                sbMenu.Append("</li>");                
                    Login_BLL bllLogin = new Login_BLL();
                    DataSet ds = bllLogin.GetUserMenu(Session["UserID"].ToString());
                    DataTable dt;
                    string SubMenu = "";

                    if (ds != null && ds.Tables.Count > 1)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (SubMenu != "")
                            {
                                sbMenu.Append("</ul>");
                                sbMenu.Append("</li>");
                            }
                            SubMenu = "";

                            sbMenu.Append("<li>");
                            sbMenu.Append("<a href='#'>" + dr["MainMenu"].ToString() + "</a>");

                            ds.Tables[1].DefaultView.RowFilter = "ReportType=" + dr["ReportType"].ToString();
                            dt = ds.Tables[1].DefaultView.ToTable();
                            if (dt.Rows.Count > 0)
                            {
                                sbMenu.Append("<ul>");
                                foreach (DataRow drow in dt.Rows)
                                {
                                    if (drow["ChildMenu"].ToString() != "")
                                    {
                                        if (drow["SubMenu"].ToString() != SubMenu)
                                        {
                                            if (SubMenu != "")
                                            {
                                                sbMenu.Append("</ul>");
                                                sbMenu.Append("</li>");
                                            }
                                            SubMenu = drow["SubMenu"].ToString();
                                            sbMenu.Append("<li>");
                                            sbMenu.Append("<a href='#'>" + SubMenu + "</a>");
                                            sbMenu.Append("<ul>");
                                        }

                                        sbMenu.Append("<li><a href='" + drow["PageURL"].ToString() + "'>" + drow["ChildMenu"].ToString() + "</a></li>");
                                    }
                                    else
                                    {
                                        if (SubMenu != "")
                                        {
                                            sbMenu.Append("</ul>");
                                            sbMenu.Append("</li>");
                                        }
                                        SubMenu = "";
                                        sbMenu.Append("<li><a href='" + drow["PageURL"].ToString() + "'>" + drow["SubMenu"].ToString() + "</a></li>");
                                    }

                                }
                                sbMenu.Append("</ul>");
                            }
                            sbMenu.Append("</li>");
                        }
                    }
                    lblName.Text = "";                
                sbMenu.Append("</ul>");
                sbMenu.Append("</div>");
                tdMenu.InnerHtml = sbMenu.ToString();
            }
        }

        public void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("Index.aspx");
        }
    }
}