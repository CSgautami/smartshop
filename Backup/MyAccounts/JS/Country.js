﻿var preid = '';
function EditClick(e, t) 
{
  debugger;
    btnCancel_Click();    
    var CountryID = document.getElementById("hidCountryID");
    document.getElementById(preid + "txtCountry").value = "";
    CountryID.value = "";
    var Country = t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    //var Country = t.cells[1].innerHTML;
    var res = confirm("Do you want to Edit the Country : " + Country);
    if (res == true) {
        CountryID.value = t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        document.getElementById(preid + "txtCountry").value = Country;
        document.getElementById("btnSave").style.display = "none";
        document.getElementById("btnUpdate").style.display = "block";
        document.getElementById("btnCancel").style.display = "block";
    }
}

function DeleteClick(e, t) {
    btnCancel_Click();    
    document.getElementById(preid + "txtCountry").value = "";
    var Countrys = t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    var res = confirm("Do you want to Delete the Country : "+Countrys);
    if (res == true) {
        var Countryid = document.getElementById("hidCountryID");
        Countryid.value = "";
        Countryid.value= t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        PageMethods.DeleteClick(Countryid.value, DeleteComplete);
    }
}

function DeleteComplete(res) {    
    showtable(res);    
    alert("Country Deleted Successfully.");
}


function btnCancel_Click() {    
    document.getElementById(preid + "txtCountry").value = "";
    document.getElementById("hidCountryID").value = "";
    document.getElementById("btnSave").style.display = "block";
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "none";
}



function btnSave_Click(t) {    
    if (document.getElementById(preid + "txtCountry").value == "") {        
        premsgbox("Enter CountryName", "txtCountry");
        return false;
    }
    var Countryname = document.getElementById(preid + "txtCountry").value;
    t.disabled = true;
    PageMethods.btnSave_Click(Countryname, SaveComplete);        
}


function btnUpdate_Click(t) {    
    if (document.getElementById(preid + "txtCountry").value == "") {        
        premsgbox("Select Country ", "txtCountry");
        return false;
    }
    var Countryid = document.getElementById("hidCountryID").value;
    var Countryname = document.getElementById(preid + "txtCountry").value;
    t.disabled=true;
    PageMethods.btnUpdate_Click(Countryid, Countryname, UpdateComplete);    
}

function UpdateComplete(res)
{
    document.getElementById(preid + "txtCountry").focus();
    if(res=="-2")
    {
        document.getElementById(preid + "txtCountry").value="";
        alert("Enter CountryName.");
        return;
    }
    if (res == "" || res == "0") {
        alert("Error in insertion.");        
        return;
    }
    if (res == "-1") {
        alert("Country already exist.");        
        return;
    }
    btnCancel_Click();
    showtable(res);
    document.getElementById("btnUpdate").disabled = false;
    alert("Country Updated Successfully.");     
}

function SaveComplete(res) {
try
{
    document.getElementById(preid + "txtCountry").focus();
    if(res=="-2")
    {
        document.getElementById(preid + "txtCountry").value="";
        alert("Enter CountryName.");
        return;
    }
    if (res == "" || res == "0") {
        alert("Error in insertion.");        
        return;
    }
    if (res == "-1") {
        alert("Country already exist.");        
        return;
    }
    document.getElementById(preid + "txtCountry").value="";
    showtable(res);
    
    alert("Country Saved Successfully.");  
}
catch(err)
{
}
finally
{
document.getElementById("btnSave").disabled = false;
}  

}

//function FillCountrys()
//{
//    PageMethods.FillCountrys(showtable);
//}

function showtable(res)
{
    var xmlObj = getXml(res);
    var tbl = document.getElementById(preid + "gvCountrys");
    for (var i = tbl.rows.length - 1; i > 0; i--) {
        tbl.deleteRow(i);
    }        
    tbl.rows[0].cells[1].style.position = 'relative';
    tbl.rows[0].cells[1].style.width = 100;
    for (var i = 0; i < xmlObj.getElementsByTagName("CountryName").length; i++) {
        tbl.insertRow(i + 1)
        tbl.rows[i + 1].insertCell(0);
        tbl.rows[i + 1].insertCell(1);        
        tbl.rows[i + 1].insertCell(2);
        tbl.rows[i + 1].cells[0].className='hidendStyle';
        tbl.rows[i + 1].cells[0].innerHTML =  xmlObj.getElementsByTagName("Countrys")[i].getElementsByTagName("CountryID")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[1].innerHTML = xmlObj.getElementsByTagName("Countrys")[i].getElementsByTagName("CountryName")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[2].innerHTML = "<a href='#' onclick='EditClick(event,this)' class='gridNavButtons'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='DeleteClick(event,this)' class='gridNavButtons'>Delete</a>";        
        tbl.rows[i + 1].cells[0].style.display="none";
    }
}

function getXml(xmlString) {
    var xmlObj;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    return xmlObj;
}

function premsgbox(msg, cntrl) {
    try {
        document.getElementById(preid + cntrl).focus();
        alert(msg);        
    } catch (error) { }

}