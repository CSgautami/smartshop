﻿var dtFormat="dd-MM-yyyy";
var ddlBranch;
var UserID;
var hidGBID;
document.write(getCalendarStyles());
var cal=new CalendarPopup("divCalendar");
cal.showNavigationDropdowns();
function showcalendar(t)
{
    cal.select(t,t.id,'dd-MM-yyyy');
}

var xmlObj;
function getXml(xmlString) 
{
    xmlObj=null;
    try
    {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") 
        {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else 
        {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }
    }
    catch (err) 
    {
    }

}
function GetDefaultBranch()
{
    hidGBID=document.getElementById("hidGBID");
}
function SetData()
{        
    document.getElementById("txtFromDate").value=new Date().format("dd-MM-yyyy");
    document.getElementById("txtToDate").value=new Date().format("dd-MM-yyyy");        
    UserID=document.getElementById("hidUserID");
    ddlBranch = document.getElementById("ddlBranch");
    GetBranch();
}

function GetPaymentBookData()
{
    var tbl=document.getElementById("tblGrid");
    for(var i=tbl.rows.length-1;i>=0;i--)
        tbl.deleteRow(i);
    var UserID=document.getElementById("hidUserID");
    var viewAll=document.getElementById("chkAll");
    var FromDt=document.getElementById("txtFromDate");
    var ToDt=document.getElementById("txtToDate");    
    if(viewAll.checked==false)
    {
        if(!isDate(FromDt.value,dtFormat))
        {            
            FromDt.focus();
            alert("Enter Valid FromDate.");
            return false;
        }
        else if(!isDate(ToDt.value,dtFormat))
        {            
            ToDt.focus();
            alert("Enter Valid ToDate.");
            return false;
        }
    }
    else
    {
        FromDt.value="";
        ToDt.value="";
    }
    PageMethods.GetPaymentBookData(ddlBranch.value,FromDt.value,ToDt.value,GetPaymentBookDataComplete);
}

function GetPaymentBookDataComplete(res)
{
    getXml(res);
    var tbl=document.getElementById("tblGrid");    
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        var tr=tbl.insertRow(i);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PaymentNo")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PaymentNo")[0].firstChild!=null)
                tr.cells[0].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PaymentNo")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PaymentDate")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PaymentDate")[0].firstChild!=null)
                tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PaymentDate")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0].firstChild!=null)
                tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToLedgerName")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToLedgerName")[0].firstChild!=null)
                tr.cells[3].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToLedgerName")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("FromLedgerName")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("FromLedgerName")[0].firstChild!=null)
                tr.cells[4].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("FromLedgerName")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Amount")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Amount")[0].firstChild!=null)
                tr.cells[5].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Amount")[0].firstChild.nodeValue;
        
        tr.cells[0].style.width=20;
        tr.cells[1].style.width=90;
        tr.cells[2].style.width=310;
        tr.cells[3].style.width=150;
        tr.cells[4].style.width=150;
        tr.cells[5].style.width=100;
        
        
        tr.cells[0].style.textAlign="center";
        tr.cells[1].style.textAlign="center";
        tr.cells[2].style.textAlign="left";
        tr.cells[3].style.textAlign="left";
        tr.cells[4].style.textAlign="left";
        tr.cells[5].style.textAlign="right";   
        
    }
    
    
}

function chkAll_Click(e,t)
{
    if(t.checked==true)
    {
        document.getElementById("spnFromDate").style.display="none";
        document.getElementById("txtFromDate").style.display="none";
        document.getElementById("spanToDate").style.display="none";
        document.getElementById("txtToDate").style.display="none";
    }
    else
    {
        document.getElementById("spnFromDate").style.display="block";
        document.getElementById("txtFromDate").style.display="block";
        document.getElementById("spanToDate").style.display="block";
        document.getElementById("txtToDate").style.display="block";
    }
}
function GetBranch()
{
   if(UserID==null)
       GetUserID(); 
    PageMethods.GetBranch(UserID.value, GetBranchComplete);   
}

function GetBranchComplete(res)
{
    getXml(res);     
    ddlBranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";    
        ddlBranch.options.add(opt);        
    }             
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlBranch.options.add(opt);
    } 
    if(hidGBID==null)
            GetDefaultBranch();
    ddlBranch.value=hidGBID.value; 
}