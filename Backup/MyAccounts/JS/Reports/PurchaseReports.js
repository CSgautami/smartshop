﻿var dtpFrom;
var dtpTo;
var ddlMode;
var ddlName;
var UserID;
var preid='ctl00_ContentPlaceHolder1_';
var dtFormat='dd-MM-yyyy'
var ddlBranch;
var ddlHeadGroup;
var hidGBID;
var preid;
function getXml(xmlString)
{
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetControls()
{
    ddlBranch=document.getElementById("ddlBranch");
    dtpFrom = document.getElementById(preid+ "txtFromDate");
    dtpTo = document.getElementById(preid+ "txtToDate");
    ddlMode =document.getElementById("ddlMode");
    ddlName=document.getElementById("ddlSupplier");
    UserID = document.getElementById(preid + "hidUserID");    
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById(preid+"hidGBID");
}
function GetDefaults()
{    
//    dtpFrom.value = new Date().format("dd-MM-yyyy");
//    dtpTo.value=new Date().format("dd-MM-yyyy");    
    ddlMode.value="0";
    ddlName.value="0";
    ddlBranch.focus();
}
function SetData()
{
    GetControls();
    GetBranch();
    //GetLedgerName();
    GetDefaults();
}
function GetUserID()
{
    UserID=document.getElementById(preid+"hidUserID");
}
function GetBranch()
{        
    if(UserID==null)
        GetUserID();
    PageMethods.GetBranch(UserID.value, GetBranchComplete);    
}

function GetBranchComplete(res)
{
    getXml(res);    
    ddlBranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlBranch.options.add(opt);
        ddlBranch.value="0";
    }  
    else
    {
        if(ddlName!=null)
            GetLedgerName();
    } 
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlBranch.options.add(opt);
    }        
    if(hidGBID==null)
            GetDefaultBranch();
        ddlBranch.value=hidGBID.value;
        if(ddlName!=null)
            GetLedgerName();
}
function GetLedgerName()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetLedgerName(ddlBranch.value,GetLedgerComplete);    
}

function GetLedgerComplete(res)
{
    getXml(res);    
    ddlName.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlName.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlName.options.add(opt);
    }    
}
function GetControlsGroupAndComWise()
{
    ddlBranch=document.getElementById("ddlBranch");
    dtpFrom = document.getElementById(preid+"txtFromDate");
    dtpTo = document.getElementById(preid+"txtToDate");
    ddlGroup =document.getElementById("ddlGroup");
    ddlCompany=document.getElementById("ddlCompany");
    //ddlHeadGroup=document.getElementById("ddlHeadGroup");   
}
function GetDefaultsGroupAndComWise()
{
//    dtpFrom.value = new Date().format("dd-MM-yyyy");
//    dtpTo.value=new Date().format("dd-MM-yyyy");
    ddlGroup.value="0";
    ddlCompany.value="0";
    ddlBranch.focus();
    //ddlHeadGroup.value="0";
}
function GetHeadGroup()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetHeadGroup(GetHeadGroupComplete);    
}

function GetHeadGroupComplete(res)
{
    getXml(res);    
    ddlHeadGroup.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlHeadGroup.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupID")[0].firstChild.nodeValue;
        ddlHeadGroup.options.add(opt);
    }    
}

function GetStockGroup()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetStockGroup(GetStockGroupComplete);    
}

function GetStockGroupComplete(res)
{
    getXml(res);    
    ddlGroup.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlGroup.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupID")[0].firstChild.nodeValue;
        ddlGroup.options.add(opt);
    }    
}
function GetCompany()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetCompany(GetCompanyComplete);    
}

function GetCompanyComplete(res)
{
    getXml(res);    
    ddlCompany.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlCompany.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyID")[0].firstChild.nodeValue;
        ddlCompany.options.add(opt);
    }    
}
function SetGroupAndComData()
{
    GetControlsGroupAndComWise();
    GetDefaultsGroupAndComWise();
    GetBranch();
    GetStockGroup();
    GetCompany();
    //GetHeadGroup();
}
function GetControlsInvWise()
{
    ddlBranch=document.getElementById("ddlBranch");
    dtpFrom = document.getElementById(preid+"txtFromDate");
    dtpTo = document.getElementById(preid+"txtToDate");    
}
function GetDefaultsInvWise()
{
//    dtpFrom.value = new Date().format("dd-MM-yyyy");
//    dtpTo.value=new Date().format("dd-MM-yyyy");  
    ddlBranch.focus();  
}
function SetInvWiseData()
{
    GetControlsInvWise();
    GetBranch();
    GetDefaultsInvWise();    
}
function GetControlsProductWise()
{
    ddlBranch=document.getElementById("ddlBranch");
    dtpFrom = document.getElementById(preid+"txtFromDate");
    dtpTo = document.getElementById(preid+"txtToDate");    
    ddlStockItem=document.getElementById("ddlStcokItem"); 
    tdItem=document.getElementById("tdItem");
    lblItem=document.getElementById(preid+"lblItem");   
}
function GetDefaultsProductWise()
{
//    dtpFrom.value = new Date().format("dd-MM-yyyy");
//    dtpTo.value=new Date().format("dd-MM-yyyy"); 
    ddlBranch.focus();       
}
function SetProductWiseData()
{    
    GetControlsProductWise();
    GetBranch();
    GetStockItem();   
    GetDefaultsProductWise();        
}
function GetStockItem()
{
    if(UserID==null)
       GetUserID(); 
    //PageMethods.GetStockItem(GetStockItemComplete);    
}

function GetStockItemComplete(res)
{
    getXml(res);    
    ddlStockItem.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlStockItem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
        ddlStockItem.options.add(opt);
    }    
}

function btnShow_Click(e, t)
{        
    if (!isDate(dtpFrom.value,dtFormat))
    {
        alert("Enter Valid Date");
        dtpFrom.focus();
        return false;
    }
    if (!isDate(dtpTo.value,dtFormat))
    {
        alert("Enter Valid Date");
        dtpTo.focus();
        return false;
    }
    window.frames["frmPurchaseReport"].location.href="Reports/PurchaseReport.aspx?FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&Mode="+ddlMode.value+"&Name="+ddlName.value+"&Branch="+ddlBranch.value;
}
function GetItemName()
{
   PageMethods.GetItemName(selectedItem,CompleteItemName);  
}
function CompleteItemName(res)
{
    try 
    {
        getXml(res);       
        var lblItem=document.getElementById(preid+"lblItem");     
        if(xmlObj.getElementsByTagName("Table").length==0)
        {          
            lblItem.innerText="";                
            return;            
        }
        else
        {            
            lblItem.innerText =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
        }      
    }
    catch(err)
    {
    }
}
var divItem;
var selectedItem="";
var prevText="";

function FillItem(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedItem="";
        divItem=document.getElementById("divItem");
        if(t.value!="")
        {        
            PageMethods.CallFillItem(t.value,FillItemComplete)
        }
        else{
        divItem.innerHTML="";
        divItem.style.display="none";
        }
    }
}

function FillItemComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var ItemID;
            var ItemName;
            var ItemCode;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild!=null)
                    ItemID=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild!=null)
                    ItemName=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            //lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild!=null)
                    //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                    ItemCode =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            //lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                selectedItem=ItemID;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divItem.innerHTML=tbl;
        if(count>0)
            divItem.style.display="block";
        else
            divItem.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectClick(t)
{
    selectedItem=t.cells[0].innerHTML;
    var txt=document.getElementById("tdItem");
    ApplyItem(txt);
}
function OnMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedItem ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedItem=t.cells[0].innerHTML;
}
function selectItem(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyItem(t);
    }
    else
    {
        if(divItem==null)
            divItem=document.getElementById("divItem");
        var tbl=divItem.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedItem=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedItem=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyItem(t)
{
    if(divItem==null)
            divItem=document.getElementById("divItem");
    if(selectedItem!="")
    {   
        var tbl=divItem.firstChild;
        var cnt=0;
        if(tbl!="" && tbl!=null)
        { 
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedItem="";
                t.value="";
            }
        }        
    }
    else
        t.value="";
    prevText=t.value;
    divItem.style.display="none";
    
}

function ProductWiseShowClick()
{    
    GetControlsProductWise();
    window.frames["frmPurchaseReportProductWise"].location.href="Reports/PurchaseReportProductWise.aspx?FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&StockItem="+selectedItem+"&Branch="+ddlBranch.value
}