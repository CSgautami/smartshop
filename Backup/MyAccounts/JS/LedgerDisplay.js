﻿var dtFormat="dd-MM-yyyy";
var ddlBranch;
var UserID;
var hidGBID;
document.write(getCalendarStyles());
var cal=new CalendarPopup("divCalendar");
cal.showNavigationDropdowns();
function showcalendar(t)
{
    cal.select(t,t.id,'dd-MM-yyyy');
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById("hidGBID");
}

var xmlObj;
function getXml(xmlString) 
{
    xmlObj=null;
    try
    {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") 
        {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else 
        {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }
    }
    catch (err) 
    {
    }

}

function SetData()
{        
    document.getElementById("txtFromDate").value=new Date().format("dd-MM-yyyy");
    document.getElementById("txtToDate").value=new Date().format("dd-MM-yyyy");        
    UserID=document.getElementById("hidUserID");
    ddlBranch= document.getElementById("ddlBranch");
    GetLedgerDisplayLedgers();
    GetBranch();
    
}
function GetLedgerDisplayLedgers()
{
    PageMethods.GetLedgerDisplayLedgers(ddlBranch.value,GetLedgerDisplayLedgersComplete);
}
function GetLedgerDisplayLedgersComplete(res)
{
    getXml(res);
    var ddl=document.getElementById("ddlLedger");
    ddl.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddl.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddl.options.add(opt);
    }
   
}
function GetLedgerDisplayData()
{
    var tbl=document.getElementById("tblGrid");
    for(var i=tbl.rows.length-1;i>=0;i--)
        tbl.deleteRow(i);
    var UserID=document.getElementById("hidUserID");
    var ddlLedger=document.getElementById("ddlLedger");
    var viewAll=document.getElementById("chkAll");
    var FromDt=document.getElementById("txtFromDate");
    var ToDt=document.getElementById("txtToDate");
    if(ddlLedger.value=="0")
    {
        alert("Select Ledger");
        ddlLedger.focus();
        return false;
    }
    if(viewAll.checked==false)
    {
        if(!isDate(FromDt.value,dtFormat))
        {            
            FromDt.focus();
            alert("Enter Valid FromDate.");
            return false;
        }
        else if(!isDate(ToDt.value,dtFormat))
        {            
            ToDt.focus();
            alert("Enter Valid ToDate.");
            return false;
        }
    }
    else
    {
        FromDt.value="";
        ToDt.value="";
    }
    PageMethods.GetLedgerDisplayData(ddlBranch.value,FromDt.value,ToDt.value,ddlLedger.value,GetLedgerDisplayDataComplete);
}

function GetLedgerDisplayDataComplete(res)
{
    getXml(res);
    var tbl=document.getElementById("tblGrid");    
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        var tr=tbl.insertRow(i);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        tr.insertCell(0);
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0].firstChild!=null)
                tr.cells[0].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("SlNo")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("SlNo")[0].firstChild!=null)
                tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("SlNo")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Date")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Date")[0].firstChild!=null)
                tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Date")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0].firstChild!=null)
                tr.cells[3].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0].firstChild!=null)
            {
                tr.cells[4].noWrap=true;                
                var nar=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0].firstChild.nodeValue;
                tr.cells[4].innerHTML="<div style='overflow:hidden; width:295px; height:18px;' title='"+nar+"'>"+nar+"</div>";
            }
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CrAmount")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CrAmount")[0].firstChild!=null)
                tr.cells[5].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CrAmount")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DrAmount")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DrAmount")[0].firstChild!=null)
                tr.cells[6].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DrAmount")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Balance")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Balance")[0].firstChild!=null)
                tr.cells[7].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Balance")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("St")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("St")[0].firstChild!=null)
                tr.cells[8].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("St")[0].firstChild.nodeValue;
        
        tr.cells[0].style.width=0;
        tr.cells[1].style.width=50;
        tr.cells[2].style.width=90;
        tr.cells[3].style.width=50;
        tr.cells[4].style.width=310;
        tr.cells[5].style.width=100;
        tr.cells[6].style.width=100;
        tr.cells[7].style.width=100;
        tr.cells[8].style.width=30;
        
        tr.cells[0].style.display="none";
        tr.cells[1].style.textAlign="center";
        tr.cells[2].style.textAlign="center";
        tr.cells[5].style.textAlign="right";
        tr.cells[6].style.textAlign="right";
        tr.cells[7].style.textAlign="right";   
        
        
    }
    
    
}

function chkAll_Click(e,t)
{
    if(t.checked==true)
    {
        document.getElementById("spnFromDate").style.display="none";
        document.getElementById("txtFromDate").style.display="none";
        document.getElementById("spanToDate").style.display="none";
        document.getElementById("txtToDate").style.display="none";
    }
    else
    {
        document.getElementById("spnFromDate").style.display="block";
        document.getElementById("txtFromDate").style.display="block";
        document.getElementById("spanToDate").style.display="block";
        document.getElementById("txtToDate").style.display="block";
    }
}
function GetUserID()
{
    UserID=document.getElementById(preid+"hidUserID");
}
function GetBranch()
{
   if(UserID==null)
       GetUserID(); 
    PageMethods.GetBranch(UserID.value, GetBranchComplete);   
}

function GetBranchComplete(res)
{
    getXml(res);     
    ddlBranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";    
        ddlBranch.options.add(opt);        
    }         
    else
    {
        GetLedgerDisplayLedgers();
    }    
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlBranch.options.add(opt);
    } 
    if(hidGBID==null)
            GetDefaultBranch();
    ddlBranch.value=hidGBID.value;
    GetLedgerDisplayLedgers();
}