﻿var preid = '';

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

var xmlObj;
function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    
}
var divLedgerNo;
var ddlBranch;
var Ledger;
var Group; 
var status;
var Amount;
var RefName; 
var Address; 
var town;
var phone;
var APGST;  
var bank; 
var fax;
var Sales;
var Purchase; 
var UserID;
var YourID;
var Password;
var RefMember;
var LandMark;
var lblMember;
var Mode;
var Area;  
var Pins;
var LedgerID;
var tblGrid;
var hidGBID;
var PinNo;
var Search;
function GetCntrls()
{
    Search =document.getElementById(preid+"txtSearch");
    ddlBranch=document.getElementById(preid+"ddlBranch");
    LedgerNo = document.getElementById(preid + "divLMNo");
    Ledger=document.getElementById(preid+"txtLedgerName");
    Group = document.getElementById(preid + "ddlGroupName");
    status = document.getElementById(preid+"ddlStatus");
    Amount=document.getElementById(preid+"txtAmount");
    //RefName = document.getElementById(preid+"txtRefName");
    Address=document.getElementById(preid+"txtAddress");
    town = document.getElementById(preid + "txtTown");
    phone = document.getElementById(preid+"txtPhone");
    APGST = document.getElementById(preid + "txtAPGST");
    //bank=document.getElementById(preid+"txtBank");
    //fax = document.getElementById(preid+"txtFax");
    Sales=document.getElementById(preid+"chkSales");
    Purchase=document.getElementById(preid+"chkPurchase");
    YourID =document.getElementById(preid+"txtYourID");
    Password =document.getElementById(preid+"txtPassword");
    //RefMember =document.getElementById(preid+"ddlRef");
    RefMember=document.getElementById(preid+"txtRef");
    LandMark =document.getElementById(preid+"txtLandMark");
    lblMember =document.getElementById(preid+"lblMember");
    Mode=document.getElementById(preid+"ddlMode");
    Area =document.getElementById(preid+"ddlArea");
    PinNo=document.getElementById(preid+"txtPin");
    //Pins=document.getElementById(preid+"ddlPinNo");    
//    tblGrid =document.getElementById("tblGrid");
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById("hidGBID");
}

function btnSave_Click(e,t) {
try
{
 
    GetCntrls();
    if (Ledger.value.trim() == "")
    {        
        Ledger.value="";
        Ledger.focus();
        alert("Enter LedgerName.");
        return;
        
    }    
    if(Group.value=="0")
    {
        Group.focus();
        alert("Enter GroupName.");
        return;
    }    
    if(Group.value=="22")
    {
//        if(YourID.value.trim()=="")
//        {
//            YourID.focus();
//            alert("Enter Your ID No.");
//            return; 
//        }
        if(LandMark.value.trim()=="")
        {
            LandMark.focus();
            alert("Enter LandMark");
            return; 
        }
        if(phone.value.trim()=="")
        {
            phone.focus();
            alert("Enter Phone");
            return; 
        }        
        if(Area.value=="0")
        {
            Area.focus();
            alert("Select Area Centre");
            return; 
        }
    }
    
//    if(tblGrid.rows.length<=1)
//    {
//        alert("Enter Pin Numbers");
//        ddlPinNo.focus();
//        return false;
//    }
//    var xmlDet="<XML>";
//    for(var i=1;i<tblGrid.rows.length;i++)
//    {
//        xmlDet=xmlDet+"<PinDet>";
//        var tr=tblGrid.rows[i];            
//        xmlDet=xmlDet+"<PinID>"+tr.cells[0].innerHTML.trim()+"</PinID>";
//        xmlDet=xmlDet+"</PinDet>";
//    }
//    xmlDet=xmlDet+"</XML>";
    t.disabled=true;
    var Con=confirm("Are You Sure,Do You Want To Save?");
    if(Con=true)
    
    //PageMethods.btnSave_Click(YourID.value,Password.value, ddlBranch.value,Ledger.value,RefMember.value,LandMark.value, Group.value,status.value,Amount.value,Address.value,town.value,phone.value,APGST.value,Mode.value,Area.value, Sales.checked,Purchase.checked,UserID.value,xmlDet,SaveComplete);
    PageMethods.btnSave_Click(YourID.value,Password.value, ddlBranch.value,Ledger.value,RefMember.value,LandMark.value, Group.value,status.value,Amount.value,Address.value,town.value,phone.value,APGST.value,Mode.value,Area.value, Sales.checked,Purchase.checked,UserID.value,PinNo.value, SaveComplete);
    //PageMethods.btnSave_Click("1","", "0","Test","","qqq", "21","Dr","","","","11111","","","", "false","false","0","143257863", SaveComplete);
    //PageMethods.Test("1",SaveComplete);
    
}

catch(err)
{
t.disabled=false;
}

}

function SaveComplete(res) 
{
    try
    {
        if (res == "" || res == "0")
        {
            alert("Error in insertion.");        
            return;
        }    
        else if(res=="-1")
        {
            if(Sales.value=="true")            
                alert("Your ID No Already Exists.");
            else 
                alert("Ledger Name Already Exists.");
            return;
        }
        else if(res=="-2")
        {
            alert("Please Check Pin No");
            PinNo.focus();
            return ;
        }  
        else        
        {
            alert("Ledger Saved Successfully.");    
            btnCancel_Click();
        }
    }
    catch(err)
    {
    }
    finally    
    {
        document.getElementById("btnSave").disabled = false;
    }
}
function btnDelete_Click(e,t)
{
    try 
    {
        var Con=confirm("Do You Want To Delete This Ledger");
        if(Con==true)
            PageMethods.btnDelete_Click(LedgerID,DeletComplete);
    }
    catch(err)
    {
    }
}
function DeletComplete(res)
{
    alert("Ledger Deleted Successfully");
    btnCancel_Click();
}
function btnUpdate_Click(e,t) {
try
{
 
    GetCntrls();
    if (Ledger.value.trim() == "")
    {        
        Ledger.value="";
        Ledger.focus();
        alert("Enter LedgerName.");
        return;
        
    }    
    if(Group.value=="0")
    {
        Group.focus();
        alert("Enter GroupName.");
        return;
    }    
//    Amount.value=Amount.value.trim();
//    if(Amount.value=="")
//    {
//        
//    }
//    else if(isNaN(Amount.value))
//    {
//        alert("Enter valid amount.");
//        Amount.focus();
//        return false;
//    }  
//    else
//    {
//        Amount.value=parseFloat(Amount.value).toFixed(2);
//    } 
    if(Group.value=="22")
    {
//        if(YourID.value.trim()=="")
//        {
//            YourID.focus();
//            alert("Enter Your ID No.");
//            return; 
//        }
        if(LandMark.value.trim()=="")
        {
            LandMark.focus();
            alert("Enter LandMark");
            return; 
        }
        if(phone.value.trim()=="")
        {
            phone.focus();
            alert("Enter Phone");
            return; 
        }
        if(Area.value=="0")
        {
            Area.focus();
            alert("Select Area Centre");
            return; 
        }
    }
//    if(tblGrid.rows.length<=2)
//    {
//        alert("Enter Pin Numbers");
//        ddlPinNo.focus();
//        return false;
//    }
//    var xmlDet="<XML>";
//    for(var i=1;i<tblGrid.rows.length;i++)
//    {
//        xmlDet=xmlDet+"<PinDet>";
//        var tr=tblGrid.rows[i];            
//        xmlDet=xmlDet+"<PinID>"+tr.cells[0].innerHTML.trim()+"</PinID>";
//        xmlDet=xmlDet+"</PinDet>";
//    }
//    xmlDet=xmlDet+"</XML>";
    if(UserID.value=="")
       GetUserID();
    t.disabled=true;
    var Con=confirm("Are You Sure,Do You Want To Update?");
    if(Con=true)
    { 
    //PageMethods.btnUpdate_Click(BranchID.value,LedgerNo.value,Ledger.value,Group.value,status.value,Amount.value,RefName.value,Address.value,town.value,phone.value,APGST.value,bank.value,fax.value,Sales.checked,Purchase.checked,UserID.value,UpdateComplete);
    //PageMethods.btnUpdate_Click(YourID.value,Password.value, BranchID.value,Ledger.value,RefMember.value,LandMark.value, Group.value,status.value,Amount.value,Address.value,town.value,phone.value,APGST.value,Mode.value,Area.value, Sales.checked,Purchase.checked,UserID.value,UpdateComplete);
    //PageMethods.btnUpdate_Click(LedgerID, LedgerNo.value,Password.value, ddlBranch.value,Ledger.value,LandMark.value, Group.value,status.value,Amount.value,Address.value,town.value,phone.value,APGST.value,Mode.value,Area.value, Sales.checked,Purchase.checked,UserID.value,xmlDet, UpdateComplete);
    PageMethods.btnUpdate_Click(LedgerID, LedgerNo.value,Password.value, ddlBranch.value,Ledger.value,LandMark.value, Group.value,status.value,Amount.value,Address.value,town.value,phone.value,APGST.value,Mode.value,Area.value, Sales.checked,Purchase.checked,UserID.value,PinNo.value, UpdateComplete);
}
}
catch(err)
{
t.dsiabled=false;
}

}

function UpdateComplete(res) 
{
    try
    {
        if (res == "" || res == "0")
        {
            alert("Error in insertion.");        
            return;
        }    
        else if(res=="-1")
        {
            alert("Ledger Already Exists.");
            return;
        }
        else if(res=="-1")
        {
            alert("Please Check Pin No");
        }  
        else        
        {
            alert("Ledger Updated Successfully.");    
            ActivateSave();
        }
    }
    catch(err)
    {
    }
    finally    
    {
        document.getElementById("btnUpdate").disabled = false;
    }
}


function btnCancel_Click()
{
    GetCntrls();
    //BranchID.value="0";
    LMNO=0;
    Search.value="";
    YourID.value="";    
    Password.value="";
    LedgerNo.value="";
    Ledger.value="";
    RefMember.value="";
    lblMember.innerText="";
    LandMark.value="";    
    Group.value="0";
    status.value="Dr";
    Amount.value="";    
    Address.value="";
    town.value="";
    phone.value="";
    APGST.value="";
    Mode.value="0";
    Area.value="0";
    PinNo.value="";
    Sales.checked=false;
    Purchase.checked=false;
    //Ledger.focus();    
    //YourID.focus();     
    ddlBranch.focus();
//    for(var i=tblGrid.rows.length-1;i>0;i--)
//    {
//       tblGrid.deleteRow(i);
//    }
//    Pins.value="0"; 
    //ActivateSave(); 
}
function SetRefData()
{
    btnCancel_Click();
    //GetRefMember();
    ActivateSave();
}
function SetData()
{
    
    GetBranch();    
    btnCancel_Click();
}
function ShowGroupMaster(e,t)
{
    if(e.keyCode==45)
    {
        window.showModalDialog("GroupMaster.aspx?Ledger="+new Date().getTime(),"","dialogHeight:200px;dialogWidth:200px;center:yes;scroll:no;");
        GetGroups();           
    }
}
function GetPassword()
{
    if(Password.value.trim()=="")
        Password.value=YourID.value;
}
function GetGroupDetails()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetGroupDetails(Group.value, GetGroupDetailsComplete);    
}
function GetGroupDetailsComplete(res)
{
    if (Group.value!="0")
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Status")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Status")[0].firstChild!=null)
            status.value =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Status")[0].firstChild.nodeValue;
        if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Customer")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Customer")[0].firstChild!=null)
            var chkSales="";
            chkSales=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Customer")[0].firstChild.nodeValue;
            if(chkSales=="false") {Sales.checked=false;}
            else {Sales.checked=true;Mode.value="Customer";} 
        if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Supplier")[0]!=null)
        if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Supplier")[0].firstChild!=null)
            var chkPurchase="";
            chkPurchase=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Supplier")[0].firstChild.nodeValue;          
            if(chkPurchase=="false") {Purchase.checked=false;}
            else {Purchase.checked=true;Mode.value="Non Vat Dealer";} 
    }
}
function GetBranch()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetBranch(UserID.value, GetBranchComplete);    
}

function GetBranchComplete(res)
{
    getXml(res);    
    ddlBranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";    
        ddlBranch.options.add(opt);        
    }         
    else
    {
        //GetRefMember();
    }    
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlBranch.options.add(opt);
    }   
    if(hidGBID==null)
       GetDefaultBranch();
    ddlBranch.value=hidGBID.value;
    GetGroups();
    //GetRefMember(); 
}
function GetRefMember()
{    
    //PageMethods.GetRefMember(ddlBranch.value ,GetRefMemberComplete);    
}

function GetRefMemberComplete(res)
{
    getXml(res);    
    RefMember.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    RefMember.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        RefMember.options.add(opt);
    }    
}
function GetPins()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetPins(RefMember.value ,GetPinsComplete);    
}

function GetPinsComplete(res)
{
    getXml(res);    
    Pins.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    Pins.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PinNumber")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PinID")[0].firstChild.nodeValue;
        Pins.options.add(opt);
    }    
}
function GetRefName(RefNo)
{
    lblMember.innerText="";
    if (RefMember.value!="0" && RefMember.value!="")
    {
        PageMethods.GetRefName(RefNo,GetRefNameComplete);    
    }
}
function GetRefNameComplete(res)
{
   lblMember.innerText=res.toString(); 
   //GetPins();
}
function GetGroups()
{
    if(UserID==null)
       GetUserID();
    PageMethods.GetGroups(ddlBranch.value,GetGroupsComplete);
}

//function GetGroups()
//{
//    UserID=document.getElementById("hidUserID");
//    PageMethods.GetGroups(UserID.value,GetGroupsComplete);
//}

function GetGroupsComplete(res)
{
    getXml(res);
    var ddlGroup=document.getElementById(preid+"ddlGroupName");
    ddlGroup.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlGroup.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("GroupName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("GroupID")[0].firstChild.nodeValue;
        ddlGroup.options.add(opt);
    }
    var hidGroupID;
    hidGroupID=document.getElementById("hidGroupID"); 
    if(hidGroupID.value!=null && hidGroupID.value!="") 
        ddlGroup.value=hidGroupID.value;
    else 
        ddlGroup.value="0";
}


var Flag;
var navbtn;
var LMNO=0;
function btnLMRecordNav_Click(e,t)
{    
    GetCntrls();
//    if (ddlBranch.value!="0") 
//    {           
        try    
        {         
            Flag="F";
            navbtn=t;
            if(t.id=="btnFirst")
                Flag="F";
            else if(t.id=="btnPrev")
            {
               if(LMNO==0)
                  Flag="L";
               else
                  Flag="P";
            }
            else if(t.id=="btnNext")
                 Flag="N";
            else if(t.id=="btnLast")
                Flag="L";    
            
            if(UserID==null)
                GetUserID();
            navbtn.disabled=true;
         
            PageMethods.GetLMNavData(Search.value,ddlBranch.value,LMNO,Flag,GetLMNavDataComplete);
        }
        catch(err)
        {
            if(navbtn!=null)
                navbtn.disabled=false;
        }
//    }
}
function SearchYourID(e,t)
{
    if(e.keyCode==13)
    {
        Flag='';
        PageMethods.GetLMNavData(Search.value,ddlBranch.value,LMNO,Flag,GetLMNavDataComplete);
    }
}
function GetLMNavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {            
            alert("No data found.");            
            btnCancel_Click();
            return;
        }
        else
        {
            btnCancel_Click();            
            LMNO=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerNo")[0].firstChild.nodeValue,10);            
            LedgerNo.value=LMNO;
            LedgerID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerID")[0].firstChild.nodeValue,10);            
            //BranchID.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("YourID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("YourID")[0].firstChild!=null)
                    YourID.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Password")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Password")[0].firstChild!=null)
                    Password.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Password")[0].firstChild.nodeValue;
            Ledger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table1")[0]!=null)
            {
                if(xmlObj.getElementsByTagName("Table1")[0].getElementsByTagName("YourID")[0]!=null )
                {
                    if(xmlObj.getElementsByTagName("Table1")[0].getElementsByTagName("YourID")[0].firstChild!=null)
                    {
                        RefMember.value=xmlObj.getElementsByTagName("Table1")[0].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                        GetRefName(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefNo")[0].firstChild.nodeValue);
                    }
                }
            }
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LandMark")[0]!=null)            
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LandMark")[0].firstChild!=null )
                    LandMark.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LandMark")[0].firstChild.nodeValue;
            Group.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("GroupID")[0].firstChild.nodeValue;
            status.options.length=0;
            var stat=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Status")[0].firstChild.nodeValue;
            opt=document.createElement("OPTION");
            opt.text=stat;
            opt.value=stat;
            status.options.add(opt);
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild!=null)
                    Amount.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild.nodeValue;
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0]!=null)
//                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild!=null)
//                    RefName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild.nodeValue;
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Address")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Address")[0].firstChild!=null)
                    Address.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Address")[0].firstChild.nodeValue;
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Town")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Town")[0].firstChild!=null)
                    town.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Town")[0].firstChild.nodeValue;
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Phone")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Phone")[0].firstChild!=null)
                    phone.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Phone")[0].firstChild.nodeValue;
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("APGST")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("APGST")[0].firstChild!=null)
                    APGST.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("APGST")[0].firstChild.nodeValue;
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Mode")[0]!=null )
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Mode")[0].firstChild!=null)
                    Mode.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Mode")[0].firstChild.nodeValue;
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("AreaID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("AreaID")[0].firstChild!=null)
                    Area.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("AreaID")[0].firstChild.nodeValue;              
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PinNo")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PinNo")[0].firstChild!=null)
                    PinNo.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PinNo")[0].firstChild.nodeValue;              
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Sales")[0]!=null)
             {
                var chkSales="";
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Sales")[0].firstChild!=null)                    
                {
                    chkSales=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Sales")[0].firstChild.nodeValue;
                    if(chkSales=="false") {Sales.checked=false;}
                    else {Sales.checked=true;}
                }
             }
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Purchase")[0]!=null)
             {
                var chkPurchase="";
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Purchase")[0].firstChild!=null)                    
                {
                    chkPurchase=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Purchase")[0].firstChild.nodeValue;                      
                    if(chkPurchase=="false") {Purchase.checked=false;}
                    else {Purchase.checked=true;}
                }
             }
//                tblGrid=document.getElementById("tblGrid");
//                for(var i=tblGrid.rows.length-1;i>0;i--)
//                    tblGrid.deleteRow(i);
//                var xmlObjLength=xmlObj.getElementsByTagName("Table1").length;
//                for(var i=0;i<xmlObjLength;i++)
//                {                
//                    if(tblGrid.rows.length==1)
//                        var tr=tblGrid.insertRow(tblGrid.rows.length);
//                    else
//                        var tr=tblGrid.insertRow(tblGrid.rows.length-1);
//                    var j;
//                    for(j=0;j<2;j++)
//                    {
//                        tr.insertCell(j);
//                    }                               
//                    tr.cells[0].style.display="none";                    
//                    var PinID =parseInt(xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("PinID")[0].firstChild.nodeValue,10);                    
//                    tr.cells[0].innerHTML=PinID;
//                    tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("PinNumber")[0].firstChild.nodeValue;                    
//                    tr.onmouseover=function(){MouseOver(this);};
//                    tr.onmouseout=function(){MouseOut(this);};
//                    tr.onclick=function(){tblClick(this);}
//                 }             
            ActivateUpdate();
//            GetRefName();
        }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
            
    }
}

function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}

function ActivateUpdate()
{    
     document.getElementById("btnSave").style.display="none";
     document.getElementById("btnUpdate").style.display="block";   
     document.getElementById("btnDelete").style.display="block";
     //btnCancel_Click();    
}

function  ActivateSave()
{    
    document.getElementById("btnSave").style.display="block";
    document.getElementById("btnUpdate").style.display="none";    
    document.getElementById("btnDelete").style.display="none";
    btnCancel_Click();    
}

function NumberCheck(e,t)
{
 var my_string=e.value;
 if(isNaN(my_string))
 {
    e.value="";
    alert("Enter Valid Income ");
    e.focus();
  }  
}
function btnAdd_onclick()
{
    GetCntrls();
    if(Pins.value=="0")
    {
        alert("Select Pin");
        Pins.focus();
        return false;
    }
    //tblGrid =document.getElementById("tblGrid");
    var i;
    i=tblGrid.rows.length;
    var tr=tblGrid.insertRow(i);    
    for(i=0;i<2;i++)
    {
        tr.insertCell(i);
    }
    tr.cells[0].innerHTML=Pins.value;
    tr.cells[0].style.display="none";
    tr.cells[1].innerHTML=Pins[Pins.selectedIndex].innerText;        
    Pins.value="0";
    tr.onmouseover=function(){MouseOver(this);};
    tr.onmouseout=function(){MouseOut(this);};
    tr.onclick=function(){tblClick(this);}                 
    Pins.focus();
}
function MouseOver(t)
{
    if(t.cells[0].innerHTML.trim()!="")
    {
        t.style.backgroundColor="gray";
        t.style.cursor="hand";     
        t.title="Click to edit the values";  
    }
}

function MouseOut(t)
{
    if(t.cells[0].innerHTML.trim()!="")
    {
        t.style.backgroundColor="";
        t.style.crusor="text";        
    }
}


function tblClick(t)
{
    var Con=confirm("Are You Sure, Do You Want Edit?");
    if(Con=true)
    {
    if(t.cells[0].innerHTML.trim()!="")
    {        
        Pins.value=t.cells[0].innerHTML.trim();
        document.getElementById("tblGrid").deleteRow(t.rowIndex);
    }
  }
}
