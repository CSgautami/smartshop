﻿var preid = 'ctl00_ContentPlaceHolder1_';
var xmlObj;
var UserID=null;
var SearchItem;
var ddlItem;
var BatchNo;
var PurchasePrice;
var MRP;
var MRPMargin;
var Retail;
var RetailMargin;
var CustomerPrice;
var CustomerMargin;
var VIP;
var VIPMargin;
var Disc;
var CDisc;
var tblGrid;
//var ddlCompany;
var ddlStock;
//var lblItem;
var tdItem;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}
function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetControls()
{
    SearchItem =document.getElementById(preid+"txtSearch");
    //ddlCompany  =document.getElementById(preid+"ddlCompany");  
    ddlStock =document.getElementById(preid+"ddlStockGroup");  
//    ddlItem =document.getElementById(preid+"ddlItemCode");        
    tdItem=document.getElementById("tdItem");
    //lblItem =document.getElementById(preid+"lblItem");
    BatchNo =document.getElementById(preid+"txtBatch");
    PurchasePrice =document.getElementById(preid+"txtPurchasePrice");
    MRP =document.getElementById(preid+"txtMRP");
    MRPMargin =document.getElementById(preid+"txtMRPMargin");
    Retail =document.getElementById(preid+"txtRetail");
    RetailMargin  =document.getElementById(preid+"txtRetailMargin");
    CustomerPrice  =document.getElementById(preid+"txtCustomer");
    CustomerMargin =document.getElementById(preid+"txtCustomerMargin");
    VIP  =document.getElementById(preid+"txtVIP");    
    VIPMargin  =document.getElementById(preid+"txtVIPMargin");
    Disc  =document.getElementById(preid+"txtDiscount");
    CDisc  =document.getElementById(preid+"txtCashDiscount");        
    //tblGrid =document.getElementById("tblGrid");    
    UserID=document.getElementById(preid+"hidUserID");
    divItem=document.getElementById("divItem");
}
function GetStockItem()
{ 
    //PageMethods.GetStockItem(GetStockItemComplete);    
}

function GetStockItemComplete(res)
{
    getXml(res);    
    ddlItem.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlItem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
        ddlItem.options.add(opt);
    }    
}
function btnCancel_Click()
{
    SearchItem.value="";
    //ddlItem.value="0";
    tdItem.value="";
    //lblItem.innerText="";
    divItem.innerHTML="";
    selectedItem="0";
    BatchNo.value="";
    PurchasePrice.value="0";
    MRP.value="0";
    MRPMargin.value="0";
    Retail.value="0";
    RetailMargin.value="0";
    CustomerPrice.value="0";
    CustomerMargin.value="0";
    VIP.value="0";
    VIPMargin.value="0";
    Disc.value="0";
    CDisc.value="0";
    tdItem.focus();
    //ddlItem.focus();
//    GetData();    
}
function Cleartable()
{
    tblGrid =document.getElementById("tblGrid");
    for(var i=tblGrid.rows.length-1;i>0;i--)
    {
       tblGrid.deleteRow(i);
   
    }
}
function btnSave_Click(e,t)
{
    GetControls();
    try
    {        
        if (selectedItem=="0"||selectedItem=="")
        {
            alert("Select Item");
            tdItem.focus();
            return false;        
        }  
        if (BatchNo.value=="")
        {
            alert("Enter BatchNo");
            BatchNo.focus();
            return false;
        } 
        var Con=confirm("Are You Sure,Do You Want To Save?");
        if(Con=true)        
            PageMethods.SavePriceDetails(selectedItem, BatchNo.value,PurchasePrice.value, MRP.value, MRPMargin.value, Retail.value, RetailMargin.value, CustomerPrice.value,CustomerMargin.value, VIP.value, VIPMargin.value, Disc.value, CDisc.value, UserID.value, SavePriceDetailsComplete);           
     }
     catch(err)
     {
        t.disable=false;
     }    
 }
 function SavePriceDetailsComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Saving");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }    
    else
    {
        alert("Saved successfully");        
        //PageMethods.GetData(tdItem.value.substring(5,tdItem.value.length-1),ddlStock.value,GetDataComplete);        
        btnCancel_Click();        
        GetData();
    }
}
function GetData()
{
    try 
    {
        GetControls();
        PageMethods.GetData(SearchItem.value,ddlStock.value,GetDataComplete);        
    }
    catch(Err)
    {
    }
}
function GetDataComplete(res)
{
    try
    {        
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            //alert("No data found.");
            return;
        }
        else
        {               
            tblGrid=document.getElementById("tblGrid");
            for(var i=tblGrid.rows.length-1;i>0;i--)
                tblGrid.deleteRow(i);
            var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<xmlObjLength;i++)
            {                
//                if(tblGrid.rows.length==1)
//                    var tr=tblGrid.insertRow(tblGrid.rows.length);
//                else
//                    var tr=tblGrid.insertRow(tblGrid.rows.length-1);
                var tr=tblGrid.insertRow(i+1);
                var j;
                for(j=0;j<16;j++)
                {
                    tr.insertCell(j);
                }                                               
                tr.cells[0].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;                
                var ItemID =parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue,10);                
                tr.cells[1].style.display="none";
                tr.cells[1].innerHTML=ItemID;
                tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                tr.cells[3].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
                if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PurchasePrice")[0]!=null)
                    tr.cells[4].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("PurchasePrice")[0].firstChild.nodeValue;                
                tr.cells[5].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("MRP")[0].firstChild.nodeValue;                
                tr.cells[6].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("MRPMargin")[0].firstChild.nodeValue;                
                tr.cells[7].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RetailPrice")[0].firstChild.nodeValue;
                tr.cells[8].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RetailMargin")[0].firstChild.nodeValue;
                tr.cells[9].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CustomerPrice")[0].firstChild.nodeValue;                
                tr.cells[10].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CustomerMargin")[0].firstChild.nodeValue;
                tr.cells[11].innerHTML= xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("VIPPrice")[0].firstChild.nodeValue;                                
                tr.cells[12].innerHTML= xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("VIPMargin")[0].firstChild.nodeValue;
                tr.cells[13].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Discount")[0].firstChild.nodeValue;
                tr.cells[14].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CashDiscount")[0].firstChild.nodeValue;                
                //tr.cells[15].innerHTML="<a href='#' onclick='tblClick(event,this);'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='tblDeleteClick(event,this);'>Delete</a>";
                tr.cells[4].style.textAlign="right";
                tr.cells[5].style.textAlign="right";
                tr.cells[6].style.textAlign="right";
                tr.cells[7].style.textAlign="right";
                tr.cells[8].style.textAlign="right";
                tr.cells[9].style.textAlign="right";
                tr.cells[10].style.textAlign="right";
                tr.cells[11].style.textAlign="right";
                tr.cells[12].style.textAlign="right";                
                tr.cells[13].style.textAlign="right";
                tr.cells[14].style.textAlign="right";
                
                tr.onmouseover=function(){MouseOver(this);};
                tr.onmouseout=function(){MouseOut(this);};
                tr.ondblclick=function(){tblClick(event,this);}      
                tr.onkeydown=function(){tblDeleteClick(event,this)};
            }
         }
     }
    catch(err)
    {
    }    
 }
 function tblDeleteClick(e,t)
 {
        
        //var tr=t.parentNode.parentNode;
        var tr=t;
        if(e.keyCode==46)
        {
            var Con=confirm("Are You Sure,Do You Want To Delete?");
            if(Con==true)
            {
                PageMethods.DeletePriceList(tr.cells[1].innerHTML.trim(), tr.cells[3].innerHTML.trim(),BatchDeleteComplete);
            }
        }
    
 }
 function BatchDeleteComplete(res)
 {
    if(res=="1")
    {
        alert("Data Deleted successfully");
        GetData();
    }
    else
        alert("Error Occur While Deleting");    
 }
 function MouseOver(t)
{
    if(t.cells[0].innerHTML.trim()!="")
    {
        t.style.backgroundColor="gray";
        t.style.cursor="hand";     
        t.title="Click to edit the values";  
    }
}

function MouseOut(t)
{
    if(t.cells[0].innerHTML.trim()!="")
    {
        t.style.backgroundColor="";
        t.style.crusor="text";        
    }
}


function tblClick(e,t)
{
    var Con=confirm("Are You Sure,Do You Want To Edit?");
    if(Con==true)
    {
    //var tr=t.parentNode.parentNode;
    var tr=t;
    if(tr.cells[0].innerHTML.trim()!="")
    {      
        
        GetControls();  
        //ddlItem.value=t.cells[1].innerHTML.trim();
        selectedItem=tr.cells[1].innerHTML.trim();
        tdItem.value=tr.cells[0].innerHTML.trim();                
        //lblItem.innerText=t.cells[2].innerHTML.trim();                
        BatchNo.value=tr.cells[3].innerHTML.trim();
        PurchasePrice.value=tr.cells[4].innerHTML.trim();
        MRP.value=tr.cells[5].innerHTML.trim();
        MRPMargin.value=tr.cells[6].innerHTML.trim();
        Retail.value=tr.cells[7].innerHTML.trim();
        RetailMargin.value=tr.cells[8].innerHTML.trim();
        CustomerPrice.value=tr.cells[9].innerHTML.trim();
        CustomerMargin.value=tr.cells[10].innerHTML.trim();          
        VIP.value=tr.cells[11].innerHTML.trim();
        VIPMargin.value=tr.cells[12].innerHTML.trim();
        Disc.value=tr.cells[13].innerHTML.trim();
        CDisc.value=tr.cells[14].innerHTML.trim();
        document.getElementById("tblGrid").deleteRow(tr.rowIndex);        
    }
    }
}
//function SetData()
//{
//    GetControls();
//    GetStockItem();
//    GetData();
//}
var divItem;
var selectedItem="";
var prevText="";

function FillItem(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedItem="";
        divItem=document.getElementById("divItem");
        if(t.value!="")
        {        
            PageMethods.CallFillItem(t.value,FillItemComplete)
        }
        else{
        divItem.innerHTML="";
        divItem.style.display="none";
        }
    }
}

function FillItemComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var ItemID;
            var ItemName;
            var ItemCode;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild!=null)
                    ItemID=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild!=null)
                    ItemName=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            //lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild!=null)
                    //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                    ItemCode =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            //lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                selectedItem=ItemID;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divItem.innerHTML=tbl;
        if(count>0)
            divItem.style.display="block";
        else
            divItem.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectClick(t)
{
    selectedItem=t.cells[0].innerHTML;
    var txt=document.getElementById("tdItem");
    ApplyItem(txt);
}
function OnMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedItem ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedItem=t.cells[0].innerHTML;
}
function selectItem(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyItem(t);
    }
    else
    {
        if(divItem==null)
            divItem=document.getElementById("divItem");
        var tbl=divItem.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedItem=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedItem=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyItem(t)
{
    if(divItem==null)
            divItem=document.getElementById("divItem");
    if(selectedItem!="")
    {   
        var tbl=divItem.firstChild;
        var cnt=0;
        if(tbl!="" && tbl!=null)
        { 
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedItem="";
                t.value="";
            }
        }        
    }
    else
        t.value="";
    prevText=t.value;
    divItem.style.display="none";
    
}
function GetItemName()
{
   PageMethods.GetItemName(selectedItem,CompleteItemName);  
}
function CompleteItemName(res)
{
    try 
    {
        getXml(res);       
        //lblItem=document.getElementById(preid+"lblItem");     
        if(xmlObj.getElementsByTagName("Table").length==0)
        {          
            //lblItem.innerText="";                
            return;            
        }
        else
        {             
            //lblItem.innerText =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
        }      
    }
    catch(err)
    {
    }
}
function GetItemPriceDet()
{
    PageMethods.GetItemPriceDet(selectedItem,GetItemPriceDetComplete);
}
function GetItemPriceDetComplete(res)
{
    try
    {        
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            //alert("No data found.");
            return;
        }
        else
        {               
            
            var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
            if(xmlObjLength>0)
            {                                       
//                tdItem.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;                
//                var ItemID =parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ICID")[0].firstChild.nodeValue,10);                                
//                selectedItem=ItemID;
//                lblItem.innerText=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                BatchNo.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
                if (xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PurchasePrice")[0]!=null)
                    PurchasePrice.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PurchasePrice")[0].firstChild.nodeValue;                
                MRP.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("MRP")[0].firstChild.nodeValue;                
                MRPMargin.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("MRPMargin")[0].firstChild.nodeValue;                
                Retail.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RetailPrice")[0].firstChild.nodeValue;
                RetailMargin.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RetailMargin")[0].firstChild.nodeValue;
                CustomerPrice.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CustomerPrice")[0].firstChild.nodeValue;                
                CustomerMargin.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CustomerMargin")[0].firstChild.nodeValue;
                VIP.value= xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("VIPPrice")[0].firstChild.nodeValue;                                
                VIPMargin.value= xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("VIPMargin")[0].firstChild.nodeValue;
                Disc.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Discount")[0].firstChild.nodeValue;
                CDisc.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CashDiscount")[0].firstChild.nodeValue;                
                
            }
         }
     }
    catch(err)
    {
    }  
}