﻿
var preid='ctl00_ContentPlaceHolder1_';
var preid2='ctl00_ContentPlaceHolder2_'
var tblGrid;
var Total;
var DCharges;
var GrandTotal;
var tbl;
//var Mrp;
//var Price;
//var ItemName;    		        
//var Qty;    
//var ItemID;
//var Amount; 
var i;		    
var TempID=null;
var UserID;
var hidTempID;
var ImgAvailable;
var NoStock;
var BatchNo;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetCntrl()
{
    tblGrid=document.getElementById(preid2+"gvShopingCart");
    Total=document.getElementById(preid2+"lblTotal");
    DCharges=document.getElementById(preid2+"lblDCharges");
    GrandTotal=document.getElementById(preid2+"lblGrand");    
}
function ConfirmEdit()
{
    if (! confirm("Do you want to Edit?"))             
    return false;            
}
function ConfirmDelete()
{
    if (! confirm("Do you want to Delete?"))
    return false;            
}
function SaveCompleted(res)
{
    hidTempID.value=res;
}
function GetUserID()
{
    UserID=document.getElementById(preid+"hidUserID");
}
function GetTempID()
{
    hidTempID=document.getElementById(preid+"hidTempID");
}
function CalculateData()
{    
    Total.innerText=GetColTotal(tblGrid,8);
    if(Total.innerText<200)
        DCharges.innerText="10.00";
    else 
        DCharges.innerText="0.00";
    var nTotal=parseFloat(Total.innerText)+parseFloat(DCharges.innerText);
    GrandTotal.innerText=nTotal.toFixed(2);
}
function AddToCart(t)
{		
    //GetCntrl();
    var txtQty;
    var nItemID;
    var nPrice;
    var lblItemName;
    var lblBatch;
    var nAmt;
    var nMrp;
    var nQty;
    nItemID=parseInt(t.id);
    txtQty=document.getElementById(nItemID.toString()+"txtQty").value;    
    nQty=txtQty;
    lblBatch =document.getElementById("hidBat"+nItemID.toString());
    document.getElementById(nItemID.toString()+"txtQty").value="";
        if(UserID==null)
            GetUserID();
        if(hidTempID==null)
            GetTempID();
        if(hidTempID.value!="")
            TempID=hidTempID.value;   
   PageMethods.SaveTempOrder(nItemID,nQty,lblBatch.value,TempID ,UserID.value,GetDataCompleted);       
 }
 function GetData()
{
    if(hidTempID==null)
        GetTempID();
    PageMethods.GetData(hidTempID.value,GetDataCompleted);
}
function GetDataCompleted(res)
{
   // if(txtQty!=null) txtQty="";
    GetCntrl();
    getXml(res); 
    if(xmlObj.getElementsByTagName("Table").length==0)
    {    
        var i;
        i=tblGrid.rows.length;
        var tr=tblGrid.insertRow(i);
        tr.className="product_heading";
        //tr.style.backgroundColor="f6fef0";    
        for(i=0;i<12;i++)
        {		        
            tr.insertCell(i);
        }
        tr.cells[0].style.display="none";
        tr.cells[1].innerHTML="No Items Selected";
        tr.cells[2].style.display="none";
        tr.cells[11].style.display="none";
    }
    else 
    {
        try
        {
            for(var i=tblGrid.rows.length-1;i>0;i--)
                tblGrid.deleteRow(i);
            var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<xmlObjLength;i++)
            {       
                
                var tr=tblGrid.insertRow(i+1);
                tr.className="product_item";
                //tr.style.backgroundColor="f6fef0"; 
                for(var j=0;j<12;j++)
                {		        
                    tr.insertCell(j);
                }
                var x=0;
                tr.cells[x].style.display="none";
                tr.cells[x+2].style.display="none";
               // tr.cells[x+10].style.display="none";
                tr.cells[x+11].style.display="none";
                ItemID =parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemID")[0].firstChild.nodeValue,10);
                tr.cells[x].innerHTML=ItemID;          
                x=x+1;  
                
                tr.cells[x].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemName")[0].firstChild.nodeValue;            
                tr.cells[x].width="134";
                tr.cells[x].height="25";
                x=x+1;
                
                tr.cells[x].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("MRP")[0].firstChild.nodeValue;
                x=x+1;
                
                tr.cells[x].innerHTML="";
                tr.cells[x].width="5";
                x=x+1;            
                
                var nPrice=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Price")[0].firstChild.nodeValue;
                tr.cells[x].innerHTML=nPrice;
                tr.cells[x].width="44";            
                x=x+1;
                
                tr.cells[x].innerHTML="";
                tr.cells[x].width="1";
                x=x+1;
                
                var nQty=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Qty")[0].firstChild.nodeValue;
                tr.cells[x].innerHTML=nQty;   
                tr.cells[x].width="31";
                x=x+1;
                
                tr.cells[x].innerHTML="";
                tr.cells[x].width="1";
                x=x+1;    
                        
                tr.cells[x].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Amount")[0].firstChild.nodeValue;             
                tr.cells[x].width="72";
                x=x+1;
                
                tr.cells[x].innerHTML="";
                tr.cells[x].width="4";
                x=x+1;
                
                //tr.cells[x].innerHTML="<asp:ImageButton ImageUrl='MasterPageFiles/cross symbol.png' />";            
                tr.cells[x].innerHTML="<input type='image' id='"+ ItemID.toString() +"' src='MasterPageFiles/cross symbol.png' runat='server' onclick='btnDelete_Click(this)'/>";            
                //tr.cells[x].innerHTML="<input type='image' id='img' src='MasterPageFiles/cross symbol.png' runat='server'/>";                        
                x=x+1;
                tr.cells[x].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;            
            
                hidTempID.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TOID")[0].firstChild.nodeValue;            
                 
            }
           CalculateData();
       }
       catch(err)
       {
           throw err;        
       } 
    }
}
function SetData()
{
    GetCntrl();
    GetData();
    //CheckAvailability();
}
function CheckOutValidation()
{
    if(tblGrid.rows[1].cells[0].innerHTML=="")
    {
        alert("Select Items.");
        return false;
    }
}
function btnDelete_Click(t)
{
    try 
    {
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
        {
            //var tblItemID =t.parentNode.parentNode.cells[0].innerText;                        
            var tblItemID=parseInt(t.id);
           // PageMethods.DeleteTempOrder(tblItemID,hidTempID.value, GetDataCompleted);                                
        }
    }
    catch(err)
    {
     throw err;
    }
}


