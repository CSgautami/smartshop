﻿var preid = '';

function EditClick(e, t) {
    btnCancel_Click();    
    var DistrictID = document.getElementById("hidDistrictID");
    document.getElementById(preid + "txtDistrict").value = "";
    DistrictID.value= "";
    var District = t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    
    var res = confirm("Are You Sure, Do you want to Edit?: " + District);
    if (res == true) {
       DistrictID.value= t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        
        document.getElementById(preid + "txtDistrict").value = District;
      
        document.getElementById("btnSave").style.display = "none";
        document.getElementById("btnUpdate").style.display = "block";
        document.getElementById("btnCancel").style.display = "block";
    }
}
function DeleteClick(e, t) {
    btnCancel_Click();    
    document.getElementById(preid + "txtDistrict").value = "";
    var District = t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    var res = confirm("Are You Sure,Do you want to Delete?: "+District);
    if (res == true) {
        var DistrictID = document.getElementById("hidDistrictID");
        DistrictID.value = "";
        DistrictID.value= t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        PageMethods.DeleteClick(DistrictID.value, DeleteComplete);
        
    }
}

function DeleteComplete(res) {    
    showtable(res);    
    alert("Name Deleted Successfully.");
}


function btnCancel_Click() {    
    document.getElementById(preid + "txtDistrict").value = "";
    document.getElementById("hidDistrictID").value = "";
    document.getElementById("btnSave").style.display = "block";
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "block";
}


function btnSave_Click(t) {    
    if (document.getElementById(preid + "txtDistrict").value == "") {        
        premsgbox("Enter DistrictName", "txtDistrict");
        return false;
    }
    var DistrictName = document.getElementById(preid + "txtDistrict").value;
    t.disabled = true;
    var Con=confirm("Are You Sure,Do You Want To Save?");
    if(Con=true)  
    PageMethods.btnSave_Click(DistrictName, SaveComplete);        
}
function SaveComplete(res) {
try
{
    document.getElementById(preid + "txtDistrict").focus();
    if(res=="-2")
    {
        document.getElementById(preid + "txtDistrict").value="";
        alert("Enter Name.");
        return;
    }
    if (res == "" || res == "0") {
        alert("Error in insertion.");        
        return;
    }
    if (res == "-1") {
        alert("Name already exist.");        
        return;
    }
    document.getElementById(preid + "txtDistrict").value="";
    showtable(res);
    
    alert("Name Saved Successfully.");  
}
catch(err)
{
}
finally
{
document.getElementById("btnSave").disabled = false;
}  

}


function btnUpdate_Click(t) {    
    if (document.getElementById(preid + "txtDistrict").value == "") {        
        premsgbox("Select District ", "txtDistrict");
        return false;
    }
    var DistrictID = document.getElementById(preid+"hidDistrictID").value;
    var DistrictName = document.getElementById(preid + "txtDistrict").value;
    t.disabled=true;
    var Con=confirm("Are You Sure,Do You Want To Update?");
    if(Con=true)  
    PageMethods.btnUpdate_Click(DistrictID, DistrictName, UpdateComplete);    
}

function UpdateComplete(res)
{
    document.getElementById(preid + "txtDistrict").focus();
    if(res=="-2")
    {
        document.getElementById(preid + "txtDistrict").value="";
        alert("Enter Name.");
        return;
    }
    if (res == "" || res == "0") {
        alert("Error in insertion.");        
        return;
    }
    if (res == "-1") {
        alert("Name already exist.");        
        return;
    }
    btnCancel_Click();
    showtable(res);
    document.getElementById("btnUpdate").disabled = false;
    alert("Name Updated Successfully.");     
}

function showtable(res)
{
    var xmlObj = getXml(res);
    var tbl = document.getElementById(preid + "gvDistrict");
    for (var i = tbl.rows.length - 1; i > 0; i--) {
        tbl.deleteRow(i);
    }        
    tbl.rows[0].cells[1].style.position = 'relative';
    tbl.rows[0].cells[1].style.width = 100;
    for (var i = 0; i < xmlObj.getElementsByTagName("DistrictName").length; i++) {
        tbl.insertRow(i + 1)
        tbl.rows[i + 1].insertCell(0);
        tbl.rows[i + 1].insertCell(1);        
        tbl.rows[i + 1].insertCell(2);
        tbl.rows[i + 1].cells[0].className='hidendStyle';
        tbl.rows[i + 1].cells[0].innerHTML =  xmlObj.getElementsByTagName("District")[i].getElementsByTagName("DistrictID")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[1].innerHTML = xmlObj.getElementsByTagName("District")[i].getElementsByTagName("DistrictName")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[2].innerHTML = "<a href='#' onclick='EditClick(event,this)' class='gridNavButtons'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='DeleteClick(event,this)' class='gridNavButtons'>Delete</a>";
        tbl.rows[i + 1].cells[0].style.display="none";
        
    }
}


function getXml(xmlString) {
    var xmlObj;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    return xmlObj;
}

function premsgbox(msg, cntrl) {
    try {
        document.getElementById(preid + cntrl).focus();
        alert(msg);        
    } catch (error) { }

}