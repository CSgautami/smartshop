﻿var dtFormat='dd-MM-yyyy'
var xmlObj;
var cashEntrycnt=0;
var paymentcnt=0;
var receiptcnt=0;
var journalcnt=0;
var UserID=null;
var dbeDate;
var dtCEDate;
var dtPDate;
var dtRDate;
var dtJDate;
var controlID;
var Count=0;
var tab="CE";
var Vocher;
var CEID=0;
var PEID=0;
var REID=0;
var JEID=0;
var ddlTempBranch;
var ddlBranch;
var ddlCEBranch;
var ddlPaymentBranch;
var ddlReceiptBranch;
var ddlJournalBranch;
var ddlGBranch;
var hidGBID;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById("hidGBID");
}

function SetVisibility()
{
    
    document.getElementById("divCashEntry").style.display="none";
    document.getElementById("divPayment").style.display="none";
    document.getElementById("divReceipt").style.display="none";
    document.getElementById("divJournal").style.display="none";    
    var tblnavrow=document.getElementById("tblDBNav").rows[0];
    for(var i=0;i<tblnavrow.cells.length-1;i++)
    {
        tblnavrow.cells[i].getElementsByTagName("a")[0].className="DBENav";        
    }
}

function SelectTab(t)
{
    Count=0;    
    if(t.className=="DBENavClick")
        return;
    CEID=0;
    PEID=0;
    REID=0;
    JEID=0;
    SetVisibility();
    t.className="DBENavClick";
    if(t.id=="CashEntry")
    {        
        document.getElementById("divCashEntry").style.display="block";
        if(cashEntrycnt==0)
        {
        GetCELedger();
        cashEntrycnt++;
        }
        tab="CE";
    }
    else if(t.id=="Payment")
    {
        document.getElementById("divPayment").style.display="block";        
        //ddlTempBranch=document.getElementById("ddlPaymentBranch");
        tab="PE";
        if(paymentcnt==0)
        {
            GetPaymentLedgers();            
            paymentcnt++;
        }
    }
    else if(t.id=="Receipts")
    {
        document.getElementById("divReceipt").style.display="block";
        //ddlTempBranch=document.getElementById("ddlReceiptBranch");
        tab="RE";
        if(receiptcnt==0)
        {            
            GetReceiptLedgers();
            receiptcnt++;
        }
    }
    else if(t.id=="JournalEntry")
    {    
        document.getElementById("divJournal").style.display="block";
        //ddlTempBranch=document.getElementById("ddlJournalBranch");
        tab="JE";
        if(journalcnt==0)
        {
            GetJournalLedger();
            journalcnt++;
        }        
    }
    //GetBranch();
    ActivateSave();
}
function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}
function GetDayBook()
{
    try
    {
        if(UserID==null)
            GetUserID();     
        document.getElementById("btnReport").disabled=true;
        PageMethods.GetDayBookReport(ddlBranch.value,dbeDate.value,GetDayBookComplete);
    }
    catch(err)
    {
        document.getElementById("btnReport").disabled=false;
    }
}

function GetDayBookComplete(res)
{
    try
    {
        getXml(res);
        var tbl=document.getElementById("tblGrid");
        ClearTable(tbl);
        tbl.rows[0].cells[0].style.display="none";
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            var tr=tbl.insertRow(i+1);
            tr.insertCell(0);
            tr.insertCell(1);
            tr.insertCell(2);
            tr.insertCell(3);
            tr.insertCell(4);
            tr.insertCell(5);
            tr.insertCell(6);
            tr.insertCell(7);
            tr.insertCell(8);
            tr.insertCell(9);
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TransDate")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TransDate")[0].firstChild!=null)
                    tr.cells[0].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TransDate")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0].firstChild!=null)
                    tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Voch")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Ledger")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Ledger")[0].firstChild!=null)
                    tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Ledger")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0].firstChild!=null)
                    tr.cells[3].innerHTML="<div style='overflow:hidden; width:210px;  height:18px;' title='"+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0].firstChild.nodeValue+"'>"+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Particulars")[0].firstChild.nodeValue+"</div>";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("InAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("InAmount")[0].firstChild!=null)
                    tr.cells[4].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("InAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("OutAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("OutAmount")[0].firstChild!=null)
                    tr.cells[5].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("OutAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CreditAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CreditAmount")[0].firstChild!=null)
                    tr.cells[6].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CreditAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DebitAmount")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DebitAmount")[0].firstChild!=null)
                    tr.cells[7].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DebitAmount")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0].firstChild!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0].firstChild.nodeValue!="0")
                        tr.cells[8].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalID")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RefNo")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RefNo")[0].firstChild!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RefNo")[0].firstChild.nodeValue!="0")
                        tr.cells[9].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RefNo")[0].firstChild.nodeValue;
            tr.cells[0].style.display="none";
            tr.cells[8].style.display="none";
            tr.cells[9].style.display="none";
            tr.cells[3].noWrap=true;
            tr.cells[4].style.textAlign="right";
            tr.cells[5].style.textAlign="right";
            tr.cells[6].style.textAlign="right";
            tr.cells[7].style.textAlign="right";       
            tr.onmouseover=function(){DayBookMouseOver(this);};
            tr.onmouseout=function(){DayBookMouseOut(this);};
            tr.onclick=function(){DayBookClick(this);}
            
        }
        document.getElementById("divDayBook").scrollTop=tbl.offsetHeight;
    }
    catch(er)
    {
    }
    finally    
    {
        document.getElementById("btnReport").disabled=false;
    }    
}

function ClearTable(tbl)
{
    for (var i = tbl.rows.length - 1; i > -0; i--)
    {
        tbl.deleteRow(i);
    } 
}

function DayBookMouseOver(t)
{
    if(t.cells[8].innerHTML.trim()!="")
    {
        t.style.backgroundColor="gray";
        t.style.cursor="hand";     
        t.title="Click to edit the values";  
    }
}

function DayBookMouseOut(t)
{
    if(t.cells[8].innerHTML.trim()!="")
    {
        t.style.backgroundColor="";
        t.style.crusor="text";        
    }
}


function DayBookClick(t)
{
    if(t.cells[8].innerHTML.trim()!="")
    {
        var jid=t.cells[8].innerHTML.trim();
        var voc=t.cells[1].innerHTML.trim();
        var RefNo=t.cells[9].innerHTML.trim();
        document.getElementById("CashEntry").className="DBENav";
        document.getElementById("Payment").className="DBENav";
        document.getElementById("Receipts").className="DBENav";
        document.getElementById("JournalEntry").className="DBENav";
        //document.getElementById("CashEntry").style.color="white";
        //document.getElementById("Payment").style.color="white";
        //document.getElementById("Receipts").style.color="white";
        //document.getElementById("JournalEntry").style.color="white";
        tab="";
        if(voc=="CE" || voc=="PE" || voc=="RE" || voc=="JE")      
            SetVisibility();
            //PageMethods.GetGVCELedger(ddlTempBranch.value,jid,voc,GetgvCELedgerComplete);
        if(voc=="CE")
        {
            //document.getElementById("CashEntry").style.color="Green";
           // SelectTab(document.getElementById("CashEntry"));            
            document.getElementById("divCashEntry").style.display="block"; 
            document.getElementById("CashEntry").className="DBENavClick";         
            tab="CE";
            if(cashEntrycnt==0)
            {
                //GetCELedger();
                cashEntrycnt++;
            }                        
            PageMethods.GetCENavData(ddlCEBranch.value,RefNo,"",GetCENavDataComplete);
        }
        else if(voc=="PE")
        {
            //document.getElementById("Payment").style.color="Green";                         
            document.getElementById("divPayment").style.display="block";
            document.getElementById("Payment").className="DBENavClick";                        
            tab="PE";
            if(paymentcnt==0)
            {
              GetPaymentLedgers();
              paymentcnt++;
            }
            PageMethods.GetPENavData(ddlPaymentBranch.value,RefNo,"",GetPENavDataComplete);
        }
        else if(voc=="RE")
        {
           // document.getElementById("Receipts").style.color="Green";
            document.getElementById("divReceipt").style.display="block";
            document.getElementById("Receipts").className="DBENavClick";                        
            tab="RE";
            if(receiptcnt==0)
            {
              GetReceiptLedgers();
              receiptcnt++;
            }
            PageMethods.GetRENavData(ddlReceiptBranch.value,RefNo,"",GetRENavDataComplete);            
        }
        else if(voc=="JE")
        {    
            //document.getElementById("JournalEntry").style.color="Green";
            //SetVisibility();
            document.getElementById("divJournal").style.display="block";
            document.getElementById("JournalEntry").className="DBENavClick";            
            tab="JE";
            if(journalcnt==0)
            {
              GetJournalLedger();
              journalcnt++;
            }
            PageMethods.GetJENavData(ddlJournalBranch.value,RefNo,"",GetJENavDataComplete);
       }
       else if(voc=="P")
       {
          //window.showModalDialog("Purchase.aspx ?ID=" + 31, " dialogHeight:800px;dialogWidth:900px" );          
          window.showModalDialog("Purchase.aspx?ID="+RefNo+ "&BranchID=" + ddlBranch.value+"&dt="+new Date().getTime(),"","dialogHeight:550px; dialogWidth:1000px");
       }
       else if(voc=="PR")
       {
        //window.showModalDialog("PurchaseReturn.aspx ?ID=" + 31, " dialogHeight:800px;dialogWidth:900px" );
          window.showModalDialog("PurchaseReturn.aspx?ID="+RefNo+ "&BranchID=" + ddlBranch.value+"&dt="+new Date().getTime(),"", "dialogHeight:550px; dialogWidth:1000px");
       }
       else if(voc=="S")
       {
        //window.showModalDialog("Sale.aspx ?ID=" + 31, " dialogHeight:800px;dialogWidth:900px" );
          window.showModalDialog("Sale.aspx?ID="+RefNo+ "&BranchID=" + ddlBranch.value+"&dt="+new Date().getTime(),"" , "dialogHeight:550px; dialogWidth:1000px");
       }
       else if(voc=="SR")
       {
         //window.showModalDialog("SalesReturn.aspx ?ID=" + 31, " dialogHeight:800px;dialogWidth:900px" );
          window.showModalDialog("SalesReturn.aspx?ID="+RefNo+ "&BranchID=" + ddlBranch.value+"&dt="+new Date().getTime(),"" , "dialogHeight:550px; dialogWidth:1000px");
       }       
       //GetDayBook();
       ActivateUpdate();
      Vocher=t.cells[1].innerHTML;
      ddlTempBranch = document.getElementById("ddlBranch");
      switch(Vocher)
      {
        case "CE":ddlTempBranch=document.getElementById("ddlCEBranch");
            break;
        case "PE":ddlTempBranch=document.getElementById("ddlPaymentBranch");
            break;
        case "RE":ddlTempBranch=document.getElementById("ddlReceiptBranch");
            break;
        case "JE":ddlTempBranch=document.getElementById("ddlJournalBranch");
            break;
      }
//      if(tab=="CE" || tab=="PE" || tab=="RE" )      
//        PageMethods.GetGVCELedger(ddlTempBranch.value,jid,voc,GetgvCELedgerComplete);
    }
}

function InitilizeCEControls()
{
        var date=document.getElementById("txtCEDate");
        var ledger=document.getElementById("ddlCELedgerName");
        var income=document.getElementById("txtCEIncome");
        var expenses=document.getElementById("txtCEExpences");
        var narration=document.getElementById("txtCENarration");
        var divJournalNo=document.getElementById("divJournalNo");
}
function InitilizePEControls()
{
    var PEdate=document.getElementById("txtPaymentDate");
    var divPaymentNo=document.getElementById("divPaymentNo");
    var PEdate=document.getElementById("txtPaymentDate");
    var PEname=document.getElementById("txtPaymentName");
    var PEddltoledger=document.getElementById("ddlPaymentToLedger");
    var PEddlfrom=document.getElementById("ddlPaymentFrom");
    var ddlPaymentType=document.getElementById("ddlPaymentType");
    var PEtypedesc=document.getElementById("txtPaymentTypeDesc");
    var PEtowards=document.getElementById("txtPaymentTowards");
    var PEamount=document.getElementById("txtPaymentAmount");
}
function InitilizeREControls()
{
        var REdate=document.getElementById("txtReceiptDate");
        var REId=document.getElementById("divReceiptId");
        var REDate=document.getElementById("txtReceiptDate");
        var REName=document.getElementById("txtReceiptName");
        var REFromLedger=document.getElementById("ddlReceiptFromLedger");
        var REddlTo=document.getElementById("ddlReceiptTo");
        var REddlType=document.getElementById("ddlReceiptType");
        var REtypeDesc=document.getElementById("txtReceiptTypeDesc");
        var REtowards=document.getElementById("txtReceiptTowards");
        var REamount=document.getElementById("txtReceiptAmount");
}
function InitilizeJEControls()
{
   divJENo=document.getElementById("divJourNo");
   ddlJLedger=document.getElementById("ddlJournalLedger");
   credit=document.getElementById("txtJournalCredit");
   debit=document.getElementById("txtJournalDebit");
   narration=document.getElementById("txtJournalNarration");
   tdJournalLedger=document.getElementById("tdJournalLedger");
   //dtJDate=document.getElementById("txtJournalDate");
   
}

function GetgvCELedgerComplete(res)
{
      getXml(res);
      SetVisibility();
      if(Vocher=="CE")
      {      
        document.getElementById("divCashEntry").style.display="block"; 
        document.getElementById("CashEntry").className="DBENavClick";          
        CEID=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("JournalNo")[0].firstChild.nodeValue;
        PageMethods.GetCENavData(ddlCEBranch.value,CEID,"",GetCENavDataComplete);
//        var date=document.getElementById("txtCEDate");
//        var ledger=document.getElementById("ddlCELedgerName");
//        var income=document.getElementById("txtCEIncome");
//        var expenses=document.getElementById("txtCEExpences");
//        var narration=document.getElementById("txtCENarration");
//        var divJournalNo=document.getElementById("divJournalNo");
//        
//       divJournalNo.innerHTML="<tr><td>"+xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("JournalNo")[0].firstChild.nodeValue+"</td></tr>";
//       CEID=divJournalNo.innerText;
//       date.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Date")[0].firstChild.nodeValue;
//       ledger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
//       income.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CrAmt")[0].firstChild.nodeValue;
//       expenses.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("DrAmt")[0].firstChild.nodeValue;
//       narration.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild.nodeValue;
      }
      if(Vocher=="PE")
      {
           // ActivateUpdate();
           document.getElementById("divPayment").style.display="block";
           document.getElementById("Payment").className="DBENavClick";  
//            var divPaymentNo=document.getElementById("divPaymentNo");
//            var PEdate=document.getElementById("txtPaymentDate");
//            var PEname=document.getElementById("txtPaymentName");
//            var PEddltoledger=document.getElementById("ddlPaymentToLedger");
//            var PEddlfrom=document.getElementById("ddlPaymentFrom");
//            var ddlPaymentType=document.getElementById("ddlPaymentType");
//            var PEtypedesc=document.getElementById("txtPaymentTypeDesc");
//            var PEtowards=document.getElementById("txtPaymentTowards");
//            var PEamount=document.getElementById("txtPaymentAmount");           
           var PID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentNo")[0].firstChild.nodeValue,10);
           PageMethods.GetPENavData(ddlPaymentBranch.value,PID,"",GetPENavDataComplete);
//           divPaymentNo.innerHTML=PID;
//           PEdate.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentDate")[0].firstChild.nodeValue;
//           PEname.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild.nodeValue;
//           PEddltoledger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ToLedgerID")[0].firstChild.nodeValue;
//           PEddlfrom.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("FromLedgerID")[0].firstChild.nodeValue;
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0]!=null)
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild!=null)
//                 PEamount.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild.nodeValue;
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentType")[0]!=null)
//              if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentType")[0].firstChild!=null)
//              {
//                    ddlPaymentType.options.length=0;
//                    var PayType=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentType")[0].firstChild.nodeValue;
//                    opt=document.createElement("OPTION");
//                    opt.text=PayType;
//                    opt.value=PayType;
//                    ddlPaymentType.options.add(opt);
//              }
//              if(ddlPaymentType.value=="Cash" || ddlPaymentType.value=="0")
//                 document.getElementById("txtPaymentTypeDesc").style.display="none";
//              else
//              {
//                 document.getElementById("txtPaymentTypeDesc").style.display="block";
//                 PEtypedesc.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentTypeDesc")[0].firstChild.nodeValue;
//              }
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0]!=null)
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild!=null)
//           PEtowards.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild.nodeValue;
    }
    if(Vocher=="RE")
    {
        ActivateUpdate();
        document.getElementById("divReceipt").style.display="block";
        document.getElementById("Receipts").className="DBENavClick";
//        var REId=document.getElementById("divReceiptId");
//        var REDate=document.getElementById("txtReceiptDate");
//        var REName=document.getElementById("txtReceiptName");
//        var REFromLedger=document.getElementById("ddlReceiptFromLedger");
//        var REddlTo=document.getElementById("ddlReceiptTo");
//        var REddlType=document.getElementById("ddlReceiptType");
//            REddlType.options.length=0;
//        var opt=document.createElement("OPTION");
//        var REtypeDesc=document.getElementById("txtReceiptTypeDesc");
//        var REtowards=document.getElementById("txtReceiptTowards");
//        var REamount=document.getElementById("txtReceiptAmount");
//        

           var RID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptNo")[0].firstChild.nodeValue,10);
           PageMethods.GetRENavData(ddlReceiptBranch.value,RID,"",GetRENavDataComplete);
//           REId.innerHTML=RID;
//           REDate.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptDate")[0].firstChild.nodeValue;
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0]!=null)
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild!=null)
//                 REName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild.nodeValue;
//           REFromLedger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("FromLedgerID")[0].firstChild.nodeValue;
//           REddlTo.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ToLedgerID")[0].firstChild.nodeValue;
//           opt=document.createElement("OPTION");
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptType")[0]!=null)
//           {
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptType")[0].firstChild!=null)
//             {
//                 
//                    var ReceiptType=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptType")[0].firstChild.nodeValue;
//                    opt=document.createElement("OPTION");
//                    opt.text=ReceiptType;
//                    opt.value=ReceiptType;
//                    REddlType.options.add(opt);
//             }
//           }
//           if(REddlType.value=="Cash" || REddlType.value=="0")
//               document.getElementById("txtReceiptTypeDesc").style.display="none";
//           else
//           {
//               document.getElementById("txtReceiptTypeDesc").style.display="block";
//               REtypeDesc.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptTypeDesc")[0].firstChild.nodeValue;
//           }
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0]!=null)
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild!=null)
//                REtowards.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild.nodeValue;
//           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0]!=null)
//             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild!=null)
//                REamount.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild.nodeValue;
       


    }
    if(Vocher=="JE")
    {
        document.getElementById("divJournal").style.display="block";
        document.getElementById("JournalEntry").className="DBENavClick";
//        var JOurNo=document.getElementById("divJourNo");
//        var tblJournal = document.getElementById("tblJournal");
//        var JDate=document.getElementById("txtJournalDate");
//            for(var i=tblJournal.rows.length-1;i>0;i--)
//                tblJournal.deleteRow(i);
//            var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
//            for(var i=0;i<xmlObjLength;i++)
//            {
//                
//                if(tblJournal.rows.length==1)
//                    var tr=tblJournal.insertRow(tblJournal.rows.length);
//                else
//                    var tr=tblJournal.insertRow(tblJournal.rows.length-1);
//                tr.insertCell(0);
//                tr.insertCell(0);
//                tr.insertCell(0);
//                tr.insertCell(0);
//                tr.insertCell(0);
//                tr.insertCell(0);
//                //tr.insertCell(0);
//                                
                //var JID=parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("JournalNo")[0].firstChild.nodeValue,10);
                var JID=parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RefNo")[0].firstChild.nodeValue,10);
                PageMethods.GetJENavData(ddlJournalBranch.value,JID,"",GetJENavDataComplete);
//                JOurNo.innerHTML=JID;
//                var JID=parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue,10);
//                tr.cells[0].innerHTML="<input type='hidden' value='"+JID+"'/>";
//                tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
//                var Narration=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0].firstChild.nodeValue;
//                tr.cells[2].innerHTML="<div style='width:340px; overflow:hidden;' title='"+Narration+"' />"+Narration+"</div>";
//                var credit=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CrAmt")[0].firstChild.nodeValue;
//                tr.cells[3].innerHTML=credit;
//                var debit=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DrAmt")[0].firstChild.nodeValue;
//                tr.cells[4].innerHTML=debit;
//                tr.cells[5].innerHTML="<a href='#' onclick='JEdit_Click(event,this);'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='JDelete_Click(event,this);'>Delete</a>";
//                tr.cells[5].noWrap=true;
//                JDate.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Date")[0].firstChild.nodeValue;
//                
//                tr.cells[0].style.display="none";
//            }
//            trtot=tblJournal.insertRow(tblJournal.rows.length);
//            trtot.insertCell(0);
//            trtot.insertCell(0);
//            trtot.insertCell(0);
//            trtot.insertCell(0);
//            trtot.insertCell(0);
//            trtot.insertCell(0);
//            trtot.cells[1].innerHTML="<b>Total</bt>";
//            trtot.cells[1].style.textAlign="center";
//            
//            var tblJournal = document.getElementById("tblJournal");
//            totCredit=0;
//            totDebit=0;
//            for(var i=1;i<tblJournal.rows.length-1;i++)
//            {
//                if(!isNaN(tblJournal.rows[i].cells[3].innerText)&&tblJournal.rows[i].cells[3].innerText!="")
//                    totCredit+=parseFloat(tblJournal.rows[i].cells[3].innerText);
//                if(!isNaN(tblJournal.rows[i].cells[4].innerText)&&tblJournal.rows[i].cells[4].innerText!="")
//                    totDebit+=parseFloat(tblJournal.rows[i].cells[4].innerText);
//            }
//            tblJournal.rows[tblJournal.rows.length-1].cells[2].innerHTML="<b>"+totCredit.toFixed(2)+"</b>";
//            tblJournal.rows[tblJournal.rows.length-1].cells[3].innerHTML="<b>"+totDebit.toFixed(2)+"</b>";
    }
}
function GetCELedger()
{    
    if(cashEntrycnt==0)
    {
        cashEntrycnt++;
        //PageMethods.GetCELedger(ddlCEBranch.value,GetCELedgerComplete);        
    }
}
function SelectBranch()
{
    //document.getElementById("hidGBID")=ddlBranch.value  
    GetDayBook();
    ddlCEBranch.value =ddlBranch.value;         
    ddlPaymentBranch.value =ddlBranch.value;
    ddlReceiptBranch.value =ddlBranch.value;     
    ddlJournalBranch.value =ddlBranch.value;      
    cashEntrycnt=0;
    paymentcnt=0;
    receiptcnt=0;
    journalcnt=0;      
//    GetCELedger();
}

function GetCELedgerComplete(res)
{
    getXml(res);
    var ddlCELedger=document.getElementById("ddlCELedgerName");
    ddlCELedger.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddlCELedger.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlCELedger.options.add(opt);
    }
    //ddlCELedger.focus();
}

function GetPaymentLedgers()
{
    if(paymentcnt==0)
    {
        paymentcnt++;
        PageMethods.GetPayRecLedgers(ddlPaymentBranch.value,GetPaymentLedgersComplete);
    }
}

function GetPaymentLedgersComplete(res)
{
    getXml(res);
//    var ddlPaymentToLedger=document.getElementById("ddlPaymentToLedger");
//    ddlPaymentToLedger.options.length=0;
//    var opt=document.createElement("OPTION");
//    opt.text="Select";
//    opt.value="0";
//    ddlPaymentToLedger.options.add(opt);
//    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//    {
//        opt=document.createElement("OPTION");
//        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
//        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
//        ddlPaymentToLedger.options.add(opt);
//    }
    var ddlPaymentFrom=document.getElementById("ddlPaymentFrom");
    ddlPaymentFrom.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddlPaymentFrom.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlPaymentFrom.options.add(opt);
    }
    var ddlPaymentType=document.getElementById("ddlPaymentType");
    ddlPaymentType.options.length=0;
    opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddlPaymentType.options.add(opt);    
}


function GetReceiptLedgers()
{
    if(receiptcnt==0)
    {
        receiptcnt++;
        PageMethods.GetPayRecLedgers(ddlReceiptBranch.value,GetReceiptLedgersComplete);
    }
}

function GetReceiptLedgersComplete(res)
{
    getXml(res);
//    var ddlReceiptFromLedger=document.getElementById("ddlReceiptFromLedger");
//    ddlReceiptFromLedger.options.length=0;
//    var opt=document.createElement("OPTION");
//    opt.text="Select";
//    opt.value="0";
//    ddlReceiptFromLedger.options.add(opt);
//    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//    {
//        opt=document.createElement("OPTION");
//        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
//        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
//        ddlReceiptFromLedger.options.add(opt);
//    }
    var ddlReceiptTo=document.getElementById("ddlReceiptTo");
    ddlReceiptTo.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddlReceiptTo.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlReceiptTo.options.add(opt);
    }
    var ddlReceiptType=document.getElementById("ddlReceiptType");
    ddlReceiptType.options.length=0;
    opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddlReceiptType.options.add(opt);    
}

function GetJournalLedger()
{    
    if(journalcnt==0)
    {
        journalcnt++;
        //PageMethods.GetJournalLedger(ddlJournalBranch.value,GetJournalLedgerComplete);
    }
}

function GetJournalLedgerComplete(res)
{
    getXml(res);
    var ddlJournalLedger=document.getElementById("ddlJournalLedger");
    ddlJournalLedger.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";
    ddlJournalLedger.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlJournalLedger.options.add(opt);
    }
}

function btnCESave_Click(e,t)
{
    try
    {
        t.disabled=true;
        if( ddlCEBranch.value=="0")
        {
            ddlCEBranch.focus();
            alert("Enter Branch");
            return false;
        }
        if(!isDate(dtCEDate.value,dtFormat))
        {            
            dtCEDate.focus();
            alert("Enter Valid Date.");
            return false;
        }
        var ledger=document.getElementById("ddlCELedgerName");
        var income=document.getElementById("txtCEIncome");
        var expenses=document.getElementById("txtCEExpences");
        var narration=document.getElementById("txtCENarration");
        
//        if(ledger.value=="0")
//        {
//            ledger.focus();
//            alert("Select Ledger");
//            return false;
//        }
        var tdCELedgerName=document.getElementById("tdCELedgerName");
        if(selectedCELedger=="" || selectedCELedger ==0) 
        {
            tdCELedgerName.focus();
            alert("Select Ledger");
            return false;
        }
        if(income.text=="" && expenses.text=="")
        {
            income.focus();
            alert("Enter Income or Expenses.");
            return false;
        }
                
        PageMethods.InsertCashEntry(dtCEDate.value,selectedCELedger,income.value,expenses.value,narration.value,UserID.value,ddlCEBranch.value, CESaveComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
}

function CESaveComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting CashEntry.");        
    }
    else
    {
        dbeDate.value=dtCEDate.value;
        GetDayBook();             
        ClearCashEntry();
    }
   document.getElementById("btnCESave").disabled=false;
}
function btnCEUpdate_Click(e,t)
{
    try
    {
        //t.disabled=true;
//        if( ddlCEBranch.value=="0")
//        {
//            ddlCEBranch.focus();
//            alert("Enter Branch");
//            return false;
//        }
        if(!isDate(dtCEDate.value,dtFormat))
        {            
            dtCEDate.focus();
            alert("Enter Valid Date.");
            return false;
        }        
        var ledger=document.getElementById("ddlCELedgerName");
        var income=document.getElementById("txtCEIncome");
        var expenses=document.getElementById("txtCEExpences");
        var narration=document.getElementById("txtCENarration");
        var divJournalNo=document.getElementById("divJournalNo");
        var tdCELedgerName=document.getElementById("tdCELedgerName");
//        if(ledger.value=="0")
//        {
//            ledger.focus();
//            alert("Select Ledger");
//            return false;
//        }
        if(selectedCELedger=="" || selectedCELedger ==0) 
        {
            tdCELedgerName.focus();
            alert("Select Ledger");
            return false;
        }
        if(income.text=="" && expenses.text=="")
        {
            income.focus();
            alert("Enter Income or Expenses.");
            return false;
        }
                
        PageMethods.UpdateCashEntry(dtCEDate.value,selectedCELedger,income.value,expenses.value,narration.value,divJournalNo.innerText,UserID.value,ddlCEBranch.value, CEUpdateComplete);
    }
    catch(err)
    {
        //t.disabled=false;
    }
    finally
    {
        
    }
}

function CEUpdateComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting CashEntry.");        
    }
    else
    {
       // alert("Updated Successfully.");
        dbeDate.value=dtCEDate.value;
        GetDayBook();             
         ActivateSave();
    }
}
function btnCEDelete_Click(e,t)
{
    try 
    {
        var divJournalNo=document.getElementById("divJournalNo");
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
            PageMethods.CEDelete(divJournalNo.innerText, ddlCEBranch.value, CEDeleteComplete )            
    }
    catch(err)
    {
    }
}
function CEDeleteComplete(res)
{
    if(res=="0")
    {
        alert("Error While Deleting");        
    }
    else 
    {
        alert("Deleted Successfully");
        GetDayBook();
        ActivateSave();
    }
}

function ClearCashEntry()
{
    CEID=0;
    document.getElementById("divJournalNo").innerHTML="";
    //document.getElementById("ddlCELedgerName").value="0";
    document.getElementById("tdCELedgerName").value="";
    selectedCELedger="";
    document.getElementById("divCELedger").innerHTML="";
    document.getElementById("txtCEIncome").value="";
    document.getElementById("txtCEExpences").value="";
    document.getElementById("txtCENarration").value="";
    //document.getElementById("ddlCELedgerName").focus();
    document.getElementById("tdCELedgerName").focus();
    document.getElementById("txtCEDate").value=new Date().format("dd-MM-yyyy");
    
}

var Flag;
var navbtn;
function FindCEData()
{
    PageMethods.GetCENavData(ddlCEBranch.value,CEID,Flag,GetCENavDataComplete);
}
function btnDBRecordNav_Click(e,t)
{    
    try    
    {
        Flag="F";
        navbtn=t;
        if(t.id=="btnFirst")
            Flag="F";
        else if(t.id=="btnPrev")
        {
             if(CEID==0&&PEID==0&&REID==0&&JEID==0)
                Flag="L";
              else
                Flag="P";
        }
        else if(t.id=="btnNext")
        {
           if(CEID==0&&PEID==0&&REID==0&&JEID==0)
              return;
           else
              Flag="N";
        }
        else if(t.id=="btnLast")
            Flag="L";              
        navbtn.disabled=true;
        if(tab=="CE")
        {
            PageMethods.GetCENavData(ddlCEBranch.value,CEID,Flag,GetCENavDataComplete);
        }
        else if(tab=="PE")
        {
            PageMethods.GetPENavData(ddlPaymentBranch.value,PEID,Flag,GetPENavDataComplete);
        }
        else if(tab=="RE")
        {
            PageMethods.GetRENavData(ddlReceiptBranch.value,REID,Flag,GetRENavDataComplete);
        }
        else if(tab=="JE")
        {
            PageMethods.GetJENavData(ddlJournalBranch.value,JEID,Flag,GetJENavDataComplete);
        }
    }
    catch(err)
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
    
}

function GetCENavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
        else
        {
            ActivateUpdate();
            var date=document.getElementById("txtCEDate");
            var ledger=document.getElementById("ddlCELedgerName");
            var income=document.getElementById("txtCEIncome");
            var expenses=document.getElementById("txtCEExpences");
            var narration=document.getElementById("txtCENarration");
            var divJournalNo=document.getElementById("divJournalNo");            
            var tdCELedgerName=document.getElementById("tdCELedgerName");
            CEID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefNo")[0].firstChild.nodeValue,10);            
            divJournalNo.innerHTML=CEID;
            date.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Date")[0].firstChild.nodeValue;
            //ledger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            selectedCELedger=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            tdCELedgerName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CELedgerName")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CrAmt")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CrAmt")[0].firstChild!=null)
                    income.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("CrAmt")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("DrAmt")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("DrAmt")[0].firstChild!=null)
                    expenses.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("DrAmt")[0].firstChild.nodeValue;
            if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild!=null)
                    narration.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild.nodeValue;
            tdCELedgerName.focus();
         }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
            
    }
}

function GetPENavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
        else
        {
            ActivateUpdate();
            var divPaymentNo=document.getElementById("divPaymentNo");
            var PEdate=document.getElementById("txtPaymentDate");
            var PEname=document.getElementById("txtPaymentName");
            var PEddltoledger=document.getElementById("ddlPaymentToLedger");
            var PEddlfrom=document.getElementById("ddlPaymentFrom");
            var ddlPaymentType=document.getElementById("ddlPaymentType");
            var PEtypedesc=document.getElementById("txtPaymentTypeDesc");
            var PEtowards=document.getElementById("txtPaymentTowards");
            var PEamount=document.getElementById("txtPaymentAmount");
            var tdPELedgerName=document.getElementById("tdPaymentToLedger");
           PEID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentNo")[0].firstChild.nodeValue,10);
           divPaymentNo.innerHTML=PEID;
           PEdate.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentDate")[0].firstChild.nodeValue;
           PEname.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild.nodeValue;
           //PEddltoledger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ToLedgerID")[0].firstChild.nodeValue;           
           PEddlfrom.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("FromLedgerID")[0].firstChild.nodeValue;           
           selectedPELedger=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ToLedgerID")[0].firstChild.nodeValue;
           tdPELedgerName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PELedgerName")[0].firstChild.nodeValue;
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0]!=null)
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild!=null)
                 PEamount.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild.nodeValue;
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentType")[0]!=null)
              if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentType")[0].firstChild!=null)
              {
                    ddlPaymentType.options.length=0;
                    var PayType=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentType")[0].firstChild.nodeValue;
                    opt=document.createElement("OPTION");
                    opt.text=PayType;
                    opt.value=PayType;
                    ddlPaymentType.options.add(opt);
              }
              if(ddlPaymentType.value=="Cash" || ddlPaymentType.value=="0")
                 document.getElementById("txtPaymentTypeDesc").style.display="none";
              else
              {
                 document.getElementById("txtPaymentTypeDesc").style.display="block";
                 PEtypedesc.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PaymentTypeDesc")[0].firstChild.nodeValue;
              }
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0]!=null)
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild!=null)
           PEtowards.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild.nodeValue;
           tdPELedgerName.focus();
        }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
 }

function GetRENavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
        else
        {
            ActivateUpdate();
            var divReceiptNo=document.getElementById("divReceiptId");
            var REDate=document.getElementById("txtReceiptDate");
            var REName=document.getElementById("txtReceiptName");
            var REFromLedger=document.getElementById("ddlReceiptFromLedger");
            var REddlTo=document.getElementById("ddlReceiptTo");
            var ddlReceiptType=document.getElementById("ddlReceiptType");
                ddlReceiptType.options.length=0;
            var opt=document.createElement("OPTION");
            var REtypeDesc=document.getElementById("txtReceiptTypeDesc");
            var REtowards=document.getElementById("txtReceiptTowards");
            var REamount=document.getElementById("txtReceiptAmount");
            
           REID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptNo")[0].firstChild.nodeValue,10);
           divReceiptNo.innerHTML=REID;
           REDate.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptDate")[0].firstChild.nodeValue;
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0]!=null)
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild!=null)
                 REName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RefName")[0].firstChild.nodeValue;                 
           //REFromLedger.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("FromLedgerID")[0].firstChild.nodeValue;
           var tdRELedgerName=document.getElementById("tdReceiptToLedger");
           selectedRELedger=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("FromLedgerID")[0].firstChild.nodeValue;
           tdRELedgerName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("RELedgerName")[0].firstChild.nodeValue;
           REddlTo.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ToLedgerID")[0].firstChild.nodeValue;
           opt=document.createElement("OPTION");
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptType")[0]!=null)
           {
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptType")[0].firstChild!=null)
             {
                 
                    var ReceiptType=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptType")[0].firstChild.nodeValue;
                    opt=document.createElement("OPTION");
                    opt.text=ReceiptType;
                    opt.value=ReceiptType;
                    ddlReceiptType.options.add(opt);
             }
           }
           if(ddlReceiptType.value=="Cash" || ddlReceiptType.value=="0")
               document.getElementById("txtReceiptTypeDesc").style.display="none";
           else
           {
               document.getElementById("txtReceiptTypeDesc").style.display="block";
               REtypeDesc.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ReceiptTypeDesc")[0].firstChild.nodeValue;
           }
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0]!=null)
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild!=null)
                REtowards.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Narration")[0].firstChild.nodeValue;
           if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0]!=null)
             if(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild!=null)
                REamount.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Amount")[0].firstChild.nodeValue;
        
          tdRELedgerName.focus();  
        }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
 }

function GetJENavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
        else
        {
            ActivateUpdate();
            InitilizeJEControls();
            var tblJournal = document.getElementById("tblJournal");
            var dtJEDate=document.getElementById("txtJournalDate");
            for(var i=tblJournal.rows.length-1;i>0;i--)
                tblJournal.deleteRow(i);
            var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<xmlObjLength;i++)
            {
                
                if(tblJournal.rows.length==1)
                    var tr=tblJournal.insertRow(tblJournal.rows.length);
                else
                    var tr=tblJournal.insertRow(tblJournal.rows.length-1);
                tr.insertCell(0);
                tr.insertCell(0);
                tr.insertCell(0);
                tr.insertCell(0);
                tr.insertCell(0);
                tr.insertCell(0);
                
                JEID=parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("RefNo")[0].firstChild.nodeValue,10);
                divJENo.innerHTML=JEID;
                dtJEDate.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Date")[0].firstChild.nodeValue;
                var JID=parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue,10);
                tr.cells[0].innerHTML="<input type='hidden' value='"+JID+"'/>";
                tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                var Narration=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Narration")[0].firstChild.nodeValue;
                tr.cells[2].innerHTML="<div style='width:340px; overflow:hidden;' title='"+Narration+"' />"+Narration+"</div>";
                var credit=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CrAmt")[0].firstChild.nodeValue;
                tr.cells[3].innerHTML=credit;
                var debit=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("DrAmt")[0].firstChild.nodeValue;
                tr.cells[4].innerHTML=debit;
                tr.cells[5].innerHTML="<a href='#' onclick='JEdit_Click(event,this);'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='JDelete_Click(event,this);'>Delete</a>";
                tr.cells[5].noWrap=true;
                tr.cells[0].style.display="none";
            }
                trtot=tblJournal.insertRow(tblJournal.rows.length);
                trtot.insertCell(0);
                trtot.insertCell(0);
                trtot.insertCell(0);
                trtot.insertCell(0);
                trtot.insertCell(0);
                trtot.insertCell(0);
                trtot.cells[1].innerHTML="<b>Total</bt>";
                trtot.cells[1].style.textAlign="center";
            
            var tblJournal = document.getElementById("tblJournal");
            
            if(tblJournal.rows.length==2)
            {
                tblJournal.deleteRow(1);
                return;
            }
            totCredit=0;
            totDebit=0;
            for(var i=1;i<tblJournal.rows.length-1;i++)
            {
                if(!isNaN(tblJournal.rows[i].cells[3].innerText)&&tblJournal.rows[i].cells[3].innerText!="")
                    totCredit+=parseFloat(tblJournal.rows[i].cells[3].innerText);
                if(!isNaN(tblJournal.rows[i].cells[4].innerText)&&tblJournal.rows[i].cells[4].innerText!="")
                    totDebit+=parseFloat(tblJournal.rows[i].cells[4].innerText);
            }
            tblJournal.rows[tblJournal.rows.length-1].cells[2].innerHTML="<b>"+totCredit.toFixed(2)+"</b>";
            tblJournal.rows[tblJournal.rows.length-1].cells[3].innerHTML="<b>"+totDebit.toFixed(2)+"</b>";
            
         }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
 }
       
function ActivateUpdate()
{
  if(tab=="CE")
  {
     document.getElementById("btnCESave").style.display="none";
     document.getElementById("btnCEUpdate").style.display="block";
     document.getElementById("btnCEDelete").style.display="block";
     document.getElementById("btnCECancel").style.display="block";
  }
  if(tab=="PE")
  {
     document.getElementById("btnPaymentSave").style.display="none";
     document.getElementById("btnPaymentUpdate").style.display="block";
     document.getElementById("btnPaymentDelete").style.display="block";
     document.getElementById("btnPaymentCancel").style.display="block";
  }
  if(tab=="RE")
  {
     document.getElementById("btnReceiptSave").style.display="none";
     document.getElementById("btnReceiptUpdate").style.display="block";
     document.getElementById("btnReceiptDelete").style.display="block";
     document.getElementById("btnReceiptCancel").style.display="block";
  }
  if(tab=="JE")
  {
     document.getElementById("btnJournalSave").style.display="none";
     document.getElementById("btnJournalUpdate").style.display="block";
     document.getElementById("btnJournalDelete").style.display="block";
     document.getElementById("btnJournalCancel").style.display="block";
  }
}

function  ActivateSave()
{
  if(tab=="CE")
  {
     document.getElementById("btnCESave").style.display="block";
     document.getElementById("btnCEUpdate").style.display="none";
     document.getElementById("btnCEDelete").style.display="none";
     document.getElementById("btnCECancel").style.display="none";
     ClearCashEntry();
  }
  if(tab=="PE")
  {
     document.getElementById("btnPaymentSave").style.display="block";
     document.getElementById("btnPaymentUpdate").style.display="none";
     document.getElementById("btnPaymentDelete").style.display="none";
     document.getElementById("btnPaymentCancel").style.display="none";
      clearPaymentflds();
  }
  if(tab=="RE")
  {
     document.getElementById("btnReceiptSave").style.display="block";
     document.getElementById("btnReceiptUpdate").style.display="none";
     document.getElementById("btnReceiptDelete").style.display="none";
     document.getElementById("btnReceiptCancel").style.display="none";
     clearReceiptflds();
  }
  if(tab=="JE")
  {
     document.getElementById("btnJournalSave").style.display="block";
     document.getElementById("btnJournalUpdate").style.display="none";
     document.getElementById("btnJournalDelete").style.display="none";
     document.getElementById("btnJournalCancel").style.display="none";
     
    document.getElementById("divJourNo").innerHTML="";
    document.getElementById("ddlJournalLedger").value="0";
    document.getElementById("txtJournalCredit").value="";
    document.getElementById("txtJournalDebit").value="";
    document.getElementById("txtJournalNarration").value="";
    document.getElementById("txtJournalDate").value=new Date().format("dd-MM-yyyy");
    
    var tblJournal=document.getElementById("tblJournal");
    for(var i=tblJournal.rows.length-1;i>0;i--)
        tblJournal.deleteRow(i);
        
  }
}
function btnPaymentSave_Click(e,t)
{
    try
    {
        t.disabled=true;        
        if( ddlPaymentBranch.value=="0")
        {
            ddlPaymentBranch.focus();
            alert("Enter Branch");
            return false;
        }
        if(!isDate(dtPDate.value,dtFormat))
        {            
            dtPDate.focus();
            t.disabled=false;
            alert("Enter Valid Date.");
            return false;
        }
        var RefName=document.getElementById("txtPaymentName");
        var ToLedgerID=document.getElementById("ddlPaymentToLedger");
        var FromLedgerID=document.getElementById("ddlPaymentFrom");
        var Amount=document.getElementById("txtPaymentAmount");
        var PaymentType=document.getElementById("ddlPaymentType");
        var PaymentTypeDesc=document.getElementById("txtPaymentTypeDesc");
        var Narration=document.getElementById("txtPaymentTowards");
        var tdPaymentToLedger=document.getElementById("tdPaymentToLedger");
//        if(ToLedgerID.value=="0")
//        {
//            ToLedgerID.focus();
//            t.disabled=false;
//            alert("Select ToLedger");
//            return false;
//        }
        if(selectedPELedger=="0" || selectedPELedger=="" )
        {
            tdPaymentToLedger.focus();
            t.disabled=false;
            alert("Select ToLedger");
            return false;
        }
         if(FromLedgerID.value=="0")
        {
            FromLedgerID.focus();
            t.disabled=false;
            alert("Select FromLedger");
            return false;
        }
        if(Amount.text=="")
        {
            Amount.focus();
            t.disabled=false;
            alert("Enter Amount.");
            return false;
        }        
        
        PageMethods.InsertPayment(dtPDate.value,RefName.value,selectedPELedger,FromLedgerID.value,Amount.value,PaymentType.value,PaymentTypeDesc.value,Narration.value,UserID.value,ddlPaymentBranch.value,InserPaymentComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
    
}

function InserPaymentComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting Payment.");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }
    else
    {
        dbeDate.value=dtPDate.value;
        GetDayBook();             
        clearPaymentflds();
    }
    document.getElementById("btnPaymentSave").disabled=false;
}


function btnPaymentUpdate_Click(e,t)
{
    try
    {
        t.disabled=true;        
        if( ddlPaymentBranch.value=="0")
        {
            ddlPaymentBranch.focus();
            alert("Enter Branch");
            return false;
        }
        if(!isDate(dtPDate.value,dtFormat))
        {            
            dtPDate.focus();
            t.disabled=false;
            alert("Enter Valid Date.");
            return false;
        }
        var RefName=document.getElementById("txtPaymentName");
        var ToLedgerID=document.getElementById("ddlPaymentToLedger");
        var FromLedgerID=document.getElementById("ddlPaymentFrom");
        var Amount=document.getElementById("txtPaymentAmount");
        var PaymentType=document.getElementById("ddlPaymentType");
        var PaymentTypeDesc=document.getElementById("txtPaymentTypeDesc");
        var Narration=document.getElementById("txtPaymentTowards");
        var divPaymentNo=document.getElementById("divPaymentNo");
        var tdPaymentToLedger=document.getElementById("tdPaymentToLedger");
//        if(ToLedgerID.value=="0")
//        {
//            ToLedgerID.focus();
//            t.disabled=false;
//            alert("Select ToLedger");
//            return false;
//        }
        if(selectedPELedger=="0" || selectedPELedger=="" )
        {
            tdPaymentToLedger.focus();
            t.disabled=false;
            alert("Select ToLedger");
            return false;
        }
         if(FromLedgerID.value=="0")
        {
            FromLedgerID.focus();
            t.disabled=false;
            alert("Select FromLedger");
            return false;
        }
        if(Amount.text=="")
        {
            Amount.focus();
            t.disabled=false;
            alert("Enter Amount.");
            return false;
        }        
        
        PageMethods.UpdatePayment(divPaymentNo.innerHTML,dtPDate.value,RefName.value,selectedPELedger,FromLedgerID.value,Amount.value,PaymentType.value,PaymentTypeDesc.value,Narration.value,UserID.value,ddlPaymentBranch.value, UpdatePaymentComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
    
}

function UpdatePaymentComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting Payment.");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }
    else
    {
        dbeDate.value=dtPDate.value;
        GetDayBook();             
        clearPaymentflds();
        ActivateSave();
    }
    document.getElementById("btnPaymentUpdate").disabled=false;
}

function clearPaymentflds()
{
    document.getElementById("divPaymentNo").innerHTML="";
    document.getElementById("txtPaymentName").value="";
    document.getElementById("ddlPaymentToLedger").value="0";
    document.getElementById("ddlPaymentFrom").value="0";
    ddlPaymentFrom_change(window.event,document.getElementById("ddlPaymentFrom"));    
    document.getElementById("txtPaymentTypeDesc").value="";
    document.getElementById("txtPaymentTowards").value="";
    document.getElementById("txtPaymentAmount").value="";
    document.getElementById("txtPaymentDate").value=new Date().format("dd-MM-yyyy");
    document.getElementById("divPELedger").innerHTML="";
    PEID=0;
    selectedPELedger="";
    document.getElementById("tdPaymentToLedger").value="";
}

function btnReceiptSave_Click(e,t)
{
    try
    {
        t.disabled=true;        
        if(ddlReceiptBranch.value=="0")
        {
            ddlReceiptBranch.focus();
            alert("Enter Branch");
            return false;
        }
        if(!isDate(dtRDate.value,dtFormat))
        {            
            dtRDate.focus();
            t.disabled=false;
            alert("Enter Valid Date.");
            return false;
        }
        var RefName=document.getElementById("txtReceiptName");
        var ToLedgerID=document.getElementById("ddlReceiptTo");
        var FromLedgerID=document.getElementById("ddlReceiptFromLedger");
        var Amount=document.getElementById("txtReceiptAmount");
        var ReceiptType=document.getElementById("ddlReceiptType");
        var ReceiptTypeDesc=document.getElementById("txtReceiptTypeDesc");
        var Narration=document.getElementById("txtReceiptTowards");
        var tdReceiptToLedger=document.getElementById("tdReceiptToLedger");
        if(ToLedgerID.value=="0")
        {
            ToLedgerID.focus();
            t.disabled=false;
            alert("Select ToLedger");
            return false;
        }
        if(selectedRELedger=="" || selectedRELedger==0)
        {
            tdReceiptToLedger.focus();            
            alert("Select ToLedger");
            return false;
        }
//         if(FromLedgerID.value=="0")
//        {
//            FromLedgerID.focus();
//            t.disabled=false;
//            alert("Select FromLedger");
//            return false;
//        }
        if(Amount.text=="")
        {
            Amount.focus();
            t.disabled=false;
            alert("Enter Amount.");
            return false;
        }        
        
        PageMethods.InsertReceipt(dtRDate.value,RefName.value, ToLedgerID.value, selectedRELedger,Amount.value,ReceiptType.value,ReceiptTypeDesc.value,Narration.value,UserID.value,ddlReceiptBranch.value,InserReceiptComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
}

function InserReceiptComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting Receipt.");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }
    else
    {
        dbeDate.value=dtRDate.value;
        GetDayBook();             
        clearReceiptflds();
    }
    document.getElementById("btnReceiptSave").disabled=false;
}


function btnReceiptUpdate_Click(e,t)
{
    try
    {
        //t.disabled=true;        
       if(ddlReceiptBranch.value=="0")
        {
            ddlReceiptBranch.focus();
            alert("Enter Branch");
            return false;
        }
        if(!isDate(dtRDate.value,dtFormat))
        {            
            dtRDate.focus();
            //t.disabled=false;
            alert("Enter Valid Date.");
            return false;
        }
        var divReceiptId=document.getElementById("divReceiptId");
        var RefName=document.getElementById("txtReceiptName");
        var ToLedgerID=document.getElementById("ddlReceiptTo");
        var FromLedgerID=document.getElementById("ddlReceiptFromLedger");
        var Amount=document.getElementById("txtReceiptAmount");
        var ReceiptType=document.getElementById("ddlReceiptType");
        var ReceiptTypeDesc=document.getElementById("txtReceiptTypeDesc");
        var Narration=document.getElementById("txtReceiptTowards");
        
        if(ToLedgerID.value=="0")
        {
            ToLedgerID.focus();
            t.disabled=false;
            alert("Select ToLedger");
            return false;
        }
        if(selectedRELedger=="" || selectedRELedger==0)
        {
            tdReceiptToLedger.focus();            
            alert("Select ToLedger");
            return false;
        }
//         if(FromLedgerID.value=="0")
//        {
//            FromLedgerID.focus();
//            t.disabled=false;
//            alert("Select FromLedger");
//            return false;
//        }
        if(Amount.text=="")
        {
            Amount.focus();
            t.disabled=false;
            alert("Enter Amount.");
            return false;
        }        
        
        PageMethods.UpdateReceipt(divReceiptId.innerHTML,dtRDate.value,RefName.value,ToLedgerID.value, selectedRELedger,Amount.value,ReceiptType.value,ReceiptTypeDesc.value,Narration.value,UserID.value,ddlReceiptBranch.value,UpdateReceiptComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
}

function UpdateReceiptComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting Receipt.");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }
    else
    {
        dbeDate.value=dtRDate.value;
        GetDayBook();             
        clearReceiptflds();
        ActivateSave();
    }
    document.getElementById("btnReceiptSave").disabled=false;
}

function clearReceiptflds()
{

    document.getElementById("divReceiptId").innerHTML="";
    document.getElementById("txtReceiptName").value="";
    document.getElementById("ddlReceiptFromLedger").value="0";
    document.getElementById("ddlReceiptTo").value="0";
    ddlReceiptTo_change(window.event,document.getElementById("ddlReceiptTo"));    
    document.getElementById("txtReceiptTypeDesc").value="";
    document.getElementById("txtReceiptTowards").value="";
    document.getElementById("txtReceiptAmount").value="";
    document.getElementById("txtReceiptDate").value=new Date().format("dd-MM-yyyy");
    REID=0;
    document.getElementById("divRELedger").innerHTML="";    
    selectedRELedger="";
    document.getElementById("tdReceiptToLedger").value="";
}

function btnJournalSave_Click(e,t)
{
    try
    {
        t.disabled=true;        
       if(ddlJournalBranch.value=="0")
        {
            ddlJournalBranch.focus();
            alert("Enter Branch");
            return false;
        }
        if(!isDate(dtJDate.value,dtFormat))
        {            
            dtJDate.focus();
            t.disabled=false;
            alert("Enter Valid Date.");
            return false;
        }
        GetAmt();
        if(totCredit!=totDebit)
        {
            alert("Tot Credit Not Matched With Tot Debti.");
            t.disabled=false;
            if(ddlJLedger!=null)
                ddlJLedger.focus();
            return false;
            
        }
        var tblJournal=document.getElementById("tblJournal");
        if(tblJournal.rows.length<3)
        {
            alert("Add Journal Details.");
            t.disabled=false;
            if(ddlJLedger!=null)
                ddlJLedger.focus();
            return false;
        }
        var xmlDet="<XML>";
        
        for(var i=1;i<tblJournal.rows.length-1;i++)
        {
            xmlDet=xmlDet+"<JournalDet>";
            var tr=tblJournal.rows[i];            
            xmlDet=xmlDet+"<LedgerID>"+tr.cells[0].firstChild.value+"</LedgerID>";
            if(tr.cells[3].innerText!="")
                xmlDet=xmlDet+"<CrAmt>"+tr.cells[3].innerText+"</CrAmt>";
            if(tr.cells[4].innerText!="")
                xmlDet=xmlDet+"<DrAmt>"+tr.cells[4].innerText+"</DrAmt>";
            xmlDet=xmlDet+"<Narration>"+tr.cells[2].innerText+"</Narration>";
            xmlDet=xmlDet+"</JournalDet>";
        }
        xmlDet=xmlDet+"</XML>";
        PageMethods.InsertJournal(xmlDet,dtJDate.value,UserID.value,ddlJournalBranch.value,InsertJournalComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
}

function InsertJournalComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting Receipt.");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }
    else
    {
        dbeDate.value=dtJDate.value;
        GetDayBook();             
        ClearJouryFields();
    }
    document.getElementById("btnJournalSave").disabled=false;
}

function btnJournalUpdate_Click(e,t)
{
    try
    {
       // t.disabled=true; 
        if(ddlJournalBranch.value=="0")
        {
            ddlJournalBranch.focus();
            alert("Enter Branch");
            return false;
        }       
        divJNo=document.getElementById("divJourNo");
        if(!isDate(dtJDate.value,dtFormat))
        {            
            dtJDate.focus();
            //t.disabled=false;
            alert("Enter Valid Date.");
            return false;
        }
        GetAmt();
        if(totCredit!=totDebit)
        {
            alert("Tot Credit Not Matched With Tot Debti.");
            t.disabled=false;
            if(ddlJLedger!=null)
                ddlJLedger.focus();
            return false;
            
        }
        var tblJournal=document.getElementById("tblJournal");
        if(tblJournal.rows.length<3)
        {
            alert("Add Journal Details.");
            t.disabled=false;
            if(ddlJLedger!=null)
                ddlJLedger.focus();
            return false;
        }
        var xmlDet="<XML>";
        
        for(var i=1;i<tblJournal.rows.length-1;i++)
        {
            xmlDet=xmlDet+"<JournalDet>";
            var tr=tblJournal.rows[i];            
            xmlDet=xmlDet+"<LedgerID>"+tr.cells[0].firstChild.value+"</LedgerID>";
            if(tr.cells[3].innerText!="")
                xmlDet=xmlDet+"<CrAmt>"+tr.cells[3].innerText+"</CrAmt>";
            if(tr.cells[4].innerText!="")
                xmlDet=xmlDet+"<DrAmt>"+tr.cells[4].innerText+"</DrAmt>";
            xmlDet=xmlDet+"<Narration>"+tr.cells[2].innerText+"</Narration>";
            xmlDet=xmlDet+"</JournalDet>";
        }
        xmlDet=xmlDet+"</XML>";
        PageMethods.UpdateJournal(divJNo.innerHTML,xmlDet,dtJDate.value,UserID.value,ddlJournalBranch.value,UpdateJournalComplete);
    }
    catch(err)
    {
        t.disabled=false;
    }
    finally
    {
        
    }
}

function UpdateJournalComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while inserting Receipt.");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }
    else
    {
        dbeDate.value=dtJDate.value;
        GetDayBook();             
        ClearJouryFields();
        ActivateSave();
    }
    //document.getElementById("btnJournalUpdate_Click").disabled=false;
   //ActivateSave();
}




function ClearJouryFields()
{
    JEID=0;
    ClearJournEntryFields();
    var tblJournal=document.getElementById("tblJournal");
    for(var i=tblJournal.rows.length-1;i>0;i--)
        tblJournal.deleteRow(i);
}

function txtDBEDate_Change()
{
    dbeDate=document.getElementById("txtDBEDate");
    dtCEDate=document.getElementById("txtCEDate");
    dtPDate=document.getElementById("txtPaymentDate");
    dtRDate=document.getElementById("txtReceiptDate");
    dtJDate=document.getElementById("txtJournalDate");
    
    if(!isDate(dbeDate.value,dtFormat))
    {
        alert("Enter Valid Date.");
        return;
    }
    
    dtCEDate.value=dbeDate.value;
    dtPDate.value=dbeDate.value;
    dtRDate.value=dbeDate.value;
    dtJDate.value=dbeDate.value;
    GetDayBook();
}

  function NumberCheck(e,t)
  {
	 var my_string=e.value;
     if(isNaN(my_string))
     {
        e.value="";
        alert("Enter Valid Income ");
        e.focus();
      }
  }

function txtCEDate_Change()
{
      dtCEDate=document.getElementById("txtCEDate");
  if(!isDate(dtCEDate.value,dtFormat))
  {
    dtCEDate.value="";
    premsgbox("Enter Valid Date.","txtCEDate");
    //alert("Enter Valid Date.");
    dtCEDate.focus();
    return false;
  }
}

function txtPEDate_Change()
{
  dtPDate=document.getElementById("txtPaymentDate");
  if(!isDate(dtPDate.value,dtFormat))
  {
    dtPDate.value="";
    alert("Enter Valid Date.");
    dtPDate.focus();
    return;
  }
}

function txtREDate_Change()
{
  if(!isDate(dtRDate.value,dtFormat))
  {
    dtRDate.value="";
    alert("Enter Valid Date.");
    dtRDate.focus();
    return;
  }
}

function txtJEDate_Change()
{
  dtJDate=document.getElementById("txtJournalDate");
  if(!isDate(dtJDate.value,dtFormat))
  {
    dtJDate.value="";
    alert("Enter Valid Date.");
    dtJDate.focus();
    return;
  }
}

function ddlPaymentFrom_change(e,t)
{
    var ddlPaymentType=document.getElementById("ddlPaymentType");  
    ddlPaymentType.options.length=0;  
    if(t.value=="0")
    {        
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";
        ddlPaymentType.options.add(opt);
    }
    else if(t.value=="1")
    {
        opt=document.createElement("OPTION");
        opt.text="Cash";
        opt.value="Cash";
        ddlPaymentType.options.add(opt);
    }
    else
    {
        opt=document.createElement("OPTION");
        opt.text="Cheque";
        opt.value="Cheque";
        ddlPaymentType.options.add(opt);
        opt=document.createElement("OPTION");
        opt.text="DD";
        opt.value="DD";
        ddlPaymentType.options.add(opt);
        opt=document.createElement("OPTION");
        opt.text="Online";
        opt.value="Online";
        ddlPaymentType.options.add(opt);
    }
    ddlPaymentType_Change(e,ddlPaymentType);
}

function ddlReceiptTo_change(e,t)
{
    var ddlReceiptType=document.getElementById("ddlReceiptType");  
    ddlReceiptType.options.length=0;  
    if(t.value=="0")
    {        
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";
        ddlReceiptType.options.add(opt);
    }
    else if(t.value=="1")
    {
        opt=document.createElement("OPTION");
        opt.text="Cash";
        opt.value="Cash";
        ddlReceiptType.options.add(opt);
    }
    else
    {
        opt=document.createElement("OPTION");
        opt.text="Cheque";
        opt.value="Cheque";
        ddlReceiptType.options.add(opt);
        opt=document.createElement("OPTION");
        opt.text="DD";
        opt.value="DD";
        ddlReceiptType.options.add(opt);
        opt=document.createElement("OPTION");
        opt.text="Online";
        opt.value="Online";
        ddlReceiptType.options.add(opt);
    }
    ddlReceiptType_Change(e,ddlReceiptType);
}

function ddlPaymentType_Change(e,t)
{
    if(t.value=="Cash" || t.value=="0")
        document.getElementById("txtPaymentTypeDesc").style.display="none";
    else
        document.getElementById("txtPaymentTypeDesc").style.display="block";
}

function ddlReceiptType_Change(e,t)
{
    if(t.value=="Cash" || t.value=="0")
        document.getElementById("txtReceiptTypeDesc").style.display="none";
    else
        document.getElementById("txtReceiptTypeDesc").style.display="block";
}

var ddlJLedger;
var credit;
var debit;
var narration;


function btnJournalAdd_Click(e,t)
{
    InitilizeJEControls();
//    if(ddlJLedger.value=="0")
//    {
//        ddlJLedger.focus();
//        alert("Select Ledger.");
//        return false;
//    }    
    //var tdJournalLedger=document.getElementById("tdJournalLedger");
    if(selectedJELedger=="" || selectedJELedger==0)
    {
        tdJournalLedger.focus();
        alert("Select Ledger.");
        return false;
    } 
    if(credit.value==""&&debit.value=="")
    {
        credit.focus();
        alert("Enter Credit or Debit Amount");
        return false;
    }
    if(credit.value!=""&&debit.value!="")
    {
        credit.focus();
        alert("Enter Only Credit or Debit Amount");
        return false;
    }
    if(credit.value!="")
    {
        if(isNaN(credit.value))
        {
            alert("Invalid Credit Amount");
            credit.focus();
            return false;
        }
        if(credit.value!="")
        {
            credit.value=parseFloat(credit.value).toFixed(2);
        }
        
    }
    if(debit.value!="")
    {
        if(isNaN(debit.value))
        {
            alert("Invalid Debit Amount");
            debit.focus();
            return false;
        }
        if(debit.value!="")
        {
            debit.value=parseFloat(debit.value).toFixed(2);
        }
    }
    AddJournalRow();
}
var trtot;
function AddJournalRow()
{
    var tblJournal = document.getElementById("tblJournal");
    for(var i=1;i<tblJournal.rows.length-1;i++)
    {
        if(tblJournal.rows[i].cells[0].firstChild.value==ddlJLedger.value)
        {
            alert("Ledger Already exist, Click Edit to Change");
            return false;
        }
    }
    if(tblJournal.rows.length==1)
        var tr=tblJournal.insertRow(tblJournal.rows.length);
    else
        var tr=tblJournal.insertRow(tblJournal.rows.length-1);
    tr.insertCell(0);
    tr.insertCell(0);
    tr.insertCell(0);
    tr.insertCell(0);
    tr.insertCell(0);
    tr.insertCell(0);
    tr.cells[0].innerHTML="<input type='hidden' value='"+selectedJELedger+"'/>";
    tr.cells[1].innerText=tdJournalLedger.value;
    tr.cells[2].innerHTML="<div style='width:340px; overflow:hidden;' title='"+narration.value+"' />"+narration.value+"</div>";
    tr.cells[3].innerText=credit.value;
    tr.cells[4].innerText=debit.value;
    tr.cells[5].innerHTML="<a href='#' onclick='JEdit_Click(event,this);'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='JDelete_Click(event,this);'>Delete</a>";
    tr.cells[5].noWrap=true;
    tr.cells[0].style.display="none";
    ClearJournEntryFields();
    
    if(tblJournal.rows.length==2)
    {
        trtot=tblJournal.insertRow(tblJournal.rows.length);
        trtot.insertCell(0);
        trtot.insertCell(0);
        trtot.insertCell(0);
        trtot.insertCell(0);
        trtot.insertCell(0);
        trtot.insertCell(0);
        trtot.cells[1].innerHTML="<b>Total</bt>";
        trtot.cells[1].style.textAlign="center";
    }
    GetAmt();
    //trtot.cells[2].innerHTML="<b>"+totCredit.toFixed(2)+"</b>";
    //trtot.cells[3].innerHTML="<b>"+totDebit.toFixed(2)+"</b>";
}

function ClearJournEntryFields()
{
    ddlJLedger.value=0;    
    document.getElementById("tdJournalLedger").value="";
    selectedJELedger="";
    document.getElementById("divJELedger").innerHTML="";
    credit.value="";
    debit.value="";
    narration.value="";
    var tdJournalLedger=document.getElementById("tdJournalLedger");
    tdJournalLedger.focus();
}
var totCredit;
var totDebit;
function GetAmt()
{
    var tblJournal = document.getElementById("tblJournal");
    debit.value="";
    credit.value="";
    if(tblJournal.rows.length==2)
    {
        tblJournal.deleteRow(1);
        return;
    }
    totCredit=0;
    totDebit=0;
    for(var i=1;i<tblJournal.rows.length-1;i++)
    {
        if(!isNaN(tblJournal.rows[i].cells[3].innerText)&&tblJournal.rows[i].cells[3].innerText!="")
            totCredit+=parseFloat(tblJournal.rows[i].cells[3].innerText);
        if(!isNaN(tblJournal.rows[i].cells[4].innerText)&&tblJournal.rows[i].cells[4].innerText!="")
            totDebit+=parseFloat(tblJournal.rows[i].cells[4].innerText);
    }
    if(totCredit>totDebit)
        debit.value=(totCredit-totDebit).toFixed(2);
    else if(totDebit>totCredit)
        credit.value=(totDebit-totCredit).toFixed(2);
    tblJournal.rows[tblJournal.rows.length-1].cells[2].innerHTML="<b>"+totCredit.toFixed(2)+"</b>";
    tblJournal.rows[tblJournal.rows.length-1].cells[3].innerHTML="<b>"+totDebit.toFixed(2)+"</b>";
}

function JDelete_Click(e,t)
{
    if(confirm("Are you sure? want to delete the record."))
    {
        document.getElementById("tblJournal").deleteRow(t.parentNode.parentNode.rowIndex);
        GetAmt();
    }
}

function JEdit_Click(e,t)
{
    if(confirm("Are you sure? want to edit the record."))
    {
        InitilizeJEControls();
        var tr=t.parentNode.parentNode;
        //ddlJLedger.value=tr.cells[0].firstChild.value;
        selectedJELedger==tr.cells[0].firstChild.value;
        tdJournalLedger.value=tr.cells[1].innerText;
        narration.value=tr.cells[2].innerText;
        credit.value=tr.cells[3].innerText;
        debit.value=tr.cells[4].innerText;
        document.getElementById("tblJournal").deleteRow(tr.rowIndex);        
        narration.focus();
        GetAmt();
    }    
    
}


function scrollpage(){
dh=document.body.scrollHeight
ch=document.body.clientHeight
if(dh>ch){
moveme=dh-ch
window.scrollTo(0,moveme)
}
}

function ShowFullScreen()
{
    window.showModalDialog("onlyDayBook.aspx?time="+new Date()+"&DBEDate="+document.getElementById("txtDBEDate").value+"&BranchID="+document.getElementById("ddlBranch").value,"","dialogHeight:650px;dialogWidth:920px;center:yes;");
}

function ShowLedgerMaster(e,t)
{
    if(e.keyCode==13)
        return e.keyCode=9;
    if(e.keyCode==45)
    {
        window.showModalDialog("LedgerMaster.aspx?DBE="+new Date().getTime(),"","dialogHeight:350px;dialogWidth:320px;center:on;scroll:0;resize:0");
        GetCELedger();
        GetPaymentLedgers();
        GetReceiptLedgers();
        GetJournalLedger();              
    }
}

function checkEnter(e)
{
    if(e.keyCode==13)
        even.keyCode=9;
}

function premsgbox(msg, cntrl) 
{
  try {
        document.getElementById(preid + cntrl).focus();
        alert(msg);        
      } 
      catch (error) { }
}


function GetBranch()
{
    ddlBranch=document.getElementById("ddlBranch");       
    ddlCEBranch=document.getElementById("ddlCEBranch");
    ddlPaymentBranch=document.getElementById("ddlPaymentBranch");        
    ddlReceiptBranch= document.getElementById("ddlReceiptBranch");        
    ddlJournalBranch=document.getElementById("ddlJournalBranch");
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetBranch(UserID.value, GetBranchComplete);   
}

function GetBranchComplete(res)
{
    getXml(res);    
    var j;
    for (j=1;j<=5;j++)
    {
        switch(j)
        {
            case 1:
                ddlTempBranch=document.getElementById("ddlBranch");
                break;
            case 2:ddlTempBranch=document.getElementById("ddlCEBranch");
                break;
            case 3:ddlTempBranch=document.getElementById("ddlPaymentBranch");
                break;
            case 4:ddlTempBranch = document.getElementById("ddlReceiptBranch");
                break;
            case 5:ddlTempBranch=document.getElementById("ddlJournalBranch");
                break;
        }
        ddlTempBranch.options.length=0;
        var opt=document.createElement("OPTION");
        if(xmlObj.getElementsByTagName("Table").length != 1)
        {
            opt=document.createElement("OPTION");
            opt.text="Select";
            opt.value="0";    
            ddlTempBranch.options.add(opt);            
        }  
        else 
        {
            
            GetCELedger();
        }
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            ddlTempBranch.options.add(opt);
        }      
        if(hidGBID==null)
            GetDefaultBranch();
        ddlTempBranch.value=hidGBID.value;  
        SelectBranch();        
    }  
    GetCELedger();  
}
function btnPaymentDelete_Click(e,t)
{
    try 
    {
        var divRefNo=document.getElementById("divPaymentNo");
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
            PageMethods.PaymentDelete(divRefNo.innerText, ddlPaymentBranch.value, PaymentDeleteComplete )            
    }
    catch(err)
    {
    }
}
function PaymentDeleteComplete(res)
{
    if(res=="0")
    {
        alert("Error While Deleting");        
    }
    else 
    {
        alert("Deleted Successfully");
        GetDayBook();
        ActivateSave();
    }
}
function btnReceiptDelete_Click(e,t)
{
    try 
    {
        var divRefNo=document.getElementById("divReceiptId");
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
            PageMethods.ReceiptDelete(divRefNo.innerText, ddlReceiptBranch.value, ReceiptDeleteComplete )            
    }
    catch(err)
    {
    }
}
function ReceiptDeleteComplete(res)
{
    if(res=="0")
    {
        alert("Error While Deleting");        
    }
    else 
    {
        alert("Deleted Successfully");
        GetDayBook();
        ActivateSave();
    }
}
function btnJournalDelete_Click(e,t)
{
    try 
    {
        var divRefNo=document.getElementById("divJourNo");
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
            PageMethods.JournalDelete(divRefNo.innerText, ddlJournalBranch.value, JournalDeleteComplete )            
    }
    catch(err)
    {
    }
}
function JournalDeleteComplete(res)
{
    if(res=="0")
    {
        alert("Error While Deleting");        
    }
    else 
    {
        alert("Deleted Successfully");
        GetDayBook();
        ActivateSave();
    }
}

//Cash Entry Ledger
var divCELedger;
var selectedCELedger="";
var prevText="";

function FillCELedger(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedCELedger="";
        divCELedger=document.getElementById("divCELedger");
        if(t.value!="")
        {        
            PageMethods.CallFillCELedger(ddlBranch.value,t.value,FillCELedgerComplete)
        }
        else{
        divCELedger.innerHTML="";
        divCELedger.style.display="none";
        }
    }
}

function FillCELedgerComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var lid;
            var lname;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                    lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                    lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                    lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectCEClick(this)' onmouseover='OnCEMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                selectedCELedger=lid;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectCEClick(this)' onmouseover='OnCEMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divCELedger.innerHTML=tbl;
        if(count>0)
            divCELedger.style.display="block";
        else
            divCELedger.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectCEClick(t)
{
    selectedCELedger=t.cells[0].innerHTML;
    var txt=document.getElementById("tdCELedgerName");
    ApplyCELedger(txt);
}
function OnCEMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedCELedger ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedCELedger=t.cells[0].innerHTML;
}
function selectCELedger(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyCELedger(t);
    }
    else
    {
        if(divCELedger==null)
            divCELedger=document.getElementById("divCELedger");
        var tbl=divCELedger.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedCELedger)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedCELedger=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedCELedger=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyCELedger(t)
{
    if(divCELedger==null)
            divCELedger=document.getElementById("divCELedger");
    if(selectedCELedger!="")
    {   
        var tbl=divCELedger.firstChild;
        var cnt=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedCELedger)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedCELedger="";
                t.value="";
            }
        }
    }
    else
        t.value="";
    prevText=t.value;
    divCELedger.style.display="none";
    
}

//Payment Entry Ledger
var divPELedger;
var selectedPELedger="";
prevText="";

function FillPELedger(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedPELedger="";
        divPELedger=document.getElementById("divPELedger");
        if(t.value!="")
        {        
            PageMethods.CallFillPayAndRecLedger(ddlBranch.value,t.value,FillPELedgerComplete)
        }
        else{
        divPELedger.innerHTML="";
        divPELedger.style.display="none";
        }
    }
}

function FillPELedgerComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var lid;
            var lname;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                    lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                    lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                    lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectPEClick(this)' onmouseover='OnPEMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                selectedPELedger=lid;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectPEClick(this)' onmouseover='OnPEMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divPELedger.innerHTML=tbl;
        if(count>0)
            divPELedger.style.display="block";
        else
            divPELedger.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectPEClick(t)
{
    selectedPELedger=t.cells[0].innerHTML;
    var txt=document.getElementById("tdPaymentToLedger");
    ApplyPELedger(txt);
}
function OnPEMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedPELedger ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedPELedger=t.cells[0].innerHTML;
}
function selectPELedger(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyPELedger(t);
    }
    else
    {
        if(divPELedger==null)
            divPELedger=document.getElementById("divPELedger");
        var tbl=divPELedger.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedPELedger)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedPELedger=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedPELedger=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyPELedger(t)
{
    if(divPELedger==null)
            divPELedger=document.getElementById("divPELedger");
    if(selectedPELedger!="")
    {   
        var tbl=divPELedger.firstChild;
        var cnt=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedPELedger)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedPELedger="";
                t.value="";
            }
        }
    }
    else
        t.value="";
    prevText=t.value;
    divPELedger.style.display="none";    
}

//Receipt Entry Ledger
var divRELedger;
var selectedRELedger="";
prevText="";

function FillRELedger(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedRELedger="";
        divRELedger=document.getElementById("divRELedger");
        if(t.value!="")
        {        
            PageMethods.CallFillPayAndRecLedger(ddlBranch.value,t.value,FillRELedgerComplete)
        }
        else{
        divRELedger.innerHTML="";
        divRELedger.style.display="none";
        }
    }
}

function FillRELedgerComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var lid;
            var lname;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                    lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                    lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                    lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectREClick(this)' onmouseover='OnREMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                selectedRELedger=lid;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectREClick(this)' onmouseover='OnREMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divRELedger.innerHTML=tbl;
        if(count>0)
            divRELedger.style.display="block";
        else
            divRELedger.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectREClick(t)
{
    selectedRELedger=t.cells[0].innerHTML;
    var txt=document.getElementById("tdReceiptToLedger");
    ApplyRELedger(txt);
}
function OnREMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedRELedger ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedRELedger=t.cells[0].innerHTML;
}
function selectRELedger(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyRELedger(t);
    }
    else
    {
        if(divRELedger==null)
            divRELedger=document.getElementById("divRELedger");
        var tbl=divRELedger.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedRELedger)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedRELedger=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedRELedger=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyRELedger(t)
{
    if(divRELedger==null)
            divRELedger=document.getElementById("divRELedger");
    if(selectedRELedger!="")
    {   
        var tbl=divRELedger.firstChild;
        var cnt=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedRELedger)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedRELedger="";
                t.value="";
            }
        }
    }
    else
        t.value="";
    prevText=t.value;
    divRELedger.style.display="none";    
}
//Journal Entry Ledger
var divJELedger;
var selectedJELedger="";
prevText="";

function FillJELedger(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedJELedger="";
        divJELedger=document.getElementById("divJELedger");
        if(t.value!="")
        {        
            PageMethods.CallFillJELedger(ddlBranch.value,t.value,FillJELedgerComplete)
        }
        else{
        divJELedger.innerHTML="";
        divJELedger.style.display="none";
        }
    }
}

function FillJELedgerComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var lid;
            var lname;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                    lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                    lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                    lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectJEClick(this)' onmouseover='OnJEMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                selectedJELedger=lid;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectJEClick(this)' onmouseover='OnJEMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+lname+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divJELedger.innerHTML=tbl;
        if(count>0)
            divJELedger.style.display="block";
        else
            divJELedger.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectJEClick(t)
{
    selectedJELedger=t.cells[0].innerHTML;
    var txt=document.getElementById("tdJournalLedger");
    ApplyJELedger(txt);
}
function OnJEMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedJELedger ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedJELedger=t.cells[0].innerHTML;
}
function selectJELedger(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyJELedger(t);
    }
    else
    {
        if(divJELedger==null)
            divJELedger=document.getElementById("divJELedger");
        var tbl=divJELedger.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedJELedger)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedJELedger=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedJELedger=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyJELedger(t)
{
    if(divJELedger==null)
            divJELedger=document.getElementById("divJELedger");
    if(selectedJELedger!="")
    {   
        var tbl=divJELedger.firstChild;
        var cnt=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedJELedger)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedJELedger="";
                t.value="";
            }
        }
    }
    else
        t.value="";
    prevText=t.value;
    divJELedger.style.display="none";    
}