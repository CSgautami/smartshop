﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class OnlyDayBook : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hidUserID.Value = Session["UserID"].ToString();
            hidDBEDate.Value = Request.QueryString["DBEDate"].ToString();
            hidBranchID.Value = Request.QueryString["BranchID"].ToString();
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetDayBookReport(string BranchID, string DateTo)
        {
            try
            {
                DayBookEntry_BLL bll_DayBookEntry = new DayBookEntry_BLL();
                return bll_DayBookEntry.GetDayBookReport(BranchID, dateFormat(DateTo)).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
