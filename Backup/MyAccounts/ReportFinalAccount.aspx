﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportFinalAccount.aspx.cs" Inherits="MyAccounts20.ReportFinalAccount" Title="ReportFinalAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="JS/Reports/Reports.js" type="text/javascript"></script>--%>
    <link href="Calendar.css" rel="stylesheet" type="text/css" />
    <script src="JS/CalendarPopup.js" type="text/javascript"></script>
   <script language="javascript" type="text/javascript">
    var dtFormat='dd-MM-yyyy';
    var preid;
    var chkDatewise;
    var From;
    var To;
    var rCondenced;
    var rDetailed;
    var rTradingAccount;
    var rProfitandLossAccount;
    var rBalenceSheet;
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
    preid ='ctl00_ContentPlaceHolder1_'
    function GetControls()
    {
        chkDatewise=document.getElementById(preid+ "chkDateWise");  
        From=document.getElementById(preid+ "txtDate");
        To=document.getElementById(preid+"txtTo");              
        rDetailed=document.getElementById(preid+"rDetailed");
        rCondenced=document.getElementById(preid+"rCondenced");
        rTradingAccount =document.getElementById(preid+"rTradingAccount");
        rProfitandLossAccount=document.getElementById(preid+"rProfitandLossAccount");
        rBalenceSheet=document.getElementById(preid+"rBalenceSheet");
    }
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function DateWise_Click(e,t)
    {
        if(t.checked==true)
        {
            From.disabled=false;
            To.disabled=false;
        }
        else
        {
            From.disabled=true;
            To.disabled=true;
        }
    }
    function SetData()
    {    
        GetControls();
//        From.value = new Date().format("dd-MM-yyyy");
//        To.value=new Date().format("dd-MM-yyyy");
        rBalenceSheet.checked=true;
        rDetailed.checked=true;
        rBalenceSheet.focus();
    }
    function btnShow_Click()
    {       
        window.frames["frmReportFinalAccount"].location.href="Reports/FinalAccountReport.aspx?bDate="+chkDatewise.checked+"&dateFrom="+From.value+"&dateTo="+To.value+"&bDetail="+rDetailed.checked+"&optTrade="+rTradingAccount.checked+"&optPAndL="+rProfitandLossAccount.checked+"&optBal="+rBalenceSheet.checked;
        rBalenceSheet.focus();
    }          
    function Condenced_Click()
    {        
       rDetailed.checked=false;     
    }
    function Detailed_Click()
    {
        rCondenced.checked=false;
    }
    function BalanceSheet_Click()
    {
        rProfitandLossAccount.checked=false;
        rTradingAccount.checked=false;
    }
    function Profit_Click()
    {
        rTradingAccount.checked=false;
        rBalenceSheet.checked=false;
    }
    function Trading_Click()
    {
        rBalenceSheet.checked=false;
        rProfitandLossAccount.checked=false;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
        
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                   <tr>
                                       <td height="20px" colspan="100%">
                                       </td>
                                   </tr>
                                    <tr>    
                                       
                                        <td align="left">
                                            <asp:CheckBox id="chkDateWise" Text="DateWise" onClick="DateWise_Click(event,this);" runat="server"/>                                            
                                        </td>
                                        <td style="width:50px;"></td>
                                        <td align="left">
                                         &nbsp;   Date:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDate"  runat="server" Width="100px" Enabled="false" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"></asp:TextBox>                                        
                                        </td>
                                        <td style="width:50px;"></td>
                                        <td>
                                            To:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTo" runat="server" Width="100px" Enabled="false" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"></asp:TextBox>
                                        </td>
                                        <td style="width:50px;"></td>
                                        <td align="left">
                                            <asp:RadioButton ID="rCondenced" Text="Condenced" runat="server" onClick="Condenced_Click()"/> 
                                        </td>
                                        <td style="width:50px;"></td>
                                        <td>
                                            <asp:RadioButton ID="rDetailed" Text="Detailed" runat="server" onClick="Detailed_Click()" />
                                        </td>
                                    </tr>    
                                    <tr><td></td></tr>                                                            
                                    <tr>                                    
                                        <td align="left">
                                            <asp:RadioButton ID="rTradingAccount" Text="TradingAccount" runat="server" onClick="Trading_Click();" />
                                        </td>  
                                        <td></td>                                      
                                        <td align="left" colspan="2">
                                            <asp:RadioButton ID="rProfitandLossAccount" Text="ProfitandLossAccount" runat="server" onClick="Profit_Click();" />
                                        </td>
                                        <td></td>                                        
                                        <td align="left" colspan="2">
                                            <asp:RadioButton ID="rBalenceSheet" Text="BalanceSheet" runat="server" onClick="BalanceSheet_Click();" />
                                        </td>
                                        <td></td>
                                        
                                        <td align="left">
                                         &nbsp;<input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmReportFinalAccount" id="frmReportFinalAccount"
                                    style="width: 100%; height: 400px;" frameborder="0" scrolling="auto" ></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
          <input type="hidden" runat="server" id="hidUserID" />
    </div>
</asp:Content>
