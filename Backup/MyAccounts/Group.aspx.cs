﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class Group : System.Web.UI.Page
    {
        ItemCreation_BLL bllHome = new ItemCreation_BLL();
        ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (Request.QueryString["GID"] != null)
                    FillDisplayGroups(Request.QueryString["GID"].ToString());
            }
        }

        void FillDisplayGroups(string StockGroupID)
        {
            try
            {
                DataSet ds = bllHome.GetDesiplayGroups(StockGroupID);
                dlItemDet.DataSource = ds;
                dlItemDet.DataBind();
                CheckAvailabity();
            }
            catch
            {
                throw;
            }
        }
        protected void btnAddtoCart_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
        void CheckAvailabity()
        {
            foreach (DataListItem dl in dlItemDet.Items)
            {
                int dlItemID = Convert.ToInt32(((HtmlInputHidden)dl.FindControl("hidID")).Value);
                decimal Qty;
                Qty = Convert.ToDecimal(obj_BLL.GetStockAvailability(dlItemID).Tables[0].Rows[0]["BalQty"].ToString());
                if (Qty > 0)
                {                    
                    ((CheckBox)dl.FindControl("chkAvailable")).Visible = true;
                    ((CheckBox)dl.FindControl("chkAvailable")).Checked = true;                    
                    ((Image)dl.FindControl("imgNA")).Visible = false;
                    ((TextBox)dl.FindControl("txtQty")).Enabled = true;
                }
                else
                {                    
                    ((CheckBox)dl.FindControl("chkAvailable")).Visible = false;
                    ((Image)dl.FindControl("imgNA")).Visible = true;
                    ((TextBox)dl.FindControl("txtQty")).Enabled = false;
                }
            }
        }               
    }
}
