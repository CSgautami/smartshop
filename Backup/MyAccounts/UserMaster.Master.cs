﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

namespace MyAccounts20
{
    public partial class UserMaster : System.Web.UI.MasterPage
    {
        public static StringBuilder sb;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                GetHead();
                if (Session[MyAccountsSession.UserName] != null)
                {
                    lblwel.Text = "WelCome To " + Session["LName"].ToString();
                }
                else
                {
                    lblwel.Text = "WelCome To Our Online Store";
                    tdHome.Visible = true;
                    tdchange.Visible = false;
                }
                if (Session["Type"] != null)
                {
                    if (Session["Type"].ToString() == "Customer")
                    {
                        tdchange.Visible = true;
                        tdHome.Visible = false;
                    }
                    else
                    {
                        tdHome.Visible = true;
                        tdchange.Visible = false;
                    }
                }         
            }
               
        }
        void GetHead()
        {
            try
            {
                DataTable dt;
                BLL.ItemCreation_BLL Obj_BLL = new BLL.ItemCreation_BLL();
                DataSet ds = Obj_BLL.GetHeadsAndCategory();
                int j, nCnt = 1;
                //sb.Append("<ul id='nav'>");
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class='product_midd_search'><div id='nav' style='white-space:nowrap;'><ul id='topnav' class='clearfix'>");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sb.Append("<li class='navMain nav2'>");
                    if(dr["HeadGroupName"].ToString()=="Pet Care")
                        sb.Append("<a class='navLink2 nav' style='border:none !important;' href=\"Listing.aspx?HID=" + dr["HeadGroupID"].ToString() + "\">" + dr["HeadGroupName"].ToString() + "</a>");
                    else
                    sb.Append("<a class='navLink2 nav' href=\"Listing.aspx?HID=" + dr["HeadGroupID"].ToString() + "\">" + dr["HeadGroupName"].ToString() + "</a>");
                    ds.Tables[1].DefaultView.RowFilter = "HeadGroupID=" + dr["HeadGroupID"].ToString();
                    dt = ds.Tables[1].DefaultView.ToTable();
                    int x = 0;
                    if (dt.Rows.Count > 0)
                    {
                        sb.Append("<div class='sub' style='display: none;'>");
                        j = 0;
                        x = 0;
                        foreach (DataRow dtrow in dt.Rows)
                        {
                            if (j == 0)
                            {
                                if(x<28)                                
                                    sb.Append("<ul class='split_navLinks borderDotted'>");
                                else
                                    sb.Append("<ul class='split_navLinks borderNone'>");
                                sb.Append("<span class='navHeader'>Categories</span>");

                            }
                            j++;
                            x++;
                            //sb.Append("<li class=''><a href=\"Group.aspx?GID=" + dtrow["StockGroupID"].ToString() + "\">" + dtrow["StockGroupName"].ToString() + "</a></li>");
                            sb.Append("<li><a href=\"Listing.aspx?CID=" + dtrow["CategoryID"].ToString() + "\">" + dtrow["Category"].ToString() + "</a></li>");
                            if (j == 14)
                            {
                                sb.Append("</ul>");
                                j = 0;
                            }
                        }
                        while (x <= 42)
                        {                            
                            while (j <= 14)
                            {
                                if (j == 0)
                                {
                                    if (x < 28)
                                        sb.Append("<ul class='split_navLinks borderDotted'>");
                                    else
                                        sb.Append("<ul class='split_navLinks borderNone'>");
                                    sb.Append("<span class='navHeader'> </span>");
                                }
                                j++;
                                x++;
                                sb.Append("<li> </li>");                             
                            }
                            sb.Append("</ul>");
                             j = 0;                            
                        }
                        sb.Append("<ul class='split_navLinks menu_image'><li style='margin:0;padding:0;'><img src='MasterPageFiles2/Images/" + nCnt + ".gif' width='395' height='300' border='0'></li></ul>");
                        nCnt++;
                        sb.Append("</div>");
                    }
                    sb.Append("</li>");
                    //if (Session[MyAccountsSession.UserID] != null)
                    //    sb.Append("<td width='171' class='ullist'><a href=\"ShopingCart.aspx?GID=" + dr["StockGroupID"].ToString() + "\">" + dr["StockGroupName"].ToString() + "</a></td>");
                    //else
                    //    sb.Append("<td width='171' class='ullist'><a href=\"Group.aspx?GID=" + dr["StockGroupID"].ToString() + "\">" + dr["StockGroupName"].ToString() + "</a></td>");

                }
                sb.Append("</ul></div></div>");
                divHead.InnerHtml = sb.ToString();
            }
            catch
            {
                throw;
            }
        }

        protected void txtSearch_Disposed(object sender, EventArgs e)
        {
            Session["Search"] = txtSearch.Text;
        }
        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            if (Session[MyAccountsSession.TempOrderID] != null && Session["UserID"] != null)
            {
                Response.Redirect("ConfirmPage.aspx", true);
            }
            else
            {                
                Response.Redirect("Login.aspx", true);
            }

        }

        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Session[MyAccountsSession.UserName] = null;
            Session[MyAccountsSession.UserID] = null;
            Session["Type"] = null;
            Session["LName"] = null;
            Response.Redirect("Index.aspx", true);           
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            Session["ItemSearch"] = txtSearch.Text;
            Response.Redirect("listing.aspx", true);
        }
    }
}
