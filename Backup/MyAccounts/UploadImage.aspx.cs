﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace MyAccounts20
{
    public partial class UploadImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                try
                {
                    /*
                    string ftpUserID = ConfigurationManager.AppSettings["FTPUserName"].ToString();
                    string ftpPassword = ConfigurationManager.AppSettings["FTPPassword"].ToString();

                    string FileName = FileUpload1.FileName;
                    string strFileExt = Path.GetExtension(FileUpload1.FileName);

                    string FTPFileName = DateTime.Now.Ticks + strFileExt;

                    string ftpLocation = ConfigurationManager.AppSettings["FTPLocation"].ToString();
                    string uri = ftpLocation + "/" + FTPFileName;
                    FtpWebRequest reqFTP;

                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(
                               uri));

                    reqFTP.Credentials = new NetworkCredential(ftpUserID,
                                                               ftpPassword);


                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.ContentLength = FileUpload1.PostedFile.ContentLength;
                    int buffLength = 2048;
                    byte[] buff = new byte[buffLength];
                    int contentLen;

                    Stream fs = FileUpload1.PostedFile.InputStream;

                    Stream strm = reqFTP.GetRequestStream();

                    contentLen = fs.Read(buff, 0, buffLength);


                    while (contentLen != 0)
                    {

                        strm.Write(buff, 0, contentLen);
                        contentLen = fs.Read(buff, 0, buffLength);
                    }


                    strm.Close();
                    fs.Close();
                    */
                    System.IO.Stream myStream;
                    Int32 fileLen;
                    fileLen = FileUpload1.PostedFile.ContentLength;
                    Byte[] Input = new Byte[fileLen];
                    myStream = FileUpload1.FileContent;
                    myStream.Read(Input, 0, fileLen);
                    Session["ImgStream"] = Input;
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "top.parent.GetImages();", true);

                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }

    }
}
