﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="geneology.aspx.cs" Inherits="MyAccounts20.geneology" Title="Geneology" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td colspan="2" align="center" nowrap="nowrap">
                <table width="800px">
                    <tr>
                        <td colspan="true" align="center">
                            <asp:TextBox ID="txtFind" runat="server" Width="100px"></asp:TextBox>                        
                            <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="true" align="center">
                            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" >
                    <tr>
                        <td colspan="3" align="center">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="bottom"  id="tdLCount" runat="server">
                                        
                                    </td>
                                    <td>
                                        <img src="Imgs/tree.jpg" alt="" id="imgMain" runat="server" />
                                    </td>
                                    <td valign="bottom"  id="tdRCount" runat="server">
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdmain" runat="server">
                                    
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" id="td1" runat="server">
                            <img src="Imgs/treerow.png" alt="" border="0"  />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <div id="divLeft" runat="server" style="border: solid 1px silver; width: 350px;">
                                &nbsp;
                            </div>
                        </td>
                        <td style="width: 150px;">
                        </td>
                        <td valign="top">
                            <div id="divRight" runat="server" style="border: solid 1px silver; width: 350px;">
                                &nbsp;
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" runat="server" id="hidYourID" />
</asp:Content>

