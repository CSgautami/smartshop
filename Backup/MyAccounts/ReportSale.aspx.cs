﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;


namespace MyAccounts20
{
    public partial class ReportSale : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
                DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
                txtFromDate.Value = nFirstDate.ToString("dd-MM-yyyy");
                txtToDate.Value = nLastDate.ToString("dd-MM-yyyy");
                //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "GetCustomer(" + hidUserID.Value + ");", true);                
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerName(string BranchID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetLedgerName(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillLedger(string BranchID, string prefix)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.CallFillLedger(BranchID, prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerDetails(string LedgerID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetLedgerDetails(LedgerID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
