﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;

namespace MyAccounts20
{
    public partial class BillFormat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sale_BLL obj_Bll = new Sale_BLL();
            string strInvID = Request.QueryString["SInvID"];
            string BranchID = Request.QueryString["BranchID"];
            DataSet ds = new DataSet();
            if (!IsPostBack)
            {
                if (strInvID != "")
                {
                    ds = obj_Bll.GetInvoiceBillData(strInvID,BranchID);
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb1 = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder();
                    StringBuilder sb3 = new StringBuilder();
                    string strDots = "----------------------------------------------------------------------------------";
                    //string strStars = "***************************************************************************************";
                    if (ds.Tables.Count > 0)
                    {
                        sb.Append("<table  border='0' cellspacing='0' align='center'>");
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>&nbsp;" + ds.Tables[0].Rows[0]["BranchName"].ToString() + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>&nbsp;" + ds.Tables[0].Rows[0]["BranchAddress"].ToString() + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>&nbsp;" + ds.Tables[0].Rows[0]["ContactNo"].ToString() + "</td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        divBranchDet.InnerHtml = sb.ToString();
                    }
                    
                    // sb.Append = "";                                       
                    if (ds.Tables.Count > 1)
                    {
                        tdBillNo.InnerText = ds.Tables[1].Rows[0]["DCNo"].ToString();
                        tdDate.InnerText = ds.Tables[1].Rows[0]["InvDate"].ToString();
                        tdCusID.InnerText = ds.Tables[1].Rows[0]["YourID"].ToString();
                        tdCusName.InnerText = ds.Tables[1].Rows[0]["LedgerName"].ToString();
                        tdAddress.InnerText = ds.Tables[1].Rows[0]["Address"].ToString().Trim();
                       // tdPhone.InnerText = ds.Tables[1].Rows[0]["Phone"].ToString();
                        tdLandMark.InnerText = ds.Tables[1].Rows[0]["LandMark"].ToString();
                        //tdMargin.InnerText =  ds.Tables[1].Rows[0]["TotMargin"].ToString().Replace(".","/");
                    }
                    //style='border: 1px solid;'                    
                    if (ds.Tables.Count > 2)
                    {
                        sb1.Append("<table border='0' cellspacing='0' align='left' valign='top' >");
                        int cellscount = ds.Tables[2].Columns.Count;
                        
                        sb1.Append("<tr>");
                        sb1.Append("<td colspan='100%' align='left'>" + strDots + "</td>");
                        sb1.Append("</tr>");
                        foreach (DataColumn dc in ds.Tables[2].Columns)
                            sb1.Append("<th>" + dc.ColumnName + "</th>");
                        sb1.Append("<tr>");
                        sb1.Append("<td colspan='100%' align='left'>" + strDots + "</td>");
                        sb1.Append("</tr>");
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            sb1.Append("<tr>");
                            for (int i = 0; i < cellscount; i++)
                            {
                                if (i == 0)
                                    sb1.Append("<td align='left'>&nbsp;" + dr[i].ToString() + "</td>");
                                //else if (i == 1)
                                //    sb1.Append("<td align='right'>&nbsp;" + dr[i].ToString() + "</td>");
                                else
                                    sb1.Append("<td align='right'>&nbsp;" + dr[i].ToString() + "</td>");
                            }
                            sb1.Append("</tr>");                            

                        }
                        sb1.Append("<tr>");
                        sb1.Append("<td colspan='100%' align='left'>" + strDots + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        sb1.Append("<td align='left'><b>Total</b></td><td align='right'>&nbsp;" + ds.Tables[2].Compute("sum(Qty)", "") + "</td><td>&nbsp;</td><td>&nbsp;</td><td align='right'>&nbsp;" + ds.Tables[2].Compute("sum(Amount)", "") + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        sb1.Append("<td align='left' colspan='" + (cellscount - 1).ToString() + "'><b>Discount</b></td><td align='right'>&nbsp;" + ds.Tables[1].Compute("sum(NetDisc)", "") + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        sb1.Append("<td align='left' colspan='" + (cellscount - 1).ToString() + "'><b>Net Amount</b></td><td align='right'>&nbsp;" +(Convert.ToInt32(ds.Tables[1].Compute("sum(NetAmount)", ""))) + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        NumberInWords obj = new NumberInWords();
                        sb1.Append("<td align='left' colspan='" + (cellscount).ToString() + "'><b>InWords :" + obj.NumberToWords(Convert.ToInt32(ds.Tables[1].Compute("sum(NetAmount)", ""))) + " </b></td>");
                        sb1.Append("</tr>");                        
                        sb1.Append("</table>");

                        //strDots = "---------------------------------------------------------------------------------------";
                        sb1.Append("<tr>");
                        sb1.Append("<td colspan='100%' align='left'>" + strDots + "</td>");
                        sb1.Append("</tr>");
                        divItemDet.InnerHtml = sb1.ToString();
                       
                    }
                    
                    if (ds.Tables.Count > 3)
                    {
                        //sb2.Append("<table>");
                        //sb2.Append("<tr>");
                        //sb2.Append("<td><h3> Total MRP : Rs.</h3> </td>");
                        //sb2.Append("<td><h2>" + ds.Tables[3].Rows[0]["TotalMrp"].ToString() + "</h2></td></tr></table>");
                        //tdNetAmt.InnerHtml = sb2.ToString();
                        //sb3.Append("<table border='0' cellspacing='0' align='left' valign='top'><tr>");
                        //sb3.Append("<td><h3> You Saved : Rs.</h3></td>");
                        //sb3.Append("<td><h2>" + ds.Tables[3].Rows[0]["DiffAmt"].ToString() + "</h2></td></tr></table>");
                        //tdMargin.InnerHtml = sb3.ToString();
                        tdNetAmt.InnerText = "Rs." + ds.Tables[3].Rows[0]["TotalMrp"].ToString();
                        tdMargin.InnerText = "Rs." + ds.Tables[3].Rows[0]["DiffAmt"].ToString();
                        
                    }
                    if (ds.Tables.Count > 4)
                    {
                        //sb3.Append("<table>");
                        //sb3.Append("<tr>");
                        //sb3.Append("<td><table>");
                        //sb3.Append("<tr><td><h3><b>Free </b></h3> </td></tr>");
                        //sb3.Append("<tr><td><h3><b>Veg</b></h3> </td></tr>");
                        //sb3.Append("<tr><td><h3><b>Coupons</b></h3> </td></tr>");
                        //sb3.Append("</table></td>");
                        //sb3.Append("Rs.<td  style='font-size:x-large' rowspan='100%'><b>" + ds.Tables[4].Rows[0]["Profit"].ToString() + "</b></td></tr></table>");
                        //tdProfit.InnerHtml = sb3.ToString();
                        //tdProfit.InnerHtml = "<h3>Free Vegitable Fruits</h3>";
                        divCoupenAmt.InnerText ="Rs." + ds.Tables[4].Rows[0]["Profit"].ToString();
                        tdline.InnerText = strDots;
                        effPrice.InnerText = "Rs." + (Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalMrp"].ToString()) - Convert.ToDecimal(ds.Tables[3].Rows[0]["DiffAmt"].ToString()) - Convert.ToDecimal(ds.Tables[4].Rows[0]["Profit"].ToString())).ToString();
                        tddisc.InnerText=Math.Round(((Convert.ToDecimal(ds.Tables[3].Rows[0]["DiffAmt"].ToString())) + (Convert.ToDecimal(ds.Tables[4].Rows[0]["Profit"].ToString())))*100/(Convert.ToDecimal(ds.Tables[3].Rows[0]["TotalMrp"].ToString()))).ToString()+"%";

                    }

                    //strDots = "---------------------------------------------------------------------------------------";
                }
            } 
        }
    }
}
