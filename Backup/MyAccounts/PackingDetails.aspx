﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="PackingDetails.aspx.cs" Inherits="MyAccounts20.PackingDetails" Title="Packing Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="JS/Packing.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 600px;">
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table align="center">
                        <tr>
                            <td align="left" style="width: 100px;">
                                Pack Name
                            </td>
                            <td align="left" style="width: 400px;">
                                <asp:TextBox ID="txtPack" runat="server" Width="205px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div class="divgrid" style="height: 150px; width: 600px; border: solid 1px #98bf21;">
                        <table id="tblGrid" width="580px">
                            <tr class="dbookheader" align="left">
                                <th style="display: none">
                                    ItemID
                                </th>
                                <th style="width: 100px;" align="left">
                                    Code
                                </th>
                                <th style="width: 200px;" align="left">
                                    Item
                                </th>
                                <th style="width: 100px;">
                                    Quantity
                                </th>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table align='left'>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="4" align="left" style="height: 25px;">
                                <asp:Label ID="lblItem" Text="" runat="server"></asp:Label>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Item
                            </td>
                            <td>
                            <%--<td align="left" nowrap="nowrap" style="display: none;">
                                <asp:DropDownList ID="ddlItem" runat="server" Width="200px" onChange="GetItemName();">
                                </asp:DropDownList>
                            </td>--%>
                                <table cellpadding="0" cellspacing="0">
                                    <tr style="display: none;">
                                        <td>
                                            <select style="width: 100px;" id="ddlItem" onchange="GetItemBatchAndTaxSystem();">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" style="width: 100px;" id="tdItem" onkeydown="selectItem(event,this);" onkeyup="FillItem(event,this);"  onblur="ApplyItem(this);GetItemName();"/>                                                
                                            &nbsp;
                                            <asp:Label ID="Label1" Text="" runat="server"></asp:Label>                                    
                                        </td>
                                    </tr>
                                    <tr style="position: relative;">
                                        <td align="left">
                                            <div style=" display: none; width:100px; " class="divautocomplete"
                                                id="divItem">
                                            </div>
                                        </td>
                                    </tr>
                                </table>                                    
                            </td>
                            <td align="left">
                                Qty
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtQty" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td align="left">
                                <input type="button" id="btnAdd" value="Add" runat="server" onclick="btnAdd_Click();" />
                            </td>
                        </tr>                        
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblMsg" runat="server" Style="color: Red;" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 200px;">
                                <table>
                                    <tr>
                                        <td>
                                            <input type="button" id="btnSave" value="Save" onclick="btnSave_Click(event,this);" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnUpdate" value="Update" runat="server" class="hidbutton"
                                                onclick="btnUpdate_Click(event,this);" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCancel" runat="server" value="Cancel" onclick="btnCancel_Click();" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnDelete" runat="server" class="hidbutton" value="Delete"
                                                onclick="btnDelete_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" width="200px" id="trNav" runat="server">
                                <input type="button" id="btnFirst" value="|<" onclick="btnPDRecordNav_Click(event,this);" />
                                <input type="button" id="btnPrev" value="<" onclick="btnPDRecordNav_Click(event,this);" />
                                <input type="button" id="btnNext" value=">" onclick="btnPDRecordNav_Click(event,this);" />
                                <input type="button" id="btnLast" value=">|" onclick="btnPDRecordNav_Click(event,this);" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <input type="hidden" id="hidUserID" runat="server" />
        <input type="hidden" id="hidPackID" runat="server" />
    </div>
</asp:Content>
