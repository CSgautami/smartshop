﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;
namespace MyAccounts20
{
    public partial class ReportUserPriceList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {                
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockGroup()
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetStockGroup().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
