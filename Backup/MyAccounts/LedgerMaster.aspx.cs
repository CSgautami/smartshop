﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class LedgerMaster : GlobalPage
    {
        //Ledger_BLL obj_BAL = new Ledger_BLL();
        //DataSet ds = new DataSet();
        //public static string strID,nGID;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                string strID = Request.QueryString["ID"];
                string nGID = Request.QueryString["GID"];
                if (hidbinditems.Value == "1")
                {
                    hidbinditems.Value = "";
                    btnBindItem_Click();
                }
               
                //txtAmount.Text = nGID.ToString();
                //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "alert('Flag : " + strID + ".');", true);
                if (!IsPostBack)
                {
                    if (Session["UserID"].ToString() != "0")
                        trNav.Visible = false;
                    txtLedgerName.Focus();
                    if (Request.QueryString["DBE"] == null)
                    {

                        trHeader.Visible = true;
                        tblContent.Attributes["align"] = "center";
                        trMenu.Visible = true;
                        trFooter.Visible = true;
                    }
                    hidUserID.Value = Session["UserID"].ToString();
                    hidGBID.Value = Session["GBID"].ToString();
                    hidGroupID.Value = nGID;
                    GetDefaults();
                   
                    //FillddList();
                    if (strID != null && strID != "")
                    {
                        trHeader.Visible = false;
                        trMenu.Visible = false;
                        tdCancel.Visible = false;
                        trFooter.Visible = false;
                        trNav.Visible = false;
                        strID = "";
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void GetDefaults()
        {
            //Purchase_BLL obj_BLL = new Purchase_BLL();
            //ddlBranch.DataSource = obj_BLL.GetBranch(Session["UserID"].ToString());
            //ddlBranch.DataTextField = "BranchName";
            //ddlBranch.DataValueField = "BranchID";
            //ddlBranch.DataBind();
            //if (ddlBranch.Items.Count != 1)
            //{
            //    ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
            //}
            //if (Session["GBID"].ToString() != null)
            //    ddlBranch.SelectedIndex = Convert.ToInt32(Session["GBID"].ToString());
            //ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
            //ddlGroupName.DataSource = obj_BAL.GetGroupName(hidUserID.Value);
            //ddlGroupName.DataTextField = "GroupName";
            //ddlGroupName.DataValueField = "GroupID";
            //ddlGroupName.DataBind();
            //ddlGroupName.Items.Insert(0, new ListItem("Select", "0"));            
            Ledger_BLL Obj_BLL = new Ledger_BLL();
            ddlArea.DataSource = Obj_BLL.GetArea();
            ddlArea.DataTextField = "AreaCode";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0,new ListItem("Select","0"));
        }
        //public void GetPins()
        //{
        //    Ledger_BLL Obj_BLL = new Ledger_BLL();
        //    ddlPinNo.DataSource = Obj_BLL.GetPins(ddlRef.SelectedValue);
        //    ddlPinNo.DataTextField = "PinNumber";
        //    ddlPinNo.DataValueField = "PinID";
        //    ddlPinNo.DataBind();
        //    ddlPinNo.Items.Insert(0, new ListItem("Select", "0"));
        //}
        //void FillddList()
        //{
        ////    ds = obj_BAL.GetGroupName(hidUserID.Value);
        ////    ddlGroupName.DataSource = ds.Tables[0];
        ////    ddlGroupName.DataTextField = "GroupName";
        ////    ddlGroupName.DataValueField = "GroupID";
        ////    ddlGroupName.DataBind();
        ////    ddlGroupName.Items.Insert(0, new ListItem("Select", "0"));
        ////}

        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetGroups(string BranchID)
        //{
        //    Ledger_BLL obj_BAL = new Ledger_BLL();
        //    return obj_BAL.GetGroupName(BranchID).GetXml();
        //}


        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnSave_Click(string YourID,string Password, string BranchID, string LedgerName,string RefNo,string LandMark,  string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode,string Area, string Sales, string Purchase, string UserID,string PinNo)
        {
            string rtnVal = "";
            try
            {
                if (Amount == "")
                    Amount = null;
                Ledger_BLL obj_BAL = new Ledger_BLL();
                string res = obj_BAL.SaveLedger(YourID,Password, BranchID, LedgerName,RefNo,LandMark, GroupID, Status, Amount,  Address, Town, Phone, APGST, Mode,Area , Sales, Purchase, UserID,PinNo);
                rtnVal = res;
            }
            catch { rtnVal = "0"; };
            return rtnVal;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnDelete_Click(string LedgerID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.DeleteLedger(LedgerID);
            }
            catch 
            {
                throw;
            }            
        }
        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string Test(string YourID, string Password, string BranchID, string LedgerName, string RefNo, string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserID)
        //{
        //    try
        //    {
        //        return "1";
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        //public static string btnUpdate_Click(string LedgerNo, string YourID, string Password, string BranchID, string LedgerName, string RefNo, string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserID)
        public static string btnUpdate_Click(string LedgerID, string LedgerNo, string Password, string BranchID, string LedgerName,  string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserID)
        {
            string rtnVal = "";
            try
            {
                if (Amount == "")
                    Amount = null;
                Ledger_BLL obj_BAL = new Ledger_BLL();
                //string res = obj_BAL.UpdateLedger(LedgerNo, YourID, Password, BranchID, LedgerName, RefNo, LandMark, GroupID, Status, Amount, Address, Town, Phone, APGST, Mode, Area, Sales, Purchase, UserID);
                string res = obj_BAL.UpdateLedger(LedgerID , LedgerNo, Password, BranchID, LedgerName, LandMark, GroupID, Status, Amount, Address, Town, Phone, APGST, Mode, Area, Sales, Purchase, UserID);
                rtnVal = res;
            }
            catch { rtnVal = "0"; };
            return rtnVal;

        }


        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLMNavData(string Search,string BranchID, string LMNO, string Flag)
        {
            try
            {
                Ledger_BLL bll_Ledger = new Ledger_BLL();
                return bll_Ledger.GetLMNavData(Search,BranchID, LMNO, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetRefMember(string BranchID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.GetRefMember(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetRefName(string LedgerID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.GetRefName(LedgerID).Tables[0].Rows[0]["LedgerName"].ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPins(string LedgerID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.GetPins(LedgerID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetGroupDetails(string GroupID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.GetGroupDetails(GroupID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetGroups(string BranchID)
        {
            try
            {
                Ledger_BLL obj_BAL = new Ledger_BLL();
                return obj_BAL.GetGroups(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void btnBindItem_Click()
        {
            try
            {
                Ledger_BLL Obj_BLL = new Ledger_BLL();
                ddlArea.DataSource = Obj_BLL.GetArea();
                ddlArea.DataTextField = "AreaCode";
                ddlArea.DataValueField = "AreaID";
                ddlArea.DataBind();
                ddlArea.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch
            {
                throw;
            }
        }
    }
}
        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetGroupDetails(string GroupID)
        //{
        //    try
        //    {
        //        Ledger_BLL Obj_BLL=new Ledger_BLL();
        //        return Obj_BLL.GetGroupDetails(GroupID).GetXml();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    
