﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="OrderSummury.aspx.cs" Inherits="MyAccounts20.OrderSummury" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target=_self />
    <script language="javascript" type="text/javascript">    
        String.prototype.trim = function () {
            return this.replace(/^\s*/, "").replace(/\s*$/, "");
        }
    var preid='ctl00_ContentPlaceHolder1_';
    function ValidateShipmentDet()
    {
       try
       {
        var TransportName=document.getElementById(preid+"txtTransport");
        var ShipmentDet=document.getElementById(preid+"ddlShipmentDet");
        var DisputeDate=document.getElementById(preid+"txtDisputeDate");
        var TrackNo=document.getElementById(preid+"txtTrackNo");
        
        if(ShipmentDet.value=="0")
        {
        alert("Select Shipment Deatails..");
        ShipmentDet.focus();
        return false;
        }
        
        if(TransportName.value=="")
        {
        alert("Enter Transport Name..");
        TransportName.focus();
        return false;
        }
        
        if(DisputeDate.value=="")
        {
        alert("Select Dispute Date..");
        DisputeDate.focus();
        return false;
        }
        if(TrackNo.value.trim()=="")
        {
        alert("Enter Track Number..");
        TrackNo.focus();
        return false;
        }
        
        return true;
       }
        catch(err)
        { return false; }
    }
   
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" style="width: 900px">
            <tr>
                <td colspan="5">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="font-weight:bold; color:Blue;" colspan="5">
                    Order Details
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="24%" height="20">
                    <strong>Shipping Details </strong>
                </td>
                <td width="24%">
                    <strong>Transport Name</strong>
                </td>
                <td width="24%">
                    <strong>Shipped Date</strong>
                </td>
                <td width="19%">
                    <strong>Tracking No</strong>
                </td>
                <td width="9%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <span <strong>
                        <asp:DropDownList ID="ddlShipmentDet" runat="server" Width="155px">
                        </asp:DropDownList>
                    </strong></span>
                </td>
                <td>
                    <%--<asp:DropDownList ID="ddlCourierName" runat="server" Width="155px">
                    </asp:DropDownList>--%>
                    <asp:TextBox ID="txtTransport" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtDisputeDate" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtTrackNo" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="return ValidateShipmentDet();"
                        OnClick="btnSave_Click" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5" style="height:10px;">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:GridView ID="gvOrder" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Order No">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderID" Width="200px" runat="server" Text='<%#Eval("OID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" Width="200px" runat="server" Text='<%#Eval("Date")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" Width="200px" runat="server" Text='<%#Eval("NetAmount")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:GridView ID="gvOrderDetails" runat="server" AutoGenerateColumns="false" 
                        ShowFooter="true" onrowdatabound="gvOrderDetails_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <input type="hidden" runat="server" id="hidItemID" value='<%#Eval("ItemID")%>' />
                                    <asp:Label ID="lblItemCode" Width="100px" runat="server" Text='<%#Eval("ItemCode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblItem" Width="300px" runat="server" Text='<%#Eval("StockItemName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity">
                                <ItemTemplate>
                                    <asp:Label ID="lblQty" Width="100px" runat="server" Text='<%#Eval("Qty")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="Price">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrice" Width="150px" runat="server" Text='<%#Eval("Price")%>'></asp:Label>
                                </ItemTemplate>
                                <footertemplate>
                                    <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#777777">
                                        <tr>
                                            <td align="right" style="color:blue">Total</td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="color:blue">Delivery Charges</td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="color:blue">Grand Total</td>
                                        </tr>
                                    </table>
                                </footertemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" Width="150px" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSubTotal" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDCharges" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNet" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:TemplateField>                            
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" DaysModeTitleFormat="dd,MMMM,yyyy"
            Format="dd-MM-yyyy" TargetControlID="txtDisputeDate" TodaysDateFormat="dd MM yyy">
        </cc1:CalendarExtender>        
    </div>
</asp:Content>
