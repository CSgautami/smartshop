﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Configuration;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class PriceList : GlobalPage
    {
        PriceList_BLL Obj_BLL = new PriceList_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Clear();
                Response.Write("Your current session expired. Please Login Again.");
                Response.End();
            }


            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    trNav.Visible = false;
                GetDefaults();
                hidUserId.Value = Session["UserID"].ToString();
            }

        }
        public void GetDefaults()
        {
            PriceList_BLL Obj_BLL = new PriceList_BLL();
            ddlTaxSystem.DataSource = Obj_BLL.GetTaxSystem();
            ddlTaxSystem.DataTextField = "TaxName";
            ddlTaxSystem.DataValueField = "TaxNo";
            ddlTaxSystem.DataBind();
            ddlItem.DataSource = Obj_BLL.GetItemCreation();
            ddlItem.DataTextField = "StockItemName";
            ddlItem.DataValueField = "ICID";
            ddlItem.DataBind();
            ddlTaxSystem.Items.Insert(0, new ListItem("Select", "0"));
            ddlItem.Items.Insert(0, new ListItem("Select", "0"));

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            string res = Obj_BLL.SavePriceList(ddlItem.SelectedValue, txtMRP.Text, txtPurchasePrice.Text, txtSalesPrice.Text, ddlTaxSystem.SelectedValue, hidUserId.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Price";
                ddlItem.Focus();
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Price already exists.";
                ddlItem.Focus();
            }
            else if (res == "1")
            {
                lblMsg.Text = "Price saved successfully.";
                ddlItem.SelectedIndex = 0;
                txtMRP.Text = "";
                txtPurchasePrice.Text = "";
                txtSalesPrice.Text = "";
                ddlTaxSystem.SelectedIndex = 0;
                ddlItem.Focus();
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPLNavData(string UserId, string PLID, string Flag)
        {
            try
            {
                PriceList_BLL bll_PriceList = new PriceList_BLL();
                return bll_PriceList.GetPLNavData(UserId, PLID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string res = Obj_BLL.UpdatePriceList(hidPLID.Value, ddlItem.SelectedValue, txtMRP.Text, txtPurchasePrice.Text, txtSalesPrice.Text, ddlTaxSystem.SelectedValue, hidUserId.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Price";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Price already exists.";
            }
            else if (res == "1")
            {
                lblMsg.Text = "Price Update successfully.";
                ddlItem.SelectedIndex = 0;
                txtMRP.Text = "";
                txtPurchasePrice.Text = "";
                txtSalesPrice.Text = "";
                ddlTaxSystem.SelectedIndex = 0;

            }
        }
    }
}
            
        
    
        

           

    
