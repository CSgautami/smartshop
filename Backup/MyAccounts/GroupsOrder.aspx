﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="GroupsOrder.aspx.cs" Inherits="MyAccounts20.GroupsOrder" Title="Groups Order" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" cellpadding ="5px" cellspacing="5px" style="width:800px;">
            <tr>
                <td style="width:100px;">&nbsp;</td>
                <td align="right">
                    Head Group  :
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlHeadGroup" runat="server" Width="200px" 
                        onselectedindexchanged="ddlHeadGroup_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:GridView ID="gvGroup" runat="server" AutoGenerateColumns="false" >    
                        <Columns>                                                   
                            <asp:TemplateField HeaderText ="SGName" ItemStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <input type="hidden" id="hidSGNo" runat="server" value='<% #Eval("SGNo") %>' />
                                    <asp:Label  ID="lblSGName" Width="150px" runat="server" Text='<% #Eval("SGName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText ="Order ID" >
                                <ItemTemplate  >
                                    <asp:TextBox ID="txtOrder" Width="150px" runat="server" Text ='<% #Eval("OrderID") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>                 
                     </asp:GridView>                                                     
                </td>
             </tr>
             <tr>
                <td colspan="3">
                    <asp:Label ID="lblMsg" Text="" runat="server" Visible="false" ForeColor = "Red"></asp:Label>
                </td>
             </tr>
             <tr>
                <td colspan="3">
                    <asp:Button ID="btnSave" Text="Save" runat="server" onclick="btnSave_Click" />
                </td>
             </tr>
        </table>
    </div>
</asp:Content>
