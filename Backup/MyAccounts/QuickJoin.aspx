﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="QuickJoin.aspx.cs" Inherits="MyAccounts20.QuickJoin" Title="QuickJoin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Calendar.css" rel="stylesheet" type="text/css" />

    <script src="JS/CalendarPopup.js" type="text/javascript"></script>

    <script src="JS/QuickJoin.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

 document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
       // var todate=document.getElementById("txtDate");
        //todate.value=new Date().format("dd-MM-yyyy");
    }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table>
            <tr style="height:30px">
            </tr>
            <tr>
                <td align="left">
                    Date:
                </td>
                <td align="left">
                    <input type="text" runat="server" id="txtDate" maxlength="10" onfocus="showcalendar(this);" onclick="showcalendar(this);"
                        onkeydown="HideCalendar(event);" style="width: 100px" tabindex="-1" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    Pin No:
                </td>
                <td>
                    <asp:TextBox ID="txtPinNo" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    ReferenceID:
                </td>
                <td>
                    <input type="text" id="txtrefid" runat="server" onblur="GetRefName();" />
                    <%--<asp:TextBox ID="txtRefID" runat="server" OnTextChanged="txtRefID_TextChanged" 
                        AutoPostBack="True"></asp:TextBox>--%>
                </td>
                <td>
                    <asp:Label ID="lblRefName" runat="server"></asp:Label>
                </td>
            </tr>
            <%--<tr>
                <td align="left">
                    Pin No:
                </td>
                <td>
                    <asp:TextBox ID="txtPinNo" runat="server"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblMassege" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Button ID="btnGO" runat="server" Text="GO" OnClick="btnGO_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
</asp:Content>
