﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.Collections.Generic;
namespace MyAccounts20
{
    public partial class Purchase : GlobalPage
    {
        Purchase_BLL obj_BLL = new Purchase_BLL();
        DataTable dt = new DataTable();
        //public static string strID;
        //public static string BranchID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            //cal.select(t,t.id,"dd-MM-yyyy");
            string strID = Request.QueryString["ID"];
            string BranchID = Request.QueryString["BranchID"];            
            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    trNav.Visible = false;
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                hidUInvID.Value = "-1";
                hidPInvID.Value = strID;
                // GridHeadings();               
                ddlMode.Items.Insert(0, new ListItem("Cash", "Cash"));
                ddlMode.Items.Insert(1, new ListItem("Credit", "Credit"));
                //txtInvNo.Focus();     
                //string flag="";
                if (strID != null && strID != "")
                {
                    trHeading.Visible = false;
                    trMenu.Visible = false;
                    trNav.Visible = false;
                    tdCancel.Visible = false;
                    //tdDelete.Visible = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + strID + "," + BranchID  + ");", true);
//                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData(" + strID + ");", true);
                    strID = "";
                }
            }
        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        void GridHeadings()
        {
            dt.Columns.Add("Stock Item");
            dt.Columns.Add("Batch No");
            dt.Columns.Add("Qty");
            dt.Columns.Add("MRP");
            dt.Columns.Add("Price");
            dt.Columns.Add("Disc");
            dt.Columns.Add("C.Disc");
            dt.Columns.Add("Tax System");
            dt.Columns.Add("Unit Price");
            dt.Columns.Add("Amount");
            dt.Columns.Add("Vat Amount");
            //gvDetails.DataSource = dt;
            //gvDetails.DataBind();
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerName(string BranchID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetLedgerName(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetLedgerDetails(string LedgerID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetLedgerDetails(LedgerID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPriceAndMRP(string ItemID, string BatchNo)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetPriceAndMRP(ItemID, BatchNo).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAccLedger(string BranchID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetAccLedger(BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem()
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetStockItem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetTaxSystem()
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetTaxSystem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveInvoice(string hidInvID, string hidUInvID, string InvNo, string Date, string PurInvNo, string PurDate, string LNo, string Address, string Mode, string Days, string Note, string AcLNo, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID,string UserID, string XmlString)
        {
            try
            {
                string res;
                Purchase_BLL obj_BLL = new Purchase_BLL();
                res = obj_BLL.SaveInvoice(hidInvID, hidUInvID, InvNo, dateFormat(Date), PurInvNo, dateFormat(PurDate), LNo, Address, Mode, Days, Note, AcLNo, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID ,UserID, XmlString);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static decimal GetTax(string tax)
        {
            decimal nTax;
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                nTax = Convert.ToDecimal(obj_BLL.GetTax(tax).Tables[0].Rows[0]["vatrate"].ToString());
            }
            catch
            {
                nTax = 0;
            }
            return nTax;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWiseTaxSystem(string ItemID)
        {
            //int nTaxNo;
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                //nTaxNo =Convert.ToInt32(obj_BLL.GetItemWiseTaxSystem(ItemID).Tables[0].Rows[0]["TaxNo"].ToString());
                return obj_BLL.GetItemWiseTaxSystem(ItemID).GetXml();
            }
            catch
            {
                //nTaxNo = 0;
                return "";
            }
            //return nTaxNo;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string BranchID, string InvID, string Flag)
        {
            try
            {
                Purchase_BLL obj_Bll = new Purchase_BLL();
                return obj_Bll.GetData(BranchID, InvID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string Delete(string InvID,string BranchID)
        {
            try
            {
                Purchase_BLL obj_BAL = new Purchase_BLL();
                return obj_BAL.DeletePurchase(InvID,BranchID);
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string Testing()
        {
            string res = "hai";
            return res;
        }
        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetInvMaxID(string UserID)
        //{
        //    string nInvID;
        //    try
        //    {
        //        Purchase_BLL obj_Bll = new Purchase_BLL();
        //        nInvID = (obj_Bll.GetInvMaxID(UserID).Tables[0].Rows[0]["MInvID"]).ToString();
        //        //  nInvID = "0";
        //    }
        //    catch (Exception ex)
        //    {
        //        //return ex.Message;
        //        nInvID = "0";
        //    }
        //    return nInvID;
        //}
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetSearchInvoiceData(string BranchID, string InvID)
        {
            try
            {
                Purchase_BLL Obj_BLL = new Purchase_BLL();
                return Obj_BLL.GetSearchInvoiceData(BranchID, InvID).GetXml();
           
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWithBarCode(string BarCode)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWithBarCode(BarCode).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBatchNo(string ItemID, string BranchID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetBatchNo(ItemID, BranchID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
