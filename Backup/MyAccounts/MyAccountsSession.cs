﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
namespace MyAccounts20
{
    public static class MyAccountsSession
    {
        public static string UserID = "UserID";
        public static string UserName = "UserName";
        public static string UserCart = "UserCart";
        public static string TempOrderID = "TOID";
        public static string OrderID = "OrderID";
        public static string UserPermissionPages(string UserID)
        {
            Login_BLL bllLogin = new Login_BLL();
            string res = bllLogin.CheckAuthorization(UserID, HttpContext.Current.Request.Url.AbsolutePath.Substring(1).ToLower());
            //if (res != "1")
            //{
            //    Response.Redirect("Login.aspx", true);
            //}
            //Response.Expires = -1500;

            //Response.AddHeader("Pragma", "no-cache");
            //Response.AddHeader("cache-control", "private");

            //Response.CacheControl = "no-cache";

            //Response.Cache.SetExpires(DateTime.Now.Date.AddDays(-100));

            //Response.Cache.SetNoServerCaching();

            //Response.ExpiresAbsolute = DateTime.Now.AddDays(-100);

            //Response.Cache.SetNoStore();

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);

            //Response.Cache.SetValidUntilExpires(false);


            //base.OnLoad(e);
            return res;
        }
    }
}
