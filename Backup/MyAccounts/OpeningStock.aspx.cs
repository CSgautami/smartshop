﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class OpeningStock : GlobalPage
    {
        OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            if (hidbinditems.Value == "1")
            {
                hidbinditems.Value = "";
                btnBindItem_Click();
            }

            if (!IsPostBack)
            {
                //tdItem.Focus();
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                //GetDefaults();
                //FillGrid(ddlbranch.SelectedValue);


            }
        }

        //private void FillGrid(string bID)
        //{
        //    try
        //    {
        //        DataSet ds = new DataSet();
        //        ds = Obj_BLL.GetOpeningStock(bID);
        //        if (ds != null)
        //        {
        //            if (ds.Tables[0].Rows.Count == 0)
        //            {
        //                DataRow dr = ds.Tables[0].NewRow();
        //                dr[0] = DBNull.Value;
        //                ds.Tables[0].Rows.Add(dr);
        //                gvOpeningStock.DataSource = ds;
        //                gvOpeningStock.DataBind();
        //                gvOpeningStock.Rows[0].Cells[5].Text = "";
        //            }
        //            else
        //            {
        //                gvOpeningStock.DataSource = ds;
        //                gvOpeningStock.DataBind();
        //            }
        //           // ddlbranch_selectedindexchanged(this,EventArgs.Empty);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string BranchID)
        {
            try
            {
                OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
                return Obj_BLL.GetData(BranchID).GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetBranch(string UserID)
        //{
        //    try
        //    {
        //        Ledger_BLL obj_BAL = new Ledger_BLL();
        //        return obj_BAL.GetBranch(UserID).GetXml();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        private void GetDefaults()
        {
            Purchase_BLL obj_BLLBranch = new Purchase_BLL();
            ddlbranch.DataSource = obj_BLLBranch.GetBranch(Session["UserID"].ToString());
            ddlbranch.DataTextField = "BranchName";
            ddlbranch.DataValueField = "BranchID";
            ddlbranch.DataBind();
            ddlbranch.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["GBID"] != null)
                ddlbranch.SelectedIndex = Convert.ToInt32(Session["GBID"].ToString());
            //OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
            //ddlitem.DataSource = Obj_BLL.GetItem();
            //ddlitem.DataTextField = "ItemCode";
            //ddlitem.DataValueField = "ICID";
            //ddlitem.DataBind();            
            //ddlitem.Items.Insert(0, new ListItem("Select", "0"));
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveOpeningStock(string BranchID, string Item, string BatchNo, string MRP, string Price, string Quantity,string UserID)
        {
            try
            {
                string res;
                OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
                res = Obj_BLL.SaveOpeningStock(BranchID, Item, BatchNo, MRP, Price, Quantity,UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string UpdateOpeningStock(string StockID, string BranchID, string Item, string BatchNo, string MRP, string Price, string Quantity, string UserID)
        {
            try
            {
                string res;
                OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
                res = Obj_BLL.UpdateOpeningStock(StockID, BranchID, Item, BatchNo, MRP, Price, Quantity,  UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }



        //protected void btnSave_Click(object sender, EventArgs e)
        //{            
        //    string res = Obj_BLL.InsertOpeningStock(ddlbranch.SelectedValue,tdItem.Text, txtbatchno.Text, txtmrp.Text, txtprice.Text,txtFreeQuantity.Text, txtquantity.Text, hidUserID.Value);
        //    if (res == "0" || res == "")
        //    {
        //        lblMsg.Text = "Error while saving Stock";
        //        tdItem.Focus();
        //        return;
        //    }
        //    else if (res == "-1")
        //    {
        //        lblMsg.Text = "Stock already existed";
        //        tdItem.Focus();
        //        return;
        //    }
        //    else if (res == "1")
        //    {
        //        lblMsg.Text = "Stock saved successfully";
        //        FillGrid(ddlbranch.SelectedValue);
        //ddlbranch.SelectedIndex = 0;
        //ddlitem.SelectedIndex = 0;
        //        txtbatchno.Text = "";
        //        txtmrp.Text = "";
        //        txtprice.Text = "";
        //        txtFreeQuantity.Text = "";
        //        txtquantity.Text = "";
        //        ddlbranch.Focus();

        //    }
        //}

        //protected void ddlbranch_selectedindexchanged(object sender, EventArgs e)
        //{
        //    FillGrid(ddlbranch.SelectedValue);
        //}

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    string res = Obj_BLL.UpdateOpeningStock(ddlbranch.SelectedValue, hidStockID.Value, hidICID.Value,txtBatchNo.Text, txtMRP.Text, txtPrice.Text, txtQuantity.Text, txtFreeQuantity.Text, hidUserID.Value);
        //    if (res == "0" || res == "")
        //    {
        //        lblMsg.Text = "Error while Updating Stock";

        //    }
        //    else if (res == "-1")
        //    {
        //        lblMsg.Text = "Stock already existed";

        //    }
        //    else if (res == "1")
        //    {
        //        lblMsg.Text = "Stock Updated successfully";
        //        //FillGrid(ddlbranch.SelectedValue);
        //        //ddlbranch.SelectedIndex = 0;
        //        //ddlitem.SelectedIndex = 0;
        //        txtBatchNo.Text = "";
        //        txtMRP.Text = "";
        //        txtPrice.Text = "";                
        //        txtQuantity.Text = "";
        //        txtFreeQuantity.Text = "";
        //        btnUpdate.Visible = false;
        //        //btnSave.Visible = true;
        //        ddlbranch.Focus();

        //    }
        //}
        //protected void btnCancel_Click(object sender, EventArgs e)
        //{

        //    //gvOpeningStock.PageIndex = 0;
        //    btnUpdate.Visible = false;
        //    //btnSave.Visible = true;
        //    //ddlbranch.SelectedIndex = 0;
        //    //ddlitem.SelectedIndex = 0;
        //    txtBatchNo.Text = "";
        //    txtMRP.Text = "";
        //    txtPrice.Text = "";
        //    txtQuantity.Text = "";
        //    ddlbranch.Focus();
        //    lblMsg.Text = "";
        //}
        //public void lbtnEdit_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //       // GetDefaults();
        //       // btnBindItem_Click();
        //        LinkButton lbtn = (LinkButton)sender;
        //        GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
        //        hidStockID.Value = ((HtmlInputHidden)grow.FindControl("hidStockID")).Value;
        //        hidICID.Value = ((HtmlInputHidden)grow.FindControl("hidICID")).Value;
        //      //  ddlitem.SelectedItem.Text= ((Label)grow.FindControl("lblItem")).Text;
        //        lblItem.Text = ((Label)grow.FindControl("lblItem")).Text;
        //        tdItem.Text = ((Label)grow.FindControl("lblItemCode")).Text;
        //        txtBatchNo.Text = ((Label)grow.FindControl("lblBatchNo")).Text;
        //        txtMRP.Text = ((Label)grow.FindControl("lblMRP")).Text;
        //        txtPrice.Text = ((Label)grow.FindControl("lblPrice")).Text;                
        //        txtQuantity.Text = ((Label)grow.FindControl("lblQuantity")).Text;
        //        txtFreeQuantity.Text = ((Label)grow.FindControl("lblFQty")).Text;
        //        tdItem.Focus();
        //        btnUpdate.Visible = true;
        //        //btnSave.Visible = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        //public void lbtnDelete_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LinkButton lbtn = (LinkButton)sender;
        //        GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
        //        hidStockID.Value = ((HtmlInputHidden)grow.FindControl("hidStockID")).Value;
        //        OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
        //        string res = Obj_BLL.DeleteOpeningStock(hidStockID.Value);
        //        if (res == "" || res == "0")
        //        {
        //            lblMsg.Text = "Error While Deleting Stock";
        //            return;

        //        }
        //        if (res == "1")
        //        {
        //            lblMsg.Text = "Stock Deleted Successfully";
        //            //ddlbranch.SelectedIndex = 0;
        //           // ddlitem.SelectedIndex = 0;
        //            txtBatchNo.Text = "";
        //            txtMRP.Text = "";
        //            txtPrice.Text = "";
        //            txtQuantity.Text = "";
        //            txtFreeQuantity.Text = "";
        //            tdItem.Focus();
        //            //btnSave.Visible = true;
        //            btnUpdate.Visible = false;
        //            btnCancel.Visible = true;
        //            //FillGrid(ddlbranch.SelectedValue);

        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //[System.Web.Services.WebMethod()]
        //[System.Web.Script.Services.ScriptMethod()]
        //public static string GetItems()
        //{
        //    try
        //    {
        //        OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
        //        return Obj_BLL.GetItem().GetXml();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public void btnBindItem_Click()
        {
            try
            {
                OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
                //ddlitem.DataSource = Obj_BLL.GetItem();
                //ddlitem.DataTextField = "StockItemName";
                //ddlitem.DataValueField = "ICID";
                //ddlitem.DataBind();
                //ddlitem.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemName(string ItemID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWiseTaxSystem(ItemID).GetXml();
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnDelete_Click(string StockID, string BranchID)
        {
            try
            {
                OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
                return Obj_BLL.DeleteOpeningStock(StockID, BranchID);
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem()
        {
            try
            {
                OpeningStock_BLL Obj_BLL = new OpeningStock_BLL();
                return Obj_BLL.GetStockItem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                OpeningStock_BLL obj_BAL = new OpeningStock_BLL();
                return obj_BAL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}


        
    



        
        
        
    

        
    

