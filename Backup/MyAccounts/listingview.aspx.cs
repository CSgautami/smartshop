﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;


namespace MyAccounts20
{
    public partial class listingview : System.Web.UI.Page
    {
        string HID = "", CID = "", ser = "";
        int pageindexchangecount = 0;
        int PageSize = 20;
        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                    hidUserID.Value = Session["UserID"].ToString();
                if (Session[MyAccountsSession.TempOrderID] != null)
                    hidTempID.Value = Session[MyAccountsSession.TempOrderID].ToString();
                if (Request.QueryString["HID"] != null) Session["HID"] = Request.QueryString["HID"].ToString();
                if (Request.QueryString["CID"] != null) Session["CID"] = Request.QueryString["CID"].ToString();
                GetItems();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);                                        
            }
        }
        void GetItems()
        {
            ItemCreation_BLL obj_BLL = new ItemCreation_BLL();
            if (Session["Search"] != null) ser = Session["Search"].ToString();
            if (Session["CID"] != null) CID = Session["CID"].ToString();
            if (Session["HID"] != null) HID = Session["HID"].ToString();
            DataSet ds = obj_BLL.GetItemsToDisplay(HID, CID, ser);
            lblHeadGroup.Text = "";
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int currentpage = 0, count = 0;
                    lblHeadGroup.Text = ds.Tables[0].Rows[0]["HeadGroupName"].ToString();
                    dlItemDet.DataSource = ds;
                    dlItemDet.DataBind();
                    dlPager.Ods = ds;
                    dlPager.ObjectControl = dlItemDet;
                    dlPager.PageSize = PageSize;
                }

            }
            Session["Search"] = null;
        }
        protected void dlPager_PageIndexChagned(object sender, EventArgs e)
        {
            GetItems();
            if (pageindexchangecount == 0)
            {
                pageindexchangecount++;
                //dlPager1_PageIndexChagned(sender, e);
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveTempOrder(string ItemID, string Qty, string BathNo, string TempID, string UserID)
        {
            try
            {
                string res;
                DataSet ds;
                ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
                ds = obj_BLL.SaveTempOrder(ItemID, Qty, BathNo, TempID, UserID);
                res = ds.Tables[0].Rows[0]["TOID"].ToString();
                HttpContext.Current.Session[MyAccountsSession.TempOrderID] = res;
                return ds.GetXml();
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string TempID)
        {
            try
            {
                ShopingCart_BLL obj_Bll = new ShopingCart_BLL();
                return obj_Bll.GetData(TempID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeleteTempOrder(string ItemID, string TOID)
        {
            try
            {
                ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
                return obj_BLL.DeleteTempOrder(ItemID, TOID).GetXml();
            }
            catch
            {
                throw;
            }
        }
        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            if (Session[MyAccountsSession.TempOrderID] != null && Session["UserID"] != null)
            {
                Response.Redirect("ConfirmPage.aspx", true);
            }
            else
            {
                Response.Redirect("Login.aspx", true);
            }
        }

    }
}


