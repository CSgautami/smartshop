﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="PinsGenerate.aspx.cs" Inherits="MyAccounts20.PinsGenerate" Title="PinsGenerate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/PinGenerate.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center">
            <tr>
                <td>&nbsp</td>
            </tr>
            <tr>
                <td>Amount Rs.</td>
                <td>
                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="10"  Width="100px" OnBlur="GetNoofPins();"></asp:TextBox>
                </td>
                <td>@</td>
                <td>
                    <asp:TextBox ID="txtValue" runat="server" MaxLength="5" Width="100px" OnBlur="GetNoofPins();"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:CheckBox ID="chkZeroValued" runat="server" Text="Zero Valued" onclick="chkZeroRated_Click();" />
                </td>
                <td>NoOfPins</td>
                <td>
                    <asp:TextBox ID="txtNoofPins" runat="server" Width="100px" MaxLength="5"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp</td>
            </tr>
            <tr>
                <td colspan ="4" align="center">
                    <%--<input type ="button" id="btnSave" value="Generate" onclick="btnSave_Click(event,this);" />--%>
                    <input type="button" id="btnSave" value="Generate" onclick="btnSave_Click(event,this);" />
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="hidUserID" runat="server" />
</asp:Content>
