﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class PriceList_BLL
    {
        PriceList_DAL Obj_DAL = new PriceList_DAL();
        public string SavePriceList(string ItemID, string MRP, string PurchasePrice, string SalesPrice, string TaxSystem, string UserId)
        {
            return Obj_DAL.SavePriceList(ItemID, MRP, PurchasePrice, SalesPrice, TaxSystem, UserId);
        }
        public DataSet GetTaxSystem()
        {
            return Obj_DAL.GetTaxSystem();
        }
        public DataSet GetItemCreation()
        {
            return Obj_DAL.GetItemCreation();
        }
        public DataSet GetPLNavData(string UserId, string PLID, string Flag)
        {
            return Obj_DAL.GetPLNavData(UserId, PLID , Flag);
        }
        public string UpdatePriceList(string PLID, string ItemID, string MRP, string PurchasePrice, string SalesPrice, string TaxSystem, string UserId)
        {
            return Obj_DAL.UpdatePriceList(PLID,ItemID,MRP,PurchasePrice,SalesPrice,TaxSystem,UserId);
        }
        //public DataSet GetCompany()
        //{
        //    return Obj_DAL.GetCompany();
        //}
        public DataSet GetStockGroup()
        {
            return Obj_DAL.GetStockGroup();
        }
        public string SavePriceDetails(string ItemID, string BatchNo, string PurchasePrice, string MRP, string MRPMargin, string Retail, string RetailMargin, string Customer, string CustomerMargin, string VIP, string VIPMargin, string Disc, string CDisc,string UserID)
        {
            return Obj_DAL.SavePriceDetails(ItemID, BatchNo,PurchasePrice, MRP, MRPMargin, Retail, RetailMargin, Customer, CustomerMargin, VIP, VIPMargin, Disc, CDisc,UserID);
        }

        public DataSet GetData(string SearchItem,string GroupID)
        {
            return Obj_DAL.GetData(SearchItem, GroupID);
        }
        public DataSet GetItemPriceDet(string ItemID)
        {
            return Obj_DAL.GetItemPriceDet(ItemID);
        }

        public string DeletePriceList(string ItemID, string BatchNo)
        {
            return Obj_DAL.DeletePriceList(ItemID, BatchNo);
        }
    }
}
   
       
    


