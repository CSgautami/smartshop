﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class SubCategory_BLL
    {
        SubCategory_DAL Obj_DAL = new SubCategory_DAL();

        public DataSet GetData()
        {
            return Obj_DAL.GetData();
        }

        public string SaveSubCategory(string SubCategoryID, string SubCategory, string CategoryID)
        {
            return Obj_DAL.SaveSubCategory(SubCategoryID, SubCategory,CategoryID);
        }

        public string DeleteSubCategory(string SubCategoryID)
        {
            return Obj_DAL.DeleteSubCategory(SubCategoryID);
        }
    }
}
