﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;
namespace BLL
{    
    public class PinGenerate_BLL
    {
        PinGenerate_DAL obj_DAL = new PinGenerate_DAL();

        public string SavePinGeneration(string Amount, string Value, string ZeroValue, string NoofPins, string UserID)
        {
            return obj_DAL.SavePinGeneration(Amount, Value, ZeroValue, NoofPins, UserID);
        }
        public DataSet GetCustomer()
        {
            return obj_DAL.GetCustomer();
        }
        public DataSet GetPins()
        {
            return obj_DAL.GetPins();
        }
        public string SavePins(string LedgerID, string xmlDet, string UserID)
        {
            return obj_DAL.SavePins(LedgerID ,xmlDet,UserID);
        }
    }
}
