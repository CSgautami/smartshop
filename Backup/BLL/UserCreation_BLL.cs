﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class UserCreation_BLL
    {
        UserCreation_DAL Obj_DAL = new UserCreation_DAL();
        public string CreateUser(string UserName, string Password,string BranchID)
        {
            return Obj_DAL.CreateUser(UserName, Password,BranchID);
        }
        public DataSet GetBranch()
        {
            return Obj_DAL.GetBranch();
        }
        public DataSet CheckUserCreation(string UserName, string Password,string Type)
        {
            return Obj_DAL.CheckUserCreation(UserName, Password,Type);
        }
        
    }
}
