﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;


namespace BLL
{
    public class VatReturn_BLL
    {
        VatReturn_DAL Obj_DAL = new VatReturn_DAL();
        public DataSet GetVatReturn(string BranchID, string FromDate, string ToDate, string Refresh)
        {
            return Obj_DAL.GetVatReturn(BranchID, FromDate, ToDate, Refresh);
        }
        public string SaveVatReturn(string VATReturnNo, string fromDate, string ToDate, string ExemptPurch, string FourPPurch, string FourPPurchVat,
           string StdRatePurch, string StdRatePurchVat, string OtherVatPurch, string OtherVatPurchVat, string SatPurch, string SatPurchVat, string ExemptSales,
           string ZeroRateSalesIE, string ZeroRateSalesOther, string TaxDue, string TaxDueVat, string FourPRateSales, string FourPRateSalesVat,
           string StdRateSales, string StdRateSalesVat, string SatSales, string SatSalesVat, string OtherVatRateSales, string OtherVatRateSalesVat,
           string TotalAmoutToPay, string BranchId, string Paid, string Refund, string CarredForward, string box24a, string box24b, string CSTCF,
           string StdRate14P5Purch, string StdRate14P5PurchVat, string StdRate14P5Sales, string StdRate14P5SalesVat,
           string FivePPurch, string FivePPurchVat, string FivePRateSales, string FivePRateSalesVat)
        {
            return Obj_DAL.SaveVatReturn(VATReturnNo, fromDate, ToDate, ExemptPurch, FourPPurch, FourPPurchVat, StdRatePurch, StdRatePurchVat, OtherVatPurch, OtherVatPurchVat, SatPurch
                  , SatPurchVat, ExemptSales, ZeroRateSalesIE, ZeroRateSalesOther, TaxDue, TaxDueVat, FourPRateSales, FourPRateSalesVat, StdRateSales, StdRateSalesVat
                  , SatSales, SatSalesVat, OtherVatRateSales, OtherVatRateSalesVat, TotalAmoutToPay, BranchId, Paid, Refund
                  , CarredForward, box24a, box24b, CSTCF, StdRate14P5Purch, StdRate14P5PurchVat, StdRate14P5Sales, StdRate14P5SalesVat, FivePPurch
                  , FivePPurchVat, FivePRateSales, FivePRateSalesVat);


        }
        public string SavePay(string VRNo, string BranchID, string XmlString)
        {
            return Obj_DAL.SavePay(VRNo, BranchID, XmlString);
        }
        public string SaveAdj(string VRNo, string BranchID, string XmlString)
        {
            return Obj_DAL.SaveAdj(VRNo, BranchID, XmlString);
        }
        public DataSet GetVatReturnPrintData(string SRNo, string BranchID)
        {
            return Obj_DAL.GetVatReturnPrintData(SRNo, BranchID);
        }
    }
}
