﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class ADDAddress_BLL
    {
        ADDAddress_DAL Obj_DAL = new ADDAddress_DAL();
        public string InsertAddress(string UserID,string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark,string Zone)
        {
            return Obj_DAL.InsertAddress(UserID,Name, HNO, SOne, STwo, Area, City, State, Mark,Zone);
        }



        public string UpdateAddress(string SID,string Name, string HNO, string SOne, string STwo, string Area, string City, string State, string Mark, string Zone)
        {
            return Obj_DAL.UpdateAddress(SID,Name, HNO, SOne, STwo, Area, City, State, Mark, Zone);
        }
    }
}
