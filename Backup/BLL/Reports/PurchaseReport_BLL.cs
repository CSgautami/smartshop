﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL.Reports;

namespace BLL.Reports
{
    public class PurchaseReport_BLL
    {
        PurchaseReport_DAL Obj_DAL = new PurchaseReport_DAL();
        public DataSet PurchaseReportBetweenDates(string Branch, string FromDate, string ToDate,string Mode, string Name,string UserID)
        {
            return Obj_DAL.PurchaseReportBetweenDates(Branch , FromDate, ToDate, Mode, Name,UserID);
        }
        public DataSet GetStockGroup()
        {
            return Obj_DAL.GetStockGroup();
        }

        public DataSet GetCompany()
        {
            return Obj_DAL.GetCompany();
        }

        public DataSet GetPurchaseReportGroupAndComWise(string Branch, string FromDate, string ToDate, string Group, string Company, string UserID)
        {
            return Obj_DAL.GetPurchaseReportGroupAndComWise(Branch, FromDate, ToDate, Group, Company, UserID);
        }

        public DataSet GetPurchaseReportInvWise(string Branch, string FromDate, string ToDate, string FromInv, string ToInv, string UserID)
        {
            return Obj_DAL.GetPurchaseReportInvWise(Branch , FromDate, ToDate, FromInv, ToInv, UserID);
        }

        public DataSet GetPurchaseReportProductWise(string Branch, string FromDate, string ToDate, string Product,string UserID)
        {
            return Obj_DAL.GetPurchaseReportProductWise(Branch , FromDate, ToDate, Product,UserID);
        }
        public DataSet GetStockItem()
        {
            return Obj_DAL.GetStockItem();
        }
        public DataSet GetHeadGroup()
        {
            return Obj_DAL.GetHeadGroup();
        }  
    }
}
