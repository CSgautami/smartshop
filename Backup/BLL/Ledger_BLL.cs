﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BLL
{
    public class Ledger_BLL
    {
        Ledger_DAL obj_DAL = new Ledger_DAL();

        //public DataSet GetGroupName(string BranchID)
        //{
        //    return obj_DAL.GetGroupName(BranchID);
        //}
        public string SaveLedger(string YourID,string Password, string BranchID,string LedgerName,string RefNo,string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone,string APGST,string Mode, string Area,string Sales,string Purchase,string UserId,string PinNo)
        {
            return obj_DAL.SaveLedger(YourID,Password, BranchID,LedgerName,RefNo,LandMark , GroupID, Status, Amount, Address, Town, Phone,APGST,Mode , Area ,Sales,Purchase,UserId,PinNo);
        }

        //public string UpdateLedger(string LedgerNo, string YourID, string Password, string BranchID, string LedgerName, string RefNo, string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserId)
        public string UpdateLedger(string LedgerID, string LedgerNo, string Password, string BranchID, string LedgerName, string LandMark, string GroupID, string Status, string Amount, string Address, string Town, string Phone, string APGST, string Mode, string Area, string Sales, string Purchase, string UserId)
        {
            //return obj_DAL.UpdateLedger(LedgerNo, YourID, Password, BranchID, LedgerName, RefNo, LandMark, GroupID, Status, Amount, Address, Town, Phone, APGST, Mode, Area, Sales, Purchase, UserId);
            return obj_DAL.UpdateLedger(LedgerID, LedgerNo, Password, BranchID, LedgerName, LandMark, GroupID, Status, Amount, Address, Town, Phone, APGST, Mode, Area, Sales, Purchase, UserId);
        }
        public DataSet GetBranch(string UserID)
        {
            return obj_DAL.GetBranch(UserID);
        }
        public DataSet GetGroups(string BranchID)
        {
            return obj_DAL.GetGroups(BranchID);
        }
        //public DataSet GetSearchData(string Search)
        //{
        //    return obj_DAL.GetSearch(Search);
        //}



        public DataSet GetLedgerDisplayData(string BranchID, string FromDate, string ToDate, string LedgerID)
        {
            DataSet ds = obj_DAL.GetLedgerDisplayData(BranchID, FromDate, ToDate, LedgerID);
            decimal bal = 0;
            decimal crAmt = 0;
            decimal drAmt = 0;
            int cnt = 0;
            if (ds != null && ds.Tables.Count != 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (cnt == 0)
                    {
                        crAmt = (decimal)dr["CrAmount"];
                        drAmt = (decimal)dr["DrAmount"];
                        bal = crAmt - drAmt;
                        if (bal < 0)
                            dr["St"] = "Dr";
                        else
                            dr["St"] = "Cr";
                        cnt++;
                    }
                    else
                    {
                        crAmt = crAmt + (decimal)dr["CrAmount"];
                        drAmt = drAmt + (decimal)dr["DrAmount"];
                        bal = bal + (decimal)dr["CrAmount"] - (decimal)dr["DrAmount"];
                        if (bal < 0)
                        {
                            dr["St"] = "Dr";
                            dr["Balance"] = -1 * bal;
                        }
                        else
                        {
                            dr["Balance"] = bal;
                            dr["St"] = "Cr";
                        }
                    }
                }
                DataRow drow = ds.Tables[0].NewRow();
                drow["Narration"] = "Total:";
                drow["CrAmount"] = crAmt;
                drow["DrAmount"] = drAmt;
                if (bal < 0)
                {
                    drow["St"] = "Dr";
                    drow["Balance"] = -1 * bal;
                }
                else
                {
                    drow["Balance"] = bal;
                    drow["St"] = "Cr";
                }
                DataRow drempty = ds.Tables[0].NewRow();
                drempty["Narration"] = "&nbsp;";
                ds.Tables[0].Rows.Add(drempty);
                ds.Tables[0].Rows.Add(drow);
            }
            return ds;
        }


        public DataSet GetLedgerDisplayLedgers(string BranchID)
        {
            return obj_DAL.GetLedgerDisplayLedgers(BranchID);
        }

        public DataSet GetLMNavData(string Search,string BranchID, string LMNO, string Flag)
        {
            return obj_DAL.GetLMNavData(Search,BranchID, LMNO, Flag);
        }

        public DataSet GetRefMember(string BranchID)
        {
            return obj_DAL.GetRefMember(BranchID);
        }

        public DataSet GetArea()
        {
            return obj_DAL.GetArea();
        }
        public DataSet GetRefName(string LedgerID)
        {
            return obj_DAL.GetRefName(LedgerID);
        }

        public DataSet GetGroupDetails(string GroupID)
        {
            return obj_DAL.GetGroupDetails(GroupID);
        }

        public DataSet GetPins(string RefNo)
        {
            return obj_DAL.GetPins(RefNo);
        }
        public string DeleteLedger(string LedgerID)
        {
            return obj_DAL.DeleteLedger(LedgerID);
        }
    }
}
