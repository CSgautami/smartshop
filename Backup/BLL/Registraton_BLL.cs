﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
   public class Registraton_BLL
    {
       Registraton_DAL obj_DAL = new Registraton_DAL();
        public DataSet GetFilldata()
        {
            return obj_DAL.GetFilldata();
        }

        public string InsertRedg(string RefID,string PinNo,string strRegDate,string Name, string fathet, string DOB, string LandMark, string Hno, string Post, string City, string District, string State, string PinCode,string SHno, string SPost, string SCity, string SDistrict, string SState, string SPinCode, string SLandMark, string PhoneNo, string Mobile, string PanNo, string Email, string Password, string Nominee, string Relation, string BankName, string AccountNo, string IFSCode, string Branch)
        {
            return obj_DAL.InsertRedg(RefID, PinNo, strRegDate, Name, fathet, DOB, LandMark, Hno, Post, City, District, State, PinCode, SHno, SPost, SCity, SDistrict, SState, SPinCode,SLandMark, PhoneNo, Mobile, PanNo, Email, Password, Nominee, Relation, BankName, AccountNo, IFSCode, Branch);
        }
        public string UpdateRedg(string RedgID, string Name, string fathet, string DOB, string LandMark, string Hno, string Post, string City, string District, string State, string PinCode, string SHno, string SPost, string SCity, string SDistrict, string SState, string SPinCode, string SLandMark, string PhoneNo, string Mobile, string PanNo, string Email, string Nominee, string Relation, string BankName, string AccountNo, string IFSCode, string Branch)
        {
            return obj_DAL.UpdateRedg(RedgID, Name, fathet, DOB, LandMark, Hno, Post, City, District, State, PinCode, SHno, SPost, SCity, SDistrict, SState, SPinCode, SLandMark, PhoneNo, Mobile, PanNo, Email, Nominee, Relation, BankName, AccountNo, IFSCode, Branch);
        }
        public DataSet GetDistrict()
        {
            return obj_DAL.GetDistrict();
        }
        public DataSet GetData(string UserID)
        {
            return obj_DAL.GetData(UserID);
        }
    }
}
