﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class DayBookEntry_BLL
    {
        DayBookEntry_DAL dal_DayBookEntry = new DayBookEntry_DAL();
        public DataSet GetDayBookReport(string BranchID, string DateTo)
        {
            return dal_DayBookEntry.GetDayBookReport(BranchID, DateTo);
        }

        public DataSet GetCELedger(string BranchID)
        {
            return dal_DayBookEntry.GetCELedger(BranchID);
        }

        public DataSet CallFillCELedger(string BranchID, string prefix)
        {
            return dal_DayBookEntry.CallFillCELedger(BranchID, prefix);
        }

        public DataSet GetCENavData(string BranchID, string CEID, string Flag)
        {
            return dal_DayBookEntry.GetCENavData(BranchID, CEID, Flag);
        }

        public DataSet GetPENavData(string BranchID, string PEID, string Flag)
        {
            return dal_DayBookEntry.GetPENavData(BranchID, PEID, Flag);
        }

        public DataSet GetRENavData(string BranchID, string REID, string Flag)
        {
            return dal_DayBookEntry.GetRENavData(BranchID, REID, Flag);
        }

        public DataSet GetJENavData(string BranchID, string JEID, string Flag)
        {
            return dal_DayBookEntry.GetJENavData(BranchID, JEID, Flag);
        }

        public DataSet GetGVCELedgersData(string BranchID, string JID, string voch)
        {
            return dal_DayBookEntry.GetGVCELedgersData(BranchID, JID, voch);
        }
        public DataSet GetPayRecLedgers(string BranchID)
        {
            return dal_DayBookEntry.GetPayRecLedgers(BranchID);
        }

        public DataSet GetJournalLedger(string BranchID)
        {
            return dal_DayBookEntry.GetJournalLedger(BranchID);
        }

        public string InsertCashEntry(string CEDate, string LedgerID, string CreditAmount, string DebitAmount, string Narration, string UserID, string BranchID)
        {
            return dal_DayBookEntry.InsertCashEntry(CEDate, LedgerID, CreditAmount, DebitAmount, Narration, UserID, BranchID);
        }

        public string UpdateCashEntry(string CEDate, string LedgerID, string CreditAmount, string DebitAmount, string Narration, string JournalNo, string UserID, string BranchID)
        {
            return dal_DayBookEntry.UpdateCashEntry(CEDate, LedgerID, CreditAmount, DebitAmount, Narration, JournalNo, UserID, BranchID);
        }

        public string InsertPayment(string PaymentDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string PaymentType, string PaymentTypeDesc, string Narration, string UserID, string BranchID)
        {
            return dal_DayBookEntry.InsertPayment(PaymentDate, RefName, ToLedgerID, FromLedgerID, Amount, PaymentType, PaymentTypeDesc, Narration, UserID, BranchID);
        }

        public string UpdatePayment(string PaymentNo, string PaymentDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string PaymentType, string PaymentTypeDesc, string Narration, string UserID, string BranchID)
        {
            return dal_DayBookEntry.UpdatePayment(PaymentNo,PaymentDate, RefName, ToLedgerID, FromLedgerID, Amount, PaymentType, PaymentTypeDesc, Narration, UserID, BranchID);
        }

        public string InsertReceipt(string ReceiptDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string ReceiptType, string ReceiptTypeDesc, string Narration, string UserID, string BranchID)
        {
            return dal_DayBookEntry.InsertReceipt(ReceiptDate, RefName, ToLedgerID, FromLedgerID, Amount, ReceiptType, ReceiptTypeDesc, Narration, UserID, BranchID);
        }

        public string UpdateReceipt(string ReceiptNo, string ReceiptDate, string RefName, string ToLedgerID, string FromLedgerID, string Amount, string ReceiptType, string ReceiptTypeDesc, string Narration, string UserID, string BranchID)
        {
            return dal_DayBookEntry.UpdateReceipt(ReceiptNo, ReceiptDate, RefName, ToLedgerID, FromLedgerID, Amount, ReceiptType, ReceiptTypeDesc, Narration, UserID, BranchID);
        }

        public string InsertJournal(string XmlJournalTable, string JournalDate, string UserID, string BranchID)
        {
            return dal_DayBookEntry.InsertJournal(XmlJournalTable, JournalDate, UserID, BranchID);
        }

        public string UpdateJournal(string JERefNo, string XmlJournalTable, string JournalDate, string UserID, string BranchID)
        {            
            return dal_DayBookEntry.UpdateJournal(JERefNo, XmlJournalTable, JournalDate, UserID, BranchID);
        }
        public string CEDelete(string RefNo, string BranchID)
        {
            return dal_DayBookEntry.CEDelete(RefNo, BranchID);
        }
        public string PaymentDelete(string RefNo, string BranchID)
        {
            return dal_DayBookEntry.PaymentDelete(RefNo, BranchID);
        }
        public string ReceiptDelete(string RefNo, string BranchID)
        {
            return dal_DayBookEntry.ReceiptDelete(RefNo, BranchID);
        }
        public string JournalDelete(string RefNo, string BranchID)
        {
            return dal_DayBookEntry.JournalDelete(RefNo, BranchID);            
        }


        public DataSet CallFillPayAndRecLedger(string BranchID, string prefix)
        {
            return dal_DayBookEntry.CallFillPayAndRecLedger(BranchID, prefix);
        }

        public DataSet CallFillJELedger(string BranchID, string prefix)
        {
            return dal_DayBookEntry.CallFillJELedger(BranchID, prefix);
        }
    }
}
