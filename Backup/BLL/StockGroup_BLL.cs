﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class StockGroup_BLL
    {
        StockGroup_DAL Obj_DAL = new StockGroup_DAL();
        public string InsertStockGroup(string StockGroupName,string UserID)
        {
            return Obj_DAL.InsertStockGroup(StockGroupName,UserID);
        }
        public DataSet GetStockGroup(string Search)
        {
            return Obj_DAL.GetStockGroup(Search);
        }
        public string UpdateStockGroup(string StockGroupID, string StockGroupName,string UserID)
        {
            return Obj_DAL.UpdateStockGroup(StockGroupID,StockGroupName,UserID);
        }
        public string DeleteStockGroup(string StockGroupID)
        {
            return Obj_DAL.DeleteStockGroup(StockGroupID);
        }
        //public DataSet GetHeadGroup()
        //{
        //    return Obj_DAL.GetHeadGroup();
        //}
        //public DataSet GetStockGroupData(string SearchItem)
        //{
        //    return Obj_DAL.GetStockGroupData(SearchItem);
        //}


   }
}
