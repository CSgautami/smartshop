﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class ShippingAddress_BLL
    {
        ShippingAddress_DAL Obj_DAL = new ShippingAddress_DAL();
        public DataSet GetShippingAddress(string UserID)
        {
            return Obj_DAL.GetShippingAddress(UserID);
        }
    }
}
