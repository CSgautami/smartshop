﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class HeadGroup_BLL
    {
        HeadGroup_DAL Obj_DAL = new HeadGroup_DAL();
        public string InsertHeadGroup(string HeadGroupName,string UserID)
        {
            return Obj_DAL.InsertHeadGroup(HeadGroupName,UserID);
        }

        public DataSet GetHeadGroup()
        {
            return Obj_DAL.GetHeadGroup();
        }
        public string UpdateHeadGroup(string HeadGroupID, string HeadGroupName,string UserID)
        {
            return Obj_DAL.UpdateHeadGroup(HeadGroupID, HeadGroupName,UserID);
        }
        public string DeleteHeadGroup(string HeadGroupID)
        {
            return Obj_DAL.DeleteHeadGroup(HeadGroupID);
        }
    }
}
