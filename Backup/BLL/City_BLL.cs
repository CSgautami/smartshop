﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using DAL;

namespace BLL
{
    public class City_BLL
    {
        City_DAL obj_DAL = new City_DAL();
        public string InsertCity(string CountryID, string StateID, string CityName)
        {
            return obj_DAL.InsertCity(CountryID, StateID, CityName);
        }

        public DataSet GetCity(string StateID)
        {
            return obj_DAL.GetCity(StateID);

        }
        public DataSet GetStates()
        {
            return obj_DAL.GetStates();

        }

        public string UpdateCity(string CityID, string CityName)
        {
            return obj_DAL.UpdateCity(CityID, CityName);
        }

        public string DeleteCity(string Cityid)
        {
            return obj_DAL.DeleteCity(Cityid);
        }

        public DataSet CountryChange(string CountryID)
        {
            return obj_DAL.CountryChange(CountryID);
        }
    }
}
