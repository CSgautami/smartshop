﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Packing_BLL
    {
        Packing_DAL Obj_DAL = new Packing_DAL();
        public DataSet GetStockItem()
        {
            return Obj_DAL.GetStockItem();
        }
        public string SavePackDetails( string Name, string xmlDet, string UserID)
        {
            return Obj_DAL.SavePackDetails(Name, xmlDet, UserID);
        }
        public string UpdatePackDetails(string PackID, string Name, string xmlDet, string UserID)
        {
            return Obj_DAL.UpdatePackDetails(PackID, Name, xmlDet, UserID);
        }
        public DataSet GetPDNavData(string PID, string Flag)
        {
            return Obj_DAL.GetPDNavData(PID, Flag);
        }
        public string DeletePack(string PackID)
        {
            return Obj_DAL.DeletePack(PackID);
        }
    }
}
