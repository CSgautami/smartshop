﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BLL
{
    public class Zone_BLL
    {
        Zone_DAL Obj_DAL = new Zone_DAL();
        public string InsertZone(string ZoneNo,string ZoneName, string Address, string Phone, string UserID,string StartID)
        {
            return Obj_DAL.InsertZone(ZoneNo,ZoneName, Address, Phone, UserID,StartID);
        }
        public DataSet GetZone(string strSearch)
        {
            return Obj_DAL.GetZone(strSearch);
        }
        public string UpdateZone(string ZoneID,string ZoneNo,string ZoneName, string Address, string Phone, string UserID,string StartID)
        {
            return Obj_DAL.UpdateZone(ZoneID,ZoneNo,ZoneName, Address, Phone, UserID,StartID);
        }
        public string DeleteZone(string ZoneID)
        {
            return Obj_DAL.DeleteZone(ZoneID);
        }
    }
}
