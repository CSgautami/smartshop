﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class AccReports_BLL
    {
        AccReports_DAL dal_Reports = new AccReports_DAL();
        public DataSet GetBalanceAndStatus(DataSet ds)
        {
            decimal bal = 0;
            decimal crAmt = 0;
            decimal drAmt = 0;
            int cnt = 0;
            if (ds != null && ds.Tables.Count != 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (cnt == 0)
                    {
                        crAmt = (decimal)dr["CrAmount"];
                        drAmt = (decimal)dr["DrAmount"];
                        bal = crAmt - drAmt;
                        if (bal < 0)
                            dr["St"] = "Dr";
                        else
                            dr["St"] = "Cr";
                        cnt++;
                    }
                    else
                    {
                        crAmt = crAmt + (decimal)dr["CrAmount"];
                        drAmt = drAmt + (decimal)dr["DrAmount"];
                        bal = bal + (decimal)dr["CrAmount"] - (decimal)dr["DrAmount"];
                        if (bal < 0)
                        {
                            dr["St"] = "Dr";
                            dr["Balance"] = -1 * bal;
                        }
                        else
                        {
                            dr["Balance"] = bal;
                            dr["St"] = "Cr";
                        }
                    }
                }
                DataRow drow = ds.Tables[0].NewRow();
                drow["Narration"] = "Total:";
                drow["CrAmount"] = crAmt;
                drow["DrAmount"] = drAmt;
                if (bal < 0)
                {
                    drow["St"] = "Dr";
                    drow["Balance"] = -1 * bal;
                }
                else
                {
                    drow["Balance"] = bal;
                    drow["St"] = "Cr";
                }
                DataRow drempty = ds.Tables[0].NewRow();
                drempty["Narration"] = "&nbsp;";
                ds.Tables[0].Rows.Add(drempty);
                ds.Tables[0].Rows.Add(drow);
            }
            return ds;
        }
        public DataSet GetBookData(string UserID, string FromDate, string ToDate, string Voch)
        {
            return GetBalanceAndStatus(dal_Reports.GetBookData(UserID, FromDate, ToDate, Voch));
        }
    }
}
