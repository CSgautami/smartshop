﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Payments_BLL
    {
        Payments_DAL dal_Payments = new Payments_DAL();

        public DataSet GetPaymentReport(string BranchID, string DateFrom, string DateTo)
        {
            return dal_Payments.GetPaymentReport(BranchID, DateFrom, DateTo);
        }

    }
}
