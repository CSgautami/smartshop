﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class Country_BLL
    {
        Country_DAL obj_DAL = new Country_DAL();
        public DataSet GetCountrys()
        {
            return obj_DAL.GetCountrys();
        }
        public string InsertCountry(string CountryName)
        {
            return obj_DAL.InsertCountry(CountryName);
        }
        public string UpdateCountrys(int Countryid, string CountryName)
        {
            return obj_DAL.UpdateCountrys(Countryid, CountryName);
        }


        public string DeleteCountry(int Countryid)
        {
            return obj_DAL.DeleteCountry(Countryid);
        }
    }
}
