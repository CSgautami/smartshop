﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class District_BLL
    {
      District_DAL Obj_DAL=new District_DAL();
      public string InsertDistrict(string DistrictName)
      {
          return Obj_DAL.InsertDistrict(DistrictName);
      }
      public DataSet GetDistrict()
      {
          return Obj_DAL.GetDistrict();
      }
      public string UpdateDistrict(string DistrictID, string DistrictName)
      {
          return Obj_DAL.UpdateDistrict(DistrictID, DistrictName);
      }
      public string DeleteDistrict(string DistrictID)
      {
          return Obj_DAL.DeleteDistrict(DistrictID);
      }
    }
}
