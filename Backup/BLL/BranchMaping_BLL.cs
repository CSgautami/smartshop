﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
    public class UserMaping_BLL
    {
        UserMaping_DAL obj_DAL = new UserMaping_DAL();
        public DataSet GetUser()
        {
            return obj_DAL.GetUser();
        }
        public DataSet GetBranch()
        {
            return obj_DAL.GetBranch();
        }
        public DataSet GetPages()
        {
            return obj_DAL.GetPages();
        }

        public void SavePages(string UserID, string UserPages)
        {
            obj_DAL.SavePages(UserID, UserPages);
        }

        public DataSet GetUserData(string UserID)
        {
            return obj_DAL.GetUserData(UserID);
        }
    }
}
