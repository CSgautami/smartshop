﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;
namespace MyAccounts20
{
    public partial class Invoice : System.Web.UI.Page
    {
        DataTable dt;
        ShopingCart_BLL obj_Bll = new ShopingCart_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {            
            string OrderID = Request.QueryString["OrderID"];
            string Status = Request.QueryString["Status"]; 
            DataSet ds = new DataSet();
            if (!IsPostBack)
            {
                if (OrderID != "")
                {
                    ds = obj_Bll.GetPrintData(OrderID, Session[MyAccountsSession.UserID].ToString());
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table rules='all' border='0' cellspacing='0' align='center' width='100%'>");
                    if (ds.Tables.Count > 1)
                    {
                        tdAddress.InnerHtml = ds.Tables[0].Rows[0]["ShippingAddress"].ToString();                        
                        tdOrderID.InnerText = ds.Tables[0].Rows[0]["OID"].ToString();
                        hidOrderID.Value = ds.Tables[0].Rows[0]["OID"].ToString();
                        tdOrderDate.InnerText = ds.Tables[0].Rows[0]["Date"].ToString();
                        tdModeOfPayment.InnerText = ds.Tables[0].Rows[0]["ModeOfPayment"].ToString();
                        tdPaymentStatus.InnerText = ds.Tables[0].Rows[0]["PaymentStatus"].ToString();
                        tdLoginID.InnerText = ds.Tables[1].Rows[0]["LoginID"].ToString().Replace(".", "/");
                        tdName.InnerText = ds.Tables[1].Rows[0]["LoginName"].ToString();
                        lblSubTotal.Text = ds.Tables[0].Rows[0]["Amount"].ToString();
                        lblDCharges.Text = ds.Tables[0].Rows[0]["DeliveryCharges"].ToString();
                        lblTotal.Text = ds.Tables[0].Rows[0]["NetAmount"].ToString();
                    }
                    if (ds.Tables.Count > 2)
                    {
                        gvShopingCart.DataSource = ds.Tables[2];
                        gvShopingCart.DataBind();
                    }
                    if (Status != "" && Status != null)
                    {
                        tdPrint.Visible = false;
                    }
                    else
                    {
                        tdPrint.Visible = true;
                    }
                }
            }
        }        
    }
}
