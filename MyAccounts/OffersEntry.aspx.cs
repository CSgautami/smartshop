﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class OffersEntry : System.Web.UI.Page
    {
        HeadGroup_BLL Obj_HBLL = new HeadGroup_BLL();
        Offers_BLL Obj_BLL = new Offers_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
                GetDefaults();

           }
        }
        private void GetDefaults()
        {
            ddlHeadGroup.DataSource = Obj_HBLL.GetHeadGroup();
            ddlHeadGroup.DataTextField = "HeadGroupName";
            ddlHeadGroup.DataValueField = "HeadGroupID";
            ddlHeadGroup.DataBind();
            ddlHeadGroup.Items.Insert(0, new ListItem("Select", "0"));


            //ddlItem.DataSource = Obj_BLL.GetItemCreation();
            //ddlItem.DataTextField = "StockItemName";
            //ddlItem.DataValueField = "ICID";
            //ddlItem.DataBind();
            //ddlItem.Items.Insert(0, new ListItem("Select", "0"));
        }
        void FillGrid()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetOffers();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvOffers.DataSource = ds;
                        gvOffers.DataBind();
                        gvOffers.Rows[0].Cells[1].Text = "";
                    }
                    else
                    {
                        gvOffers.DataSource = ds;
                        gvOffers.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res =Obj_BLL.InsertOffers(ddlHeadGroup.SelectedValue.ToString(),ddlItem.SelectedValue,txtPercent.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Items";


            }
            else if (res == "-1")
            {
                lblMsg.Text = "Items already existed";
                ddlItem.Focus();

            }
            else if (res == "1")
            {
                lblMsg.Text = "Items saved successfully";
                btnCancel_Click(sender,e);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateOffers(hidOfersID.Value, ddlHeadGroup.SelectedValue.ToString(), ddlItem.SelectedValue, txtPercent.Text, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Items";
                ddlItem.Focus();
                return;
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Items already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "Items Updated successfully";                
                btnCancel_Click(sender, e);
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvOffers.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtPercent.Text = "";
            ddlHeadGroup.SelectedIndex = 0;
            ddlItem.SelectedIndex = 0;
            lblMsg.Text = "";
            FillGrid();
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidOfersID.Value = ((HtmlInputHidden)grow.FindControl("hidOfersID")).Value;
                ddlHeadGroup.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidHeadGroupID")).Value;
                ddlItem.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidICID")).Value;
                txtPercent.Text = ((Label)grow.FindControl("lblPercentage")).Text;
                ddlItem.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidOfersID.Value = ((HtmlInputHidden)grow.FindControl("hidOfersID")).Value;
                Offers_BLL Obj_BLL = new Offers_BLL();
                string res =Obj_BLL.DeleteOffers(hidOfersID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Item";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "Item Deleted Successfully";
                    btnCancel_Click(sender, e);
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        protected void gvOffers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvOffers.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void ddlHeadGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlItem.DataSource = Obj_BLL.GetHeadWiseItems(ddlHeadGroup.SelectedValue.ToString());
            ddlItem.DataTextField = "StockItemName";
            ddlItem.DataValueField = "ICID";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("Select", "0"));
        }

    }
}
