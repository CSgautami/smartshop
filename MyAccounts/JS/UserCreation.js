﻿var preid="";
var xmlObj;
function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
}


function CreateUser(e,t)
{
    var UName=document.getElementById(preid+"txtUserName");
    var pwd=document.getElementById(preid+"txtPassword");
    var CPassword=document.getElementById(preid+"txtCPassword");
    var Branch=document.getElementById(preid+"ddlBranch");
    
    if(UName.value.trim()=="")
    {
        alert("Enter UserName");
        UName.focus();
        return false;
    }
    if(pwd.value.trim()=="")
    {
        alert("Enter Password");
        pwd.focus();
        return false;
    }

    if(pwd.value!=CPassword.value)
    {
        alert("Please Enter password.");
        pwd.focus();
        return false;
    }
    if(Branch.value=="0")
    {
        alert("Select Branch");
        Branch.focus();
        return false;
    }
    
    t.disabled=true;
    PageMethods.CreateUser(UName.value,pwd.value,Branch.value,CreateUserComplete);
}

function CreateUserComplete(res)
{
    try
    {
        if(res==""||res=="0")
        {
            alert("UserName Already Exists");
            return;
        }
        if(res==""||res=="0")
        {
            alert("Error While Saving UserCreation");
            return;
        }
        else if(res=="1")
        {
            alert("UserCreation Saved successfully");
            window.parent.location="login.aspx";
        }
        else
        {
            alert("Error While Creation, Please Try Later");
            return;
        }
    }
    catch(err)
    {
    }
    finally{
    document.getElementById("btnSave").disabled=false;
    }
}