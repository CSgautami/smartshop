﻿var dtFormat='dd-MM-yyyy'
var preid = '';
var xmlObj;
var UserID=null;
var hidSRInvID=0;
var SRInvNo=0;
var dtpDate;
var SRetInvNo=0;
var SRDate;
var Name;
var Address;
var GST;
var Mode;
var Days;
var Note;
var AccLedger;
var tblGrid;
var Item;
var BatchNo;
var Qty;
var Mrp;
var Price;
var Disc;
var CDisc;
var TaxSystem;
var TotAmount;
var TotDisc;
var TotTax;
var NetDisc;
var Frieght;
var UlCharges;
var Adjustment;
var NetAmount;
var hidUInvID;
var temp;
var nQtyTemp;
var lblQty;
var btnDelete;
var ddlBranch;
var hidBranchID;
var hidGBID;
//var lblItem;
var tdLedger;
var tdItem;
var lblName;
var Search;
var BarCode;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById("hidGBID");
}
function GetControls()
{
    hidSRInvID =document.getElementById("hidSRInvID");
    hidUInvID=document.getElementById("hidUInvID");
    //hidInvID=document.getElementById("hidInvID");
    SRInvNo =document.getElementById("txtInvNo");
    dtpDate=document.getElementById("txtDate");
    SRetInvNo=document.getElementById("txtSRetInvNo");
    SRDate =document.getElementById("txtSRDate");
    Name =document.getElementById("ddlName");
    tdLedger =document.getElementById("tdName");
    Address =document.getElementById("txtAddress");
    GST =document.getElementById("txtGst");
    Mode =document.getElementById("ddlMode");    
    Days =document.getElementById("txtDays");
    Note =document.getElementById("txtNote");
    AccLedger =document.getElementById("ddlAccLedger");        
    tblGrid =document.getElementById("tblGrid");
    TotAmount =document.getElementById("txtTotAmount");
    TotDisc =document.getElementById("txtTotDisc");
    TotTax =document.getElementById("txtTotTax");
    NetDisc =document.getElementById("txtNetDisc");
    Frieght =document.getElementById("txtFright");
    UlCharges =document.getElementById("txtUlCharges");
    Adjustment =document.getElementById("txtAdjustment");
    NetAmount =document.getElementById("txtNetAmount");
    lblName =document.getElementById("lblName");
    
    Item = document.getElementById("ddlStockItem");
    tdItem= document.getElementById("tdItem");
    //lblItem =document.getElementById("lblItem");
    BatchNo =document.getElementById("ddlBatchNo");
    Qty=document.getElementById("txtQty");
    Mrp = document.getElementById("txtMrp");
    Price=document.getElementById("txtPrice");
    Disc  = document.getElementById("txtDiscPer");
    CDisc=document.getElementById("txtCashDisc");
    TaxSystem =document.getElementById("ddlTaxSystem");
    lblQty=document.getElementById("lblQty");
    ddlBranch=document.getElementById("ddlBranch"); 
    hidBranchID=document.getElementById(preid+"hidBranchID");  
    Search=document.getElementById("txtSearch");
    BarCode=document.getElementById("txtBarCode"); 
}
function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}
function GetBranch()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetBranch(UserID.value, GetBranchComplete);    
}

function GetBranchComplete(res)
{
    getXml(res);    
    ddlBranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";    
        ddlBranch.options.add(opt);        
    }  
    else
    {
        GetLedgerName();
        GetAccLedger();
    }    
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlBranch.options.add(opt);
    }     
    if(hidBranchID.value!="")
    {
        ddlBranch.value=hidBranchID.value;
        ddlBranch.disabled=true;
    }
    else 
    {
        ddlBranch.disabled=false;
        if(hidGBID==null)
            GetDefaultBranch();
        ddlBranch.value=hidGBID.value;
        GetLedgerName();
        GetAccLedger();
    }
}
function GetLedgerName()
{
    if(UserID==null)
       GetUserID(); 
    //PageMethods.GetLedgerName(ddlBranch.value,GetLedgerComplete);    
}
function GetLedgerComplete(res)
{
    getXml(res);
    var ddlName=document.getElementById(preid+"ddlName");
    ddlName.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlName.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlName.options.add(opt);
    }    
}
function GetAccLedger()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetAccLedger(ddlBranch.value,GetAccLedgerComplete);
}
function GetAccLedgerComplete(res)
{
    getXml(res);
    var ddlAccLedger=document.getElementById(preid+"ddlAccLedger");
    ddlAccLedger.options.length=0;
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
        ddlAccLedger.options.add(opt);
    }    
}
function  GetStockItem()
{
    if(UserID==null)
       GetUserID(); 
    //PageMethods.GetStockItem(GetStockItemComplete);
}
function GetStockItemComplete(res)
{
    getXml(res);
    var ddlStockItem=document.getElementById(preid+"ddlStockItem");
    ddlStockItem.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlStockItem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
        ddlStockItem.options.add(opt);
    }    
}
function  GetTaxSystem()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetTaxSystem(GetGetTaxSystemComplete);
}
function GetGetTaxSystemComplete(res)
{
    getXml(res);
    var ddlTaxSystem=document.getElementById(preid+"ddlTaxSystem");
    ddlTaxSystem.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlTaxSystem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TaxName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TaxNo")[0].firstChild.nodeValue;
        ddlTaxSystem.options.add(opt);
    }    
}
function GetItemBatchAndTaxSystem()
{
    GetBatchNo();
    GetItemWiseTaxSystem();
}
function GetItemWiseTaxSystem()
{
   Item=document.getElementById("ddlStockItem");
   PageMethods.GetItemWiseTaxSystem(selectedItem,CompleteItemWiseTaxSystem);  
}
function CompleteItemWiseTaxSystem(res)
{
    try 
    {
        getXml(res);
        TaxSystem=document.getElementById("ddlTaxSystem");
        //lblItem=document.getElementById("lblItem");
        if(xmlObj.getElementsByTagName("Table").length==0)
        {          
            //lblItem.innerText="";
            TaxSystem.value="0";  
            return;            
        }
        else
        {              
            TaxSystem.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("TaxNo")[0].firstChild.nodeValue;
            //lblItem.innerText =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
        }      
    }
    catch(err)
    {
    }
}
function GetBatchNo()
{
    if(UserID==null)
       GetUserID();        
    Item = document.getElementById("ddlStockItem");
    PageMethods.GetBatchNo(selectedItem, ddlBranch.value,GetBatchNoComplete);    
}
function GetBatchNoComplete(res)
{
    getXml(res);
    //var ddlBatchNo=document.getElementById(preid+"ddlBatchNo");
    BatchNo.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    BatchNo.options.add(opt);
    var opt=document.createElement("OPTION");    
    var bfound;
    bfound=false;
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        if( xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue=="1" )
            bfound =true; 
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
        BatchNo.options.add(opt);
    }    
     if (bfound==false)
        temp="";
    if(bfound==true && temp =="")
        temp = "1"; 
    if (temp != "")
    {
        BatchNo.value=temp;                    
        GetStock();
    }
    else    
    {
        BatchNo.value="0";    
    }        
}
function GetLedgerDetails()
{
    if(UserID==null)
       GetUserID();        
    Name =document.getElementById("ddlName");
    PageMethods.GetLedgerDetails(selectedLedger,GetLedgerDetailsComplete);
}
function GetLedgerDetailsComplete(res)
{
    try 
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {            
            return;
        }
        else
        {   
            if(hidSRInvID.value=="-1")        
            { 
                Address.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Address")[0].firstChild.nodeValue;
            }
            lblName.innerText=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
            GST.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("APGST")[0].firstChild.nodeValue;
        }
    }
    catch(ex)
    {
    }
    SelectMode();       
}
function SelectMode()
{
    if(selectedLedger=="1")
    {
        Mode.value="Cash";
        Mode.disabled=true;
    }
    else 
    {
        Mode.disabled=false;
    }
}
function GetStock()
{    
    Item = document.getElementById("ddlStockItem");
    BatchNo = document.getElementById("ddlBatchNo");    
    lblQty = document.getElementById("lblQty");
    lblQty.innerText="";
    PageMethods.GetStock(selectedItem, BatchNo.value, dtpDate.value, ddlBranch.value,selectedLedger, GetStockComplete);  
}
function GetStockComplete(res)
{
    lblQty = document.getElementById("lblQty");
    if (hidSRInvID.value !=-1 && nQtyTemp!="")
    {
        res = parseInt(res) + parseInt(nQtyTemp);
        //nQtyTemp = "";
    }
    lblQty.innerText=parseInt(res);
}

function btnCancel_Click()
{
    GetControls();
    if(UserID==null)  
        GetUserID(); 
     GetMaxIDComplete();
    //PageMethods.GetInvMaxID(UserID.value, GetMaxIDComplete);    
    //PageMethods.GetInvMaxID(UserID.value, GetMaxIDComplete);
}
function GetMaxIDComplete(nInvID)
{
    
//    InvNo.value=nInvID;
//    PurInvNo.value=nInvID;    
//    document.getElementById("txtInvNo").value=nInvID;    
//    document.getElementById("txtPInvNo").value=nInvID;  
//    InvNo.value =nInvID ;
//    PurInvNo.value=nInvID ;
    SRInvNo.value ="" ;
    SRInvNo.disabled=true;
    SRetInvNo.value="";
    hidSRInvID.value="-1";
    hidUInvID.value="-1";    
    dtpDate.value =new Date().format("dd-MM-yyyy");
    SRDate.value=new Date().format("dd-MM-yyyy");    
    Name.value="0"
    tdLedger.value="";
    lblName.innerText="";
    Address.value=""
    GST.value="";
    Mode.value="Cash";
    Mode.disabled=true;
    Days.value="";
    Note.value="";
    //AccLedger.value="0";    
    for(var i=tblGrid.rows.length-1;i>0;i--)
    {
       tblGrid.deleteRow(i);
    }
    ClearFields();
    TotAmount.value="0";
    TotDisc.value="0";
    TotTax.value="0";
    NetDisc.value="0";
    Frieght.value="0";
    UlCharges.value="0";
    Adjustment.value="0";
    NetAmount.value="0";
    //dtpDate.focus();  
    Search.checked=false;      
    tdLedger.focus();
    document.getElementById("btnPrint").style.display="none";    
    enabledbtns();
}
function ActivateDelete()
{
    document.getElementById("btnDelete").style.display="none";
}
function btnAdd_Click()
{    
    if (tdItem.value=="")
    {
        tdItem.focus();
        alert("Select Item");
        return false;
    }
    //if (BatchNo[BatchNo.selectedIndex].text=="" || BatchNo[BatchNo.selectedIndex].text=="Select")
    if (BatchNo.value==0)
    {
        BatchNo.focus();
        alert("Enter Batch No");
        return false;
    }
    //if (Qty.value=="")
    if (parseInt(lblQty.innerText)<parseInt(Qty.value))
    {
        Qty.focus();
        alert("Enter Qty <=" + parseInt(lblQty.innerText));
        return false;
    }
    if (isNaN(Qty.value))
    {
        Qty.text="";
        Qty.focus();
        alert("Enter valid Qty");
        return false;
    }
    if (Mrp.value == "")
    {
        Mrp.focus();
        alert("Enter Mrp");
        return false;        
    }
    if (isNaN(Mrp.value))
    {
        Mrp.text="";
        Mrp.focus();
        alert("Enter valid Mrp");
        return false;        
    }
    if (Price.value=="")
    {
        Price.focus();
        alert("Enter Price");
        return false;
    }
    if (isNaN(Price.value))
    {
        Price.text="";
        Price.focus();
        alert("Enter valid Price");
        return false;
    }
    if (parseFloat(Price.value) > parseFloat(Mrp.value))
    {
        Price.focus();
        alert("Enter Price Less than Mrp");
        return false;
    }
    if(TaxSystem.value==0)
    {
        TaxSystem.focus();
        alert("Enter Tax");
        return false;
    }
    if (isNaN(Disc.value))
    {
        Disc.text="";
        Disc.focus();
        alert("Enter valid Disc %");
        return false;        
    }
    if (isNaN(CDisc.value))
    {
        CDisc.text="";
        CDisc.focus();
        alert("Enter valid Cash Disc");
        return false;        
    }    
    GetTax(TaxSystem.value);
//    tr.cells[0].innerHTML=Item.value;
//    tr.cells[0].style.display="none";
//    tr.cells[1].innerHTML=Item[Item.selectedIndex].innerText;
//    tr.cells[2].innerHTML=BatchNo.value;
//    tr.cells[3].innerHTML=parseFloat(Qty.value);
//    tr.cells[4].innerHTML=parseFloat(Mrp.value);
//    tr.cells[5].innerHTML=parseFloat(Price.value);    
//    tr.cells[6].innerHTML=Disc.value;
//    tr.cells[7].innerHTML=CDisc.value;
//    var UnitRate, nDisc;    
//    UnitRate=Price.value-(Price.value)*(Disc.value)/100;
//    UnitRate =UnitRate-(CDisc.value);     
//    nDisc =(parseFloat(Price.value)- parseFloat(UnitRate))*  parseFloat(Qty.value);
//    tr.cells[8].innerHTML=TaxSystem.value;
//    tr.cells[8].style.display="none";
//    tr.cells[9].innerHTML=TaxSystem[TaxSystem.selectedIndex].innerText;
//    tr.cells[10].innerHTML=UnitRate;
//    tr.cells[11].innerHTML=UnitRate*Qty.value;
//    tr.cells[12].innerHTML=parseFloat(Qty.value)*parseFloat(Price.value);
//    tr.cells[12].style.display="none";
//    tr.cells[13].innerHTML=nDisc;
//    tr.cells[13].style.display="none";    
//    //tr.cells[14].innerHTML=GetTax(TaxSystem.value);
//    tr.cells[14].innerHTML=0;
//    tr.cells[14].style.display="none";
//    document.getElementById("txtTotAmount").value=GetColTotal(tbl,12); 
//    document.getElementById("txtTotDisc").value=GetColTotal(tbl,13); 
//    document.getElementById("txtTotTax").value=GetColTotal(tbl,14); 
//    CalculateData();
//    ClearFields();  
//    tr.onmouseover=function(){MouseOver(this);};
//    tr.onmouseout=function(){MouseOut(this);};
//    tr.onclick=function(){tblClick(this);}                 
//    Item.focus();
}
function ClearFields()
{
    document.getElementById("ddlStockItem").value="0";
    tdItem.value="";
    //lblItem.innerText="";
    document.getElementById("ddlBatchNo").value="";
    document.getElementById("txtQty").value="";
    document.getElementById("txtMrp").value="";
    document.getElementById("txtPrice").value="";
    document.getElementById("txtDiscPer").value="";
    document.getElementById("txtCashDisc").value="";
    document.getElementById("ddlTaxSystem").value="0";
    temp="";
    nQtyTemp="";
    lblQty.text="";
    BarCode.value="";
}
function disabledbtns()
{
    document.getElementById("btnSave").disabled=true;    
}
function enabledbtns()
{
    document.getElementById("btnSave").disabled=false;    
}

function btnSave_Click(e,t)
{
    disabledbtns();
    GetControls();
    try
    {
        UserID=document.getElementById("hidUserID");
        if (ddlBranch.value=="0")
        {
            alert("Select Branch");
            InvNo.focus();
            return enabledbtns();        
        }            
        if (!isDate(dtpDate.value,dtFormat))
        {
            alert("Enter Valid Date");
            dtpDate.focus();
            return enabledbtns();
        }
//        if (parseInt(PurInvNo.value)== 0)
//        {
//            alert("Enter Purchase Invoice No");
//            PurInvNo.focus();
//            return false;
//        }
        if (!isDate(SRDate.value,dtFormat))
        {
            alert("Enter Valid Date");
            PDate.focus();
            return enabledbtns();
        }
//        if (Name.value=="0")
//        {
//            alert("Select Name");
//            Name.focus();
//            return false;
//        }
        if (tdLedger.value=="")
        {
            alert("Select Name");
            tdLedger.focus();
            return enabledbtns();
        }
        if(Address.value=="")
        {
            alert("Enter Address");
            Address.focus();
            return enabledbtns();
        }
        if(Mode.value=="")
        {
            alert("Select Mode");
            Mode.focus();
            return enabledbtns();
        }
        if(isNaN(Days.value))
        {
            alert("Enter Valid Days");
            Days.text="";
            Days.focus();            
            return enabledbtns();
        }
        if( AccLedger.value=="0" || AccLedger.value=="")
        {
            alert("Select A/c Ledger");
            AccLedger.focus();
            return enabledbtns();
        }
        if(document.getElementById("tblGrid").rows.length<2)
        {
            alert("Enter Items");
            Item.focus();
            return enabledbtns();
        }
        if(isNaN(NetDisc.value))
        {
            alert("Enter valid Discount");
            NetDisc.text="";
            NetDisc.focus();
            return enabledbtns();
        }
        if(isNaN(NetDisc.value))
        {
            alert("Enter valid Discount");
            NetDisc.text="";
            NetDisc.focus();
            return enabledbtns();
        }
        var tbl;
        tbl=document.getElementById("tblGrid");
        var xmlDet="<XML>";
        for(var i=1;i<tbl.rows.length;i++)
        {
            xmlDet=xmlDet+"<ItemDet>";
            var tr=tbl.rows[i];            
            xmlDet=xmlDet+"<ItemID>"+tr.cells[0].innerHTML.trim()+"</ItemID>";
            xmlDet=xmlDet+"<BatchNo>"+tr.cells[2].innerHTML.trim()+"</BatchNo>";
            xmlDet=xmlDet+"<Qty>"+tr.cells[3].innerHTML.trim()+"</Qty>";
            xmlDet=xmlDet+"<MRP>"+tr.cells[4].innerHTML.trim()+"</MRP>";
            xmlDet=xmlDet+"<Price>"+tr.cells[5].innerHTML.trim()+"</Price>";
            if (tr.cells[6].innerHTML.trim()=="") tr.cells[6].innerHTML=0 ;         
            if (tr.cells[7].innerHTML.trim()=="") tr.cells[7].innerHTML=0;
            xmlDet=xmlDet+"<Disc>"+tr.cells[6].innerHTML.trim()+"</Disc>";             
            xmlDet=xmlDet+"<CDisc>"+tr.cells[7].innerHTML.trim()+"</CDisc>";
            xmlDet=xmlDet+"<TaxNo>"+tr.cells[8].innerHTML.trim()+"</TaxNo>";
            xmlDet=xmlDet+"<UnitPrice>"+tr.cells[10].innerHTML.trim()+"</UnitPrice>";
            xmlDet=xmlDet+"<Amount>"+tr.cells[11].innerHTML.trim()+"</Amount>";           
            xmlDet=xmlDet+"<TotAmt>"+tr.cells[12].innerHTML.trim()+"</TotAmt>";           
            xmlDet=xmlDet+"<TotDisc>"+tr.cells[13].innerHTML.trim()+"</TotDisc>";           
            xmlDet=xmlDet+"<TotTax>"+tr.cells[14].innerHTML.trim()+"</TotTax>";           
            xmlDet=xmlDet+"</ItemDet>";
        }
        xmlDet=xmlDet+"</XML>";  
        var Con=confirm("Are You Sure,Do You Want To Save?");
        if(Con=true)
        {       
        PageMethods.SaveInvoice(hidSRInvID.value, hidUInvID.value, SRInvNo.value, dtpDate.value, SRetInvNo.value, SRDate.value, selectedLedger,Address.value, Mode.value, Days.value, Note.value, AccLedger.value, TotAmount.value, TotDisc.value, TotTax.value, NetDisc.value, Frieght.value, UlCharges.value, Adjustment.value, NetAmount.value, ddlBranch.value,UserID.value, xmlDet, SaveInvoiceComplete);        
     }
    }
     catch(err)
     {
        t.disable=false;
     }    
 }

function MouseOver(t)
{
    if(t.cells[0].innerHTML.trim()!="")
    {
        t.style.backgroundColor="gray";
        t.style.cursor="hand";     
        t.title="Click to edit the values";  
    }
}

function MouseOut(t)
{
    if(t.cells[0].innerHTML.trim()!="")
    {
        t.style.backgroundColor="";
        t.style.crusor="text";        
    }
}


function tblClick(t)
{
    var Con=confirm("Are You Sure,Do You Want To Edit?");
    if(Con==true)
    {   
    if(t.cells[0].innerHTML.trim()!="")
    {
             
        //document.getElementById("ddlStockItem").value=t.cells[0].innerHTML.trim();
        //tdItem.value=t.cells[1].innerHTML.trim();
        selectedItem =t.cells[0].innerHTML.trim();
        var ItemWithCode;
        ItemWithCode= t.cells[1].innerHTML.trim(); 
        //ItemWithCode=ItemWithCode.substring(0,ItemWithCode.indexOf(' -'));
        tdItem.value=ItemWithCode;
        GetBatchNo();
        //document.getElementById("ddlBatchNo").value=t.cells[2].innerHTML.trim();
        temp=t.cells[2].innerHTML.trim();
        document.getElementById("txtQty").value=t.cells[3].innerHTML.trim();
        document.getElementById("txtMrp").value=t.cells[4].innerHTML.trim();
        document.getElementById("txtPrice").value=t.cells[5].innerHTML.trim();
        document.getElementById("txtDiscPer").value=t.cells[6].innerHTML.trim();
        document.getElementById("txtCashDisc").value=t.cells[7].innerHTML.trim();
        document.getElementById("ddlTaxSystem").value=t.cells[8].innerHTML.trim();          
        document.getElementById("tblGrid").deleteRow(t.rowIndex)
        nQtyTemp=document.getElementById("txtQty").value;
        tdItem.focus();
        CalculateData();
    }
  }
}
function GetTax(tax)
{    
    PageMethods.GetTax(tax,retunnTax);  
}
function  retunnTax(nTax)
{
    // nTaxRate =  nTax;  
    var tbl=document.getElementById("tblGrid");
    var i;
    i=tbl.rows.length;
    var tr=tbl.insertRow(i);    
    for(i=0;i<15;i++)
    {
        tr.insertCell(i);
    }
    tr.cells[0].innerHTML=selectedItem;
    tr.cells[0].style.display="none";
    tr.cells[1].innerHTML=tdItem.value ;
    tr.cells[2].innerHTML=BatchNo[BatchNo.selectedIndex].innerText;
    tr.cells[3].innerHTML=parseFloat(Qty.value);
    tr.cells[4].innerHTML=parseFloat(Mrp.value);
    tr.cells[5].innerHTML=parseFloat(Price.value);    
    tr.cells[6].innerHTML=Disc.value;
    tr.cells[7].innerHTML=CDisc.value;
    var UnitRate, nDisc;    
    UnitRate=Price.value-(Price.value)*(Disc.value)/100;
    UnitRate = UnitRate-(CDisc.value);
    //UnitRate = UnitRate.toFixed(2);
    nDisc =(parseFloat(Price.value)- parseFloat(UnitRate))*  parseFloat(Qty.value);
    nDisc = nDisc.toFixed(2);
    tr.cells[8].innerHTML=TaxSystem.value;
    tr.cells[8].style.display="none";
    tr.cells[9].innerHTML=TaxSystem[TaxSystem.selectedIndex].innerText;
    tr.cells[14].innerHTML= (UnitRate * Qty.value * nTax/100).toFixed(2);
    UnitRate = UnitRate + (UnitRate*nTax/100);
    UnitRate = UnitRate.toFixed(2);
    tr.cells[10].innerHTML=UnitRate;
    tr.cells[11].innerHTML=UnitRate*Qty.value;
    tr.cells[12].innerHTML=parseFloat(Qty.value)*parseFloat(Price.value);
    tr.cells[12].style.display="none";
    tr.cells[13].innerHTML=nDisc;
    tr.cells[13].style.display="none";            
    tr.cells[14].style.display="none";    
//    document.getElementById("txtTotAmount").value=GetColTotal(tbl,12); 
//    document.getElementById("txtTotDisc").value=GetColTotal(tbl,13); 
//    document.getElementById("txtTotTax").value=GetColTotal(tbl,14); 
    CalculateData();
    ClearFields();
    tr.cells[4].style.textAlign="right";
    tr.cells[5].style.textAlign="right";
    tr.cells[6].style.textAlign="right";
    tr.cells[7].style.textAlign="right";
    tr.cells[10].style.textAlign="right";
    tr.cells[11].style.textAlign="right";
    tr.cells[12].style.textAlign="right";
    tr.cells[13].style.textAlign="right";
    tr.cells[14].style.textAlign="right";  
    tr.onmouseover=function(){MouseOver(this);};
    tr.onmouseout=function(){MouseOut(this);};
    tr.onclick=function(){tblClick(this);}                 
    tdItem.focus();
}
function GetColTotal(t,c)
{
   var i;
   var nCnt=0;
   for(i=1;i<t.rows.length;i++)
   {
        var tr=t.rows[i];            
        if(!isNaN(tr.cells[c].innerHTML.trim()))
                nCnt =nCnt + parseFloat(tr.cells[c].innerHTML);        
   }
   return nCnt.toFixed(2);
}
function CalculateData()
{
    var nTotal;
    GetControls();
    var tbl=document.getElementById("tblGrid");
    TotAmount.value=GetColTotal(tbl,12);     
    TotDisc.value=GetColTotal(tbl,13); 
    TotTax.value=GetColTotal(tbl,14); 
    nTotal =parseFloat(TotAmount.value)-parseFloat(TotDisc.value)+parseFloat(TotTax.value)-parseFloat(NetDisc.value)+parseFloat(Frieght.value)+parseFloat(UlCharges.value)+parseFloat(Adjustment.value);
    NetAmount.value=nTotal.toFixed(2);
}

function SaveInvoiceComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Saving");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }    
    else
    {
        alert("Saved successfully With Sale Return InvNo." + res);
        //btnCancel_Click(); 
        hidUInvID.value=res;
        document.getElementById(preid+"btnPrint").style.display="block";
    }
}var Flag;
var navbtn;
function btnDBRecordNav_Click(e,t)
{    
    try    
    {
        GetControls();
        Flag="F";
        navbtn=t;
        if(t.id=="btnFirst")
            Flag="F";
        else if(t.id=="btnPrev")
        {
             if(hidSRInvID.value==-1)
                Flag="L";
              else
                Flag="P";
        }
        else if(t.id=="btnNext")
        {
           if(hidSRInvID.value==-1)
              return;
           else
              Flag="N";
        }
        else if(t.id=="btnLast")
            Flag="L";              
        if(UserID==null)
            GetUserID();
        navbtn.disabled=true;        
        PageMethods.GetData(ddlBranch.value,hidSRInvID.value,Flag,GetDataComplete);        
    }
    catch(err)
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
    
}
function GetDataComplete(res)
{
    try
    {
        btnCancel_Click();
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
        else
        {   
            ClearFields();
            //ddlBranch.value =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            hidSRInvID.value = parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("SRInvID")[0].firstChild.nodeValue,10);
            hidUInvID.value = parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("UInvID")[0].firstChild.nodeValue,10);                
            document.getElementById("hidSRInvID").value=hidSRInvID.value;
            document.getElementById("hidUInvID").value=hidUInvID.value;
          //  alert( hidPInvID);
            SRInvNo.value =  xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("SRInvNo")[0].firstChild.nodeValue;
            //InvNo =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("InvNo")[0].firstChild.nodeValue
            dtpDate.value = xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("InvDate")[0].firstChild.nodeValue;
            SRetInvNo.value = xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("DCNo")[0].firstChild.nodeValue;
            SRDate.value = xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("DCDate")[0].firstChild.nodeValue;
            Name.value=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerID")[0].firstChild.nodeValue,10);
            selectedLedger=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LedgerID")[0].firstChild.nodeValue,10);
            tdLedger.value =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            GetLedgerDetails(); 
            Mode.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Mode")[0].firstChild.nodeValue;
            Days.value=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Days")[0].firstChild.nodeValue,10);
            Address.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Address")[0].firstChild.nodeValue;
            AccLedger.value=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ACLedgerID")[0].firstChild.nodeValue,10);
            Note.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Note")[0].firstChild.nodeValue;
            TotAmount.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("TotalAmount")[0].firstChild.nodeValue,10)
            TotDisc.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("TotalDisc")[0].firstChild.nodeValue,10)
            TotTax.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("TotalTax")[0].firstChild.nodeValue,10)
            NetDisc.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("NetDisc")[0].firstChild.nodeValue,10)
            Frieght.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Frieght")[0].firstChild.nodeValue,10)
            UlCharges.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("LULCharges")[0].firstChild.nodeValue,10)
            Adjustment.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Adjustment")[0].firstChild.nodeValue,10)
            NetAmount.value=parseFloat(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("NetAmount")[0].firstChild.nodeValue,10)
            var tbl;
            tbl=document.getElementById("tblGrid");
            for(var i=tbl.rows.length-1;i>0;i--)
                tbl.deleteRow(i);
            var xmlObjLength=xmlObj.getElementsByTagName("Table1").length;
            for(var i=0;i<xmlObjLength;i++)
            {                
                if(tbl.rows.length==1)
                    var tr=tbl.insertRow(tbl.rows.length);
                else
                    var tr=tbl.insertRow(tbl.rows.length-1);
                var j;
                for(j=0;j<15;j++)
                {
                    tr.insertCell(j);
                }                               
                tr.cells[0].style.display="none";
                tr.cells[8].style.display="none";
                tr.cells[12].style.display="none";
                tr.cells[13].style.display="none";
                tr.cells[14].style.display="none";
                var ItemID =parseInt(xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("ItemID")[0].firstChild.nodeValue,10);
                //tr.cells[0].innerHTML="<input type='hidden' value='"+ItemID+"'/>";
                tr.cells[0].innerHTML=ItemID;
                selectedItem =ItemID;
                //tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
                var ItemWithCode;
                ItemWithCode =xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
                ItemWithCode=ItemWithCode + " - " + xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                tr.cells[1].innerHTML=ItemWithCode; 
                tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
                var nQty=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("Qty")[0].firstChild.nodeValue;
                tr.cells[3].innerHTML=nQty;                
                tr.cells[4].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("MRP")[0].firstChild.nodeValue;
                var nPrice=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("Price")[0].firstChild.nodeValue;
                tr.cells[5].innerHTML=nPrice;
                tr.cells[6].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("Disc")[0].firstChild.nodeValue;
                tr.cells[7].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("CDisc")[0].firstChild.nodeValue;
                var TaxID =parseInt(xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("TaxNo")[0].firstChild.nodeValue,10);
//                tr.cells[8].innerHTML="<input type='hidden' value='"+TaxID+"'/>";                
                tr.cells[8].innerHTML=TaxID;                
                tr.cells[9].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("TaxName")[0].firstChild.nodeValue;
                tr.cells[10].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("UnitPrice")[0].firstChild.nodeValue;                
                tr.cells[11].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("Amount")[0].firstChild.nodeValue;
                var nTemp =xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("TotAmt")[0].firstChild.nodeValue;
                //tr.cells[12].innerHTML= "<input type='hidden' value='"+nTemp+"'/>";
                tr.cells[12].innerHTML= nTemp;
                nTemp=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("TotDisc")[0].firstChild.nodeValue; 
                //tr.cells[13].innerHTML="<input type='hidden' value='"+nTemp+"'/>";
                tr.cells[13].innerHTML= nTemp;
                nTemp =xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("TotTax")[0].firstChild.nodeValue;
                //tr.cells[14].innerHTML="<input type='hidden' value='"+nTemp+"'/>";
                tr.cells[14].innerHTML= nTemp;
                tr.cells[4].style.textAlign="right";
                tr.cells[5].style.textAlign="right";
                tr.cells[6].style.textAlign="right";
                tr.cells[7].style.textAlign="right";
                tr.cells[10].style.textAlign="right";
                tr.cells[11].style.textAlign="right";
                tr.cells[12].style.textAlign="right";
                tr.cells[13].style.textAlign="right";
                tr.cells[14].style.textAlign="right";
                tr.onmouseover=function(){MouseOver(this);};
                tr.onmouseout=function(){MouseOut(this);};
                tr.onclick=function(){tblClick(this);}                
            }
            document.getElementById("btnDelete").style.display="block";  
            document.getElementById("btnPrint").style.display="block";              
         }
     }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
 }
function FindData(ID,BranchID)
{
    GetControls();
    if(UserID==null)
        GetUserID();
    ddlBranch.value=BranchID.toString();
    hidBranchID.value=BranchID;
    GetLedgerName();
    PageMethods.GetData(BranchID,ID,'',GetDataComplete); 
}
function btnDelete_Click(e,t)
{
    try 
    {
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)
            PageMethods.btnDelete_Click(hidSRInvID.value,ddlBranch.value, DeletComplete);
    }
    catch(err)
    {
    }
}
function DeletComplete(res)
{
    if(res=="-1")
    {
        alert("Can't Delete Transaction Exists");        
    }
    else 
    {
        alert("Deleted Successfully");
        btnCancel_Click();
    }
}
var divLedger;
var selectedLedger="";
var prevText="";

function FillLedger(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedLedger="";
        divLedger=document.getElementById("divLedger");
        if(t.value!="")
        {        
            PageMethods.CallFillLedger(ddlBranch.value,t.value, FillLedgerComplete)
        }
        else{
        divLedger.innerHTML="";
        divLedger.style.display="none";
        }
    }
}

function FillLedgerComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var lid;
            var lname;
            var YourID;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                    lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                    lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                    //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                    YourID =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
            lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+YourID+"</td></tr>";
                selectedLedger=lid;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+YourID+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divLedger.innerHTML=tbl;
        if(count>0)
            divLedger.style.display="block";
        else
            divLedger.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectClick(t)
{
    selectedLedger=t.cells[0].innerHTML;
    var txt=document.getElementById("tdName");
    ApplyLedger(txt);
}
function OnMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedLedger ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedLedger=t.cells[0].innerHTML;
}
function selectLedger(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyLedger(t);
    }
    else
    {
        if(divLedger==null)
            divLedger=document.getElementById("divLedger");
        var tbl=divLedger.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedLedger)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedLedger=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedLedger=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyLedger(t)
{
    if(divLedger==null)
            divLedger=document.getElementById("divLedger");
    if(selectedLedger!="")
    {   
        var tbl=divLedger.firstChild;
        var cnt=0;
        if(tbl!="" && tbl!=null)
        { 
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedLedger)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedLedger="";
                t.value="";
            }
        }        
    }
    else
        t.value="";
    prevText=t.value;
    divLedger.style.display="none";    
}
var divItem;
var selectedItem="";
var prevText="";

function FillItem(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedItem="";
        divItem=document.getElementById("divItem");
        if(t.value!="")
        {        
            PageMethods.CallFillItem(t.value,FillItemComplete)
        }
        else{
        divItem.innerHTML="";
        divItem.style.display="none";
        }
    }
}

function FillItemComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var ItemID;
            var ItemName;
            var ItemCode;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild!=null)
                    ItemID=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild!=null)
                    ItemName=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            //lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild!=null)
                    //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                    ItemCode =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            //lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                selectedItem=ItemID;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divItem.innerHTML=tbl;
        if(count>0)
            divItem.style.display="block";
        else
            divItem.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectClick(t)
{
    selectedItem=t.cells[0].innerHTML;
    var txt=document.getElementById("tdItem");
    ApplyItem(txt);
}
function OnMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedItem ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedItem=t.cells[0].innerHTML;
}
function selectItem(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyItem(t);
    }
    else
    {
        if(divItem==null)
            divItem=document.getElementById("divItem");
        var tbl=divItem.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedItem=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedItem=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyItem(t)
{
    if(divItem==null)
            divItem=document.getElementById("divItem");
    if(selectedItem!="")
    {   
        var tbl=divItem.firstChild;
        var cnt=0;
        if(tbl!="" && tbl!=null)
        { 
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedItem="";
                t.value="";
            }
        }        
    }
    else
        t.value="";
    prevText=t.value;
    divItem.style.display="none";    
}
function chkSearch_Click(e,t)
{
    if(t.checked==true)
    {
        document.getElementById("txtSearch").style.display="block";
        document.getElementById("btnOk").style.display="block";
    }
    else
    {
        document.getElementById("txtSearch").style.display="none";
        document.getElementById("btnOk").style.display="none";
    }
}
function SearchInvoice()
{
    GetControls();    
    if(UserID==null)
        GetUserID();    
    PageMethods.GetSearchInvoiceData(ddlBranch.value,Search.value,GetDataComplete); 
}
function BarcodeKeydown(e,t)
{
    if(e.keyCode==13 || e.keyCode==9)  
    {                  
        if(BarCode!="")
        {
            PageMethods.GetItemWithBarCode(BarCode.value,CompleteItemWithBarCode);  
        }        
    }
}
function CompleteItemWithBarCode(res)
{
    try 
    {
        getXml(res);        
        if(xmlObj.getElementsByTagName("Table").length==0)
        {        
            return;            
        }
        else
        {              
            tdItem.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            selectedItem =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            GetItemBatchAndTaxSystem();
            Qty.focus();
        }      
    }
    catch(err)
    {
    }
}