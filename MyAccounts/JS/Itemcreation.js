﻿var UserID;
var ICID;
var StockGroupID;
var ItemCode;
var StockItemName;
var PurchaseTaxSystem;
var SalesTaxSystem;
var ReOrderlevel;
var preid = '';
var ddlHeadGroup;
var ddlCategory;
var ddlSubCategory;
var Search;
var BarCode;
var ddlCompany;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}


function GetUserID()
{
    UserID=document.getElementById("hidUserId");
}
function GetControls()
{
    GetUserID();
    ICID=document.getElementById(preid+"ICID");
    StockGroupID=document.getElementById(preid+"ddlStockGroup");
    ItemCode=document.getElementById(preid+"txtItemCode");
    StockItemName=document.getElementById(preid+"txtStockItemName");
    PurchaseTaxSystem=document.getElementById(preid+"ddlPurchaseTaxSystem");
    SalesTaxSystem=document.getElementById(preid+"ddlSalesTaxSystem");
    ReOrderlevel=document.getElementById(preid+"txtReOrderlevel"); 
    ddlHeadGroup=document.getElementById(preid+"ddlHeadGroup");
    ddlCategory=document.getElementById(preid+"ddlCategory");
    ddlSubCategory=document.getElementById(preid+"ddlSubCategory");
    BarCode=document.getElementById(preid+"txtBarcode");
    ddlCompany=document.getElementById(preid+"ddlCompany");
    
}
function SetData()
{
    GetControls();
    GetHeadGroup();
    GetCompany();
    //GetStockGroup();
    GetPurchaseTax();
    GetSaleTax();    
}
function GetCompany()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetCompany(GetCompanyComplete);
}
function GetCompanyComplete(res)
{
    getXml(res);
     ddlCompany=document.getElementById(preid+"ddlCompany");
    ddlCompany.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlCompany.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyID")[0].firstChild.nodeValue;
        ddlCompany.options.add(opt);
    }    
}
//function GetStockGroup()
//{
//    if(UserID==null)
//       GetUserID(); 
//    PageMethods.GetStockGroupName(GetStockGroupComplete);
//}

//function GetStockGroupComplete(res)
//{
//    getXml(res);
//    var ddlStockGroup=document.getElementById(preid+"ddlStockGroup");
//    ddlStockGroup.options.length=0;
//    var opt=document.createElement("OPTION");
////    opt.text="Select";
////    opt.value="0";    
////    ddlStockGroup.options.add(opt);
//    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//    {
//        opt=document.createElement("OPTION");
//        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupName")[0].firstChild.nodeValue;
//        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupID")[0].firstChild.nodeValue;
//        ddlStockGroup.options.add(opt);
//    }    
//}
function GetHeadGroup()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetHeadGroup(HeadGroupComplete);
}
function HeadGroupComplete(res)
{
    getXml(res);
    ddlHeadGroup=document.getElementById(preid+"ddlHeadGroup");
    ddlHeadGroup.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlHeadGroup.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupID")[0].firstChild.nodeValue;
        ddlHeadGroup.options.add(opt);
    }    
}
function GetPurchaseTax()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetPurchaseTaxSystem(PurchaseTaxComplete);
}
function PurchaseTaxComplete(res)
{
    getXml(res);
    PurchaseTaxSystem=document.getElementById(preid+"ddlPurchaseTaxSystem");
    PurchaseTaxSystem.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    PurchaseTaxSystem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TaxName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TaxNo")[0].firstChild.nodeValue;
        PurchaseTaxSystem.options.add(opt);
    }    
}
function GetSaleTax()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetSaleTaxSystem(SaleTaxComplete);
}
function SaleTaxComplete(res)
{
    getXml(res);
    SalesTaxSystem=document.getElementById(preid+"ddlSalesTaxSystem");
    SalesTaxSystem.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    SalesTaxSystem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TaxName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("TaxNo")[0].firstChild.nodeValue;
        SalesTaxSystem.options.add(opt);
    }    
}
function GetCategory()
{
    if(UserID==null)
       GetUserID();     
    PageMethods.GetHeadGroupWiseCategory(ddlHeadGroup.value,CategoryComplete);
}
function CategoryComplete(res)
{
    getXml(res);    
    ddlCategory=document.getElementById(preid+"ddlCategory"); 
    ddlCategory.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlCategory.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Category")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CategoryID")[0].firstChild.nodeValue;
        ddlCategory.options.add(opt);
    }    
}
function GetSubCategory()
{
    if(UserID==null)
       GetUserID();       
    PageMethods.GetCategoryWiseSubCategory(ddlCategory.value,SubCategoryComplete);
}
function SubCategoryComplete(res)
{
    getXml(res);    
    ddlSubCategory=document.getElementById(preid+"ddlSubCategory"); 
    ddlSubCategory.options.length=0;
    var opt=document.createElement("OPTION");   
    opt.text="Select";
    opt.value="0";    
    ddlSubCategory.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("SubCategory")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("SubCategoryID")[0].firstChild.nodeValue;
        ddlSubCategory.options.add(opt);
    }    
}
function btnSave_Click(e,t)
{
    GetControls();
    try
    {              
            if(StockGroupID.value=="0")
            {
                StockGroupID.focus();
                alert("Select StockGroupID");
                return false;
            }
            
            if(ItemCode.value.trim()=="")
            {
                ItemCode.focus();
                alert("Enter ItemCode");
                return false;
            }
            if(StockItemName.value.trim()=="")
            {
               
                StockItemName.focus();
                alert("Enter StockItemName");
                return false;
            }

            if(PurchaseTaxSystem.value=="0")
            {
                PurchaseTaxSystem.focus();
                alert("Select PurchaseTaxSystem");
                return false;
            }
            if(SalesTaxSystem.value=="0")
            {
                SalesTaxSystem.focus();
                alert("Select SalesTaxSystem");
                return false;
            }
            if(ddlHeadGroup.value=="0")
            {
                ddlHeadGroup.focus();
                alert("Select HeadGroup");
                return false;
             }
             if(ReOrderlevel.value=="")
             {
                ReOrderlevel.value=0;                
             }
        var Con=confirm("Are You Sure,Do You Want To Save?");
        if(Con==true)        
            PageMethods.SaveItemCreation(StockGroupID.value, ddlCompany.value, ItemCode.value, StockItemName.value, PurchaseTaxSystem.value, SalesTaxSystem.value,ReOrderlevel.value,BarCode.value,ddlHeadGroup.value,ddlCategory.value,ddlSubCategory.value,UserID.value, SaveComplete);           
     }
     catch(err)
     {
        t.disable=false;
     }    
 }
 function SaveComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Saving");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }    
    else
    {
        alert("Saved successfully");        
        //btnCancel_Click();        
        //GetData();
    }
}



function btnUpdate_Click(e,t)
{
    GetControls();
    try
    {              
            if(StockGroupID.value=="0")
            {
                StockGroupID.focus();
                alert("Select StockGroupID");
                return false;
            }
            
            if(ItemCode.value.trim()=="")
            {
                ItemCode.focus();
                alert("Enter ItemCode");
                return false;
            }
            if(StockItemName.value.trim()=="")
            {
               
                StockItemName.focus();
                alert("Enter StockItemName");
                return false;
            }

            if(PurchaseTaxSystem.value=="0")
            {
                PurchaseTaxSystem.focus();
                alert("Select PurchaseTaxSystem");
                return false;
            }
            if(SalesTaxSystem.value=="0")
            {
                SalesTaxSystem.focus();
                alert("Select SalesTaxSystem");
                return false;
            }
            if(ddlHeadGroup.value=="0")
            {
                ddlHeadGroup.focus();
                alert("Select HeadGroup");
                return false;
             }
             if(ReOrderlevel.value=="")
             {
                ReOrderlevel.value=0;                
             }
        var Con=confirm("Are You Sure,Do You Want To Update?");
        if(Con==true)        
            PageMethods.UpdateItemCreation(ICID.value,StockGroupID.value, ddlCompany.value, ItemCode.value, StockItemName.value, PurchaseTaxSystem.value, SalesTaxSystem.value,ReOrderlevel.value,BarCode.value,ddlHeadGroup.value,ddlCategory.value,ddlSubCategory.value,UserID.value, UpdateComplete);           
     }
     catch(err)
     {
        t.disable=false;
     }    
 }
 function UpdateComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Saving");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }    
    else
    {
        alert("Saved successfully");        
        //btnCancel_Click();        
        //GetData();
    }
}
