﻿var preid = '';
function EditClick(e, t) {    
    btnCancel_Click();
    var CityID= document.getElementById("hidCityID");
    document.getElementById(preid + "txtCity").value = "";
    CityID.value = "";
    var City= t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    var res = confirm("Do you want to Edit the City : " + City);
    if (res == true) {
        CityID.value= t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        document.getElementById(preid + "txtCity").value = City;
        document.getElementById("btnSave").style.display = "none";
        document.getElementById("btnUpdate").style.display = "block";
        document.getElementById("btnCancel").style.display = "block";
    }
}

function ddlCountryChange(e,t)
{
    btnCancel_Click();
    document.getElementById("hidCityID").value = "";
    var ddlState=document.getElementById(preid+"ddlState");
    var City=document.getElementById(preid+"txtCity");
    var tblCity=document.getElementById(preid+"gvCity");
    City.value="";
    ddlState.length=0;        
    var opt=document.createElement("OPTION");    
    opt.text="Select";
    opt.value="0";
    ddlState.options.add(opt);
    for(var i=tblCity.rows.length-1;i>0;i--)    
    {
        tblCity.deleteRow(i);
    }
    
    if(t.value!="0")
    {
        PageMethods.ddlCountry_Change(t.value,ddlCountryChangeComplete);
    }    
     
}

function ddlCountryChangeComplete(res)
{
    
    var xmlObj = getXml(res);
    var ddlState=document.getElementById(preid+"ddlState");
    var opt;
    for (var i = 0; i < xmlObj.getElementsByTagName("States").length; i++) {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("States")[i].getElementsByTagName("StateName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("States")[i].getElementsByTagName("StateID")[0].firstChild.nodeValue;        
        ddlState.options.add(opt);
    }    
}

function ddlStateChange(e,t)
{    
    btnCancel_Click();
    document.getElementById("hidCityID").value = "";
    var City=document.getElementById(preid+"txtCity");
    var tblCity=document.getElementById(preid+"gvCity");
    City.value="";    
    for(var i=tblCity.rows.length-1;i>0;i--)    
    {
        tblCity.deleteRow(i);
    }    
    if(t.value!="0")
    {
        PageMethods.ddlState_Change(t.value,ShowTable);
    }         
}

function btnCancel_Click() {
    document.getElementById(preid + "txtCity").value = "";    
    document.getElementById("hidCityID").value = "";
    document.getElementById("btnSave").style.display = "block";
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "none";
    document.getElementById(preid + "txtCity").focus();
}

function DeleteClick(e, t) {  
    btnCancel_Click();  
    var Citys=t.parentNode.parentNode.cells[2].firstChild.nodeValue;
    var State = document.getElementById(preid+"ddlState").value;
    var res = confirm("Do you want to Delete the City : " + Citys);
    if (res == true) {
        var Cityid = document.getElementById("hidCityID");
        Cityid.value = "";
        Cityid.value= t.parentNode.parentNode.cells[0].firstChild.nodeValue;                
        PageMethods.DeleteClick(State,Cityid.value,ShowTable);
        alert("City Deleted Sucessfully.");
    }
}

function btnUpdate_Click(e,t) {

    var Country = document.getElementById(preid + "ddlCountry").value;
    var State = document.getElementById(preid+"ddlState").value;
//    if(Country=="0")
//    {
//        premsgbox("Select Country","ddlCountry");
//        return false;
//    }
    if(State=="0")
    {
        premsgbox("Select State","ddlState");
        return false;
    }
    if (document.getElementById(preid + "txtCity").value == "") {        
        premsgbox("Enter CityName","txtCity");
        return false;
    }    
    
    var Cityname = document.getElementById(preid + "txtCity").value;
    t.disabled = true;
    var CityID= document.getElementById("hidCityID");
    PageMethods.btnUpdate_Click(Country,State,CityID.value,Cityname,UpdateComplete);
}

function UpdateComplete(res)
{
try
{
    if (res == "" || res == "0") {
        alert("Error in updation.");        
        return;
    }
    if (res == "-1") {
        alert("Service already exist.");        
        return;
    }
    if (res == "-2") {
        alert("Select Country.");        
        return;
    }
    if (res == "-3") {
        alert("Select State.");        
        return;
    }
    if (res == "-4") {
        alert("Enter City.");        
        return;
    }
    ShowTable(res);
    
    btnCancel_Click();
    alert("City Updated Successfully.");
    }
    catch(err)
    {
    }
    finally
    {
    document.getElementById("btnUpdate").disabled = false;
    }
}

function btnSave_Click(e,t) {
    var Country = document.getElementById(preid + "ddlCountry").value;
    var State = document.getElementById(preid+"ddlState").value;
//    if(Country=="0")
//    {
//        premsgbox("Select Country","ddlCountry");
//        return false;
//    }
    if(State=="0")
    {
        premsgbox("Select State","ddlState");
        return false;
    }
    if (document.getElementById(preid + "txtCity").value == "") {        
        premsgbox("Enter CityName","txtCity");
        return false;
    }    
    
    var Cityname = document.getElementById(preid + "txtCity").value;
    t.disabled = true;
    PageMethods.btnSave_Click(Country,State,Cityname,SaveComplete);
    
}

function SaveComplete(res) {
try
{
    if (res == "" || res == "0") {
        alert("Error in insertion.");        
        return;
    }
    if (res == "-1") {
        alert("Service already exist.");        
        return;
    }
    if (res == "-2") {
        alert("Select Country.");        
        return;
    }
    if (res == "-3") {
        alert("Select State.");        
        return;
    }
    if (res == "-4") {
        alert("Enter City.");        
        return;
    }
    ShowTable(res);
    document.getElementById(preid+"txtCity").value="";
    
    alert("City Saved Successfully.");    
    }
    catch(err)
    {
    }
    finally    
    {
        document.getElementById("btnSave").disabled = false;
    }
}

function ShowTable(res)
    {
    var xmlObj = getXml(res);
    var tbl = document.getElementById(preid + "gvCity");
    
    for (var i = tbl.rows.length - 1; i > -0; i--) {
        tbl.deleteRow(i);
    }    
    tbl.rows[0].cells[1].style.position = 'relative';
    tbl.rows[0].cells[1].style.width = 100;
    for (var i = 0; i < xmlObj.getElementsByTagName("Citys").length; i++) {
        tbl.insertRow(i + 1)
        tbl.rows[i + 1].insertCell(0);
        tbl.rows[i + 1].insertCell(1);
        tbl.rows[i + 1].insertCell(2);  
        tbl.rows[i + 1].cells[0].className="hidendStyle";      
        tbl.rows[i + 1].cells[0].innerHTML = xmlObj.getElementsByTagName("Citys")[i].getElementsByTagName("CityID")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[1].innerHTML = xmlObj.getElementsByTagName("Citys")[i].getElementsByTagName("CityName")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[2].innerHTML = "<a href='#' onclick='EditClick(event,this)' class='gridNavButtons'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='DeleteClick(event,this)' class='gridNavButtons'>Delete</a>";        
        tbl.rows[i + 1].cells[0].style.display="none";
    }
    }

function getXml(xmlString) {
    var xmlObj;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    return xmlObj;
}

function premsgbox(msg, cntrl) {
    try {
        document.getElementById(preid + cntrl).focus();
        alert(msg);        
    } catch (error) { }

}