﻿function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}

//function Test()
//{
//    alert("Call Test method");
//}
var strFrom1;
var strTo1;
var strPayDate1;

function PrintCustomerSlipDataJS(strFrom,strTo,strPayDate)
{
    var tmp;    
    var i=0;
    getXml(document.getElementById("hidXML").value);    

    document.getElementById("tdHead").innerText = "STATEMENT FOR THE WEEK FROM :" + strFrom + " TO :" + strTo;       
    strFrom1=strFrom;
    strTo1=strTo;
    strPayDate1=strPayDate;
    //ID
    tmp=" ";
    if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CID")[0]!=null)
        tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CID")[0].firstChild.nodeValue;                    
    document.getElementById("tdCID").innerText=tmp;   
    
    //Name             
    tmp=" ";
    if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CName")[0]!=null)
        tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CName")[0].firstChild.nodeValue;
    document.getElementById("tdName").innerText=tmp;                
    
    //Address
    tmp=" ";
    if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Address")[0]!=null)
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Address")[0].firstChild!=null)
            tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Address")[0].firstChild.nodeValue;        
    document.getElementById("tdAddr").innerText=tmp;   
    //Phone             
    tmp=" ";
    if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Phone")[0]!=null)
        if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Phone")[0].firstChild!=null)
            tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Phone")[0].firstChild.nodeValue;
    document.getElementById("tdPhone").innerText=tmp;                
    
    //Commission for the week
    tmp=0;
    if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToPay")[0]!=null)
        tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToPay")[0].firstChild.nodeValue;        
    tmp= Math.round(tmp);
    document.getElementById("tdCW").innerText=tmp.toFixed(2);   
    var comm;
    comm= Math.round(tmp*15/100);
    
    //Service
    document.getElementById("tdSTds").innerText=comm.toFixed(2);                
    //NetAmt
    tmp=tmp-comm;
    document.getElementById("tdNetAmt").innerText=tmp.toFixed(2);
                    
    //InWords
    document.getElementById("tdInWords").innerText=NumberInWords(tmp);
    //PayDate
    document.getElementById("tdPayDate").innerText=strPayDate;
    window.print();
    if(xmlObj.getElementsByTagName("Table").length>1)
        setTimeout("callloop(1)",1000);
}
function callloop(cnt)
{
    var i=cnt;
    //document.getElementById("tdHead").innerText = "STATEMENT FOR THE WEEK FROM :" + strFrom1 + " TO :" + strTo1;       
    document.getElementById("tdHead").innerText = "STATEMENT FOR THE WEEK FROM :" + strFrom1 + " TO :" + strTo1;       
        //ID
        tmp=" ";
        if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CID")[0]!=null)
            tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CID")[0].firstChild.nodeValue;                    
        document.getElementById("tdCID").innerText=tmp;   
        
        //Name             
        tmp=" ";
        if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CName")[0]!=null)
            tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CName")[0].firstChild.nodeValue;
        document.getElementById("tdName").innerText=tmp;                
        
        //Address
        tmp=" ";
        if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Address")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Address")[0].firstChild!=null)
                tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Address")[0].firstChild.nodeValue;        
        document.getElementById("tdAddr").innerText=tmp;   
        //Phone             
        tmp=" ";
        if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Phone")[0]!=null)
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Phone")[0].firstChild!=null)
                tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Phone")[0].firstChild.nodeValue;
        document.getElementById("tdPhone").innerText=tmp;                
        
        //Commission for the week
        tmp=0;
        if (xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToPay")[0]!=null)
            tmp=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ToPay")[0].firstChild.nodeValue;        
        tmp= Math.round(tmp);
        document.getElementById("tdCW").innerText=tmp.toFixed(2);   
        var comm;
        comm= Math.round(tmp*15/100);
        
        //Service
        document.getElementById("tdSTds").innerText=comm.toFixed(2);                
        //NetAmt
        tmp=tmp-comm;
        document.getElementById("tdNetAmt").innerText=tmp.toFixed(2);
                        
        //InWords
        document.getElementById("tdInWords").innerText=NumberInWords(tmp);
        //PayDate
        document.getElementById("tdPayDate").innerText=strPayDate1;
        window.print();
        if(xmlObj.getElementsByTagName("Table").length-1>cnt)
            setTimeout("callloop("+(cnt+1)+")",1000);
}