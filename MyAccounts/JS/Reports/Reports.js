﻿var dtpFrom;
var dtpTo;
var UserID;
var preid='ctl00_ContentPlaceHolder1_';
var ddlHG;
var ddlGroup;
var ddlCompany;
var ddlBranch;
var hidGBID;
var preid;
function getXml(xmlString)
{
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById(preid+"hidGBID");
}

function GetControls()
{
    dtpFrom = document.getElementById(preid+"txtFromDate");
    dtpTo = document.getElementById(preid+"txtToDate");
    //ddlHG = document.getElementById("ddlHG");
    ddlGroup =document.getElementById("ddlStcokGroup");
    ddlCompany=document.getElementById("ddlCompany");
    ddlBranch = document.getElementById("ddlBranch");
}
//function GetDefaults()
//{
//    dtpFrom.value = new Date().format("dd-MM-yyyy");
//    dtpTo.value=new Date().format("dd-MM-yyyy");    
//}
function GetUserID()
{
    UserID=document.getElementById(preid+"hidUserID");
}
// function GetHeadGroups()
//{
//    PageMethods.GetHeadGroup( GetHeadGroupsComplete);    
//}

//function GetHeadGroupsComplete(res)
//{
//    getXml(res);    
//    ddlHG.options.length=0;
//    var opt=document.createElement("OPTION");
//    opt.text="Select";
//    opt.value="0";    
//    ddlHG.options.add(opt);
//    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//    {
//        opt=document.createElement("OPTION");
//        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupName")[0].firstChild.nodeValue;
//        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupID")[0].firstChild.nodeValue;
//        ddlHG.options.add(opt);
//    }    
//}
function GetStockGroup()
{    
    PageMethods.GetStockGroup(GetStockGroupComplete);    
}

function GetStockGroupComplete(res)
{
    getXml(res);    
    ddlGroup.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlGroup.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupID")[0].firstChild.nodeValue;
        ddlGroup.options.add(opt);
    }    
}
function GetCompany()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetCompany(GetCompanyComplete);    
}

function GetCompanyComplete(res)
{
    getXml(res);    
    ddlCompany.options.length=0;
    var opt=document.createElement("OPTION");
    //opt.text="Select";
    opt.text="All";
    opt.value="0";    
    ddlCompany.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("CompanyID")[0].firstChild.nodeValue;
        ddlCompany.options.add(opt);
    }    
}
function SetData()
{
    GetControls();    
    GetBranch();
    //GetHeadGroups();
    GetStockGroup();
    GetCompany();
//    GetDefaults();
    ddlBranch.focus();
}
function chkAll_Click(e,t)
{
    if(t.checked==true)
    {
        document.getElementById("spnFromDate").style.display="none";
        document.getElementById("txtFromDate").style.display="none";
        document.getElementById("spnToDate").style.display="none";
        document.getElementById("txtToDate").style.display="none";
    }
    else
    {
        document.getElementById("spnFromDate").style.display="block";
        document.getElementById("txtFromDate").style.display="block";
        document.getElementById("spnToDate").style.display="block";
        document.getElementById("txtToDate").style.display="block";
    }
}
function GetBranch()
{    
    if(UserID==null)
        GetUserID();    
    PageMethods.GetBranch(UserID.value, GetBranchComplete);    
}

function GetBranchComplete(res)
{
    getXml(res);    
    ddlBranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlBranch.options.add(opt);
        ddlBranch.value="0";
    }  
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlBranch.options.add(opt);
    }       
    if(hidGBID==null)
           GetDefaultBranch();
    ddlBranch.value=hidGBID.value;
}