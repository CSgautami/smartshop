﻿var UserID=null;
var preid = '';
var tdItem;
var BatchNo;
var MRP;
var Price;
var FreeQty;
var Quantity;
var ddlbranch;
var StockID;
var tblGrid;
//var StockID;
var tdItem;
var hidBranchID;
var hidStockID=0;
//var lblItem;
var divItem;
var ddlItem;
var hidGBID;

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}


function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}
function GetDefaultBranch()
{
    hidGBID=document.getElementById("hidGBID");
}
function GetBranch()
{
    if(UserID==null)
       GetUserID(); 
    PageMethods.GetBranch(UserID.value, GetBranchComplete);    
}

function GetBranchComplete(res)
{
    getXml(res);    
    ddlbranch.options.length=0;
    var opt=document.createElement("OPTION");
    if(xmlObj.getElementsByTagName("Table").length != 1)
    {
        opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";    
        ddlbranch.options.add(opt);
    }         
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
        ddlbranch.options.add(opt);
    }  
    if(hidGBID==null)
        GetDefaultBranch();
    ddlbranch.value=hidGBID.value;        
    if(ddlbranch.value!="")
        GetData();
}
function SetData()
{
    GetControls();
    GetBranch();
}
function GetControls()
{
    
    hidStockID=document.getElementById(preid+"hidStockID");    
    ddlbranch=document.getElementById(preid+"ddlbranch");   
    tdItem=document.getElementById(preid+"tdItem");
    BatchNo =document.getElementById(preid+"txtBatchNo");
    MRP =document.getElementById(preid+"txtMRP");    
    Price =document.getElementById(preid+"txtPrice"); 
    Quantity=document.getElementById(preid+"txtQuantity");   
    UserID=document.getElementById("hidUserID");
    tblGrid =document.getElementById("tblGrid");
    //lblItem =document.getElementById(preid+"lblItem");
    divItem=document.getElementById("divItem"); 
}

function btnSave_Click(e,t)
{
    GetControls();
    try
    {        

        UserID=document.getElementById("hidUserID");
        if (ddlbranch.value==0)
        {
            alert("select ddlbranch");
            ddlbranch.focus();
            return false;
        } 
//        if (Item.value==0)
//        {
//            alert("select Item");
//            Item.focus();
//            return false;
//        } 

        if (selectedItem=="0"||selectedItem=="")
        {
            alert("Select Item");
            tdItem.focus();
            return false;        
        }  
        if (BatchNo.value=="")
        {
            alert("Enter BatchNo");
            BatchNo.focus();
            return false;
        } 
  
        if (MRP.value=="")
        {
            alert("Enter MRP");
            MRP.focus();
            return false;
        } 
        if (Price.value=="")
        {
            alert("Enter Price");
            Price.focus();
            return false;
        } 
        if (Quantity.value=="")
        {
            alert("Enter quantity");
            Quantity.focus();
            return false;
        } 

        var Con=confirm("Are You Sure,Do You Want To Save?");
        if(Con=true)        
            PageMethods.SaveOpeningStock(ddlbranch.value,selectedItem, BatchNo.value,MRP.value,Price.value,Quantity.value,UserID.value, SaveOpeningStockComplete);           
     }
     catch(err)
     {
        t.disable=false;
     }    
 }
 function SaveOpeningStockComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Saving");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }    
    else
    {
        alert("Saved successfully");      
        GetData();
    }
}
function btnUpdate_Click(e,t)
{
    GetControls();
    try
    {        
       
        UserID=document.getElementById("hidUserID");
        if (ddlbranch.value==0)
        {
            alert("select ddlbranch");
            ddlbranch.focus();
            return false;
        } 

        if (selectedItem=="0"||selectedItem=="")
        {
            alert("Select Item");
            tdItem.focus();
            return false;        
        }  
        if (BatchNo.value=="")
        {
            alert("Enter BatchNo");
            BatchNo.focus();
            return false;
        } 
  
        if (MRP.value=="")
        {
            alert("Enter MRP");
            MRP.focus();
            return false;
        } 
        if (Price.value=="")
        {
            alert("Enter Price");
            Price.focus();
            return false;
        } 
        if (Quantity.value=="")
        {
            alert("Enter quantity");
            Quantity.focus();
            return false;
        } 

        var Con=confirm("Are You Sure,Do You Want To Update");
        if(Con=true)     
            PageMethods.UpdateOpeningStock(hidStockID.value,ddlbranch.value,selectedItem, BatchNo.value,MRP.value,Price.value,Quantity.value,UserID.value, UpdateOpeningStockComplete);           
     }
     catch(err)
     {
        t.disable=false;
     }    
 }
 function UpdateOpeningStockComplete(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Updating");        
    }
    else if(res.substring(0,1)=="E")
    {
        alert(res);
    }    
    else
    {
        alert("Updated successfully");      
        btnCancel_Click();        
        GetData();
    }
}
function btnCancel_Click()
{
    
    selectedItem="0";
    tdItem.value="";
    //lblItem.innerText="";
    divItem.innerHTML="";
    BatchNo.value="";    
    MRP.value="";
    Price.value="";
    Quantity.value="";
    tdItem.focus();
    ActivateSave();
    ddlbranch.focus();
      
}
function Cleartable()
{
    tblGrid =document.getElementById("tblGrid");
    for(var i=tblGrid.rows.length-1;i>0;i--)
    {
       tblGrid.deleteRow(i);
   
    }
}


function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}
function Cleartable()
{
    tblGrid =document.getElementById("tblGrid");
    for(var i=tblGrid.rows.length-1;i>0;i--)
    {
       tblGrid.deleteRow(i);
   
    }
}
function GetData()
{
    try 
    {
        GetControls();
        PageMethods.GetData(ddlbranch.value,GetDataComplete);        
    }
    catch(Err)
    {
    }
}
function GetDataComplete(res)
{
    try
    {
        GetControls();         
        btnCancel_Click();       
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            //alert("No data found.");
            Cleartable();
            return;
        }
        else
        {               
            tblGrid=document.getElementById("tblGrid");
            for(var i=tblGrid.rows.length-1;i>0;i--)
                tblGrid.deleteRow(i);
            var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<xmlObjLength;i++)
            {                
//                if(tblGrid.rows.length==1)
//                    var tr=tblGrid.insertRow(tblGrid.rows.length);
//                else
//                    var tr=tblGrid.insertRow(tblGrid.rows.length-1);
                var tr=tblGrid.insertRow(i+1);
                var j;
                for(j=0;j<8;j++)
                {
                    tr.insertCell(j);
                }
                var StockID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockID")[0].firstChild.nodeValue,10);                        
                hidStockID.value=StockID;
                tr.cells[0].innerHTML=hidStockID.value;
                tr.cells[0].style.display="none";                                               
                tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
                tr.cells[1].style.display="none";                
                var ItemID =parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue,10);                
                
                tr.cells[2].innerHTML=ItemID;
                tr.cells[2].style.display="none";        
               
//                tr.cells[1].innerHTML=ItemID;
//                tr.cells[1].style.display="none";                
                tr.cells[3].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Item")[0].firstChild.nodeValue;
                tr.cells[4].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
                tr.cells[5].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("MRP")[0].firstChild.nodeValue;                
                tr.cells[6].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Price")[0].firstChild.nodeValue;                
                tr.cells[7].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Quantity")[0].firstChild.nodeValue;
                tr.cells[5].style.textAlign="right";
                tr.cells[6].style.textAlign="right";
                
                
                tr.onmouseover=function(){MouseOver(this);};
                tr.onmouseout=function(){MouseOut(this);};
                tr.onclick=function(){tblClick(this);}      
                //ActivateUpdate();
            }
         }
     }
    catch(err)
    {
    }    
 }
    function MouseOver(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {
            t.style.backgroundColor="gray";
            t.style.cursor="hand";     
            t.title="Click to Edit the values";  
        }
    }

    function MouseOut(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {
            t.style.backgroundColor="";
            t.style.crusor="text";        
        }
    }


function tblClick(t)
{    
    var Con=confirm("Are You Sure,Do You Want To Edit?");
    if(Con=true)
    {
    if(t.cells[0].innerHTML.trim()!="")
    {      
        GetControls();                         
        selectedItem=t.cells[2].innerHTML.trim();
        tdItem.value=t.cells[3].innerHTML.trim();                
        //lblItem.innerText=t.cells[1].innerHTML.trim();                
        BatchNo.value=t.cells[4].innerHTML.trim();        
        MRP.value=t.cells[5].innerHTML.trim();
        Price.value=t.cells[6].innerHTML.trim();
        Quantity.value=t.cells[7].innerHTML.trim();
        document.getElementById("tblGrid").deleteRow(t.rowIndex);  
        ActivateUpdate();
    }
    }
}



var divItem;
var selectedItem="";
var prevText="";

function FillItem(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedItem="";
        divItem=document.getElementById("divItem");
        if(t.value!="")
        {        
            PageMethods.CallFillItem(t.value,FillItemComplete)
        }
        else{
        divItem.innerHTML="";
        divItem.style.display="none";
        }
    }
}

function FillItemComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var ItemID;
            var ItemName;
            var ItemCode;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild!=null)
                    ItemID=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild!=null)
                    ItemName=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            //lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild!=null)
                    //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                    ItemCode =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            //lname=lname+")";
            if(i==0)
            {
                tbl = tbl + "<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>" + ItemID + "</td><td>" + ItemCode + '-' + ItemName + "</td></tr>";
                selectedItem=ItemID;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl = tbl + "<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>" + ItemID + "</td><td>" + ItemCode + '-' + ItemName + "</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divItem.innerHTML=tbl;
        if(count>0)
            divItem.style.display="block";
        else
            divItem.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectClick(t)
{
    selectedItem=t.cells[0].innerHTML;
    var txt=document.getElementById(preid+"tdItem");
    ApplyItem(txt);
}
function OnMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedItem ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedItem=t.cells[0].innerHTML;
}
function selectItem(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyItem(t);
    }
    else
    {
        if(divItem==null)
            divItem=document.getElementById("divItem");
        var tbl=divItem.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedItem=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedItem=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyItem(t)
{
    if(divItem==null)
            divItem=document.getElementById("divItem");
    if(selectedItem!="")
    {   
        var tbl=divItem.firstChild;
        var cnt=0;
        if(tbl!="" && tbl!=null)
        { 
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedItem="";
                t.value="";
            }
        }        
    }
    else
        t.value="";
    prevText=t.value;
    divItem.style.display="none";
    
}
function GetItemName()
{
   //Item=document.getElementById(preid+"ddlItem");
   PageMethods.GetItemName(selectedItem,CompleteItemName);  
}
function CompleteItemName(res)
{
    try 
    {
        getXml(res);        
        //lblItem=document.getElementById(preid+"lblItem");
        if(xmlObj.getElementsByTagName("Table").length==0)
        {          
            //lblItem.innerText="";            
            return;            
        }
        else
        {            
            //lblItem.innerText =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
        }      
    }
    catch(err)
    {
    }
}
function GetStockItem()
{ 
    //PageMethods.GetStockItem(GetStockItemComplete);    
}

function GetStockItemComplete(res)
{
    getXml(res);    
    ddlItem.options.length=0;
    var opt=document.createElement("OPTION");
    opt.text="Select";
    opt.value="0";    
    ddlItem.options.add(opt);
    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
    {
        opt=document.createElement("OPTION");
        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
        ddlItem.options.add(opt);
    }    
}
function btnDelete_Click()
{
    try 
    {
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
            PageMethods.btnDelete_Click(hidStockID.value,ddlbranch.value,DeletComplete)
            //PageMethods.Testing();
    }
    catch(err)
    {
    }
}
function DeletComplete(res)
{
    if(res=="-1")
    {
        alert("Can't Delete Transaction Exists");        
    }
    else 
    {
        alert("Deleted Successfully");
        btnCancel_Click();
    }
  }

function ActivateUpdate()
{    
     document.getElementById("btnSave").style.display="none";
     document.getElementById("btnUpdate").style.display="block";   
     document.getElementById("btnDelete").style.display="block";     
}
function  ActivateSave()
{    
    document.getElementById("btnSave").style.display="block";
    document.getElementById("btnUpdate").style.display="none";    
    document.getElementById("btnDelete").style.display="none";    
}