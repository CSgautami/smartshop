﻿var preid="";
var UserID=null;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}
var BranchID;
var group;
var groupcode;

function getXml(xmlString) 
{
    xmlObj=null;
    try 
    {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") 
        {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
         }
         else 
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");

    }
    catch (err) {}
}


function validatesave()
{
    BranchID=document.getElementById("ddlBranch");
    group=document.getElementById("txtGroupName");
    groupcode=document.getElementById("ddlGCode");
    
    if(BranchID.value=="0")
    {
        BranchID.focus();
        alert("Select BranchID");
        return false;
    }
    if(group.value.trim()=="")
    {
        group.value="";
        group.focus();
        alert("Enter GroupName");
        return false;
    }
    if(groupcode.value=="0")
    {
        groupcode.focus();
        alert("Enter Groupcode.");
        return false;
    }
    return true;
}

function validateUpdate()
{
    BranchID=document.getElementById("ddlBranch");
    group=document.getElementById("txtGroupName");
    groupcode=document.getElementById("ddlGCode");
    GetCntrls();
    
    if(BranchID.value=="0")
    {
        BranchID.focus();
        alert("Select BranchID");
        return false;
    }
    if(GroupName.value.trim()=="")
    {
        GroupName.value="";
        GroupName.focus();
        alert("Enter GroupName");
        return false;
    }
    if(ddlCode.value=="0")
    {
        ddlCode.focus();
        alert("Enter Groupcode.");
        return false;
    }
    if(ddlStatus.value=="0")
    {
        ddlStatus.focus();
        alert("Enter Groupcode.");
        return false;
    }
    return true;
}
//function GetBranch()
//{
//    if(UserID==null)
//       GetUserID(); 
//    PageMethods.GetBranch(GetBranchComplete);    
//}

//function GetBranchComplete(res)
//{
//    getXml(res);    
//    ddlBranch.options.length=0;
//    var opt=document.createElement("OPTION");
//    opt.text="Select";
//    opt.value="0";    
//    ddlBranch.options.add(opt);
//    for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//    {
//        opt=document.createElement("OPTION");
//        opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
//        opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
//        ddlBranch.options.add(opt);
//    }    
//}



var Flag;
var navbtn;
var PackID=0;
function btnGMRecordNav_Click(e,t)
{    
    GetCntrls();
    if (BranchID.value=="0")    
        return;
    try    
    {        
        Flag="F";
        navbtn=t;
        if(t.id=="btnFirst")
            Flag="F";
        else if(t.id=="btnPrev")
        {
           if(PackID==0)
              Flag="F";
           else
              Flag="P";
        }
        else if(t.id=="btnNext")
             Flag="N";
        else if(t.id=="btnLast")
            Flag="L";    
        
        if(UserID==null)
            GetUserID();
        navbtn.disabled=true;
     
        PageMethods.GetGMNavData(BranchID.value,PackID,Flag,GetGMNavDataComplete);
    }
    catch(err)
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
    
}

function GetGMNavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
        else
        {
            GetCntrls();
            GMID=parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("GroupID")[0].firstChild.nodeValue,10);            
            hiddenGMID.value=GMID;
            //BranchID.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            GroupName.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("GroupName")[0].firstChild.nodeValue;
            ddlCode.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("GroupCode")[0].firstChild.nodeValue;
            var Status=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Status")[0].firstChild.nodeValue;
            ddlStatus.text=Status;
            ddlStatus.value=Status;
            var ch= xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("Bank")[0].firstChild.nodeValue;
            if(ch=="true")
               chkBank.checked=true;
            else
              chkBank.checked=false;
            ActivateUpdate();
            
       }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
            
    }
}

function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}

function GetCntrls()
{
     hiddenGMID = document.getElementById(preid + "HiddenGMID");
     BranchID=document.getElementById(preid+"ddlBranch");
     GroupName = document.getElementById(preid + "txtGroupName");
     ddlCode = document.getElementById(preid+"ddlGCode");
     ddlStatus=document.getElementById(preid+"ddlstatus");
     chkBank = document.getElementById(preid+"chkbank");
}
function ActivateUpdate()
{
     document.getElementById("btnsave").style.display="none";
     document.getElementById("btnupdate").style.display="block";
     document.getElementById("btnCancel").style.display="block";
     document.getElementById("btnDelete").style.display="block";
     //btnCancel_Click();
  
}

function  ActivateSave()
{
    document.getElementById("btnsave").style.display="block";
    document.getElementById("btnupdate").style.display="none";
    document.getElementById("btnCancel").style.display="none";
    document.getElementById("btnDelete").style.display="none";
    GetCntrls();
    hiddenGMID.value="";
    BranchID.value="";
    GroupName.value="";
    ddlCode.value=0;
    ddlStatus.value=0;
    chkBank.checked=false;
}
