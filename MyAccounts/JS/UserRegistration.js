﻿var preid="";
var xmlObj;
function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
}

function ddlCountry_Change(e,t)
{   
    var ddlState=document.getElementById(preid+"ddlState");
    var ddlCity=document.getElementById(preid+"ddlCity");
    var tdOtherCity=document.getElementById(preid+"tdOtherCity");
    var txtOtherCity=document.getElementById(preid+"txtOtherCity");
    tdOtherCity.style.display="none";
    txtOtherCity.value="";
    ddlState.options.length=0;
    ddlCity.options.length=0;
    var opt=document.createElement("OPTION");    
    opt.text="Select";
    opt.value="0";
    ddlState.options.add(opt);
    opt=document.createElement("OPTION");    
    opt.text="Select";
    opt.value="Select";
    ddlCity.options.add(opt);
    if(t.value!=0)
        PageMethods.ddlCountry_Change(t.value,ddlCountry_ChangeComplete);
}
function ddlCountry_ChangeComplete(res)
{
    getXml(res);
    var ddlState=document.getElementById(preid+"ddlState");
    var opt;
    for (var i = 0; i < xmlObj.getElementsByTagName("States").length; i++)
    {
        opt=document.createElement("OPTION");    
        opt.text=xmlObj.getElementsByTagName("States")[i].getElementsByTagName("StateName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("States")[i].getElementsByTagName("StateID")[0].firstChild.nodeValue;
        ddlState.options.add(opt);
    }
    
}
function ddlState_Change(e,t)
{
    var ddlCity=document.getElementById(preid+"ddlCity");
    var tdOtherCity=document.getElementById(preid+"tdOtherCity");
    var txtOtherCity=document.getElementById(preid+"txtOtherCity");
    tdOtherCity.style.display="none";
    txtOtherCity.value="";
    ddlCity.options.length=0;
    var opt=document.createElement("OPTION");         
    opt.text="Select";
    opt.value="Select";
    ddlCity.options.add(opt);
    if(t.value!=0)
        PageMethods.ddlState_Change(t.value,ddlState_ChangeComplete);
}
function ddlState_ChangeComplete(res)
{
    getXml(res);
    var ddlCity=document.getElementById(preid+"ddlCity");
    var opt;
    for (var i = 0; i < xmlObj.getElementsByTagName("Cities").length; i++)
    {
        opt=document.createElement("OPTION");    
        opt.text=xmlObj.getElementsByTagName("Cities")[i].getElementsByTagName("CityName")[0].firstChild.nodeValue;
        opt.value=xmlObj.getElementsByTagName("Cities")[i].getElementsByTagName("CityID")[0].firstChild.nodeValue;
        ddlCity.options.add(opt);
    }
    opt=document.createElement("OPTION");         
    opt.text="Other";
    opt.value="0";
    ddlCity.options.add(opt);
}

function ddlCity_Change(e,t)
{
    var tdOtherCity=document.getElementById(preid+"tdOtherCity");
    var txtOtherCity=document.getElementById(preid+"txtOtherCity");
    if(t.value=="0")
    {        
        tdOtherCity.style.display="block";
        txtOtherCity.value="";
    }
    else
    {
        tdOtherCity.style.display="none";
        txtOtherCity.value="";
    }
}

function RegisterUser(e,t)
{
    var UName=document.getElementById(preid+"txtUserName");
    var pwd=document.getElementById(preid+"txtPassword");
    var address=document.getElementById(preid+"txtAddress");
    var country=document.getElementById(preid+"ddlCountry");
    var state=document.getElementById(preid+"ddlState");
    var city=document.getElementById(preid+"ddlCity");
    var ocity=document.getElementById(preid+"txtOtherCity");
    var emailid=document.getElementById(preid+"txtEmailID");
    var phnos=document.getElementById(preid+"txtPhoneNos");
    var mobnos=document.getElementById(preid+"txtMobileNos");
    if(UName.value.trim()=="")
    {
        alert("Enter UserName");
        UName.focus();
        return false;
    }
    if(pwd.value.trim()=="")
    {
        alert("Enter Password");
        pwd.focus();
        return false;
    }
    if(address.value.trim()=="")
    {
        alert("Enter Address");
        address.focus();
        return false;
    }
    if(country.value=="0")
    {
        alert("Select Country");
        country.focus();
        return false;
    }
    if(state.value=="0")
    {
        alert("Select State");
        state.focus();
        return false;
    }
    if(city.value=="Select")
    {
        alert("Select City");
        city.focus();
        return false;
    }
    t.disabled=true;
    PageMethods.RegisterUser(UName.value,pwd.value,address.value,city.value,ocity.value,state.value,country.value,emailid.value,phnos.value,mobnos.value,RegisterUserComplete);
}

function RegisterUserComplete(res)
{
    try
    {
        if(res==""||res=="0")
        {
            alert("UserName Already Exists.");
            return;
        }
        if(res==""||res=="0")
        {
            alert("Error While Registration, Please Try Later.");
            return;
        }
        else if(res=="1")
        {
            alert("User Registration success.");
            window.parent.location="login.aspx";
        }
        else
        {
            alert("Error While Registration, Please Try Later.");
            return;
        }
    }
    catch(err)
    {
    }
    finally{
    document.getElementById("btnRegister").disabled=false;
    }
}