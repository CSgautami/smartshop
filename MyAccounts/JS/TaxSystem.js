﻿var preid = '';
var UserID;

String.prototype.trim = function ()
{
  return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

var xmlObj;
function getXml(xmlString) 
{
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {}
    
}
function GetMasterData()
{
    GetVatMaster();     
    GetVatTypeMaster();
}

function GetVatMaster()
{
    UserID=document.getElementById(preid+"hidUserID");
    PageMethods.GetVatMaster(GetVatMasterCompleted);
}
function GetVatMasterCompleted(res)
{
   getXml(res);
   if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
  else
  {
        var ddlVat=document.getElementById(preid+"ddlVat");
        var opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";
        ddlVat.options.add(opt);                       
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length;i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("VatDesc")[0].firstChild.nodeValue; 
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("VatID")[0].firstChild.nodeValue;
            ddlVat.options.add(opt);
        } 
  }       
}
function GetVatTypeMaster()
{
    UserID=document.getElementById(preid+"hidUserID");
    PageMethods.GetVatTypeMaster(GetVatTypeMasaterCompleted);
    
}
function GetVatTypeMasaterCompleted(res)
{
    getXml(res);
   if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found.");
            return;
        }
  else
  {
        var ddlType=document.getElementById(preid+"ddlType");
        var opt=document.createElement("OPTION");
        opt.text="Select";
        opt.value="0";
        ddlType.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length;i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("VatDesc")[0].firstChild.nodeValue; 
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("VatID")[0].firstChild.nodeValue;
            ddlType.options.add(opt);            
        } 
  }          
}