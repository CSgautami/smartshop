﻿function GetColTotal(t,c)
{
   var i;
   var nCnt=0;
   for(i=1;i<t.rows.length;i++)
   {
        var tr=t.rows[i];            
        if(!isNaN(tr.cells[c].innerHTML.trim()))
                nCnt =nCnt + parseFloat(tr.cells[c].innerHTML);        
   }
   return nCnt.toFixed(2);
}