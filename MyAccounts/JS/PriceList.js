﻿var preid='';
var UserID;

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}
var ItemID;
var MRP;
var PurchasePrice;
var SalesPrice;
var TaxNo;
var Flag;
var navbtn;
var PLID=0;
function getXml(xmlString) 
{
    xmlObj=null;
    try 
    {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") 
        {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
         }
         else 
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");

    }
    catch (err) {}
}


function ValidateSave()
{
    GetCntrls();
    if(ItemID.value=="0")
    {
        ItemID.focus();
        alert("Select Item");
        return false;
    }
    
    if(MRP.value.trim()=="")
    {
        MRP.value="";
        MRP.focus();
        alert("Enter MRP");
        return false;
    }
    if(PurchasePrice.value.trim()=="")
    {
        PurchasePrice.value="";
        PurchasePrice.focus();
        alert("Enter PurchasePrice");
        return false;
    }
    
    if(SalesPrice.value.trim()=="")
    {
        SalesPrice.value="";
        SalesPrice.focus();
        alert("Enter SalesPrice");
        return false;
    }
    if(TaxNo.value=="0")
    {
        TaxNo.focus();
        alert("Select TaxSystem ");
        return false;
    }   
    
    return true;
}
function btnPLRecordNav_Click(e,t)
{    
    try    
    {
        Flag="F";
        navbtn=t;
        if(t.id=="btnFirst")
            Flag="F";
        else if(t.id=="btnPrev")
        {
           if(PLID==0)
              Flag="F";
           else
              Flag="P";
        }
        else if(t.id=="btnNext")
             Flag="N";
        else if(t.id=="btnLast")
             Flag="L";     
        
        
        if(UserID==null)
            GetUserId();
        navbtn.disabled=true;
     
        PageMethods.GetPLNavData(UserID.value,PLID,Flag,GetPLNavDataComplete);
    }
    catch(err)
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    }
    
}

function GetPLNavDataComplete(res)
{
    try
    {
        getXml(res);
        if(xmlObj.getElementsByTagName("Table").length==0)
        {
            alert("No data found");
            return;
        }
        else
        {
            GetCntrls();
            PLID =parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PLID")[0].firstChild.nodeValue,10);                        
            hidPLID.value=PLID;
            ItemID.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ItemID")[0].firstChild.nodeValue;
            MRP.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("MRP")[0].firstChild.nodeValue;
            PurchasePrice.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PurchasePrice")[0].firstChild.nodeValue;
            SalesPrice.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("SalesPrice")[0].firstChild.nodeValue;
            TaxNo.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("TaxNo")[0].firstChild.nodeValue;
            ActivateUpdate();
            
       }
    }
    catch(err)
    {
    }
    finally
    {
        if(navbtn!=null)
            navbtn.disabled=false;
            
    }
}

function GetUserId()
{
    UserID=document.getElementById("hidUserId");
}
function GetCntrls()
{
     hidPLID=document.getElementById(preid+"hidPLID");
     ItemID=document.getElementById(preid+"ddlItem");
     MRP=document.getElementById(preid+"txtMRP");
     PurchasePrice=document.getElementById(preid+"txtPurchasePrice");
     SalesPrice=document.getElementById(preid+"txtSalesPrice");
     TaxNo=document.getElementById(preid+"ddlTaxSystem");
     
}
function ActivateUpdate()
{
     document.getElementById("btnSave").style.display="none";
     document.getElementById("btnUpdate").style.display="block";
     document.getElementById("btnCancel").style.display="block";
     btnCancel_Click();
     
  
}

function  ActivateSave()
{
    
    document.getElementById("btnSave").style.display="block";
    document.getElementById("btnUpdate").style.display="none"; 
    document.getElementById("btnCancel").style.display="none";
    GetCntrls();
    PLID.value = "";
    ItemID.value = 0;
    MRP.value = "";
    PurchasePrice.value = "";
    SalesPrice.value = "";
    TaxNo.value=0;
}
