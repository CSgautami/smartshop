﻿var preid = 'ctl00_ContentPlaceHolder1_';
var xmlObj;
var UserID=null;
var tblGrid;
var Item;
var Qty;
var Name;
var xmlObj;
var hidPackID;
var lblItem;
var tdItem;

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetControls()
{
    
    hidPackID=document.getElementById(preid+"hidPackID");
//    Item = document.getElementById(preid+"ddlItem");
    tdItem=document.getElementById("tdItem");
    Name=document.getElementById(preid+"txtPack");
    Qty = document.getElementById(preid+"txtQty");
    UserID =document.getElementById(preid+"hidUserID");
    tblGrid =document.getElementById("tblGrid");
    lblItem=document.getElementById(preid+"lblItem");
    divItem=document.getElementById("divItem");
    
}
function GetUserID()
{
    UserID=document.getElementById("hidUserID");
}
function btnAdd_Click()
{
    GetControls();

    if (selectedItem==0 || selectedItem=="")
    {
        tdItem.focus();
        alert("Enter Item");
        return false;
    }
    
    if (Qty.value=="")
    {
        Qty.focus();
        alert("Enter Qty");
        return false;
    }
    if(isNaN(Qty.value))
    {
        alert("Enter Valid Quantity");
        Qty.focus();
        return false;
    }
    tblGrid =document.getElementById("tblGrid");
    var i;
    i=tblGrid.rows.length;
    var tr=tblGrid.insertRow(i);    
    for(i=0;i<4;i++)
    {
        tr.insertCell(i);
    }
    tr.cells[0].innerHTML= selectedItem;
    tr.cells[0].style.display="none";
    tr.cells[1].innerHTML=tdItem.value;        
    tr.cells[2].innerHTML=lblItem.innerText;
    tr.cells[3].innerHTML=Qty.value;
    //tr.cells[3].style.display="none";
//    Item.value="0";
    tdItem.value="";
    selectedItem="";
    divItem.innerText="";
    Qty.value="";
    lblItem.innerText="";
    tr.onmouseover=function(){MouseOver(this);};
    tr.onmouseout=function(){MouseOut(this);};
    tr.onclick=function(){tblClick(this);}                 
    tdItem.focus();
 }
    
 function MouseOver(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {
            t.style.backgroundColor="gray";
            t.style.cursor="hand";     
            t.title="Click to edit the values";  
        }
    }

    function MouseOut(t)
    {
        if(t.cells[0].innerHTML.trim()!="")
        {
            t.style.backgroundColor="";
            t.style.crusor="text";        
        }
    }


    function tblClick(t)
    {
        var Con=confirm("Are You Sure,Do You Want To Edit?");
        if(Con=true)
        {
        if(t.cells[0].innerHTML.trim()!="")
        {        
            selectedItem=t.cells[0].innerHTML.trim();
            tdItem.value=t.cells[1].innerHTML.trim();            
            Qty.value=t.cells[3].innerHTML.trim();
            lblItem.innerText=t.cells[2].innerHTML.trim();
            tblGrid.deleteRow(t.rowIndex);
            tdItem.focus();
        }
        }
    }
    function btnSave_Click(e,t)
    {
        GetControls();
        if(Name.value=="")
        {
            alert("Enter Name");
            Name.focus();
            return false;
        }        
        if(tblGrid.rows.length<=1)
        {
            alert("Enter Packing Details");
            //Item.focus();
            tdItem.focus();
            return false;
        }
//        if(tblGrid.rows.length<=2)
//        {
//            alert("Enter Qty");
//            Qty.focus();
//            return false;
//        }
        var xmlDet="<XML>";
        for(var i=1;i<tblGrid.rows.length;i++)
        {
            xmlDet=xmlDet+"<Pack_Det>";
            var tr=tblGrid.rows[i];            
            xmlDet=xmlDet+"<ItemID>"+tr.cells[0].innerHTML.trim()+"</ItemID>";
            xmlDet=xmlDet+"<Qty>"+tr.cells[3].innerHTML.trim()+"</Qty>";
            xmlDet=xmlDet+"</Pack_Det>";
        }
        xmlDet=xmlDet+"</XML>";
        var Con=confirm("Are You Sure,Do You Want To Save?");
        if(Con=true)
        PageMethods.SavePackDetails(Name.value,xmlDet,UserID.value,SaveComplete);
    }
    
    function SaveComplete(res)
    {
        if(res=="0" || res=="")
        {
            alert("Error while Saving");        
        }
        else if(res.substring(0,1)=="E")
        {
            alert(res);
        }    
        else
        {
            alert("Saved successfully ");
            Clearfields();
            btnCancel_Click();
             
        }
    }
    function Clearfields()
    {
        GetControls();
        Name.value="";
        for(var i=tblGrid.rows.length-1;i>0;i--)
        {
           tblGrid.deleteRow(i);
        }       
         
         //Item.value="0";
         tdItem.value="";
         selectedItem="";
         divItem.innerHTML="";
         Qty.value="";
    }
    function btnUpdate_Click(e,t)
    {
        GetControls();
        if(Name.value=="")
        {
            alert("Enter Name");
            Name.focus();
            return false;
        }        
        if(tblGrid.rows.length<=1)
        {
            alert("Enter Pack Details");
            tdItem.focus();
            return false;
        }        
        var xmlDet="<XML>";
        for(var i=1;i<tblGrid.rows.length;i++)
        {
            xmlDet=xmlDet+"<Pack_Det>";
            var tr=tblGrid.rows[i];            
            xmlDet=xmlDet+"<ItemID>"+tr.cells[0].innerHTML.trim()+"</ItemID>";
            xmlDet=xmlDet+"<Qty>"+tr.cells[3].innerHTML.trim()+"</Qty>";
            xmlDet=xmlDet+"</Pack_Det>";
        }
        xmlDet=xmlDet+"</XML>";
        var Con=confirm("Are You Sure,Do You Want To Update?");
        if(Con=true)
        PageMethods.UpdatePackDetails(hidPackID.value,Name.value,xmlDet,UserID.value,UpdateComplete);
    }
    function UpdateComplete(res)
    {
        if(res=="0" || res=="")
        {
            alert("Error while Updating");        
        }
        else if(res.substring(0,1)=="E")
        {
            alert(res);
        }    
        else
        {
            alert("Updated successfully ");
            Clearfields(); 
            //btnCancel_Click();
            document.getElementById("btnSave").style.display="block";
            document.getElementById(preid+"btnUpdate").style.display="none";
            document.getElementById(preid+"btnDelete").style.display="none";
        }
    }
    function Clearfields()
    {
        GetControls();
        Name.value="";
        for(var i=tblGrid.rows.length-1;i>0;i--)
        {
           tblGrid.deleteRow(i);
        }
         
         Name.value="";
         //Item.value="0";
         tdItem.value="";
         Qty.value="";
         lblItem.innerText="";
         selectedItem="";
         divItem.innerText="";
         
    }
    function btnCancel_Click()
    {
//        GetControls();
//        Name.value="";
//        Item.value="0";
//        Qty.value="";
        Clearfields();
        PID =0;
        document.getElementById("btnSave").style.display="block";
        document.getElementById(preid+"btnUpdate").style.display="none";
        document.getElementById(preid+"btnDelete").style.display="none";
        Name.focus();
    }
var Flag;
var navbtn;
var PID=0;
function btnPDRecordNav_Click(e,t)
{    
    try    
    {
       GetControls();
        Flag="F";
        navbtn=t;
        if(t.id=="btnFirst")
            Flag="F";
        else if(t.id=="btnPrev")
        {
//           if(PID==0)
//              Flag="F";
//           else
              Flag="P";
        }
        else if(t.id=="btnNext")
             Flag="N";
        else if(t.id=="btnLast")
            Flag="L";    
        
        if(UserID==null)
            GetUserID();
        navbtn.disabled=true;
     
        PageMethods.GetPDNavData(PID,Flag,GetPDNavDataComplete);
    }
    catch(err)
    {
        if(navbtn!=null)
            navbtn.disabled=false;
    } 
   }
    function GetPDNavDataComplete(res)
    {
      try
        {
            btnCancel_Click();
            getXml(res);
            if(xmlObj.getElementsByTagName("Table").length==0)
            {
                alert("No data found.");
                return;
            }
            else
            {   
            Clearfields();
            //GetControls();
            //hidPackID.value= xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PackID")[0].firstChild.nodeValue;
            hidPackID.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PackID")[0].firstChild.nodeValue;
            PID=hidPackID.value;
            //document.getElementById("hidPackID").value=hidPackID.value
            Name.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("PackName")[0].firstChild.nodeValue;
                
              tblGrid=document.getElementById("tblGrid");
                for(var i=tblGrid.rows.length-1;i>0;i--)
                    tblGrid.deleteRow(i);
                var xmlObjLength=xmlObj.getElementsByTagName("Table1").length;
                for(var i=0;i<xmlObjLength;i++)
                {                
                    if(tblGrid.rows.length==1)
                        var tr=tblGrid.insertRow(tblGrid.rows.length);
                    else
                        var tr=tblGrid.insertRow(tblGrid.rows.length-1);
                    var j;
                    for(j=0;j<4;j++)
                    {
                        tr.insertCell(j);
                    }                    
                    tr.cells[0].style.display="none";
                    //tr.cells[3].style.display="none";
                    tr.cells[0].innerHTML= parseInt(xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("ItemID")[0].firstChild.nodeValue);
                    tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;        
                    tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;                   
                    tr.cells[3].innerHTML=xmlObj.getElementsByTagName("Table1")[i].getElementsByTagName("Qty")[0].firstChild.nodeValue;                   
                    tr.onmouseover=function(){MouseOver(this);};
                    tr.onmouseout=function(){MouseOut(this);};
                    tr.onclick=function(){tblClick(this);}                                       
                }
                  document.getElementById("btnSave").style.display="none";
                  document.getElementById(preid+"btnUpdate").style.display="block";
                  document.getElementById(preid+"btnDelete").style.display="block";
               } 
            }
        catch(err)
        {
        }
        finally
        {
        if(navbtn!=null)
           navbtn.disabled=false;
        }
      }
      function btnDelete_Click()
        {
            try 
            {
                var Con=confirm("Are You Sure,Do You Want To Delete?");
                if(Con==true)           
                    PageMethods.DeletePack(hidPackID.value,DeletePackComplete)
                    
            }
            catch(err)
            {
            }
        }
        function DeletePackComplete(res)
        {
            if(res=="-1")
            {
                alert("Can't Delete Transaction Exists");        
            }
            else 
            {
                alert("Deleted Successfully");
                btnCancel_Click();
            }
        }
function GetItemName()
{
   //Item=document.getElementById(preid+"ddlItem");
   PageMethods.GetItemName(selectedItem,CompleteItemName);  
}
function CompleteItemName(res)
{
    try 
    {
        getXml(res);        
        lblItem=document.getElementById(preid+"lblItem");
        if(xmlObj.getElementsByTagName("Table").length==0)
        {          
            lblItem.innerText="";            
            return;            
        }
        else
        {            
            lblItem.innerText =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
        }      
    }
    catch(err)
    {
    }
}
var divItem;
var selectedItem="";
var prevText="";

function FillItem(e,t)
{
    if(prevText != t.value)
    {
        prevText=t.value;
        //alert(e.keyCode);
        selectedItem="";
        divItem=document.getElementById("divItem");
        if(t.value!="")
        {        
            PageMethods.CallFillItem(t.value,FillItemComplete)
        }
        else{
        divItem.innerHTML="";
        divItem.style.display="none";
        }
    }
}

function FillItemComplete(res)
{
    try
    {
        
        getXml(res);
        var tbl="<table style='width:100%'>";
        var count=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<count; i++)
        {
            var ItemID;
            var ItemName;
            var ItemCode;
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild!=null)
                    ItemID=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild!=null)
                    ItemName=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                    
            //lname=lname+" ("+lid+"-";
            //lname=lname+" (";
            if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0]!=null)
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild!=null)
                    //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                    ItemCode =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            //lname=lname+")";
            if(i==0)
            {
                tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+"</td></tr>";
                selectedItem=ItemID;
            }
            else
            {
                //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+"</td></tr>";
                
            }
        }
        tbl=tbl+"</table>";
        divItem.innerHTML=tbl;
        if(count>0)
            divItem.style.display="block";
        else
            divItem.style.display="none";
    }
    catch(err)
    {
    }   
    
}
function OnSelectClick(t)
{
    selectedItem=t.cells[0].innerHTML;
    var txt=document.getElementById("tdItem");
    ApplyItem(txt);
}
function OnMouseOver(t)
{
    var tbl=t.parentNode;
    var cnt=0;
    selectedItem ="";
    for(var i=0;i<tbl.rows.length;i++)
    {
        tbl.rows[i].className='unselectedrow';   
    }
    t.className='selectedrow';
    selectedItem=t.cells[0].innerHTML;
}
function selectItem(e,t)
{
    if(e.keyCode==9||e.keyCode==13)
    {
        ApplyItem(t);
    }
    else
    {
        if(divItem==null)
            divItem=document.getElementById("divItem");
        var tbl=divItem.firstChild;    
        var currow=0;
        if(tbl!=null)
        {
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    currow=i;
                }
            }
            if(e.keyCode==38)
            {

                if(tbl.rows[currow-1]!=null)
                {
                    tbl.rows[currow-1].className='selectedrow';
                    selectedItem=tbl.rows[currow-1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
            else if(e.keyCode==40)
            {
                if(tbl.rows[currow+1]!=null)
                {
                    tbl.rows[currow+1].className='selectedrow';
                    selectedItem=tbl.rows[currow+1].cells[0].innerHTML;         
                    tbl.rows[currow].className='unselectedrow';         
                }
            }
        }
    }
}

function ApplyItem(t)
{
    if(divItem==null)
            divItem=document.getElementById("divItem");
    if(selectedItem!="")
    {   
        var tbl=divItem.firstChild;
        var cnt=0;
        if(tbl!="" && tbl!=null)
        { 
            for(var i=0;i<tbl.rows.length;i++)
            {
                if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                {
                    t.value=tbl.rows[i].cells[1].innerHTML;
                    cnt++;
                    break;
                }
            }
            if(cnt==0)
            {
                selectedItem="";
                t.value="";
            }
        }        
    }
    else
        t.value="";
    prevText=t.value;
    divItem.style.display="none";
    
}
                



    

    
    
        
