﻿var preid='ctl00_ContentPlaceHolder1_';
var tblGrid;
var Total;
var DCharges;
var GrandTotal;
var tbl;
var Mrp;
var Price;
var ItemName;    		        
var Qty;    
var ItemID;
var Amount; 
var i;		    
var TempID=null;
var UserID;
var hidTempID;
var ImgAvailable;
var NoStock;
var BatchNo;
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}
function GetCntrl()
{
    tblGrid=document.getElementById(preid+"gvShopingCart");
    Total=document.getElementById(preid+"lblTotal");
    DCharges=document.getElementById(preid+"lblDCharges");
    GrandTotal=document.getElementById(preid+"lblGrand");    
}
function ConfirmEdit()
{
    if (! confirm("Do you want to Edit?"))             
    return false;            
}
function ConfirmDelete()
{
    if (! confirm("Do you want to Delete?"))
    return false;            
}
function Validation(t)
{		
    GetCntrl();
    tbl=t.parentNode.parentNode.parentNode.parentNode;
    Mrp=tbl.rows[0].cells[1].children[0].value;
    Price=tbl.rows[0].cells[2].children[0].value;
    ItemName=tbl.rows[1].cells[1].children[0].value;  
    ItemID=tbl.rows[3].cells[0].children[0].value;
    BatchNo=tbl.rows[3].cells[2].children[0].value;
    NoStock=tbl.rows[1].cells[0].children[0].rows[2].cells[2].children[0];
    Qty=tbl.rows[2].cells[1].children[0].rows[0].cells[1].children[0];    
    ImgAvailable=tbl.rows[2].cells[1].children[0].rows[0].cells[0].children[0].children[0];    
    Amount=(Qty.value)*(Price);        
    if(ImgAvailable.style.display=="")
       ImgAvailable.style.display="none";
    if(ImgAvailable.style.display=="none")
    {
        if(Qty.disabled==true)
        {
            alert("Stock Not Available");
            return false;
        }
        if(Qty.value=="")
        {
            alert("Enter Quantity");
            Qty.focus();
            return false;
        }    
        i=tblGrid.rows.length;
        var tr;
        if(i>1 && tblGrid.rows[1].cells[1].innerHTML=="No Items Selected")
        {
            tr=tblGrid.rows[1];
        } 
        else 
        {
            tr =tblGrid.insertRow(i);
            tr.className="zixe4";
            tr.style.backgroundColor="f6fef0";    
            for(i=0;i<8;i++)
            {		        
                tr.insertCell(i);
            }
        }
        tr.cells[0].innerHTML=ItemID;
        tr.cells[1].innerHTML=ItemName;
        tr.cells[2].innerHTML=Mrp;
        tr.cells[3].innerHTML=Price;
        tr.cells[4].innerHTML=Qty.value;
        tr.cells[5].innerHTML=Amount.toFixed(2);
        tr.cells[6].innerHTML="<input type='image' id='img' src='MasterPageFiles/cross symbol.png' onclick='btnDelete_Click(this)'/>";
        tr.cells[7].innerHTML=BatchNo;
        tr.cells[0].style.display="none";
        tr.cells[2].style.display="none"; 
        tr.cells[7].style.display="none"; 
        CalculateData();    
        Qty.value="";  
        var xmlDet="<XML>";   
        for(i=1;i<tblGrid.rows.length;i++)
        {    
            xmlDet=xmlDet+"<ShopingCart>";    
            var tr=tblGrid.rows[i];         
            xmlDet=xmlDet+"<ItemID>"+tr.cells[0].innerHTML.trim()+"</ItemID>";    
            xmlDet=xmlDet+"<MRP>"+tr.cells[2].innerHTML.trim()+"</MRP>";
            xmlDet=xmlDet+"<Price>"+tr.cells[3].innerHTML.trim()+"</Price>";
            xmlDet=xmlDet+"<Qty>"+tr.cells[4].innerHTML.trim()+"</Qty>";    
            xmlDet=xmlDet+"<Amount>"+tr.cells[5].innerHTML.trim()+"</Amount>";               
            xmlDet=xmlDet+"<BatchNo>"+tr.cells[7].innerHTML.trim()+"</BatchNo>";               
            xmlDet=xmlDet+"</ShopingCart>";        
        }
        xmlDet=xmlDet+"</XML>";    
        if(UserID==null)
            GetUserID();
        if(hidTempID==null)
            GetTempID();
        if(hidTempID.value!="")
            TempID=hidTempID.value;
        PageMethods.SaveTempOrder(xmlDet,TempID ,UserID.value,SaveCompleted);
        ImgAvailable.style.display="block";
    }
}
function SaveCompleted(res)
{
    hidTempID.value=res;
}
function GetUserID()
{
    UserID=document.getElementById(preid+"hidUserID");
}
function GetTempID()
{
    hidTempID=document.getElementById(preid+"hidTempID");
}
function CalculateData()
{    
    Total.innerText=GetColTotal(tblGrid,5);
    if(Total.innerText<200)
        DCharges.innerText="10.00";
    else 
        DCharges.innerText="0.00";
    var nTotal=parseFloat(Total.innerText)+parseFloat(DCharges.innerText);
    GrandTotal.innerText=nTotal.toFixed(2);
}
function btnDelete_Click(t)
{
    try 
    {
        var Con=confirm("Are You Sure,Do You Want To Delete?");
        if(Con==true)           
        {
            var tblItemID =t.parentNode.parentNode.cells[0].innerText;            
            var dlItems=document.getElementById(preid+"dlItemDet");
            var bFound=false;
            PageMethods.DeleteTempOrder(tblItemID,hidTempID.value, DeleteComplete);            
            for(var i=0;i<dlItems.rows.length;i++)
            {
                //dlItems.rows[i].cells[0].firstChild.cells[0].firstChild.cells[2]
                ItemID=dlItems.rows[i].cells[0].firstChild.cells[0].firstChild.cells[3].firstChild.value;
                if(ItemID==tblItemID )
                {
                    bFound=true;
                    dlItems.rows[i].cells[0].firstChild.cells[1].firstChild.cells[1].firstChild.cells[7].firstChild.cells[0].firstChild.style.display="none";       
                    return;
                 }
            }            
            
        }
    }
    catch(err)
    {
     throw err;
    }
}
function DeleteComplete(res)
{
    //tblGrid.deleteRow(t.parentNode.parentNode.rowIndex);    
   // GetData();
    CalculateData();  
//    var dlItems=document.getElementById(preid+"dlItemDet");
//    for(var i=1;i<dlItems.rows.length;i++)
//    {
//        //dlItems.rows[i].cells[0].firstChild.cells[0].firstChild.cells[2]
//        ItemID=dlItems.rows[i].cells[0].firstChild.cells[0].firstChild.cells[3].firstChild.value;
//        if(ItemID==tblItemID)
//            dlItems.rows[i].cells[0].firstChild.cells[1].firstChild.cells[1].firstChild.cells[7].firstChild.cells[0].firstChild.style.display="none";       
//    }            
}
//function CheckAvailability()
//{
//    var dlItems=document.getElementById(preid+"dlItemDet");
//    for(var i=0;i<dlItems.rows.length;i++)
//    {
//        //dlItems.rows[i].cells[0].firstChild.cells[0].firstChild.cells[2]
//        ItemID=dlItems.rows[i].cells[0].firstChild.cells[0].firstChild.cells[3].firstChild.value;
//        PageMethods.CheckStockAvailable(ItemID,StockAvailableCompleted);
//    }
//}
//function StockAvailableCompleted(res)
//{
//    getXml(res);
//    var Qty;
//    Qty =parseInt(xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("BalQty")[0].firstChild.nodeValue,10);
//    if(Qty>0)
//    {

//    }
//}
function GetData()
{
    if(hidTempID==null)
        GetTempID();
    PageMethods.GetData(hidTempID.value,GetDataCompleted);
}
function GetDataCompleted(res)
{
    GetCntrl();
    getXml(res); 
    if(xmlObj.getElementsByTagName("Table").length==0)
    {    
        var i;
        i=tblGrid.rows.length;
        var tr=tblGrid.insertRow(i);
        tr.className="zixe4";
        tr.style.backgroundColor="f6fef0";    
        for(i=0;i<8;i++)
        {		        
            tr.insertCell(i);
        }
        tr.cells[0].style.display="none";
        tr.cells[1].innerHTML="No Items Selected";
        tr.cells[2].style.display="none";
        tr.cells[7].style.display="none";
    }
    else 
    {
        for(var i=tblGrid.rows.length-1;i>0;i--)
            tblGrid.deleteRow(i);
        var xmlObjLength=xmlObj.getElementsByTagName("Table").length;
        for(var i=0;i<xmlObjLength;i++)
        {       
            
            var tr=tblGrid.insertRow(i+1);
//            if(tblGrid.rows.length==1)
//                tr=tblGrid.insertRow(tblGrid.rows.length)
//            else 
//                tr=tblGrid.insertRow(tblGrid.rows.length-1)
            tr.className="zixe4";
            tr.style.backgroundColor="f6fef0"; 
            for(var j=0;j<8;j++)
            {		        
                tr.insertCell(j);
            }
            tr.cells[0].style.display="none";
            tr.cells[2].style.display="none";
            tr.cells[7].style.display="none";
            ItemID =parseInt(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemID")[0].firstChild.nodeValue,10);
            tr.cells[0].innerHTML=ItemID;
            tr.cells[1].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemName")[0].firstChild.nodeValue;            
            tr.cells[2].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("MRP")[0].firstChild.nodeValue;
            var nPrice=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Price")[0].firstChild.nodeValue;
            tr.cells[3].innerHTML=nPrice;
            var nQty=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Qty")[0].firstChild.nodeValue;
            tr.cells[4].innerHTML=nQty;   
            tr.cells[5].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("Amount")[0].firstChild.nodeValue;             
            tr.cells[6].innerHTML="<input type='image' id='img' src='MasterPageFiles/cross symbol.png' onclick='btnDelete_Click(this)'/>";            
            tr.cells[7].innerHTML=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;             
            var dlItems=document.getElementById(preid+"dlItemDet");            
            if(dlItems!=null)
            {
                for(var k=0;k<dlItems.rows.length;k++)
                {   
                    var dlItemID;                                 
                    dlItemID=dlItems.rows[k].cells[0].children[0].rows[0].children[0].children[0].rows[1].children[1].children[0].value;                    
                    if(dlItemID==ItemID)
                    {
                        for(var l=0;l<dlItems.rows[k].getElementsByTagName("div").length;l++)
                        {
                            dlItems.rows[k].getElementsByTagName("div")[l].children[0].style.display="block";
                          //dlItems.rows[k].getElementsByTagName("img")[l].style.display="block";
                        }
                        //dlItems.rows[k].cells[0].firstChild.cells[1].firstChild.cells[1].firstChild.cells[7].firstChild.cells[0].firstChild.style.display="block";       
                    }
                }
                
            }
        }
        CalculateData();
    }
}
function SetData()
{
    GetCntrl();
    GetData();
    //CheckAvailability();
}
function CheckOutValidation()
{
    if(tblGrid.rows[1].cells[0].innerHTML=="")
    {
        alert("Select Items.");
        return false;
    }
}
function AddItems(e,t)
{
    if(e.keyCode==13)
    {
        Validation(t);
        
    }
}