﻿var preid = '';
function EditClick(e, t) {
    btnCancel_Click();
    var StateID = document.getElementById("hidStateID");
    document.getElementById(preid + "txtState").value = "";
    StateID.value = "";
    var State = t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    var res = confirm("Do you want to Edit the State : " + State);
    if (res == true) {
        StateID.value = t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        document.getElementById(preid + "txtState").value = State;
        document.getElementById("btnSave").style.display = "none";
        document.getElementById("btnUpdate").style.display = "block";
        document.getElementById("btnCancel").style.display = "block";
    }
}

function btnUpdate_Click(t) {
    if (document.getElementById(preid + "txtState").value == "") {
        document.getElementById(preid + "txtState").value = "";
        premsgbox("Select Country ", "txtState");
        return false;
    }
    var Stateid = document.getElementById("hidStateID").value;
    var Statename = document.getElementById(preid + "txtState").value;
    var Countryid = document.getElementById(preid + "ddlCountrys").value
    t.disabled=true;
    PageMethods.btnUpdate_Click(Countryid,Stateid,Statename, UpdateComplete);    
}

function btnCancel_Click() {
    document.getElementById(preid + "txtState").value = "";
    document.getElementById("hidStateID").value = "";
    document.getElementById("btnSave").style.display = "block";
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "none";
    document.getElementById(preid + "txtState").focus();
}

function UpdateComplete(res)
{
    try
    {
//    if(res=="-2")
//    {
//        alert("Select Country.");
//        document.getElementById(preid + "ddlCountrys").focus();        
//        return;
//    }
    if(res=="-3")
    {
        document.getElementById(preid + "txtState").value="";
        document.getElementById(preid + "txtState").focus();
        alert("Enter State.");        
        return;
    }
    if (res == "" || res == "0") {
        alert("Error in Updation.");        
        return;
    }
    if (res == "-1") {
        alert("State already exist.");        
        return;
    }
    alert("Updated Successfully.");
    ShowTable(res);    
    document.getElementById(preid + "txtState").value = "";
    document.getElementById(preid + "txtState").focus();
    document.getElementById("btnSave").style.display = "block";    
    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnCancel").style.display = "none";    
    }
    catch(err)
    {
    }
    finally
    {
    document.getElementById("btnUpdate").disabled=false;
    }
}

function DeleteClick(e, t) {
    btnCancel_Click();
    document.getElementById(preid + "txtState").value = "";
    var States = t.parentNode.parentNode.cells[1].firstChild.nodeValue;
    var res = confirm("Do you want to Delete the State : " + States);
    
    if (res == true) {
        var Countryid = document.getElementById(preid + "ddlCountrys").value
        var Stateid = document.getElementById("hidStateID");
        Stateid.value = "";
        Stateid.value = t.parentNode.parentNode.cells[0].firstChild.nodeValue;
        PageMethods.DeleteClick(Stateid.value,Countryid, DeleteComplete);

    }
}

function ddlCountrys_Changed() 
{
// debugger;
    btnCancel_Click();
//    if (document.getElementById(preid+"ddlCountrys").value == "0") {        
//        premsgbox("Select Country ", "ddlCountrys");
//        return false;
//    }
    var Countryid = document.getElementById(preid+"ddlCountrys").value;
    PageMethods.ddlCountrys_Changed(Countryid,ShowTable);
}

function DeleteComplete(res) {
    ShowTable(res);
    alert("State Deleted Sucessfully.");
}




function btnSave_Click(t) {
    if (document.getElementById(preid + "txtState").value == "") {
        document.getElementById(preid + "txtState").value = "";
        premsgbox("Enter StateName", "txtState");
        return false;
    }
//    if (document.getElementById(preid+"ddlCountrys").value=="0") {
//        document.getElementById(preid + "ddlCountrys").value = "0";
//        premsgbox("Select Country", "ddlCountrys");
//        return false;
//    }
    var Country = document.getElementById(preid + "ddlCountrys").value;
    var Statename = document.getElementById(preid + "txtState").value;
    t.disabled = true;
//    var Con=confirm("Are You Sure, Do You Want To Save?");
//    if(Con=true)
    PageMethods.btnSave_Click(Country, Statename, SaveComplete);    
}

function SaveComplete(res) {
try
{
//    if(res=="-2")
//    {
//        alert("Select Country.");
//        document.getElementById(preid + "ddlCountrys").focus();        
//        return;
//    }
    if(res=="-3")
    {
        document.getElementById(preid + "txtState").value="";
        document.getElementById(preid + "txtState").focus();
        alert("Enter State.");        
        return;
    }
    if (res == "" || res == "0") {
        alert("Error in insertion.");        
        return;
    }
    if (res == "-1") {
        alert("State already exist.");        
        return;
    }
    alert("Saved Successfully.");
    ShowTable(res);
    document.getElementById(preid + "txtState").value=""; 
    document.getElementById(preid + "txtState").focus();   
}
catch(err)
{
}
finally
{
document.getElementById("btnSave").disabled = false;
}
}

function ShowTable(res) {    
    var xmlObj = getXml(res);
    var tbl = document.getElementById(preid + "gvStates");
    for (var i = tbl.rows.length - 1; i > 0; i--) {
        tbl.deleteRow(i);
    }    
    tbl.rows[0].cells[1].style.position = 'relative';
    tbl.rows[0].cells[1].style.width = 100;
    for (var i = 0; i < xmlObj.getElementsByTagName("States").length; i++) {
        tbl.insertRow(i + 1)
        tbl.rows[i + 1].insertCell(0);
        tbl.rows[i + 1].insertCell(1);
        tbl.rows[i + 1].insertCell(2);
        tbl.rows[i + 1].cells[0].className="hidendStyle";
        tbl.rows[i + 1].cells[0].innerHTML =  xmlObj.getElementsByTagName("States")[i].getElementsByTagName("StateID")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[1].innerHTML = xmlObj.getElementsByTagName("States")[i].getElementsByTagName("StateName")[0].firstChild.nodeValue;
        tbl.rows[i + 1].cells[2].innerHTML = "<a href='#' onclick='EditClick(event,this)' class='gridNavButtons'>Edit</a>&nbsp;&nbsp;<a href='#' onclick='DeleteClick(event,this)' class='gridNavButtons'>Delete</a>";        
        tbl.rows[i + 1].cells[0].style.display="none";
    }    
}

function getXml(xmlString) {
    var xmlObj;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }
    return xmlObj;
}

function premsgbox(msg, cntrl) {
    try {
        document.getElementById(preid + cntrl).focus();
        alert(msg);        
    } catch (error) { }

}