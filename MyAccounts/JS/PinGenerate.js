﻿var preid = 'ctl00_ContentPlaceHolder1_';
var xmlObj;
var UserID=null;
var Amount;
var nValue;
var ZeroValue;
var NoofPins;
var Temp;
String.prototype.trim = function () {
return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function getXml(xmlString) {
    xmlObj=null;
    try {
        var browserName = navigator.appName;
        if (browserName == "Microsoft Internet Explorer") {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.async = "false";
            xmlObj.loadXML(xmlString);
        }
        else {
            xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
        }

    }
    catch (err) {
    }    
}  
function GetControls()
{
    Amount  =document.getElementById(preid+ "txtAmount");
    nValue  =document.getElementById(preid+ "txtValue");        
    ZeroValue  =document.getElementById(preid+ "chkZeroValued"); 
    NoofPins=document.getElementById(preid+"txtNoofPins");
    UserID = document.getElementById(preid+"hidUserID");  
}
function ClearFeilds()
{
    GetControls();
    Amount.value="";
    nValue.value=500;
    ZeroValue.checked=false;
    NoofPins.value="0";
    Amount.focus();
    chkZeroRated_Click();
}
function GetNoofPins()
{
    if(Amount.value!="0" && nValue.value!="0")
    {
        Temp =Amount.value/nValue.value;
        NoofPins.value=Temp.toFixed();
    }
}
function btnSave_Click(e,t)
{   
    try 
    {
        GetControls();
        if (ZeroValue.checked==false)
        {  
            if(Amount.value=="")
            {
                alert("Enter Amount");
                Amount.focus();
                return;
            }
            if (isNaN(Amount.value))
            {
                alert("Enter Valid Amount");
                Amount.focus();
                return;
            } 
            if(nValue.value=="0")
            {
                alert("Enter Value");
                nValue.focus();
                return;
            }
//            if (isNaN(nValue.value));
//            {
//                alert("Enter Valid Value");
//                nValue.focus();
//                return;
//            }
        }
        if(NoofPins.value=="0")
        {
            alert("Enter No Of Pins");
            NoofPins.focus();
            return;
        }      
        t.disabled=true;
        if(Amount.value=="")
            Amount.value=0;
        var Con=confirm("Are You Sure,Do You Want To Save?");
        if(Con=true)
        {     
        PageMethods.SavePinGeneration(Amount.value,nValue.value,ZeroValue.checked,NoofPins.value,UserID.value,SaveCompleted);
    }
    }
    
    catch(err)
    {
        t.disabled=false;
    }
    finally 
    {
        t.disabled=false;
    }
}
function SaveCompleted(res)
{
    if(res=="0" || res=="")
    {
        alert("Error while Saving");        
    }   
    else
    {
        alert("Saved successfully");
        ClearFeilds();         
    }
} 
function chkZeroRated_Click()
{
    if(ZeroValue.checked==true)
    {
        NoofPins.disabled=false;
    }
    else
    {
        NoofPins.disabled=true ;    
    }
}      