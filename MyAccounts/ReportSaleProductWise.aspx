﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportSaleProductWise.aspx.cs" Inherits="MyAccounts20.ReportSaleProductWise" Title="Sale ProductWise" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/Reports/SalesReturnReport.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
     function btnShow_Click()
    {
        ProductWiseShowClick();
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">         
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                   
                                    <tr>    
                                        <td align="left">Branch</td>
                                        <td colspan="3" align="left">
                                            <select id="ddlBranch" style="width:300px;"></select>
                                        </td>
                                        <td>
                                            Stock Item:
                                        </td>
                                        <td>
                                           <%-- <select id="ddlStcokItem" style="width:300px;"/>--%>
                                           <table cellpadding="0" cellspacing="0">
                                                <tr style="display: none;">
                                                    <td>
                                                        <select style="width: 100px;" id="ddlStockItem">
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:300px;" align="left">
                                                        <input type="text" style="width: 300px;" id="tdItem" onkeydown="selectItem(event,this);" onkeyup="FillItem(event,this);"  onblur="ApplyItem(this);GetItemName();"/>                                                
                                                        
                                                                                            
                                                    </td>
                                                </tr>
                                                <tr style="position: relative;">
                                                    <td align="left">
                                                        <div style=" display: none; width:300px; " class="divautocomplete"
                                                            id="divItem">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>            
                                        </td>      
                                    </tr>
                                    <tr>
                                        <td>
                                            From Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" id="txtFromDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>
                                        <td>
                                            To Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" id="txtToDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>                                      
                                                                          
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmSaleReportProWise" id="frmSaleReportProWise"
                                    style="width: 100%; height: 400px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" runat="server" id="hidUserID" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
