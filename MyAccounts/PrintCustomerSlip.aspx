﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintCustomerSlip.aspx.cs"
    Inherits="MyAccounts20.PrintCustomerSlip" ValidateRequest="false" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Slip</title>
    <script src="JS/Reports/CustomerSlip.js" type="text/javascript"></script>
    <script src="JS/NumberInWords.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center">
            <tr>
                <td id="tdHead" runat="server" style="font-size: medium; " align="center">                
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    TO :
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellspacing="0" align="left" width="100%">
                        <tr>
                            <td>
                                CustomerID
                            </td>
                            <td id="tdCID" runat="server" style="width: 170px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Name
                            </td>
                            <td id="tdName" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address
                            </td>
                            <td id="tdAddr" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone No.
                            </td>
                            <td id="tdPhone" runat="server">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    Here by forwarded the following commission towards the promotion of our products:
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="1" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                Total Commission for the Week
                            </td>
                            <td id="tdCW" runat="server" align="right" style="width: 170px">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Less Deduction (Service +TDS) @ 15%
                            </td>
                            <td id="tdSTds" runat="server" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Net Amount Payable
                            </td>
                            <td id="tdNetAmt" runat="server" align="right">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td id="tdInWords" runat="server" align="left" colspan="2">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td align="center">
                    Thanking You,
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td id="tdPayDate" runat="server">
                </td>
                <td align="right">
                    for Smart Retailer
                </td>
            </tr>
        </table>
    </div>
    <div>
        <input type="hidden" id="hidXML" runat="server" />
    </div>
    </form>
</body>
</html>
