﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
namespace MyAccounts20
{
    public partial class PinsGenerate : System.Web.UI.Page
    {
        PinGenerate_BLL obj_BLL = new PinGenerate_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "ClearFeilds();", true);                
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SavePinGeneration(string Amount, string Value, string ZeroValue, string NoofPins, string UserID)
        {
            try
            {
                string res;
                PinGenerate_BLL obj_BLL = new  PinGenerate_BLL();
                res = obj_BLL.SavePinGeneration(Amount , Value , ZeroValue , NoofPins , UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
    }
}
