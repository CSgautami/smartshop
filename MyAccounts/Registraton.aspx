<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="Registraton.aspx.cs" Inherits="MyAccounts20.Registraton" Title="Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
//    function validate()
//    {
//    var Email=document.getElementById("txtEmail");
//    var exp=  /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
//            if(!exp.exec(Email.value.trim()))
//            {
//              alert ("Invalid EmailID");
//              Email.focus();
//              return false;
//            }
        //    }
        var preid = 'ctl00_ContentPlaceHolder1_'
        function FillFields(box) 
        {
            var BHNo = document.getElementById(preid + "txtBHNo");
            var SHNO = document.getElementById(preid + "txtSHNO");
            var BPVC = document.getElementById(preid + "txtBPVC");
            var SPVC = document.getElementById(preid + "txtSPVC");
            var BCMA = document.getElementById(preid + "txtBCMA");
            var SCMA = document.getElementById(preid + "txtSCMA");
            var BPC = document.getElementById(preid + "txtBPC");
            var SPC = document.getElementById(preid + "txtSPC");
            var BLM = document.getElementById(preid + "txtBLM");
            var SLM = document.getElementById(preid + "txtSLM");


            if (box.checked == true)
            {           
             SHNO.value=BHNo.value;
             SPVC.value=BPVC.value;
             SCMA.value= BCMA.value;
             SPC.value= BPC.value;
             SLM.value=BLM.value;
            }
            return;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <table>
    <tr>
    <td>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
            <tr>
                <td>
                    <table align="center" style="text-align: left;">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="trYourID" runat="server" visible="false">
                            <td colspan="4" align="right">
                                YourID:
                            </td>
                            <td style="font-size: 14px" colspan="6" align="left">                                
                                <asp:TextBox ID="txtYourID" runat="server" Width="150px" OnTextChanged="txtYourID_TextChanged"
                                    AutoPostBack="true" ></asp:TextBox>                                
                            </td>
                        </tr>
                        <tr>
                        <td align="left" style="color: Green ;font-size:14px" colspan="8">
                        <b>Personal Details:</b>
                        </td>
                        </tr>
                        <tr>    
                            <td style="font-size: 14px">                                
                                Name
                            </td>                       
                            <td>
                                <asp:TextBox ID="txtName" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                Father\Husband Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtFather" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                DOB
                            </td>
                            <td>
                                <asp:TextBox ID="txtDOB" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <%-- <td style="font-size: 14px">
                                House No
                            </td>
                            <td>
                                <asp:TextBox ID="txtHouseNO" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                Post/vill/Colony
                            </td>
                            <td>
                                <asp:TextBox ID="txtPost" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px">
                                City/Mandal/Area
                            </td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                District/city
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDistrict" runat="server" Width="155px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                State
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlState" runat="server" Width="155px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px">
                                Pin Code
                            </td>
                            <td>
                                <asp:TextBox ID="txtPinCode" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>--%>                           <%--<input type="checkbox" id="chkSelect" runat="server" onclick="FillFields(this);" />
                             <asp:Label ID="lblText" runat="server" ForeColor="Red" Text=" Select This, If Shipping Address Is Same As Billing Address"></asp:Label>--%>
                            <td style="font-size: 14px">
                                Phone No
                            </td>
                            <td>
                                <asp:TextBox ID="txtPhone" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                Mobile
                            </td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px">
                                PAN No
                            </td>
                            <td>
                                <asp:TextBox ID="txtPAN" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                EmailID
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trCPWd" runat="server">
                            <td>
                                Password
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"  Width="150px"></asp:TextBox>
                            </td>
                            
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px" id="tdlbl">
                                Confirm Password
                            </td>
                            <td id="tdtext" runat="server">
                                <asp:TextBox ID="txtCpassword" runat="server" TextMode="Password" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                         <tr id="trPwd" runat="server">
                            <td>
                               Password
                            </td>
                           <td>
                            <asp:TextBox ID="txtpwd" runat="server"  Width="150px"></asp:TextBox>
                           </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                        <td style="color:Green ;font-size:14px" align="left" colspan="8"  >
                        <b>Billing Address:</b>
                        </td>
                        </tr>
                        <tr>
                        <td style="font-size:14px">
                        House No
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtBHNo" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width:10px"></td>
                        <td style="font-size:14px">
                        Post/Village/Colony
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtBPVC" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width:10px"></td>
                        <td style="font-size:14px">
                        City/Mandal/Area
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtBCMA" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        </tr>
                        <tr>
                        <td style ="font-size:14px">
                        District/City
                        </td>
                        <td style="font-size:14px">
                        <asp:DropDownList ID="ddl_BDC" runat="server" Width="150px"></asp:DropDownList>
                        </td>
                        <td style="width:10px"></td>
                        <td style ="font-size:14px">
                        State
                        </td>
                        <td style="font-size:14px">
                        <asp:DropDownList ID="ddl_BState" runat="server" Width="150px"></asp:DropDownList>
                        </td>
                        <td style="width:10px"></td>
                        <td style ="font-size:14px">
                        PinCode
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtBPC" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <tr>
                        <td style="font-size:14px">
                        Land Mark
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtBLM" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        </tr>
                        </tr>
                        <tr>
                        <td style="color:Green ; font-size:14px;" colspan="8" align="left" >
                        <b>Shipping Address:</b>
                        <asp:CheckBox ID="ckb_Billadd" runat="server" 
                        Text=" Select This, If Shipping Address Is Same As Billing Address" 
                        ForeColor="#FF3300" oncheckedchanged="ckb_Billadd_CheckedChanged" AutoPostBack="true"/>
                             <%--<input type="checkbox" id="chkSelect" runat="server" onclick="FillFields(this);" />
                             <asp:Label ID="lblText" runat="server" ForeColor="Red" Text=" Select This, If Shipping Address Is Same As Billing Address"></asp:Label>--%>
                        </td>
                        </tr>
                        <tr>
                        <td style="font-size:14px">
                        House No
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtSHNO" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width:10px"></td>
                        <td style="font-size:14px">
                        Post/Village/Colony
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtSPVC" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <td style="width:10px"></td>
                        <td style="font-size:14px">
                        City/Mandal/Area
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtSCMA" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        </tr>
                        <tr>
                        <td style ="font-size:14px">
                        District/City
                        </td>
                        <td style="font-size:14px">
                        <asp:DropDownList ID="ddl_SDC" runat="server" Width="150px"></asp:DropDownList>
                        </td>
                        <td style="width:10px"></td>
                        <td style ="font-size:14px">
                        State
                        </td>
                        <td style="font-size:14px">
                        <asp:DropDownList ID="ddl_SState" runat="server" Width="150px"></asp:DropDownList>
                        </td>
                        <td style="width:10px"></td>
                        <td style ="font-size:14px">
                        PinCode
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtSPC" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        <tr>
                        <td style="font-size:14px">
                        Land Mark
                        </td>
                        <td style="font-size:14px">
                        <asp:TextBox ID="txtSLM" runat="server" Width="150px"></asp:TextBox>
                        </td>
                        </tr>
                        </tr>
                        <tr>
                            <td align="left" style="color: Green;font-size: 14px" colspan="8">
                                <b>Nominee Details:</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px">
                                Nominee Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtNomineeName" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                Relation With Applicant
                            </td>
                            <td>
                                <asp:TextBox ID="txtrelation" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="color: Green;font-size: 14px" colspan="8">
                                <b>Bank Details:</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px">
                                Bank Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtBank" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                A/C No
                            </td>
                            <td>
                                <asp:TextBox ID="txtAccountNO" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px">
                                IFS Code
                            </td>
                            <td>
                                <asp:TextBox ID="txtIFScode" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td style="font-size: 14px">
                                Branch Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtBranchName" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;  
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100%" align="center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Blue" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100%" align="center">
                                <asp:Button ID="btnSave" runat="server" Text="Submit" OnClick="btnSave_Click" ValidationGroup="a" />
                                <asp:Button ID="btnCancel" runat="server" Text="Clear" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" DaysModeTitleFormat="dd,MM,yyyy"
            Format="dd-MM-yyyy" TargetControlID="txtDOB" TodaysDateFormat="dd,MM,yyyy">
        </cc1:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Name......"
            ControlToValidate="txtName" Display="None" SetFocusOnError="True" ValidationGroup="a">
        </asp:RequiredFieldValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Father Name"
            ControlToValidate="txtFather" Display="None" SetFocusOnError="True" ValidationGroup="a">
        </asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpression" runat="server" ErrorMessage="InValidEmailAddress"
            ControlToValidate="txtEmail" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            SetFocusOnError="true" ValidationGroup="a"></asp:RegularExpressionValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Your Date Of Birth"
            ControlToValidate="txtDOB" Display="None" SetFocusOnError="True" ValidationGroup="a"></asp:RequiredFieldValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter Password"
            ControlToValidate="txtPassword" Display="None" SetFocusOnError="True" ValidationGroup="a">
        </asp:RequiredFieldValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RequiredFieldValidator4">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter CPassword"
            ControlToValidate="txtCpassword" Display="None" SetFocusOnError="True" ValidationGroup="a">
        </asp:RequiredFieldValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RequiredFieldValidator5">
        </cc1:ValidatorCalloutExtender>
        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" 
            ControlToValidate="txtPhone" Display="None" ErrorMessage="Invalid Characters" 
            SetFocusOnError="True" ValidationExpression="[0-9]{10}" ValidationGroup="a" 
            Visible="False"></asp:RegularExpressionValidator>
        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" TargetControlID="RegularExpressionValidator">
        </cc1:ValidatorCalloutExtender>    --%>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
            ControlToValidate="txtMobile" ErrorMessage="Please Enter Mobile No" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator6">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
            ControlToValidate="txtBHNo" ErrorMessage="Please Enter House  No" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="server" TargetControlID="RequiredFieldValidator7">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
            ControlToValidate="txtBPVC" ErrorMessage="Please Enter Post/Village/Colony" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="server" TargetControlID="RequiredFieldValidator8">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
            ControlToValidate="txtBCMA" ErrorMessage="Please Enter City/Mandal/Area" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="server" TargetControlID="RequiredFieldValidator9">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
            ControlToValidate="ddl_BDC" ErrorMessage="Select District/City" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="server" TargetControlID="RequiredFieldValidator10">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
            ControlToValidate="ddl_BState" ErrorMessage="Select State" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="server" TargetControlID="RequiredFieldValidator11">
        </cc1:ValidatorCalloutExtender>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
            ControlToValidate="txtBPC" ErrorMessage="Please Enter Pin Code" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="server" TargetControlID="RequiredFieldValidator12">
        </cc1:ValidatorCalloutExtender>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
            ControlToValidate="txtBLM" ErrorMessage="Please Enter Landmark" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="server" TargetControlID="RequiredFieldValidator13">
        </cc1:ValidatorCalloutExtender>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
            ControlToValidate="txtSHNO" ErrorMessage="Please Enter House  No" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="server" TargetControlID="RequiredFieldValidator14">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
            ControlToValidate="txtSPVC" ErrorMessage="Please Enter Post/Village/Colony" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="server" TargetControlID="RequiredFieldValidator15">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
            ControlToValidate="txtSCMA" ErrorMessage="Please Enter City/Mandal/Area" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="server" TargetControlID="RequiredFieldValidator16">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
            ControlToValidate="ddl_SDC" ErrorMessage="Select District/City" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="server" TargetControlID="RequiredFieldValidator17">
        </cc1:ValidatorCalloutExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" 
            ControlToValidate="ddl_SState" ErrorMessage="Select State" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="server" TargetControlID="RequiredFieldValidator18">
        </cc1:ValidatorCalloutExtender>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" 
            ControlToValidate="txtSPC" ErrorMessage="Please Enter Pin Code" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="server" TargetControlID="RequiredFieldValidator19">
        </cc1:ValidatorCalloutExtender>
         <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" 
            ControlToValidate="txtSLM" ErrorMessage="Please Enter Landmark" 
            SetFocusOnError="True" ValidationGroup="a" Display ="None"></asp:RequiredFieldValidator>
             <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="server" TargetControlID="RequiredFieldValidator20">
        </cc1:ValidatorCalloutExtender>
        
    </div>
    <input type="hidden" id="hidUserID" runat="server" />
    
    <input type="hidden" id="hidRefID" runat="server" />
    <input type="hidden" id="hidPinNo" runat="server" />
    <input type="hidden" id="hidRegDate" runat="server" />
    
</asp:Content>
