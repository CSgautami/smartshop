﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class Category : System.Web.UI.Page
    {
        Category_BLL Obj_BLL = new Category_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {   
                hidCategoryID.Value = "-1";                
                GetDefaults();
                FillGrid();
            }
        }

        private void FillGrid()
        {
            try
            {
                DataSet ds = Obj_BLL.GetData();
                gvCategory.DataSource = ds;
                gvCategory.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
        private void GetDefaults()
        {
            HeadGroup_BLL Obj_HBLL = new HeadGroup_BLL();
            ddlHead.DataSource = Obj_HBLL.GetHeadGroup();
            ddlHead.DataTextField = "HeadGroupName";
            ddlHead.DataValueField = "HeadGroupID";
            ddlHead.DataBind();
            ddlHead.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {               
                string res = Obj_BLL.SaveCategory(ddlHead.SelectedValue,hidCategoryID.Value,txtCategory.Text);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error while Saving Category";
                }
                if (res == "-1")
                {
                    lblMsg.Text = "Category Already Exist";
                }
                if (res == "1")
                {
                    lblMsg.Text = "Category Saved Successfully";                    
                    btnCancel_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvCategory.PageIndex = 0;
            ddlHead.SelectedIndex = 0;
            txtCategory.Text = "";
            txtCategory.Focus();
            lblMsg.Text = "";
            hidCategoryID.Value = "-1";
            FillGrid();
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {                
                btnCancel.Visible = true;
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidCategoryID.Value = ((HtmlInputHidden)grow.FindControl("hidCategoryID")).Value;
                ddlHead.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidHeadID")).Value;
                txtCategory.Text = ((Label)grow.FindControl("lblCategory")).Text;

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidCategoryID.Value = ((HtmlInputHidden)grow.FindControl("hidCategoryID")).Value;                
                string res = Obj_BLL.DeleteCategory(hidCategoryID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error while Deleting Category";
                    return;
                }
                if (res == "1")
                {
                    lblMsg.Text = "Category Deleted Successfully";
                    btnSave.Visible = true;
                    btnCancel.Visible = true;                    
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void gvCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCategory.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        
    }
}
