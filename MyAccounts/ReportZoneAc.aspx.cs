﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL.Reports;

namespace MyAccounts20
{
    public partial class ReportZoneAc : GlobalPage 
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
            // hidUserID.Value = Session["UserID"].ToString();
            if (!IsPostBack)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetZone()
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetZone().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

