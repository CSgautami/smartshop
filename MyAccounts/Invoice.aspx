﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="Invoice.aspx.cs" Inherits="MyAccounts20.Invoice" Title="Invoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        var preid;
        preid ='ctl00_ContentPlaceHolder1_'
        function btnPrintClick()
        {
            try
            {           
                var OrderID=document.getElementById(preid+"hidOrderID").value;                
                window.open("ShoppingCartPrint.aspx?OrderID="+OrderID,"","Height=700,Width=700,scrollbars=yes");
            }
            catch(err)
            {
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table width="720" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="roundedcorners">
                    <table width="700" border="0" align="center" cellpadding="0" bordercolor="#CCCCCC">
                        <tr>
                            <td colspan="3" align="center" class="border-bottom3">
                                 <img src="MasterPageFiles2/images/logo.gif" width="250" height="90" border="0">
                            </td>
                        </tr>
                        <tr>
                            <td width="29%" align="left">
                                <table width="350" border="0" cellspacing="0" cellpadding="0">
                                    <tr bordercolor="#CCCCCC">
                                        <td align="left">
                                            <strong class="bodytext">Delivery Address</strong>
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td align="left" class="bodytext" id="tdAddress" runat="server">
                                            <%--V nagesh&nbsp;(9440123554),<br />
                                            B-7, 1st Floor,Sri Lekha Apartments, Huda Complex,<br />
                                            SaroorNagar,&nbsp;&nbsp;Hyderabad--%>
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td align="left">
                                            <strong class="bodytext">Ordered By</strong>
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td align="left" class="bodytext">
                                            <table>
                                                <tr>
                                                    <td>
                                                        LoginId:
                                                    </td>
                                                    <td align="left" id="tdLoginID" runat="server">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" id="tdName" runat="server">
                                                    </td>
                                                </tr>
                                            </table>
                                            <%--LoginId: nagesh, #<strong>1999</strong>(3,BO)<br />
                                            (V nagesh, 944012355)--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="71%" colspan="2" align="left" valign="top">
                                <table width="350" border="0" cellspacing="0" cellpadding="0">
                                    <tr bordercolor="#CCCCCC">
                                        <td height="29" colspan="3" align="left">
                                            <strong>Invoice Details</strong>
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td width="116" height="29" align="left">
                                            OrderID
                                        </td>
                                        <td width="10" align="left">
                                            <strong>:</strong>
                                        </td>
                                        <td width="226" align="left" id="tdOrderID" runat="server">
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td height="22" align="left">
                                            OrderDate
                                        </td>
                                        <td align="left">
                                            <strong>:</strong>
                                        </td>
                                        <td align="left" id="tdOrderDate" runat="server">
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td align="left">
                                            Mode of Payment
                                        </td>
                                        <td align="left">
                                            <strong>:</strong>
                                        </td>
                                        <td align="left">
                                            <table width="200" border="1" cellpadding="0" cellspacing="2" bordercolor="#CCCCCC">
                                                <tr>
                                                    <td id="tdModeOfPayment" runat="server">
                                                        <%--Cash On Delivery--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr bordercolor="#CCCCCC">
                                        <td align="left">
                                            Payment Status
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td align="left">
                                            <table width="200" border="1" cellpadding="0" cellspacing="2" bordercolor="#CCCCCC">
                                                <tr>
                                                    <td id="tdPaymentStatus" runat="server">
                                                        <%--Due--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                     <table width="700" border="1" align="center" cellpadding="0" cellspacing="2" bordercolor="#CCCCCC">
                        <tr>
                            <td>
                                <asp:gridview id="gvShopingCart" runat="server" autogeneratecolumns="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="46" height="25" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                    <tr>
                                                        <td width="46" height="25" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                            SL.No
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table bgcolor="#f6fef0" class="zixe4" width="46">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="46">
                                                            <asp:Label ID="lblSno" runat="server" Text='<%#Eval("SlNo")%>'></asp:Label>    
                                                        </td>
                                                    </tr>                                                                                    
                                                </table>                                                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="70" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                    <tr>
                                                        <td width="70" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                            ItemCode
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table bgcolor="#f6fef0" class="zixe4" width="70" height="25">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="70" height="25">
                                                            <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode")%>'></asp:Label>    
                                                        </td>
                                                    </tr>
                                                </table>                                                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField>                    
                                            <HeaderTemplate>
                                                <table width="300" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                    <tr>
                                                        <td width="300" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                            Item Name
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table bgcolor="#f6fef0" class="zixe4" width="300" height="25">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="300" height="25">
                                                            <%--<input type="hidden" runat="server" id="hidOID" value='<%#Eval("OID") %>' />
                                                            <input type="hidden" runat="server" id="hidItemID" value='<% #Eval("ItemID") %>'/>--%>
                                                            <asp:Label ID="lblItemName" runat="server" Text=' <%#Eval("ItemName")%>'></asp:Label>    
                                                        </td>
                                                    </tr>
                                                </table>                                                                                
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="89" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                    <tr>
                                                        <td width="89" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                            M.R.P
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table bgcolor="#f6fef0" class="zixe4" width="89" height="25">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="89" height="25">
                                                            <asp:Label ID="lblMRP" runat="server" Text=' <%#Eval("MRP")%>'></asp:Label>            
                                                        </td>
                                                    </tr>
                                                </table>                                                                                
                                            </ItemTemplate>                    
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="79" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                    <tr>
                                                        <td width="79" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                            Price
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table bgcolor="#f6fef0" class="zixe4" width="79" height="25">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="79" height="25">
                                                            <asp:Label ID="lblPrice" runat="server" Text=' <%#Eval("Price")%>'></asp:Label>    
                                                        </td>
                                                    </tr>
                                                </table>                                                                                
                                            </ItemTemplate>                    
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="39" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                    <tr>
                                                        <td width="39" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                            Qty
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate >
                                                <table bgcolor="#f6fef0" class="zixe4" width="39" height="25">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="39" height="25">
                                                            <asp:Label ID="lblQty" runat="server" Text=' <%#Eval("Qty")%>'></asp:Label>
                                                            <%--<asp:TextBox ID="txtQty" Width="30px" runat="server" Text='<%#Eval("Qty")%>'
                                                            OnTextChanged="txtQty_TextChanged" AutoPostBack="true" ></asp:TextBox>--%>    
                                                        </td>
                                                    </tr>
                                                </table>                                                                                
                                            </ItemTemplate>                    
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table width="90" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                    <tr>
                                                        <td width="90" align="center" bgcolor="#ecebeb" class="zixegreen2" height="25">
                                                            Amount
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table bgcolor="#f6fef0" class="zixe4" width="90" height="25">
                                                    <tr>
                                                        <td bgcolor="#f6fef0" class="zixe4" width="90" height="25">
                                                            <asp:Label ID="lblAmount" runat="server" Text=' <%#Eval("Amount")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>                                                                                
                                            </ItemTemplate>                    
                                        </asp:TemplateField>                           
                                    </Columns>                
                                </asp:gridview>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td height="25" colspan="6" align="right">
                                            <strong>Total :</strong>
                                        </td>
                                        <td align="center" class="orange2">
                                            <strong>
                                                <asp:label id="lblSubTotal" runat="server" text="0.00"></asp:label>
                                            </strong>
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td height="25" colspan="6" align="right">
                                            <strong>Delivery Charges :</strong>
                                        </td>
                                        <td align="center" class="orange2">
                                            <strong>
                                                <asp:label id="lblDCharges" runat="server" text="0.00"></asp:label>
                                            </strong>
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td height="25" colspan="6" align="right">
                                            <strong>Grand Total :</strong>
                                        </td>
                                        <td align="center" class="orange2">
                                            <strong>
                                                <asp:label id="lblTotal" runat="server" text="0.00"></asp:label>
                                            </strong>
                                        </td>
                                        <td align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="5" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="720" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="roundedcorners">
                                            <table width="710" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <strong>Note :</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server" id="tdPrint">                
                            <td height="30" align="center" valign="middle">
                                <%--<input type="submit" name="button2" id="button2" value="Print" />--%>
                                <asp:Button ID="btnPrint" Text="Print" runat="server" OnClientClick="btnPrintClick();return false;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" valign="top">
                                &nbsp;
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
        </table>       
    </div>
    <div>
        <input type="hidden" id="hidOrderID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
