﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL.Reports;
using BLL;
namespace MyAccounts20
{
    public partial class ReportMyTeam :  System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");

            // hidUserID.Value = Session["UserID"].ToString();     
            if (Session["Type"].ToString() != "Customer")
            {
                Login_BLL bllLogin = new Login_BLL();
                string res = bllLogin.CheckAuthorization(Session["UserID"].ToString(), Request.Url.AbsolutePath.Substring(1).ToLower());
                if (res != "1")
                {
                    Response.Redirect("Login.aspx", true);
                }
            }
            if (!IsPostBack)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
                if (Session["Type"].ToString() != "Customer" || Session["UserID"].ToString() == "0")
                    GetMember();
                else
                    trMember.Visible = false;
            }
        }        
        public void GetMember()
        {
            try
            {                
                //ddlMember.DataSource = obj_BLL.GetMember().Tables[0];
                //ddlMember.DataTextField = "YourID";
                //ddlMember.DataValueField = "LedgerID";
                //ddlMember.DataBind();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCustomerName(string LedgerID)
        {
            string res = "";
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.GetLedgerDetails(LedgerID).Tables[0].Rows[0]["LedgerName"].ToString();
            }
            catch (Exception ex)
            {
                res = "";
                throw;
            }
            return res;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillMember(string prefix)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.CallFillMember(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
