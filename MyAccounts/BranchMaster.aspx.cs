﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class BranchMaster : GlobalPage
    {
        Branch_BLL Obj_BLL = new Branch_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
                ddlType.Items.Insert(0, new ListItem("Select", "0"));
                ddlType.Items.Insert(1, new ListItem("Branch", "Branch"));
                ddlType.Items.Insert(2, new ListItem("Franchise", "Franchise"));
            }
        }
            
        private void FillGrid()
        {
            try
            {
                Branch_BLL Obj_BLL=new Branch_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetBranch();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvBranch.DataSource = ds;
                        gvBranch.DataBind();
                        gvBranch.Rows[0].Cells[5].Text = "";
                    }
                    else
                    {
                        gvBranch.DataSource = ds;
                        gvBranch.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res=Obj_BLL.InsertBranch(txtBranchName.Text,txtBranchAddress.Text,txtContactPerson.Text,txtContactNo.Text,txtEmailID.Text,ddlType.SelectedValue,txtCircle.Text,txtDivision.Text,txtDesig.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Branch";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
            }
            else if (res == "1")
            {
                lblMsg.Text = "Branch saved successfully";
                FillGrid();
                txtBranchName.Text = "";
                txtBranchAddress.Text = "";
                txtContactPerson.Text = "";
                txtContactNo.Text = "";
                txtEmailID.Text = "";
                ddlType.SelectedIndex = 0;
                txtCircle.Text = "";
                txtDivision.Text = "";
                txtDesig.Text = "";
                txtBranchName.Focus();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string res = Obj_BLL.UpdateBranch(hidBranchID.Value, txtBranchName.Text, txtBranchAddress.Text, txtContactPerson.Text, txtContactNo.Text, txtEmailID.Text, ddlType.SelectedValue, txtCircle.Text, txtDivision.Text, txtDesig.Text, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Branch";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
            }
            else if (res == "1")
            {
                lblMsg.Text = "Branch Updated successfully";
                FillGrid();
                txtBranchName.Text = "";
                txtBranchAddress.Text = "";
                txtContactPerson.Text = "";
                txtContactNo.Text = "";
                txtEmailID.Text = "";
                ddlType.SelectedIndex = 0;
                txtCircle.Text = "";
                txtDivision.Text = "";
                txtDesig.Text = "";
                btnUpdate.Visible = false;
                btnSave.Visible = true;
                txtBranchName.Focus();

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvBranch.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtBranchName.Text = "";
            txtBranchAddress.Text = "";
            txtContactPerson.Text = "";
            txtContactNo.Text = "";
            txtEmailID.Text = "";
            ddlType.SelectedIndex = 0;
            txtCircle.Text = "";
            txtDivision.Text = "";
            txtDesig.Text = "";
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidBranchID.Value = ((HtmlInputHidden)grow.FindControl("hidBranchID")).Value;
                txtBranchName.Text = ((Label)grow.FindControl("lblBranchName")).Text;
                txtBranchAddress.Text = ((Label)grow.FindControl("lblAddress")).Text;
                txtContactPerson.Text = ((Label)grow.FindControl("lblContactPerson")).Text;
                txtContactNo.Text = ((Label)grow.FindControl("lblContactNo")).Text;
                txtEmailID.Text = ((Label)grow.FindControl("lblEmailID")).Text;
                if (((Label)grow.FindControl("lblType")).Text != "")
                      ddlType.SelectedValue = ((Label)grow.FindControl("lblType")).Text;
                //txtCircle.Text = ((Label)grow.FindControl("lblCircle")).Text;
                //txtDivision.Text = ((Label)grow.FindControl("lblDivision")).Text;
                txtDesig.Text = ((Label)grow.FindControl("lblDesig")).Text;
                txtBranchName.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
            {
                try
                {
                    LinkButton lbtn = (LinkButton)sender;
                    GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                    hidBranchID.Value = ((HtmlInputHidden)grow.FindControl("hidBranchID")).Value;
                    Branch_BLL Obj_BLL = new Branch_BLL();
                    string res = Obj_BLL.DeleteBranch(hidBranchID.Value);
                    if (res == "" || res == "0")
                    {
                        lblMsg.Text = "Error While Deleting Branch";
                        

                    }
                    if (res == "1")
                    {
                        lblMsg.Text = "Branch Deleted Successfully";
                        txtBranchName.Text = "";
                        txtBranchAddress.Text = "";
                        txtContactPerson.Text = "";
                        txtContactNo.Text = "";
                        txtEmailID.Text = "";
                        ddlType.SelectedIndex = 0;
                        txtCircle.Text = "";
                        txtDivision.Text = "";
                        txtDesig.Text = "";
                        txtBranchName.Focus();
                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                        btnCancel.Visible = true;
                        FillGrid();

                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
        protected void gvBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBranch.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        }
    }

    
    

        

    
