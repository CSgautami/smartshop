﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportPriceWiseSaleReport.aspx.cs" Inherits="MyAccounts20.ReportPriceWiseSaleReport" Title="PriceWise SaleReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Calendar.css" rel="stylesheet" type="text/css" />
    <script src="JS/CalendarPopup.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">         
        document.write(getCalendarStyles());        
        var cal=new CalendarPopup("divCalendar");        
        cal.showNavigationDropdowns();
        function showcalendar(t)
        {
            cal.select(t,t.id,'dd-MM-yyyy');
        }  
    var dtFormat='dd-MM-yyyy'
    var preid = 'ctl00_ContentPlaceHolder1_';
    var xmlObj;
    var UserID=null;
    var ddlBranch;
    var dtpFrom;
    var dtpTo;
    var hidGBID;
    var TypeOfPrices;
    String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
    }

    function getXml(xmlString) {
        xmlObj=null;
        try {
            var browserName = navigator.appName;
            if (browserName == "Microsoft Internet Explorer") {
                xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                xmlObj.async = "false";
                xmlObj.loadXML(xmlString);
            }
            else {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }  
    function GetControls()
    {
        dtpFrom =document.getElementById(preid+ "txtFromDate");
        dtpTo =document.getElementById(preid+"txtToDate");        
        ddlBranch =document.getElementById("ddlBranch"); 
        UserID = document.getElementById(preid+"hidUserID");  
        TypeOfPrices=document.getElementById("ddlPrices");
    }
    function btnShow_Click()
    {    
        window.frames["frmPriceWiseSaleReport"].location.href="Reports/PriceWiseSaleReport.aspx?FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&Branch="+ddlBranch.value+"&Prices="+ TypeOfPrices.value +"&dt="+new Date().getTime();
    }
    function GetDefaults()
    {    
//        dtpFrom.value = new Date().format("dd-MM-yyyy");
//        dtpTo.value=new Date().format("dd-MM-yyyy"); 
        TypeOfPrices.value="New Sales";
        ddlBranch.focus();           
    }
    function SetData()
    {
        GetControls();
        GetBranch();
        GetDefaults();
    }
    function GetDefaultBranch()
    {
        hidGBID=document.getElementById(preid+"hidGBID");
    }
    function GetBranch()
    {        
        PageMethods.GetBranch(UserID.value, GetBranchComplete);    
    }
    function GetBranchComplete(res)
    {
        getXml(res);         
        ddlBranch.options.length=0;
        var opt=document.createElement("OPTION");
        if(xmlObj.getElementsByTagName("Table").length != 1)
        {
            opt=document.createElement("OPTION");
            //opt.text="Select";
            opt.text="All";
            opt.value="0";    
            ddlBranch.options.add(opt);            
        }  
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            ddlBranch.options.add(opt);
        }       
        if(hidGBID==null)
            GetDefaultBranch();
        ddlBranch.value=hidGBID.value;   
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table onload="SetData();">
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>    
                                        <td align="left">Branch</td>
                                        <td colspan="3" align="left">
                                            <select id="ddlBranch" style="width:250px;"></select>
                                        </td>
                                        <td>TypeOfPrice</td>
                                        <td>
                                            <select id="ddlPrices" style="width: 105px" tabindex="-1">
                                                <option value="Customer Price">Customer Price</option>
                                                <option value="MRP">MRP</option>
                                                <option value="New Sales">New Sales</option>
                                                <option value="Retail Price">Retail Price</option>
                                                <option value="VIP Price">VIP Price</option>
                                            </select>
                                        </td>
                                        <td>
                                            FromDate:
                                        </td>
                                        <td>
                                            <input type="text" id="txtFromDate" runat="server" maxlength="10" style="width: 100px;"
                                             onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"/>
                                             
                                        </td>
                                        <td>
                                            ToDate:
                                        </td>
                                        <td>
                                            <input type="text" id="txtToDate" runat="server" maxlength="10" style="width: 100px;" 
                                            onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"/>
                                        </td>
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>                                    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmPriceWiseSaleReport" id="frmPriceWiseSaleReport" style="width: 100%;
                                    height: 400px; font-size:6px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" id="hidUserID" runat="server" />
    <input type="hidden" runat="server" id="hidGBID" />
</asp:Content>
