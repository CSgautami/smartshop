﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportPurchaseReturn.aspx.cs" Inherits="MyAccounts20.ReportPurchaseReturn" Title="Report Purchase Return" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="JS/Reports/PurchaseReports.js" type="text/javascript"></script>
    <link href="Calendar.css" rel="stylesheet" type="text/css" />
    <script src="JS/CalendarPopup.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">         
        document.write(getCalendarStyles());        
        var cal=new CalendarPopup("divCalendar");        
        cal.showNavigationDropdowns();
        function showcalendar(t)
        {
            cal.select(t,t.id,'dd-MM-yyyy');
        }    
    function btnShow_Click()
    {
        preid='';
        var Name=document.getElementById(preid+"ddlSupplier").value;
        var fromdt=document.getElementById(preid+"txtFromDate").value;
        var todt=document.getElementById(preid+"txtToDate").value;
        var Mode=document.getElementById(preid+"ddlMode").value;
        var Branch=document.getElementById(preid+"ddlBranch").value;
        window.frames["frmPurchasReturnReport"].location.href="Reports/PurchasReturnReport.aspx?Customer="+Name+"&FromDate="+fromdt+"&ToDate="+todt+"&Mode="+Mode+"&Branch="+Branch+"&dt="+new Date().getTime();
    }    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table onload="SetData();">
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>    
                                        <td align="left">Branch</td>
                                        <td colspan="3" align="left">
                                            <select id="ddlBranch" style="width:300px;" onchange="GetLedgerName();"></select>
                                        </td>
                                        <td>
                                            Mode
                                        </td>
                                        <td>
                                            <select id="ddlMode" style="width: 100px;">
                                                <option value="">All</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Credit">Credit</option>
                                            </select>
                                        </td>
                                        <td>
                                            Supplier
                                        </td>
                                        <td>
                                            
                                            <select id="ddlSupplier" style="width: 200px;">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" runat="server" id="txtFromDate" maxlength="10" style="width: 100px;"
                                             onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"/>
                                             
                                        </td>
                                        <td>
                                            To Date:
                                        </td>
                                        <td>
                                            <input type="text" runat="server" id="txtToDate" maxlength="10" style="width: 100px;" 
                                            onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);"/>
                                        </td>                                        
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmPurchasReturnReport" id="frmPurchasReturnReport" style="width: 100%;
                                    height: 400px; font-size:6px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" id="hidUserID" runat="server" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>

