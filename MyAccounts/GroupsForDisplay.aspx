﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="GroupsForDisplay.aspx.cs" Inherits="MyAccounts20.GroupsForDisplay" Title="Groups For Display" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" width="100%">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">
                    Group
                </td>
                <td align="center">
                    Stock Item Name
                </td>
                <td align="center">
                    Description
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGID1" runat="server" Text="1" Width="40"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup1" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem1" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc1" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup2" runat="server" Text="2"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup2" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem2" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc2" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup3" runat="server" Text="3"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup3" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem3" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc3" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup4" runat="server" Text="4"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup4" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem4" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc4" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup5" runat="server" Text="5"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup5" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem5" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc5" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup6" runat="server" Text="6"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup6" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem6" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc6" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup7" runat="server" Text="7"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup7" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem7" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc7" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblGroup8" runat="server" Text="8"></asp:Label>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtGroup8" runat="server" Width="100"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:DropDownList ID="ddlItem8" runat="server" Width="150"></asp:DropDownList>
                </td>
                <td align="center">
                    <asp:TextBox ID="txtDesc8" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan ="4" align="center">
                    <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" Text="Save" runat="server" OnClick="btnSave_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnDelete" Text="Delete" runat="server" OnClick ="btnDelete_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick ="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </td>                
            </tr>
        </table>
    </div>
    <input type="hidden" id="hidUserID" runat="server" />
</asp:Content>
