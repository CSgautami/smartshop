﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="SubCategory.aspx.cs" Inherits="MyAccounts20.SubCategory" Title="SubCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
   var SubCategory;  
   var Category;
   var preid = 'ctl00_ContentPlaceHolder1_';
          function ValidateSave()
          {
              try
              {
                 SubCategory=document.getElementById(preid+"txtSCategory");    
                 Category=document.getElementById(preid+"ddlCategory");             
                 if(SubCategory.value.trim()=="")
                 {
                     alert("Please Enter SubCategory");
                     SubCategory.focus();
                     return false;
                 }
                 if(SubCategory.value=="0")
                 {
                     alert("Please Select Category");
                     Category.focus();
                     return false;
                 }

              return true;
               }
              catch(err)
               {
              return false;
               }
             }
     function ConfirmEdit()
     {
        if(!confirm("Do you want to Edit?"))
           return false;
     }
     function ConfirmDelete()
     {
        if(! confirm("Do you want to Delete?"))
           return false;
     } 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table align="center" width="100%">
    <tr>
        <td>
            <asp:GridView ID="gvSCategory" runat="server" AutoGenerateColumns="false" CssClass="grid"
              PageSize="13" AllowPaging="true" Font-Size="Small" OnPageIndexChanging="gvSCategory_PageIndexChanging" width="80%">
                    <Columns>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidCategoryID" value='<%#Eval("CategoryID")%>' />
                                <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SubCategory">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidSubCategoryID" value='<%#Eval("SubCategoryID")%>' />
                                <asp:Label ID="lblSCategory" runat="server" Text='<%#Eval("SubCategory") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                    OnClick="lbtnEdit_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                    OnClick="lbtnDelete_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
             </asp:GridView>
        </td>
    </tr>
</table>
<table align="center">
    
   <tr>
      <td align="left">
           SubCategory
      </td>
      <td>
          <asp:TextBox ID="txtSCategory" runat="server" Width="150px"></asp:TextBox>
      </td> 
   </tr>
   <tr>
      <td align="left">
           Category
      </td>
      <td>
          <asp:DropDownList ID="ddlCategory" runat="server" Width="150px"></asp:DropDownList>
      </td> 
   </tr>
   <tr>
       <td align="center" colspan="2">
           <asp:Button ID="btnSave" runat="server" Text="Save" 
               OnClientClick="return ValidateSave();" onclick="btnSave_Click" />
           <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
               onclick="btnCancel_Click"/>
       </td>
   </tr>
   <tr>
       <td align="center" colspan="2">
           <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
       </td>
   </tr>
</table>
     <input id="hidSubCategoryID" runat="server" type="hidden" /> 
</asp:Content>
