﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using BLL;
using System.Net;

namespace MyAccounts20
{
    public partial class IFrame : System.Web.UI.Page
    {
        //PhotoUploadas_BLL obj_BLL = new PhotoUploadas_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                try
                {
                    //string ftpLocation = ConfigurationManager.AppSettings["FTPLocation"].ToString();
                    string currentdt = DateTime.Now.ToString("ddMMyyyy");
                    string currentfile = DateTime.Now.Ticks.ToString() + "_" + FileUpload1.FileName;

                    string strFileName = currentdt + "/" + currentfile;

                    string currentdirectory = AppDomain.CurrentDomain.BaseDirectory + "\\temp\\" + currentdt;
                    if (!Directory.Exists(currentdirectory))
                        Directory.CreateDirectory(currentdirectory);
                    string ftpUserID = ConfigurationManager.AppSettings["FTPUserName"].ToString();
                    string ftpPassword = ConfigurationManager.AppSettings["FTPPassword"].ToString();


                    string ftpLocation = ConfigurationManager.AppSettings["FTPLocation"].ToString();
                    string uri = ftpLocation + "/" + strFileName;
                    FtpWebRequest reqFTP;


                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(
                               uri));

                    reqFTP.Credentials = new NetworkCredential(ftpUserID,
                                                               ftpPassword);

                    reqFTP.KeepAlive = false;
                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
                    reqFTP.UseBinary = true;
                    reqFTP.ContentLength = FileUpload1.PostedFile.ContentLength;// fileInf.Length;
                    int buffLength = 2048;
                    byte[] buff = new byte[buffLength];
                    int contentLen;

                    //FileStream fs = new FileStream(FileUpload1.PostedFile.FileName, FileMode.Open, FileAccess.Read);


                    Stream fs = FileUpload1.PostedFile.InputStream;

                    Stream strm = reqFTP.GetRequestStream();

                    contentLen = fs.Read(buff, 0, buffLength);


                    while (contentLen != 0)
                    {

                        strm.Write(buff, 0, contentLen);
                        contentLen = fs.Read(buff, 0, buffLength);
                    }


                    strm.Close();
                    fs.Close();

                    // string Imgname = "UploadImages/" + strFileExt;
                    //string ImgPath = NewDirectory + "/" + strFileExt;
                    //string ImgPath = "ItemImages/" + "Album/" + UserID + "/" + AlbumID + "/" + strFileExt;
                    //string res = obj_BLL.InsertAlbumDet(AlbumID, Imgname);
                    //string res = obj_BLL.InsertAlbumDet(AlbumID, ImageName, ImgPath, UserID);
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "top.parent.GetImages('" + currentdt + "','" + currentfile + "');", true);

                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }

    }
}
