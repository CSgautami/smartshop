﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportAgentAccnt.aspx.cs" Inherits="MyAccounts20.ReportAgentAccnt" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="Calendar.css" rel="stylesheet" type="text/css" />
 <script src="JS/CalendarPopup.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">        
    var preid;
    var ddlAgent;
    var ddlCust;
    var dtpFrom;
    var dtpTo;
    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
    preid ='ctl00_ContentPlaceHolder1_'
    function GetControls()
    {
        ddlAgent=document.getElementById("ddlAgent"); 
        ddlCust=document.getElementById("ddlCust");  
        dtpFrom=document.getElementById(preid+ "txtFromDate");
        dtpTo=document.getElementById(preid+ "txtToDate");             
        
    }
    var xmlObj;
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function GetAgent()
    {        
        PageMethods.GetAgent(GetAgentComplete);    
    }

    function GetAgentComplete(res)
    {
        getXml(res);    
        ddlAgent.options.length=0;             
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            var opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("AgentCode")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("AgentID")[0].firstChild.nodeValue;
            ddlAgent.options.add(opt);
        }   
        GetCustomer(); 
    }    
    function GetCustomer()
    {    
        ddlAgent=document.getElementById("ddlAgent");     
        PageMethods.GetCustomer(ddlAgent.value,GetCustomerComplete);    
    }

    function GetCustomerComplete(res)
    {
        getXml(res);    
        ddlCust.options.length=0;        
        var opt=document.createElement("OPTION");
        opt.text="All";
        opt.value="0";    
        ddlCust.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
            ddlCust.options.add(opt);
        }    
    }            
    function SetData()
    {    
        GetControls();
        dtpFrom.value = new Date().format("dd-MM-yyyy");
        dtpTo.value=new Date().format("dd-MM-yyyy");  
        GetAgent();     
        //GetCustomer();
        ddlAgent.focus();
    }
    function btnShow_Click()
    {       
        window.frames["frmAgentAccnt"].location.href="Reports/AgentAccntReport.aspx?AgentID="+ddlAgent.value+"&FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&CustID="+ddlCust.value;
    }          
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border:solid 1px black;">          
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>
                                        <td align="center">
                                            Agent
                                        </td>
                                        <td align ="left">                                            
                                            <select id="ddlAgent" style="width: 200px;" onchange="GetCustomer();">
                                            </select>
                                        </td>
                                        <td align="center">
                                            Customer
                                        </td>
                                        <td align ="left">                                            
                                            <select id="ddlCust" style="width: 200px;">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>                                        
                                        <td align="left">
                                            <span id="spnFromDate">From :</span>                                                                                     
                                        </td>                                        
                                        <td align ="left">
                                            <input type="text" runat="server" id="txtFromDate" maxlength="10" onfocus="showcalendar(this);" onclick="showcalendar(this);" onkeydown="HideCalendar(event);" style="width:100px;" />
                                        </td>
                                        <td align="left">
                                            <span id="spnToDate" >To :</span>
                                        </td>
                                        <td align="left">
                                            <input type="text" runat="server" id="txtToDate" maxlength="10" onfocus="showcalendar(this);" onclick="showcalendar(this);" onkeydown="HideCalendar(event);" style="width:100px;" />                                                                                    
                                        </td>                                                                                                                                                      
                                                                                                                                                                                                 
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>                                                                                                                       
                                    </tr>                                                      
                                </table>                             
                            </td>                            
                        </tr>    
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>                                        
                                        <td>                                            
                                            <iframe name="frmAgentAccnt" id="frmAgentAccnt" style="width: 100%; height: 400px;"
                                                frameborder="0" scrolling="auto"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                    
                    </table>
                </td>
            </tr>            
        </table>
    </div>    
     <div id="divCalendar" class="Calendar">
    </div>
</asp:Content>
