﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using BLL;

namespace MyAccounts20
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                //ddlType.Items.Insert(0, new ListItem("Select", "0"));
                ddlType.Items.Insert(0, new ListItem("Customer", "Customer"));
                ddlType.Items.Insert(1, new ListItem("User", "User"));
                txtUserName.Focus();
            }
        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text.Trim() == "")
                {
                    txtUserName.Focus();
                    lblMessage.Text = "Enter username.";
                    return;
                }
                if (txtPassword.Text.Trim() == "")
                {
                    txtPassword.Focus();
                    lblMessage.Text = "Enter Password.";
                    return;
                }
               
                if (txtUserName.Text == UserConfig.UserName && txtPassword.Text == UserConfig.Password)
                {
                    Session["UserID"] = "0";
                    //Session[MyAccounts20Session.UserID] = "0";
                    Session["GBID"] = "1";
                    Session["Type"] = "User";
                    Response.Redirect("Home.aspx");


                }
                else
                {
                    UserCreation_BLL Obj_BLL = new UserCreation_BLL();
                    DataSet ds = new DataSet();
                    ds =Obj_BLL.CheckUserCreation(txtUserName.Text, txtPassword.Text, ddlType.SelectedValue); 
                   // string res = Obj_BLL.CheckUserCreation(txtUserName.Text, txtPassword.Text, ddlType.SelectedValue);                    
                    //if (res == "")
                    //{
                    //    if (ddlType.SelectedValue== "User")
                    //        lblMessage.Text = "Invalid User Credentials.";
                    //    else 
                    //        lblMessage.Text = "Cannot Login Until There Is Purchase.";
                    //    return;
                    //}
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        if (ddlType.SelectedValue== "User")
                            lblMessage.Text = "Invalid User Credentials.";
                        else
                            lblMessage.Text = "Cannot Login Until There Is Purchase.";
                        return;
                    }
                    else
                    {
                        Session["UserID"] = ds.Tables[0].Rows[0]["RegisID"];                        
                        Session["GBID"] = "0";
                        Session["Type"] = ddlType.Text;
                        Session[MyAccountsSession.UserName] = txtUserName.Text;
                        if (ddlType.Text == "Customer")
                        {
                            Session["LName"] = ds.Tables[0].Rows[0]["LedgerName"];
                            StringBuilder str = new StringBuilder();
                            UserMaster.sb = str;
                            Response.Redirect("Index.aspx", false);
                        }
                        else
                            Response.Redirect("Home.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
    }
}
