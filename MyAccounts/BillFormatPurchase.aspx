﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillFormatPurchase.aspx.cs" Inherits="MyAccounts20.BillFormatPurchase" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Invoice</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center">           
            <tr>
                <td align="center" colspan="100%">
                    <div id="divBranchDet" runat="server">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 50px;">
                    Bill No
                </td>
                <td align="right">
                    :
                </td>
                <td id="tdBillNo" runat="server">
                </td>                
                <td align="left">
                    Date
                </td>
                <td align="right">
                    :
                </td>
                <td id="tdDate" runat="server">
                </td>
            </tr>
            <tr>               
                <td align="left">
                    Name
                </td>
                <td align="right">
                    :
                </td>
                <td id="tdCusName" runat="server" align="left" colspan="4">
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    Address
                </td>
                <td align="right" valign="top">
                    :
                </td>
                <td id="tdAddress" runat="server" colspan="4">
                </td>
            </tr>
            <tr>
                <td align="left">
                    Phone
                </td>
                <td align="right">
                    :
                </td>
                <td id="tdPhone" runat="server" colspan="4">
                </td>
            </tr>           
            <tr>
                <td align="center" colspan="100%" width="100%">
                    <div id="divItemDet" runat="server" width="100%">
                    </div>
                </td>
            </tr>            
            <tr>
                <td id="tdNetAmt" runat="server">
                </td>
            </tr>           
            <tr>
                <td align="left">
                    Note :
                </td>
                <td align="right" colspan="100%">
                    Signature
                </td>
            </tr>
            <tr>
                <td colspan="100%" align="left">
                    *Goods once sold cannot be taken back.
                </td>
            </tr>
            <tr>
                <td colspan="100%" align="left">
                    *All the disputes are suject to hyderabad Jurisdisction Only.
                </td>
            </tr>
            <tr>
                <td colspan="100%" align="center">
                    THANQ VISIT AGAIN
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
