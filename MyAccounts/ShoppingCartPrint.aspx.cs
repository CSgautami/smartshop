﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;
namespace MyAccounts20
{
    public partial class ShoppingCartPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ShopingCart_BLL obj_Bll = new ShopingCart_BLL();
            string OrderID = Request.QueryString["OrderID"];
            string Status = Request.QueryString["Status"]; 
            DataSet ds = new DataSet();
            if (!IsPostBack)
            {
                if (OrderID != "")
                {
                    ds = obj_Bll.GetPrintData(OrderID,Session[MyAccountsSession.UserID].ToString());
                    StringBuilder sb = new StringBuilder();                    
                    sb.Append("<table rules='all' border='0' cellspacing='0' align='center' width='100%'>");                    
                    if (ds.Tables.Count > 1)
                    {
                        tdAddress.InnerHtml = ds.Tables[0].Rows[0]["ShippingAddress"].ToString();
                        tdTransport.InnerText = ds.Tables[0].Rows[0]["Transport"].ToString();                        
                        tdOrderID.InnerText = ds.Tables[0].Rows[0]["OID"].ToString();
                        tdOrderDate.InnerText = ds.Tables[0].Rows[0]["Date"].ToString();
                        tdPayment.InnerText = ds.Tables[0].Rows[0]["ModeOfPayment"].ToString();
                        tdPaymentStatus.InnerText = ds.Tables[0].Rows[0]["PaymentStatus"].ToString();
                        tdLoginID.InnerText = ds.Tables[1].Rows[0]["LoginID"].ToString().Replace(".", "/");
                        tdName.InnerText = ds.Tables[1].Rows[0]["LoginName"].ToString();
                    }
                    sb.Append("<br/><table rules='all'  rules='all' border='0' style='border: 1px solid;' cellspacing='0' cellpading='0' align='center' width='100%'>");
                    if (ds.Tables.Count > 2)
                    {
                        foreach (DataColumn dc in ds.Tables[2].Columns)
                            sb.Append("<th>" + dc.ColumnName + "</th>");
                        int cellscount = ds.Tables[2].Columns.Count;
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            sb.Append("<tr>");
                            for (int i = 0; i < cellscount; i++)
                            {
                                if(i==0 || i==2)
                                    sb.Append("<td align='left'>&nbsp;" + dr[i].ToString() + "</td>");
                                else if(i==1)
                                    sb.Append("<td>&nbsp;" + dr[i].ToString() + "</td>");
                                else
                                    sb.Append("<td align='right'>&nbsp;" + dr[i].ToString() + "</td>");
                            }
                            sb.Append("</tr>");                            
                        }
                        sb.Append("<tr>");
                        sb.Append("<td align='right' colspan='" + (cellscount - 1).ToString() + "'><b>Total</b></td><td align='right'>&nbsp;" + ds.Tables[2].Compute("sum(Amount)", "") + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td align='right' colspan='" + (cellscount - 1).ToString() + "'><b>Delivery Charges</b></td><td align='right'>&nbsp;" + ds.Tables[0].Compute("sum(DeliveryCharges)","") + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td align='right' colspan='" + (cellscount - 1).ToString() + "'><b>Grand Total</b></td><td align='right'>&nbsp;" + ds.Tables[0].Compute("sum(NetAmount)", "") + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        //NumberInWords obj = new NumberInWords();
                        //sb.Append("<td align='left' colspan='" + (cellscount).ToString() + "'><b>InWords :"+ obj.NumberToWords(Convert.ToInt32( ds.Tables[1].Compute("sum(NetAmount)", ""))) +" </b></td>");
                        sb.Append("</tr>");
                        sb.Append("</table><br/>");
                        divItemDet.InnerHtml = sb.ToString();
                        if (Status != "" && Status!=null)
                        {
                            tdProduct.Visible = false;
                            tdLogOut.Visible = false;
                        }
                        else
                        {
                            tdProduct.Visible = true;
                            tdLogOut.Visible = true;
                        }
                    }
                }
            }
        }

        protected void lbtnProducts_Click(object sender, EventArgs e)
        {
            Response.Redirect("ShopingCart.aspx", false);
        }

        protected void lbtnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx", false);
        }
        
    }
}
