﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
namespace MyAccounts20
{
    public partial class OrderSummury : System.Web.UI.Page
    {
        ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
        string OrderID;
        string UserID;
        decimal DeliveryCharges = 0;
        decimal Total = 0;
        decimal GrandTotal = 0;        
        protected void Page_Load(object sender, EventArgs e)
        {
            OrderID = Request.QueryString["OrderID"];
            UserID =Session[MyAccountsSession.UserID].ToString();
            if (!IsPostBack)
            {
                if(OrderID!="" && OrderID != null)
                    GetData(OrderID);
                ddlShipmentDet.Items.Insert(0, new ListItem("Select", "0"));
                ddlShipmentDet.Items.Insert(1, new ListItem("Delivered", "1"));
                ddlShipmentDet.Items.Insert(2, new ListItem("Pending", "2"));
                ddlShipmentDet.Items.Insert(3, new ListItem("All", "3"));
                ddlShipmentDet.Items.Insert(4, new ListItem("InProcess", "4"));
                txtDisputeDate.Text = DateTime.Now.Month.ToString();
                FillShipDet();
            }
        }
        void FillShipDet()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = obj_BLL.GetOrderShipmentDet(OrderID);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ddlShipmentDet.SelectedValue = ds.Tables[0].Rows[0]["ShipmentDetID"].ToString();
                    // ddlShipmentDet.SelectedItem = ds.Tables[0].Rows[0]["ShipmentDet"].ToString();
                    txtTransport.Text  = ds.Tables[0].Rows[0]["TransportName"].ToString();
                    txtDisputeDate.Text = ds.Tables[0].Rows[0]["DisputeDate"].ToString();
                    txtTrackNo.Text = ds.Tables[0].Rows[0]["TrackNo"].ToString();
                }
                else
                {
                    ddlShipmentDet.SelectedValue = "0";
                    txtTransport.Text  = "";
                    txtDisputeDate.Text = "";
                    txtTrackNo.Text = "";
                }

            }
            catch (Exception ex) { throw; }
        }
        void GetData(string OrderID)
        {
            DataSet ds = new DataSet();
            ds = obj_BLL.GetOrderSummury(OrderID);
            gvOrder.DataSource = ds.Tables[0];
            gvOrder.DataBind();
            gvOrderDetails.DataSource = ds.Tables[1];
            gvOrderDetails.DataBind();

            //if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //{
            //    DataRow dr = ds.Tables[0].Rows[0];
            //    DeliveryCharges = (decimal)dr["DeliveryCharges"];
            //}
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (ddlShipmentDet.SelectedValue == "1")
                //{
                //    lblMsg.Text = "Stock Already Delivered";
                //    return;
                //}
                //else
                //{
                    string res = obj_BLL.SaveOrderShipmentDetails(OrderID, ddlShipmentDet.SelectedItem.ToString(), ddlShipmentDet.SelectedValue, txtTransport.Text, getDateFromat(txtDisputeDate.Text), txtTrackNo.Text, UserID);
                    if (res == "" || res == "0")
                    {
                        lblMsg.Text = "Error In Record Insertion";
                        return;
                    }
                    if (res == "-1")
                    {
                        lblMsg.Text = "Shippment Details is already exit";
                        return;
                    }
                    if (res == "1")
                    {
                        lblMsg.Text = "Shippment Details Saved Successfully";
                    }
                //}
            }
            catch (Exception ex)
            { }

        }
        public string getDateFromat(string date)
        {
            date = date.Replace("/", "-");
            date = date.Replace(".", "-");
            string[] dt = date.Split('-');
            return dt[1] + "-" + dt[0] + "-" + dt[2];
        }

        protected void gvOrderDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal rowTotal=Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem,"Amount"));
                Total = Total + rowTotal;
            }
            DataSet ds = new DataSet();
            ds = obj_BLL.GetOrderSummury(OrderID);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                DeliveryCharges = (decimal)dr["DeliveryCharges"];
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotal = (Label)e.Row.FindControl("lblSubTotal");
                Label lblDCharges = (Label)e.Row.FindControl("lblDCharges");
                Label lblNet = (Label)e.Row.FindControl("lblNet");
                lblTotal.Text = Total.ToString("0.00");
                lblDCharges.Text  = DeliveryCharges.ToString("0.00");
                lblNet.Text  = (Total + DeliveryCharges).ToString("0.00");
            }
        }
       

    }
}
