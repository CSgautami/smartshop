﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;

namespace MyAccounts20
{
    public partial class ADDAddress : System.Web.UI.Page
    {
        ADDAddress_BLL Obj_BLL = new ADDAddress_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();                
                GetDefaults();
                //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);

            }              
        }

        private void GetDefaults()
        {
            RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
            ddlArea.DataSource = Obj_BLL.GetArea();
            ddlArea.DataTextField = "AreaCentre";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();

            ddlCity.DataSource = Obj_BLL.GetCity();
            ddlCity.DataTextField = "CityName";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();

            ddlState.DataSource = Obj_BLL.GetState();
            ddlState.DataTextField = "StateName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();

            ddlArea.Items.Insert(0, new ListItem("Select", "0"));
            ddlCity.Items.Insert(0, new ListItem("Select", "0"));
            ddlState.Items.Insert(0, new ListItem("Select", "0"));
        }

        
        
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAreaWiseZone(string AreaID)
        {
            try
            {
                RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
                return Obj_BLL.GetAreaWiseZone(AreaID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select Area";
                ddlArea.Focus();
                return;
            }
            if (ddlCity.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select City";
                ddlCity.Focus();
                return;
            }
            if (ddlState.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select State";
                ddlState.Focus();
                return;
            }
            string res = Obj_BLL.InsertAddress(hidUserID.Value, txtName.Text, txtHNO.Text, txtSOne.Text, txtSTwo.Text, ddlArea.SelectedValue, ddlCity.SelectedValue, ddlState.SelectedValue,txtMark.Text, txtZone.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Address";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Address already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "Address saved successfully";
                txtCustomer.Text = "";
                txtName.Text = "";
                txtHNO.Text = "";
                txtSOne.Text = "";
                txtSTwo.Text = "";
                ddlArea.SelectedValue = "0";
                ddlCity.SelectedValue = "0";
                ddlState.SelectedValue = "0";
                txtMark.Text = "";
                txtZone.Text = "";
                txtName.Focus();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateAddress(ShippingID.Value, txtName.Text, txtHNO.Text, txtSOne.Text, txtSTwo.Text, ddlArea.SelectedValue, ddlCity.SelectedValue, ddlState.SelectedValue, txtMark.Text, txtZone.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Address";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Address already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "Address Updated successfully";
                txtCustomer.Text = "";
                txtName.Text = "";
                txtHNO.Text = "";
                txtSOne.Text = "";
                txtSTwo.Text = "";
                ddlArea.SelectedValue = "0";
                ddlCity.SelectedValue = "0";
                ddlState.SelectedValue = "0";
                txtMark.Text = "";
                txtZone.Text = "";
                txtName.Focus();

            }
        }

        
    }
}
