﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;

namespace MyAccounts20
{
    public partial class PrintVatReturn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            VatReturn_BLL Obj_BLL = new VatReturn_BLL();
            string SRNo = Request.QueryString["SRNo"];
            string BranchID = Request.QueryString["BranchID"];
            DataSet ds = new DataSet();
            if (!IsPostBack)
            {
                if (SRNo != "")
                {
                    ds = Obj_BLL.GetVatReturnPrintData(SRNo, BranchID);
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb1 = new StringBuilder();
                    if (ds.Tables.Count > 0)
                    {
                        string strTin;
                        int i;
                        strTin = ds.Tables[0].Rows[0]["TinNo"].ToString();
                        tdCircle.InnerHtml = ds.Tables[0].Rows[0]["Circle"].ToString();
                        tdDivision.InnerHtml = ds.Tables[0].Rows[0]["Division"].ToString();
                        tdCompany.InnerHtml = ds.Tables[0].Rows[0]["BranchName"].ToString();
                        tdName.InnerHtml = ds.Tables[0].Rows[0]["BranchName"].ToString();
                        tdAddress.InnerHtml = ds.Tables[0].Rows[0]["BranchAddress"].ToString();
                        tdEmailID.InnerHtml = ds.Tables[0].Rows[0]["EmailID"].ToString();
                        lblPhone.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                        tdMdName.InnerHtml = ds.Tables[0].Rows[0]["ContactPerson"].ToString();
                        i = 0;
                        if (strTin != "")
                        {
                            tdTin1.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin2.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin3.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin4.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin5.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin6.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin7.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin8.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin9.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin10.InnerHtml = strTin.Substring(i, 1); i++;
                            tdTin11.InnerHtml = strTin.Substring(i, 1); i++;
                        }
                    }
                    if (ds.Tables.Count > 1)
                    {
                        DateTime dtpdate = Convert.ToDateTime(ds.Tables[1].Rows[0]["FromDate"].ToString());
                        tdFDate.InnerHtml = dtpdate.Day.ToString();
                        tdFMonth.InnerHtml = dtpdate.Month.ToString();
                        tdFYear.InnerHtml = dtpdate.Year.ToString();

                        dtpdate = Convert.ToDateTime(ds.Tables[1].Rows[0]["ToDate"].ToString());
                        tdTDate.InnerHtml = dtpdate.Day.ToString();
                        tdTMonth.InnerHtml = dtpdate.Month.ToString();
                        tdTYear.InnerHtml = dtpdate.Year.ToString();
                    }
                    string strAmt;
                    if (ds.Tables.Count > 2)
                    {
                        strAmt = ds.Tables[2].Rows[0]["PExempValue"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox6a.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVAT5AMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox7a.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVAT5"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox7b.InnerHtml = strAmt;

                        //strAmt = ds.Tables[2].Rows[0]["PVATSTDAMT"].ToString();
                        //if (Convert.ToDecimal(strAmt) == 0)
                        //    strAmt = "NIL";
                        //tdbox8a.InnerHtml = strAmt;

                        //strAmt = ds.Tables[2].Rows[0]["PVATSTD"].ToString();
                        //if (Convert.ToDecimal(strAmt) == 0)
                        //    strAmt = "NIL";
                        //tdbox8b.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVAT14P5AMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox8a1.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVAT14P5"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox8b1.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVAT1AMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox9a.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVAT1"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox9b.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PVATSATAMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox10a.InnerHtml = strAmt;

                        strAmt = ds.Tables[2].Rows[0]["PTotTax"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox11a.InnerHtml = strAmt;
                    }
                    if (ds.Tables.Count > 3)
                    {
                        strAmt = ds.Tables[3].Rows[0]["SExempted"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox12a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SExports"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox13a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SInterState"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox14a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SPurchaseVal"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox15a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVATPayable"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox15b.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVAT5AMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox16a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVAT5"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox16b.InnerHtml = strAmt;

                        //strAmt = ds.Tables[3].Rows[0]["SVATSTDAMT"].ToString();
                        //if (Convert.ToDecimal(strAmt) == 0)
                        //    strAmt = "NIL";
                        //tdbox17a.InnerHtml = strAmt;

                        //strAmt = ds.Tables[3].Rows[0]["SVATSTD"].ToString();
                        //if (Convert.ToDecimal(strAmt) == 0)
                        //    strAmt = "";
                        //tdbox17b.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVAT14P5AMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox17a1.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVAT14P5"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox17b1.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVAT1AMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox18a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVAT1"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox18b.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVATSATAMT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox19a.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["SVATSAT"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox19b.InnerHtml = strAmt;

                        strAmt = ds.Tables[3].Rows[0]["STotTax"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox20b.InnerHtml = strAmt;
                    }
                    if (ds.Tables.Count > 4)
                    {
                        strAmt = ds.Tables[4].Rows[0]["TotalAmoutToPay"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox21.InnerHtml = strAmt;

                        strAmt = ds.Tables[4].Rows[0]["Refund"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox23.InnerHtml = strAmt;

                        strAmt = ds.Tables[4].Rows[0]["CarredForward"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox24.InnerHtml = strAmt;

                        strAmt = ds.Tables[4].Rows[0]["box24a"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox24a.InnerHtml = strAmt;

                        strAmt = ds.Tables[4].Rows[0]["box24b"].ToString();
                        if (Convert.ToDecimal(strAmt) == 0)
                            strAmt = "NIL";
                        tdbox24b.InnerHtml = strAmt;
                    }
                    int count = 0, cellscount = 0;
                    string EmptyVal = "&nbsp";
                    if (ds.Tables.Count > 5)
                    {
                        sb.Append("<table  rules='all' border='1' cellspacing='0' align='center' style='font-size:x-small'>");
                        foreach (DataColumn dc in ds.Tables[5].Columns)
                        {
                            if (count == 0)
                                sb.Append("<th style='width:50px;'>" + dc.ColumnName + "</th>");
                            else if (count == 1)
                                sb.Append("<th style='width:400px;'>" + dc.ColumnName + "</th>");
                            else if (count == 2)
                                sb.Append("<th style='width:150px;'>" + dc.ColumnName + "</th>");
                            else if (count == 3)
                                sb.Append("<th style='width:400px;'>" + dc.ColumnName + "</th>");
                            else if (count == 4)
                                sb.Append("<th style='width:150px;'>" + dc.ColumnName + "</th>");
                            else if (count == 5)
                                sb.Append("<th style='width:200px;'>" + dc.ColumnName + "</th>");
                            else if (count == 6)
                                sb.Append("<th style='width:250px;'>" + dc.ColumnName + "</th>");
                            else
                                sb.Append("<th>" + dc.ColumnName + "</th>");
                            count++;
                        }
                        cellscount = ds.Tables[5].Columns.Count;
                        foreach (DataRow dr in ds.Tables[5].Rows)
                        {
                            sb.Append("<tr>");
                            for (int i = 0; i < cellscount; i++)
                            {
                                if (dr[i].ToString() == "")
                                    sb.Append("<td>" + EmptyVal + "</td>");
                                else if (i == 1)
                                    sb.Append("<td align='center'>" + dr[i].ToString() + "</td>");
                                else if (i == 2 || i == 4)
                                    sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                                else
                                    sb.Append("<td>" + dr[i].ToString() + "</td>");
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        divPaymentDet.InnerHtml = sb.ToString();
                    }
                    if (ds.Tables.Count > 6)
                    {
                        sb1.Append("<table  rules='all' border='1' cellspacing='0' align='center' style='font-size:x-small'>");
                        cellscount = ds.Tables[6].Columns.Count;
                        foreach (DataColumn dc in ds.Tables[6].Columns)
                        {
                            if (count == 0)
                                sb1.Append("<th style='width:50px;'>" + dc.ColumnName + "</th>");
                            else if (count == 1)
                                sb1.Append("<th style='width:400px;'>" + dc.ColumnName + "</th>");
                            else if (count == 2)
                                sb1.Append("<th style='width:150px;'>" + dc.ColumnName + "</th>");
                            else if (count == 3)
                                sb1.Append("<th style='width:400px;'>" + dc.ColumnName + "</th>");
                            else if (count == 4)
                                sb1.Append("<th style='width:150px;'>" + dc.ColumnName + "</th>");
                            else if (count == 5)
                                sb1.Append("<th style='width:200px;'>" + dc.ColumnName + "</th>");
                            else if (count == 6)
                                sb1.Append("<th style='width:250px;'>" + dc.ColumnName + "</th>");
                            else
                                sb1.Append("<th>" + dc.ColumnName + "</th>");
                            count++;
                        }
                        foreach (DataRow dr in ds.Tables[6].Rows)
                        {
                            sb1.Append("<tr>");
                            for (int i = 0; i < cellscount; i++)
                            {
                                if (dr[i].ToString() == "")
                                    sb1.Append("<td>" + EmptyVal + "</td>");
                                else if (i == 1)
                                    sb1.Append("<td align='center'>" + dr[i].ToString() + "</td>");
                                else if (i == 2 || i == 4)
                                    sb1.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                                else
                                    sb1.Append("<td>" + dr[i].ToString() + "</td>");
                            }
                            sb1.Append("</tr>");
                        }
                        sb1.Append("</table>");
                        divAdjDet.InnerHtml = sb1.ToString();
                    }
                }
            }
        }
    }
}

