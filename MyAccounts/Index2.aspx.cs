﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Text;

namespace MyAccounts20
{
    public partial class Index2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               GetItems();                
            }
        }
        void GetItems()
        {
            BLL.ItemCreation_BLL Obj_BLL = new BLL.ItemCreation_BLL();
            DataSet ds = Obj_BLL.GetItemsDisplay();            
            DataTable dt2;
            StringBuilder sb = new StringBuilder();
            //sb.Append("<div class='main1'>");
            //sb.Append("<div style='margin-bottom:10px;'><img src='MasterPageFiles2/images/banner.jpg' width='700' height='285'></div>");
            foreach (DataRow dr1 in ds.Tables[0].Rows)
            {
                sb.Append("<div>");
                sb.Append("<div class='salwar'>");
                sb.Append("<div class='totalsalwar'><span style='padding:0px 0px 0px 8px;'>" + dr1["HeadName"].ToString() + "</span></div>");
                sb.Append("<div class='viewmore'><a href=\"listing.aspx?HID=" + dr1["HeadGroupID"].ToString() + "\">" + "View More</a></div>");
                sb.Append("</div>");
                sb.Append("<div class='product1'>");
                ds.Tables[1].DefaultView.RowFilter = "HeadGroupID=" + dr1["HeadGroupID"].ToString();
                dt2 = ds.Tables[1].DefaultView.ToTable();
                int nNOOFItems;
                nNOOFItems = 1;               
                foreach (DataRow dr2 in dt2.Rows)
                {
                    if (nNOOFItems <= 4)
                    {
                        sb.Append("<div class='product1_1'>");

                        sb.Append("<div class='product_midd'>");
                        sb.Append("<span class='kws_off'>");
                        sb.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                        sb.Append("<tbody><tr>");
                        sb.Append("<td>" + dr2["Percentage"].ToString() + "%</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr><td>Off</td></tr></tbody></table>");
                        sb.Append("</span>");
                        sb.Append("<img src='ItemImages/" + dr2["ItemID"] + ".jpg' width='100' height='100' style='vertical-align:middle; position:relative; z-index:-1;'>");
                        sb.Append("</div>");

                        sb.Append("<div class='item_code'>");
                        sb.Append("<h2>" + dr2["ItemName"].ToString() + "</h2>");
                        sb.Append("<span>ItemCode :<span style='padding-left:3px;'>" + dr2["ItemCode"].ToString() + "</span></span><br/>");
                        sb.Append("<span><del>MRP <span class='WebRupee' style='color:#000 !important;'>Rs.</span><span style='padding-left:3px;'>" + dr2["MRP"].ToString() + "</span></del></span><br/>");
                        sb.Append("<span style='color:#1570a2; padding-left:3px;'>Our Price <span class='WebRupee' style='color:#1570a2 !important;'>Rs.</span><span style='padding-left:3px;'>" + dr2["OSP"].ToString() + "</span></span>");
                        sb.Append("</div>");

                        sb.Append("<div class='product_bottom'>");
                        sb.Append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                        sb.Append("<tbody><tr>");
                        sb.Append("<td width='40%'><input type='button' id='btnD'" + dr2["ItemID"].ToString() + " value='Details' class='add1'></td>");
                        sb.Append("<td width='28%' align='center' class='qty'>Qty<br/><input type='txt'" + dr2["ItemID"].ToString() + " class='input' ;=''/></td>");
                        sb.Append("<td width='40%' align='right'><input  type='button' id='" + dr2["ItemID"].ToString() + "btnAdd' value='Add to Cart' class='add' /></td></tr></tbody></table>");
                        sb.Append("</div>");

                        sb.Append("</div>");
                    }
                    nNOOFItems = nNOOFItems + 1;
                }
                sb.Append("</div>");
                sb.Append("</div>");
            }
            //sb.Append("</div>");
            //sb.Append("</div>");
            divItemDet.InnerHtml = sb.ToString();
        }
       
    }
}
