﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartPrint.aspx.cs" Inherits="MyAccounts20.ShoppingCartPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table align="center" width="600px" border="0">
           <%-- <tr>
                <td colspan="100%">
                </td>
            </tr>--%>
            <tr>
                <td align="center" colspan="100%" style="font-size:large">
                    <%--<div id="divBranchDet" runat="server">
                    </div>--%>
                    <%--SmartRetailer.in--%>
                    <img src="MasterPageFiles/normal logo.jpg" width="150" height="50" />
                </td>
            </tr>
            <tr>
                <td align="left" style="font-weight:bold;width:250px;">
                    Delivery Address
                </td>    
                <td style="font-weight:bold ;width:250px;" align="center" colspan="3">
                    Order Details
                </td>            
            </tr>
            <tr>
                <td id="tdAddress" runat="server" rowspan ="4" valign="top" align="left" style="width:200px;">                    
                </td>
                <td style="width:100px;">
                    Order ID
                </td>
                <td>:</td>
                <td id="tdOrderID" runat="server">                    
                </td>
            </tr>
            <tr>
                <%--<td id="tdVillage" runat="server">                    
                </td>--%>
                <td>Order Date</td>
                <td>:</td>
                <td id="tdOrderDate" runat="server" style="width:150px;">                    
                </td>
            </tr>
            <tr>
                <%--<td id="tdCity" runat="server">                    
                </td>--%>
                <td style="width:150px;">Mode Of Payment</td>
                <td>:</td>
                <td id="tdPayment" runat="server"></td>
            </tr>
            <tr>
                <%--<td id="tdPinCode" runat="server">                    
                </td>--%>
                <td>Payment Status</td>
                <td>:</td>
                <td id="tdPaymentStatus" runat="server"></td>
            </tr>
            <tr>
                <td style="font-weight:bold;">
                    Ordered By
                </td>
                <td style="font-weight:bold; display:none;" colspan="3" align="center">
                    Near Transport
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                Login ID            
                            </td>
                            <td>:</td>
                            <td id="tdLoginID" runat="server"></td>
                        </tr>
                        <tr>
                            <td colspan="3" id="tdName" runat="server" style="font-size:small;">                                
                            </td>
                        </tr>
                    </table>                    
                </td>
                <td colspan="3" id="tdTransport" runat="server" valign="top">                    
                </td>
            </tr>
            <tr>
                <td align="center" colspan="100%" width="100%">
                    <div id="divItemDet" runat="server" width="100%">
                    </div>
                </td>
            </tr>            
            <tr>
                <td align="left" colspan="100%">
                    Notes :
                </td>
                <%--<td align="right" colspan="100%">
                    Signature
                </td>--%>
            </tr>
            <tr>
                <td colspan="100%" align="left">
                    *Goods once sold cannot be taken back.
                </td>
            </tr>
            <tr>
                <td colspan="100%" align="left">
                    *All the disputes are suject to hyderabad Jurisdisction Only.
                </td>
            </tr>
            <%--<tr>
                <td colspan="100%" align="center">
                    THANQ VISIT AGAIN
                </td>
            </tr>--%>
            <tr>
                <td align="center" style="font-weight:bold; width:250px;">
                    Ph: 040-65021555, 9247990226
                </td>
                <td style="width:250px;" colspan="3">
                    
                </td>
            </tr>
            <tr style="display:none;">
                <td align="right" style="width:250px;" runat="server" id="tdProduct">
                    <asp:LinkButton ID="lbtnProducts" runat="server" Text="Back To Products" 
                        onclick="lbtnProducts_Click"></asp:LinkButton>
                </td>
                <td style="width:250px;" colspan="3" runat="server" id="tdLogOut">
                    <asp:LinkButton ID="lbtnLogOut" runat="server" Text="Log Out" 
                        onclick="lbtnLogOut_Click"></asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
