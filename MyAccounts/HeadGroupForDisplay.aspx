﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="HeadGroupForDisplay.aspx.cs" Inherits="MyAccounts20.HeadGroupForDisplay" Title="HeadGroup For Display" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <table style="height:200px; width:600px;">
        <tr>
            <td colspan="100%">
            <%--<div class="divgrid" style="width:500px; height:200px; border:solid 1px #98bf21;">
            <table  id="divGrid" style="width:500px;">
                <tr class="dbookheader">   
                 <th style="width: 0px; display:none;">
                        HeadGroupID
                    </th>
                    <th style="width: 0px; display:none;">
                        ItemID
                    </th>                               
                    <th style="width: 50px;">
                        HeadGroupName
                    </th>
                    <th style="width: 90px;">
                        ItemName
                    </th>                                
                </tr>
            </table>
            </div>--%>
            <asp:GridView ID="gvHeadGroupFD" runat="server" AutoGenerateColumns="false" CssClass="grid"
              PageSize="13" AllowPaging="true" Font-Size="Small" OnPageIndexChanging="gvCategory_PageIndexChanging"  width="80%" >
                    <Columns>
                         <asp:TemplateField HeaderText="HeadGroup">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidHeadID" value='<%#Eval("HeadGroupID")%>' />
                                <asp:Label ID="lblHead" runat="server" Text='<%#Eval("HeadGroupName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidCategoryID" value='<%#Eval("CategoryID")%>' />
                                <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("Category") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                    OnClick="lbtnEdit_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                    OnClick="lbtnDelete_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
             </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                HeadGroup
            </td>
            <td>
                <asp:DropDownList ID="ddlHeadGroup" runat="server" Width="150px"></asp:DropDownList>
            </td>
            <td>
                ItemName
            </td>
            <td>
                <asp:DropDownList ID="ddlItem" runat="server" Width="150px"></asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnAdd" runat="server" Text="ADD" onclick="btnAdd_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="100%;" align="center">
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px"/>
                 <asp:Button ID="btnCancel" Text="Cancel" runat="server" 
                    onclick="btnCancel_Click"/>
            </td>            
        </tr>
    </table>
</div>
</asp:Content>
