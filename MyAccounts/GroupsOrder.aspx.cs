﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class GroupsOrder : System.Web.UI.Page
    {
        GroupsOrder_BLL obj_Bll = new GroupsOrder_BLL();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetHeadGroups();
                GetGroups();
                ddlHeadGroup.Focus();
                //GetColsForTable();

            }
        }
        private void GetHeadGroups()
        {
            HeadGroup_BLL Obj_BLL = new HeadGroup_BLL();
            ddlHeadGroup.DataSource = Obj_BLL.GetHeadGroup();
            ddlHeadGroup.DataTextField = "HeadGroupName";
            ddlHeadGroup.DataValueField = "HeadGroupID";
            ddlHeadGroup.DataBind();
        }
        private void GetColsForTable()
        {
            dt.Columns.Add("SGNo");
            dt.Columns.Add("OrderID");
        }
        private void GetGroups()
        {
            gvGroup.DataSource = obj_Bll.GetProductList(ddlHeadGroup.SelectedValue).Tables[0];
            gvGroup.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            GetColsForTable();
            int SGNo, OrderID;
            string strRes;
            strRes = "";
            foreach (GridViewRow grow in gvGroup.Rows)
            {
                SGNo = Convert.ToInt32(((HtmlInputHidden)grow.FindControl("hidSGNo")).Value);
                OrderID = 0;
                if (((TextBox)grow.FindControl("txtOrder")).Text.Trim() != "")
                    OrderID = Convert.ToInt32(((TextBox)grow.FindControl("txtOrder")).Text.Trim());
                //if (OrderID == "") OrderID = 0;
                if (OrderID > 0) dt.Rows.Add(SGNo, OrderID);
            }
            if (dt.Rows.Count > 0)
            {
                //dt.TableName = "GroupsOrder";
                DataSet ds = new DataSet();
                ds.DataSetName = "XML";
                ds.Tables.Add(dt);
                ds.Tables[0].TableName = "GroupsOrder";
                strRes = obj_Bll.SaveGroupsOrder(ds.GetXml());
            }
            lblMsg.Visible = true;
            if (strRes == "" || strRes == "0")
                lblMsg.Text = "Error In Saving";
            else
                lblMsg.Text = "Successfully Saved";


        }

        protected void ddlHeadGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetGroups();
            ddlHeadGroup.Focus();
        }
    }
}
