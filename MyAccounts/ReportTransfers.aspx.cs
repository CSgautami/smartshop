﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.Text;
namespace MyAccounts20
{
    public partial class ReportTransfers : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
                txtFromDate.Value = nFirstDate.ToString("dd-MM-yyyy");
                txtToDate.Value = nLastDate.ToString("dd-MM-yyyy");
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);                
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
