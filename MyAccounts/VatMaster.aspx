﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="VatMaster.aspx.cs" Inherits="MyAccounts20.VatMaster" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    var preid='ctl00_ContentPlaceHolder1_';
        function ConfirmEdit()
                {
                    if(!confirm("Do you want to Edit?"))
                    return false;
                }
                function ConfirmDelete()
                {
                    if(! confirm("Do you want to Delete?"))
                    return false;
                } 

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div>
  
    <asp:GridView ID="gvVat" runat="server" AutoGenerateColumns="false" CssClass="grid"
    PageSize="5" AllowPaging="true" Font-Size="Small" 
                        OnPageIndexChanging="gvVat_PageIndexChanging">
                        <Columns>
                        
                            <asp:TemplateField HeaderText="VatDesc">
                                <ItemTemplate>
                                    <input type="hidden" runat="server" id="hidVatID" value='<%#Eval("VatID")%>' />
                                    <asp:Label ID="lblVatDesc" runat="server" Text='<%#Eval("VatDesc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                        OnClick="lbtnEdit_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                        OnClick="lbtnDelete_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                         </Columns>
                    </asp:GridView>
                    </br>
                    <table>
                        <tr>
                            <td>
                                VatDescription:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtVatDesc" runat="server" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                        <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                            </td> 
                            <td runat="server" id="tdUpdate">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" Visible="false" OnClick="btnUpdate_Click" />
                            </td>
                            <td runat="server" id="tdCancel">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                        <tr>
                           <td align="center" colspan="100%">
                                <asp:Label ID="lblMsg" runat="server" EnableViewState="false" ForeColor="Blue"></asp:Label>
                           </td>
                        </tr>
                    </table>
   </div>
   <input type="hidden" runat="server" id="hidVatID" />
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
