﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Net;
namespace MyAccounts20
{
    public partial class Registraton : System.Web.UI.Page
    {
        Registraton_BLL obj_BLL = new Registraton_BLL();
        //public static string strRefID;
        //public static string strPinNo;
        //public static string strRegDate;

        public  string strRefID;
        public  string strPinNo;
        public  string strRegDate;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidRefID.Value = Request.QueryString["REFID"];
                hidPinNo.Value = Request.QueryString["PINNo"];
                hidRegDate.Value = Request.QueryString["RegDate"];
                FillData();
                if (Session["UserID"] != null)
                {
                    hidUserID.Value = Session["UserID"].ToString();
                    if (Session["Type"].ToString() == "Customer" && Session["UserID"] != null && Session["UserID"].ToString() != "0" && hidRefID.Value == "")
                    {
                        GetData(hidUserID.Value);
                    }

                    if (Session["UserID"].ToString() == "0" && (hidRefID.Value == "" || hidRefID.Value == null))
                    {
                        //txtYourID.Visible = true;
                        trYourID.Visible = true;                        
                    }
                    trPwd.Visible = false;
                }
                string YourID = Request.QueryString["UserID"];
                if (YourID != "" && YourID != null)
                {
                    GetData(YourID);
                    txtSHNO.Focus();
                }
                else
                    txtName.Focus();
                //if (strRefID == null && Session["Type"].ToString() == "Customer")
                //{
                //    GetData(Session["UserID"].ToString());
                //}                
            }
            strRefID = hidRefID.Value;
            strPinNo = hidPinNo.Value;
            strRegDate = hidRegDate.Value;
        }
        void GetData(string YourID)
        {
            DataSet ds = obj_BLL.GetData(YourID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtYourID.Text = ds.Tables[0].Rows[0]["RedgID"].ToString();
                txtName.Text = ds.Tables[0].Rows[0]["ApplicantName"].ToString();
                txtFather.Text = ds.Tables[0].Rows[0]["FatherOrHusband"].ToString();
                txtDOB.Text = ds.Tables[0].Rows[0]["DOB"].ToString();
                txtBLM.Text = ds.Tables[0].Rows[0]["LandMark"].ToString();
                txtBHNo.Text = ds.Tables[0].Rows[0]["HNo"].ToString();
                txtBPVC.Text = ds.Tables[0].Rows[0]["Post"].ToString();
                txtBCMA.Text = ds.Tables[0].Rows[0]["City"].ToString();
                if (ds.Tables[0].Rows[0]["DistrictID"].ToString() != "" && ds.Tables[0].Rows[0]["DistrictID"].ToString() != null)
                    ddl_BDC.SelectedValue = ds.Tables[0].Rows[0]["DistrictID"].ToString();
                ddl_BState.SelectedValue = ds.Tables[0].Rows[0]["StateID"].ToString();
                txtBPC.Text = ds.Tables[0].Rows[0]["PinCode"].ToString();
                txtSHNO.Text = ds.Tables[0].Rows[0]["HNo"].ToString();
                txtSPVC.Text = ds.Tables[0].Rows[0]["Post"].ToString();
                txtSCMA.Text = ds.Tables[0].Rows[0]["City"].ToString();
                if (ds.Tables[0].Rows[0]["DistrictID"].ToString() != "" && ds.Tables[0].Rows[0]["DistrictID"].ToString() != null)
                    ddl_SDC.SelectedValue = ds.Tables[0].Rows[0]["DistrictID"].ToString();
                ddl_SState.SelectedValue = ds.Tables[0].Rows[0]["StateID"].ToString();
                txtSPC.Text = ds.Tables[0].Rows[0]["PinCode"].ToString();
                txtSLM.Text = ds.Tables[0].Rows[0]["SLandMark"].ToString();
                txtPhone.Text = ds.Tables[0].Rows[0]["PhoneNo"].ToString();
                txtMobile.Text = ds.Tables[0].Rows[0]["Mobile"].ToString();
                txtPAN.Text = ds.Tables[0].Rows[0]["PANNo"].ToString();
                txtEmail.Text = ds.Tables[0].Rows[0]["EmailID"].ToString();
                txtpwd.Text = ds.Tables[0].Rows[0]["Password"].ToString();
                trPwd.Visible = true;
                trCPWd.Visible = false;                
                txtNomineeName.Text = ds.Tables[0].Rows[0]["NomineeName"].ToString();
                txtrelation.Text = ds.Tables[0].Rows[0]["Relation"].ToString();
                txtBank.Text = ds.Tables[0].Rows[0]["BankName"].ToString();
                txtAccountNO.Text = ds.Tables[0].Rows[0]["AccountNo"].ToString();
                txtIFScode.Text = ds.Tables[0].Rows[0]["IFSCode"].ToString();
                txtBranchName.Text = ds.Tables[0].Rows[0]["BranchName"].ToString();
                btnSave.Text = "Update";                
                if (Session["UserID"].ToString() != "0")
                    txtName.Enabled = false;
      
            }
            else
            {
                CancelClick();
                lblMsg.Text = "No Data Found";
            }
        }
        void FillData()
        {
            DataSet ds = new DataSet();
            try
            {
                ds = obj_BLL.GetFilldata();
                ddl_BState.DataSource = ds.Tables[0];
                ddl_BState.DataTextField = "StateName";
                ddl_BState.DataValueField = "StateID";
                ddl_BState.DataBind();
                ddl_BState.Items.Insert(0, new ListItem("Select", "0"));
                ddl_BState.SelectedIndex = 0;
                ddl_SState.DataSource = ds.Tables[0];
                ddl_SState.DataTextField = "StateName";
                ddl_SState.DataValueField = "StateID";
                ddl_SState.DataBind();
                ddl_SState.Items.Insert(0, new ListItem("Select", "0"));
                ddl_SState.SelectedIndex = 0;
                //ddlState.Items.Insert(0, new ListItem("Select", "0"));
                ddl_BDC.DataSource = obj_BLL.GetDistrict().Tables[0];
                ddl_BDC.DataTextField = "DistrictName";
                ddl_BDC.DataValueField = "DistrictID";
                ddl_BDC.DataBind();
                ddl_BDC.Items.Insert(0, new ListItem("Select", "0"));
                ddl_SDC.DataSource = obj_BLL.GetDistrict().Tables[0];
                ddl_SDC.DataTextField = "DistrictName";
                ddl_SDC.DataValueField = "DistrictID";
                ddl_SDC.DataBind();
                ddl_SDC.Items.Insert(0, new ListItem("Select", "0"));


            }
            catch
            {
                throw;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = "";
            try
            {              
                //if(txtPinCode.Text =="")
                if (btnSave.Text == "Submit")
                {
                    //lblMsg.Text = "";
                    if (txtPassword.Text != txtCpassword.Text)
                    {
                        lblMsg.Text = "Check Password and ConfirmPassword.";
                        return;
                    }
                    if (ddl_BDC.SelectedIndex <= 0)
                    {
                        lblMsg.Text = "Please Select District/City";
                        ddl_BDC.Focus();
                        return;

                    }
                    if (ddl_BState.SelectedIndex <= 0)
                    {
                        lblMsg.Text = "Please Select State";
                        ddl_BState.Focus();
                        return;
                    }
                    if (ddl_SDC.SelectedIndex <= 0)
                    {
                        lblMsg.Text = "Please Select District/City";
                        ddl_SDC.Focus();
                        return;
                    }
                    if (ddl_SState.SelectedIndex <= 0)
                    {
                        lblMsg.Text = "Please Select State";
                        ddl_SState.Focus();
                        return;
                    }
                    if (strRefID == "" || (strPinNo == "" && Session["UserID"].ToString() != "0"))
                    {
                        Response.Redirect("QuickJoin.aspx", true);
                    }
                    res = obj_BLL.InsertRedg(strRefID, strPinNo, dateFormat(strRegDate), txtName.Text, txtFather.Text, dateFormat(txtDOB.Text), txtBLM.Text, txtBHNo.Text, txtBPVC.Text, txtBCMA.Text, ddl_BDC.SelectedValue, ddl_BState.SelectedValue, txtBPC.Text, txtSHNO.Text, txtSPVC.Text, txtSCMA.Text, ddl_SDC.SelectedValue, ddl_SState.SelectedValue, txtSPC.Text, txtSLM.Text, txtPhone.Text, txtMobile.Text, txtPAN.Text, txtEmail.Text, txtPassword.Text, txtNomineeName.Text, txtrelation.Text, txtBank.Text, txtAccountNO.Text, txtIFScode.Text, txtBranchName.Text);
                    if (res == "0" || res == "")
                    {
                        lblMsg.Text = "Error while saving Registration";
                    }
                    else if (res == "-1")
                    {
                        lblMsg.Text = "Registration already existed";
                    }
                    else
                    {
                        lblMsg.Text = "Registration saved successfully - YourID : " + res;
                        txtYourID.Text = res.ToString();
                        btnSave.Visible = false;
                        strRefID = "";
                        strPinNo = "";
                        Response.Redirect("RegistrationConfirm.aspx?YourID=" + res + " &MobileNo=" + txtMobile.Text);
                        //txtName.Text = "";
                        //txtFather.Text = "";
                        //txtDOB.Text = "";
                        //txtLandMark.Text = "";
                        //txtHouseNO.Text = "";
                        //txtPost.Text = "";
                        //txtCity.Text = "";
                        //ddlDistrict.SelectedIndex = 0;
                        //ddlState.SelectedIndex = 0;
                        //txtPinCode.Text = "";
                        //txtPhone.Text = "";
                        //txtMobile.Text = "";
                        //txtPAN.Text = "";
                        //txtEmail.Text = "";
                        //txtPassword.Text = "";
                        ////txtCpassword.Text = "";
                        //txtNomineeName.Text = "";
                        //txtrelation.Text = "";
                        //txtBank.Text = "";
                        //txtAccountNO.Text = "";
                        //txtIFScode.Text = "";
                        //txtBranchName.Text = "";
                        //txtName.Focus();
                    }
                }
                else
                {
                    res = obj_BLL.UpdateRedg(txtYourID.Text, txtName.Text, txtFather.Text, dateFormat(txtDOB.Text), txtBLM.Text, txtBHNo.Text, txtBPVC.Text, txtBCMA.Text, ddl_BDC.SelectedValue, ddl_BState.SelectedValue, txtBPC.Text, txtSHNO.Text, txtSPVC.Text, txtSCMA.Text, ddl_SDC.SelectedValue, ddl_SState.SelectedValue, txtSPC.Text, txtSLM.Text, txtPhone.Text, txtMobile.Text, txtPAN.Text, txtEmail.Text, txtNomineeName.Text, txtrelation.Text, txtBank.Text, txtAccountNO.Text, txtIFScode.Text, txtBranchName.Text);
                    CancelClick();
                    btnSave.Text = "Submit";
                    lblMsg.Text = "Updated Successfully";
                    txtName.Focus();
                    if (Session[MyAccountsSession.TempOrderID] != null)
                        Response.Redirect("ConfirmPage.aspx", false);
                }   


            }
            catch
            {
                throw;
            }
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        void CancelClick()
        {
            try
            {
                txtYourID.Text = "";
                btnSave.Visible = true;
                txtName.Text = "";
                txtFather.Text = "";
                txtDOB.Text = "";
                txtBLM.Text = "";
                txtBHNo.Text = "";
                txtBPVC.Text = "";
                txtBCMA.Text = "";
                ddl_BDC.SelectedIndex = 0;
                ddl_BState.SelectedIndex = 0;
                txtBPC.Text = "";
                txtSHNO.Text = "";
                txtSPVC.Text = "";
                txtSCMA.Text = "";
                ddl_SDC.SelectedIndex = 0;
                ddl_SState.SelectedIndex = 0;
                txtSPC.Text = "";
                txtSLM.Text = "";
                txtPhone.Text = "";
                txtMobile.Text = "";
                txtPAN.Text = "";
                txtEmail.Text = "";
                txtPassword.Text = "";
                txtpwd.Text = "";
                txtNomineeName.Text = "";
                txtrelation.Text = "";
                txtBank.Text = "";
                txtAccountNO.Text = "";
                txtIFScode.Text = "";
                txtBranchName.Text = "";
                lblMsg.Text = "";
                ckb_Billadd.Checked = false;
            }
            catch
            {
                throw;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            CancelClick();
        }

        protected void txtYourID_TextChanged(object sender, EventArgs e)
        {
            GetData(txtYourID.Text);
        }

        protected void ckb_Billadd_CheckedChanged(object sender, EventArgs e)
        {
            if (ckb_Billadd.Checked == true)
            {
                txtSHNO.Text = txtBHNo.Text;
                txtSPVC.Text = txtBPVC.Text;
                txtSCMA.Text = txtBCMA.Text;
                ddl_SDC.SelectedValue = ddl_BDC.SelectedValue;
                ddl_SState.SelectedValue = ddl_BState.SelectedValue;
                txtSPC.Text = txtBPC.Text;
                txtSLM.Text = txtBLM.Text;
            }
        }
       
    }
}



