﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="OrderHistory.aspx.cs" Inherits="MyAccounts20.OrderHistory" Title="OrderHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/jscript" language="javascript">
        function FindData(OrderID)
        {
            try
            {         
                var Status='O';
                window.open("OrderDetailsReport.aspx?OrderID="+OrderID+"&Status="+Status,"","Height=700,Width=800,scrollbars=yes");
            }
            catch(err)
            {
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" style="width: 700px">
            <%--<tr>
                <td>&nbsp;</td>
            </tr>--%>
            <tr>
                <td background="MasterPageFiles/top--bg.jpg" style="height:45px; color:White;font-weight:bold;">
                    Order History
                    
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <asp:GridView ID="gvOrder" AutoGenerateColumns="false" runat="server">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table width="100" align="center" bgcolor="#ecebeb" class="zixegreen2" >
                                            <tr>
                                                <td width="100" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                    Order No
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table bgcolor="#f6fef0" class="zixe4" width="100" height="110">
                                            <tr>
                                                <td bgcolor="#f6fef0" class="zixe4" width="100" height="110">
                                                    <input type="hidden" id="hidOrderID" runat="server" value='<%#Eval("OID") %>' />
                                                    <%--<a href='ShoppingCartPrint.aspx?OrderID=<%#Eval("OID")%>&Status=O'><%#Eval("OID")%></a>--%>
                                                    <asp:LinkButton ID="lbtnOrderID" runat="server" Text='<%#Eval("OID")%>' OnClick="lbtnOrderID_Click"></asp:LinkButton>    
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>                                    
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table width="100" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                            <tr>
                                                <td width="100" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                    Date
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table bgcolor="#f6fef0" class="zixe4" width="100" height="110">
                                            <tr>
                                                <td bgcolor="#f6fef0" class="zixe4" width="100" height="110">
                                                    <asp:Label ID="lblOrderDate" runat="server" Text='<%#Eval("Date") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table width="200" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                            <tr>
                                                <td width="200" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                    Status
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table bgcolor="#f6fef0" class="zixe4" width="200" height="110">
                                            <tr>
                                                <td bgcolor="#f6fef0" class="zixe4" width="200" height="110">
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>    
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table width="100" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                            <tr>
                                                <td width="100" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                    Amount
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table bgcolor="#f6fef0" class="zixe4" width="100" height="110">
                                            <tr>
                                                <td bgcolor="#f6fef0" class="zixe4" width="100" height="110">
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("NetAmount")%>'></asp:Label>    
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table width="200" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                            <tr>
                                                <td width="200" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                    Delivered To  
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table bgcolor="#f6fef0" class="zixe4" width="200" height="110">
                                            <tr>
                                                <td bgcolor="#f6fef0" class="zixe4" width="200" height="110">
                                                    <asp:Label ID="lblDelivery" runat="server" Text='<%#Eval("ShippingAddress")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Near Transport">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransport" Width="100px" runat="server" Text='<%#Eval("Transport")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
