﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="HeadGroupsOrder.aspx.cs" Inherits="MyAccounts20.HeadGroupsOrder" Title="Head Groups Order" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" cellpadding ="10px" cellspacing="15px" style="width:800px;">
            <tr>
                <td>
                    <asp:GridView ID="gvHeadGroup" runat="server" AutoGenerateColumns="false" >    
                        <Columns>                                                   
                            <asp:TemplateField HeaderText ="Head Group Name" ItemStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <input type="hidden" id="hidHGNo" runat="server" value='<% #Eval("HGNo") %>' />
                                    <asp:Label  ID="lblHGName" runat="server" Text='<% #Eval("HGName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText ="Order ID" >
                                <ItemTemplate  >
                                    <asp:TextBox ID="txtOrder" runat="server" Text ='<% #Eval("OrderID") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>                 
                     </asp:GridView>                                                    
                </td>
             </tr>
             <tr>
                <td>
                    <asp:Label ID="lblMsg" Text="" runat="server" Visible="false" ForeColor = "Red"></asp:Label>
                </td>
             </tr>
             <tr>
                <td>
                    <asp:Button ID="btnSave" Text="Save" runat="server" onclick="btnSave_Click" />
                </td>
             </tr>
        </table>
    </div>
</asp:Content>
