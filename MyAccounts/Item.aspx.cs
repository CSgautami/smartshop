﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class Item : System.Web.UI.Page
    {
        ItemCreation_BLL bllHome = new ItemCreation_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //this.Title = "Vanitha - Item";
                if (Request.QueryString["ItemID"] != null)
                    FillDisplayGroups(Request.QueryString["ItemID"].ToString());
            }
        }

        void FillDisplayGroups(string SGNo)
        {
            try
            {
                DataSet ds = bllHome.GetItem(SGNo);
                dlItems.DataSource = ds;
                dlItems.DataBind();
            }
            catch
            {
                throw;
            }
        }
    }
}
