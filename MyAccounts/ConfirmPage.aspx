﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/UserMaster.Master" CodeBehind="ConfirmPage.aspx.cs" Inherits="MyAccounts20.ConfirmPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language ="javascript" type ="text/javascript">
        var preid;
        preid ='ctl00_ContentPlaceHolder1_'
        function ConfirmEdit()
        {
            if (! confirm("Do you want to Edit?") )             
            return false;            
        }
        function ConfirmDelete()
        {
            if (! confirm("Do you want to Delete?") )
            return false;            
        }
        function Validation()
		{
		    if(document.getElementById(preid+"gvShopingCart").rows.length<2)
		    {
		        alert("Select Items");
		        //document.getElementById("txtQty").focus();
		        return false;
		    }
		    if(document.getElementById(preid+"rAddress").checked==false)
		    {
		        alert("Select Address");		        
		        document.getElementById(preid+"rAddress").focus();
		        return false;
		    }
//		    if(document.getElementById("txtTransport").value=="")
//		    {
//		        alert("Enter Near Transport");
//		        document.getElementById("txtTransport").focus();
//		        return false;
//		    }
            if(document.getElementById(preid+"rcdCard").checked==false && document.getElementById(preid+"rOnline").checked==false && document.getElementById(preid+"rCDelivery").checked==false)
		    {
		        alert("Select Mode Of Payment");		        
		        document.getElementById(preid+"rcdCard").focus();
		        return false;
		    }
		}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">                                    
                    <tr>
                        <td>                    
                            <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="285" valign="top">
                                        <table width="720" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <table width="720" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="59">
                                                                <img src="MasterPageFiles/top-left.jpg" width="59" height="42" />
                                                            </td>
                                                            <td background="MasterPageFiles/top--bg.jpg">
                                                                &nbsp;
                                                            </td>
                                                            <td width="68" align="right">
                                                                <img src="MasterPageFiles/top-right.jpg" width="68" height="42" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="border-left-right-bg">
                                                    <table width="720" border="0" align="center" cellpadding="0" cellspacing="2">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="gvShopingCart" runat="server" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="46" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="46" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            SL.No
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="46">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="46">
                                                                                            <asp:Label ID="lblSno" runat="server" Text='<%#Eval("SlNo")%>'></asp:Label>    
                                                                                        </td>
                                                                                    </tr>                                                                                    
                                                                                </table>                                                                                
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="70" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="70" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            ItemCode
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="70">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="70">
                                                                                            <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode")%>'></asp:Label>    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                                
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                        
                                                                        <asp:TemplateField>                    
                                                                            <HeaderTemplate>
                                                                                <table width="250" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="250" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            Item Name
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="250">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="250">
                                                                                            <input type="hidden" runat="server" id="hidTODID" value='<%#Eval("TODID") %>' />
                                                                                            <input type="hidden" runat="server" id="hidItemID" value='<% #Eval("ItemID") %>'/>
                                                                                            <asp:Label ID="lblItemName" runat="server" Text=' <%#Eval("ItemName")%>'></asp:Label>    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                                
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField> 
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="89" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="89" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            M.R.P
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="89">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="89">
                                                                                            <asp:Label ID="lblMRP" runat="server" Text=' <%#Eval("MRP")%>'></asp:Label>            
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                                
                                                                            </ItemTemplate>                    
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="79" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="79" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            Price
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="79">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="79">
                                                                                            <asp:Label ID="lblPrice" runat="server" Text=' <%#Eval("Price")%>'></asp:Label>    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                                
                                                                            </ItemTemplate>                    
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="39" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="39" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            Qty
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate >
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="39">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="39">
                                                                                            <asp:Label ID="lblQty" runat="server" Text=' <%#Eval("Qty")%>'></asp:Label>
                                                                                            <%--<asp:TextBox ID="txtQty" Width="30px" runat="server" Text='<%#Eval("Qty")%>'
                                                                                            OnTextChanged="txtQty_TextChanged" AutoPostBack="true" ></asp:TextBox>--%>    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                                
                                                                            </ItemTemplate>                    
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <table width="90" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="90" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            Amount
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table bgcolor="#f6fef0" class="zixe4" width="90">
                                                                                    <tr>
                                                                                        <td bgcolor="#f6fef0" class="zixe4" width="90">
                                                                                            <asp:Label ID="lblAmount" runat="server" Text=' <%#Eval("Amount")%>'></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                                
                                                                            </ItemTemplate>                    
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>    
                                                                                <table width="24" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                    <tr>
                                                                                        <td width="24" align="center" bgcolor="#ecebeb" class="zixegreen2">
                                                                                            &nbsp;
                                                                                        </td>                                                                                        
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <table align="center" bgcolor="#f6fef0"  class="zixe4">
                                                                                    <tr>
                                                                                        <td align="center" bgcolor="#f6fef0"  class="zixe4">
                                                                                            <%--<img src="images/cross symbol.png" width="14" height="14" />
                                                                                            <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick ="return ConfirmDelete();" OnClick="lbtnDelete_Click"></asp:LinkButton>--%>
                                                                                            <asp:ImageButton ID="lbtnDelete" runat="server" ImageUrl="MasterPageFiles/cross symbol.png" OnClientClick ="return ConfirmDelete();" OnClick="lbtnDelete_Click"></asp:ImageButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>                                                                              
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>                
                                                                </asp:GridView>   
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td height="25" colspan="6" align="right">
                                                                            <strong>Total :</strong>
                                                                        </td>
                                                                        <td align="center" class="orange2">
                                                                            <strong><asp:Label ID="lblSubTotal" runat="server" Text="0.00"></asp:Label></strong>
                                                                        </td>
                                                                        <td align="center">
                                                                            &nbsp;
                                                                        </td>       
                                                                    </tr>
                                                                </table>
                                                            </td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td height="25" colspan="6" align="right">
                                                                            <strong>Delivery Charges :</strong>
                                                                        </td>
                                                                        <td align="center" class="orange2">
                                                                            <strong><asp:Label ID="lblDCharges" runat="server" Text="0.00"></asp:Label></strong>
                                                                        </td>
                                                                        <td align="center">
                                                                            &nbsp;
                                                                        </td>       
                                                                    </tr>
                                                                </table>
                                                            </td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <table>
                                                                    <tr>
                                                                        <td height="25" colspan="6" align="right">
                                                                            <strong>Grand Total :</strong>
                                                                        </td>
                                                                        <td align="center" class="orange2">
                                                                            <strong><asp:Label ID="lblTotal" runat="server" Text="0.00"></asp:Label></strong>
                                                                        </td>
                                                                        <td align="center">
                                                                            &nbsp;
                                                                        </td>       
                                                                    </tr>
                                                                </table>
                                                            </td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td height="7" align="left" valign="top">
                                                                <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="106">
                                                                            <a href="#">
                                                                                <%--<img src="MasterPageFiles/bottom-left.jpg" width="106" height="32" border="0" />--%>
                                                                                <asp:ImageButton id="ibtnContinue" runat="server" width="106" height="32" 
                                                                                border="0" ImageUrl="MasterPageFiles/bottom-left.jpg" 
                                                                                onclick="ibtnContinue_Click"></asp:ImageButton>
                                                                            </a>
                                                                        </td>
                                                                        <td background="MasterPageFiles/bottom-2-bg2.jpg">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="7" align="right">
                                                                            <img src="MasterPageFiles/normal-bottom-box-right.jpg" width="7" height="32" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                </tr>                                                                                        
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="720" border="0" align="left" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="285" height="30" align="center">
                                        <table width="720" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="59">
                                                    <img src="MasterPageFiles/top-left.jpg" width="59" height="42" />
                                                </td>
                                                <td align="center" background="MasterPageFiles/top--bg.jpg" class="addtocart_text">
                                                   <b><h5>Select Shipping Adress</h5></b>
                                                </td>
                                                <td width="68" align="right">
                                                    <img src="MasterPageFiles/top-right.jpg" width="68" height="42" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" align="left" valign="middle" bgcolor="#f3f3f3">
                                        <%--<input type="radio" name="radio" id="radio3" value="radio" />--%>
                                        <asp:RadioButton ID="rAddress" runat="server" Text="" />
                                        <strong>Address</strong>
                                        <%--<a href="#" class="samrtprize"> 
                                            <strong class="samrtprize5"><asp:LinkButton ID="lbtnAdd" runat="server"
                                            Text="" onclick="lbtnAdd_Click"></asp:LinkButton>
                                            (Add New)</strong>
                                        </a>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <table width="720" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <table width="720" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="8" height="32">
                                                                <img src="MasterPageFiles/normal-top-box-left.jpg" width="8" height="32" />
                                                            </td>
                                                            <td align="center" background="MasterPageFiles/normal-top-box-bg.jpg">
                                                                &nbsp;
                                                            </td>
                                                            <td width="8" align="right">
                                                                <img src="MasterPageFiles/normal-top-box-right.jpg" width="8" height="32" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" class="border-left-right-bg">
                                                    <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="700" align="center">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" id="tdAddress" runat="server"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="7" align="right" valign="top">
                                                    <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="7">
                                                                <img src="MasterPageFiles/normal-bottom-box-left.jpg" width="7" height="32" />
                                                            </td>
                                                            <td height="32" background="MasterPageFiles/normal-bottom-box-bg.jpg">
                                                                &nbsp;
                                                            </td>
                                                            <td width="7" align="right">
                                                                <img src="MasterPageFiles/normal-bottom-box-right.jpg" width="7" height="32" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="720" border="0" cellspacing="0" cellpadding="0" align="left">
                                <tr>
                                    <td >
                                        <table width="720" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="59">
                                                    <img src="MasterPageFiles/top-left.jpg" width="59" height="42" />
                                                </td>
                                                <td align="center" background="MasterPageFiles/top--bg.jpg"  class="addtocart_text">
                                                    <h5>Payment Mode</h5>
                                                </td>
                                                <td width="68" align="right">
                                                    <img src="MasterPageFiles/top-right.jpg" width="68" height="42" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" class="border-left-right-bg">
                                        <table width="720" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="205">
                                                    <table width="200" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td height="30" align="center">
                                                                <%--<input type="radio" name="radio" id="radio" value="radio" />--%>
                                                                <asp:RadioButton ID="rcdCard" runat="server" Text="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <img src="MasterPageFiles/credit or debit.jpg" width="156" height="49" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20" align="center">
                                                    &nbsp;
                                                </td>
                                                <td width="20" align="center">
                                                    <table width="200" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td height="30" align="center">
                                                                <%--<input type="radio" name="radio" id="radio2" value="radio" />--%>
                                                                <asp:RadioButton ID="rOnline" runat="server" Text="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <img src="MasterPageFiles/internet banking.jpg" width="156" height="49" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20" align="center">
                                                    &nbsp;
                                                </td>
                                                <td width="475">
                                                    <table width="200" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td height="30" align="center">
                                                                <%--<input type="radio" name="radio" id="radio4" value="radio" />--%>
                                                                <asp:RadioButton ID="rCDelivery" runat="server" Text="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <img src="MasterPageFiles/cash on delivery.jpg" width="156" height="48" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="7" align="right" valign="top">
                                        <table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="7">
                                                    <img src="MasterPageFiles/normal-bottom-box-left.jpg" width="7" height="32" />
                                                </td>
                                                <td height="32" background="MasterPageFiles/normal-bottom-box-bg.jpg">
                                                    &nbsp;
                                                </td>
                                                <td width="7" align="right">
                                                    <img src="MasterPageFiles/normal-bottom-box-right.jpg" width="7" height="32" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>                    
                        <%--<td align="center" valign="top">
                            &nbsp;
                        </td>--%>
                        <td height="30" align="center" valign="middle">
                            <%--<input type="submit" name="button2" id="button2" value="Submit " />--%>
                            <asp:Button ID="btnConfirm" runat="server" Text="Submit" 
                             OnClientClick="return Validation();" onclick="btnConfirm_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            &nbsp;
                        </td>
                    </tr>                                
                </table>                                        
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>