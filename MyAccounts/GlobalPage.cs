﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
namespace MyAccounts20
{
    public class GlobalPage : System.Web.UI.Page
    {
 
        
        Login_BLL bllLogin = new Login_BLL();
        protected override void OnLoad(EventArgs e)
        {

            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx", true);
            }
            string[] strArray = Request.Url.AbsolutePath.Split('/');
            string strActPage;
            strActPage = strArray[strArray.Length - 1];
            if (strActPage.IndexOf('?') > 0)
                strActPage = strActPage.Substring(0, strActPage.IndexOf('?'));
            string strPage;
            strPage = strActPage.ToUpper();

            if (!((strPage == ("ReportWeeklyComm.aspx").ToUpper() || strPage == ("UpdateProfile.aspx").ToUpper() || strPage == ("ReportMyAccount.aspx").ToUpper() || strPage == ("ReportMyTeam.aspx").ToUpper()
                  || strPage == ("ChangePwd.aspx").ToUpper() || strPage == ("MyPins.aspx ").ToUpper() || strPage == ("QuickJoin.aspx").ToUpper() 
                  || strPage==("ReportIncentives.aspx")) && Session["Type"].ToString() == "Customer"))
            {
                string res = bllLogin.CheckAuthorization(Session["UserID"].ToString(), strActPage.ToLower());
                if (res != "1")
                {
                    Response.Redirect("Login.aspx", true);
                }
            }
            Response.Expires = -1500;

            Response.AddHeader("Pragma", "no-cache");
            Response.AddHeader("cache-control", "private");

            Response.CacheControl = "no-cache";

            Response.Cache.SetExpires(DateTime.Now.Date.AddDays(-100));

            Response.Cache.SetNoServerCaching();

            Response.ExpiresAbsolute = DateTime.Now.AddDays(-100);

            Response.Cache.SetNoStore();

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.Cache.SetValidUntilExpires(false);


            base.OnLoad(e);
        }
    }
}
