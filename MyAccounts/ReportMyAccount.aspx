﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="ReportMyAccount.aspx.cs" Inherits="MyAccounts20.ReportMyAccount" Title="My Account"
    EnableEventValidation="false" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">        
    var preid;
    var ddlMember;
    var dtpFrom;
    var dtpTo;
    var chkPay;
    var spnPay;
    var dtpPayDate;
    var btnPay;
    var btnPrintCus;
    var tblAc;
    var hidUserID;
    var lblCusName;
    var trMember;
    var Member;
    var tdMember;
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
    preid ='ctl00_ContentPlaceHolder1_'    
    function GetControls()
    {
        ddlMember=document.getElementById(preid+"ddlMember");  
        dtpFrom=document.getElementById(preid+"txtFromDate");
        dtpTo=document.getElementById(preid+"txtToDate");              
        chkPay=document.getElementById(preid+"chkPay");
        spnPay =document.getElementById("spnPayDate");
        dtpPayDate=document.getElementById(preid+"txtPayDate");
        btnPay=document.getElementById(preid+"btnPay");
        btnPrintCus=document.getElementById(preid+"btnPrintCustomerSlip");
        hidUserID =document.getElementById("hidUserID");
        lblCusName = document.getElementById(preid+"lblCusName");
        trMember=document.getElementById(preid+"trMember");
        tdMember=document.getElementById("tdName");
        Member="0";      
            
        //member=
        //tblAc = document.getElementById("tblAc");
    }
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }     
    function SetData()
    {    
        GetControls();              
        //dtpFrom.value = DateTime.Today.AddDays(-(DateTime.Now.DayOfWeek - System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)).ToString("dd-MM-yyyy");
        //dtpTo.value=DateTime.Today.AddDays(6 - (DateTime.Now.DayOfWeek - System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)).ToString("dd-MM-yyyy");
        dtpFrom.value = new Date().format("dd-MM-yyyy");
        dtpTo.value=new Date().format("dd-MM-yyyy");  
        dtpPayDate.value = new Date().format("dd-MM-yyyy");
        if(tdMember!=null)
            tdMember.focus();                
        else 
            dtpFrom.focus();
    }
    var bPay;
    function btnShow_Click()
    {       
//        if(ddlMember != null)
//            Member=ddlMember.value
//        else    
//            Member="0";
        if(selectedLedger  != "")
            Member=selectedLedger;
        else    
            Member="0";
        if(chkPay!=null)
        {
            if(chkPay.checked)
            {
                btnPay.style.display="block";
                btnPrintCus.style.display="block";
            }
            bPay=chkPay.checked;
        }
        else
        {
            bPay=false;
        }
        window.frames["frmMemberAc"].location.href="Reports/MyAccount.aspx?LedgerID="+Member+"&FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&Pay="+bPay;
    }          
    function btnPrintCustomerSlip_Click()
    {
        try
        {
            window.open("PrintCustomerSlip.aspx?FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&PayDate="+dtpPayDate.value);        
        }
        catch(err)
        {
        }
    }
    function chkPayClick()
    {
        if (chkPay.checked)
        {
            spnPay.style.display= "block";
            dtpPayDate.style.display="block";
            //btnPay.style.display="block";
            //btnPay.visible=true;
            //document.getElementById['btnPay'].style.visibility = 'hidden';
        }
        else
        {
            spnPay.style.display= "none";
            dtpPayDate.style.display="none";
            btnPay.style.display="none";
            btnPrintCus.style.display="none";
           // btnPay.visible=false;
         }         
    }
    function GetCustomerName()
    {
        if (selectedLedger!="")
            PageMethods.GetCustomerName(selectedLedger,GetCustomerNameComplete);
    }
    function GetCustomerNameComplete(res)
    {
        lblCusName.innerText=res;
    }
    var divLedger;
    var selectedLedger="";
    var prevText="";

    function FillLedger(e,t)
    {
        if(prevText != t.value)
        {
            prevText=t.value;
            //alert(e.keyCode);
            selectedLedger="";
            divLedger=document.getElementById("divLedger");
            if(t.value!="")
            {        
                PageMethods.CallFillMember(t.value,FillLedgerComplete)
            }
            else{
            divLedger.innerHTML="";
            divLedger.style.display="none";
            }
        }
    }

    function FillLedgerComplete(res)
    {
        try
        {        
            getXml(res);
            var tbl="<table style='width:100%'>";
            var count=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<count; i++)
            {
                var lid;
                var lname;
                var YourID;
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                        lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
                
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                        lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                        
                //lname=lname+" ("+lid+"-";
                lname=lname+" (";
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                        //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                        YourID =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                lname=lname+")";
                if(i==0)
                {
                    tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+YourID+"</td></tr>";
                    selectedLedger=lid;
                }
                else
                {
                    //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                    tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+YourID+"</td></tr>";
                    
                }
            }
            tbl=tbl+"</table>";
            divLedger.innerHTML=tbl;
            if(count>0)
                divLedger.style.display="block";
            else
                divLedger.style.display="none";
        }
        catch(err)
        {
        }   
        
    }
    function OnSelectClick(t)
    {
        selectedLedger=t.cells[0].innerHTML;
        var txt=document.getElementById("tdName");
        ApplyLedger(txt);
    }
    function OnMouseOver(t)
    {
        var tbl=t.parentNode;
        var cnt=0;
        selectedLedger ="";
        for(var i=0;i<tbl.rows.length;i++)
        {
            tbl.rows[i].className='unselectedrow';   
        }
        t.className='selectedrow';
        selectedLedger=t.cells[0].innerHTML;
    }
    function selectLedger(e,t)
    {
        if(e.keyCode==9||e.keyCode==13)
        {
            ApplyLedger(t);
        }
        else
        {
            if(divLedger==null)
                divLedger=document.getElementById("divLedger");
            var tbl=divLedger.firstChild;    
            var currow=0;
            if(tbl!=null)
            {
                for(var i=0;i<tbl.rows.length;i++)
                {
                    if(tbl.rows[i].cells[0].innerHTML==selectedLedger)
                    {
                        currow=i;
                    }
                }
                if(e.keyCode==38)
                {

                    if(tbl.rows[currow-1]!=null)
                    {
                        tbl.rows[currow-1].className='selectedrow';
                        selectedLedger=tbl.rows[currow-1].cells[0].innerHTML;         
                        tbl.rows[currow].className='unselectedrow';         
                    }
                }
                else if(e.keyCode==40)
                {
                    if(tbl.rows[currow+1]!=null)
                    {
                        tbl.rows[currow+1].className='selectedrow';
                        selectedLedger=tbl.rows[currow+1].cells[0].innerHTML;         
                        tbl.rows[currow].className='unselectedrow';         
                    }
                }
            }
        }
    }
    function ApplyLedger(t)
    {
        if(divLedger==null)
                divLedger=document.getElementById("divLedger");
        if(selectedLedger!="")
        {   
            var tbl=divLedger.firstChild;
            var cnt=0;
            if(tbl!="" && tbl!=null)
            { 
                for(var i=0;i<tbl.rows.length;i++)
                {
                    if(tbl.rows[i].cells[0].innerHTML==selectedLedger)
                    {
                        t.value=tbl.rows[i].cells[1].innerHTML;
                        cnt++;
                        break;
                    }
                }
                if(cnt==0)
                {
                    selectedLedger="";
                    t.value="";
                }
            }        
        }
        else
            t.value="";
        prevText=t.value;
        divLedger.style.display="none";    
    }
    </script>

    <style type="text/css">
        .style1
        {
            width: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border: solid 1px black;">
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;" valign="top"
                    align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr id="trMember" runat="server">
                                        <td align="right" colspan="3">
                                            Member
                                        </td>
                                        <td align="left">
                                            <%--<asp:DropDownList ID="ddlMember" Width="100px" runat="server" onchange="GetCustomerName();">
                                            </asp:DropDownList>--%>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr style="display: none;">
                                                    <td>
                                                        <select style="width: 100px;" id="ddlMember" onchange="GetCustomerName();">
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 300px;">
                                                        <input type="text" style="width: 100px;" id="tdName" onkeydown="selectLedger(event,this);"
                                                            onkeyup="FillLedger(event,this);" onblur="ApplyLedger(this);GetCustomerName();" />
                                                        <asp:Label ID="lblCusName" Text="" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr style="position: relative;">
                                                    <td align="left">
                                                        <div style="display: none; width: 100px;" class="divautocomplete" id="divLedger">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <span id="spnFromDate">From :</span>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtFromDate" MaxLength="10" runat="server" onfocus="showcalendar(this);"
                                                            onkeydown="HideCalendar(event);" onclick="showcalendar(this);" Style="width: 100px;"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <span id="spnToDate">To :</span>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtToDate" MaxLength="10" runat="server" onfocus="showcalendar(this);"
                                                            onkeydown="HideCalendar(event);" onclick="showcalendar(this);" Style="width: 100px;" />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkPay" Text="Pay" runat="server" onclick="chkPayClick();" />
                                                    </td>
                                                    <td>
                                                        <span id="spnPayDate" style="display: none;">Pay Date :</span>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtPayDate" runat="server" MaxLength="10" Style="display: none;
                                                            width: 100px;" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);"
                                                            onclick="showcalendar(this);" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnPay" Text="Pay" runat="server" Style="display: none;" OnClick="btnPay_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnPrintCustomerSlip" Text="Print Customer Slip" runat="server" Style="display: none;"
                                                            OnClientClick="btnPrintCustomerSlip_Click();return false;" />
                                                    </td>
                                                    <td>
                                                        <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td colspan="10">
                                            <iframe name="frmMemberAc" id="frmMemberAc" style="width: 100%; height: 400px;" frameborder="0"
                                                scrolling="auto"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
        <input type="hidden" runat="server" id="hidUserID" />
        <input type="hidden" runat="server" id="hidXmlString" />
        <%-- <input type="hidden" runat="server" id="hidType" />--%>
    </div>
</asp:Content>
