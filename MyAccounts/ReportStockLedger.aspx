﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportStockLedger.aspx.cs" Inherits="MyAccounts20.ReportStockLedger" Title="Stock Ledger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Calendar.css" rel="stylesheet" type="text/css" />
    <script src="JS/CalendarPopup.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">         
        document.write(getCalendarStyles());        
        var cal=new CalendarPopup("divCalendar");        
        cal.showNavigationDropdowns();
        function showcalendar(t)
        {
            cal.select(t,t.id,'dd-MM-yyyy');
        }
    var ddlBranch;
    var ddlHG;
    var ddlGroup;
    var ddlItem;
    var ddlBatch;
    var dtpFrom;
    var dtpTo;    
    var dtFormat='dd-MM-yyyy';
    var UserID;
    var preid;    
    var hidGBID;
    var tdItem;
    var lblItem;
    preid ='ctl00_ContentPlaceHolder1_'
    function GetControls()
    {
        ddlBranch=document.getElementById("ddlBranch");
        //ddlHG = document.getElementById("ddlHeadGroup");    
        ddlGroup = document.getElementById("ddlStockGroup");    
        ddlItem=document.getElementById("ddlItem");
        ddlBatch=document.getElementById("ddlBatch");
        dtpFrom=document.getElementById(preid+"txtFromDate");
        dtpTo = document.getElementById(preid+"txtToDate");
        UserID = document.getElementById(preid+"hidUserID");
        tdItem=document.getElementById("tdItem");
        lblItem=document.getElementById(preid+"lblItem");
    }       
 
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function GetBranch()
    {        
        PageMethods.GetBranch(UserID.value, GetBranchComplete);    
    }

    function GetBranchComplete(res)
    {
        getXml(res);    
        ddlBranch.options.length=0;
        var opt=document.createElement("OPTION");
        if(xmlObj.getElementsByTagName("Table").length != 1)
        {
            opt=document.createElement("OPTION");
            //opt.text="Select";
            opt.text="All";
            opt.value="0";    
            ddlBranch.options.add(opt);
            ddlBranch.value="0";
        }
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            ddlBranch.options.add(opt);
        }            
        if(hidGBID==null)
            GetDefaultBranch();
        ddlBranch.value=hidGBID.value;
    }
    function GetGroups()
    {
        if(UserID==null)
           GetUserID(); 
        PageMethods.GetStockGroup(GetGroupsComplete);    
    }

    function GetGroupsComplete(res)
    {
        getXml(res);    
        ddlGroup.options.length=0;
        var opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlGroup.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupID")[0].firstChild.nodeValue;
            ddlGroup.options.add(opt);
        }    
    }
//    function GetHeadGroups()
//    {
//        if(UserID==null)
//           GetUserID(); 
//        PageMethods.GetHeadGroup(GetHeadGroupsComplete);    
//    }

//    function GetHeadGroupsComplete(res)
//    {
//        getXml(res);    
//        ddlHG.options.length=0;
//        var opt=document.createElement("OPTION");
//        opt.text="Select";
//        opt.value="0";    
//        ddlHG.options.add(opt);
//        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//        {
//            opt=document.createElement("OPTION");
//            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupName")[0].firstChild.nodeValue;
//            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("HeadGroupID")[0].firstChild.nodeValue;
//            ddlHG.options.add(opt);
//        }    
//    }
    function GetStockItem()
    {
        if(UserID==null)
           GetUserID(); 
        //PageMethods.GetStockItem(ddlGroup.value, GetStockItemComplete);    
    }

    function GetStockItemComplete(res)
    {
        getXml(res);    
        ddlItem.options.length=0;
        var opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlItem.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
            ddlItem.options.add(opt);
        }    
    }
    function GetDefaultBranch()
    {
        hidGBID=document.getElementById(preid+"hidGBID");
    }

    function GetBatch()
    {
        if(UserID==null)
           GetUserID(); 
        PageMethods.GetBatchNo(selectedItem,ddlBranch.value, GetBatchNoComplete);    
    }

    function GetBatchNoComplete(res)
    {
        getXml(res);    
        ddlBatch.options.length=0;
        var opt=document.createElement("OPTION");
        //opt.text="Select";
        opt.text="All";
        opt.value="0";    
        ddlBatch.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BatchNo")[0].firstChild.nodeValue;
            ddlBatch.options.add(opt);
        }    
    }
    function GetItemName()
    {
       PageMethods.GetItemName(selectedItem,CompleteItemName);  
    }
    function CompleteItemName(res)
    {
        try 
        {
            getXml(res);       
            lblItem=document.getElementById(preid+"lblItem");     
            if(xmlObj.getElementsByTagName("Table").length==0)
            {          
                lblItem.innerText="";                
                return;            
            }
            else
            {             
                lblItem.innerText =xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
            }      
        }
        catch(err)
        {
        }
    }

//    function GetDefaults()
//    {
//        dtpFrom.value=new Date().format("dd-MM-yyyy");
//        dtpTo.value=new Date().format("dd-MM-yyyy");            
//    }
    function SetData()
    {
        GetControls();
        GetBranch();
        //GetHeadGroups();
        GetGroups();
        GetStockItem();
        //GetDefaults();
        ddlBranch.focus();        
    }
    var divItem;
    var selectedItem="";
    var prevText="";

    function FillItem(e,t)
    {
        if(prevText != t.value)
        {
            prevText=t.value;
            //alert(e.keyCode);
            selectedItem="";
            divItem=document.getElementById("divItem");
            if(t.value!="")
            {        
                PageMethods.CallFillItem(t.value,FillItemComplete)
            }
            else{
            divItem.innerHTML="";
            divItem.style.display="none";
            }
        }
    }

    function FillItemComplete(res)
    {
        try
        {
            
            getXml(res);
            var tbl="<table style='width:100%'>";
            var count=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<count; i++)
            {
                var ItemID;
                var ItemName;
                var ItemCode;
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild!=null)
                        ItemID=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ICID")[0].firstChild.nodeValue;
                
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild!=null)
                        ItemName=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockItemName")[0].firstChild.nodeValue;
                        
                //lname=lname+" ("+lid+"-";
                //lname=lname+" (";
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild!=null)
                        //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                        ItemCode =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ItemCode")[0].firstChild.nodeValue;
                //lname=lname+")";
                if(i==0)
                {
                    tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                    selectedItem=ItemID;
                }
                else
                {
                    //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                    tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+ItemID+"</td><td>"+ItemCode+'-'+ItemName+"</td></tr>";
                    
                }
            }
            tbl=tbl+"</table>";
            divItem.innerHTML=tbl;
            if(count>0)
                divItem.style.display="block";
            else
                divItem.style.display="none";
        }
        catch(err)
        {
        }   
        
    }
    function OnSelectClick(t)
    {
        selectedItem=t.cells[0].innerHTML;
        var txt=document.getElementById("tdItem");
        ApplyItem(txt);
    }
    function OnMouseOver(t)
    {
        var tbl=t.parentNode;
        var cnt=0;
        selectedItem ="";
        for(var i=0;i<tbl.rows.length;i++)
        {
            tbl.rows[i].className='unselectedrow';   
        }
        t.className='selectedrow';
        selectedItem=t.cells[0].innerHTML;
    }
    function selectItem(e,t)
    {
        if(e.keyCode==9||e.keyCode==13)
        {
            ApplyItem(t);
        }
        else
        {
            if(divItem==null)
                divItem=document.getElementById("divItem");
            var tbl=divItem.firstChild;    
            var currow=0;
            if(tbl!=null)
            {
                for(var i=0;i<tbl.rows.length;i++)
                {
                    if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                    {
                        currow=i;
                    }
                }
                if(e.keyCode==38)
                {

                    if(tbl.rows[currow-1]!=null)
                    {
                        tbl.rows[currow-1].className='selectedrow';
                        selectedItem=tbl.rows[currow-1].cells[0].innerHTML;         
                        tbl.rows[currow].className='unselectedrow';         
                    }
                }
                else if(e.keyCode==40)
                {
                    if(tbl.rows[currow+1]!=null)
                    {
                        tbl.rows[currow+1].className='selectedrow';
                        selectedItem=tbl.rows[currow+1].cells[0].innerHTML;         
                        tbl.rows[currow].className='unselectedrow';         
                    }
                }
            }
        }
    }

    function ApplyItem(t)
    {
        if(divItem==null)
                divItem=document.getElementById("divItem");
        if(selectedItem!="")
        {   
            var tbl=divItem.firstChild;
            var cnt=0;
            if(tbl!="" && tbl!=null)
            { 
                for(var i=0;i<tbl.rows.length;i++)
                {
                    if(tbl.rows[i].cells[0].innerHTML==selectedItem)
                    {
                        t.value=tbl.rows[i].cells[1].innerHTML;
                        cnt++;
                        break;
                    }
                }
                if(cnt==0)
                {
                    selectedItem="";
                    t.value="";
                }
            }        
        }
        else
            t.value="";
        prevText=t.value;
        divItem.style.display="none";
        
    }
    function btnShow_Click()
    {        
//        if(ddlHG.value=="0")
//        {
//            alert("Select HeadGroup");
//            ddlHG.focus();
//            return false;
//        }
//        if(ddlGroup.value=="0")
//        {
//            alert("Select Stock Group");
//            ddlGroup.focus();
//            return false;
//        }
        if(selectedItem=="")
        {            
            selectedItem='0';            
        }
        window.frames["frmStockLedger"].location.href="Reports/StockLedger.aspx?Branch="+ddlBranch.value+"&FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value+"&GID="+ddlGroup.value+"&Item="+selectedItem+"&Batch="+ddlBatch.value;
        ddlBranch.focus();
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table>
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>    
                                        <td align="left">Branch</td>
                                        <td align="left">
                                            <select id="ddlBranch" style="width:170px;"></select>
                                        </td>
                                        <%--<td align="left">HeadGroup</td>
                                        <td colspan="2">
                                            <select id="ddlHeadGroup" style="width:200px;" onchange="GetGroups();"></select>
                                        </td>--%>
                                        <td align="left">StockGroup</td>
                                        <td>
                                            <select id="ddlStockGroup" style="width:170px" onchange="GetStockItem();"></select>
                                        </td>
                                        <td align="left">StockItem</td>
                                        <td>                                            
                                            <%--<select id="ddlItem" style="width: 200px;" onchange="GetBatch();"></select>--%>
                                            <table cellpadding="0" cellspacing="0">
                                            <tr style="display: none;">
                                                <td>
                                                    <select style="width: 80px;" id="ddlItem" onchange="GetBatch();">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" style="width: 300px;" id="tdItem" onkeydown="selectItem(event,this);" onkeyup="FillItem(event,this);"  onblur="ApplyItem(this);GetBatch();GetItemName();"/>                                                
                                                    
                                                                                        
                                                </td>
                                            </tr>
                                            <tr style="position: relative;">
                                                <td align="left">
                                                    <div style=" display: none; width:300px; " class="divautocomplete"
                                                        id="divItem">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>       
                                        </td>     
                                    </tr>
                                    <tr> 
                                                                      
                                        <td align="left">Batch No</td>
                                        <td align="left">
                                            <select id="ddlBatch" style="width: 90px;"></select>                                      
                                        </td>                                        
                                        <td align="left">FromDate</td>
                                        <td align="left">
                                            <input type="text" id="txtFromDate" runat="server" maxlength="10" style="width: 80px;"
                                             onfocus="showcalendar(this);" onclick="showcalendar(this);" onkeydown="cal.hideCalendar();"/>
                                             
                                        </td>
                                        <td>ToDate</td>
                                        <td align="left">
                                            <input type="text" id="txtToDate" runat="server" maxlength="10" style="width: 80px;" 
                                            onfocus="showcalendar(this);"  onclick="showcalendar(this);" onkeydown="cal.hideCalendar();"/>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                        <%--<td>
                                            
                                        </td>--%>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmStockLedger" id="frmStockLedger" style="width: 100%;
                                    height: 400px; font-size:6px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>    
    <div>
    <input type="hidden" id="hidUserID" runat="server" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
