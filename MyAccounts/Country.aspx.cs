﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class Country : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["UserID"].ToString() != "0")
                {
                    Response.Redirect("Login.aspx");
                }
                if (!IsPostBack)
                {
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
            }

        }
        void FillGrid()
        {
            try
            {
                Country_BLL obj_BAL = new Country_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetCountrys();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvCountrys.DataSource = ds;
                        gvCountrys.DataBind();
                        //gvCountrys.Rows[0].Cells[2].Text = "";
                    }
                    else
                    {
                        gvCountrys.DataSource = ds;
                        gvCountrys.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }


        public static string GetGridData()
        {
            StringWriter sw = new StringWriter();
            try
            {
                Country_BLL obj_BAL = new Country_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetCountrys();
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "Countrys";
                ds.WriteXml(sw);
            }
            catch (Exception ex)
            {
            }
            return sw.ToString();
        }


        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string FillCountrys()
        {
            return GetGridData();
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnSave_Click(string CountryName)
        {
            string rtval = "";
            if (string.IsNullOrEmpty(CountryName.Trim()))
            {
                rtval = "-2";
                return rtval;
            }
            Country_BLL obj_BAL = new Country_BLL();
            string res = obj_BAL.InsertCountry(CountryName);
            if (res == "1")
                rtval = GetGridData();
            else
                rtval = res;
            return rtval;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnUpdate_Click(int Countryid, string CountryName)
        {
            string rtnval = "";
            if (string.IsNullOrEmpty(CountryName.Trim()))
            {
                rtnval = "-2";
                return rtnval;
            }
            Country_BLL obj_BAL = new Country_BLL();
            string result = obj_BAL.UpdateCountrys(Countryid, CountryName);
            if (result == "1")
                rtnval = GetGridData();
            else
                rtnval = result;
            return rtnval;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string DeleteClick(int Countryid)
        {
            string rtn = "";
            Country_BLL obj_BAL = new Country_BLL();
            string res = obj_BAL.DeleteCountry(Countryid);
            if (res == "1")
                rtn = GetGridData();
            else
                rtn = res;
            return rtn;
        }

    }
}
