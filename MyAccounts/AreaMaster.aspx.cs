﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;


namespace MyAccounts20
{
    public partial class AreaMaster : GlobalPage
    {
        Area_BLL Obj_BLL = new Area_BLL();
        //public static string strID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            string strID = Request.QueryString["ID"];
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
                GetDefaults();
                if (strID != null && strID != "")
                {                 
                    tdUpdate.Visible = false;                 
                    tdCancel.Visible = false;                    
                    strID = "";
                }
            }              

        }


        private void FillGrid()
        {
            try
            {
                Area_BLL Obj_BLL = new Area_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetArea(txtSearch.Text);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvArea.DataSource = ds;
                        gvArea.DataBind();
                        gvArea.Rows[0].Cells[5].Text = "";
                        //gvArea.Rows[0].Cells[2].="";
                    }
                    else
                    {
                        gvArea.DataSource = ds;
                        gvArea.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        

        private void GetDefaults()
        {
            Area_BLL Obj_BLL = new Area_BLL();
            ddlZone.DataSource = Obj_BLL.GetZone();
            ddlZone.DataTextField = "ZoneName";
            ddlZone.DataValueField = "ZoneID";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("Select", "0"));
            ddlBranch.DataSource = Obj_BLL.GetBranch();
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "BranchID";
            ddlBranch.DataBind();
            ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertArea(txtAreaCode.Text, txtAreaCentre.Text, txtAddress.Text, ddlZone.SelectedValue, txtContactNo.Text,ddlBranch.SelectedValue,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Area";
                

            }
            else if (res == "-1")
            {
                lblMsg.Text = "Area already existed";
                
            }
            else if (res == "1")
            {
                lblMsg.Text = "Area saved successfully";
                FillGrid();
                txtAreaCode.Text = "";
                txtAreaCentre.Text = "";
                txtAddress.Text = "";
                ddlZone.SelectedIndex = 0;
                txtContactNo.Text = "";
                ddlBranch.SelectedIndex = 0;
                txtSearch.Text = "";
                txtAreaCode.Focus();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateArea(hidAreaID.Value,txtAreaCode.Text,txtAreaCentre.Text,txtAddress.Text,ddlZone.SelectedValue,txtContactNo.Text,ddlBranch.SelectedValue,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Area";

            }
            else if (res == "-1")
            {
                lblMsg.Text = "Area already existed";
                
            }
            else if (res == "1")
            {
                lblMsg.Text = "Area Updated successfully";
                FillGrid();
                txtAreaCode.Text = "";
                txtAreaCentre.Text = "";
                txtAddress.Text = "";
                ddlZone.SelectedIndex = 0;
                txtContactNo.Text = "";
                ddlBranch.SelectedIndex = 0;
                txtSearch.Text = "";
                btnUpdate.Visible = false;
                btnSave.Visible = true; 
                txtAreaCode.Focus();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvArea.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtAreaCode.Text = "";
            txtAreaCentre.Text = "";
            txtAddress.Text = "";
            ddlZone.SelectedIndex = 0;
            txtContactNo.Text = "";
            ddlBranch.SelectedIndex = 0;
            lblMsg.Text = "";
            txtSearch.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidAreaID.Value = ((HtmlInputHidden)grow.FindControl("hidAreaID")).Value;
                txtAreaCode.Text = ((Label)grow.FindControl("lblAreaCode")).Text;
                txtAreaCentre.Text = ((Label)grow.FindControl("lblAreaCentre")).Text;
                txtAddress.Text = ((Label)grow.FindControl("lblAddress")).Text;
                ddlZone.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidZoneID")).Value;
                txtContactNo.Text = ((Label)grow.FindControl("lblContactNo")).Text;
                if (((HtmlInputHidden)grow.FindControl("hidBranchID")).Value != "0")
                    ddlBranch.SelectedValue = ((HtmlInputHidden)grow.FindControl("hidBranchID")).Value;                
                txtAreaCode.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
          public void lbtnDelete_Click(object sender, EventArgs e)
            {
                try
                {
                    LinkButton lbtn = (LinkButton)sender;
                    GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                    hidAreaID.Value = ((HtmlInputHidden)grow.FindControl("hidAreaID")).Value;
                    Area_BLL Obj_BLL = new Area_BLL();
                    string res = Obj_BLL.DeleteArea(hidAreaID.Value);
                    if (res == "" || res == "0")
                    {
                        lblMsg.Text = "Error While Deleting Area";
                        //return;

                    }
                    if (res == "1")
                    {
                        lblMsg.Text = "Area Deleted Successfully";
                        txtAreaCode.Text = "";
                        txtAreaCentre.Text = "";
                        txtAddress.Text = "";
                        ddlZone.SelectedIndex = 0;
                        txtContactNo.Text = "";
                        ddlBranch.SelectedIndex = 0;
                        txtAreaCode.Focus();
                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                        btnCancel.Visible = true;
                        FillGrid();

                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
          protected void gvArea_PageIndexChanging(object sender, GridViewPageEventArgs e)
          {
              gvArea.PageIndex= e.NewPageIndex;
              FillGrid();
          }

          protected void txtSearch_TextChanged(object sender, EventArgs e)
          {
              FillGrid();
          }
        }
    }

    
        
        

       
    
