﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopingCart.aspx.cs" MasterPageFile="~/UserMaster.Master"
    Inherits="MyAccounts20.ShopingCart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript" language="javascript">
    function vv()
    {
        alert("hai");
    }
     </script>
    <script src="JS/ShoppingCart.js" type="text/javascript"></script>
    <script src="JS/Commonfunctions.js" type="text/javascript"></script>    
    <link href="MyAccounts20.css" rel="stylesheet" type="text/css" />
    <%--OnClick="btnAddtoCart_Click" --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
       <%-- <asp:UpdatePanel runat="server">
            <ContentTemplate>--%>
                <table align="center" style="width: 700px; font-size: small;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="700" valign="top">
                            <table width="700" border="0" align="left" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="400" align="left" valign="top">
                                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="400">
                                                    <asp:DataList ID="dlItemDet" runat="server" Width="400px" CellSpacing="0" 
                                                        CellPadding="0" >
                                                        <ItemTemplate>
                                                            <table width="430" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="440">
                                                                        <table width="425" border="1" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                                                            <tr>
                                                                                <td width="91" rowspan="2" align="center" bgcolor="#F6FEF0" class="zixe">
                                                                                    <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode") %>'></asp:Label>                                                                                    
                                                                                </td>
                                                                                <td width="367" align="left" bgcolor="#F6FEF0" class="zixeleftpadding">                                                                                    
                                                                                    <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("Description") %>'></asp:Label>                                                                                    
                                                                                </td>                                                                                
                                                                            </tr>
                                                                            <tr align="center">
                                                                                <td bgcolor="#F6F7FC" class="2border">                                                                                                                                                                        
                                                                                    <img src="TeluguImages/<%#Eval("SiNo")%>.jpg" width="90" height="20" alt="" />
                                                                                </td>
                                                                                <td style="display:none;">
                                                                                    <input type="hidden" runat="server" id="hidItemID" value='<% #Eval("SiNo") %>'/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="425" border="1" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                                                            <tr>
                                                                                <td width="100" bgcolor="#FFFFFF">
                                                                                    <table width="100" border="0" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img src="ItemImages/<%#Eval("SiNo")%>.jpg" width="98" height="98" alt="" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td align="left" valign="middle" bgcolor="#F6FEF0">
                                                                                    <table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                        <tr style="display:none;">
                                                                                            <td align="right" class="red" colspan="2" style="font-weight: bold;">
                                                                                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                                                                            </td>
                                                                                            <td height="25" bgcolor="#F6FEF0" class="zixegreen">
                                                                                                <input type="hidden" id="hidMRP" runat="server" value='<%#Eval("MRP") %>'/>
                                                                                            </td>
                                                                                            <td height="25" bgcolor="#F6FEF0" class="zixegreen">
                                                                                                <input type="hidden" id="hidPrice" runat="server" value='<%#Eval("osp") %>'/>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" rowspan="3" valign="middle" bgcolor="#F6FEF0">
                                                                                                <table width="210" border="0" cellpadding="0" cellspacing="0" bgcolor="#F6FEF0">
                                                                                                    <tr>
                                                                                                        <td style="height: 25px; width: 120px;" class="zixe">
                                                                                                            Market price / M.R.P
                                                                                                        </td>
                                                                                                        <td class="zixe">
                                                                                                            :
                                                                                                        </td>
                                                                                                        <td style="height: 33px;" align="center" class="red">
                                                                                                            <strong><%--<span class="WebRupee">Rs</span>--%>
                                                                                                                <span>
                                                                                                                    <img src="MasterPageFiles/finalred.jpg" />
                                                                                                                </span>
                                                                                                                <asp:Label ID="lblMRP" runat="server" Text='<%#Eval("MRP") %>'></asp:Label></strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="height: 15px;" class="zixe">
                                                                                                            Our Price
                                                                                                        </td>
                                                                                                        <td class="zixe">
                                                                                                            :
                                                                                                        </td>
                                                                                                        <td style="height: 33px;" align="center">
                                                                                                            <span class="green"><strong>
                                                                                                            <%--<span class="WebRupee"><strong>Rs</strong></span>--%>
                                                                                                            <span>
                                                                                                                <img src="MasterPageFiles/finalgreen.jpg" />
                                                                                                            </span>
                                                                                                            <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("osp") %>'></asp:Label></strong></span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="height: 25px;" class="zixe">
                                                                                                            Availability
                                                                                                        </td>
                                                                                                        <td align="center" class="zixe">
                                                                                                            :
                                                                                                        </td>
                                                                                                        <td style="height: 33px;" align="center">
                                                                                                            <asp:Image ID="imgNA" runat="server" ImageUrl="MasterPageFiles/cross symbol.png" />                                                                                                            
                                                                                                            <asp:CheckBox ID="chkAvailable" runat="server"/>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="11" height="30" bgcolor="#F6FEF0" class="zixegreen">
                                                                                                <input type="hidden" id="hidItemName" runat="server" value='<%#Eval("Description")%>'/>
                                                                                            </td>
                                                                                            <td width="129" height="20" align="center" valign="bottom" bgcolor="#F6FEF0" class="black">
                                                                                                <strong>Quantity</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>                                                                                            
                                                                                            <td height="25" bgcolor="#F6FEF0" class="zixegreen">
                                                                                                <%--<asp:Image ID="imgA" runat="server" ImageUrl="MasterPageFiles/correct%20button.jpg" />--%>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td height="25" align="left" valign="middle" bgcolor="#F6FEF0">                                                                                                
                                                                                                <table align="center">
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                        <div>
                                                                                                            <input type="image" id="imgA" runat="server" class="hidbutton" src="MasterPageFiles/correct%20button.jpg" />      
                                                                                                            </div>
                                                                                                        </td>
                                                                                                        <td valign="middle">
                                                                                                            <input type="text" id="txtQty" runat="server" value="" maxlength="3" style="text-align: center; width:40px;" onkeydown="AddItems(event,this);" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>                                                                                                
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>                                                                                            
                                                                                            <td height="30" bgcolor="#F6FEF0" class="zixegreen">
                                                                                                <input type="hidden" id="hidID" runat="server" value='<% #Eval("SiNo") %>' />
                                                                                            </td>
                                                                                            <td height="20" align="center" valign="top" bgcolor="#F6FEF0">
                                                                                                <a href="#">                                                                                                    
                                                                                                    <%--<asp:imagebutton id="btnaddtocart" imageurl="masterpagefiles/cart.jpg" runat="server"
                                                                                                        onclick="validation(this)"/>--%>
                                                                                                        <%--onclientclick="return validation(this);" --%>
                                                                                                    <%-- <a href="#">
                                                                                                        <img src="MasterPageFiles/cart.jpg" width="80" height="26" onclick ="Validation(this)" runat="server" id="btnAddtoCart" />
                                                                                                     </a>--%>
                                                                                                     <%--<a href="#">--%>
                                                                                                        <img src="MasterPageFiles/cart.jpg" width="80" height="26" alt="" border="0" onclick ="Validation(this);" runat="server" id="btnAddtoCart"/>
                                                                                                    <%--</a>--%>                                                                                                    
                                                                                                </a>
                                                                                            </td>
                                                                                            <td style="display:none;">
                                                                                                <input type="hidden" id="hidBatchno" runat="server" value='<%#Eval("BatchNo")%>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <%--<tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>--%>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </td>
                                                <td width="290" valign="top">
                                                    <table width="290" border="0" align="right" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="280" align="left">
                                                                <table width="290" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="300" border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td width="59">
                                                                                        <img src="MasterPageFiles/top-left.jpg" width="59" height="42" />
                                                                                    </td>
                                                                                    <td background="MasterPageFiles/top--bg.jpg">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td width="68" align="right">
                                                                                        <img src="MasterPageFiles/top-right.jpg" width="68" height="42" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" class="border-left-right-bg">                                                                           
                                                                            <table width="295" border="0" align="center" cellpadding="0" cellspacing="2">
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <div>
                                                                                            <table width="295" border="0" align="center" cellpadding="0" cellspacing="2" id="gvShopingCart" runat="server">
                                                                                                <tr bgcolor="#ecebeb" class="zixegreen2">
                                                                                                    <th style="display:none;">
                                                                                                        ItemID
                                                                                                    </th>
                                                                                                    <th width="121" align="center">
                                                                                                        Item Name
                                                                                                    </th>
                                                                                                    <th style="display:none;">
                                                                                                        MRP
                                                                                                    </th>
                                                                                                    <th width="46" align="center">
                                                                                                        Price
                                                                                                    </th>
                                                                                                    <th width="33" align="center">
                                                                                                        Qty
                                                                                                    </th>
                                                                                                    <th height="25" align="center">
                                                                                                        Amount
                                                                                                    </th>
                                                                                                    <th height="25" align="center">
                                                                                                        &nbsp;
                                                                                                    </th>  
                                                                                                    <th style="display:none;">
                                                                                                        BatchNo
                                                                                                    </th>         
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>                                                                                    
                                                                                </tr>                                                                                                                                         
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td height="25" align="right">
                                                                                                    <strong>Total :</strong>
                                                                                                </td>
                                                                                                <td height="25" align="left" class="orange2">
                                                                                                    <strong><asp:Label ID="lblTotal" runat="server" Width="70px" Text="0.00"></asp:Label></strong>
                                                                                                </td>
                                                                                                <td height="25" align="left" class="orange2">
                                                                                                    &nbsp;
                                                                                                </td>       
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td height="25" align="right">
                                                                                                    <strong>Delivery Charges :</strong>
                                                                                                </td>
                                                                                                <td height="25" align="left" class="orange2">
                                                                                                    <strong><asp:Label ID="lblDCharges" runat="server" Width="70px" Text="0.00"></asp:Label></strong>
                                                                                                </td>
                                                                                                <td height="25" align="left" class="orange2">
                                                                                                    &nbsp;
                                                                                                </td>            
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>    
                                                                                </tr>
                                                                                <tr>
                                                                                     <td align="right">
                                                                                        <table>
                                                                                            <tr>    
                                                                                                <td height="25" align="right">
                                                                                                    <strong>Grand Total :</strong>
                                                                                                </td>
                                                                                                <td height="25" align="left" class="orange2">
                                                                                                    <strong><asp:Label ID="lblGrand" runat="server" Width="70px" Text="0.00"></asp:Label></strong>
                                                                                                </td>
                                                                                                <td height="25" align="left" class="orange2">
                                                                                                    &nbsp;
                                                                                                </td>       
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>      
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <table width="290" border="0" align="left" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td background="MasterPageFiles/bottom-2-bg.jpg">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td width="107" align="right">
                                                                                        <%--<a href="#">
                                                                                            <img src="MasterPageFiles/bottom-rigth.jpg" width="107" height="32" border="0" /></a>--%>
                                                                                        <a href="#">                                                                                                        
                                                                                            <asp:ImageButton ID="btnCheckOut" runat="server" width="107" height="32" ImageUrl="MasterPageFiles/bottom-rigth.jpg" 
                                                                                             onclick="btnCheckOut_Click" OnClientClick="return CheckOutValidation();"></asp:ImageButton>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <img src="MasterPageFiles/left-box2.jpg" width="232" height="288" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
          <%--  </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
    <div>
        <input type ="hidden" id="hidUserID" runat="server" />
        <input type ="hidden" id="hidTempID" runat="server" />
    </div>
</asp:Content>
