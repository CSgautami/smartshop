﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;

namespace MyAccounts20
{
    public partial class UpdateProfile : GlobalPage 
    {
        RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();                
                GetDefaults();
                GetData();
            }        
        }
        void GetData()
        {
            DataSet ds=new DataSet();
            ds=Obj_BLL.GetUserData(hidUserID.Value);
            txtCustomer.Text = ds.Tables[0].Rows[0]["CustomerID"].ToString();
            txtName.Text = ds.Tables[0].Rows[0]["RegisName"].ToString();
            txtHNO.Text = ds.Tables[0].Rows[0]["HNO"].ToString();
            txtSOne.Text = ds.Tables[0].Rows[0]["StreetName1"].ToString();
            txtSTwo.Text = ds.Tables[0].Rows[0]["StreetName2"].ToString();
            ddlArea.SelectedValue = ds.Tables[0].Rows[0]["AreaID"].ToString();
            ddlCity.SelectedValue = ds.Tables[0].Rows[0]["CityID"].ToString();
            ddlState.SelectedValue = ds.Tables[0].Rows[0]["StateID"].ToString();
            txtLMark.Text = ds.Tables[0].Rows[0]["LandMark"].ToString();
            txtPHNO.Text = ds.Tables[0].Rows[0]["PhoneNo"].ToString();
            txtMobile.Text = ds.Tables[0].Rows[0]["Mobile"].ToString();
            txtEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
           // txtZone.Text = ds.Tables[0].Rows[0]["Zone"].ToString();

        }
        private void GetDefaults()
        {
           
            
            ddlArea.DataSource = Obj_BLL.GetArea();
            ddlArea.DataTextField = "AreaCentre";
            ddlArea.DataValueField = "AreaID";
            ddlArea.DataBind();

            ddlCity.DataSource = Obj_BLL.GetCity();
            ddlCity.DataTextField = "CityName";
            ddlCity.DataValueField = "CityID";
            ddlCity.DataBind();
            
            ddlState.DataSource = Obj_BLL.GetState();
            ddlState.DataTextField = "StateName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();

            ddlArea.Items.Insert(0, new ListItem("Select", "0"));
            ddlCity.Items.Insert(0, new ListItem("Select", "0"));
            ddlState.Items.Insert(0, new ListItem("Select", "0"));
        
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ddlArea.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select Area";
                ddlArea.Focus();
                return;
            }
            if (ddlCity.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select City";
                ddlCity.Focus();
                return;
            }
            if (ddlState.SelectedIndex <= 0)
            {
                lblMsg.Text = "Please Select State";
                ddlState.Focus();
                return;
            }
            string res = Obj_BLL.UpdateProfile(hidUserID.Value,txtName.Text, txtHNO.Text, txtSOne.Text, txtSTwo.Text, ddlArea.SelectedValue, ddlCity.SelectedValue, ddlState.SelectedValue,txtLMark.Text,txtPHNO.Text,txtMobile.Text,txtEmail.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Address";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Address already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "Address Updated successfully";
                txtCustomer.Text = "";
                txtName.Text = "";
                txtHNO.Text = "";
                txtSOne.Text = "";
                txtSTwo.Text = "";
                ddlArea.SelectedValue = "0";
                ddlCity.SelectedValue = "0";
                ddlState.SelectedValue = "0";
                txtLMark.Text = "";
                txtPHNO.Text = "";
                txtMobile.Text = "";
                txtEmail.Text = "";
                txtZone.Text = "";
                txtName.Focus();

            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAreaWiseZone(string AreaID)
        {
            try
            {
                RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
                return Obj_BLL.GetAreaWiseZone(AreaID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
