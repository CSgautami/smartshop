﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.Collections.Generic;
namespace MyAccounts20
{
    public partial class DefaultBranchSelection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");                    
                }

            }
             catch (Exception ex)
             {

             }
             if (!IsPostBack)
             {
                 GetBranchs();                  
             }
        }        
        private void GetBranchs()
        {
            Purchase_BLL Obj_BLL = new Purchase_BLL();
            ddlGBranch.DataSource = Obj_BLL.GetBranch(Session["UserID"].ToString());
            ddlGBranch.DataTextField = "BranchName";
            ddlGBranch.DataValueField = "BranchID";
            ddlGBranch.DataBind();
            if (ddlGBranch.Items.Count != 1)
            { 
                ddlGBranch.Items.Insert(0, new ListItem("Select", "0"));
            }            
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            Session["GBID"] = ddlGBranch.SelectedIndex;
            Response.Redirect("Home.aspx");
        }
    }
}
