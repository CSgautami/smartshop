﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using BLL;
namespace MyAccounts20
{
    public partial class ConfirmPage : System.Web.UI.Page
    {
        ShopingCart_BLL obj_BLL = new ShopingCart_BLL();
        string TempOrderID;
        DataTable dt;
        int count = 0;
        string OrderID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session[MyAccountsSession.TempOrderID]!= null)
                TempOrderID = Session[MyAccountsSession.TempOrderID].ToString();
            if (!IsPostBack)
            {                
                GetProductDetails(TempOrderID);
                GetTotals();
                GetAddress();                                
            }
        }
        void GetProductDetails(string TempOrderID)
        {
            dt = obj_BLL.GetProductDetails(TempOrderID);
            gvShopingCart.DataSource = dt;
            gvShopingCart.DataBind();
        }
        void GetAddress()
        {
            DataSet set = new DataSet();
            set = obj_BLL.GetAddress(Session[MyAccountsSession.UserID].ToString());
            if(set.Tables.Count>0)
                tdAddress.InnerHtml= obj_BLL.GetAddress(Session[MyAccountsSession.UserID].ToString()).Tables[0].Rows[0]["ShippingAddress"].ToString();            
              
        }
        decimal SubTotal;
        void GetTotals()
        {
            DataTable dt2 = new DataTable();
            dt2 = obj_BLL.GetProductDetails(TempOrderID);            
            foreach (DataRow dr in dt2.Rows)
            {
                SubTotal = SubTotal + Convert.ToDecimal(dt2.Rows[count]["Amount"]);
                count++;
            }
            if(SubTotal<200)
                lblDCharges.Text = "10.00";
            else
                lblDCharges.Text = "0.00";
            lblSubTotal.Text = SubTotal.ToString("0.00");            
            lblTotal.Text = (Convert.ToDecimal(lblSubTotal.Text) + Convert.ToDecimal(lblDCharges.Text)).ToString();
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
            int i, TODID;
            TODID = Convert.ToInt32(((HtmlInputHidden)grow.FindControl("hidTODID")).Value);
            string res = obj_BLL.DeleteTempOrder(TODID);
            GetProductDetails(TempOrderID);
            //return res;            
        }
        void UpdateTempOrder(DataTable tdt, string TempOrderID)
        {
            DataSet set = new DataSet();
            set.Tables.Add(tdt);
            set.DataSetName = "XML";
            set.Tables[0].TableName = "ShopingCart";
           // obj_BLL.SaveTempOrder(set.GetXml(), TempOrderID, Session[MyAccountsSession.UserID].ToString());
        }        
        protected void txtQty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int ItemID=Convert.ToInt32(((HtmlInputHidden)((TextBox)sender).FindControl("hidItemID")).Value);
                int Qty=Convert.ToInt32(((TextBox)sender).Text);
                decimal Price = Convert.ToDecimal(((Label)((TextBox)sender).FindControl("lblPrice")).Text);
                obj_BLL.UpdateTempOrderQtyChange(TempOrderID, ItemID.ToString(), Qty.ToString(),Price.ToString());
                if (Qty > 0)
                {
                    DataTable dt1 = new DataTable();
                    dt1 = obj_BLL.GetProductDetails(TempOrderID);                    
                    gvShopingCart.DataSource = dt1;
                    gvShopingCart.DataBind();
                    GetTotals();
                    //DataRow[] drow = dt1.Select("ItemID=" + ItemID);
                    //int tempQty = Convert.ToInt32(((System.Data.DataRow)(drow.GetValue(0))).ItemArray[1]);
                    //foreach (DataRow dr in dt1.Rows)
                    //{
                    //    if (dr["ItemID"].ToString() == ItemID.ToString())
                    //    {
                    //        dt1.Rows[count]["Qty"] = Convert.ToInt32(Qty);
                    //        dt1.Rows[count]["Amount"] = Convert.ToInt32(Qty) * Convert.ToInt32(dt1.Rows[count]["Price"]);
                    //        dt1.AcceptChanges();
                    //        UpdateTempOrder(dt1, TempOrderID);
                    //        gvShopingCart.DataSource = dt1;
                    //        gvShopingCart.DataBind();
                    //    }
                    //    count++;
                    //}
                }
                else
                {
                    Response.Write("<script type='text/javascript'>alert('0 Is Not Valid Quantity');</script>");
                    ((TextBox)sender).Focus();
                }
            }
            catch
            {
                throw;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (TempOrderID != "")
            {
                string ModeOfPayment="",PaymentStatus="",Transport="";
                //Address = lblAddress.Text + " ," + System.Environment.NewLine + lblVillage.Text + " , " + System.Environment.NewLine + lblCity.Text + " , "  + System.Environment.NewLine + lblPinCode.Text;
                //Transport = txtTransport.Text;
                if (rcdCard.Checked == true)
                {
                    ModeOfPayment = "Credit Card Or Debit Card";
                    PaymentStatus = "Paid";
                }
                else if (rOnline.Checked == true)
                {
                    ModeOfPayment = "Online Payment";
                    PaymentStatus = "Paid";
                }
                else if (rCDelivery.Checked == true)
                {
                    ModeOfPayment = "Cash On Delivery";
                    PaymentStatus = "Due";
                }
                string res= obj_BLL.SaveOrder(TempOrderID,lblSubTotal.Text,lblDCharges.Text,tdAddress.InnerText, Transport,ModeOfPayment,PaymentStatus);
                if (res != "" && int.Parse(res) > 0)
                {
                    Session[MyAccountsSession.TempOrderID] = null;
                    Session[MyAccountsSession.UserCart] = null;
                    Response.Redirect("Invoice.aspx?OrderID=" + res);
                }
            }
        }

        //protected void lbtnAdd_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("Registraton.aspx?UserID=" + Session[MyAccountsSession.UserID].ToString());
        //}

        protected void ibtnContinue_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("listing.aspx", false);
        }
    }
}
