﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportUserPriceList.aspx.cs" Inherits="MyAccounts20.ReportUserPriceList" Title="User PriceList Report" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        var preid;
        var GroupID;
        var Search;
        var WoMargin;
        preid ='ctl00_ContentPlaceHolder1_'
        var xmlObj;
        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }
            
        }
        function GetCntrl()
        {
            GroupID=document.getElementById(preid+"ddlGroup");
            Search=document.getElementById(preid+"txtSearchItem");
            WoMargin=document.getElementById(preid+"chkWOMargin");
        }
        function GetGroups()
        {             
            PageMethods.GetStockGroup(GetGroupsComplete);    
        }

        function GetGroupsComplete(res)
        {
            getXml(res);    
            GroupID.options.length=0;
            var opt=document.createElement("OPTION");
            //opt.text="Select";
            opt.text="All";
            opt.value="0";    
            GroupID.options.add(opt);
            for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
            {
                opt=document.createElement("OPTION");
                opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupName")[0].firstChild.nodeValue;
                opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("StockGroupID")[0].firstChild.nodeValue;
                GroupID.options.add(opt);
            }    
        }
        function SetData()
        {
            GetCntrl();         
            GetGroups();
            GroupID.focus();
        }
         function btnShow_Click()
        {         
            window.frames["frmUserPriceList"].location.href="Reports/UserPriceListReport.aspx?GID="+GroupID.value+"&SItem="+Search.value+"&StockItemsOnly="+WoMargin.checked;
            GroupID.focus();
        }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 800px; border:solid 1px black;">
            <tr>
                <td>Group</td>
                <td>
                    <asp:DropDownList ID="ddlGroup" runat="server" Width="200px"></asp:DropDownList>
                </td>
                <td>SearchItem</td>
                <td>
                    <asp:TextBox ID="txtSearchItem" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:CheckBox ID="chkWOMargin" Text="Stock Items Only" runat="server" />
                </td>
                <td>
                    <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                </td>
            </tr>
            <tr>                                        
                <td colspan="6">                                            
                    <iframe name="frmUserPriceList" id="frmUserPriceList" style="width: 800px; height: 400px;"
                        frameborder="0" scrolling="auto"></iframe>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
