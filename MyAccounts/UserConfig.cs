﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace MyAccounts20
{
    public static class UserConfig
    {
        public static string UserName = ConfigurationManager.AppSettings["AdminUserName"].ToString();
        public static string Password = ConfigurationManager.AppSettings["AdminPassword"].ToString();
    }
}
//    public string strUserID;
//        protected override void OnLoad(EventArgs e)
//        {
//            if (Session[SalwarSession.UserID] == null)
//            {
//                Response.Redirect("Login.aspx");
//                Response.End();
//            }
//            strUserID = Session[SalwarSession.UserID].ToString();
//            base.OnLoad(e);
//        }

//        public void FillHeading(string Heading)
//        {
//            try
//            {
//                Title = Heading;
//                if (Master != null)
//                {
//                    Label lblHeading = (Label)Master.FindControl("lblHeading");
//                    if (lblHeading != null)
//                        lblHeading.Text = Heading;
//                }
//            }
//            catch
//            {
//                throw;
//            }
//        }
//}
