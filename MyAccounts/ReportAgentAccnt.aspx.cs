﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL.Reports;
using BLL;


namespace MyAccounts20
{
    public partial class ReportAgentAccnt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
            // hidUserID.Value = Session["UserID"].ToString();
            if (!IsPostBack)
            {
                DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
                txtFromDate.Value = nFirstDate.ToString("dd-MM-yyyy");
                txtToDate.Value = nLastDate.ToString("dd-MM-yyyy");
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetAgent()
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetAgentNames().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCustomer(string AgentID)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetAgentWiseCustomer(AgentID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

