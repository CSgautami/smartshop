﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;

namespace MyAccounts20
{
    public partial class ReportFinalAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
                Response.Redirect("Login.aspx");
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                //hidGBID.Value = Session["GBID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
                DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
                txtDate.Text = nFirstDate.ToString("dd-MM-yyyy");
                txtTo.Text = nLastDate.ToString("dd-MM-yyyy");
            }
        }
    }
}

        
    
