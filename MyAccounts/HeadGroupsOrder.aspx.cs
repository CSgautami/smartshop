﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
namespace MyAccounts20
{
    public partial class HeadGroupsOrder : System.Web.UI.Page
    {
        GroupsOrder_BLL obj_Bll = new GroupsOrder_BLL();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetHeadGroups();                
            }
        }
        private void GetHeadGroups()
        {
            gvHeadGroup.DataSource = obj_Bll.GetHeadGroupList().Tables[0];
            gvHeadGroup.DataBind();
        }
        private void GetColsForTable()
        {
            dt.Columns.Add("HGNo");
            dt.Columns.Add("OrderID");
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            GetColsForTable();
            int HGNo, OrderID;
            string strRes;
            strRes = "";
            foreach (GridViewRow grow in gvHeadGroup.Rows)
            {
                HGNo = Convert.ToInt32(((HtmlInputHidden)grow.FindControl("hidHGNo")).Value);
                OrderID = 0;
                if (((TextBox)grow.FindControl("txtOrder")).Text.Trim() != "")
                    OrderID = Convert.ToInt32(((TextBox)grow.FindControl("txtOrder")).Text.Trim());
                //if (OrderID == "") OrderID = 0;
                if (OrderID > 0) dt.Rows.Add(HGNo, OrderID);
            }
            if (dt.Rows.Count > 0)
            {
                //dt.TableName = "GroupsOrder";
                DataSet ds = new DataSet();
                ds.DataSetName = "XML";
                ds.Tables.Add(dt);
                ds.Tables[0].TableName = "HeadGroupsOrder";
                strRes = obj_Bll.SaveHeadGroupsOrder(ds.GetXml());
            }
            lblMsg.Visible = true;
            if (strRes == "" || strRes == "0")
                lblMsg.Text = "Error In Saving";
            else
                lblMsg.Text = "Successfully Saved";
        }
    }
}
