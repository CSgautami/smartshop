﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="PriceDetails.aspx.cs" Inherits="MyAccounts20.PriceDetails" Title="Price Details" EnableEventValidation="false"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/PriceDetails.js" type="text/javascript"></script>

    <script src="JS/ShowForms.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    function SetData()
    {
        GetControls();
        btnCancel_Click();
        GetStockItem();
//        GetData();
    }
    function InsertItem()
    {
        ItemClick();
        GetStockItem();
    }              
    function ItemKeydown(e,t)
    {
        if(e.keyCode==45)
        {
            InsertItem();
        }
    }   
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
            <tr>
                <td>&nbsp</td>
            </tr>
            <tr>
                <td align="left">Search Item</td>
                <td align="left" >
                    <%--<asp:DropDownList runat="server" ID="ddlCompany" Width="300px"></asp:DropDownList>--%>
                    <asp:TextBox ID="txtSearch" runat="server" Width="100px" onfocus ="Cleartable();"></asp:TextBox>
                </td>
                <td> Group:</td>
                <td align="left">                    
                    <asp:DropDownList ID="ddlStockGroup" runat="server" Width="100px" onchange="Cleartable();" ></asp:DropDownList>
                 </td>
                 <%--<td>
                    Company:
                 </td>
                 <td align="left">
                    <asp:DropDownList ID="ddlCompany" runat="server" Width="100px" onchange="Cleartable();"></asp:DropDownList>
                 </td>--%>
                <td align="center">                    
                    <input type="button" id="btnShow" value="Show" onclick="GetData();" />                    
                </td>
            </tr> 
            <tr>
                <td style="height:10px">
                </td>                
            </tr>      
            <tr>
                <td colspan="8" align="left">
                    <div class="divgrid" style="height: 230px; border: solid 1px #98bf21;">
                        <table id="tblGrid">
                            <tr class="dbookheader">
                                <th style ="width:30px;">
                                    Code
                                </th>
                                <th style="display: none; width: 0px;">
                                    Item ID
                                </th>
                                <th style="width: 200px;">
                                    Item Name
                                </th>
                                <th style="width: 60px;">
                                    BatchNo
                                </th>
                                <th style="width: 60px;">
                                    PurchasePrice
                                </th>
                                <th style="width: 30px;">
                                    MRP
                                </th>
                                <th style="width: 30px;">
                                    M.M
                                </th>
                                <th style="width: 60px;">
                                    Retail
                                </th>
                                <th style="width: 30px;">
                                    R.M
                                </th>
                                <th style="width: 60px;">
                                    Customer
                                </th>
                                <th style="width: 30px;">
                                    C.M
                                </th>
                                <th style="width: 30px;">
                                    VIP
                                </th>
                                <th style="width: 30px;">
                                    V.M
                                </th>
                                <th style="width: 30px;">
                                    Disc
                                </th>
                                <th style="width: 30px;">
                                    CDisc
                                </th> 
                               <%--  <th style="width: 120px;">
                                                </th>--%>                               
                            </tr>
                        </table>
                    </div>
                </td>                
            </tr>   
         <%--   <tr>
                <td colspan="8">&nbsp</td>
                <td colspan="8">&nbsp</td>
            </tr>    --%>    
            
           <tr>
           <td colspan="100%">
           <table align="center" width="100%">
           <tr>
                <td align="left">Item Code</td>                
               <td align="left">
                    <%--<asp:DropDownList ID="ddlItemCode" runat="server" Width="100px" 
                    onkeydown="ItemKeydown(event,this);"></asp:DropDownList>
                    <img id="imgItem" src="Imgs/create.gif" runat="server" onclick="InsertItem();" /> --%>
                    <%--<select id="ddlItemCode" style="width:115px;"></select>--%>                    
                    <table cellpadding="0" cellspacing="0">
                        <tr style="display: none;">
                            <td>
                                <select style="width: 100px;" id="ddlItemCode">
                                </select>
                            </td>
                        </tr>                        
                        <tr>
                            <td>
                                <input type="text" style="width: 300px;" id="tdItem" onkeydown="selectItem(event,this);" onkeyup="FillItem(event,this);"  onblur="ApplyItem(this);GetItemName();GetItemPriceDet();"/>                                                
                                <img id="imgItem" src="Imgs/create.gif" runat="server" onclick="InsertItem();" />                                
                            </td>
                        </tr>
                        <tr style="position: relative;">
                            <td>
                                <div style=" display: none; width:300px; " class="divautocomplete"
                                    id="divItem">
                                </div>
                            </td>
                        </tr>
                    </table> 
                </td>
                <td align="left">BatchNo</td>
                <td align="left">
                    <asp:TextBox ID="txtBatch" runat="server" Width="110px"></asp:TextBox> 
                </td>
                <td align="left">PurchasePrice</td> 
                <td align="left">
                     <asp:TextBox ID="txtPurchasePrice" runat="server" Width="110px" style="text-align:right"></asp:TextBox>
                </td>
                </tr>
                </table>
                </td>
           </tr>             
           <tr>
                 <td align="left">MRP</td>
                 <td align="left">
                       <asp:TextBox ID="txtMRP" runat="server" Width="110px" style="text-align:right"></asp:TextBox>
                 </td>
                 <td align="left">MRPMargin</td>
                 <td align="left">
                      <asp:TextBox ID="txtMRPMargin" runat="server" Width="110px" style="text-align:right"></asp:TextBox>
                 </td>
                <td align="left">Retail/Price1</td>
                <td align="left">
                    <asp:TextBox ID="txtRetail" runat="server" Width="110px" style="text-align:right"></asp:TextBox> 
                </td>                 
                <td align="left">RetailMargin</td>
                <td align="left">
                    <asp:TextBox ID="txtRetailMargin" runat="server" Width="110px" style="text-align:right"></asp:TextBox> 
                </td> 
                
           </tr>           
           <tr>
                <td align="left">CustomerPrice</td>
                <td align="left">
                    <asp:TextBox ID="txtCustomer" runat="server" Width="110px" style="text-align:right"></asp:TextBox>
                </td>
                <td align="left">CustomerMargin</td>
                <td align="left">
                    <asp:TextBox ID="txtCustomerMargin" runat="server" Width="110px" style="text-align:right"></asp:TextBox>
                </td>
                <td align="left">VIP/Price2</td>
                <td align="left">
                    <asp:TextBox ID="txtVIP" runat="server" Width="110px" style="text-align:right"></asp:TextBox> 
                </td>                 
                <td align="left">VIPMargin</td>
                <td align="left">
                    <asp:TextBox ID="txtVIPMargin" runat="server" Width="110px" style="text-align:right"></asp:TextBox> 
                </td>
            </tr>
            <tr>  
                <td align="left">Discount</td>
                <td align="left">
                    <asp:TextBox ID="txtDiscount" runat="server" Width="110px" style="text-align:right"></asp:TextBox> 
                </td> 
                <td align="left">C Discount</td>
                <td align="left">
                    <asp:TextBox ID="txtCashDiscount" runat="server" Width="110px" style="text-align:right"></asp:TextBox> 
                </td>               
            </tr>
            <tr>
                <td style="height:10px">
                </td>                
            </tr>
            <tr>
                <td align="center" colspan="8">                    
                    <input type="button" id="btnSave" value="Save" onclick="btnSave_Click(event,this);" />
                    <input type="button" id="btnCancel" runat="server" value="Cancel" onclick="btnCancel_Click();" />                    
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>

