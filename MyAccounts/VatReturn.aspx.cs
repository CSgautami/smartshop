﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;

namespace MyAccounts20
{
    public partial class VatReturn : System.Web.UI.Page
    {
        VatReturn_BLL Obj_BLL = new VatReturn_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {

                
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                hidVRID.Value = "-1";
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
                Purchase_BLL obj_BLL = new Purchase_BLL();               
                ddlBranch.DataSource = obj_BLL.GetBranch(hidUserID.Value);
                ddlBranch.DataValueField = "BranchID";
                ddlBranch.DataTextField = "BranchName";
                ddlBranch.DataBind();
                if (ddlBranch.Items.Count != 1)
                {
                    ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
                }
                txtVRSrNo.Text="";
                btnCancel_Click(sender, e);
                GetData();
            }

        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        public void GetData()
        {
            DataSet ds = Obj_BLL.GetVatReturn(ddlBranch.SelectedValue.ToString(), dateFormat(txtVRDtpFrom.Text), dateFormat(txtVRDTPTo.Text), chkVRRefresh.Checked.ToString());
            #region Input
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtExAmt.Text = ds.Tables[0].Rows[0]["PExempValue"].ToString();
                //txt6B.Text = ds.Tables[0].Rows[0][""].ToString();
                txt4PurAmt.Text = ds.Tables[0].Rows[0]["PVAT4AMT"].ToString();
                txt4PurTax.Text = ds.Tables[0].Rows[0]["PVAT4"].ToString();
                txt5PurAmt.Text = ds.Tables[0].Rows[0]["PVat5Amt"].ToString();
                txt5PurTax.Text = ds.Tables[0].Rows[0]["PVat5"].ToString();
                txt125PurAmt.Text = ds.Tables[0].Rows[0]["PVATSTDAMT"].ToString();
                txt125PurTax.Text = ds.Tables[0].Rows[0]["PVATSTD"].ToString();
                txt145PurAmt.Text = ds.Tables[0].Rows[0]["PVat14P5Amt"].ToString();
                txt145PurTax.Text = ds.Tables[0].Rows[0]["PVat14P5"].ToString();
                txt1PurAmt.Text = ds.Tables[0].Rows[0]["PVAT1AMT"].ToString();
                txt1PurTax.Text = ds.Tables[0].Rows[0]["PVAT1"].ToString();
                txtSplPurAmt.Text = ds.Tables[0].Rows[0]["PVATSATAMT"].ToString();
                txtSplPurTax.Text = ds.Tables[0].Rows[0]["PVATSAT"].ToString();
                txtTotIpTaxAmt.Text = ds.Tables[0].Rows[0]["PTotAmt"].ToString();
                txtTotIpTax.Text = ds.Tables[0].Rows[0]["PTotTax"].ToString();
            }
            #endregion
            #region Output
            if (ds.Tables[1].Rows.Count > 0)
            {
                txtExSaleAmt.Text = ds.Tables[1].Rows[0]["SExempted"].ToString();
                txtSaleExpoAmt.Text = ds.Tables[1].Rows[0]["SExports"].ToString();
                //txtSaleExpoTax.Text = ds.Tables[0].Rows[0]["SVAT4"].ToString();
                txtCSTAmt.Text = ds.Tables[1].Rows[0]["SInterState"].ToString();
                //txtCSTTax.Text = ds.Tables[0].Rows[0]["SVat5"].ToString();
                txtPurVal.Text = ds.Tables[1].Rows[0]["SPURCHASEVAL"].ToString();
                txtVatPay.Text = ds.Tables[1].Rows[0]["SVATPAYABLE"].ToString();
                txt4SaleAmt.Text = ds.Tables[1].Rows[0]["SVAT4AMT"].ToString();
                txt4SaleTax.Text = ds.Tables[1].Rows[0]["SVAT4"].ToString();
                txt5SaleAmt.Text = ds.Tables[1].Rows[0]["SVat5Amt"].ToString();
                txt5SaleTax.Text = ds.Tables[1].Rows[0]["SVat5"].ToString();
                txt125SaleAmt.Text = ds.Tables[1].Rows[0]["SVATSTDAMT"].ToString();
                txt125SaleTax.Text = ds.Tables[1].Rows[0]["SVATSTD"].ToString();
                txt145SaleAmt.Text = ds.Tables[1].Rows[0]["SVat14P5Amt"].ToString();
                txt145SaleTax.Text = ds.Tables[1].Rows[0]["SVat14P5"].ToString();
                txtSplSaleAmt.Text = ds.Tables[1].Rows[0]["SVATSATAMT"].ToString();
                txtSplSaleTax.Text = ds.Tables[1].Rows[0]["SVATSAT"].ToString();
                txt1SaleAmt.Text = ds.Tables[1].Rows[0]["SVAT1AMT"].ToString();
                txt1SaleTax.Text = ds.Tables[1].Rows[0]["SVAT1"].ToString();
                txtTotOpTaxAmt.Text = ds.Tables[1].Rows[0]["STotAmt"].ToString();
                txtTotOpTax.Text = ds.Tables[1].Rows[0]["STotTax"].ToString();
            }
            #endregion
            if (ds.Tables.Count > 2)
            {
                if (ds.Tables[2].Rows.Count > 0)
                {
                    txtVRSrNo.Text = ds.Tables[2].Rows[0]["VATReturnNo"].ToString();
                    hidVRID.Value = txtVRSrNo.Text;
                    txtVRAmtPaid.Text = ds.Tables[2].Rows[0]["TotalAmoutToPay"].ToString();
                    txtVRPaid.Text = ds.Tables[2].Rows[0]["Paid"].ToString();
                    txtVRRef.Text = ds.Tables[2].Rows[0]["Refund"].ToString();
                    txtVRCF.Text = ds.Tables[2].Rows[0]["CarredForward"].ToString();
                    txtVR24a.Text = ds.Tables[2].Rows[0]["box24a"].ToString();
                    txtVR24b.Text = ds.Tables[2].Rows[0]["box24b"].ToString();
                }
            }
            else
                txtVRAmtPaid.Text = (Convert.ToDecimal(txtTotIpTax.Text) - Convert.ToDecimal(txtTotOpTax.Text)).ToString();

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //btnCancel_Click(sender, e); 
            GetData();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            chkVRRefresh.Checked = false;
            txtVRSrNo.Text = "";
            DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
            DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
            txtVRDtpFrom.Text = nFirstDate.ToString("dd-MM-yyyy");
            txtVRDTPTo.Text = nLastDate.ToString("dd-MM-yyyy");
            ddlBranch.SelectedIndex = 0;
            foreach (HtmlTableRow tr in tblMain.Rows)
            {
                foreach (HtmlTableCell tc in tr.Cells)
                {
                    foreach (Control cntrl in tc.Controls)
                    {
                        if (cntrl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)cntrl).Text = "0";
                        }
                    }
                }
            }
            hidVRID.Value = "-1";
            foreach (HtmlTableRow tr in tblPaymets.Rows)
            {
                foreach (HtmlTableCell tc in tr.Cells)
                {
                    foreach (Control cntrl in tc.Controls)
                    {
                        if (cntrl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)cntrl).Text = "0";
                        }
                    }
                }
            }
            foreach (HtmlTableRow tr in tblAdjustments.Rows)
            {
                foreach (HtmlTableCell tc in tr.Cells)
                {
                    foreach (Control cntrl in tc.Controls)
                    {
                        if (cntrl.GetType() == typeof(TextBox))
                        {
                            ((TextBox)cntrl).Text = "";
                            DateTime nPADate = new DateTime(DateTime.Now.Year, DateTime.Now.Month,DateTime.Now.Day);
                            txtPADate.Text = nPADate.ToString("dd-MM-yyyy");
                            //txtPADate.Text = dateFormat(DateTime.Now.ToString());                            
                        }                     
                    }
                }
            }      



        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if(ddlBranch.SelectedIndex==0)
            {
                lblMsg.Text= "Please Select Branch";
                return;
            }
            string res = Obj_BLL.SaveVatReturn(hidVRID.Value, dateFormat(txtVRDtpFrom.Text), dateFormat(txtVRDTPTo.Text), txtExAmt.Text, txt4PurAmt.Text, txt4PurTax.Text,
                 txt125PurAmt.Text, txt125PurTax.Text, txt1PurAmt.Text, txt1PurTax.Text, txtSplPurAmt.Text, txtSplPurTax.Text, txtExSaleAmt.Text, txtSaleExpoAmt.Text, txtCSTAmt.Text, txtPurVal.Text, txtVatPay.Text,
                 txt4SaleAmt.Text, txt4SaleTax.Text, txt125SaleAmt.Text, txt125SaleTax.Text, txtSplSaleAmt.Text, txtSplSaleTax.Text, txt1SaleAmt.Text, txt1SaleTax.Text, txtVRAmtPaid.Text,
                 ddlBranch.SelectedValue.ToString(), txtVRPaid.Text, txtVRRef.Text, txtVRCF.Text, txtVR24a.Text, txtVR24b.Text, "0", txt145PurAmt.Text,
                 txt145PurTax.Text, txt145SaleAmt.Text, txt145SaleTax.Text, txt5PurAmt.Text, txt5PurTax.Text, txt5SaleAmt.Text, txt5SaleTax.Text);

            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving";
            }
            else if (res == "1")
            {
                lblMsg.Text = "Saved successfully";
                btnCancel_Click(sender, e);
            }
        }

        protected void txtVRDTPTo_TextChanged(object sender, EventArgs e)
        {
            btnCancel_Click(sender, e);
        }

        protected void txtVRPaid_TextChanged(object sender, EventArgs e)
        {
            txtVRCF.Text = ((Convert.ToDecimal(txtVRRef.Text) + Convert.ToDecimal(txtVRPaid.Text)) - Convert.ToDecimal(txtVRAmtPaid.Text)).ToString();
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SavePay(string SrNo, string Branch, string XmlString)
        {
            try
            {
                string res;
                VatReturn_BLL Obj_BLL = new VatReturn_BLL();
                res = Obj_BLL.SavePay(SrNo, Branch, XmlString);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveAdj(string SrNo, string Branch, string XmlString)
        {
            try
            {
                string res;
                VatReturn_BLL Obj_BLL = new VatReturn_BLL();
                res = Obj_BLL.SaveAdj(SrNo, Branch, XmlString);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
    }
}
