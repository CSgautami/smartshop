﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class PaymentBook : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
            }

        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPaymentBookData(string BranchID, string FromDate, string ToDate)
        {
            try
            {
                Payments_BLL bll_Payments = new Payments_BLL();
                if (FromDate.Trim() == "")
                {
                    FromDate = null;
                    ToDate = null;
                }
                else
                {
                    FromDate = dateFormat(FromDate);
                    if (ToDate.Trim() == "")
                        ToDate = DateTime.Now.ToString("MM-dd-yyyy");
                    else
                        ToDate = dateFormat(ToDate);
                }
                return bll_Payments.GetPaymentReport(BranchID, FromDate, ToDate).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
