﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;
using System.Text;

namespace MyAccounts20
{
    public partial class BillFormatPurchase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Purchase_BLL obj_Bll = new Purchase_BLL();
            string strInvID = Request.QueryString["InvID"];
            string BranchID = Request.QueryString["BranchID"];
            DataSet ds = new DataSet();
            if (!IsPostBack)
            {
                if (strInvID != "")
                {
                    ds = obj_Bll.GetInvoiceBillData(strInvID, BranchID);
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb1 = new StringBuilder();
                    sb.Append("<table rules='all' border='0' cellspacing='0' align='center' width='100%'>");
                    if (ds.Tables.Count > 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>&nbsp;" + ds.Tables[0].Rows[0]["BranchName"].ToString() + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>&nbsp;" + ds.Tables[0].Rows[0]["BranchAddress"].ToString() + "</td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td align='center'>&nbsp;" + ds.Tables[0].Rows[0]["ContactNo"].ToString() + "</td>");
                        sb.Append("</tr>");
                        sb.Append("</table><br/>");
                        divBranchDet.InnerHtml = sb.ToString();
                    }
                    // sb.Append = "";                                       
                    if (ds.Tables.Count > 1)
                    {
                        tdBillNo.InnerText = ds.Tables[1].Rows[0]["DCNo"].ToString();
                        tdDate.InnerText = ds.Tables[1].Rows[0]["InvDate"].ToString();
                        tdCusName.InnerText = ds.Tables[1].Rows[0]["LedgerName"].ToString();
                        tdAddress.InnerText = ds.Tables[1].Rows[0]["Address"].ToString();
                        tdPhone.InnerText = ds.Tables[1].Rows[0]["Phone"].ToString();                        
                    }
                    sb1.Append("<br/><table rules='all'  rules='all' border='1' cellspacing='0' align='center' width='100%'>");
                    if (ds.Tables.Count > 2)
                    {
                        foreach (DataColumn dc in ds.Tables[2].Columns)
                            sb1.Append("<th>" + dc.ColumnName + "</th>");
                        int cellscount = ds.Tables[2].Columns.Count;
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            sb1.Append("<tr>");
                            for (int i = 0; i < cellscount; i++)
                            {
                                if (i == 0)
                                    sb1.Append("<td align='left'>&nbsp;" + dr[i].ToString() + "</td>");
                                else if (i == 1)
                                    sb1.Append("<td>&nbsp;" + dr[i].ToString() + "</td>");
                                else
                                    sb1.Append("<td align='right'>&nbsp;" + dr[i].ToString() + "</td>");
                            }
                            sb1.Append("</tr>");
                        }
                        sb1.Append("<tr>");
                        sb1.Append("<td align='left' colspan='" + (cellscount - 4).ToString() + "'><b>Total</b></td><td align='right'>&nbsp;" + ds.Tables[2].Compute("sum(Qty)", "") + " </td><td></td>&nbsp;<td></td>&nbsp;<td align='right'>&nbsp;" + ds.Tables[2].Compute("sum(Amount)", "") + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        sb1.Append("<td align='left' colspan='" + (cellscount - 1).ToString() + "'><b>Discount</b></td><td align='right'>&nbsp;" + ds.Tables[1].Compute("sum(NetDisc)", "") + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        sb1.Append("<td align='left' colspan='" + (cellscount - 1).ToString() + "'><b>Net Amount</b></td><td align='right'>&nbsp;" + ds.Tables[1].Compute("sum(NetAmount)", "") + "</td>");
                        sb1.Append("</tr>");
                        sb1.Append("<tr>");
                        NumberInWords obj = new NumberInWords();
                        sb1.Append("<td align='left' colspan='" + (cellscount).ToString() + "'><b>InWords :" + obj.NumberToWords(Convert.ToInt32(ds.Tables[1].Compute("sum(NetAmount)", ""))) + " </b></td>");
                        sb1.Append("</tr>");
                        sb1.Append("</table><br/>");
                        divItemDet.InnerHtml = sb1.ToString();
                    }
                }
            }    
        }
    }
}
