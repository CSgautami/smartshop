﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class PinIssue : System.Web.UI.Page
    {
        PinGenerate_BLL Obj_BLL = new PinGenerate_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"].ToString() == null)
            {
                Response.Redirect("Login.aspx");
            }
            hidUserID.Value = Session["UserID"].ToString();
            if (!IsPostBack)
            {
                ddlCustomer.Focus();
                GetDefaults();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "Clearfields();", true);
            }

        }

        private void GetDefaults()
        {
            ddlCustomer.DataSource = Obj_BLL.GetCustomer();
            ddlCustomer.DataTextField = "YourID";
            ddlCustomer.DataValueField = "LedgerID";
            ddlCustomer.DataBind();
            ddlCustomer.Items.Insert(0, new ListItem("Select", "0"));
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPins()
        {
            try
            {
                PinGenerate_BLL Obj_BLL = new PinGenerate_BLL();
                return Obj_BLL.GetPins().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SavePins(string LedgerID, string xmlDet, string UserID)
        {
            try
            {
                string res;
                PinGenerate_BLL Obj_BLL = new PinGenerate_BLL();
                res = Obj_BLL.SavePins(LedgerID, xmlDet,UserID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetCustomerName(string LedgerID)
        {
            string res = "";
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.GetLedgerDetails(LedgerID).Tables[0].Rows[0]["LedgerName"].ToString();
            }
            catch (Exception ex)
            {
                res = "";
                throw;
            }
            return res;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetPinValue(string PinID)
        {
            string res = "";
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                res = obj_BLL.GetPinValue(PinID).Tables[0].Rows[0]["PinValue"].ToString();
            }
            catch (Exception ex)
            {
                res = "";
                throw;
            }
            return res;
        }
    }
}
