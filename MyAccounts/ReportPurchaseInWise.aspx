﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportPurchaseInWise.aspx.cs" Inherits="MyAccounts20.ReportPurchaseInWise" Title="Purchase Invoice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="JS/Reports/PurchaseReports.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
     function btnShow_Click()
    {
        preid='ctl00_ContentPlaceHolder1_';        
        var fromdt=document.getElementById(preid+"txtFromDate").value;
        var todt=document.getElementById(preid+"txtToDate").value;
        var FromInv=document.getElementById("txtFromInv").value;
        var ToInv=document.getElementById("txtToInv").value;
        var Branch=document.getElementById("ddlBranch").value;
        window.frames["frmPurchaseReportInvWise"].location.href="Reports/PurchaseReportInvWise.aspx?FromDate="+fromdt+"&ToDate="+todt+"&FromInv="+FromInv+"&ToInv="+ToInv+"&Branch="+Branch  
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">         
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>    
                                        <td align="left">Branch</td>
                                        <td colspan="3" align="left">
                                            <select id="ddlBranch" style="width:300px;"></select>
                                        </td>
                                        <td>
                                            InvNo From:
                                        </td>
                                        <td>
                                            <input type="text" id="txtFromInv" maxlength="10" style="width:100px;"/>
                                        </td>
                                        <td>
                                            InvNo To:
                                        </td>
                                        <td>
                                            <input type="text" id="txtToInv" maxlength="10" style="width:100px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" runat="server" id="txtFromDate" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>
                                        <td>
                                            To Date:
                                        </td>
                                        <td align="left">
                                            <input type="text" runat="server" id="txtToDate" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>                                                                              
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmPurchaseReportInvWise" id="frmPurchaseReportInvWise"
                                    style="width: 100%; height: 400px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" runat="server" id="hidUserID" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
