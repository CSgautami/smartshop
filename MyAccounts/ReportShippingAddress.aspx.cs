﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class ReportShippingAddress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();  
            }
        }

        private void FillGrid()
        {
            try
            {
                ShippingAddress_BLL Obj_BLL=new ShippingAddress_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetShippingAddress(hidUserID.Value);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvAddress.DataSource = ds;
                        gvAddress.DataBind();
                        gvAddress.Rows[0].Cells[5].Text = "";
                        
                    }
                    else
                    {
                        gvAddress.DataSource = ds;
                        gvAddress.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
