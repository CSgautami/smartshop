﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="ReportPinIssue.aspx.cs" Inherits="MyAccounts20.ReportPinIssue" Title="Report PinIssue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript">
    var ddlMember;
    var lblCusName;
    var tdMember;
    var preid ='ctl00_ContentPlaceHolder1_';
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }    
    function GetControls()
    {
        ddlMember=document.getElementById(preid+"ddlMember");  
        lblCusName = document.getElementById(preid+"lblCusName");
        tdMember=document.getElementById("tdName");
        tdMember.focus();
    }
    function GetCustomerName()
    {      
        if(selectedLedger!="")
            PageMethods.GetCustomerName(selectedLedger,GetCustomerNameComplete);
    }
    function GetCustomerNameComplete(res)
    {        
        lblCusName.innerText=res;
    }
    function btnShow_Click()
    {     
        window.frames["frmPinIssue"].location.href="Reports/PinIssueReport.aspx?LedgerID="+selectedLedger;
    }     
    var divLedger;
    var selectedLedger="";
    var prevText="";

    function FillLedger(e,t)
    {
        if(prevText != t.value)
        {
            prevText=t.value;
            //alert(e.keyCode);
            selectedLedger="";
            divLedger=document.getElementById("divLedger");
            if(t.value!="")
            {        
                PageMethods.CallFillMember(t.value,FillLedgerComplete)
            }
            else{
            divLedger.innerHTML="";
            divLedger.style.display="none";
            }
        }
    }
    function FillLedgerComplete(res)
    {
        try
        {        
            getXml(res);
            var tbl="<table style='width:100%'>";
            var count=xmlObj.getElementsByTagName("Table").length;
            for(var i=0;i<count; i++)
            {
                var lid;
                var lname;
                var YourID;
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild!=null)
                        lid=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerID")[0].firstChild.nodeValue;
                
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild!=null)
                        lname=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("LedgerName")[0].firstChild.nodeValue;
                        
                //lname=lname+" ("+lid+"-";
                lname=lname+" (";
                if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0]!=null)
                    if(xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild!=null)
                        //lname=lname+xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                        YourID =xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("YourID")[0].firstChild.nodeValue;
                lname=lname+")";
                if(i==0)
                {
                    tbl=tbl+"<tr class='selectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+YourID+"</td></tr>";
                    selectedLedger=lid;
                }
                else
                {
                    //tbl=tbl+"<tr><td style='display:none' class='unselectedrow' onclick='OnSelectLedgerClick(this)' onmouseover='OnLedgerMouseOver(this)' onkeydown='OnLedgerKeyDown(event,this)'>"+lid+"</td><td>"+lname+"</td></tr>";
                    tbl=tbl+"<tr class='unselectedrow' onclick='OnSelectClick(this)' onmouseover='OnMouseOver(this)'><td style='display:none'>"+lid+"</td><td>"+YourID+"</td></tr>";
                    
                }
            }
            tbl=tbl+"</table>";
            divLedger.innerHTML=tbl;
            if(count>0)
                divLedger.style.display="block";
            else
                divLedger.style.display="none";
        }
        catch(err)
        {
        }   
        
    }
    function OnSelectClick(t)
    {
        selectedLedger=t.cells[0].innerHTML;
        var txt=document.getElementById("tdName");
        ApplyLedger(txt);
    }
    function OnMouseOver(t)
    {
        var tbl=t.parentNode;
        var cnt=0;
        selectedLedger ="";
        for(var i=0;i<tbl.rows.length;i++)
        {
            tbl.rows[i].className='unselectedrow';   
        }
        t.className='selectedrow';
        selectedLedger=t.cells[0].innerHTML;
    }
    function selectLedger(e,t)
    {
        if(e.keyCode==9||e.keyCode==13)
        {
            ApplyLedger(t);
        }
        else
        {
            if(divLedger==null)
                divLedger=document.getElementById("divLedger");
            var tbl=divLedger.firstChild;    
            var currow=0;
            if(tbl!=null)
            {
                for(var i=0;i<tbl.rows.length;i++)
                {
                    if(tbl.rows[i].cells[0].innerHTML==selectedLedger)
                    {
                        currow=i;
                    }
                }
                if(e.keyCode==38)
                {

                    if(tbl.rows[currow-1]!=null)
                    {
                        tbl.rows[currow-1].className='selectedrow';
                        selectedLedger=tbl.rows[currow-1].cells[0].innerHTML;         
                        tbl.rows[currow].className='unselectedrow';         
                    }
                }
                else if(e.keyCode==40)
                {
                    if(tbl.rows[currow+1]!=null)
                    {
                        tbl.rows[currow+1].className='selectedrow';
                        selectedLedger=tbl.rows[currow+1].cells[0].innerHTML;         
                        tbl.rows[currow].className='unselectedrow';         
                    }
                }
            }
        }
    }
    function ApplyLedger(t)
    {
        if(divLedger==null)
                divLedger=document.getElementById("divLedger");
        if(selectedLedger!="")
        {   
            var tbl=divLedger.firstChild;
            var cnt=0;
            if(tbl!="" && tbl!=null)
            { 
                for(var i=0;i<tbl.rows.length;i++)
                {
                    if(tbl.rows[i].cells[0].innerHTML==selectedLedger)
                    {
                        t.value=tbl.rows[i].cells[1].innerHTML;
                        cnt++;
                        break;
                    }
                }
                if(cnt==0)
                {
                    selectedLedger="";
                    t.value="";
                }
            }        
        }
        else
            t.value="";
        prevText=t.value;
        divLedger.style.display="none";    
    }       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border: solid 1px black;">
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;" valign="top"
                    align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    
                                    <tr>
                                        <td>
                                            Members:
                                        </td>
                                        <td align="left">
                                            <%--<asp:DropDownList ID="ddlMember" Width="100px" runat="server" onchange="GetCustomerName();">
                                            </asp:DropDownList>--%>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr style="display: none;">
                                                    <td>
                                                        <select style="width: 100px;" id="ddlMember" onchange="GetCustomerName();">
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width:300px;">
                                                        <input type="text" style="width: 100px;" id="tdName" onkeydown="selectLedger(event,this);" onkeyup="FillLedger(event,this);"  onblur="ApplyLedger(this);GetCustomerName();"/>                                                        
                                                        <asp:Label ID="lblCusName" Text="" runat="server"></asp:Label>                                    
                                                    </td>
                                                </tr>
                                                <tr style="position: relative;">
                                                    <td align="left">
                                                        <div style=" display: none; width:100px; " class="divautocomplete"
                                                            id="divLedger">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>                                                       
                                        </td>                                       
                                       
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <iframe name="frmPinIssue" id="frmPinIssue" style="width: 100%; height: 400px;"
                                                frameborder="0" scrolling="auto"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
