﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.Collections.Generic;

namespace MyAccounts20
{
    public partial class TransferIn : System.Web.UI.Page
    {
        Transfer_BLL obj_BLL = new Transfer_BLL();
        DataTable dt = new DataTable();
        public static string strID;
        public static string BranchID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            strID = Request.QueryString["ID"];
            BranchID = Request.QueryString["BranchID"];
            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    trNav.Visible = false;
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                hidUInvID.Value = "-1";
                hidTInvID.Value = strID;
                ddlMode.Items.Insert(0, new ListItem("Cash", "Cash"));
                ddlMode.Items.Insert(1, new ListItem("Credit", "Credit"));
                if (strID != null && strID != "")
                {
                    trHeading.Visible = false;
                    trMenu.Visible = false;
                    trNav.Visible = false;
                    tdCancel.Visible = false;
                    tdDelete.Visible = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "myscript", "FindData('" + strID + "' , '" + BranchID + "');", true);
                    strID = "";
                }
            }
        }
        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;

        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Transfer_BLL obj_BLL = new Transfer_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetToBranch(string BranchID,string UserID)
        {
            try
            {
                Transfer_BLL obj_BLL = new Transfer_BLL();
                return obj_BLL.GetToBranch(BranchID,UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranchDetails(string ToBranchID)
        {
            try
            {
                Transfer_BLL obj_BLL = new Transfer_BLL();
                return obj_BLL.GetBranchDetails(ToBranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem()
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetStockItem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetTaxSystem()
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetTaxSystem().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string SaveInvoice(string hidTInvID, string hidUInvID, string InvNo, string Date, string TInvNo, string TDate, string ToBranchID, string Address, string Mode, string Days, string Note, string TotAmt, string TotDisc, string TotTax, string NetDisc, string Frieght, string LAndUL, string Adj, string NetAmt, string BranchID, string UserID, string XmlString, string RefTInvID)
        {
            try
            {
                string res;
                Transfer_BLL obj_BLL = new Transfer_BLL();
                res = obj_BLL.SaveInvoiceTransferIn(hidTInvID, hidUInvID, InvNo, dateFormat(Date), TInvNo, dateFormat(TDate), ToBranchID, Address, Mode, Days, Note, TotAmt, TotDisc, TotTax, NetDisc, Frieght, LAndUL, Adj, NetAmt, BranchID, UserID, XmlString, RefTInvID);
                return res;
            }
            catch (Exception ex)
            {
                return "E-" + ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static decimal GetTax(string tax)
        {
            decimal nTax;
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                nTax = Convert.ToDecimal(obj_BLL.GetTax(tax).Tables[0].Rows[0]["vatrate"].ToString());
            }
            catch
            {
                nTax = 0;
            }
            return nTax;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static int GetItemWiseTaxSystem(string ItemID)
        {
            int nTaxNo;
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                nTaxNo = Convert.ToInt32(obj_BLL.GetItemWiseTaxSystem(ItemID).Tables[0].Rows[0]["TaxNo"].ToString());
            }
            catch
            {
                nTaxNo = 0;
            }
            return nTaxNo;
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetData(string BranchID, string InvID, string Flag)
        {
            try
            {
                Transfer_BLL obj_Bll = new Transfer_BLL();
                return obj_Bll.GetDataTransferIn(BranchID, InvID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetDataTransfer(string RefTInvID)
        //public static string GetTransfer(string BranchID,string InvID,string Flag)
        {
            try
            {
                Transfer_BLL obj_Bll = new Transfer_BLL();
                return obj_Bll.GetDataTransfer(RefTInvID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStock(string ItemID, string BatchNo, string UDate, string BranchID)
        {
            try
            {
                Transfer_BLL obj_BLL = new Transfer_BLL();
                return obj_BLL.GetStock(ItemID, BatchNo, dateFormat(UDate), BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBatchNo(string ItemID, string BranchID)
        {
            try
            {
                Transfer_BLL obj_BLL = new Transfer_BLL();
                return obj_BLL.GetBatchNo(ItemID, BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string btnDelete_Click(string InvID, string BranchID)
        {
            try
            {
                Transfer_BLL obj_BAL = new Transfer_BLL();
                return obj_BAL.DeleteTransferIn(InvID, BranchID);
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Transfer_BLL obj_BLL = new Transfer_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetSearchInvoiceDat(string BranchID, string TInvID)
        {
            try
            {
                Transfer_BLL Obj_BLL = new Transfer_BLL();
                return Obj_BLL.GetSearchInvoiceDat(BranchID, TInvID).GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemWithBarCode(string BarCode)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL();
                return obj_BLL.GetItemWithBarCode(BarCode).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
