﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.Configuration;
using System.Xml.Linq;
using BLL;
using System.IO;


namespace MyAccounts20
{
    public partial class GroupMaster : GlobalPage
    {
        GroupMaster_BLL obj_BAL = new GroupMaster_BLL();
        //public static string strID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Clear();
                Response.Write("Your current session expired. Please Login Again.");
                Response.End();
            }
            string strID = Request.QueryString["ID"];
            if (!IsPostBack)
            {
                if (Session["UserID"].ToString() != "0")
                    tdNav.Visible = false;
                //ddlBranch.Focus();
                txtGroupName.Focus();
                if (Request.QueryString["Ledger"] == null)
                {
                    trHeader.Visible = true;
                    tblContent.Attributes["align"] = "center";
                    trMenu.Visible = true;
                    trFooter.Visible = true;
                    GetDefaults();
                }
                hidUserID.Value = Session["UserID"].ToString();
                if (strID != null && strID != "")
                {
                    trHeader.Visible = false;
                    trMenu.Visible = false;
                    tdUpdate.Visible = false;
                    tdDelete.Visible = false;
                    tdCancel.Visible = false;
                    trFooter.Visible = false;
                    tdNav.Visible = false;
                    strID = "";
                }
            }
        }

        private void GetDefaults()
        {
            Purchase_BLL Obj_BLL = new Purchase_BLL();
            ddlBranch.DataSource = Obj_BLL.GetBranch(Session["UserID"].ToString());
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "BranchID";
            ddlBranch.DataBind();
            if (ddlBranch.Items.Count != 1)
            {
                ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
            }            
            if (Session["GBID"] != null)
            {
                ddlBranch.SelectedIndex = Convert.ToInt32(Session["GBID"].ToString());
            }
            
            //ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            string bank = "0";
            if (chkbank.Checked == true)
                bank = "1";
            string res = obj_BAL.SaveGroup(ddlBranch.SelectedValue, txtGroupName.Text, ddlGCode.Text, ddlstatus.Text, bank, hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMessage.Text = "Error while saving Group";
            }
            else if (res == "-1")
            {
                lblMessage.Text = "Name already exists.";
            }
            else if (res == "1")
            {
                lblMessage.Text = "Group saved successfully.";
                ddlBranch.SelectedIndex = 0;
                txtGroupName.Text = "";
                ddlGCode.SelectedIndex = 0;
                ddlstatus.SelectedIndex = 0;
                chkbank.Checked = false;
                ddlBranch.Focus();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string bank = "0";
            if (chkbank.Checked == true)
                bank = "1";
            string res = obj_BAL.UpdateGroup(HiddenGmID.Value, ddlBranch.SelectedValue, txtGroupName.Text, ddlGCode.SelectedValue, ddlstatus.SelectedItem.ToString(), bank, hidUserID.Value);

            if (res == "0" || res == "")
            {
                lblMessage.Text = "Error while Updating GroupName.";
            }
            else if (res == "-1")
            {
                lblMessage.Text = "Name already exists.";
            }
            else if (res == "1")
            {
                lblMessage.Text = "Group saved successfully.";
                ddlBranch.SelectedIndex = 0;
                txtGroupName.Text = "";
                ddlGCode.SelectedIndex = 0;
                ddlstatus.SelectedIndex = 0;
                chkbank.Checked = false;
                ddlBranch.Focus();
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetGMNavData(string BranchID, string GMID, string Flag)
        {
            try
            {
                GroupMaster_BLL bll_GroupMaster = new GroupMaster_BLL();
                return bll_GroupMaster.GetGMNavData(BranchID, GMID, Flag).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
          
            string res;
            try
            {
                GroupMaster_BLL Obj_BLL = new GroupMaster_BLL();
                res=Obj_BLL.DeleteGroup(HiddenGmID.Value);                
                if (res == "0" || res == "")
                {
                    lblMessage.Text = "Error while Deleting";
                }                
                else if (res == "1")
                {
                    lblMessage.Text = "Deleted successfully.";
                    txtGroupName.Text = "";
                    ddlGCode.SelectedIndex = 0;
                    ddlstatus.SelectedIndex = 0;
                    chkbank.Checked = false;
                    txtGroupName.Focus();
                }
            }
            catch
            {
                throw;
            }

        }        
    }
}
    

