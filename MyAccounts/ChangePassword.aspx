﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="MyAccounts20.ChangePassword" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
var preid='ctl00_ContentPlaceHolder1_';
function Validate()
{   
    
    NPassword   = document.getElementById(preid+"txtNPassword");
    CPassword = document.getElementById(preid+"txtCPassword");                                        
    
      if(NPassword.value.trim() == "")
       {
            alert ("Enter New Password");
            NPassword.focus();
            return false;
       }
       if(CPassword.value.trim() == "")
       {
            alert ("Enter Confirm Password");
            CPassword.focus();
            return false;
       }
     
       if(NPassword.value.trim() != CPassword.value.trim())
       {
            alert ("Check New Password and Confirm Password");
            NPassword.text="";
            CPassword.text="";
            NPassword.focus();
            return false;
       }
       return true;
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="730" border="0" align="left" cellpadding="0" cellspacing="2">
                     <tr>
                        <td height="2" align="left" valign="top"></td>
                      </tr>
                      <tr>
                        <td align="left" valign="middle" bgcolor="#cccccc"><table width="700" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                            <td width="141"><h6 class="balck"><strong>Change Password </strong></h6></td>
                            <td width="559" align="left"><table width="400" align="left" cellpadding="0" cellspacing="2">
                               <tr>
                                  <td class="bodytext"><strong>Customer Id</strong></td>
                                  <td align="center"><strong>:</strong></td>
                                  <td> <asp:TextBox ID="txtCustomerID" runat="server" Size="7"></asp:TextBox></td>
                                  <td class="bodytext"><strong>Name</strong></td>
                                  <td align="center"><strong>:</strong></td>
                                  <td><asp:TextBox ID="txtCustomer" runat="server" Size="20"></asp:TextBox></td>
                                </tr>
                            </table></td>
                          </tr>
                        </table>                       </td>
                      </tr>
                      <tr>
                        <td align="left" valign="middle" bgcolor="#cccccc">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" bgcolor="#f1f0f0" class="rectangleborder"><table width="500" border="0" cellspacing="7" cellpadding="0">
                           <tr>
                              <td colspan="3" bgcolor="#f3f3f3" class="zixegreen">General Information</td>
                            </tr>
                            <tr>
                              <td width="126" class="bodytext"><strong>New Password  </strong></td>
                              <td width="9" align="center"><strong>:</strong></td>
                              <td width="337"><asp:TextBox ID="txtNPassword" runat="server" Size="31" TextMode="Password"></asp:TextBox>                              </td>
                              </tr>
                            <tr>
                              <td class="bodytext"><strong>Conform Password </strong></td>
                              <td align="center"><strong>:</strong></td>
                              <td><asp:TextBox ID="txtCPassword" runat="server" Size="31" TextMode="Password"></asp:TextBox></td>
                              </tr>
                            <tr>
                            <td class="bodytext">&nbsp;</td>
                              <td align="center">&nbsp;</td>
                              <td>
                              <asp:Label id="lblMsg" runat="server"></asp:Label>
                              </td>
                            </tr>
                            <tr>
                              <td class="bodytext">&nbsp;</td>
                              <td align="center">&nbsp;</td>
                              <td><asp:Button ID="btnUpdate" runat="server" Text="Update" 
                                    OnClientClick="return Validate();" onclick="btnUpdate_Click"/>  </td>
                              </tr>
                            <tr>
                              <td colspan="3" align="left" valign="top">&nbsp;</td>
                              </tr>
                            <tr>
                              <td colspan="3" align="left" valign="top">&nbsp;</td>
                              </tr>
                          </table></td>
                      </tr>
                      <tr>
                        <td height="25" align="center" valign="middle" bgcolor="#cccccc" class="rectangleborder">&nbsp;</td>
                      </tr>
                    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
