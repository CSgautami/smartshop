﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ReportInActiveMembers.aspx.cs" Inherits="MyAccounts20.ReportInActiveMembers" Title="InActiveMembers List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
    function btnShow_Click()
        {
            window.frames["frmInActiveMembers"].location.href="Reports/InActiveMembers.aspx" 
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
<table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
    <tr>
   <td>
    <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
    </td>
    </tr>
    <tr>
        <td>
            <iframe name="frmInActiveMembers" id="frmInActiveMembers" 
                style="width: 100%; height: 400px;">
            </iframe>             
        </td>
    </tr>    
</table>
</div>
<input type="hidden" runat="server" id="hidUserID" />
</asp:Content>
