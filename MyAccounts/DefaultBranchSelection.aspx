﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultBranchSelection.aspx.cs" Inherits="MyAccounts20.DefaultBranchSelection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Default Branch Selection</title>
    <script type="text/javascript" language="javascript">        
//        var ddlGBranch;
//        var BranchID;
//        BranchID=document.getElementById("ddlGBranch"); 
//        function GetUserID()
//        {
//            UserID=document.getElementById("hidUserID");
//        }        
//        function GetBranch()
//        {
//            if(UserID==null)
//               GetUserID(); 
//            PageMethods.GetBranch(UserID, GetBranchComplete);     
//        }

//        function GetBranchComplete(res)
//        {
//            getXml(res);    
//            ddlGBranch.options.length=0;
//            var opt=document.createElement("OPTION");
//            opt.text="Select";
//            opt.value="0";    
//            ddlGBranch.options.add(opt);
//            for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//            {
//                opt=document.createElement("OPTION");
//                opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
//                opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
//                ddlGBranch.options.add(opt);
//            }    
//        }
//        function Validate()
//        {   
//            BranchID=document.getElementById("ddlGBranch"); 
//            if(BranchID.value=="0")
//            {
//                BranchID.focus();
//                alert("Select Branch");
//                return false;
//            }
//        }
    </script> 
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: left;">
        <table align="center" width="150">
            <tr>
            <td>Branch:</td>
            <td>
                <asp:DropDownList ID="ddlGBranch" runat="server"  ></asp:DropDownList>
            </td>        
            </tr>
            <tr>
                <td colspan="100%" align="center">
                <asp:Button ID="btnOk" Text="OK" runat="server" 
                         onclick="btnOk_Click" /> 
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" runat="server" id="hidUserID" /> 
    </form>
</body>
</html>
