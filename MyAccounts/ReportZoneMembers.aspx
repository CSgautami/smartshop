﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportZoneMembers.aspx.cs" Inherits="MyAccounts20.ReportZoneMembers" Title="Report Zone Members" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript">        
    var preid;
    var ddlZone;
    var dtpFrom;
    var dtpTo;
    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
    preid ='ctl00_ContentPlaceHolder1_'
    function GetControls()
    {
        ddlZone=document.getElementById("ddlZone"); 
        dtpFrom=document.getElementById(preid+ "txtFromDate");
        dtpTo=document.getElementById(preid+ "txtToDate");             
        
    }
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function GetZone()
    {        
        PageMethods.GetZone(GetZoneComplete);    
    }

    function GetZoneComplete(res)
    {
        getXml(res);    
        ddlZone.options.length=0;
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ZoneName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("ZoneID")[0].firstChild.nodeValue;
            ddlZone.options.add(opt);
        }    
    }    
    
    function SetData()
    {    
        GetControls();
        dtpFrom.value = new Date().format("dd-MM-yyyy");
        dtpTo.value=new Date().format("dd-MM-yyyy");  
        GetZone();     
        ddlZone.focus();
    }
    function btnShow_Click()
    {       
        window.frames["frmZoneMembers"].location.href="Reports/ZoneMembersReport.aspx?ZoneID="+ddlZone.value+"&FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value;
    }          
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border:solid 1px black;">          
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>
                                        <td align="center">
                                            Zone
                                        </td>
                                        <td align ="left">                                            
                                            <select id="ddlZone" style="width: 200px;">
                                            </select>
                                        </td>
                                        
                                    </tr>
                                    <tr>                                        
                                        <td align="left">
                                            <span id="spnFromDate">From :</span>                                                                                     
                                        </td>                                        
                                        <td align ="left">
                                            <input type="text" runat="server" id="txtFromDate" maxlength="10" onfocus="showcalendar(this);" onclick="showcalendar(this);" onkeydown="HideCalendar(event);" style="width:100px;" />
                                        </td>
                                        <td align="left">
                                            <span id="spnToDate" >To :</span>
                                        </td>
                                        <td align="left">
                                            <input type="text" runat="server" id="txtToDate" maxlength="10" onfocus="showcalendar(this);" onclick="showcalendar(this);" onkeydown="HideCalendar(event);" style="width:100px;" />                                                                                    
                                        </td>                                                                                                                                                      
                                                                                                                                                                                                 
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>                                                                                                                       
                                    </tr>                                                      
                                </table>                             
                            </td>                            
                        </tr>    
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>                                        
                                        <td>                                            
                                            <iframe name="frmZoneMembers" id="frmZoneMembers" style="width: 100%; height: 400px;"
                                                frameborder="0" scrolling="auto"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                    
                    </table>
                </td>
            </tr>            
        </table>
    </div>    
     <div id="divCalendar" class="Calendar">
    </div>
</asp:Content>
