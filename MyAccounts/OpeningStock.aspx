﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpeningStock.aspx.cs" Inherits="MyAccounts20.OpeningStock"
    EnableEventValidation="false" %>

<%@ Register Src="~/UserControl/MenuBar.ascx" TagPrefix="UC" TagName="Menu" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OpeningStock</title>
    <link href="MyAccounts20.css" rel="stylesheet" type="text/css" />

    <script src="JS/ShowForms.js" type="text/javascript"></script>

    <script src="JS/OpenStock.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
    var preid='';
    String.prototype.trim=function() {
          return this.replace(/^\s*/, "").replace(/\s*$/, "");
          }
//          function ValidateSave()
//          {
//              try
//              {  var BranchID=document.getElementById(preid+"ddlbranch");
//                 var Item=document.getElementById(preid+"tdItem");
//                 var BatchNo=document.getElementById(preid+"txtbatchno");
//                 var MRP=document.getElementById(preid+"txtmrp");
//                 var Price=document.getElementById(preid+"txtprice");
//                 var Quantity=document.getElementById(preid+"txtquantity");
//                 
//                 if(BranchID.value=="0")
//                 {
//                 alert("Select BranchID");
//                 BranchID.focus();
//                 return false;
//                 }
//                 if(Item.value=="")
//                 {
//                 alert("Select Item");
//                 Item.focus();
//                 return false;
//                 }
//                 if(BatchNo.value.trim()=="")
//                 {
//                 alert("Enter BatchNo");
//                 BatchNo.focus();
//                 return false;
//                 }
//                 if(MRP.value.trim()=="")
//                 {
//                 alert("Enter MRP");
//                 MRP.focus();
//                 return false;
//                 }
//                 if(Price.value.trim()=="")
//                 {
//                 alert("Enter Price");
//                 Price.focus();
//                 return false;
//                 }
//                 if(Quantity.value.trim()=="")
//                 {
//                 alert("Select Quantity");
//                 Quantity.focus();
//                 return false;
//                 }
//              return true;
//               }
//              catch(err)
//               {
//              return false;
//               }
//             }
//            function ConfirmEdit()
//            {
//            if(!confirm("Do you want to Edit?"))
//            return false;
//            }
//            function ConfirmDelete()
//            {
//            if(! confirm("Do you want to Delete?"))
//            return false;
//            }
//    function SetData()
//    {
//        GetItems();
//    }
//    
    function binddata()
    {
        document.getElementById("hidbinditems").value="1";
        document.forms[0].submit();
    }
    function InsertItem()
    {
        ItemClick();
        //binddata();
    }              
    function ItemKeydown(e,t)
    {
        if(e.keyCode==45)
        {
            InsertItem();
        }
    }   
    </script>

</head>
<body onload="SetData();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="SM1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
            <tr>
                <td width="100%" valign="middle" class="Header">
                    Foster
                </td>
            </tr>
            <tr>
                <td width="100%" valign="middle" class="Tabs">
                    <UC:Menu ID="menu" runat="server"></UC:Menu>
                </td>
            </tr>
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top">
                    <table align="center" width="800px">
                        <tr>
                            <td height="20px" colspan="100%">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                            <table align="center" width="100%">
                            <tr>
                            <td align="right" style="width: 55px;">
                                Branch
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlbranch" runat="server" Width="175px" onchange="GetData();">
                                </asp:DropDownList>
                            </td>
                            </tr>
                            </table>
                            </td>
                            
                            <%--<td align="center">                    
                                <input type="button" id="btnShow" value="Show" onclick="GetData();" />                    
                            </td>--%>
                        </tr>
                        <tr>
                            <td style="height: 5px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8" align="left">
                                <div class="divgrid" style="height: 150px; border: solid 1px #98bf21;">
                                    <table id="tblGrid">
                                        <tr class="dbookheader">
                                            <%--<th style ="width:30px;">
                                                    Code
                                                </th>--%>
                                            <th style="display: none; width: 0px;">
                                                StockID
                                            </th>
                                            <th style="width: 0px; display: none">
                                                Code
                                            </th>
                                            <th style="display: none; width: 0px;">
                                                ICID
                                            </th>
                                            <th style="width: 250px;">
                                                Item
                                            </th>
                                            <th style="width: 100px;">
                                                BatchNo
                                            </th>
                                            <th style="width: 100px;">
                                                MRP
                                            </th>
                                            <th style="width: 100px;">
                                                Price
                                            </th>
                                            <th style="width: 100px;">
                                                Quantity
                                            </th>
                                            <%--<th style="width: 100px;">
                                                FreeQty
                                            </th>--%>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="6" align="center" style="width:800px">
                                <asp:GridView ID="gvOpeningStock" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                    PageSize="10" AllowPaging="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Code">
                                            <ItemTemplate>
                                                <input type="hidden" runat="server" id="hidICID" value='<%#Eval("ICID")%>' />
                                                <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode") %>'></asp:Label>
                                            </ItemTemplate>                                            
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item">
                                            <ItemTemplate>
                                                <input type="hidden" runat="server" id="hidStockID" value='<%#Eval("StockID")%>' />                                                
                                                <asp:Label ID="lblItem" runat="server" Text='<%#Eval("Item") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BatchNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBatchNo" runat="server" Text='<%#Eval("BatchNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQuantity" runat="server" Text='<%#Eval("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MRP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMRP" runat="server" Text='<%#Eval("MRP") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Price">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FreeQuantity">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFQty" runat="server" Text='<%#Eval("FreeQty") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                        <%--<asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnEdit"  runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                                    OnClick="lbtnEdit_Click"></asp:LinkButton>
                                                <asp:LinkButton ID="lbtnDelete"  runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                                    OnClick="lbtnDelete_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>  --%>
                        <%-- <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="gridNavButtons" onclick="EditClick(event,this)">Edit</a> &nbsp;&nbsp
                                                <a href="#" class="gridNavButtons" onclick="DeleteClick(event,this)">Delete</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>    
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>    --%>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            
                        </tr>
                        <tr>
                            
                            <td align="left" >
                               &nbsp;Item 
                            </td>
                            <td align="left">                                
                                <table cellpadding="0" cellspacing="0">
                                    <tr style="display: none;">
                                        <td>
                                            <select style="width: 100px;" id="ddlItem" onkeydown="ItemKeydown(event,this);">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="tdItem" Width="300px" runat="server" onkeydown="selectItem(event,this);" onkeyup="FillItem(event,this);"  onblur="ApplyItem(this);GetItemName();"></asp:TextBox>                                                
                                            <img alt="" id="imgItem" src="~/Imgs/create.gif" runat="server" onclick="InsertItem();" />
                                        </td>
                                    </tr>
                                    <tr style="position: relative;">
                                        <td>
                                            <div style=" display: none; width:300px; " class="divautocomplete"
                                                id="divItem">
                                            </div>
                                        </td>
                                    </tr>
                                </table>                                    
                            </td>
                            <td align="right">
                                Batchno
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtBatchNo" runat="server" Width="100px"></asp:TextBox>
                            </td>
                            <td align="left">
                                Quantity &nbsp;
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtQuantity" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                        <td colspan="3">
                        <table align="center" width="100%">
                        <tr>
                            <td align="left">
                                &nbsp;&nbsp; MRP
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtMRP" runat="server" MaxLength="20" Width="100px" style="text-align:right"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                price
                            </td>                            
                            <td align="left">
                                <asp:TextBox ID="txtPrice" runat="server" Width="100px" style="text-align:right"></asp:TextBox>
                            </td>
                            <td><td></td></td>
                            <td>
                                            <input type="button" id="btnSave" value="Save" onclick="btnSave_Click(event,this);" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnUpdate" value="Update" class="hidbutton" onclick="btnUpdate_Click(event,this);" />
                                        </td>
                                        <td>                                           
                                            <input type="button" id="btnDelete" class="hidbutton" value="Delete" onclick="btnDelete_Click();" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCancel" runat="server" value="Cancel" onclick="btnCancel_Click();" />
                                        </td>
                                        <input type="hidden" runat="server" id="hidbinditems" />
                            </tr>
                            </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 7px" colspan="100px">
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="100px" align="center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" id="trFooter">
                <td width="100%" valign="middle" class="Footer">
                    Powered By V2SOFTECH &nbsp;&nbsp;&nbsp;VisitUs:http://www.v2softech.com
                </td>
            </tr>
        </table>
        <input type="hidden" runat="server" id="hidStockID" />
        <input type="hidden" runat="server" id="hidICID" />
        <input type="hidden" runat="server" id="hidBranchID" />
        <input type="hidden" runat="server" id="hidUserID" />
        <input type="hidden" runat="server" id="hidGBID" />
    </div>
    </form>
</body>
</html>
