﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="BranchMaster.aspx.cs" Inherits="MyAccounts20.BranchMaster" Title="Branch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
    var preid='ctl00_ContentPlaceHolder1_';
    String.prototype.trim=function() {
          return this.replace(/^\s*/, "").replace(/\s*$/, "");
          }
          function ValidateSave()
          {
              try
              {
                var BranchName = document.getElementById(preid + "txtBranchName");
                var BranchAddress=document.getElementById(preid+"txtBranchAddress");
                var ContactPerson= document.getElementById(preid + "txtContactPerson");
                var ContactNo = document.getElementById(preid+"txtContactNo");
                var EmailID = document.getElementById(preid+"txtEmailID");
                var BranchType=document.getElementById(preid+"ddlType");
                var Circle = document.getElementById(preid+"txtCircle");
                var Division = document.getElementById(preid+"txtDivision");
                var Desig = document.getElementById(preid+"txtDesig");
                if (BranchName.value.trim() == "")
                {        
                    BranchName.value="";
                    BranchName.focus();
                    alert("Enter BranchName");
                    return false;
                    
                }    
                if (BranchAddress.value.trim() == "")
                {        
                    BranchAddress.value="";
                    BranchAddress.focus();
                    alert("Enter BranchAddress");
                    return false;
                    
                }    
                if(BranchType.value=="0")
                {
                   BranchType.focus();
                   alert("Select BranchType");
                   return false;    
                }
                  return true;
                   }
                  catch(err)
                  {
                     return false;
                  }
               }
                function ConfirmEdit()
                {
                    if(!confirm("Do you want to Edit?"))
                    return false;
                }
                function ConfirmDelete()
                {
                    if(! confirm("Do you want to Delete?"))
                    return false;
                } 
               
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center" width="800px">   
        <tr>
            <td height="20px" colspan="100%">
            </td>
        </tr>        
          <tr>            
            <td>
                <table align="center">
                    <tr>
                        &nbsp;<td align="left" style="width:150px;">
                          Branch Name 
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBranchName" runat="server" Width="170px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                             Branch Address 
                        </td>
                        
                        <td align="left">
                            <asp:TextBox ID="txtBranchAddress" runat="server"  Width="170px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        
                        <td align="left">
                             Contact Person
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtContactPerson" runat="server"  Width="170px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        
                        <td align="left">
                           Contact No
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtContactNo" runat="server"  Width="170px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                      
                        <td align="left">
                           Email ID
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmailID" runat="server"  Width="170px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                       <td align="left">
                            Type
                       </td>
                       <td align="left">
                            <asp:DropDownList ID="ddlType" runat="server" Width="175px"></asp:DropDownList>
                       </td>
                    </tr>
                    <tr style="display:none">
                         <td align="left">
                             Circle
                         </td>
                        <td align="left">
                            <asp:TextBox ID="txtCircle" runat="server"  Width="170px"></asp:TextBox>
                        </td>
                   </tr>
                   <tr style="display:none">
                        <td align="left">
                             Division
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDivision" runat="server"  Width="170px"></asp:TextBox>
                        </td>
                   </tr>
                   <tr>
                      <td align="left">
                           Designation
                      </td>
                      <td align="left">
                           <asp:TextBox ID="txtDesig" runat="server" Width="170px"></asp:TextBox>
                      </td>
                   </tr>
                </table>
            </td>           
            </tr>            
        <tr>
            <td colspan="100%">
                <table align="center">
                    <tr align="left">
                        <td  align="left">
                            <asp:Button ID="btnSave" runat="server" Text="Save" 
                                OnClientClick="return ValidateSave();" onclick="btnSave_Click" />
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" 
                                OnClientClick="return ValidateSave();" onclick="btnUpdate_Click" Visible="false"/>    
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblMsg" runat="server" EnableViewState="false" ForeColor="Blue"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="width:100px;">
            <td align="center" colspan="100%" >
                <asp:GridView ID="gvBranch" runat="server" AutoGenerateColumns="false" CssClass="grid"
                    PageSize="10" AllowPaging="true" Font-Size="Small" OnPageIndexChanging="gvBranch_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="BranchName">
                            <ItemTemplate>
                                <input type="hidden" runat="server" id="hidBranchID" value='<%#Eval("BranchID")%>' />
                                <asp:Label ID="lblBranchName" runat="server"  Text='<%#Eval("BranchName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BranchAddress">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server"  Text='<%#Eval("BranchAddress") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ContactPerson">
                            <ItemTemplate>
                                <asp:Label ID="lblContactPerson" runat="server" Text='<%#Eval("ContactPerson") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ContactNo">
                            <ItemTemplate>
                                <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EmailID">
                            <ItemTemplate>
                                <asp:Label ID="lblEmailID" runat="server" Text='<%#Eval("EmailID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("BranchType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <%-- <asp:TemplateField HeaderText="Circle">
                            <ItemTemplate>
                                <asp:Label ID="lblCircle" runat="server" Text='<%#Eval("Circle") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Division">
                            <ItemTemplate>
                                <asp:Label ID="lblDivision" runat="server" Text='<%#Eval("Division") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Designation">
                            <ItemTemplate>
                                <asp:Label ID="lblDesig" runat="server" Text='<%#Eval("Designation") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit"  OnClientClick="return ConfirmEdit();" OnClick="lbtnEdit_Click"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();" OnClick="lbtnDelete_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <input type="hidden" runat="server" id="hidBranchID" />
    <input type="hidden" runat="server" id="hidUserID" />
</asp:Content>

