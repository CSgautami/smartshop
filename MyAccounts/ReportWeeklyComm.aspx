﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportWeeklyComm.aspx.cs" Inherits="MyAccounts20.ReportWeeklyComm" Title="Weekly Commision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript" >
    function SetData()
    {
        window.frames["frmWeeklyComm"].location.href="Reports/WeeklyCommReport.aspx";
    }
    function btnShow_Click()
    {       
        window.frames["frmWeeklyComm"].location.href="Reports/WeeklyCommReport.aspx";
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border:solid 1px black;">          
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">                                    
                                    <tr>                                       
                                                                                                                                                        
                                        <td>
                                        <%--    <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />--%>
                                        </td>                                                                                                                       
                                    </tr>                                                      
                                </table>                             
                            </td>                            
                        </tr>    
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>                                        
                                        <td>                                            
                                            <iframe name="frmWeeklyComm" id="frmWeeklyComm" style="width: 100%; height: 400px;"
                                                frameborder="0" scrolling="auto"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                    
                    </table>
                </td>
            </tr>            
        </table>
 </div>        
</asp:Content>
