﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuBar.ascx.cs" Inherits="MyAccounts20.UserControl.MenuBar" %>
<link href="menu/ddsmoothmenu.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/ddsmoothmenu.js">
        /***********************************************
        * Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/
</script>

<script type="text/javascript">
        ddsmoothmenu.init({
            mainmenuid: "smoothmenu1", //menu DIV id
            orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
            classname: 'ddsmoothmenu', //class added to menu's outer DIV
            //customtheme: ["#1c5a80", "#18374a"],
            contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
        })
</script>

<table width="100%" align="left">
    <tr>
        <td>
            <table width="100%" align="left" class="tblmenu">
                <tr>
                    <td runat="server" id="tdMenu" style="text-align: left;">
                    </td>
                    <td>
                    </td>
                    <td width="100px">
                        <asp:LinkButton ID="lbtnLogOut" runat="server" Text="LogOut" OnClick="lbtnLogOut_Click"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="background-color: White;">
            <table cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td id="tdName" runat="server" align="center" style="font-size: large; font-family: Verdana;">
                        <asp:Label ID="lblName" Text="" runat="server" ForeColor="CadetBlue" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
