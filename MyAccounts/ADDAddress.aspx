﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ADDAddress.aspx.cs" Inherits="MyAccounts20.ADDAddress" Title="ADD Address" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
        var preid;
        var Customer; 
        var Name;
        var HNO;
        var Street1;
        var Street2;
        var AreaName;
        var City;
        var State;
        var LandMark;
        var Zone;
        var UserID;
        var preid = 'ctl00_ContentPlaceHolder1_'
        
        var xmlObj;
        function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }
            
        }
           function GetControls()
           {
             Customer=document.getElementById(preid+"txtCustomer");
             Name = document.getElementById(preid + "txtName");
             HNO=document.getElementById(preid+"txtHNO");
             Street1= document.getElementById(preid + "txtSOne");
             Street2 = document.getElementById(preid+"txtSTwo");
             AreaName = document.getElementById(preid+"ddlArea");
             City = document.getElementById(preid + "ddlCity");
             State=document.getElementById(preid+"ddlState");
             LandMark = document.getElementById(preid + "txtMark");
             Zone=document.getElementById(preid+"txtZone");
             
           }

                function GetAreaWiseZone()
                {
                    GetControls();
                    PageMethods.GetAreaWiseZone(AreaName.value,GetAreaWiseZoneComplete);    
                }
                function GetAreaWiseZoneComplete(res)
                {
                    getXml(res);    
                    Zone.value=xmlObj.getElementsByTagName("Table")[0].getElementsByTagName("ZoneName")[0].firstChild.nodeValue;;
                    
                }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
         <table cellspacing="0" cellpadding="0" width="730" align="left" border="0">
            <tbody>
            <tr>
            <td valign="top" align="left" height="2"></td></tr>
            <tr>
            <td class="rectangleborder" valign="middle"align="left" bgcolor="#cccccc" height="15">
            <table cellspacing="0" cellpadding="0" width="720" border="0">
            <tbody>
            <tr>
            <td width="134">
            <h6 class="balck"><strong>Add Adress / New</strong></h6></td>
            <td width="566">
            <table cellspacing="2" cellpadding="0" width="600" align="center">
            <tbody>
            <tr>
            <td class="bodytext" width="82"><strong>Customer Id</strong></td>
            <td align="center" width="12"><strong>:</strong></td>
            <td width="48"> 
                <asp:textbox ID="txtCustomer" runat="server" width="50px"></asp:textbox>
            </td>
            <td class="bodytext" width="43"><strong>Name</strong></td>
            <td align="center" width="12"><strong>:</strong></td>
            <td width="387">
                <asp:textbox ID="txtName" runat="server"></asp:textbox>
            </td></tr></tbody></table></td></tr></tbody></table></td></tr>
            <tr>
            <td class="rectangleborder" valign="middle" align="left" bgcolor="#cccccc" height="15">
                &nbsp;</td></tr>
            <tr>
            <td class="rectangleborder" valign="top" align="left" bgcolor="#f1f0f0">
            <table cellspacing="2" cellpadding="0" width="600" border="0">
            <tbody>
            <tr>
            <td class="bodytext" width="131"><strong>H.No.</strong></td>
            <td align="center" width="9"><strong>:</strong></td>
            <td width="432">
                <asp:textbox ID="txtHNO" runat="server" width="200px"></asp:textbox>
            </td></tr>
            <tr>
            <td class="bodytext"><strong>Street Name 1 
            </strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:textbox ID="txtSOne" runat="server" width="200px"></asp:textbox>
            </td></tr>
            <tr>
            <td class="bodytext"><strong>Street Name 2</strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:textbox ID="txtSTwo" runat="server" width="200px"></asp:textbox>
            </td></tr>
            <tr>
            <td class="bodytext"><strong>Area name <span class="style5">*</span> </strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:dropdownlist ID="ddlArea" runat="server" width="205px" 
                    OnChange="GetAreaWiseZone();" ></asp:dropdownlist>  
            </td></tr>
            <tr>
            <td class="bodytext"><strong>City <span class="style5">*</span> </strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:dropdownlist ID="ddlCity" runat="server" width="205px"></asp:dropdownlist>
             </td></tr>
            <tr>
            <td class="bodytext"><strong>State <span class="style5">*</span></strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:dropdownlist ID="ddlState" runat="server" width="205px"></asp:dropdownlist>
            </td></tr>
            <tr>
            <td class="bodytext"><strong>Land mark 
            </strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:textbox ID="txtMark" runat="server" width="200px"></asp:textbox>
            </td></tr>
            <tr>
            <td><strong class="bodytext">Zone</strong></td>
            <td align="center"><strong>:</strong></td>
            <td>
                <asp:textbox ID="txtZone" runat="server" width="200px" ReadOnly="True"></asp:textbox> 
            
            </td></tr>
            <tr>
            <td class="bodytext">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td>
            <asp:Button ID="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click" /> 
            <asp:Button ID="btnAdd" runat="server" Text="Add" onclick="btnAdd_Click" />
            </td></tr>
             <tr>
                <td colspan="100%" align="left">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                </td>
            </tr>
            <tr>
            <td valign="top" align="left" colspan="3">&nbsp;</td></tr>
            <tr>
            <td valign="top" align="left" colspan="3">&nbsp;</td></tr></tbody></table></td></tr>
            <tr>
            <td class="rectangleborder" valign="middle" align="center" bgcolor="#cccccc" height="25">
                &nbsp;</td></tr></tbody></table>
     </div>
         <input type="hidden" runat="server" id="hidUserID" />
         <input type="hidden" runat="server" id="ShippingID" />
</asp:Content>

