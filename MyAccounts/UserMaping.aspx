﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true"
    CodeBehind="UserMaping.aspx.cs" Inherits="MyAccounts20.UserMaping" Title="UserMaping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
        var ddlUser;
        var chkLBranch;
        var preid;
        preid='ctl00_ContentPlaceHolder1_';
        function GetControls()
        {
            ddlUser = document.getElementById("ddlUser");
            chkLBranch = document.getElementById(preid+"chkLBranch");
        }
        function SaveData()
        {
            if(ddlUser.value=="0")
            {
                alert("Select user");
                ddlUser.focus();
                return false;
            }            
            var xmlDet="<XML>"            
            for (var i=0;i<chkLBranch.childNodes.length; i++)
            {
                var currentTd = chkLBranch.childNodes[i].childNodes[0];
                var listControl = currentTd.childNodes[0];                                
                if ( listControl.checked == true )
                 {            
                    //xmlDet=xmlDet+"<BranchDet>";                            
                    //xmlDet=xmlDet+"<BranchID>"+tr.cells[0].innerHTML.trim()+"</BranchID>";
                 }                 
            }   
            xmlDet=xmlDet+"</XML>";
        }
        function getXml(xmlString)
        {
            xmlObj=null;
            try {
                    var browserName = navigator.appName;
                    if (browserName == "Microsoft Internet Explorer") {
                        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                        xmlObj.async = "false";
                        xmlObj.loadXML(xmlString);
                }
                else 
                {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }    
        }
        function SetData()
        {
            GetControls();
            GetUser();
          //  GetBranch();
        }
        function GetUser()
        {
            PageMethods.GetUser(GetUserComplete);
        }
        function GetUserComplete(res)
        {
            getXml(res);    
            ddlUser.options.length=0;
            var opt=document.createElement("OPTION");
            opt.text="Select";
            opt.value="0";    
            ddlUser.options.add(opt);
            for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
            {
                opt=document.createElement("OPTION");
                opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("UserName")[0].firstChild.nodeValue;
                opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("UserID")[0].firstChild.nodeValue;
                ddlUser.options.add(opt);
            }    
        }
//        function GetBranch()
//        {
//            PageMethods.GetBranch(GetBranchComplete);
//        }
//        function GetBranchComplete(res)
//        {
//            getXml(res);    
//            chkLBranch.options.length=0;
//            var opt=document.createElement("OPTION");
//            opt.text="Select";
//            opt.value="0";    
//            chkLBranch.options.add(opt);
//            for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
//            {
//                opt=document.createElement("OPTION");
//                opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
//                opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
//                chkLBranch.options.add(opt);
//            }    
//        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                User
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlUser" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlUser_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset id="fldsetBranch" style="width: 800px; overflow-x: auto; overflow-y: hidden;">
                        <legend>Branches</legend>
                        <table align="left">
                            <tr>
                                <td>
                                    <asp:CheckBoxList ID="chkLBranch" runat="server" RepeatDirection="Horizontal">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px;">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset id="fldsetPages">
                        <legend>Pages</legend>
                        <div style="width: 800px; height: 280px; overflow: auto;">
                            <table align="left">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="phPages" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="height: 5px;">
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false" Style="color: red;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
