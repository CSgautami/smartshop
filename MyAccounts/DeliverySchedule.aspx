﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="DeliverySchedule.aspx.cs" Inherits="MyAccounts20.DeliverySchedule"
    Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="ja-topsl2">
        <div class="moduletable">
            <h3>
                DELIVERY SCHEDULE</h3>
            <div id="devlier">
                <p>
                    Door Delivery for Hyderabad & Surroundings Will Starts from 01st May 2011.<br />
                    Door delivery is at free of cost.</p>
                <p>
                    No delivery for other places. Ex Showroom/Office delivery only.</p>
                <p>
                    However the other place orders will be delivered on to pay basis by the customers.</p>
                <p>
                    Delivery Schedule for Hyderabad will be Published very soon</p>
            </div>
        </div>
    </div>
</asp:Content>
