﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class UserRegistration : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FillDropdowns();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        void FillDropdowns()
        {
            try
            {
                Country_BLL bll_Country = new Country_BLL();
                ddlCountry.DataSource = bll_Country.GetCountrys().Tables[0];
                ddlCountry.DataTextField = "CountryName";
                ddlCountry.DataValueField = "CountryID";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("Select", "0"));
                ddlState.Items.Insert(0, new ListItem("Select", "0"));
                ddlCity.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch
            {
                throw;
            }
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string ddlCountry_Change(string CountryID)
        {
            StringWriter sw = new StringWriter();
            try
            {
                City_BLL obj_BAL = new City_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetStates();
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "States";
                ds.WriteXml(sw);

            }
            catch { }
            return sw.ToString();
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string ddlState_Change(string StateID)
        {
            StringWriter sw = new StringWriter();
            try
            {
                City_BLL obj_BAL = new City_BLL();
                DataSet ds = new DataSet();
                ds = obj_BAL.GetCity(StateID);
                ds.DataSetName = "XML";
                ds.Tables[0].TableName = "Cities";
                ds.WriteXml(sw);

            }
            catch { }
            return sw.ToString();
        }

        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string RegisterUser(string UserName, string Password, string Address, string CityID, string OtherCity, string StateID, string CountryID, string EmailID, string PhoneNos, string MobileNos)
        {

            try
            {
                UserRegistration_BLL obj_BAL = new UserRegistration_BLL();
                return obj_BAL.RegisterUser(UserName, Password, Address, CityID, OtherCity, StateID, CountryID, EmailID, PhoneNos, MobileNos);

            }
            catch { throw; }

        }
    }
}
