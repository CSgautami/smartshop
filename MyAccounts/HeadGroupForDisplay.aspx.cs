﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.Data;

namespace MyAccounts20
{
    public partial class HeadGroupForDisplay : System.Web.UI.Page
    {
        HeadGroupFD_BLL Obj_BLL = new HeadGroupFD_BLL();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            GetDefaults();
            btnCancel_Click(sender, e);
        }
        void GetDefaults()
        {
            HeadGroup_BLL Obj_HBLL = new HeadGroup_BLL();
            ddlHeadGroup.DataSource = Obj_HBLL.GetHeadGroup();
            ddlHeadGroup.DataTextField = "HeadGroupName";
            ddlHeadGroup.DataValueField = "HeadGroupID";
            ddlHeadGroup.DataBind();
            ddlHeadGroup.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem.DataSource = Obj_BLL.GetHeadWiseItems(ddlHeadGroup.SelectedValue.ToString());
            ddlItem.DataTextField = "StockItemName";
            ddlItem.DataValueField = "ICID";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("Select", "0"));

        }
        private void FillGrid()
        {
            try
            {
                ds = Obj_BLL.GetData();
                
                
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ddlHeadGroup.SelectedValue = "0";
            ddlItem.SelectedValue = "0";
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

        }
    }
}
