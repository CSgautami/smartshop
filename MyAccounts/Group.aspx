﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="Group.aspx.cs" Inherits="MyAccounts20.Group" Title="Groups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript" type="text/javascript">
function showItem(t)
{
    window.parent.location="Item.aspx?ItemID="+t.id;
}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center" style="width: 700px;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:DataList ID="dlItemDet" runat="server" AllowPaging="true" PageSize="7" Width="700px">
                    <ItemTemplate>
                        <table width="500" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="500">
                                    <table width="615" border="1" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                        <tr>
                                            <td width="91" rowspan="2" align="center" bgcolor="#F6FEF0" class="zixe">
                                                <input type="hidden" id="hidID" runat="server" value='<% #Eval("SiNo") %>' />
                                                <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode") %>'></asp:Label>
                                            </td>
                                            <td width="500" align="left" bgcolor="#F6FEF0" class="zixeleftpadding">
                                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td bgcolor="#F6F7FC" class="2border">
                                                <%--<img src="MasterPageFiles/jeedipappu.jpg" width="54" height="20" />--%>
                                                <img src="TeluguImages/<%#Eval("SiNo")%>.jpg" width="90" height="20" alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="500" border="1" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                        <tr>
                                            <td width="100" bgcolor="#FFFFFF">
                                                <table width="100" border="0" cellpadding="0" cellspacing="2" style="border-color: #eeeeee">
                                                    <tr>
                                                        <td>
                                                            <img src="ItemImages/<%#Eval("SiNo")%>.jpg" width="98" height="98" alt="" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#F6FEF0">
                                                <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td colspan="2" rowspan="3" valign="middle" bgcolor="#F6FEF0">
                                                            <table width="210" border="0" cellpadding="0" cellspacing="0" bgcolor="#F6FEF0">
                                                                <tr>
                                                                    <td style="height: 25px; width: 120px;" class="zixe">
                                                                        Market price / M.R.P
                                                                    </td>
                                                                    <td class="zixe">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 33px;" align="center" class="red">
                                                                        <strong><span>
                                                                            <img src="MasterPageFiles/finalred.jpg" />
                                                                        </span>
                                                                            <asp:Label ID="lblMRP" runat="server" Text='<%#Eval("MRP") %>'></asp:Label></strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 15px;" class="zixe">
                                                                        Our Price
                                                                    </td>
                                                                    <td class="zixe">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 33px;" align="center">
                                                                        <strong><span class="green"><span>
                                                                            <img src="MasterPageFiles/finalgreen.jpg" />
                                                                        </span>
                                                                            <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("osp") %>'></asp:Label></strong></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 25px;" class="zixe">
                                                                        Availability
                                                                    </td>
                                                                    <td align="center" class="zixe">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 33px;" align="center">
                                                                        <asp:Image ID="imgNA" runat="server" ImageUrl="MasterPageFiles/cross symbol.png" />                                                                                                            
                                                                        <asp:CheckBox ID="chkAvailable" runat="server"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="11" height="30" bgcolor="#F6FEF0" class="zixegreen">
                                                            &nbsp;
                                                        </td>
                                                        <td width="129" height="20" align="center" valign="bottom" bgcolor="#F6FEF0" class="black">
                                                            <strong>Quantity</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" bgcolor="#F6FEF0" class="zixegreen">
                                                            &nbsp;
                                                        </td>
                                                        <td height="25" align="center" valign="middle" bgcolor="#F6FEF0">
                                                            <%--<input name="textfield2" type="text" id="textfield" value="1" size="1" style="text-align: center;" />--%>
                                                            <asp:TextBox ID="txtQty" runat="server" Width="40px" MaxLength="1" Style="text-align: center;"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="30" bgcolor="#F6FEF0" class="zixegreen">
                                                            &nbsp;
                                                        </td>
                                                        <td height="20" align="center" valign="top" bgcolor="#F6FEF0">
                                                            <a href="#">
                                                                <%--<img src="images/cart.jpg" width="80" height="26" border="0" /></a>--%>
                                                                <%--<asp:Button ID="btnAddtoCart" runat="server" Width="80px" Height="25px" Text="Add to Cart" OnClick="btnAddtoCart_Click"/>--%>
                                                                <asp:ImageButton ID="btnAddtoCart" ImageUrl="MasterPageFiles/cart.jpg" runat="server"
                                                                    Width="80px" Height="26px" OnClick="btnAddtoCart_Click" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>
