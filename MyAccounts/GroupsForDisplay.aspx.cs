﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;
namespace MyAccounts20
{
    public partial class GroupsForDisplay : System.Web.UI.Page
    {
        GroupsFD_BLL obj_BLL = new GroupsFD_BLL();
        public static int nID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                GetDefaults();
                ClearFields();
            }
        }
        private void GetDefaults()
        {
            //GroupsFD_BLL obj_BLL = new GroupsFD_BLL();
            ddlItem1.DataSource = obj_BLL.GetStcokItem();
            ddlItem1.DataTextField = "StockItemName";
            ddlItem1.DataValueField = "ICID";
            ddlItem1.DataBind();
            ddlItem1.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem2.DataSource = obj_BLL.GetStcokItem();
            ddlItem2.DataTextField = "StockItemName";
            ddlItem2.DataValueField = "ICID";
            ddlItem2.DataBind();
            ddlItem2.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem3.DataSource = obj_BLL.GetStcokItem();
            ddlItem3.DataTextField = "StockItemName";
            ddlItem3.DataValueField = "ICID";
            ddlItem3.DataBind();
            ddlItem3.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem4.DataSource = obj_BLL.GetStcokItem();
            ddlItem4.DataTextField = "StockItemName";
            ddlItem4.DataValueField = "ICID";
            ddlItem4.DataBind();
            ddlItem4.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem5.DataSource = obj_BLL.GetStcokItem();
            ddlItem5.DataTextField = "StockItemName";
            ddlItem5.DataValueField = "ICID";
            ddlItem5.DataBind();
            ddlItem5.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem6.DataSource = obj_BLL.GetStcokItem();
            ddlItem6.DataTextField = "StockItemName";
            ddlItem6.DataValueField = "ICID";
            ddlItem6.DataBind();
            ddlItem6.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem7.DataSource = obj_BLL.GetStcokItem();
            ddlItem7.DataTextField = "StockItemName";
            ddlItem7.DataValueField = "ICID";
            ddlItem7.DataBind();
            ddlItem7.Items.Insert(0, new ListItem("Select", "0"));

            ddlItem8.DataSource = obj_BLL.GetStcokItem();
            ddlItem8.DataTextField = "StockItemName";
            ddlItem8.DataValueField = "ICID";
            ddlItem8.DataBind();
            ddlItem8.Items.Insert(0, new ListItem("Select", "0"));            
        }
        private void ClearFields()
        {
            try
            {
                nID = -1;
                txtGroup1.Text = "";
                txtGroup2.Text = "";
                txtGroup3.Text = "";
                txtGroup4.Text = "";
                txtGroup5.Text = "";
                txtGroup6.Text = "";
                txtGroup7.Text = "";
                txtGroup8.Text = "";
                ddlItem1.SelectedValue = "0";
                ddlItem2.SelectedValue = "0";
                ddlItem3.SelectedValue = "0";
                ddlItem4.SelectedValue = "0";
                ddlItem5.SelectedValue = "0";
                ddlItem6.SelectedValue = "0";
                ddlItem7.SelectedValue = "0";
                ddlItem8.SelectedValue = "0";
                txtDesc1.Text = "";
                txtDesc2.Text = "";
                txtDesc3.Text = "";
                txtDesc4.Text = "";
                txtDesc5.Text = "";
                txtDesc6.Text = "";
                txtDesc7.Text = "";
                txtDesc8.Text = "";                
                txtGroup1.Focus();
                GetData();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        void GetData()
        {
            try
            {
                DataSet ds = obj_BLL.GetData();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    nID = Convert.ToInt32(ds.Tables[0].Rows[0]["ID"].ToString().ToString());                    
                    txtGroup1.Text = ds.Tables[0].Rows[0]["GroupName1"].ToString();
                    txtGroup2.Text = ds.Tables[0].Rows[0]["GroupName2"].ToString();
                    txtGroup3.Text = ds.Tables[0].Rows[0]["GroupName3"].ToString();
                    txtGroup4.Text = ds.Tables[0].Rows[0]["GroupName4"].ToString();
                    txtGroup5.Text = ds.Tables[0].Rows[0]["GroupName5"].ToString();
                    txtGroup6.Text = ds.Tables[0].Rows[0]["GroupName6"].ToString();
                    txtGroup7.Text = ds.Tables[0].Rows[0]["GroupName7"].ToString();
                    txtGroup8.Text = ds.Tables[0].Rows[0]["GroupName8"].ToString();
                    ddlItem1.SelectedValue = ds.Tables[0].Rows[0]["ItemID1"].ToString();
                    ddlItem2.SelectedValue = ds.Tables[0].Rows[0]["ItemID2"].ToString();
                    ddlItem3.SelectedValue = ds.Tables[0].Rows[0]["ItemID3"].ToString();
                    ddlItem4.SelectedValue = ds.Tables[0].Rows[0]["ItemID4"].ToString();
                    ddlItem5.SelectedValue = ds.Tables[0].Rows[0]["ItemID5"].ToString();
                    ddlItem6.SelectedValue = ds.Tables[0].Rows[0]["ItemID6"].ToString();
                    ddlItem7.SelectedValue = ds.Tables[0].Rows[0]["ItemID7"].ToString();
                    ddlItem8.SelectedValue = ds.Tables[0].Rows[0]["ItemID8"].ToString();
                    txtDesc1.Text = ds.Tables[0].Rows[0]["Desc1"].ToString();
                    txtDesc2.Text = ds.Tables[0].Rows[0]["Desc2"].ToString();
                    txtDesc3.Text = ds.Tables[0].Rows[0]["Desc3"].ToString();
                    txtDesc4.Text = ds.Tables[0].Rows[0]["Desc4"].ToString();
                    txtDesc5.Text = ds.Tables[0].Rows[0]["Desc5"].ToString();
                    txtDesc6.Text = ds.Tables[0].Rows[0]["Desc6"].ToString();
                    txtDesc7.Text = ds.Tables[0].Rows[0]["Desc7"].ToString();
                    txtDesc8.Text = ds.Tables[0].Rows[0]["Desc8"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string res = obj_BLL.SaveData(nID, txtGroup1.Text, txtGroup2.Text, txtGroup3.Text, txtGroup4.Text, txtGroup5.Text, txtGroup6.Text, txtGroup7.Text,txtGroup8.Text,ddlItem1.SelectedValue,
                    ddlItem2.SelectedValue, ddlItem3.SelectedValue, ddlItem4.SelectedValue, ddlItem5.SelectedValue, ddlItem6.SelectedValue, ddlItem7.SelectedValue, ddlItem8.SelectedValue,
                    txtDesc1.Text, txtDesc2.Text, txtDesc3.Text, txtDesc4.Text, txtDesc5.Text, txtDesc6.Text, txtDesc7.Text, txtDesc8.Text, hidUserID.Value);
                if (res == "0" || res == "")
                {
                    lblMessage.Text = "Error while Saving";
                    txtGroup1.Focus();
                }                
                else
                {
                    lblMessage.Text = "Record Saved successfully.";
                    ClearFields();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            lblMessage.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string res = obj_BLL.DeleteData(nID);
                if (res == "" || res == "0")
                {
                    lblMessage.Text = "Error While Deleting";
                    return;

                }
                if (res == "1")
                {
                    lblMessage.Text = "Record Deleted Successfully";
                    ClearFields();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
