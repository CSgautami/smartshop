﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportStockMovement.aspx.cs" Inherits="MyAccounts20.ReportStockMovement" Title="Stock Movement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/Reports/Reports.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">    
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
     function btnShow_Click()
    {        
        var fromdt=document.getElementById(preid+"txtFromDate").value;
        var todt=document.getElementById(preid+"txtToDate").value;
        var StockGroup=document.getElementById("ddlStcokGroup").value;        
        var Company=document.getElementById("ddlCompany").value;
        var All=document.getElementById(preid+"chkAll").checked;
        window.frames["frmStockMovement"].location.href="Reports/StockMovementReport.aspx?FromDate="+fromdt+"&ToDate="+todt+"&StockGroup="+StockGroup+"&Company="+Company+"&All="+All+"&BranchID="+ ddlBranch.value
        ddlBranch.focus();        
    }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">         
            <tr>
                <td width="100%" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>
                                        <td align ="left">Branch :</td>
                                        <td align="left">
                                            <select id="ddlBranch" style="width:200px;"></select>
                                        </td>
                                        <%--<td>
                                            Head Group:
                                        </td>
                                        <td>
                                            <select id="ddlHG" style="width:250px;"  onchange="GetStockGroup();"></select>
                                        </td>    --%>
                                        <td>
                                            Stock Group:
                                        </td>
                                        <td>
                                            <select id="ddlStcokGroup" style="width:200px;"/>
                                        </td>                                        
                                        <td align="left">
                                            Company:
                                        </td>
                                        <td>
                                            <select id="ddlCompany" style="width:200px;"/>
                                        </td> 
                                     </tr>                                    
                                    <tr>                                                     
                                        <td align="left">
                                            <span id="spnFromDate">From Date:</span>                                                                                     
                                        </td>                                        
                                        <td align ="left">
                                            <input type="text" id="txtFromDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>
                                        <td align="left">
                                            <span id="spnToDate" >To Date:</span>
                                        </td>
                                        <td align="left">
                                            <input type="text" id="txtToDate" runat="server" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />                                                                                    
                                        </td>
                                         <td align="left">
                                            <asp:CheckBox ID="chkAll" Text="All" runat="server" onclick="chkAll_Click(event,this);" />
                                        </td>                                                                                                                      
                                        <td align="right">
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmStockMovement" id="frmStockMovement"
                                    style="width: 100%; height: 400px;"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
    <input type="hidden" runat="server" id="hidUserID" />
    <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
