﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true"
    CodeBehind="AreaCenters.aspx.cs" Inherits="MyAccounts20.AreaCenters" Title="Area Centers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table align="center" style="width: 100%;">
            <tr>
                <td align="left">
                    <asp:DataList ID="dlArea" runat="server" RepeatColumns="4">
                        <ItemTemplate>                                                     
                            <div >                                            
                                <div id="homeshoptitle"><%#Eval("SMName") %></div>
                                
                                <div id="homeshopdetails"> <p><%#Eval("Address")%></p></div>
                            </div>                            
                        </ItemTemplate>
                    </asp:DataList>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
