﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="VatType.aspx.cs" Inherits="MyAccounts20.VatType" Title="Vat Type" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript" type="text/javascript">
        var preid = 'ctl00_ContentPlaceHolder1_';
        String.prototype.trim=function() {
          return this.replace(/^\s*/, "").replace(/\s*$/, "");
          }
          function getXml(xmlString) {
            xmlObj=null;
            try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
                }
                else {
                    xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
                }

            }
            catch (err) {
            }    
        }
          function ValidateSave()
          {
              try
              {
                  var VatType = document.getElementById(preid + "txtVatType");
                 if(VatType.value.trim()=="")
                 {
                     alert("Enter VatType");
                     VatType.focus();
                     return false;
              }
              return true;
               }
              catch(err)
               {
              return false;
               }
             }
             function ConfirmEdit()
                {
                if(!confirm("Do you want to Edit?"))
                return false;
                }
                function ConfirmDelete()
                {
                if(! confirm("Do you want to Delete?"))
                return false;
                }
                
                </script>
                <table align="center" width="100%">
                <tr>
                <td colspan="2" align="center">
                &nbsp;
                </td>
                </tr>
                        <tr>
                            <td colspan= "2" align="center" >
                                    <asp:GridView ID="gvVatType" runat="server" AutoGenerateColumns="false" CssClass="grid"
                                     AllowPaging="true" Font-Size="Small" OnPageIndexChanging="gvVatType_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="VAT Type Description">
                                            <ItemTemplate>
                                                <input type="hidden" runat="server" id="hidVTypeID" value='<%#Eval("VatTypeID")%>' />
                                                <asp:Label ID="lblVTypeDesc" runat="server" Text='<%#Eval("VatTypeDesc") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" OnClientClick="return ConfirmEdit();"
                                                    OnClick="lbtnEdit_Click"></asp:LinkButton>
                                                <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClientClick="return ConfirmDelete();"
                                                    OnClick="lbtnDelete_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                             </td>
                        </tr>
                   
                       <tr>
                       <td align="right">
                       Vat Type Description:
                       </td>
                       <td align="left">
                        <asp:TextBox ID="txtVatType" runat="server" Width="200px"></asp:TextBox>
                       </td>
                       </tr>
                         <tr>
                        
                            <td colspan="2" align="center">
                                <table>
                                    <tr>
                                   
                                        <td>
                                             <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="return ValidateSave();"
                                                OnClick="btnSave_Click" />
                                        </td>
                                        <td>
                                             <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClientClick="return ValidateSave();"
                                                OnClick="btnUpdate_Click" Visible="false" />
                                        </td>
                                        <td runat="server" id="tdCancel">
                                             <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                </table>    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="100%" align="center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>                       
                    </table>
                     <input type="hidden" runat="server" id="hidVTypeID" />
                     <input type="hidden" runat="server" id="hidUserID" />

</asp:Content>
