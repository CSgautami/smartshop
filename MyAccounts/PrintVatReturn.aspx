﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintVatReturn.aspx.cs" Inherits="MyAccounts20.PrintVatReturn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>VatReturn</title>
</head>
<body>
    <form id="form1" runat="server">
     <div>
        <table width="700px" align="center" style="font-size:small;">
            <tr>
                <td style="width: 50%;">
                    &nbsp;
                </td>
                <td style="width: 10%;">
                    Circle
                </td>
                <td align="center" style="width: 2%;">
                    :
                </td>
                <td id="tdCircle" runat="server" style="width: 38;">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    Division
                </td>
                <td align="center">
                    :
                </td>
                <td id="tdDivision" runat="server">
                </td>
            </tr>
            <tr>
                <td style="width: 100%; font-weight: bold;" align="center" colspan="4">
                    FORM VAT 200
                </td>
            </tr>
            <tr>
                <td style="width: 100%; font-weight: bold;" align="center" colspan="4">
                    MONTHLY RETURN FOR VALUE ADDED TAX
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" align="center" colspan="4">
                    [See Rule 23(1)]
                </td>
            </tr>
            <tr>
                <td style="width: 100%; font-weight: bold;" align="right" runat="server" id="tdCompany" colspan="4">
                </td>
            </tr>
            <tr>
                <td style="width: 50%" runat="server" id="tdTest">
                    <table align="center" border="1" width="100%" style="height: 77px;">
                        <tr>
                            <td style="width: 10px;">
                                01.
                            </td>
                            <td colspan="10" align="center">
                                TIN
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin1">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin2">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin3">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin4">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin5">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin6">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin7">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin8">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin9">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin10">
                            </td>
                            <td style="width: 10px;" runat="server" align="center" id="tdTin11">
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 50%" colspan="3">
                    <table align="center" border="1" width="100%">
                        <tr>
                            <td>
                                02.
                            </td>
                            <td colspan="7" align="center">
                                Period covered by this Return
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" valign="top">
                                From
                            </td>
                            <td>
                                DD
                            </td>
                            <td>
                                MM
                            </td>
                            <td>
                                YY
                            </td>
                            <td rowspan="2" valign="top">
                                To
                            </td>
                            <td>
                                DD
                            </td>
                            <td>
                                MM
                            </td>
                            <td>
                                YY
                            </td>
                        </tr>
                        <tr>
                            <td runat="server" align="center" id="tdFDate">
                            </td>
                            <td runat="server" align="center" id="tdFMonth">
                            </td>
                            <td runat="server" align="center" id="tdFYear">
                            </td>
                            <td runat="server" align="center" id="tdTDate">
                            </td>
                            <td runat="server" align="center" id="tdTMonth">
                            </td>
                            <td runat="server" align="center" id="tdTYear">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <table align="center" width="100%" style="border: solid 1px;">
                        <tr>
                            <td rowspan="2" valign="top">
                                03.
                            </td>
                            <td>
                                Name of Enterprises
                            </td>
                            <td runat="server" id="tdName" style="width: 50%;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address
                            </td>
                            <td runat="server" id="tdAddress" rowspan="2" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email ID :
                            </td>
                            <td runat="server" id="tdEmailID">
                            </td>
                            <td>
                                Phone No :
                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <table align="center" width="100%">
                        <tr>
                            <td style="width: 60%">
                                If you have made no purchase and no sales cross this box
                            </td>
                            <td style="border: solid 1px;">
                                04.
                            </td>
                            <td runat="server" id="tdbox4" style="width: 35%; border: solid 1px;" align="center">
                            </td>
                        </tr>
                        <tr style="height: 2px;">
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;font-weight: bold;" colspan="3">
                                If you have no entry for a box, Insert “NIL”. Do not leave any box blank unless
                                you cross box 04.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Input tax credit from previous month
                            </td>
                            <td style="border: solid 1px;">
                                05.
                            </td>
                            <td style="border: solid 1px;" runat="server" id="tdbox5" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                (Box 24 or 24(b) of your previous tax return
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <table align="center" width="100%">
                        <tr>
                            <td style="font-weight: bold;">
                                PURCHASES IN THE MONTH (INPUT)
                            </td>
                            <td>
                                Value Excluding VAT (A)
                            </td>
                            <td>
                                VAT Claimed (B)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <table align="center" width="100%" border="1">
                        <tr>
                            <td style="width: 5%;">
                                06.
                            </td>
                            <td style="width: 45%;">
                                Exempt or non creditable purchases
                            </td>
                            <td style="width: 5%;">
                                Rs.
                            </td>
                            <td runat="server" id="tdbox6a" style="width: 20%;" align="right">
                            </td>
                            <td style="width: 5%;">
                                &nbsp;
                            </td>
                            <td style="width: 20%;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                07.
                            </td>
                            <td>
                                5% Rate Purchases
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox7a" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox7b" align="right">
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                08.
                            </td>
                            <td>
                                14.5% Rate Purchases
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox8a1" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox8b1" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                09.
                            </td>
                            <td>
                                1% Rate Purchases
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox9a" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox9b" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                10.
                            </td>
                            <td>
                                Special Rate Purchases
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox10a" align="right">
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                11.
                            </td>
                            <td>
                                Total Amount of input tax ( 5+7(B)+8(B)+9(B))
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox11a" align="right">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <table align="center" width="100%">
                        <tr>
                            <td style="font-weight: bold;">
                                SALES IN THE MONTH (OUTPUT)
                            </td>
                            <td>
                                Value Excluding VAT (A)
                            </td>
                            <td>
                                VAT Due (B)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%;" colspan="4">
                    <table align="center" width="100%" border="1">
                        <tr>
                            <td style="width: 5%;">
                                12.
                            </td>
                            <td style="width: 45%;">
                                Exempt Sales
                            </td>
                            <td style="width: 5%;">
                                Rs.
                            </td>
                            <td runat="server" id="tdbox12a" style="width: 20%;" align="right">
                            </td>
                            <td style="width: 5%;">
                                &nbsp;
                            </td>
                            <td style="width: 20%;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                13.
                            </td>
                            <td>
                                Zero Rate Sales – International Exports
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox13a" align="right">
                            </td>
                            <td style="width: 5%;">
                                &nbsp;
                            </td>
                            <td style="width: 20%;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                14.
                            </td>
                            <td>
                                Zero Rate Sales – Others ( CST Sales )
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox14a" align="right">
                            </td>
                            <td style="width: 5%;">
                                &nbsp;
                            </td>
                            <td style="width: 20%;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                15.
                            </td>
                            <td>
                                Tax Due on purchase of goods
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox15a" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox15b" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                16.
                            </td>
                            <td>
                                5% Rate Sales
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox16a" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox16b" align="right">
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                17.
                            </td>
                            <td>
                                14.5% Rate Sales
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox17a1" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox17b1" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                18.
                            </td>
                            <td>
                                Special Rate Sales
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox18a" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox18b" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                19.
                            </td>
                            <td>
                                1% Rate Sales
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox19a" align="right">
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox19b" align="right">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                20.
                            </td>
                            <td>
                                Total amount of output tax ( 15(B)+16(B)+17(B)+19(B) )
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                Rs.
                            </td>
                            <td runat="server" id="tdbox20b" align="right">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                  <tr>

                <td>

                    &nbsp;

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%">

                        <tr>

                            <td style="width: 5%;">

                                21.

                            </td>

                            <td style="width: 60%">

                                If Total of box 20 exceeds box 11 pay this amount

                            </td>

                            <td style="border: solid 1px;">

                                Rs.

                            </td>

                            <td runat="server" id="tdbox21" style="width: 35%; border: solid 1px;" align="right">

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

            <tr>

                <td>

                    &nbsp;

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%">

                        <tr>

                            <td style="width: 5%;">

                                22.

                            </td>

                            <td style="width: 60%">

                                Payment / Adjustment details

                            </td>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

            <tr>
                <td style="width: 100%;" colspan="4">
                    <div id="divPaymentDet" runat="server" width="100%">
                    </div>
                </td>
            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%">

                        <tr>

                            <td style="width: 7%;">

                                22(a).

                            </td>

                            <td style="width: 60%">

                                Adjustment Details:

                            </td>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <div id="divAdjDet" runat="server" width="100%">

                    </div>

                </td>

            </tr>

            <tr>

                <td>

                    &nbsp;

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If total of box 11 exceeds total of box 20 (or the

                    payment and adjustment in boxes 22 and 22 (a) put together exceed the tax due on

                    box 21) and you have declared exports in box 13 (A) and not adjusting the excess

                    amount against tax liability if any under the CST Act, you can claim a refund in

                    box 23 or carry a credit forward in box 24.

                </td>

            </tr>

            <tr>

                <td>

                    &nbsp;

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If you have declared no exports in box 13 (A) you

                    must carry the credit forward in box 24,unless you have carried forward a tax credit

                    and not adjusting the excess amount against the tax liability if any under the CST

                    Act.

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%">

                        <tr>

                            <td style="width: 8%;">

                                Refund

                            </td>

                            <td style="width: 5%; border: solid 1px;">

                                23.

                            </td>

                            <td style="width: 5%; border: solid 1px;">

                                Rs.

                            </td>

                            <td id="tdbox23" runat="server" style="width: 20%; border: solid 1px;" align="right">

                            </td>

                            <td style="width: 20%">

                                Credit carried forward

                            </td>

                            <td style="width: 7%; border: solid 1px;">

                                24.

                            </td>

                            <td style="width: 5%; border: solid 1px;">

                                Rs.

                            </td>

                            <td runat="server" id="tdbox24" style="width: 20%; border: solid 1px;" align="right">

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                24(a).

                            </td>

                            <td colspan="4" rowspan="3">

                                If you want to adjust the amount against the liability under the CST Act please

                                fill in box 24 (a) and 24 (b) Tax Due under the CST Act and adjusted against the

                                excess amount in box 24.

                            </td>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                            <td style="border: solid 1px;">

                                24(a).

                            </td>

                            <td style="border: solid 1px;">

                                Rs.

                            </td>

                            <td runat="server" id="tdbox24a" style="border: solid 1px;" align="right">

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                24(b).

                            </td>

                            <td colspan="4" rowspan="3">

                                Net credit carried forward

                            </td>

                            <td style="border: solid 1px;">

                                24(b).

                            </td>

                            <td style="border: solid 1px;">

                                Rs.

                            </td>

                            <td runat="server" id="tdbox24b" style="border: solid 1px;" align="right">

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%" style="border: solid 1px;">

                        <tr>

                            <td style="font-weight: bold;" colspan="2">

                                DECLARATION:

                            </td>

                        </tr>

                        <tr>

                            <td style="width: 7%;">

                                25.

                            </td>

                            <td style="width: 10%;">

                                Name

                            </td>

                            <td runat="server" id="tdMdName" style="width: 50%;">

                            </td>

                            <td style="width: 20%;">

                                &nbsp;

                            </td>

                            <td style="width: 10%;">

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                Being (title)

                            </td>

                            <td id="tdMdDisg" runat="server">

                            </td>

                            <td colspan="2">

                                of the above enterprise do hereby

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                            <td colspan="4">

                                declare that the information given in this return is true and correct.

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td colspan="2">

                                Signature & Stamp

                            </td>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                Date of Declaration

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%">

                        <tr>

                            <td style="font-weight: bold;">

                                Please Note:

                            </td>

                        </tr>

                        <tr>

                            <td>

                                1. This return and payment must be presented on or before 20th day of the following

                                month mentioned in box 02.

                            </td>

                        </tr>

                        <tr>

                            <td>

                                2. In case the payment is made by a challan in the bank, please enclose a copy of

                                the same.

                            </td>

                        </tr>

                        <tr>

                            <td>

                                3. You will be, as per provisions of the APVAT Act, 2005, subject to penalties if

                                you :

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                - &nbsp;&nbsp;&nbsp; Fail to file the VAT Return at the Local Tax Office even if

                                it is a nil return.

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                - &nbsp;&nbsp;&nbsp; Make a late payment of tax

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                - &nbsp;&nbsp;&nbsp; Make a false declaration

                            </td>

                        </tr>

                       

                    </table>

                </td>

            </tr>

            <tr>

                <td style="width: 100%;" colspan="4">

                    <table align="center" width="100%" style="border: solid 1px;">

                        <tr>

                            <td colspan="2" align="center" style="font-weight: bold;">

                                FOR OFFICIAL USE ONLY:

                            </td>

                        </tr>

                        <tr>

                            <td style="width: 50%">

                                &nbsp;

                            </td>

                            <td style="width: 50%">

                                &nbsp;

                            </td>

                        </tr>

                     

                        <tr>

                            <td>

                                Date of Receipt

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                           

                        </tr>

                      

                        <tr>

                            <td>

                                Amount of Tax Paid Rs.

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                       

                        <tr>

                            <td>

                                Mode of Payment

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                            <td>

                                &nbsp;

                            </td>

                        </tr>

                        <tr>

                            <td>

                                &nbsp;

                            </td>

                            <td align="right">

                                Signature of Receiving Officer with Stamp

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

        </table>
    </div>
    </form>
</body>
</html>
