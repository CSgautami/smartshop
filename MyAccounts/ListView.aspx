﻿<%@ Page Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ListView.aspx.cs" Inherits="MyAccounts20.ListView" Title="ListView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="main1">
        <div>
            <div class="salwar">
                <div class="totalsalwar">
                    <span style="padding: 0px 0px 0px 8px;">
                        <asp:Label ID="lblHeadGroup" runat="server" Style="align: left"></asp:Label></span></div>
                <div class="listview">
                    <a href="#">List View</a></div>
                <div class="gridview">
                    <a href="#">Grid View</a></div>
            </div>
        </div>
        <%-- <asp:GridView ID="gvItemDet" runat="server" AllowPaging="true" PageSize="20" ></asp:GridView>--%>
        <asp:DataList ID="dlItemDet" runat="server" CellPadding="0" CellSpacing="0">
            <ItemTemplate>
                <%-- <asp:Repeater ID="rpItemDet" runat="server"></asp:Repeater>--%>
                <div class="product_bg">
                    <div class="product_top_div">
                        <div class="product_coad">
                         <span>
                                <asp:Label ID="lblItemCode" runat="server" Text='<%#Eval("ItemCode") %>'></asp:Label>
                           </span>
                        </div>
                        <div class="product_name_main">
                            <div class="product_name1">
                                <asp:Label ID="lblItemName" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                            </div>
                            <div class="product_name1">
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                            </div>
                        </div>                       
                    </div>
                    <div class="product_bottom_div">
                        <div class="product_img">
                            <img src="ItemImages/<%#Eval("SiNo")%>.jpg" width="96" height="96" style="border:#cccccc solid 1px;" />
                        </div>
                        <div class="product_cost">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tbody>
                              <tr>                                
                                <td width="45%" height="35" class="product_cost1">Market price / M.R.P</td>  
                                <td width="12%" style="text-align:center;">:</td>
                                <td width="21%" class="product_rs">Rs.<asp:Label ID="lblMrp" runat="server" Text='<%#Eval("MRP") %>'></asp:Label>
                               </td>                              
                               <td> 
                               <div class="product_off">
                               <asp:Label ID="lblPer" runat="server" Text='<%#Eval("Percentage") %>'></asp:Label>
                               </div>
                               </td>
                              </tr>
                              <tr>                               
                                <td class="product_cost1" style="height:35;">Our Price </td>
                                <td width="12%" style="text-align:center;">:</td>
                                <td class="product_rs1">Rs.<asp:Label ID="lblPrice" runat="server" Text='<%#Eval("OSP") %>'></asp:Label>
                               </tr>                              
                              <tr>
                                <td style="height:35;" class="product_cost1">Availability</td>
                                    <td width="12%" style="text-align:center">: </td>
                                <td><img src="MasterPageFiles2/images/no.png" width="16" height="16" border="0"></td>
                                </tr>
                            </tbody></table>
                        </div>
                        <div class="product_apply">
                            <div class="quantity_text">Quantity</div>
                            <div align="center" style="margin-bottom:10px;"><input type="text" class="input_style"  id="<%#Eval("SiNo")%>txtQty" runat="server" value=""/></div>
                            <div class="addtocart" style="height:23px;">                                                                                                                                                            
                            <asp:ImageButton ID="btnCheckOut" runat="server" width="100" height="25" ImageAlign="Left" ImageUrl="MasterPageFiles2/images/checkout.png"                              
                             onclick="btnCheckOut_Click" class="addtocart_text"></asp:ImageButton>
                            
                            </div>    
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:DataList>
         <table align="center">
            <tr>
                <td>
                    <DataList:Pager ID="dlPager" runat="server" OnPageIndexChanged="dlPager_PageIndexChagned"/>                   
                    
                </td>
            </tr>
        </table>
    </div>
    <div>
        <input type="hidden" id="hidUserID" runat="server" />
        <input type="hidden" id="hidTempID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>








0