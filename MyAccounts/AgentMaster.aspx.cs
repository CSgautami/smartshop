﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;

namespace MyAccounts20
{
    public partial class AgentMaster : System.Web.UI.Page
    {
        RegistrationDet_BLL Obj_BLL = new RegistrationDet_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                hidAgentID.Value  = "-1";
                FillGrid();
            }

        }
        private void FillGrid()
        {
            try
            {                
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetAgent();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvAgent.DataSource = ds;
                        gvAgent.DataBind();
                        gvAgent.Rows[0].Cells[1].Text = "";
                    }
                    else
                    {
                        gvAgent.DataSource = ds;
                        gvAgent.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertAgentDet(hidAgentID.Value, txtAgentCode.Text,txtName.Text,txtAddress.Text,txtContactNo.Text,txtEmailID.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Group";

            }
            else if (res == "-1")
            {
                lblMsg.Text = "Agent Code already existed";
                txtAgentCode.Focus();

            }
            else if (res == "1")
            {
                lblMsg.Text = "Agent saved successfully";
                FillGrid();
                btnCancel_Click(sender, e);

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvAgent.PageIndex = 0;
            txtName.Text = "";
            txtAgentCode.Text = "";
            txtEmailID.Text = "";
            txtContactNo.Text = "";
            txtAddress.Text = "";
            hidAgentID.Value = "-1";
            lblMsg.Text = "";
            FillGrid();
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidAgentID.Value = ((HtmlInputHidden)grow.FindControl("hidAgentID")).Value;
                txtAgentCode.Text = ((Label)grow.FindControl("lblAgent")).Text;
                txtName.Text = ((Label)grow.FindControl("lblName")).Text;
                txtAddress.Text = ((Label)grow.FindControl("lblAddress")).Text;
                txtContactNo.Text = ((Label)grow.FindControl("lblContactNo")).Text;
                txtEmailID.Text = ((Label)grow.FindControl("lblEmail")).Text;
                txtAgentCode.Focus();                
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidAgentID.Value = ((HtmlInputHidden)grow.FindControl("hidAgentID")).Value;
                HeadGroup_BLL Obj_BLL = new HeadGroup_BLL();
                string res = Obj_BLL.DeleteHeadGroup(hidAgentID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Agent";
                    return;
                }
                if (res == "1")
                {
                    lblMsg.Text = "Agent Deleted Successfully";
                    btnCancel_Click(sender, e);
                    FillGrid();
                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        protected void gvAgent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAgent.PageIndex = e.NewPageIndex;
            FillGrid();
        }
    }
}
