﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" AutoEventWireup="true" CodeBehind="ReportRegisteredMembersList.aspx.cs" Inherits="MyAccounts20.ReportRegisteredMembersList" Title="Registered Members List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript">        
    var preid;
    var ddlArea;
    var dtpFrom;
    var dtpTo;
    document.write(getCalendarStyles());
    var cal=new CalendarPopup("divCalendar");
    cal.showNavigationDropdowns();
    function showcalendar(t)
    {
        cal.select(t,t.id,'dd-MM-yyyy');
    }
    preid ='ctl00_ContentPlaceHolder1_'
    function GetControls()
    {
        ddlArea=document.getElementById("ddlArea");  
        dtpFrom=document.getElementById(preid+"txtFromDate");
        dtpTo=document.getElementById(preid+"txtToDate");              
    }
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function GetArea()
    {        
        PageMethods.GetArea(GetAreaComplete);    
    }

    function GetAreaComplete(res)
    {
        getXml(res);    
        ddlArea.options.length=0;
        var opt=document.createElement("OPTION");
        opt.text="All";
        opt.value="0";    
        ddlArea.options.add(opt);
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("AreaCentre")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("AreaID")[0].firstChild.nodeValue;
            ddlArea.options.add(opt);
        }    
    }        
    function SetData()
    {    
        GetControls();
        GetArea();   
        ddlArea.focus();  
    }
    function btnShow_Click()
    {       
        window.frames["frmRegisteredMembersList"].location.href="Reports/RegisteredMembersListReport.aspx?AreaID="+ddlArea.value+"&FromDate="+dtpFrom.value+"&ToDate="+dtpTo.value;
    }          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px; border:solid 1px black;">
             <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top" align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>                                        
                                        <td align="left">
                                            <span id="spnFromDate">From Date:</span>                                                                                     
                                        </td>                                        
                                        <td align ="left">
                                            <input type="text" runat="server" id="txtFromDate" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />
                                        </td>
                                        <td align="left">
                                            <span id="spnToDate" >To Date:</span>
                                        </td>
                                        <td align="left">
                                            <input type="text" runat="server" id="txtToDate" maxlength="10" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;" />                                                                                    
                                        </td>                                                                                                                     
                                        <td align="left">
                                            Area
                                        </td>
                                        <td>
                                            <select id="ddlArea" style="width: 200px;">
                                            </select>
                                        </td>
                                        <td>
                                            <input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />
                                        </td>                                        
                                    </tr>             
                                                                                               
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmRegisteredMembersList" id="frmRegisteredMembersList" style="width: 100%; height: 400px;"
                                    frameborder="0" scrolling="auto"></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>    
     <div id="divCalendar" class="Calendar">
    </div>
</asp:Content>
