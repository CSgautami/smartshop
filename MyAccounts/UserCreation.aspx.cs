﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;


namespace MyAccounts20
{
    public partial class UserCreation : GlobalPage
    {
        UserCreation_BLL Obj_BLL = new UserCreation_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    GetDefaults();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        void GetDefaults()
        {
            try
            {
                UserCreation_BLL Obj_BLL = new UserCreation_BLL();
                ddlBranch.DataSource = Obj_BLL.GetBranch();
                ddlBranch.DataTextField = "BranchName";
                ddlBranch.DataValueField = "BranchID";
                ddlBranch.DataBind();
                ddlBranch.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CreateUser(string UserName, string Password, string BranchID)
        {

            try
            {
                UserCreation_BLL Obj_BLL = new UserCreation_BLL();
                return Obj_BLL.CreateUser(UserName, Password,BranchID);

            }
            catch { throw; }

        }
    }
}

    


        
    
