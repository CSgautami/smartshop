﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataListPager.ascx.cs"
    Inherits="MyAccounts20.DataListPager" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<link href="style.css" rel="stylesheet" type="text/css" />--%>
<style type="text/css">
    .dpnav2
    {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        font-weight: bold;
        color: white;
        text-decoration: none;
        padding-left: 2px;
    }
    .pageinfo2
    {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        font-weight: bold;
        color: red;
        text-decoration: none;
        padding-left: 2px;
    }



</style>
<script language="javascript" type="text/javascript">
    function ValidatinGO()
    {
        var preid="ctl00_SuperSareeContent_dlPager_";
        var GOTO=document.getElementById(preid+"txtGoTo");
        
        if(GOTO.value=="" ||isNaN(GOTO.value) || GOTO.value<=0 )
        {
            alert("Enter Valid Numbers Only");
            GOTO.focus();
            return false;
        }
        
    }
        function NubersOnly(evt)
        {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        {
        return false;
        document.getElementById(preid+"txtGoTo").value="";;
        }
        else
        return true;
        }
       
</script>

<table cellpadding="0" cellspacing="0" id="tblPager" runat="server">
    <tr>
        <td class="dpnav2">
            <asp:LinkButton ID="btnFirstRecord" runat="server" Text="FIRST" OnClick="btnFirstRecord_Click" ForeColor="#1570a2"
                ></asp:LinkButton>
        </td>
        <td>
            |
        </td>
        <td class="dpnav2">
            <asp:LinkButton ID="btnPrevious" runat="server" Text="PREV" OnClick="btnPrevious_Click" ForeColor="#1570a2"
                ></asp:LinkButton>
        </td>
        <td>
            |
        </td>
        <td class="dpnav2">
            <asp:Label ID="lblCurrentPage" runat="server" Text="Label" CssClass="pageinfo2"></asp:Label>
        </td>
        <td>
            |
        </td>
        <td class="dpnav2">
            <asp:LinkButton ID="btnNext" runat="server" Text="NEXT" OnClick="btnNext_Click"  ForeColor="#1570a2"> </asp:LinkButton>
        </td>
        <td>
            |
        </td>
        <td class="dpnav2">
            <asp:LinkButton ID="btnLastRecord" runat="server" Text="LAST" OnClick="btnLastRecord_Click" ForeColor="#1570a2"></asp:LinkButton>
        </td>
        <td>
            <table>
                <tr>
                    <td class="dpnav2">
                        - GO TO
                    </td>
                    <td>
                        <asp:TextBox ID="txtGoTo" runat="server" Style="width: 30px; height: 12px;"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGoTo" runat="server" Text="GO" Height="20px" Font-Size="11px"
                            OnClientClick="return ValidatinGO();" OnClick="btnGoTo_Click" />
                    </td>
                    <td>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtGoTo"
                            ValidChars="1,2,3,4,5,6,7,8,9,0">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
