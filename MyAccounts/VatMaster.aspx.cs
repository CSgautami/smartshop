﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class VatMaster : System.Web.UI.Page
    {
        
        VatMaster_BLL Obj_BLL=new VatMaster_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            string strID = Request.QueryString["ID"];
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
                if (strID != null && strID != "")
                {                 
                    tdUpdate.Visible=false;
                    tdCancel.Visible=false;
                    strID = "";
                }
            }              

        }


        private void FillGrid()
        {
            try
            {
                VatMaster_BLL  Obj_BLL = new VatMaster_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetVatMaster();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvVat.DataSource = ds;
                        gvVat.DataBind();
                        gvVat.Rows[0].Cells[5].Text = "";
                    }
                    else
                    {
                        gvVat.DataSource = ds;
                        gvVat.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertVatMaster(txtVatDesc.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving VatMaster";


            }
            else if (res == "-1")
            {
                lblMsg.Text = "VatMaster already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "VatMaster saved successfully";
                FillGrid();
                txtVatDesc.Text = "";
                txtVatDesc.Focus();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateVatMaster(hidVatID.Value,txtVatDesc.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating VatMaster";

            }
            else if (res == "-1")
            {
                lblMsg.Text = "VatMaster already existed";

            }
            else if (res == "1")
            {
                lblMsg.Text = "VatMaster Updated successfully";
                FillGrid();
                txtVatDesc.Text = "";
                txtVatDesc.Focus();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvVat.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtVatDesc.Text = "";
            lblMsg.Text = "";
            
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidVatID.Value = ((HtmlInputHidden)grow.FindControl("hidVatID")).Value;
                txtVatDesc.Text = ((Label)grow.FindControl("lblVatDesc")).Text;
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
          public void lbtnDelete_Click(object sender, EventArgs e)
            {
                try
                {
                    LinkButton lbtn = (LinkButton)sender;
                    GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                    hidVatID.Value = ((HtmlInputHidden)grow.FindControl("hidVatID")).Value;
                    VatMaster_BLL Obj_BLL = new VatMaster_BLL();
                    string res = Obj_BLL.DeleteVatMaster(hidVatID.Value);
                    if (res == "" || res == "0")
                    {
                        lblMsg.Text = "Error While Deleting VatMaster";
                        //return;

                    }
                    if (res == "1")
                    {
                        lblMsg.Text = "VatMaster Deleted Successfully";
                        txtVatDesc.Text = "";
                        txtVatDesc.Focus();
                        btnSave.Visible = true;
                        btnUpdate.Visible = false;
                        btnCancel.Visible = true;
                        FillGrid();

                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
          protected void gvVat_PageIndexChanging(object sender, GridViewPageEventArgs e)
          {
              gvVat.PageIndex = e.NewPageIndex;
              FillGrid();
          }
          


        }
    }

