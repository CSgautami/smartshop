﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;

namespace MyAccounts20
{
    public partial class HeadGroupMaster : GlobalPage
    {
        HeadGroup_BLL Obj_BLL = new HeadGroup_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                FillGrid();
                

            }
        }

        private void FillGrid()
        {
            try
            {
                HeadGroup_BLL Obj_BLL = new HeadGroup_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetHeadGroup();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvHeadGroup.DataSource = ds;
                        gvHeadGroup.DataBind();
                        gvHeadGroup.Rows[0].Cells[1].Text = "";
                    }
                    else
                    {
                        gvHeadGroup.DataSource = ds;
                        gvHeadGroup.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertHeadGroup(txtHeadGroup.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Group";
                
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
                txtHeadGroup.Focus();
                
            }
            else if (res == "1")
            {
                lblMsg.Text = "Group saved successfully";
                FillGrid();
                txtHeadGroup.Text = "";
                txtHeadGroup.Focus();

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateHeadGroup(hidHeadGroupID.Value, txtHeadGroup.Text,hidUserID.Value);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while Updating Group";
                txtHeadGroup.Focus();
                return;
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Name already existed";
                txtHeadGroup.Focus();
                return;
            }
            else if (res == "1")
            {
                lblMsg.Text = "Group Updated successfully";
                FillGrid();
                txtHeadGroup.Text = "";
                txtHeadGroup.Focus();
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnCancel.Visible = true;
                FillGrid();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvHeadGroup.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtHeadGroup.Text = "";
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidHeadGroupID.Value = ((HtmlInputHidden)grow.FindControl("hidHeadGroupID")).Value;
                txtHeadGroup.Text = ((Label)grow.FindControl("lblHeadGroup")).Text;
                txtHeadGroup.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidHeadGroupID.Value = ((HtmlInputHidden)grow.FindControl("hidHeadGroupID")).Value;
                HeadGroup_BLL Obj_BLL = new HeadGroup_BLL();
                string res = Obj_BLL.DeleteHeadGroup(hidHeadGroupID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Group";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "Group Deleted Successfully";
                    txtHeadGroup.Text = "";
                    txtHeadGroup.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                    FillGrid();

                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        protected void gvHeadGroup_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvHeadGroup.PageIndex = e.NewPageIndex;
            FillGrid();
        }
    }
}
            

    
        

        
        

   


