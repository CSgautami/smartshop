﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL;
using BLL.Reports;
using System.Text;
namespace MyAccounts20
{
    public partial class ReportStockLedger : GlobalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                hidUserID.Value = Session["UserID"].ToString();
                hidGBID.Value = Session["GBID"].ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myscript", "SetData();", true);
                DateTime nFirstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                DateTime nLastDate = new DateTime(nFirstDate.Year, nFirstDate.Month, DateTime.DaysInMonth(nFirstDate.Year, nFirstDate.Month));
                txtFromDate.Value = nFirstDate.ToString("dd-MM-yyyy");
                txtToDate.Value = nLastDate.ToString("dd-MM-yyyy");
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBranch(string UserID)
        {
            try
            {
                Purchase_BLL obj_BLL = new Purchase_BLL();
                return obj_BLL.GetBranch(UserID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetHeadGroup()
        {
            try
            {
                PurchaseReport_BLL obj_BLL = new  PurchaseReport_BLL();
                return obj_BLL.GetHeadGroup().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockGroup()
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetStockGroup().GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetStockItem(string GID)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetStockItem(GID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetBatchNo(string ItemID, string BranchID)
        {
            try
            {
                Sale_BLL obj_BLL = new Sale_BLL ();
                return obj_BLL.GetBatchNo(ItemID,BranchID).GetXml();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string CallFillItem(string prefix)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.CallFillItem(prefix).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [System.Web.Services.WebMethod()]
        [System.Web.Script.Services.ScriptMethod()]
        public static string GetItemName(string ItemID)
        {
            try
            {
                Reports_BLL obj_BLL = new Reports_BLL();
                return obj_BLL.GetItemName(ItemID).GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
