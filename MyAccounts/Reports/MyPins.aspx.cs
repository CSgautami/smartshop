﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.Text;
using System.IO;

namespace MyAccounts20.Reports
{
    public partial class MyPins : System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            tdHeading.InnerText = "My PinNumbers";
            DataSet ds = obj_BLL.GetMyPins(Session["UserID"].ToString());
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center'>");
            if (ds.Tables.Count > 0)
            {
                int count = 0;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (count == 0)
                        sb.Append("<th style='width:250px;'>" + dc.ColumnName + "</th>");
                    //else 
                    //    sb.Append("<th>" + dc.ColumnName + "</th>");
                    count++;
                }
                int cellscount = ds.Tables[0].Columns.Count;
                string EmptyVal = "&nbsp";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sb.Append("<tr>");
                    if (dr[0].ToString() == "")
                              sb.Append("<td>" + EmptyVal + "</td>");
                    else
                    if (dr[1].ToString() == "True")
                    {
                        sb.Append("<td align='center' style=' background-color:Green;color:white'>" + dr[0].ToString() + "</td>");
                    }
                    else
                    {
                        sb.Append("<td align='center' style='background-color:Red;color:white'>" + dr[0].ToString() + "</td>");
                    }
                    //for (int i = 0; i < cellscount; i++)
                    //{
                    //    if (dr[i].ToString() == "")
                    //        sb.Append("<td>" + EmptyVal + "</td>");
                    //    //else if (dr[1].ToString() =="True")
                    //    //{
                    //    //    sb.Append("<td align='center' style='color:Green' >" + dr[0].ToString() + "</td>");
                    //    //}
                    //    //else if(dr[1].ToString() =="False")
                    //    //{
                    //    //    sb.Append("<td align='center' style='color:red' >" + dr[0].ToString() + "</td>");
                    //    //}
                    //    else if (i == 0)
                    //    {

                    //        sb.Append("<td align='right' >" + dr[i].ToString() + "</td>");
                    //    }
                    //    //else
                    //    //{
                    //    //    sb.Append("<td align='center' style='color:Green'>" + dr[i].ToString() + "</td>");
                    //    //}
                    //}
                    sb.Append("</tr>");
                }
                sb.Append("<table>");
                //if (ds.Tables[1].Rows.Count > 0)
                //{
                //    sb.Append("</table>");
                //    sb.Append("<table>");
                //    sb.Append("<tr>");
                //    sb.Append("<td align='left'> Consider:" + ds.Tables[1].Rows[0]["ToConsider"].ToString());
                //    sb.Append("</td>");
                //    sb.Append("</tr>");
                //}
                
                divTable.InnerHtml = sb.ToString();
                //divTable.Visible = false;
                trMenu.Visible = true;

            }
        }
        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }
    }
}
