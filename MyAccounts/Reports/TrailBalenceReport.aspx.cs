﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.Text;
using System.IO;

namespace MyAccounts20.Reports
{
    public partial class TrailBalenceReport : System.Web.UI.Page
    {
        Reports_BLL obj_BLL = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string chkSerialByName = Request.QueryString["bOrderName"];
            string Branch = Request.QueryString["BranchID"];
            string chkGroups = Request.QueryString["bGroup"];
            string chkOpenings = Request.QueryString["bOpenings"];
            string rDebtors = Request.QueryString["bDebitor"];
            string chkLSTGroups = Request.QueryString["strGIDs"];
            string chkDateWise = Request.QueryString["bDtwise"];
            //string rAllLedgers = Request.QueryString["Branch"];
            //string rGroups = Request.QueryString["ToDate"];
            string Date = Request.QueryString["Date"];
            string chkShowDebtors = Request.QueryString["bDebiConde"];
            if(chkDateWise=="true")
                tdHeading.InnerText = "Trail Balance Report On " + Date;
            else
                tdHeading.InnerText = "Trail Balance Report";
            DataSet ds = obj_BLL.GetTrailBalence(chkSerialByName,Branch,chkGroups,chkOpenings,rDebtors,chkLSTGroups,chkDateWise,dateFormat(Date),chkShowDebtors,Session["UserID"].ToString());
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            if (ds.Tables.Count > 0)
            {
                int count = 0;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (count == 0)
                        sb.Append("<th style='Width:50px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 1)
                        sb.Append("<th style='display:none'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count == 2)
                        sb.Append("<th style='Width:300px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count==3)
                        sb.Append("<th style='Width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else if (count==4 || count==5)
                        sb.Append("<th style='Width:150px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                    else 
                        sb.Append("<th>" + dc.ColumnName + "</th>");
                    count++;
                }
                int cellscount = ds.Tables[0].Columns.Count;
                string emptyval = "&nbsp;";
                int Sno;
                count = 1;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {               
                    Sno = count;
                    sb.Append("<tr>");
                    for (int i = 0; i < cellscount; i++)
                    {                        
                        if (dr[i].ToString() == "")
                            sb.Append("<td>" + emptyval + "</td>");
                        else if (i == 0)
                            sb.Append("<td>" + Sno + "</td>");                        
                        else if (i == 1)
                            sb.Append("<td style='display:none'>" + dr[i].ToString() + "</td>");                        
                        else if (i == 4 || i == 5)
                            sb.Append("<td align='right'>" + dr[i].ToString() + "</td>");
                        else
                            sb.Append("<td>" + dr[i].ToString() + "</td>");                        
                    }
                    sb.Append("</tr>");
                    count++;
                }
                sb.Append("</table>");
                divTable.InnerHtml = sb.ToString();
                trMenu.Visible = true;
            }
            else
            {
                lblMsg.Text = "No Data Found";
            }
        }
        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }

        public static string dateFormat(string date)
        {
            string returnva = string.Empty;
            try
            {
                string dd = string.Empty;
                string mm = string.Empty;
                string yy = string.Empty;
                date = date.Replace("-", "/");
                dd = date.Substring(0, date.IndexOf("/"));
                date = date.Substring(date.IndexOf("/") + 1);
                mm = date.Substring(0, date.IndexOf("/"));
                yy = date.Substring(date.IndexOf("/") + 1);
                returnva = mm + "/" + dd + "/" + yy;
            }
            catch
            {
            }
            return returnva;
        }

    }

}

        
    
