﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderReport.aspx.cs" Inherits="MyAccounts20.Reports.OrderReport"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
        var preid='ctl00_ContentPlaceHolder1_';
        function callprint()
        {
            document.getElementById("trMenu").style.display="none";
            window.print();
            document.getElementById("trMenu").style.display="block";
            PageMethods.SaveInProcess(document.getElementById("hidOrderIDs").value,SaveInProcessComplete);
            GetData();
        }
        function SaveInProcessComplete(res)
        {
        }
        function rowclick(ct)
        {        
            var t=ct.parentNode.parentNode;        
            window.showModalDialog("../OrderSummury.aspx?OrderID="+t.cells[0].firstChild.firstChild.nodeValue+"&ID="+1+"&dt="+new Date().getTime(),"","dialogHeight:1000px; dialogWidth:1000px;");
            document.forms[0].submit();  
            GetData();      
        }
        function CancelClick(OrderID)
        {        
            
            PageMethods.CancelClick(OrderID,CancelComplete);
            GetData();      
        }
        function CancelComplete(res)
        {
        }
        function GetData()
        {
            document.getElementById("btnOrderSummary").click();
        }
    </script>
    <style type="text/css">
        th#tdFixedHeader
        {
            position: relative;
            background-color:Gray;
            height:20px;
            color:White;
            top: expression(document.getElementById( "tdFixedHeader" ).parentElement.parentElement.parentElement.parentElement.scrollTop); /*IE5+ only*/
        }
    </style>
</head>
<body>
     
    <form id="form1" runat="server">  
    <asp:ScriptManager ID="SM" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>  
    <div>
        <table width="100%">
            <tr id="trMenu" runat="server" visible="false">
                <td nowrap="nowrap">
                    <img src="../Imgs/print.png" alt="" onclick="callprint();" style="cursor: pointer;" />&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnExport" runat="server" ImageUrl="~/Imgs/excel.png" OnClick="ibtnExport_Click"
                        Style="cursor: pointer;" />                  
                    
                </td>                
                <td style="display:none;">
                    <asp:Button ID="btnOrderSummary" runat="server" Text="Order Summary" 
                        onclick="btnOrderSummary_Click"/>
                </td>
            </tr>
            <tr>
                <td runat="server" id="tdHeading" align="center">
                </td>
            </tr>
            <tr>
                <td style="height: 20px;" align="center">
                    <asp:Label ID="lblMsg" Text="" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div id="divTable" runat="server" style="width: 97%;">
                    </div>
                     <%--<div id="divTable" runat="server" style="width: 97%;">
                        <asp:GridView ID="gvOrder" AutoGenerateColumns="false" runat="server">
                            <Columns>
                                <asp:TemplateField HeaderText="Order No">
                                    <ItemTemplate>
                                        <input type="hidden" id="hidOrderID" runat="server" value='<%#Eval("OID") %>' />
                                        <a href='OrderSummury.aspx?OrderID=<%#Eval("OID")%>&Status=O'><%#Eval("OID")%></a>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>                                    
                                <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOrderDate" Width="150px" runat="server" Text='<%#Eval("Date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" Width="150px" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" Width="100px" runat="server" Text='<%#Eval("NetAmount")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delivered To">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDelivery" Width="200px" runat="server" Text='<%#Eval("ShippingAddress")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                        </asp:GridView>
                    </div>--%>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hidOrderIDs" runat="server" />
    </div>
    </form>
</body>
</html>
