﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL.Reports;
using System.IO;
using System.Text;

namespace MyAccounts20.Reports
{
    public partial class InActiveMembers : System.Web.UI.Page
    {
        Reports_BLL bllRep = new Reports_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            tdHeading.InnerText = "InActive Members List";
            DataSet ds = bllRep.InActiveMembers(Session["UserID"].ToString());
            StringBuilder sb = new StringBuilder();
            sb.Append("<table rules='all' border='1' cellspacing='0' align='center' style='font-size:small'>");
            int count = 0;
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                if (count == 0)
                    sb.Append("<th>" + dc.ColumnName + "</th>");
                else if (count == 1)
                    sb.Append("<th style='width:300px;'id='tdFixedHeader'>" + dc.ColumnName + "</th>");
                //else if (count == 3)
                //    sb.Append("<th style='width:150px;'>" + dc.ColumnName + "</th>");
                //else if (count == 5)
                //    sb.Append("<th style='width:200px;'>" + dc.ColumnName + "</th>");
                //else
                //    sb.Append("<th>" + dc.ColumnName + "</th>");
                count++;
            }
            int cellscount = ds.Tables[0].Columns.Count;
            string emptyval = "&nbsp;";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                sb.Append("<tr>");
                for (int i = 0; i < cellscount; i++)
                {
                    if (i == 0)
                        sb.Append("<td>" + dr[i].ToString() + "</td>");
                    else if (dr[i].ToString() == "")
                        sb.Append("<td>" + emptyval + "</td>");
                    else if (i == 1)
                        sb.Append("<td align='left'>" + dr[i].ToString() + "</td>");
                    else
                        sb.Append("<td>" + dr[i].ToString() + "</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            divTable.InnerHtml = sb.ToString();
            trMenu.Visible = true;
        }
        public void ibtnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string attachment = "attachment; filename=" + tdHeading.InnerText + ".xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                HtmlForm frm = new HtmlForm();
                divTable.Parent.Controls.Add(frm);
                frm.Attributes.Add("runat", "server");
                HtmlGenericControl div = divTable;
                frm.Controls.Add(div);
                frm.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();

            }
            catch
            {

            }
        }
    }
}
