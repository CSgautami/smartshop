﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyAccount.aspx.cs" Inherits="MyAccounts20.Reports.MyAccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My Account</title>

    <script language="javascript" type="text/javascript">
    function callprint()
    {
    document.getElementById("trMenu").style.display="none";
    window.print();
    document.getElementById("trMenu").style.display="block";
    }
    </script>

</head>
<body style="margin-left: 0px;">
    <form id="form1" runat="server">
    <div>
        <table width="100%" align="left">
            <tr id="trMenu" runat="server" visible="false">
                <td nowrap="nowrap">
                    <img src="../Imgs/print.png" alt="" onclick="callprint();" style="cursor: pointer;" />&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="ibtnExport" runat="server" ImageUrl="~/Imgs/excel.png" OnClick="ibtnExport_Click"
                        Style="cursor: pointer;" />
                </td>
            </tr>
            <tr>
                <td runat="server" id="tdHeading" align="center">
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divTable" runat="server" style="width: 97%;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
