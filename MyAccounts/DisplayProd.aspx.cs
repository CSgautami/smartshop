﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using BLL;
using System.IO;


namespace MyAccounts20
{
    public partial class DisplayProd : System.Web.UI.Page
    {
        DisplayProd_BLL Obj_BLL = new DisplayProd_BLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Session["UserID"].ToString() != "0")
                {
                    Response.Redirect("Login.aspx");
                }
                if (!IsPostBack)
                {
                    txtProduct.Focus();
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void FillGrid()
        {
            try
            {
                DisplayProd_BLL Obj_BLL = new DisplayProd_BLL();
                DataSet ds = new DataSet();
                ds = Obj_BLL.GetDisplayProd();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        DataRow dr = ds.Tables[0].NewRow();
                        dr[0] = DBNull.Value;
                        ds.Tables[0].Rows.Add(dr);
                        gvDisplayProducts.DataSource = ds;
                        gvDisplayProducts.DataBind();
                        gvDisplayProducts.Rows[0].Cells[2].Text = "";
                    }
                    else
                    {
                        gvDisplayProducts.DataSource = ds;
                        gvDisplayProducts.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.InsertDisplayProd(txtProduct.Text, txtPrice.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Product";
            }
            else if (res == "-1")
            {
                lblMsg.Text = "Product already existed";
                txtProduct.Focus();

            }
            else if (res == "1")
            {
                lblMsg.Text = "Product saved successfully";
                FillGrid();
                txtProduct.Text = "";
                txtPrice.Text = "";
                txtProduct.Focus();
            }
        }

        protected void gvDisplayProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDisplayProducts.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string res = Obj_BLL.UpdateDisplayProd(hidID.Value, txtProduct.Text, txtPrice.Text);
            if (res == "0" || res == "")
            {
                lblMsg.Text = "Error while saving Product";


            }
            else if (res == "-1")
            {
                lblMsg.Text = "Product already existed";
                txtProduct.Focus();

            }
            else if (res == "1")
            {
                lblMsg.Text = "Product Updated successfully";
                FillGrid();
                txtProduct.Text = "";
                txtPrice.Text = "";
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                txtProduct.Focus();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            gvDisplayProducts.PageIndex = 0;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            txtProduct.Text = "";
            txtPrice.Text = "";
            lblMsg.Text = "";
        }
        public void lbtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidID.Value = ((HtmlInputHidden)grow.FindControl("hidID")).Value;
                txtProduct.Text = ((Label)grow.FindControl("lblProduct")).Text;
                txtPrice.Text = ((Label)grow.FindControl("lblPrice")).Text;
                txtProduct.Focus();
                btnUpdate.Visible = true;
                btnSave.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                GridViewRow grow = (GridViewRow)lbtn.NamingContainer;
                hidID.Value = ((HtmlInputHidden)grow.FindControl("hidID")).Value;
                DisplayProd_BLL Obj_BLL = new DisplayProd_BLL();
                string res = Obj_BLL.DeleteDisplayProd(hidID.Value);
                if (res == "" || res == "0")
                {
                    lblMsg.Text = "Error While Deleting Product";
                    return;

                }
                if (res == "1")
                {
                    lblMsg.Text = "Product Deleted Successfully";
                    FillGrid();
                    txtProduct.Text = "";
                    txtPrice.Text = "";
                    txtProduct.Focus();
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnCancel.Visible = true;
                }
            }

            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
    
