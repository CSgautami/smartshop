﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using BLL.Reports;
namespace MyAccounts20
{
    public partial class SMSMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtMessage.Focus();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Reports_BLL obj_Bll = new Reports_BLL();
            string Phone; string Mobile;
            DataSet ds = obj_Bll.GetMember();
            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Phone = dr["Phone"].ToString();
                    string[] ary = Phone.Split(',');                             
                    if (ary.Length > 1)
                        Phone = ary[1];
                    //char[] chartotrim = { ',', ' ' };
                    //Phone = Phone.TrimStart(chartotrim);                    
                    if (Phone.ToString() != "")
                    {
                        SMS obj = new SMS();
                        obj.SendSMS("SmartHomeShop", "smart33399", Phone, txtMessage.Text, "N");
                    }
                }
            }
        }
    }
}
