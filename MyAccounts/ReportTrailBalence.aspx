﻿<%@ Page Language="C#" MasterPageFile="~/MyAccounts20.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ReportTrailBalence.aspx.cs" Inherits="MyAccounts20.ReportTrailBalence" Title="Report Trail Balance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <link href="Calendar.css" rel="stylesheet" type="text/css" />
    <script src="JS/CalendarPopup.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        document.write(getCalendarStyles());        
        var cal=new CalendarPopup("divCalendar");        
        cal.showNavigationDropdowns();
        function showcalendar(t)
        {
            cal.select(t,t.id,'dd-MM-yyyy');
        }
        var ddlBranch;
        var chkSerialByName;
        var chkGroups;
        var chkOpenings;
        var rDebtors;
        var chkLSTGroups;
        var chkDateWise;
        var rAllLedgers;
        var rGroups;
        var dtpDate;
        var chkShowDebtors;    
        var dtFormat='dd-MM-yyyy';
        var UserID;
        var strGIDs;
        var hidGBID;
    preid ='ctl00_ContentPlaceHolder1_'
    function GetControls()
    {
        ddlBranch=document.getElementById(preid+"ddlBranch");
        chkSerialByName=document.getElementById(preid+"chkSerialByName"); 
        chkGroups = document.getElementById(preid+"chkAllGroups");    
        chkOpenings=document.getElementById(preid+"chkOpenings");
        rDebtors=document.getElementById(preid+"rDebtors");
        chkLSTGroups=document.getElementById(preid+"chkLSTGroups");
        chkDateWise = document.getElementById(preid+"chkDateWise");
        rAllLedgers = document.getElementById(preid+"rAllLedgers");
        rGroups=document.getElementById(preid+"rGroups");
        dtpDate=document.getElementById(preid+"txtDate");
        chkShowDebtors=document.getElementById(preid+"chkShowDebtors");
        UserID=document.getElementById(preid+"hidUserID");
    }       
 
    function getXml(xmlString)
    {
        xmlObj=null;
        try {
                var browserName = navigator.appName;
                if (browserName == "Microsoft Internet Explorer") {
                    xmlObj = new ActiveXObject("Microsoft.XMLDOM");
                    xmlObj.async = "false";
                    xmlObj.loadXML(xmlString);
            }
            else 
            {
                xmlObj = (new DOMParser()).parseFromString(xmlString, "text/xml");
            }

        }
        catch (err) {
        }    
    }
    function GetGroups()
    {        
        PageMethods.GetGroups(GetGroupComplete);    
    }

    function GetGroupComplete(res)
    {
        getXml(res);    
        chkLSTGroups.options.length=0;
        var opt=document.createElement("OPTION");        
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("GroupName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("GroupID")[0].firstChild.nodeValue;
            chkLSTGroups.options.add(opt);
        }                    
    }
    function GetBranch()
    {        
        PageMethods.GetBranch(UserID.value, GetBranchComplete);    
    }

    function GetBranchComplete(res)
    {
        getXml(res);    
        ddlBranch.options.length=0;
        var opt=document.createElement("OPTION");
        if(xmlObj.getElementsByTagName("Table").length != 1)
        {
            opt=document.createElement("OPTION");
            //opt.text="Select";
            opt.text="All";
            opt.value="0";    
            ddlBranch.options.add(opt);
            ddlBranch.value="0";
        }
        for(var i=0;i<xmlObj.getElementsByTagName("Table").length; i++)
        {
            opt=document.createElement("OPTION");
            opt.text=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchName")[0].firstChild.nodeValue;
            opt.value=xmlObj.getElementsByTagName("Table")[i].getElementsByTagName("BranchID")[0].firstChild.nodeValue;
            ddlBranch.options.add(opt);
        }            
        if(hidGBID==null)
            GetDefaultBranch();
        ddlBranch.value=hidGBID.value;
    }
    function GetDefaultBranch()
    {
        hidGBID=document.getElementById(preid+"hidGBID");
    }
    function SetData()
    {    
        GetControls();
        dtpDate.value = new Date().format("dd-MM-yyyy");
        GetBranch();
        chkSerialByName.checked=true;
        rAllLedgers.checked=true;
        //GetGroups();
        
    }
    function chkAll_Click()    
    {  
        SetData();      
        for (var i=0;i<chkLSTGroups.childNodes.length;i++)
        {            
            if(chkGroups.checked==true)
            {
                chkLSTGroups.childNodes[i].checked=true;                                
            }
            else 
            {
                chkLSTGroups.childNodes[i].checked=false;                                
            }
        }        
    }
    function ShowData(SBName,BID,bGroup,bopening,bDebitor,strGIDs,bDWise,Date,bDCondenced)
    {        
        window.frames["frmReportTrailBalence"].location.href="Reports/TrailBalenceReport.aspx?bOrderName="+SBName+"&BranchID="+BID+"&bGroup="+bGroup+"&bOpenings="+bopening+"&bDebitor="+bDebitor+"&strGIDs="+strGIDs+"&bDtwise="+bDWise+"&Date="+Date+"&bDebiConde="+bDCondenced;
    }
//    function btnShow_Click()
//    {       
//        window.frames["frmReportTrailBalence"].location.href="Reports/TrailBalenceReport.aspx?bOrderName="+chkSerialByName.checked+"&BranchID="+ddlBranch.value+"&bGroup="+chkGroups.checked+"&bOpenings="+chkOpenings.checked+"&bDebitor="+rDebtors.checked+"&strGIDs="+chkLSTGroups.checked+"&bDtwise="+chkDateWise.checked+"&Date="+dtpDate.value+"&bDebiConde="+chkShowDebtors.checked;
//    }    
    function Debitor_Click()
    {
        SetData();
        rAllLedgers.checked=false;
        rGroups.checked=false;        
    }     
    function AllLedgers_Click()
    {
        SetData();
        rDebtors.checked=false;
        rGroups.checked=false;        
    }    
    function DateWise_Click()
    {
        SetData();
        if(chkDateWise.checked==true)
            dtpDate.disabled=false;
        else
            dtpDate.disabled=true;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
        <table cellpadding="0" cellspacing="0" align="center" style="width: 900px;">
        
            <tr>
                <td width="100%" align="left" class="Content" style="min-height: 450px; height: 450px;"
                    valign="top">
                    <table width="100%">
                        <tr>
                            <td>
                                <table align="center">
                                    <tr>
                                        <td height="20px" colspan="100%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:CheckBox ID="chkOpenings" Text="Openings Only" runat="server" />
                                        </td>
                                        <td style="width:30px;"></td>
                                        <td align="left">
                                            <asp:CheckBox id="chkSerialByName" Text="Serial By Name"  runat="server"/>                                            
                                        </td>    
                                        <td style="width:30px;"></td>
                                        <td>
                                            Branch
                                        </td>
                                        <td style="width:30px;"></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlBranch" runat="server" Width="250px"></asp:DropDownList>
                                        </td>
                                        <td style="width:30px;"></td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkAllGroups" Enabled="false" Text="All Groups" 
                                                runat="server" AutoPostBack="true"
                                                oncheckedchanged="chkAllGroups_CheckedChanged"/>
                                        </td>
                                    </tr>                                    
                                    <tr>                                  
                                        <td align="left">
                                            <asp:RadioButton ID="rAllLedgers" Text="All Ledgers" runat="server" 
                                                AutoPostBack="true" oncheckedchanged="rAllLedgers_CheckedChanged" />
                                        </td>  
                                        <td></td>                                      
                                        <td align="left">
                                            <asp:RadioButton ID="rDebtors" Text="Debtors" runat="server" 
                                                AutoPostBack="true" oncheckedchanged="rDebtors_CheckedChanged" />
                                        </td>  
                                        <td></td>
                                        <td align="left">
                                            <asp:RadioButton ID="rGroups" Text="Groups" runat="server" AutoPostBack="true" 
                                               oncheckedchanged="rGroups_CheckedChanged"/>
                                        </td>    
                                        <td></td>                                     
                                        <td align="left" rowspan="3" valign="top">
                                            <div id="divGroup" style="position:relative;width:250px;height:60px;overflow:scroll">
                                                <asp:CheckBoxList ID="chkLSTGroups" runat="server" Height="100px" Width="250px"
                                                 RepeatLayout="Flow" RepeatDirection="Vertical" Enabled="false"></asp:CheckBoxList>
                                            </div>
                                        </td>    
                                        <td></td>                                    
                                        <td align="left">
                                            <asp:CheckBox ID="chkDateWise" Text="Date Wise" runat="server" 
                                                AutoPostBack="true" oncheckedchanged="chkDateWise_CheckedChanged"/>
                                        </td>                                        
                                    </tr>                                    
                                    <tr>
                                        <td colspan="5" align="left">
                                            <asp:CheckBox ID="chkShowDebtors" Text="Show Debtors Condenced" runat="server" />
                                        </td>
                                        <td></td>  
                                        <td></td>                                      
                                        <td align="left">
                                            <asp:TextBox ID="txtDate" Enabled="false" runat="server" Width="90px" onfocus="showcalendar(this);" onkeydown="HideCalendar(event);" onclick="showcalendar(this);" style="width:100px;"></asp:TextBox>
                                        </td>
                                        <td align="right">
                                            <%--<input type="button" id="btnShow" value="Show" onclick="btnShow_Click();" />--%>
                                            <asp:Button ID="btnShow" runat="server" Text="Show" onclick="btnShow_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <iframe name="frmReportTrailBalence" id="frmReportTrailBalence"
                                    style="width: 100%; height: 400px;" frameborder="0" scrolling="auto" ></iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>          
        </table>
    </div>
    <div id="divCalendar" class="Calendar">
    </div>
    <div>
          <input type="hidden" id="hidUserID" runat="server" />
          <input type="hidden" runat="server" id="hidGBID" />
    </div>
</asp:Content>
